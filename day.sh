#! /bin/sh
FILE=./Stat/`date +%m_%Y`.stat
FILE_OUT=./Stat/`date +%m_%Y`_day.stat
tail -24 ${FILE}  | ./Stat/day.awk | tee -a ${FILE_OUT} 
return 0;
