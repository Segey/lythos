/*
 * File:   main.cpp
 * Author: dix75
 *
 * Created on 22 Апрель 2010 г., 1:29
 */
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    vCalcRepeats calc;
    calc.show();
    calc.test();

    return app.exec();
}
