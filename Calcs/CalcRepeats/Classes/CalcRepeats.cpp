/* 
 * File:   CalcRepeats.cpp
 * Author: S.Panin
 * 
 * Created on Ср апр. 28 2010, 13:11:56
 */
//------------------------------------------------------------------------------
#include "CalcRepeats.h"
//------------------------------------------------------------------------------
void vCalcRepeats::show()
{
        set_settings_table("CalcRepeats");
        set_window_title(vtr::Calc::tr("Calculator \"Repetitions\""));
        set_title_first(vtr::Body::tr("Weight"));
        set_title_second(vtr::Calc::tr("The maximum weight in one repetition"));

        inherited::show();
}
//------------------------------------------------------------------------------
double vCalcRepeats::do_execute(v_calc_base::methods method, double weight, double max)
{
        if ( weight > max ) return 0.0;
        else if ( static_cast<int>(weight+0.5) == static_cast<int>(max+0.5) ) return 1.0;
        
        switch(method)
        {
                default:
                case v_calc_base::brzycki       : return (1.0278 - (weight / max)) / 0.0278;
                case v_calc_base::apple         : return (max - weight) / (weight * 0.033);
                case v_calc_base::lander        : return (1.013 - ( weight / max )) / 0.0267123;
        }
}
//------------------------------------------------------------------------------
bool vCalcRepeats::test()
{
        double d=0.0;
        Q_ASSERT(inherited::test());
        Q_ASSERT( do_execute(v_calc_base::brzycki, 77.0, 77.0) ==1 );
        Q_ASSERT( do_execute(v_calc_base::brzycki, 77.0, 75.0) ==0 );
        d =do_execute(v_calc_base::brzycki, 6.0, 10.0);
        Q_ASSERT( d >15.38 && d <15.4 );
        d =do_execute(v_calc_base::brzycki, 26.0, 100.0);
        Q_ASSERT( d > 27.61 && d <27.63 );

        Q_ASSERT( do_execute(v_calc_base::apple, 77.0, 77.0) ==1 );
        Q_ASSERT( do_execute(v_calc_base::apple, 77.0, 75.0) ==0 );
        d =do_execute(v_calc_base::apple, 6.0, 10.0);
        Q_ASSERT( d >20.1 && d <20.3 );
        d =do_execute(v_calc_base::apple, 26.0, 100.0);
        Q_ASSERT( d > 86.2 && d <86.3 );

        Q_ASSERT( do_execute(v_calc_base::lander, 77.0, 77.0) ==1 );
        Q_ASSERT( do_execute(v_calc_base::lander, 77.0, 75.0) ==0 );
        d =do_execute(v_calc_base::lander, 6.0, 10.0);
        Q_ASSERT( d >15.4 && d <15.5 );
        d =do_execute(v_calc_base::lander, 26.0, 100.0);
        Q_ASSERT( d > 28.1 && d <28.2 );

        return true;
}
//------------------------------------------------------------------------------