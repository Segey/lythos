/* 
 * File:   CalcRepeats.h
 * Author: S.Panin
 *
 * Created on Ср апр. 28 2010, 13:11:56
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_REPEATS_H_
#define	_V_CALC_REPEATS_H_
//------------------------------------------------------------------------------
class vCalcRepeats :  public vCalcBase
{
    typedef                         vCalcBase                                   inherited;
    typedef                         vCalcRepeats                                class_name;

    virtual double                  do_execute(v_calc_base::methods method, double first, double second);

public:
    void                            show();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_REPEATS_H_ */
