/*
 * File:   main.cpp
 * Author: dix75
 *
 * Created on 29 Апрель 2010 г., 10:39
 */
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
        QApplication app(argc, argv);

        vCalcMaxWeight calc;
        calc.show();
        calc.test();

        return app.exec();
}
