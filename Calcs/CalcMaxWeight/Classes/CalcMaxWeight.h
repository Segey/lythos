/* 
 * File:   CalcMaxWeight.h
 * Author: S.Panin
 *
 * Created on Чт апр. 29 2010, 10:44:34
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_MAX_WEIGHT_H_
#define	_V_CALC_MAX_WEIGHT_H_
//------------------------------------------------------------------------------
class vCalcMaxWeight :  public vCalcBase
{
    typedef                         vCalcBase                                   inherited;
    typedef                         vCalcMaxWeight                              class_name;

    virtual double                  do_execute(v_calc_base::methods method, double first, double second);

public:
    void                            show();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_MAX_WEIGHT_H_ */
