/* 
 * File:   CalcMaxWeight.cpp
 * Author: S.Panin
 * 
 * Created on Чт апр. 29 2010, 10:44:34
 */
//------------------------------------------------------------------------------
#include "CalcMaxWeight.h"
//------------------------------------------------------------------------------
void vCalcMaxWeight::show()
{
        set_settings_table("CalcMaxWeight");
        set_window_title(vtr::Calc::tr("Calculator \"Maximum Weight\""));
        set_title_first(vtr::Body::tr("Weight"));
        set_title_second(vtr::Calc::tr("Number of repetitions"));

        inherited::show();
}
//------------------------------------------------------------------------------
double vCalcMaxWeight::do_execute(v_calc_base::methods method, double weight, double reps)
{
        if ( 0.99  <reps && reps <1.01) return weight;
        
        switch(method)
        {
                default:
                case v_calc_base::brzycki       : return  weight / (1.0278 - (0.0278 * reps));
                case v_calc_base::apple         : return (weight * reps * 0.033) + weight;
                case v_calc_base::lander        : return weight / (1.013 - (0.0267123 * reps));
        }
}
//------------------------------------------------------------------------------
bool vCalcMaxWeight::test()
{
        double d=0.0;
        Q_ASSERT(inherited::test());
        Q_ASSERT( do_execute(v_calc_base::brzycki, 77.0, 1.0) ==77.0 );
        d =do_execute(v_calc_base::brzycki, 10.0, 6.0);
        Q_ASSERT( d >11.61 && d <11.62 );
        d =do_execute(v_calc_base::brzycki, 100.0, 26.0);
        Q_ASSERT( d > 327.86 && d <327.87 );

        Q_ASSERT( do_execute(v_calc_base::apple, 77.0, 1.0) ==77.0 );
        d =do_execute(v_calc_base::apple, 10.0, 6.0);
        Q_ASSERT( d >11.9 && d <12.0 );
        d =do_execute(v_calc_base::apple, 100.0, 26.0);
        Q_ASSERT( d > 185.7 && d <185.9 );

        Q_ASSERT( do_execute(v_calc_base::lander, 77.0, 1.0) ==77.0 );
        d =do_execute(v_calc_base::lander, 10.0, 6.0);
        Q_ASSERT( d >11.72 && d <11.73 );
        d =do_execute(v_calc_base::lander, 100.0, 26.0);
        Q_ASSERT( d > 313.9 && d <314.0 );

        return true;
}
//------------------------------------------------------------------------------