/* 
 * File:   CalcWorkingWeight.h
 * Author: S.Panin
 *
 * Created on Чт апр. 29 2010, 11:00:09
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_WORKING_WEIGHT_H_
#define	_V_CALC_WORKING_WEIGHT_H_
//------------------------------------------------------------------------------
class vCalcWorkingWeight :  public vCalcBase
{
    typedef                         vCalcBase                                   inherited;
    typedef                         vCalcWorkingWeight                          class_name;

    virtual double                  do_execute(v_calc_base::methods method, double first, double second);

public:
    void                            show();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_WORKING_WEIGHT_H_ */
