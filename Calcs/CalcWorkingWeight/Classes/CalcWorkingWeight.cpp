/* 
 * File:   CalcWorkingWeight.cpp
 * Author: S.Panin
 * 
 * Created on Чт апр. 29 2010, 11:00:09
 */
//------------------------------------------------------------------------------
#include "CalcWorkingWeight.h"
//------------------------------------------------------------------------------
void vCalcWorkingWeight::show()
{
        set_settings_table("CalcWorkingWeight");
        set_window_title(vtr::Calc::tr("Calculator \"Working Weight\""));
        set_title_first(vtr::Calc::tr("Number of repetitions"));
        set_title_second(vtr::Calc::tr("The maximum weight in one repetition"));

        inherited::show();
}
//------------------------------------------------------------------------------
double vCalcWorkingWeight::do_execute(v_calc_base::methods method, double number, double max)
{       
        if (number ==1.0) return max;
        
        switch(method)
        {
                default:
                case v_calc_base::brzycki       : return (1.0278 - (0.0278 * number)) * max;
                case v_calc_base::apple         : return ::sqrt((max * max) / (0.066 * number + 0.001089 * number * number + 1));
                case v_calc_base::lander        : return (1.013 - (0.0267123 * number)) * max;
        }
}
//------------------------------------------------------------------------------
bool vCalcWorkingWeight::test()
{
        double d=0.0;
        Q_ASSERT( inherited::test() );
        Q_ASSERT( do_execute(v_calc_base::brzycki, 1.0, 77.0) ==77.0 );
        d =do_execute(v_calc_base::brzycki, 6.0, 10.0);
        Q_ASSERT( d >8.6 && d <8.62 );
        d =do_execute(v_calc_base::brzycki, 26.0, 100.0);
        Q_ASSERT( d > 30.4 && d <30.6 );

        Q_ASSERT( do_execute(v_calc_base::apple, 1.0, 77.0) ==77.0 );

        d =do_execute(v_calc_base::apple, 6.0, 10.0);
        Q_ASSERT( d >8.34 && d <8.36 );
        d =do_execute(v_calc_base::apple, 26.0, 100.0);
        Q_ASSERT( d > 53.8 && d <53.9 );

        Q_ASSERT( do_execute(v_calc_base::lander, 1.0, 77.0) ==77.0 );

        d =do_execute(v_calc_base::lander, 6.0, 10.0);
        Q_ASSERT( d >8.52 && d <8.53 );
        d =do_execute(v_calc_base::lander, 26.0, 100.0);
        Q_ASSERT( d > 31.84 && d <31.85 );

        return true;
}
//------------------------------------------------------------------------------