// -*- C++ -*-
/* 
 * File:   pch.h
 * Author: S.Panin
 *
 * Created on 22 Апреля 2010 г., 22:32
 */
//------------------------------------------------------------------------------
#ifndef _V_PCH_H_
#define	_V_PCH_H_
//------------------------------------------------------------------------------
#include "../../Go/go.h"
#include "../../Go/go/noncopyable.h"

#include "../../Go/Component/MainWindow/CalcBase/CalcBase.h"
#include "Classes/CalcWorkingWeight.h"
//------------------------------------------------------------------------------
#endif	/* _V_PCH_H */

