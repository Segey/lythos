#ifndef GO_NONCOPYABLE_H
#define GO_NONCOPYABLE_H
//------------------------------------------------------------------------------
//--------------------  Go noncopyable.hpp header file  ------------------------

//------------------------------------------------------------------------------
namespace go {

//  Private copy constructor and copy assignment ensure classes derived from
//  class noncopyable cannot be copied.

//------------------------------------------------------------------------------
class noncopyable
{
public:
                                    noncopyable()                               = default;
                                    ~noncopyable()                              = default;
private:
                                    noncopyable(const noncopyable&)             = delete;
    const noncopyable&              operator=  (const noncopyable&)             = delete;
};
//------------------------------------------------------------------------------
} // namespace go

//------------------------------------------------------------------------------
#endif // GO_NONCOPYABLE_H
