#ifndef GO_CONTAINERABLE_H
#define GO_CONTAINERABLE_H
//------------------------------------------------------------------------------
//--------------------  Go containerable.hpp header file  ----------------------

//------------------------------------------------------------------------------
namespace go {

//  Private copy constructor and copy assignment ensure classes derived from
//  class containerable cannot be copied.

//------------------------------------------------------------------------------
template< template<typename> class T >
class containerable
{
    typedef                         T                                           container;

public:
                                    containerable()                             =default;
                                    ~containerable()                            =default;
    bool                            empty()                                     { return size() ==0; }
    virtual container::size_type    size()                                      =0;
    virtual container::iterator     begin()                                     =0;
    virtual container::iterator     end()                                       =0;
};
//------------------------------------------------------------------------------
} // namespace go

//------------------------------------------------------------------------------
#endif // GO_CONTAINERABLE_H
