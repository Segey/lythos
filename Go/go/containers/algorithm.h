// -*- C++ -*-
/* 
 * File:   algorithm.h
 * Author: S.Panin
 *
 * Created on 28 Декабрь 2009 г., 11:57
 */
//------------------------------------------------------------------------------
#ifndef _V_ALGORITHM_H
#define	_V_ALGORITHM_H
//------------------------------------------------------------------------------
#include "algorithm"
//------------------------------------------------------------------------------
namespace go {      // begin namespace GO#
//------------------------------------------------------------------------------
template <typename Iterator>
inline bool is ( Iterator first_begin, Iterator first_end, Iterator second_begin, Iterator second_end )
{
        for ( ; second_begin != second_end && std::find(first_begin,first_end,*second_begin) != first_end ; ++second_begin) { }
        return second_begin == second_end;
}
//------------------------------------------------------------------------------
template <typename Container>
inline bool is ( const Container& first, const Container& second )
{
        return go::is(first.begin(), first.end(), second.begin(), second.end());
}
};                  // end namespace GO
//------------------------------------------------------------------------------
#endif	/* _V_ALGORITHM_H */

