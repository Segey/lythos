/**
    \file   exception.h
    \brief  Файл exception.h

    \author S.Panin
    \date   Created on 8 Декабрь 2010 г., 1:30
 */
//------------------------------------------------------------------------------
#ifndef _EXCEPTION_H_
#define	_EXCEPTION_H_
//------------------------------------------------------------------------------
#include <stdexcept>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
/**
    \brief Thrown to indicate file not found.
 */
class file_not_found : public std::runtime_error
{
    typedef             std::runtime_error                                      inherited;

  public:
    explicit file_not_found(const std::string& arg) : inherited(arg)            {}
};

/**
    \brief Thrown to indicate bad_data.
 */
class bad_data : public std::runtime_error
{
    typedef             std::runtime_error                                      inherited;

  public:
    explicit bad_data(const std::string& arg) : inherited(arg)            {}
};
//------------------------------------------------------------------------------
} /* end namespace go  */
//------------------------------------------------------------------------------
#endif	/* _EXCEPTION_H_ */

