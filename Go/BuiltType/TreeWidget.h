/* 
 * File:   TreeWidget.h
 * Author: S.Panin
 *
 * Created on 17 Марта 2009 г., 14:52
 */
//------------------------------------------------------------------------------
#ifndef _V_BT_TREE_WIDGET_H_
#define	_V_BT_TREE_WIDGET_H_
//------------------------------------------------------------------------------
struct vbtTreeWidget
{

static inline int get_header_item_index(QTreeWidgetItem* item, const QString& str)
    {
        for(int i = 0; i < item->columnCount(); ++i)
            if(item->text(i) == str) return i;
        return -1;
    }

static inline QStringList get_header_items_indexs_hidden(QTreeWidget* item, const QStringList& list)
    {
        QStringList result;
        int index;

        for (QStringList::const_iterator it=list.begin(); it !=list.end(); ++it)
        {
            index =get_header_item_index(item->headerItem(), *it);
            if ( item->header()->isSectionHidden(index)) result << QString::number(index);
        }

        return result;
    }

static inline void set_header_items_width_and_hidden(QTreeWidget* item, const QStringList& list =QStringList(), int value_default =150 )
    {
        for(int x = 0; x < item->columnCount(); ++x)
        {          
            if( list.size() > x && v_user::get_int(list[x]) ==0 )
            {
                item->header()->setSectionHidden(x, true);
                item->setColumnWidth(x, value_default );
            }
            else item->setColumnWidth(x, list.size() > x ? v_user::get_int(list[x]) : value_default );
        }

        // O(2n)
    }

static inline QStringList get_header_items_widths(QTreeWidget* item)
    {
        QStringList result;

        for (int x=0; x < item->columnCount(); ++x)
                result << QString::number(item->columnWidth(x));

        return result;
    }

    bool test()
    {
       QStringList lst =QStringList() << vtr::Date::tr("Date") << vtr::Body::tr("Weight") << vtr::Body::tr("Chest");
       QTreeWidget item;
       item.setHeaderLabels( QStringList() << vtr::Body::tr("Chest")  << vtr::Body::tr("Bicep right") << vtr::Body::tr("Abdomen") );
       item.setColumnWidth(0, 20);
       item.setColumnWidth(1, 210);
       item.setColumnWidth(2, 10);
       
       QStringList widths =get_header_items_widths(&item);
       Q_ASSERT(widths.size() == 3);
       Q_ASSERT(widths[0] == "20");
       Q_ASSERT(widths[1] == "210");
       Q_ASSERT(widths[2] == "10");
       
       item.header()->setSectionHidden(0, true);
       item.header()->setSectionHidden(1, true);
       item.header()->setSectionHidden(2, true);
       QStringList result = get_header_items_indexs_hidden(&item, lst);

       Q_ASSERT(result.size() == 1);
       Q_ASSERT(result[0] == "0");

       lst << vtr::Body::tr("Bicep right") ;
       result = get_header_items_indexs_hidden(&item, lst);
       Q_ASSERT(result.size() == 2);
       Q_ASSERT(result[1] == "1");
       
       lst << vtr::Body::tr("Abdomen") ;
       result = get_header_items_indexs_hidden(&item, lst);
       Q_ASSERT(result.size() == 3);
       Q_ASSERT(result[2] == "2");
       
       lst << vtr::Body::tr("Abdomen") ;
       result = get_header_items_indexs_hidden(&item, lst);
       Q_ASSERT(result.size() == 4);
       Q_ASSERT(result[3] == "2");

       item.header()->setSectionHidden(0, false);
       item.header()->setSectionHidden(1, false);
       item.header()->setSectionHidden(2, false);
       set_header_items_width_and_hidden(&item);

       Q_ASSERT(item.columnWidth(0) == 150);
       Q_ASSERT(item.columnWidth(1) == 150);
       Q_ASSERT(item.columnWidth(2) == 150);

       set_header_items_width_and_hidden(&item, QStringList() <<"20" << "ii", 80);
       Q_ASSERT(item.columnWidth(0) == 20);
       Q_ASSERT(item.columnWidth(1) == 0);
       Q_ASSERT(item.columnWidth(2) == 80);
       return true;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_BT_TREE_WIDGET_H_ */
