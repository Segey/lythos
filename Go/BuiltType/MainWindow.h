/* 
 * File:   MainWindow.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 15:50
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_WINDOW_H_
#define	_V_MAIN_WINDOW_H_
//------------------------------------------------------------------------------
struct vMainWindow
{
    static void   add_Menu_and_ToolBar(QAction* action, QMenu* menu, QToolBar* tool)
    {
        menu->addAction(action);
        tool->addAction(action);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_WINDOW_H_ */
