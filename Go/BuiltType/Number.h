// -*- C++ -*-
/* 
 * File:   String.h
 * Author: S.Panin
 *
 * Created on 1 Январь 2010 г., 14:56
 */
//------------------------------------------------------------------------------
#ifndef _V_NUMBER_V_
#define	_V_NUMBER_V_
//------------------------------------------------------------------------------
namespace go {
//------------------------------------------------------------------------------
struct number
{

static int get_int(const QString& text, int default_int =0)
{
        bool done;
        const int value=text.toInt(&done);
        return done ? value : default_int;
}
//------------------------------------------------------------------------------
bool test()
{
        QString text = QLatin1String("111");
        Q_ASSERT( get_int(text) ==111 );
        
        text = QLatin1String("111ddd");
        Q_ASSERT( get_int(text,5) ==5 );

        text = QLatin1String("11555551");;
        Q_ASSERT( get_int(text) ==11555551 );
        return true;
}
};
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_NUMBER_V_ */

