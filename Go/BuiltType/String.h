// -*- C++ -*-
/* 
 * File:   String.h
 * Author: S.Panin
 *
 * Created on 1 Январь 2010 г., 14:56
 */
//------------------------------------------------------------------------------
#ifndef _V_STRING_H_
#define	_V_STRING_H_
//------------------------------------------------------------------------------
GO_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
struct string
{
//------------------------------------------------------------------------------
static inline QString get_string_with_trim(const QString& str)
{
    return QLatin1String(" ") + str + QLatin1String(" ");
}
//------------------------------------------------------------------------------
static QString    get_string_modified(QString text, bool value)
    {
        if( text.endsWith(QLatin1Char('*')) && !value ) text.chop(1);
        else if( !text.endsWith(QLatin1Char('*')) && value ) text.push_back(QLatin1Char('*'));
        return text;
    }
//------------------------------------------------------------------------------
bool test()
    {
        QString text = QLatin1String("000*");
        text =get_string_modified(text,true);
        Q_ASSERT(text == QLatin1String("000*"));

        text =get_string_modified(text,false);
        Q_ASSERT(text == QLatin1String("000"));

        text =get_string_modified(text,true);
        Q_ASSERT(text == QLatin1String("000*"));

        text =get_string_modified(text,false);
        Q_ASSERT(text == QLatin1String("000"));

        text = QLatin1String("hello");
        Q_ASSERT(get_string_with_trim(text) == QLatin1String(" hello ") );
        return true;
    }
};
//------------------------------------------------------------------------------
GO_END_NAMESPACE
//------------------------------------------------------------------------------
#endif	/* _V_STRING_H_ */

