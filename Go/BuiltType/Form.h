/**
    \file   Form.h
    \brief  Файл Базовых функций для всех форм

    \author S.Panin
    \date   Created on 9 February 2011 г., 0:52
 */
//------------------------------------------------------------------------------
#ifndef _BUILT_TYPE_FORM_H_
#define	_BUILT_TYPE_FORM_H_
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace built_type {
//------------------------------------------------------------------------------
/**
\brief Базовый класс EG.
*/
struct Form
{
    static float               correct_with_number()                            { return 1.62; }
/**
\brief Функция получения идеального размера окна.
\param size - Желаемые размеры окна.
\result QRect - Идеальные размеры окна.

Пример использования:
\code
setGeometry(go::built_type::Form::ideal_size(QDesktopWidget().size()*0.75));
\endcode
*/
static inline QRect  ideal_size(const QSize& size)
{
    const int height = size.height();
    const int width = size.height()*correct_with_number();
    const int x =( QDesktopWidget().screenGeometry().width() - width ) >>1;
    const int y =( QDesktopWidget().screenGeometry().height() - height ) >>1;
    return QRect(x, y, width, height );
}

};
//------------------------------------------------------------------------------
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _BUILT_TYPE_FORM_H_ */
