/* 
 * File:   Dialog.h
 * Author: S.Panin
 *
 * Created on 19 Июль 2009 г., 17:33
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_H_
#define	_V_DIALOG_H_
//------------------------------------------------------------------------------
// Deprecated see Form.h
struct vDialog
{
    static float               correct_with_number()                            { return 1.62; }
//------------------------------------------------------------------------------
template<typename T>static  inline void  size_hint(T* widget, float number =correct_with_number() )
    {
        widget->minimumSizeHint();
        widget->setMinimumWidth(widget->height()*number);
       // widget->setMinimumSize(widget->height()*number, widget->height());
    }

// Deprecated see Form.h
//------------------------------------------------------------------------------
template<typename T>static inline QRect  form_size_calculation(T* widget)
{
    const int height = widget->height();
    const int width = widget->height()*correct_with_number();
    const int x =( QDesktopWidget().screenGeometry().width() - width ) >>1;
    const int y =( QDesktopWidget().screenGeometry().height() - height ) >>1;
    return QRect(x, y, width, height );
}
    //------------------------------------------------------------------------------
static QString  get_window_title(const QString& text)
    {
        return QString("%1 - %2 %3").arg(text).arg(program::name_full()).arg(QLatin1String("[*]"));
    }
//------------------------------------------------------------------------------
template<typename T>static void     window_title(T* widget)
    {
        widget->setWindowTitle(QString(program::name_full()) + QLatin1String(" [*]"));
        widget->setWindowIcon(const_icon_16::program());
    }
//------------------------------------------------------------------------------
template<typename T>static void     window_title(T* widget, const QString& text)
    {
        widget->setWindowTitle(get_window_title(text));
        widget->setWindowIcon(const_icon_16::program());
    }
//------------------------------------------------------------------------------
    static QString              getBaseDatabaseFilters()                        { return vtr::Filter::tr("Database file %1All files %2").arg("(*.ldb *.ldb2 *.ldb3);;").arg("(*.*)"); }
    static QString              getImageFilters()                               { return vtr::Filter::tr("Image Files %1Microsoft Windows Bitmap BMP%2Graphics Interchange Format GIF %3Portable Network Graphics PNG %4JPEG %5XPixMap XPM %6PPM %7").arg("(*.BMP *.GIF *.PNG *.JPG *.JPEG *.JPE *.XPM *.PPM);;").arg(" (*.BMP);;").arg(" (*.GIF);;").arg(" (*.PNG);;").arg(" (*.JPG *.JPEG *.JPE);;").arg(" (*.XPM);;").arg(" (*.PPM);;"); }
    static QString              getReadWriteImageFilters()                      { return vtr::Filter::tr("Image Files %1Microsoft Windows Bitmap BMP%2Portable Network Graphics PNG %3JPEG %4").arg("(*.BMP *.PNG *.JPG *.JPEG *.JPE);;").arg(" (*.BMP);;").arg(" (*.PNG);;").arg(" (*.JPG *.JPEG *.JPE);;"); }
    static QString              getWriteRichFilters()                           { return vtr::Filter::tr("All supported types %1Adobe PDF Files %2ODF files %3Hyper Text Markup Language file %4Text Files %5").arg("(*.*);;").arg("(*.PDF);;").arg("(*.ODT);;").arg(" (*.HTM *.HTML);;").arg(" (*.TXT);;"); }
    static QString              getReadRichFilters()                            { return vtr::Filter::tr("All supported types %1ODF files %2Hyper Text Markup Language file %3Text Files %4").arg("(*.*);;").arg("(*.ODT);;").arg(" (*.HTM *.HTML);;").arg(" (*.TXT);;"); }
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_H_ */
