/* 
 * File:   vActionChart.h
 * Author: S.Panin
 *
 * Created on 8 Марта 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_CHART_H_
#define	_V_ACTION_CHART_H_
//------------------------------------------------------------------------------
#include "../Action.h"
//------------------------------------------------------------------------------
class vActionChart : private vAction
{
    typedef                         vAction                                     inherited;
    typedef                         vActionChart                                class_name;

public:
    static QAction*                 createVisibleCharts(QObject* parent);
    static QAction*                 createPanelOpen(QObject* parent);           // 1
    static QAction*                 createPanelClose(QObject* parent);          // 1
    static QAction*                 createShowAll(QObject* parent);             // 2
    static QAction*                 createHideAll(QObject* parent);             // 2
    static QAction*                 createStyleSimple(QObject* parent);         // 3
    static QAction*                 createStylePercentage(QObject* parent);     // 3
    static QAction*                 createStyleScale(QObject* parent);          // 3
    static QAction*                 createTypeHistogram(QObject* parent);       // 4
    static QAction*                 createTypePie(QObject* parent);             // 4
    static QAction*                 createTypeText(QObject* parent);            // 4
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_CHART_H_ */
