/* 
 * File:   vActionChart.cpp
 * Author: S.Panin
 * 
  * Created on 8 Марта 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#include "ActionChart.h"
#include "../../../tr.h"
#include "../../../Const/const_icon.h"
//------------------------------------------------------------------------------
QAction* vActionChart::createPanelOpen(QObject* parent) // 1
{
        return create(QIcon(), vtr::Chart::tr("&Open Chart Panel" , "Open Chart Panel"), vtr::Chart::tr("Open Panel" , "Open Chart Panel"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createPanelClose(QObject* parent) // 1
{
        return create(QIcon(), vtr::Chart::tr("&Close Chart Panel" , "Close Chart Panel"), vtr::Chart::tr("Close Panel" , "Close Chart Panel"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createShowAll(QObject* parent) // 2
{
        return create(QIcon(), vtr::Chart::tr("&Show All" , "Show All Charts"), vtr::Chart::tr("Show All Charts"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createHideAll(QObject* parent) // 2
{
        return create(QIcon(), vtr::Chart::tr("&Hide All" , "Hide All Charts"), vtr::Chart::tr("Hide All Charts"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createStyleSimple(QObject* parent) // 3
{
        return create(QIcon(), vtr::Chart::tr("&Simple"), vtr::Chart::tr("Style Simple"), parent ,   QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createStylePercentage(QObject* parent) // 3
{
        return create(QIcon(), vtr::Chart::tr("&Percentage"), vtr::Chart::tr("Style Percentage"), parent ,   QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createStyleScale(QObject* parent) // 3
{
        return create(QIcon(), vtr::Chart::tr("S&cale"), vtr::Chart::tr("Style Scale"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createTypeHistogram(QObject* parent) // 4
{
        return create(const_icon_32::chart_histogram(), vtr::Chart::tr("&Histogram"), vtr::Chart::tr("Type Histogram"), parent ,   QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createTypePie(QObject* parent)  // 4
{
        return create(const_icon_32::chart_pie(), vtr::Chart::tr("&Pie"), vtr::Chart::tr("Type Pie"), parent ,   QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionChart::createTypeText(QObject* parent) // 4
{
        return create(const_icon_32::chart_text(), vtr::Chart::tr("&Text"), vtr::Chart::tr("Type Text"), parent , QKeySequence::UnknownKey);
}