/* 
 * File:   vActionBody.cpp
 * Author: S.Panin
 * 
  * Created on 7 Марта 2009 г., 21:57
 */
//------------------------------------------------------------------------------
#include "ActionBody.h"
#include "../../../tr.h"
#include "../../../Const/const_icon.h"
//------------------------------------------------------------------------------
QAction* vActionBody:: createGrowth(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Growth"), vtr::Chart::tr("Chart \'Growth\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createBiceps(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Biceps"), vtr::Chart::tr("Chart \'Biceps\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createAbdomen(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Abdomen"), vtr::Chart::tr("Chart \'Abdomen\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createCalves(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("Ca&lfes"), vtr::Chart::tr("Chart \'Calves\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createChest(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Chest"), vtr::Chart::tr("Chart \'Chest\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createForearms(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Forearms"), vtr::Chart::tr("Chart \'Forearms\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createHips(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("Hi&ps"), vtr::Chart::tr("Chart \'Hips\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createNeck(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Neck"), vtr::Chart::tr("Chart \'Neck\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createShoulders(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Shoulders"), vtr::Chart::tr("Chart \'Shoulders\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createThighs(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Thighs"), vtr::Chart::tr("Chart \'Thighs\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createWaist(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("Wa&ist"), vtr::Chart::tr("Chart \'Waist\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createWeight(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("&Weight"), vtr::Chart::tr("Chart \'Weight\'"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vActionBody:: createWrist(QObject* parent)
{
        return create(QIcon(), vtr::Body::tr("W&rist"), vtr::Chart::tr("Chart \'Wrist\'"), parent , QKeySequence::UnknownKey);
}
