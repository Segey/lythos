/* 
 * File:   vActionBody.h
 * Author: S.Panin
 *
 * Created on 7 Марта 2009 г., 21:57
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_BODY_H
#define	_V_ACTION_BODY_H
//------------------------------------------------------------------------------
#include "../Action.h"
//------------------------------------------------------------------------------
class vActionBody : private vAction
{
    typedef                         vAction                                     inherited;
    typedef                         vActionBody                                 class_name;

public:
    static QAction*                 createOpen(QObject* parent);
    static QAction*                 createGrowth(QObject* parent);
    static QAction*                 createBiceps(QObject* parent);
    static QAction*                 createAbdomen(QObject* parent);
    static QAction*                 createCalves(QObject* parent);
    static QAction*                 createChest(QObject* parent);
    static QAction*                 createForearms(QObject* parent);
    static QAction*                 createHips(QObject* parent);
    static QAction*                 createNeck(QObject* parent);
    static QAction*                 createShoulders(QObject* parent);
    static QAction*                 createThighs(QObject* parent);
    static QAction*                 createWaist(QObject* parent);
    static QAction*                 createWeight(QObject* parent);
    static QAction*                 createWrist(QObject* parent);
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_BODY_H*/
