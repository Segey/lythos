/* 
 * File:   ActionWithText.cpp
 * Author: S.Panin
 * 
 * Created on Пт мая 7 2010, 23:49:51
 */
//------------------------------------------------------------------------------
#include "ActionWithText.h"
#include "../../../tr.h"
#include "../../../Const/const_icon.h"
//------------------------------------------------------------------------------
QAction* vActionWithText::add_group(QObject* parent, const QString& text, const QString& description)
{
        return vAction::create(const_icon_32::add_group(), text, description, parent , Qt::CTRL + Qt::SHIFT + Qt::Key_N);
}
//------------------------------------------------------------------------------
QAction* vActionWithText::add_item(QObject* parent, const QString& text, const QString& description)
{
        return vAction::create(const_icon_32::add_item(), text, description, parent , Qt::CTRL + Qt::Key_N);
}
//------------------------------------------------------------------------------
QAction* vActionWithText::erase_group(QObject* parent, const QString& text, const QString& description)
{
        return vAction::create(const_icon_32::erase_group(), text, description, parent , Qt::CTRL + Qt::SHIFT + Qt::Key_Delete);
}
//------------------------------------------------------------------------------
QAction* vActionWithText::erase_item(QObject* parent, const QString& text, const QString& description)
{
        return vAction::create(const_icon_32::erase_item(), text, description, parent , Qt::CTRL + Qt::Key_Delete);
}
