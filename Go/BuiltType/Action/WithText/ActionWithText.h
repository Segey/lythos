/* 
 * File:   ActionWithText.h
 * Author: S.Panin
 *
 * Created on Пт мая 7 2010, 23:49:51
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_WITH_TEXT_H_
#define	_V_ACTION_WITH_TEXT_H_
//------------------------------------------------------------------------------
#include "../Action.h"
//------------------------------------------------------------------------------
class vActionWithText : private vAction
{
    typedef                         vAction                                     inherited;
    typedef                         vActionWithText                             class_name;

public:
    static QAction*                 add_group(QObject* parent, const QString& tex, const QString& description);
    static QAction*                 add_item(QObject* parent, const QString& text, const QString& description);
    static QAction*                 erase_group(QObject* parent, const QString& tex, const QString& description);
    static QAction*                 erase_item(QObject* parent, const QString& text, const QString& description);
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_WITH_TEXT_H_ */
