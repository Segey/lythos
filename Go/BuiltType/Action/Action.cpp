/* 
 * File:   vAction.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 22:27
 */
//------------------------------------------------------------------------------
#include "Action.h"
//------------------------------------------------------------------------------
QAction* vAction::create(const QIcon& icon, const QString& text, const QString& tooltip, QObject* parent, const QKeySequence& key)
{
        QAction* action = new QAction(icon, text, parent);
        action->setShortcut(key);
        if( key == QKeySequence(QKeySequence::UnknownKey) ) action->setToolTip(tooltip);
                else action->setToolTip((tooltip + " (%1)").arg(key.toString()));
        return action;
}
//------------------------------------------------------------------------------
QAction* vAction::createAbout(QObject* parent)
{
        return create(const_icon_32::about(), vtr::Button::tr("&About..."), vtr::Button::tr("About"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createNew(QObject* parent)
{
        return create(const_icon_32::new_(), vtr::Button::tr("&New"), vtr::Button::tr("New"), parent, QKeySequence::New);
}
//------------------------------------------------------------------------------
QAction* vAction::createNewImage(QObject* parent)
{
        return create(const_icon_32::new_image(), vtr::Button::tr("&New a image"), vtr::Button::tr("New a image"), parent, QKeySequence::New);
}
//------------------------------------------------------------------------------
QAction* vAction::createNewDatabase(QObject* parent)
{
        return create(const_icon_32::new_database(), vtr::Button::tr("Create &New Database..."), vtr::Button::tr("Create New Database"), parent, QKeySequence::New);
}
//------------------------------------------------------------------------------
QAction* vAction::createOpen(QObject* parent)
{
        return create(const_icon_32::open(), vtr::Button::tr("&Open..."), vtr::Button::tr("Open"), parent, QKeySequence::Open);
}
//------------------------------------------------------------------------------
QAction* vAction::createOpenDatabase(QObject* parent)
{
        return create(const_icon_32::open_database(), vtr::Button::tr("&Open..."), vtr::Button::tr("Open Database"), parent, QKeySequence::Open);
}
//------------------------------------------------------------------------------
QAction* vAction::createSave(QObject* parent)
{
        return create(const_icon_32::save(), vtr::Button::tr("&Save"), vtr::Button::tr("Save"), parent, QKeySequence::Save);
}
//------------------------------------------------------------------------------
QAction* vAction::createSaveDatabase(QObject* parent)
{
        return create(const_icon_32::save_database(), vtr::Button::tr("&Save"), vtr::Button::tr("Save"), parent, QKeySequence::Save);
}
//------------------------------------------------------------------------------
QAction* vAction::createSaveAs(QObject* parent)
{
        return create(const_icon_32::save_as(), vtr::Button::tr("Save &As..."), vtr::Button::tr("Save As"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createSaveAsDatabase(QObject* parent)
{
        return create(const_icon_32::save_as_database(), vtr::Button::tr("Save &As..."), vtr::Button::tr("Save As"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createExit(QObject* parent)
{
        return create(const_icon_32::exit(), vtr::Button::tr("Clos&e"), vtr::Button::tr("Close"), parent, QKeySequence::Close);
}
//------------------------------------------------------------------------------
QAction* vAction::createCut(QObject* parent)
{
        return create(const_icon_32::cut(), vtr::Editor::tr("C&ut"), vtr::Editor::tr("Cut"), parent, QKeySequence::Cut);
}
//------------------------------------------------------------------------------
QAction* vAction::createCutWithTip(QObject* parent)
{
        QAction* action = createCut(parent);
        action->setStatusTip(vtr::Editor::tr("Cut the current selection's contents to the clipboard."));
        return action;
}
//------------------------------------------------------------------------------
QAction* vAction::createCopy(QObject* parent)
{
        return  create(const_icon_32::copy(), vtr::Editor::tr("&Copy"), vtr::Editor::tr("Copy"), parent, QKeySequence::Copy);
}
//------------------------------------------------------------------------------
QAction* vAction::createCopyWithTip(QObject* parent)
{
        QAction* action = createCopy(parent);
        action->setStatusTip(vtr::Editor::tr("Copy the current selection's contents to the clipboard."));
        return action;
}
//------------------------------------------------------------------------------
QAction* vAction::createPaste(QObject* parent)
{
        return create(const_icon_32::paste(), vtr::Editor::tr("&Paste"), vtr::Editor::tr("Paste"), parent, QKeySequence::Paste);
}
//------------------------------------------------------------------------------
QAction* vAction::createPasteWithTip(QObject* parent)
{
        QAction* action = createPaste(parent);
        action->setStatusTip(vtr::Editor::tr("Paste the clipboard's contents into the current selection."));
        return action;
}
//------------------------------------------------------------------------------
QAction* vAction::createClear(QObject* parent)
{
        return create(const_icon_32::clear(), vtr::Button::tr("C&lear"), vtr::Button::tr("Clear"), parent, QKeySequence::Delete);
}
//------------------------------------------------------------------------------
QAction* vAction::createClearImage(QObject* parent)
{
        return create(const_icon_32::clear(), vtr::Image::tr("&Delete a image"), vtr::Image::tr("Delete a image"), parent, QKeySequence::Delete);
}
//------------------------------------------------------------------------------
QAction* vAction::createClearAll(QObject* parent)
{
        return create(const_icon_32::clear_all(), vtr::Button::tr("Cl&ear All"), vtr::Button::tr("Clear All"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createClearText(QObject* parent)
{
        QAction* action = createClear(parent);
        action->setStatusTip(vtr::Button::tr("Completely clears the document contents"));
        return action;
}
//------------------------------------------------------------------------------
QAction* vAction::createPrint(QObject* parent)
{
        return create(const_icon_32::print(), vtr::Button::tr("&Print..."), vtr::Button::tr("Print"), parent, QKeySequence::Print);
}
//------------------------------------------------------------------------------
QAction* vAction::createPrintPreview(QObject* parent)
{
        return create(const_icon_32::print_preview(), vtr::Button::tr("Print Preview..."), vtr::Button::tr("Print Preview"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createUndo(QObject* parent)
{
        return create(const_icon_32::undo(), vtr::Button::tr("U&ndo"), vtr::Button::tr("Undo"), parent, QKeySequence::Undo);
}
//------------------------------------------------------------------------------
QAction* vAction::createRedo(QObject* parent)
{
        return create(const_icon_32::redo(), vtr::Button::tr("Re&do"), vtr::Button::tr("Redo"), parent, QKeySequence::Redo);
}
//------------------------------------------------------------------------------
QAction* vAction::createLeft(QObject* parent)
{
        return create(const_icon_32::left(), vtr::Button::tr("&Left"), vtr::Button::tr("Left"), parent, Qt::CTRL + Qt::Key_L);
}
//------------------------------------------------------------------------------
QAction* vAction::createCenter(QObject* parent)
{
        return create(const_icon_32::center(), vtr::Button::tr("Cen&ter"), vtr::Button::tr("Center"), parent, Qt::CTRL + Qt::Key_E);
}
//------------------------------------------------------------------------------
QAction* vAction::createRight(QObject* parent)
{
        return create(const_icon_32::right(), vtr::Button::tr("&Right"), vtr::Button::tr("Right"), parent, Qt::CTRL + Qt::Key_R);
}
//------------------------------------------------------------------------------
QAction* vAction::createJustify(QObject* parent)
{
        return create(const_icon_32::justify(), vtr::Button::tr("&Justify"), vtr::Button::tr("Justify"), parent, Qt::CTRL + Qt::Key_J);
}
//------------------------------------------------------------------------------
QAction* vAction::createBold(QObject* parent)
{
        return create(const_icon_32::bold(), vtr::Button::tr("&Bold"), vtr::Button::tr("Bold"), parent, Qt::CTRL + Qt::Key_B);
}
//------------------------------------------------------------------------------
QAction* vAction::createItalic(QObject* parent)
{
        return create(const_icon_32::italic(), vtr::Button::tr("&Italic"), vtr::Button::tr("Italic"), parent, Qt::CTRL + Qt::Key_I);
}
//------------------------------------------------------------------------------
QAction* vAction::createUnderline(QObject* parent)
{
        return create(const_icon_32::underline(), vtr::Button::tr("&Underline"), vtr::Button::tr("Underline"), parent, Qt::CTRL + Qt::Key_U);
}
//------------------------------------------------------------------------------
QAction* vAction::createColor(QObject* parent)
{
        return create(QIcon(), vtr::Color::tr("&Color..."), vtr::Color::tr("Color palette"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createHelp(QObject* parent)
{
        return create(const_icon_32::help(), vtr::Button::tr("&Help Contents..."), vtr::Button::tr("Help Contents"), parent, QKeySequence::HelpContents);
}
//------------------------------------------------------------------------------
QAction* vAction::createRegistration(QObject* parent)
{
        return create(QIcon(), vtr::Copyright::tr("&Registration..."), vtr::Copyright::tr("Registration"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createHomePage(QObject* parent)
{
        return create(QIcon(), vtr::Button::tr("Ho&me Page..."), vtr::Button::tr("Home Page"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createDefault(QObject* parent)
{
        return create(QIcon(), vtr::Button::tr("Default"), vtr::Button::tr("Default"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction::createRestoreDefaultSettings(QObject* parent)
{
        return create(QIcon(), vtr::Button::tr("Restore Default Settings"), vtr::Button::tr("Restore Default Settings"), parent, QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorRed(QObject* parent)
{
        return create(const_icon_32::color_red(), vtr::Color::tr("&Red"), vtr::Color::tr("Color Red"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorGreen(QObject* parent)
{
        return create(const_icon_32::color_green(), vtr::Color::tr("&Green"), vtr::Color::tr("Color Green"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorBlue(QObject* parent)
{
        return create(const_icon_32::color_blue(), vtr::Color::tr("&Blue"), vtr::Color::tr("Color Blue"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorCyan(QObject* parent)
{
        return create(const_icon_32::color_cyan(), vtr::Color::tr("&Cyan"), vtr::Color::tr("Color Cyan"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorMagenta(QObject* parent)
{
        return create(const_icon_32::color_magenta(), vtr::Color::tr("&Magenta"), vtr::Color::tr("Color Magenta"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorYellow(QObject* parent)
{
        return create(const_icon_32::color_yellow(), vtr::Color::tr("&Yellow"), vtr::Color::tr("Color Yellow"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorGray(QObject* parent)
{
        return create(const_icon_32::color_gray(), vtr::Color::tr("Gr&ay"), vtr::Color::tr("Color Gray"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createColorBlack(QObject* parent)
{
        return create(const_icon_32::color_black(), vtr::Color::tr("B&lack"), vtr::Color::tr("Color Black"), parent , QKeySequence::UnknownKey);
}
//------------------------------------------------------------------------------
QAction* vAction:: createRecordNew(QObject* parent)
{
        return create(const_icon_32::new_(), vtr::Record::tr("&New Record..."), vtr::Record::tr("Create New Record"), parent, QKeySequence::New);
}
//------------------------------------------------------------------------------
QAction* vAction:: createRecordOpen(QObject* parent)
{
        return create(const_icon_32::open(), vtr::Record::tr("&Open Record..."), vtr::Record::tr("Open Record"), parent, QKeySequence::Open);
}
//------------------------------------------------------------------------------
QAction* vAction:: createRecordDelete(QObject* parent)
{
        return create(const_icon_32::clear(), vtr::Record::tr("&Delete Record"), vtr::Record::tr("Delete Current Record"), parent, QKeySequence::Delete);
}
//------------------------------------------------------------------------------
QAction* vAction::rename(QObject* parent)
{
        return create(const_icon_32::edit(), vtr::Button::tr("Rename..."), vtr::Record::tr("Rename"), parent, Qt::CTRL + Qt::Key_M);
}
