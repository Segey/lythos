/* 
 * File:   vAction.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 22:27
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_H_
#define	_V_ACTION_H_
//------------------------------------------------------------------------------
#include <QAction>
#include "../../tr.h"
#include "../../Const/const_icon.h"
//------------------------------------------------------------------------------
class vAction 
{
    typedef                         vAction                                     class_name;

public:
    static QAction*                 create(const QIcon& icon, const QString& text, const QString& tooltip, QObject* parent, const QKeySequence& key);
    static QAction*                 createAbout(QObject* parent);
    static QAction*                 createNew(QObject* parent);
    static QAction*                 createNewImage(QObject* parent);
    static QAction*                 createNewDatabase(QObject* parent);
    static QAction*                 createOpen(QObject* parent);
    static QAction*                 createOpenDatabase(QObject* parent);
    static QAction*                 createSave(QObject* parent);
    static QAction*                 createSaveDatabase(QObject* parent);
    static QAction*                 createSaveAs(QObject* parent);
    static QAction*                 createSaveAsDatabase(QObject* parent);
    static QAction*                 createExit(QObject* parent);
    static QAction*                 createCut(QObject* parent);
    static QAction*                 createCutWithTip(QObject* parent);
    static QAction*                 createCopy(QObject* parent);
    static QAction*                 createCopyWithTip(QObject* parent);
    static QAction*                 createPaste(QObject* parent);
    static QAction*                 createPasteWithTip(QObject* parent);
    static QAction*                 createClear(QObject* parent);
    static QAction*                 createClearImage(QObject* parent);
    static QAction*                 createClearAll(QObject* parent);
    static QAction*                 createClearText(QObject* parent);
    static QAction*                 createPrint(QObject* parent);
    static QAction*                 createPrintPreview(QObject* parent);
    static QAction*                 createUndo(QObject* parent);
    static QAction*                 createRedo(QObject* parent);
    static QAction*                 createLeft(QObject* parent);
    static QAction*                 createCenter(QObject* parent);
    static QAction*                 createRight(QObject* parent);
    static QAction*                 createJustify(QObject* parent);
    static QAction*                 createBold(QObject* parent);
    static QAction*                 createItalic(QObject* parent);
    static QAction*                 createUnderline(QObject* parent);
    static QAction*                 createColor(QObject* parent);
    static QAction*                 createHelp(QObject* parent);
    static QAction*                 createRegistration(QObject* parent);
    static QAction*                 createHomePage(QObject* parent);
    static QAction*                 createDefault(QObject* parent);
    static QAction*                 createRestoreDefaultSettings(QObject* parent);      
    static QAction*                 createColorRed(QObject* parent);
    static QAction*                 createColorGreen(QObject* parent);
    static QAction*                 createColorBlue(QObject* parent);
    static QAction*                 createColorCyan(QObject* parent);
    static QAction*                 createColorMagenta(QObject* parent);
    static QAction*                 createColorYellow(QObject* parent);
    static QAction*                 createColorGray(QObject* parent);
    static QAction*                 createColorBlack(QObject* parent);
    static QAction*                 createRecordNew(QObject* parent);
    static QAction*                 createRecordOpen(QObject* parent);
    static QAction*                 createRecordDelete(QObject* parent);
    static QAction*                 rename(QObject* parent);
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_H_ */
