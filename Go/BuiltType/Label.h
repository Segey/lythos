/* 
 * File:   Label.h
 * Author: S.Panin
 *
 * Created on 16 Август 2009 г., 1:29
 */
//------------------------------------------------------------------------------
#ifndef _V_LABEL_H_
#define	_V_LABEL_H_
//------------------------------------------------------------------------------
struct vLabel
{
    static void                     set_word_wrap(QLabel* label)
    {
        label->setWordWrap(true);
        label->setAlignment(Qt::AlignJustify);
        label->setTextFormat(Qt::RichText);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_LABEL_H_ */
