/* 
 * File:   File.h
 * Author: S.Panin
 *
 * Created on Чт июня 3 2010, 23:40:26
 */
//------------------------------------------------------------------------------
#ifndef _V_FILE_H_
#define	_V_FILE_H_
//------------------------------------------------------------------------------
#include <QString>
#include "../Const/const_ext.h"
//------------------------------------------------------------------------------
class vFile
{
        static QChar               point()                                      { return QChar('.');}
public:
        static bool     ext_exists(const QString& fileName, const QString& ext) { return fileName.endsWith(ext, Qt::CaseInsensitive); }
        static QString  ext(const QString& file)
        {
            const QString result =QFileInfo(file).fileName();
            return result.mid(result.lastIndexOf( point() ) +1,-1).toUpper();
        }
        static bool is_name_containts_point(const QString& file)                { return file.lastIndexOf(point() ) !=file.indexOf(point() );  }
//        static bool     is_ext_image(const QString& ext)
//        {
//            return ext ==const_ext_image::bmp()    || ext ==const_ext_image::jpe() ||
//                   ext ==const_ext_image::jpeg()   || ext ==const_ext_image::jpg() ||
//                   ext ==const_ext_image::png();
//        }
//        static bool     is_ext_video(const QString& ext)
//        {
//            return ext ==const_ext_video::avi();
//        }
//        static bool     is_ext_audio(const QString& ext)
//        {
//            return ext ==const_ext_video::avi();
//        }
};
//------------------------------------------------------------------------------
#endif	/* _V_FILE_H_ */
