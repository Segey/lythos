/* 
 * File:   ActionGroup.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 15:50
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_GROUP_H_
#define	_V_ACTION_GROUP_H_
//------------------------------------------------------------------------------
namespace v_action_group
{

    template<typename T>
    QActionGroup*   make_group(const T& list, QWidget* parent, int index=-1)
    {
        QActionGroup* result = new QActionGroup(parent);
        std::for_each(list.begin(), list.end(), boost::bind( static_cast<QAction* ( QActionGroup::* )(QAction*)>(&QActionGroup::addAction), result, _1));
        std::for_each(list.begin(), list.end(), boost::bind( (&QAction::setCheckable),_1, true));
        if ( -1 != index ) list[index]->setCheckable(true);
        return result;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_GROUP_H_ */
