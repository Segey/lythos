/* 
 * File:   DLLsTest.h.h
 * Author: S.Panin
 *
 * Created on 11 Август 2009 г., 23:25
 */
//------------------------------------------------------------------------------
#ifndef _V_DDLs_TEST_H_
#define	_V_DDLs_TEST_H_
//------------------------------------------------------------------------------
struct vWINDLLsTest
{
    bool                            test();
    static bool                     St001ng1_test();
    static bool                     f12nt1_test();
    static bool                     In1256t1_test();
};
//------------------------------------------------------------------------------
#endif	/* _V_DDLs_TEST_H_ */
