/* 
 * File:   Test.h
 * Author: S.Panin
 *
 * Created on 11 Август 2009 г., 23:25
 */
//------------------------------------------------------------------------------
#ifndef _V_TEST_H_
#define	_V_TEST_H_
//------------------------------------------------------------------------------
class vTest
{
    typedef                         vTest                                       class_name;

    QTime                                                                       counter_;
    size_t                                                                      cx_;
    static const size_t 							size_ =120;
    
    QString                          cx()
    {
        ++cx_;
        if (cx_ < 10)   return QString("00%1").arg(cx_);
        if (cx_ < 100)  return QString("0%1").arg(cx_);
        return QString("%1").arg(cx_);
    }


    QString                          line() const                               { return QString(size_+4, '-');  }
    QString                          text_begin(const QString& name)
    {
        QString s;
        QTextStream out(&s);
        out.setFieldWidth(size_);
        out.setFieldAlignment(QTextStream::AlignLeft);
        out.setPadChar(' ');
        out << QString("Class tested: %1). %2").arg(cx()).arg(name);
        return s;
    }
   static  QString                  text_end()                                  { return "[ OK ]"; }

public:
    explicit                        vTest() : cx_(0) {}
    QString                         text(const QString& name)                   { return text_begin(name)  +  vTest::text_end(); }
    void                            start()                                     { counter_.start();   qDebug() << "Start Test:" << QDateTime().currentDateTime().toString("dd/mm/yy hh:mm:ss:zzz") <<'\n' <<line(); }
    void                            end() const                                 { qDebug() <<line() << '\n' <<"Time elapsed: " <<   counter_.elapsed() <<" ms";  }
};
//------------------------------------------------------------------------------
#endif	/* _V_TEST_H_ */
