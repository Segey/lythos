/* 
 * File:   DLLsTest.h.cpp
 * Author: S.Panin
 * 
 * Created on 11 Август 2009 г., 23:25
 */
//------------------------------------------------------------------------------
#include "DLLsTest.h"
#include "../../Go/Strategy/DLL/Dll.h"
//------------------------------------------------------------------------------
bool vWINDLLsTest::test()
{
        St001ng1_test();
        f12nt1_test();
        In1256t1_test();
        return true;
}
//------------------------------------------------------------------------------
bool vWINDLLsTest::St001ng1_test()
{
        QString str ="0000";
        vDLL<QString> dll ( const_libs::st001ng(), "s_truncate") ;
        
        str =dll(str, str,2);
        Q_ASSERT(str =="00...");
        str=dll(str, str,10);
        Q_ASSERT(str =="00...");
        str=dll(str, str,0);
        Q_ASSERT(str =="...");
        str.clear();
        str=dll(str, str,100);
        Q_ASSERT(str.isEmpty());
        return true;
}
//------------------------------------------------------------------------------
bool vWINDLLsTest::f12nt1_test()
{
        vDLL<int> lib ( const_libs::f12nt(), "width");
        Q_ASSERT( lib(int(), QFont(), QString("y")) );
        return true;
}
//------------------------------------------------------------------------------
bool vWINDLLsTest::In1256t1_test()
{
        vDLL<int> lib ( const_libs::in1256t(), "round_to_multiple_ten") ;

        int num = lib(1, 0);
        Q_ASSERT(num == 0);
        num = lib(9, 9);
        Q_ASSERT(num == 10);
        num = lib(10, 10);
        Q_ASSERT(num == 100);
        num = lib(100, 100);
        Q_ASSERT(num == 1000);
        num = lib(1000, 1000);
        Q_ASSERT(num == 10000);

        return true;
}