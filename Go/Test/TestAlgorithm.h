/* 
 * File:   TestAlgorithm.h
 * Author: S.Panin
 *
 * Created on 11 Август 2009 г., 23:25
 */
//------------------------------------------------------------------------------
#ifndef _V_TEST_ALGORITHM_H_
#define	_V_TEST_ALGORITHM_H_
//------------------------------------------------------------------------------
struct vTestAlgorithm
{

static bool load_text(QTextEdit* text_edit_, const QString& file_name)
{
        QFile file(file_name);
        if(!file.open(QFile::ReadOnly)) return false;

        QByteArray data = file.readAll();
        QTextCodec* codec = Qt::codecForHtml(data);
        QString str = codec->toUnicode(data);

        if(Qt::mightBeRichText(str)) text_edit_->setHtml(str);
        else
        {
                str = QString::fromLocal8Bit(data);
                text_edit_->setPlainText(str);
        }

        return true;
}
};
//------------------------------------------------------------------------------
#endif	/* _V_TEST_ALGORITHM_H_ */
