// -*- C++ -*-
/* 
 * File:   incBoost.h
 * Author: S.Panin
 *
 * Created on 28 Апрель 2010 г., 21:04
 */
//------------------------------------------------------------------------------
#ifndef _V_BOOST_H_
#define	_V_BOOST_H_
//------------------------------------------------------------------------------
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/minmax.hpp>
#include <boost/signals2/signal.hpp>
//------------------------------------------------------------------------------
#endif	/* _V_BOOST_H_ */

