/* 
 * File:   Instance.h
 * Author: S.Panin
 *
 * Created on 28 Июнь 2009 г., 2:55
 */
//------------------------------------------------------------------------------
#ifndef _V_INSTANCE_H_
#define	_V_INSTANCE_H_
//------------------------------------------------------------------------------
template<typename T>
class vInstance :private T
{
typedef                             T                                           ihnerited;
typedef                             vInstance<T>      				class_name;

    template<class L>void           instance_layout(L* base_layout, QLayout*(vInstance<T>:: *layout)() );

    void                            instance_form()                             {ihnerited::instance_form();}
    void                            instance_widgets()                          {ihnerited::instance_widgets();}
    QLayout*                        instance_top_layout()                       {return ihnerited::instance_top_layout();}
    QLayout*                        instance_middle_layout()                    {return ihnerited::instance_middle_layout();}
    QLayout*                        instance_bottom_layout()                    {return ihnerited::instance_bottom_layout();}
    void                            instance_images()                           {ihnerited::instance_images();}
    void                            translate()                                 {ihnerited::translate();}
public:
   template <typename U>            explicit vInstance(U* form) : ihnerited(form) {}
   void                             instance();
};
//------------------------------------------------------------------------------
template<typename T>
void vInstance<T>::instance()
{
    instance_form();
    instance_widgets();
    instance_images();
    translate();

    QVBoxLayout* base_layout = new QVBoxLayout(ihnerited::parent());
    instance_layout(base_layout, &vInstance<T>::instance_top_layout);
    instance_layout(base_layout, &vInstance<T>::instance_middle_layout);
    instance_layout(base_layout, &vInstance<T>::instance_bottom_layout); 
}
//------------------------------------------------------------------------------
template<typename T>
    template<typename L>
void vInstance<T>::instance_layout(L* base_layout, QLayout*(vInstance<T>::*layout)())
{
    QLayout* temp_layout = (this->*layout)();
    if(temp_layout) base_layout->addLayout(temp_layout);
}

//------------------------------------------------------------------------------
#endif	/* _V_INSTANCE_H_ */
