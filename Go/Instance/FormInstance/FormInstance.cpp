/* 
 * File:   FormInstance.cpp
 * Author: S.Panin
 * 
 * Created on 21 Июль 2009 г., 12:14
 */
//------------------------------------------------------------------------------
#include "FormInstance.h"
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
bool vTopInstance::test()
{
        Q_ASSERT(inherited::cheight() == 15);
        Q_ASSERT(inherited::clayout()->count() == 0);
        Q_ASSERT(!is_added_cap_bevel_);
        add_caption_bevel(new QLabel);
        Q_ASSERT(inherited::clayout()->count() == 1);
        Q_ASSERT(is_added_cap_bevel_);
        add_caption_label(new QLabel, new QLabel);
        Q_ASSERT(inherited::clayout()->count() == 2);
        add_stretch();
        Q_ASSERT(inherited::clayout()->count() == 3);
        add_widget(new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 4);
        Q_ASSERT(clayout()->count() ==4);
        Q_ASSERT(inherited::clayout()->count() == 4);
        Q_ASSERT(clayout_with_size_hint());
        Q_ASSERT(inherited::clayout()->count() == 4);
        inherited::create_frame();
        Q_ASSERT(inherited::clayout()->count() == 4);
        return true;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool vBottomInstance::test()
{
        Q_ASSERT(inherited::cheight() == 10);
        Q_ASSERT(inherited::clayout()->count() == 0);
        add_bevel();
        Q_ASSERT(inherited::clayout()->count() == 1);
        add_buttons(new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 2);
        add_buttons(new QPushButton, new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 3);
        add_buttons(new QPushButton, new QPushButton, new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 4);
        add_label_buttons(new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 5);
        add_label_buttons(new QPushButton, new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 6);
        add_label_buttons(new QPushButton, new QPushButton, new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 7);
        add_stretch();
        Q_ASSERT(inherited::clayout()->count() == 8);
        add_widget(new QPushButton);
        Q_ASSERT(inherited::clayout()->count() == 9);
        Q_ASSERT(clayout()->count() ==9);
        Q_ASSERT(inherited::clayout()->count() == 9);
        Q_ASSERT(clayout_with_size_hint());
        Q_ASSERT(inherited::clayout()->count() == 9);
        inherited::create_frame();
        Q_ASSERT(inherited::clayout()->count() == 9);
        return true;
}
//------------------------------------------------------------------------------