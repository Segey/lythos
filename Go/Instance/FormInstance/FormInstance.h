/* 
 * File:   FormInstance.h
 * Author: S.Panin
 *
 * Created on 24 Июнь 2009 г., 23:09
 */
//------------------------------------------------------------------------------
#ifndef _V_FORM_INSTANCE_H_
#define	_V_FORM_INSTANCE_H_
//------------------------------------------------------------------------------
class vBaseInstance
{
    typedef                         vBaseInstance                               class_name;

    QVBoxLayout*                                                                layout_;
    const int                                                                   height_;

protected:
    virtual QSize                   do_size_hint(QVBoxLayout* layout)           =0;
    int                             cheight()                                   {return height_;  }
    QFrame*                         create_frame()
    {
        QFrame* frame = new QFrame;
        frame->setFrameShape(QFrame::HLine);
        frame->setFrameShadow(QFrame::Sunken);
        return frame;
    }

public:
    explicit                        vBaseInstance(int height) : layout_(new QVBoxLayout) , height_(height) {}
    virtual                         ~vBaseInstance()                            { }
    QSpacerItem*                    add_stretch(int w = 20, int h = 40, QSizePolicy::Policy hData = QSizePolicy::Minimum, QSizePolicy::Policy vData = QSizePolicy::Minimum)
    {
         QSpacerItem* item = new QSpacerItem(w, h, hData, vData);
        layout_->addItem(item);
        return item;
    }
    void                            add_widget(QWidget* widget)                 {layout_->addWidget(widget);}
    QSize                           size_hint()                                 {return do_size_hint(layout_);}
    QVBoxLayout*                    clayout_with_size_hint()                    {size_hint(); return clayout(); }
    QVBoxLayout*                    clayout() const                             {return layout_;}
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
class vTopInstance : public vBaseInstance
{
    typedef                             vBaseInstance                           inherited;
    typedef                             vTopInstance                            class_name;

    bool                                                                        is_added_cap_bevel_;
    
    void                                instance_caption_label(QLabel* label)   {label->setAlignment(Qt::AlignAbsolute | Qt::AlignRight);  instance_font(label, false); }
    void                                instance_font(QLabel* label, bool do_set_size)  { QFont font; if(do_set_size) font.setPointSize(12); font.setBold(true);  label->setFont(font); }

protected:
    virtual QSize                       do_size_hint(QVBoxLayout* layout)
    {
        QSize temp = layout->sizeHint();
        if(temp.height() < inherited::cheight())
        {
                is_added_cap_bevel_ ? layout->insertSpacing(0, inherited::cheight() - temp.height()) : layout->addSpacing(inherited::cheight() - temp.height());
                temp.setHeight(inherited::cheight());
        }
        return temp;
    }

public:
                                        vTopInstance() : inherited(15), is_added_cap_bevel_(false)          {}
    template<typename U> void           add_caption_label(U* icon, QLabel* label)
    {
        instance_font(label, true);
        QFormLayout* layout = new QFormLayout;
        layout->setWidget(0, QFormLayout::LabelRole, icon);
        layout->setWidget(0, QFormLayout::FieldRole, label);
        inherited::clayout()->addLayout(layout);
    }
    void                                add_caption_bevel(QLabel* caption)
    {
        QVBoxLayout* layout = new QVBoxLayout;

        QFrame* frame = inherited::create_frame();
        instance_caption_label(caption);

        layout->addWidget(caption);
        layout->addWidget(frame);
        inherited::clayout()->addLayout(layout);

        is_added_cap_bevel_ = true;
    }
    bool test();
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
class vBottomInstance : public vBaseInstance
{
    typedef                         vBaseInstance                               inherited;
    typedef                         vBottomInstance                             class_name;

protected:
    virtual QSize                   do_size_hint(QVBoxLayout* layout)
    {
        QSize temp = layout->sizeHint();
        if(temp.height() < inherited::cheight())
        {
            layout->insertSpacing(0, inherited::cheight() - temp.height());
            temp.setHeight(inherited::cheight());
        }
        return temp;
    }
    
public:
                                    vBottomInstance() : inherited(10)           {}

    void add_buttons(QPushButton* button1, QPushButton* button2 = 0, QPushButton* button3 = 0)
    {
        QHBoxLayout* layout = new QHBoxLayout;
        layout->addStretch();
        if(button1) layout->addWidget(button1);
        if(button2) layout->addWidget(button2);
        if(button3) layout->addWidget(button3);
        inherited::clayout()->addLayout(layout);
    }
    template <typename U> void      add_label_buttons(U* label, QPushButton* button1 = 0, QPushButton* button2 = 0, QPushButton* button3 = 0)
    {
        QHBoxLayout* layout = new QHBoxLayout;
        layout->addWidget(label);
        layout->addStretch();
        if(button1) layout->addWidget(button1);
        if(button2) layout->addWidget(button2);
        if(button3) layout->addWidget(button3);
        inherited::clayout()->addLayout(layout);
    }
    inline void                            add_bevel()                          {inherited::clayout()->addWidget(inherited::create_frame());}
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_FORM_INSTANCE_H_ */
