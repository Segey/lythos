/* 
 * File:   Instance.h
 * Author: S.Panin
 *
 * Created on 28 Июнь 2009 г., 2:55
 */
//------------------------------------------------------------------------------
#ifndef _V_INSTANCE_2_H_
#define	_V_INSTANCE_2_H_
//------------------------------------------------------------------------------
#include "../Const/const_namespace.h"
//------------------------------------------------------------------------------
GO_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
class Instance
{
    typedef                 Instance                                            class_name;

    template<typename L, typename F>   static void instance_layout(L* base_layout, F f)
    {
        QLayout* temp_layout = f();
        if(temp_layout) base_layout->addLayout(temp_layout);
    }

public:
    template< typename S> static void item(S* strategy)
    {       
        strategy->instance_widgets();
        strategy->instance_images();
        strategy->translate();
    }

    template< typename W, typename S> static void form(W* widget, S* strategy)
    {
        item(strategy);

        QVBoxLayout* base_layout = new QVBoxLayout(widget);
        instance_layout(base_layout, boost::bind(&S::instance_top_layout, strategy));
        instance_layout(base_layout, boost::bind(&S::instance_middle_layout, strategy));
        instance_layout(base_layout, boost::bind(&S::instance_bottom_layout, strategy));
    }

    template< typename W, typename S> static void main_window(W* widget, S* strategy)
    {
        strategy->instance_form(widget);
        strategy->instance_widgets(widget);
        strategy->instance_docs(widget);
        strategy->instance_images(widget);
        strategy->translate();

//        QVBoxLayout* base_layout = new QVBoxLayout(widget);
//        instance_layout(base_layout, boost::bind(&S::instance_top_layout, strategy));
//        instance_layout(base_layout, boost::bind(&S::instance_middle_layout, strategy));
//        instance_layout(base_layout, boost::bind(&S::instance_bottom_layout, strategy));
    }
};
//------------------------------------------------------------------------------
GO_END_NAMESPACE
//------------------------------------------------------------------------------
#endif	/* _V_INSTANCE_2_H_ */
