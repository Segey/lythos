/* 
 * File:   Instance.h
 * Author: S.Panin
 *
 * Created on 28 Июнь 2009 г., 2:55
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_INSTANCE_H_
#define	_V_WIDGET_INSTANCE_H_
//------------------------------------------------------------------------------
template<typename T>
class vWidgetInstance :private T
{
typedef                                 T                                       ihnerited;
typedef                                 vWidgetInstance<T>      		class_name;

        void                            instance_widgets()                      {ihnerited::instance_widgets();}
        void                            instance_layout(QVBoxLayout* layout)    {ihnerited::instance_layout(layout);}
        void                            translate()                             {ihnerited::translate();}
        
public:
        template <typename U>            explicit vWidgetInstance(U* form) : ihnerited(form) {}
        void                             instance();
};
//------------------------------------------------------------------------------
template<typename T>
void vWidgetInstance<T>::instance()
{
    instance_widgets();
    instance_layout(new QVBoxLayout(ihnerited::parent()));
    translate();
}
//------------------------------------------------------------------------------
#endif	/* _V_WIDGET_INSTANCE_H_ */
