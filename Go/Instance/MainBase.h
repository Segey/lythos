// -*- C++ -*-
/* 
 * File:   MainBase
 * Author: S.Panin
 *
 * Created on 14 Май 2010 г., 22:28
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_BASE_V_
#define	_V_MAIN_BASE_V_
//------------------------------------------------------------------------------
#include "../Singleton/User/SingletonUser.h"
#include "../Singleton/BD/Base/SingletonBaseBD.h"
#include "../Singleton/BD/Settings/SingletonSettingsBD.h"
//#include "../Singleton/BD/EG/SingletonEGBD.h"
//------------------------------------------------------------------------------
template <typename T>
class vMainBase : public T
{
    typedef                             T                                       inherited;
    typedef                             vMainBase<T>                            class_name;

public:
    void                                execute()                               { inherited::execute(); }
    QString                             name()                                  { return inherited::name(); }
    double                              version()                               { return inherited::version(); }
    static vSingletonBaseBD&            basebd()                                { static vSingletonBaseBD basebd;      return basebd; }
    static vSingletonSettingsBD&        settings()                              { static vSingletonSettingsBD bd;      return bd; }
    static vSingletonUser&              user()                                  { static vSingletonUser user;          return user; }
    static bool                         test()                                  { return inherited::test(); }
    static bool                         try_load_dlls()                         { return inherited::try_load_dlls(); }
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_BASE_V_ */

