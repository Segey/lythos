/* 
 * File:   Instance.h
 * Author: S.Panin
 *
 * Created on 28 Июнь 2009 г., 2:55
 */
//------------------------------------------------------------------------------
#ifndef _MAIN_WINDOW_INSTANCE_H_
#define	_MAIN_WINDOW_INSTANCE_H_
//------------------------------------------------------------------------------
/** \brief Depregated */
template<typename T>
class vMainWindowInstance :private T
{
    typedef                         T                                           ihnerited;
    typedef                         vMainWindowInstance<T>      		class_name;

    void                            instance_form()                             {ihnerited::instance_form();}
    void                            instance_widgets()                          {ihnerited::instance_widgets();}
    void                            translate()                                 {ihnerited::translate();}

public:
   template <typename U>            explicit vMainWindowInstance(U* form) : ihnerited(form) {}
   void                             instance();
};
//------------------------------------------------------------------------------
template<typename T>
void vMainWindowInstance<T>::instance()
{
    instance_form();
    instance_widgets();
    translate();
}
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::instance  */
    namespace instance {
//------------------------------------------------------------------------------
/** 
 \brief Инстанцирование MainWindow
 \note Шаблонный параметр в обязательном порядке, должен содержать следующие функции-член.
 <pre>
  - instance_form();
  - instance_widgets();
  - translate();
 </pre>

 Пример инстанцирования.
\code
 instance::MainWindow< main::Instance > (this).instance();
\endcode
 
 */
template<typename T>
class MainWindow :private T
{
    typedef                         T                                           ihnerited;
    typedef                         MainWindow<T>                               class_name;

    void                            instance_form()                             {ihnerited::instance_form();}
    void                            instance_widgets()                          {ihnerited::instance_widgets();}
    void                            translate()                                 {ihnerited::translate();}

public:
   /** 
    \brief Конструктор.
    \param form - Шаблонный указатель на MainWindow.
   */
    template <typename U>            explicit MainWindow(U* form) : ihnerited(form) {}
    /** \brief Непосредственное инстанцирование формы MainWindow.  */
    void                             instance();
};
//------------------------------------------------------------------------------
template<typename T>
void MainWindow<T>::instance()
{
    instance_form();
    instance_widgets();
    translate();
}
//------------------------------------------------------------------------------
    }; // end namespace instance
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _MAIN_WINDOW_INSTANCE_H_ */
