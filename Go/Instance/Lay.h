// -*- C++ -*-
/* 
 * File:   vLay.
 * Author: S.Panin
 *
 * Created on 16 Август 2009 г., 1:39
 */
//------------------------------------------------------------------------------
#ifndef _V_VLAY_
#define	_V_VLAY_
//------------------------------------------------------------------------------
class vLay
{
    typedef                         vLay                                        class_name;

    QGridLayout*                                                                layout_;
    int                                                                         count_;

//------------------------------------------------------------------------------
template<typename T> void                                   instance_font(T* label)
    {
        QFont font;
        font.setBold(true);
        label->setFont(font);
    }
//------------------------------------------------------------------------------
template<typename T> void                                   instance_bevel(T* bevel)
    {
        bevel->setFrameShape(QFrame::HLine);
        bevel->setFrameShadow(QFrame::Sunken);
    }
//------------------------------------------------------------------------------
public:
     explicit vLay(QGridLayout* layout, int count =2) : layout_(layout), count_(count)           {}
     explicit vLay( int count =2) : layout_( new QGridLayout), count_(count)    {}
     QGridLayout*                                           layout()            {return layout_;}
     QGridLayout*                                           clayout() const     {return layout_;}

//------------------------------------------------------------------------------
void                                                        add_stretch(bool is_stretch =true)
    {
        if (is_stretch) layout_->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), layout_->count(), 0, 1, count_);
    }
//------------------------------------------------------------------------------
template<typename T>void                                    add_caption_bevel_first(T* label)
    {
        instance_font(label);

        layout_->addWidget(label, layout_->count(), count_ -1, 1, 1, Qt::AlignAbsolute | Qt::AlignRight);
        add_bevel(false);
    }
//------------------------------------------------------------------------------
template<typename T>void                                    add_caption(T* label,  bool is_stretch =true)
    {
        add_stretch(true);
        add_caption_bevel_first(label);
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T, typename U, typename Y>void            add_caption_with_image(T* label, U* image, Y* bevel, bool is_stretch =true)
    {
        add_stretch(true);
        instance_font(label);
        instance_bevel(bevel);

        QHBoxLayout* layout = new QHBoxLayout;
        layout->addWidget(label);
        layout->addWidget(image);

        layout_->addLayout(layout, layout_->count(), count_ -1, 1, 1, Qt::AlignAbsolute | Qt::AlignRight);
        layout_->addWidget(bevel,  layout_->count(), 0, 1, count_);
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
void                                                        add_bevel(bool is_stretch =true)
    {
        QFrame* bevel =new QFrame;
        instance_bevel(bevel);
        layout_->addWidget(bevel, layout_->count(), 0, 1, count_);
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T>void                                    add_layout(T* layout, bool is_stretch =true)
    {
        layout_->addLayout(layout, layout_->count(), 0, 1, count_);
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T>void                                    add_unary(T* widget, bool is_stretch =true)
    {
        layout_->addWidget(widget, layout_->count(), 0, 1, count_);
        add_stretch(is_stretch);
    }

//------------------------------------------------------------------------------
template<typename T, typename U>void                        add_unary_label(T* label, U * edit, bool is_stretch =true)
    {
        layout_->addWidget(label, layout_->count(), 0);
        layout_->addWidget(edit, layout_->count() -1, 1, 1, count_ -1);
        add_stretch(is_stretch);
    }

//------------------------------------------------------------------------------
template<typename T1, typename T2>void                      add_binary(T1* edit1, T2* edit2, bool is_stretch =true)
    {
        layout_->addWidget(edit1, layout_->count(), 0, 1, count_/2 );
        layout_->addWidget(edit2, layout_->count() -1, count_/2, 1, count_/2 );
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T1, typename T2>void                      add_binary_1_layout(T1* layout, T2* edit, bool is_stretch =true)
    {
        layout_->addLayout(layout,  layout_->count(), 0, 1, count_/2 );
        layout_->addWidget(edit, layout_->count() -1, count_/2, 1, count_/2 );
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T1, typename T2>void                      add_binary_2_layout(T1* edit, T2* layout, bool is_stretch =true)
    {
        layout_->addWidget(edit,  layout_->count(), 0, 1, count_/2 );
        layout_->addLayout(layout, layout_->count() -1, count_/2, 1, count_/2 );
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T1, typename T2>void                      add_binary_layouts(T1* edit, T2* layout, bool is_stretch =true)
    {
        layout_->addLayout(edit,  layout_->count(), 0, 1, count_/2 );
        layout_->addLayout(layout, layout_->count() -1, count_/2, 1, count_/2 );
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
template<typename T1, typename U1, typename T2, typename U2>void add_binary_label(T1* label1, U1* edit1, T2* label2, U2 * edit2, bool is_stretch =true)
    {
        layout_->addWidget(label1,  layout_->count(), 0, 1, count_/4 );
        layout_->addWidget(edit1,   layout_->count() -1, count_/4, 1, count_/4 );
        layout_->addWidget(label2,  layout_->count() -2, count_/2, 1, count_/4, Qt::AlignAbsolute | Qt::AlignCenter );
        layout_->addWidget(edit2,   layout_->count() -3, count_*3/4, 1, count_/4 );
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
void     add_empty( int height, bool is_stretch =true)
    {
        QWidget *widget = new QWidget;
        widget->setFixedHeight(height);
        layout_->addWidget(widget, layout_->count(), 0, 1, count_);
        add_stretch(is_stretch);
    }
//------------------------------------------------------------------------------
bool  test()
    {
        QDialog dialog;

        QLabel label;
        Q_ASSERT(!label.font().bold());
        instance_font(&label);
        Q_ASSERT(label.font().bold());
        Q_ASSERT(!clayout()->count());
        add_bevel();
        Q_ASSERT(clayout()->count() ==2);
        add_binary(new QLabel("111"), new QLabel("111"));
        Q_ASSERT(clayout()->count() ==5);
        add_binary_2_layout(new QLabel, new QGridLayout);
        Q_ASSERT(clayout()->count() ==8);

        QLabel label2;
        add_caption(&label2);
        Q_ASSERT(clayout()->count() ==12);
        Q_ASSERT(label2.font().bold());

        QLabel label3;
        add_caption_bevel_first(&label3);
        Q_ASSERT(clayout()->count() ==14);
        Q_ASSERT(label3.font().bold());

        add_unary(new QLabel);
        Q_ASSERT(clayout()->count() ==16);

        add_unary_label(new QLabel, new QLabel);
        Q_ASSERT(clayout()->count() ==19);

        add_binary_label(new QLabel, new QComboBox, new QLabel, new QComboBox, true);
        Q_ASSERT(clayout()->count() ==24);

        add_binary_1_layout(new QHBoxLayout, new QComboBox);

        dialog.setLayout(clayout());
        return true;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_VLAY_ */

