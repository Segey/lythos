// -*- C++ -*-
/* 
 * File:   DllInstance.h
 * Author: S.Panin
 *
 * Created on 13 Январь 2010 г., 14:50
 */
//------------------------------------------------------------------------------
#ifndef _V_DLL_INSTANCE_H_
#define	_V_DLL_INSTANCE_H_
//------------------------------------------------------------------------------
#include <algorithm>
#include <boost/bind.hpp>
#include "../Strategy/DLL/Dll.h"
//------------------------------------------------------------------------------
class vDllInstance
{
typedef                             vDllInstance                                class_name;

public:

 template <typename T, typename U>  static  void instance(T* form, U* data)
    {
        std::vector<QLayout*> result;
        result = ::vDLL<std::vector<QLayout*> >(U::name_dll(), "instance")(result, data);

        QVBoxLayout* layout = new QVBoxLayout(form);
        std::for_each(result.begin(), result.end(), boost::bind(&QVBoxLayout::addLayout, layout, _1, 0));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_DLL_INSTANCE_H_ */

