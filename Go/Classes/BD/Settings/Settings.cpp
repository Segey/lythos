/* 
 * File:   Settings.cpp
 * Author: S.Panin
 * 
 * Created on 23 Март 2010 г., 10:11
 */
//------------------------------------------------------------------------------
#include "Settings.h"
//------------------------------------------------------------------------------
bool vSettings::test()
{
        vMain::user().set_test_id();

        QStringList list_set = QStringList() << "1" <<"3" <<"0";
        Q_ASSERT(vSettings::set("MeasurementHistory", settings::make_stringlist("HeaderList", list_set)));

        Q_ASSERT( vSettings::get("MeasurementHistory", settings::make_stringlist("HeaderList")).get<0>() );
        QStringList list_get = vSettings::get("MeasurementHistory", settings::make_stringlist("HeaderList")).get<1>();
        Q_ASSERT(list_set == list_get);

        QSqlQuery query(vMain::settingsbd().database());
        return query.exec("DELETE FROM MeasurementHistory WHERE UserID =0");
}
//------------------------------------------------------------------------------