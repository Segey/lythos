// -*- C++ -*-
/* 
 * File:   SettingsStrategy.h
 * Author: S.Panin
 *
 * Created on 30 Март 2010 г., 7:55
 */
//------------------------------------------------------------------------------
#ifndef _V_SETTINGS_STRATEGY_H_
#define	_V_SETTINGS_STRATEGY_H_
//------------------------------------------------------------------------------
template <typename T>
class vSettingsStrategyNo
{
    typedef                     T                                               value_type;
    typedef                     vSettingsStrategyNo<value_type>                 class_type;

protected:
    QVariant                    execute_value( const value_type& value) const   { return QVariant::fromValue(value); }
    value_type                  execute_result( const QVariant& value, const value_type& ) const    { return value.value<value_type>() ; }
};
//------------------------------------------------------------------------------
template <typename T>
class vSettingsStrategyStringList
{
    QString                                                                     divider_;

    typedef                      T                                              value_type;
    typedef                      vSettingsStrategyStringList<value_type>        class_type;

protected:
    QVariant                     execute_value( const value_type& value) const  { return QVariant::fromValue(value.join(divider_)); }
    value_type                   execute_result( const QVariant& value, const value_type& value_default) const   { return value.toString().isEmpty() ? value_default : value.toString().split(divider_) ; }

public:
    explicit                     vSettingsStrategyStringList() : divider_(" ")  { }
    void                         set_divider( const QString& divider)           { divider_ =divider; }
};
//------------------------------------------------------------------------------
template <typename T>
class vSettingsStrategyMap
{
    QString                                                                     divider_field_;
    QString                                                                     divider_line_;

    typedef                      T                                              value_type;
    typedef                      vSettingsStrategyMap<value_type>               class_type;

protected:
    QVariant                     execute_value( const value_type& value) const
    {
        QString str;
        QTextStream in(&str);
        for (typename value_type::const_iterator it=value.begin(); it !=value.end(); ++it)
                in << it.key() << divider_field_ << it.value() << divider_line_;
        if ( false ==str.isEmpty() ) str.chop(divider_line_.size());
        return QVariant::fromValue(str);
    }

    value_type                   execute_result( const QVariant& value, const value_type& value_default) const
    {
        if ( value.toString().isEmpty() ) return value_default;
        value_type map;
        QStringList lst =value.toString().split(divider_line_);
        QStringList lst_temp;
        for ( QStringList::const_iterator it=lst.begin(); it !=lst.end(); ++it)
        {
                lst_temp =it->split(divider_field_);
                map.insert(lst_temp.first(), lst_temp.last());
        }

        return  map;
    }

public:
    explicit                     vSettingsStrategyMap() : divider_field_("^"), divider_line_("#")  { }
    void                         set_divider_field( const QString& divider)     { divider_field_ =divider; }
    void                         set_divider_line( const QString& divider)      { divider_line_ =divider; }
};
//------------------------------------------------------------------------------
template <typename T>
class vSettingsStrategyFormSize
{
    typedef                      T                                              value_type;
    typedef                      vSettingsStrategyFormSize<value_type>          class_type;

protected:
    QVariant                     execute_value( const value_type& value) const
    {
        QString temp = QString("%1 %2 %3 %4").arg(value.left()).arg(value.top()).arg(value.width()).arg(value.height());
        return QVariant::fromValue(temp);
    }
    value_type                   execute_result( const QVariant& value, const value_type& value_default) const
    {
     QStringList lst =value.toString().split(' ');
     if ( lst.size() <4 ) return value_default;

     return QRect(lst[0].toInt(), lst[1].toInt(), lst[2].toInt(), lst[3].toInt());
    }

};
//------------------------------------------------------------------------------
#endif	/* _V_SETTINGSSTRATEGY_H_ */

