// -*- C++ -*-
/* 
 * File:   SettingsVariant.h
 * Author: S.Panin
 *
 * Created on 27 Март 2010 г., 22:57
 */
//------------------------------------------------------------------------------
#ifndef _V_SETTINGS_VARIANT_H_
#define	_V_SETTINGS_VARIANT_H_
//------------------------------------------------------------------------------
template <typename T, template <typename> class S >
class vSettingsType : public S<T>
{
public:
    typedef                     QString                                         name_type;
    typedef                     T                                               value_type;
    typedef                     S<value_type>                                   inherited;
    typedef                     vSettingsType<value_type, S >                   class_type;

private:
    name_type                                                                   name_;    
    value_type                                                                  value_;

public:
    explicit                    vSettingsType(const name_type& name, const value_type value) : name_(name), value_(value) {}
    const name_type &           name() const                                    { return name_ ;}
    value_type                  default_value() const                           { return value_ ; }
    QVariant                    value() const                                   { return inherited::execute_value( value_ ); }
    value_type                  result(const QVariant& var ) const              { return inherited::execute_result( var, value_ ); }
};
//------------------------------------------------------------------------------
namespace settings { // BEGIN NAMESPACE SETTINGS
//------------------------------------------------------------------------------
template<typename T, template <typename> class S = vSettingsStrategyNo >
inline vSettingsType<T, S >   make(const typename vSettingsType<T, S>::name_type & name, const T& value )
{
    return vSettingsType<T, S >(name, value);
}
//------------------------------------------------------------------------------
template<template <typename> class S = vSettingsStrategyStringList >
inline vSettingsType<QStringList, S >   make_stringlist(const typename vSettingsType<QStringList, S>::name_type & name, const QStringList& value = QStringList() )
{
    return vSettingsType<QStringList, S >(name, value);
}
//------------------------------------------------------------------------------
template<template <typename> class S = vSettingsStrategyMap >
inline vSettingsType< QMap<QString,QString>, S >   make_map(const typename vSettingsType<QMap<QString,QString>, S>::name_type & name, const QMap<QString,QString>& value = QMap<QString,QString>() )
{
    return vSettingsType<QMap<QString,QString>, S >(name, value);
}
//------------------------------------------------------------------------------
template<template <typename> class S = vSettingsStrategyFormSize >
inline vSettingsType< QRect, S >   make_form_size(const typename vSettingsType<QRect, S>::name_type & name, const QRect& value = QRect() )
{
    return vSettingsType<QRect, S >(name, value);
}

}   // END NAMESPACE SETTINGS
//------------------------------------------------------------------------------
#endif	/* _V_SETTINGS_VARIANT_H_ */

