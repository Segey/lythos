/*
 * File:   Settings.h
 * Author: S.Panin
 *
 * Created on 23 Март 2010 г., 10:11
 */
//------------------------------------------------------------------------------
#ifndef _V_SETTINGS_H_
#define	_V_SETTINGS_H_
//------------------------------------------------------------------------------
#include "Variant/Strategy/SettingsStrategy.h"
#include "Variant/SettingsVariant.h"
//------------------------------------------------------------------------------
class vSettings
{
    typedef                         vSettings                                   class_name;

template<typename T1, template <typename> class S1, typename T2, template <typename> class S2 >
    static inline bool  update_insert (bool is_update, const typename vSettingsType<T1,S1>::name_type& name, const vSettingsType<T1,S1>& val1, const vSettingsType<T2,S2>& val2)
    {
        QSqlQuery query(vMain::settingsbd().database());
        is_update ? query.prepare(QString("UPDATE %1 SET %2 =?, %3 =? WHERE UserID =?;").arg(name).arg(val1.name()).arg(val2.name())) : query.prepare(QString("INSERT INTO %1 ( %2 , %3, UserID) VALUES (?, ?, ?);").arg(name).arg(val1.name()).arg(val2.name()));
        query.addBindValue(val1.value());
        query.addBindValue(val2.value());
        query.addBindValue(vMain::user().id());
        return query.exec();
    }

template<typename T, template <typename> class S >
    static inline bool  update_insert (bool is_update, const typename vSettingsType<T,S>::name_type& name, const vSettingsType<T,S>& val)
    {
        QSqlQuery query(vMain::settingsbd().database());
        is_update ? query.prepare(QString("UPDATE %1 SET %2 =? WHERE UserID =?;").arg(name).arg(val.name())) : query.prepare(QString("INSERT INTO %1 ( %2 , UserID) VALUES (?, ?);").arg(name).arg(val.name()));
        query.addBindValue(val.value());
        query.addBindValue(vMain::user().id());
        return query.exec();
    }

public:
template<typename T, template <typename> class S >
static inline bool insert (const typename vSettingsType<T,S>::name_type& name, const vSettingsType<T,S>& val)
    {
        return update_insert(false, name, val);
    }

template<typename T1, template <typename> class S1, typename T2, template <typename> class S2 >
static inline bool insert (const typename vSettingsType<T1,S1>::name_type& name, const vSettingsType<T1,S1>& val1, const vSettingsType<T2,S2>& val2)
    {
        return update_insert(false, name, val1, val2);
    }

template<typename T, template <typename> class S >
static inline bool update (const typename vSettingsType<T,S>::name_type& name, const vSettingsType<T,S>& val)
    {
        return update_insert(true, name, val);
    }

template<typename T1, template <typename> class S1, typename T2, template <typename> class S2 >
static inline bool update (const typename vSettingsType<T1,S1>::name_type& name, const vSettingsType<T1,S1>& val1, const vSettingsType<T2,S2>& val2)
    {
        return update_insert(true, name, val1, val2);
    }

template<typename T, template <typename> class S >
static inline bool  set (const typename vSettingsType<T,S>::name_type& name, const vSettingsType<T,S>& val)
    {
        return update_insert(get(name, val).get<0>(), name, val);
    }

template<typename T1, template <typename> class S1, typename T2, template <typename> class S2 >
static inline bool set (const typename vSettingsType<T1,S1>::name_type& name, const vSettingsType<T1,S1>& val1, const vSettingsType<T2,S2>& val2)
    {
        return update_insert(get(name, val1, val2).get<0>(), name, val1, val2);
    }


template<typename T, template <typename> class S > 
    static inline boost::tuple<bool, T> get(const typename vSettingsType<T,S>::name_type& name, const vSettingsType<T,S>& val)
    {
        QSqlQuery query(vMain::settingsbd().database());
        query.prepare(QString("SELECT %2 FROM %1 WHERE userID = ?;").arg(name).arg(val.name()));
        query.addBindValue(vMain::user().id());
        return query.exec() && query.next() ? boost::make_tuple(true, val.result(query.value(0))) : boost::make_tuple(false, val.default_value());
    }


template<typename T1, template <typename> class S1, typename T2, template <typename> class S2 >
    static inline boost::tuple<bool, T1, T2> get (const typename vSettingsType<T1,S1>::name_type& name, const vSettingsType<T1,S1>& val1, const vSettingsType<T2,S2>& val2)
    {
        QSqlQuery query(vMain::settingsbd().database());
        query.prepare(QString("SELECT %2, %3 FROM %1 WHERE userID = ?;").arg(name).arg(val1.name()).arg(val2.name()));
        query.addBindValue(vMain::user().id());
        return query.exec() && query.next() ? boost::make_tuple(true, val1.result( query.value(0)), val2.result(query.value(1))) : boost::make_tuple(false, val1.default_value(), val2.default_value());
    }

    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SETTINGS_H_ */
