// -*- C++ -*-
/* 
 * File:   Html.h
 * Author: S.Panin
 *
 * Created on Вт июня 15 2010, 10:45:47
 */
//------------------------------------------------------------------------------
#ifndef _V_HTML_HREF_H_
#define	_V_HTML_HREF_H_
//------------------------------------------------------------------------------
namespace go
{ 
//------------------------------------------------------------------------------
    namespace html
    { 
//------------------------------------------------------------------------------
class Href
{
    typedef             Href                                                    class_name;

    QString&                                                                    text_;

public:
    explicit            Href(QString& text) : text_(text)                       {}
    void                local(const QString& adress, const QString& text)       { text_.append(QString("<a href=\"#%1\">%2</a>").arg(adress).arg(text)); }
    void                global(const QString& adress, const QString& text)      { text_.append(QString("<a href=\"%1\">%2</a>").arg(adress).arg(text)); }

    bool test()
    {
        text_.clear();
        global(QString(), QString());
        Q_ASSERT( QString("<a href=\"\"></a>")           == text_);

        text_.clear();
        global(QString("Hello"), QString("Cool"));
        Q_ASSERT( QString("<a href=\"Hello\">Cool</a>")  == text_);
        
        text_.clear();
        local(QString(), QString());
        Q_ASSERT( QString("<a href=\"#\"></a>")          == text_);

        text_.clear();
        local(QString("Hello"), QString("Cool"));
        Q_ASSERT( QString("<a href=\"#Hello\">Cool</a>") == text_);
        return true;
    }

};
    //--------------------------------------------------------------------------
    }; 
//------------------------------------------------------------------------------
}; 
//------------------------------------------------------------------------------
#endif	/* _V_HTML_HREF_H_ */

