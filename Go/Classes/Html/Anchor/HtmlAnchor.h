// -*- C++ -*-
/* 
 * File:   HtmlAnchor.h
 * Author: S.Panin
 *
 * Created on Вт июня 15 2010, 10:45:47
 */
//------------------------------------------------------------------------------
#ifndef _V_HTML_ANCHOR_H_
#define	_V_HTML_ANCHOR_H_
//------------------------------------------------------------------------------
namespace go
{
//------------------------------------------------------------------------------
    namespace html
    {
//------------------------------------------------------------------------------
class Anchor
{
    typedef             Anchor                                                  class_name;

    QString&                                                                    text_;

public:
    explicit            Anchor(QString& text) : text_(text)                       {}
    void                caption(const QString& anchor_name, const QString& text)     { text_.append(QString("<span style=\"font-weight: bold;\"><a name=\"%1\"></a>%2</span>").arg(anchor_name).arg(text)); }

    bool test()
    {
        text_.clear();
        caption(QString(), QString());
        Q_ASSERT( QString("<span style=\"font-weight: bold;\"><a name=\"\"></a></span>") == text_);

        text_.clear();
        caption(QString("Hello"), QString("Cool"));
        Q_ASSERT( QString("<span style=\"font-weight: bold;\"><a name=\"Hello\"></a>Cool</span>")  == text_);
        return true;
    }

};
    //------------------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
}; 
//------------------------------------------------------------------------------
#endif	/* _V_HTML_ANCHOR_H_ */

