// -*- C++ -*-
/* 
 * File:   Html.h
 * Author: S.Panin
 *
 * Created on Вт июня 15 2010, 10:45:47
 */
//------------------------------------------------------------------------------
#ifndef _V_HTML_H_
#define	_V_HTML_H_
//------------------------------------------------------------------------------
#include "Href/HtmlHref.h"
#include "Anchor/HtmlAnchor.h"
//------------------------------------------------------------------------------
namespace go
{ 
//------------------------------------------------------------------------------
class Html
{
    typedef             Html                                                    class_name;
    typedef             html::Href                                              class_href;
    typedef             html::Anchor                                            class_anchor;
    typedef             boost::shared_ptr<class_href>                           href_type;
    typedef             boost::shared_ptr<class_anchor>                         anchor_type;

    QString                                                                     text_;
    href_type                                                                   href_;
    anchor_type                                                                 anchor_;

public:
    explicit            Html(const QString& text =QString()) : text_(text)          
    {
        href_.reset( new class_href(text_));
        anchor_.reset( new class_anchor(text_));
    }
    const QString&      html() const                                            { return text_;}
    const QString&      html_with_br()                                          { add_br(); return text_;}
    class_href*         add_href()                                              { return href_.get(); }
    class_anchor*       add_anchor()                                            { return anchor_.get(); }
    void                add_br()                                                { text_ +=QString("<br>"); }
    void                add_html(const QString& html)                           { text_ +=html;  }
    bool test()
    {
        text_.clear();
        add_br();
        Q_ASSERT( QString("<br>") == text_);

        return true;
    }

};
//------------------------------------------------------------------------------
}; 
//------------------------------------------------------------------------------
#endif	/* _V_HTML_H_ */

