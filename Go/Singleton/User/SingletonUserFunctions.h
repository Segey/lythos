/* 
 * File:   SingletonUserFunctions.h
 * Author: S.Panin
 *
 * Created on 4 Апрель 2010 г., 0:29
 */
//------------------------------------------------------------------------------
#ifndef _V_SINGLETON_USER_FUNCTIONS_H_
#define	_V_SINGLETON_USER_FUNCTIONS_H_
//------------------------------------------------------------------------------
namespace v_user // BEGIN NAMESPACE v_user
{
//------------------------------------------------------------------------------
    const QStringList               get_all_user_names();
    bool                            check_password(const QString& user_name, const QString& password);
    bool                            exists(const QString& user_name);
    int                             create_new(const QString& user_name, const QString& password);
    double                          get_double(const QString& number);
    int                             get_int(const QString& number);
    bool                            is_double(const QString& text, const bool empty = true );
    QString                         get_string_is_double(const QString& number, const QString& result);
bool  test();
//------------------------------------------------------------------------------
}; // END NAMESPACE v_user
//------------------------------------------------------------------------------
#endif	/* _V_SINGLETON_USER_FUNCTIONS_H_ */
