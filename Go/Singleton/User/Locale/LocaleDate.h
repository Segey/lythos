
/* 
 * File:   OptionDate.h
 * Author: S.Panin
 *
 * Created on 7 Апрель 2010 г., 14:58
 */
//------------------------------------------------------------------------------
#ifndef _V_LOCALE_DATE_H_
#define	_V_LOCALE_DATE_H_
//------------------------------------------------------------------------------
template <typename S>
class vLocaleType : public S
{    
    typedef                     S                                               strategy_name;
    typedef                     S                                               inherited;
    typedef                     vLocaleType<strategy_name>                      class_name;
    typedef                     typename inherited::type_name                   type_name;
    
    QLocale                                                                     locale_;
    
public:
    void                        set_locale(const QLocale& locale)               { locale_ = locale; }
    const QLocale&              locale() const                                  { return locale_; }
    QString                     to_string(const type_name& value)  const        { return inherited::to_string(value, locale()); }
    type_name                   from_string(const QString& value)  const        { return inherited::from_string(value, locale()); }
    bool                        check_string(const QString& value) const        { return inherited::check_string(value, locale()); }
    bool                        test()                                          { return inherited::test(locale()); }
};
//------------------------------------------------------------------------------
struct vLocaleTypeInt
{
    typedef                     vLocaleTypeInt                                  class_name;
    
protected:
    typedef                     int                                             type_name;

    QString                     to_string(const type_name& value, const QLocale& locale) const              { return locale.toString(value); }
    type_name                   from_string(const QString& value, const QLocale& locale) const
    {
        bool done =false;
        type_name result=locale.toInt(value, &done);
        return done ? result : 0;
    }
    bool                        check_string(const QString& value, const QLocale& locale) const
    {
        bool done =false;
        locale.toInt(value, &done);
        return done;
    }
    bool test(const QLocale& locale)
    {
        type_name x =15;
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x =0;
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x =-1800000;
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));
        Q_ASSERT( check_string("to",locale) ==false );
        return true;
    }
};
//------------------------------------------------------------------------------
class vLocaleTypeDouble
{
    typedef              vLocaleTypeDouble                                      class_name;

protected:
    typedef              double                                                 type_name;

public:
    QString              to_string_long(const type_name& value, const QLocale& locale, char f = 'g', int prec = 12) const        { return locale.toString(value,f,prec); }
    type_name            from_string_long(const QString& value, const QLocale& locale) const        { return from_string(value, locale); }

protected:
    QString              to_string(const type_name& value, const QLocale& locale) const   { return locale.toString(value,'g',6); }
    type_name            from_string(const QString& value, const QLocale& locale) const
    {
        bool done =false;
        type_name result=locale.toDouble(value, &done);
        return done ? result : 0.0;
    }
    bool                 check_string(const QString& value, const QLocale& locale) const
    {
        bool done =false;
        locale.toDouble(value, &done);
        return done;
    }
    bool test(const QLocale& locale)
    {
        type_name x =15.56;
        Q_ASSERT( from_string_long(to_string_long(x, locale), locale) ==x );
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x =0;
        Q_ASSERT( from_string_long(to_string_long(x, locale), locale) ==x );
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x =18000.9;
        Q_ASSERT( from_string_long(to_string_long(x, locale), locale) ==x );
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));
        Q_ASSERT( check_string("to",locale) ==false );
        return true;
    }
};
//------------------------------------------------------------------------------
class vLocaleTypeDate
{
    typedef              vLocaleTypeDate                                        class_name;

protected:
    typedef              QDate                                                  type_name;

public:
    QString              to_string_short(const type_name& value, const QLocale& locale) const        { return locale.toString(value,QLocale::ShortFormat); }
    type_name            from_string_short(const QString& value, const QLocale& locale) const        { return locale.toDate(value,QLocale::ShortFormat); }
    bool                 check_string_short(const QString& value, const QLocale& locale) const       { return check_string(value, locale); }

protected:
    QString              to_string(const type_name& value, const QLocale& locale) const              { return locale.toString(value,QLocale::LongFormat); }
    type_name            from_string(const QString& value, const QLocale& locale) const              { return locale.toDate(value,QLocale::LongFormat); }
    bool                 check_string(const QString&, const QLocale&) const             { return true; }
    bool test(const QLocale& locale)
    {
        type_name x (2015,5,6);
//        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x = QDate(2000,12,26);
//        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));

        x =QDate::currentDate();
//        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
        Q_ASSERT( check_string(to_string(x,locale), locale));
        return true;

    }
};
//------------------------------------------------------------------------------
class vLocaleTypeDateTime
{
    typedef              vLocaleTypeDateTime                                    class_name;

protected:
    typedef              QDateTime                                              type_name;

public:
    QString              to_string_short(const type_name& value, const QLocale& locale) const        { return locale.toString(value,QLocale::ShortFormat); }
    type_name            from_string_short(const QString& value, const QLocale& locale) const        { return locale.toDateTime(value,QLocale::ShortFormat); }
    bool                 check_string_short(const QString& value, const QLocale& locale) const       { return check_string(value, locale); }

protected:
    QString              to_string(const type_name& value, const QLocale& locale) const              { return locale.toString(value,QLocale::LongFormat); }
    type_name            from_string(const QString& value, const QLocale& locale) const              { return locale.toDateTime(value,QLocale::LongFormat); }
    bool                 check_string(const QString&, const QLocale&) const                          { return true; }
    bool test(const QLocale&)
    {
//        type_name x =QDateTime::currentDateTime();
//        qDebug() << x;
//        qDebug() << to_string(x, locale);
//        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
//        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
//        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
//        Q_ASSERT( check_string(to_string(x,locale), locale));
//
//        x = QDateTime(QDate(2000,12,26));
////        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
//        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
//        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
//        Q_ASSERT( check_string(to_string(x,locale), locale));
//
//        x =QDateTime::currentDateTime();
////        Q_ASSERT( from_string_short(to_string_short(x, locale), locale) ==x );
//        Q_ASSERT( check_string_short(to_string_short(x,locale), locale));
//        Q_ASSERT( from_string(to_string(x,locale), locale) ==x );
//        Q_ASSERT( check_string(to_string(x,locale), locale));
        return true;

    }
};
//------------------------------------------------------------------------------
#endif	/* _V_LOCALE_DATE_H_ */

