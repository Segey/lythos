// -*- C++ -*-
/* 
 * File:   Locale.h
 * Author: S.Panin
 *
 * Created on 10 Апрель 2010 г., 23:03
 */
//------------------------------------------------------------------------------
#ifndef _V_LOCALE_H_
#define	_V_LOCALE_H_
//------------------------------------------------------------------------------
#include "LocaleDate.h"
#include "Mea/MeaHeight.h"
#include "Mea/MeaWeight.h"
//------------------------------------------------------------------------------
class vLocale : private go::noncopyable
{
    typedef                     vLocale                                         class_type;
    typedef                     vLocaleType<vLocaleTypeInt>                     int_type;
    typedef                     vLocaleType<vLocaleTypeDouble>                  double_type;
    typedef                     vLocaleType<vLocaleTypeDate>                    date_type;
    typedef                     vLocaleType<vLocaleTypeDateTime>                date_time_type;
    typedef                     boost::shared_ptr<vLocaleMeaHeight>             height_type;
    typedef                     boost::shared_ptr<vLocaleMeaWeight>             weight_type;

    QLocale                                                                     locale_;
    int_type                                                                    int_;
    double_type                                                                 double_;
    date_type                                                                   date_;
    date_time_type                                                              date_time_;
    height_type                                                                 height_;
    weight_type                                                                 weight_;

public:
    explicit                    vLocale() : height_(new vLocaleMeaHeightMetric), weight_(new vLocaleMeaWeightMetric) {}
    void                        set_locale(const QLocale& locale)
    {
        locale_ =locale;
        int_.set_locale(locale);
        double_.set_locale(locale);
        date_.set_locale(locale);
        date_time_.set_locale(locale);
        if (locale.measurementSystem() == QLocale::ImperialSystem )
        {
            height_.reset(new vLocaleMeaHeightImperial);
            weight_.reset(new vLocaleMeaWeightImperial);
        }
        else
        {
            height_.reset(new vLocaleMeaHeightMetric);
            weight_.reset(new vLocaleMeaWeightMetric);
        }
    }
    const QLocale&              locale() const                                  { return locale_;}
    void                        clear()                                         { set_locale(QLocale::system()); }
    const int_type&             Int() const                                     { return int_; }
    const double_type&          Double() const                                  { return double_; }
    const date_type&            Date() const                                    { return date_; }
    const date_time_type&       DateTime() const                                { return date_time_; }
    vLocaleMeaHeight*           height() const                                  { return height_.get(); }
    vLocaleMeaWeight*           weight() const                                  { return weight_.get(); }
    bool test()
    {
        return true;
    }

};
//------------------------------------------------------------------------------
#endif	/* _V_LOCALE_H */
