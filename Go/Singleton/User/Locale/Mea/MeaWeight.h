// -*- C++ -*-
/* 
 * File:   MeaWeight.h
 * Author: S.Panin
 *
 * Created on 11 Апрель 2010 г., 1:23
 */
//------------------------------------------------------------------------------
#ifndef _V_MEA_WEIGHT_H_
#define	_V_MEA_WEIGHT_H_
//------------------------------------------------------------------------------
class vLocaleMeaWeight
{
    typedef                         vLocaleMeaWeight                            class_name;

    virtual QString                 do_kg() const                               =0;
    virtual QString                 do_kgs() const                              =0;

public:
    QString                         kg() const                                  { return do_kg(); }
    QString                         kgs() const                                 { return do_kgs(); }
};
//------------------------------------------------------------------------------
class vLocaleMeaWeightImperial : public vLocaleMeaWeight
{
    typedef                        vLocaleMeaWeight                             inherited;
    typedef                        vLocaleMeaWeightImperial                     class_name;

    virtual QString                do_kg() const                                { return vtr::Mea::tr("lb"); }
    virtual QString                do_kgs() const                               { return vtr::Mea::tr("lbs"); }
};
//------------------------------------------------------------------------------
class vLocaleMeaWeightMetric : public vLocaleMeaWeight
{
    typedef                        vLocaleMeaWeight                             inherited;
    typedef                        vLocaleMeaWeightMetric                       class_name;

    virtual QString                do_kg() const                                { return vtr::Mea::tr("kg"); }
    virtual QString                do_kgs() const                                { return vtr::Mea::tr("kg"); }
};
//------------------------------------------------------------------------------
#endif	/* _V_MEA_WEIGHT_H */
