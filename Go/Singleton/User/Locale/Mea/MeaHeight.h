// -*- C++ -*-
/* 
 * File:   MeaHeight.h
 * Author: S.Panin
 *
 * Created on 11 Апрель 2010 г., 1:21
 */
//------------------------------------------------------------------------------
#ifndef _V_MEA_HEIGHT_H_
#define	_V_MEA_HEIGHT_H_
//------------------------------------------------------------------------------
class vLocaleMeaHeight
{
    typedef                         vLocaleMeaHeight                            class_name;

    virtual QString                 do_cm() const                               =0;
    virtual QString                 do_cms() const                              =0;

public:
    QString                         cm() const                                  { return do_cm(); }
    QString                         cms() const                                 { return do_cms(); }
};
//------------------------------------------------------------------------------
class vLocaleMeaHeightImperial : public vLocaleMeaHeight
{
    typedef                        vLocaleMeaHeight                             inherited;
    typedef                        vLocaleMeaHeightImperial                     class_name;

    virtual QString                do_cm() const                                { return vtr::Mea::tr("inch"); }
    virtual QString                do_cms() const                               { return vtr::Mea::tr("inches"); }
};
//------------------------------------------------------------------------------
class vLocaleMeaHeightMetric : public vLocaleMeaHeight
{
    typedef                        vLocaleMeaHeight                             inherited;
    typedef                        vLocaleMeaHeightMetric                       class_name;

    virtual QString                do_cm() const                                { return vtr::Mea::tr("cm"); }
    virtual QString                do_cms() const                               { return vtr::Mea::tr("cms"); }
};
//------------------------------------------------------------------------------
#endif	/* _V_MEA_HEIGHT_H */
