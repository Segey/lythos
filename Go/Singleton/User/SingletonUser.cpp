/* 
 * File:   SingletonUser.cpp
 * Author: S.Panin
 * 
 * Created on 12 Июль 2009 г., 15:38
 */
//------------------------------------------------------------------------------
#include "SingletonUser.h"
//------------------------------------------------------------------------------
void vSingletonUser::settings_read()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT Language, Country, Translation FROM Options WHERE UserID = ?;");
        query.addBindValue(userID_);
        if(!query.exec() ) return;
        if(query.first())
        {
                locale_.set_locale(QLocale(static_cast<QLocale::Language>(query.value(0).toInt()), static_cast<QLocale::Country>(query.value(1).toInt())));
                translation_->load_file(query.value(2).toString());
        }else
                {
                        locale_.set_locale(QLocale::system());
                        translation_->load_file(vTranslation::file_default());
                }
}
//------------------------------------------------------------------------------
void vSingletonUser::settings_write() const
{
        const bool result =v_bd::exists(vMain::basebd().database(),"SELECT UserID FROM Options WHERE UserID = ?;");
        QSqlQuery query(vMain::basebd().database());
        if (result) query.prepare("UPDATE Options SET Language=?, Country=?, Translation=?  WHERE UserID =?;");
                else query.prepare("INSERT INTO Options (Language, Country, Translation, UserID)  VALUES (?,?,?,?);");
        query.addBindValue( locale_.locale().language() );
        query.addBindValue( locale_.locale().country() );
        query.addBindValue( translation_->file() );
        query.addBindValue( id() );
        query.exec();
}
//------------------------------------------------------------------------------
void vSingletonUser::profile_clear()
{
        userID_ = 0;
        name_.clear();
        password_.clear();
        locale_.clear();
}
//------------------------------------------------------------------------------
void   vSingletonUser::load(const QString& name)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT userID, UserPassword FROM SIGNUP WHERE userName = ?;");
        query.addBindValue(name);
        if(!query.exec() || !query.next() ) return profile_clear();
        userID_   = query.value(0).toInt();
        name_     = name;
        password_ = query.value(1).toString();

        settings_read();
}
//------------------------------------------------------------------------------
bool vSingletonUser::update_user(const QString& new_user_name, const QString& password) const
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE SignUp SET UserName =?, UserPassword =? WHERE userName =?;");
        query.addBindValue(new_user_name);
        query.addBindValue(password);
        query.addBindValue(name_);
        return query.exec();
}
//------------------------------------------------------------------------------
bool vSingletonUser::profile_update(const QString& new_user_name, const QString& password)
{
        if (new_user_name == name_ && password == password_) return true;
        if ( !update_user(new_user_name, password) ) return false;

        name_ = new_user_name;
        password_ = password;
        return true;
}
//------------------------------------------------------------------------------
bool vSingletonUser::test()
{
      QSqlQuery query(vMain::basebd().database());
      Q_ASSERT(query.exec("INSERT INTO SignUp (UserID, UserName, UserPassword)  VALUES (0, '999', 'pass');"));

      profile_clear();
      Q_ASSERT(userID_ ==0);
      Q_ASSERT(name_.isEmpty());
      Q_ASSERT(password_.isEmpty());

      settings_write();
     
      load("999");
      Q_ASSERT( userID_ ==0 );
      Q_ASSERT( name_ == "999" );
      Q_ASSERT( password_ == "pass" );

      profile_update(name_, password_);
      Q_ASSERT( name_ == "999" );
      Q_ASSERT( password_ == "pass" );

      profile_update("cool", "cool password" );
      Q_ASSERT( name_ == "cool" );
      Q_ASSERT( password_ == "cool password" );

      set_test_id();
      Q_ASSERT(userID_ ==0);

      profile_clear();
      Q_ASSERT(query.exec("DELETE FROM SignUp WHERE UserID =0;"));
      return query.exec("DELETE FROM Options WHERE UserID =0;");
}
//------------------------------------------------------------------------------