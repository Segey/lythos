/* 
 * File:   SingletonUser.h
 * Author: S.Panin
 *
 * Created on 12 Июль 2009 г., 15:38
 */
//------------------------------------------------------------------------------
#ifndef V_SINGLETON_USER_H_
#define	V_SINGLETON_USER_H_
//------------------------------------------------------------------------------
#include "SingletonUserFunctions.h"
#include "Translation/Translation.h"
#include "Locale/Locale.h"
//------------------------------------------------------------------------------
class vSingletonUser
{
    typedef                             vSingletonUser                          class_name;
    typedef                             boost::shared_ptr<vTranslation>         translation_type;
    
    int                                                                         userID_;
    QString                                                                     name_;
    QString                                                                     password_;
    vLocale                                                                     locale_;
    translation_type                                                            translation_;

    void                                settings_read();
    void                                profile_clear();
    bool                                update_user(const QString& new_user_name, const QString& password) const;

public:
    explicit vSingletonUser() : translation_(new vTranslation ) {}
    void                                load(const QString& name);
    void                                settings_write() const;
    bool                                profile_update(const QString& new_user_name, const QString& password);
    int                                 id() const                              { return userID_;   }
    const QString&                      name() const                            { return name_;     }
    const QString&                      password() const                        { return password_; }
    void                                set_test_id()                           { userID_ = 0;      }
    const vLocale&                      locale() const                          { return locale_; }
    vTranslation*                       translation() const                     { return translation_.get(); }
    void                                set_application(QApplication* app)      { translation_->set_application(app); }
    bool                                test();
};
//------------------------------------------------------------------------------
#endif	/* V_SINGLETON_USER_H_ */
