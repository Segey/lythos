/* 
 * File:   SingletonUserFunctions.cpp
 * Author: S.Panin
 * 
 * Created on 4 Апрель 2010 г., 0:29
 */
//------------------------------------------------------------------------------
#include "SingletonUserFunctions.h"
//------------------------------------------------------------------------------
namespace v_user // BEGIN NAMESPACE v_user
{
//------------------------------------------------------------------------------
const QStringList get_all_user_names()
{
        QStringList List;
        QSqlQuery query(vMain::basebd().database());
        query.exec("SELECT userName FROM SignUp;");
        while(query.next())
                List.push_back(query.value(0).toString());
        return List;
}
//------------------------------------------------------------------------------
bool check_password(const QString& user_name, const QString& password)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT userPassword FROM SIGNUP WHERE userName = ?;");
        query.addBindValue(user_name);
        return query.exec() && query.next() ? query.value(0).toString() == password : false;
}
//------------------------------------------------------------------------------
bool exists (const QString& user_name)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT userName FROM SIGNUP WHERE userName = ?;");
        query.addBindValue(user_name);
        return query.exec() && query.next() ? false ==query.isNull(0) : false;
}
//------------------------------------------------------------------------------
int create_new(const QString& user_name, const QString& password)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO SignUp (UserName, UserPassword)  VALUES (?, ?);");
        query.addBindValue(user_name);
        query.addBindValue(password);
        if ( !query.exec() ) return 0;

        query.prepare("SELECT userID FROM SIGNUP WHERE userName = ?;");
        query.addBindValue(user_name);
        return query.exec() && query.next() ? query.value(0).toInt() : 0;
}
//------------------------------------------------------------------------------
double get_double(const QString& number)
{
        return vMain::user().locale().Double().from_string(number);
}
//------------------------------------------------------------------------------
int get_int(const QString& number)
{
        return vMain::user().locale().Int().from_string(number);
}
//------------------------------------------------------------------------------
bool  is_double(const QString& text, const bool empty /*= true */)
{
        if ( text.isEmpty() && empty ) return true;
        return vMain::user().locale().Double().check_string(text);
}
//------------------------------------------------------------------------------
QString get_string_is_double(const QString& number, const QString& result)
{
        return vMain::user().locale().Double().check_string(number) ? number : result;
}
//------------------------------------------------------------------------------
bool test()
{
        const QString name      = "syper as";
        const QString password  = "asdf";
        create_new(name, password);
        Q_ASSERT(exists (name));
        Q_ASSERT( false == check_password(name, "s") );
        Q_ASSERT( check_password(name, password) );
        Q_ASSERT( get_all_user_names().size() );

        QSqlQuery query(vMain::basebd().database());
        return query.exec(QString("DELETE FROM SignUp WHERE UserName ='%1';").arg(name));
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
}; // END NAMESPACE v_user
