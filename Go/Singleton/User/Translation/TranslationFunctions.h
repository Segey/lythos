/* 
 * File:   TranslationFunctions.h
 * Author: S.Panin
 *
 * Created on 11 Апрель 2010 г., 22:07
 */
//------------------------------------------------------------------------------
#ifndef _V_TRANSLATION_FUNCTIONS_H_
#define	_V_TRANSLATION_FUNCTIONS_H_
//------------------------------------------------------------------------------
namespace v_translation // BEGIN NAMESPACE vTranslationFunctions
{
//------------------------------------------------------------------------------
QMap<QString,QString>               get_all_translations();
bool  test();
//------------------------------------------------------------------------------
}; // END NAMESPACE vTranslationFunctions
//------------------------------------------------------------------------------
#endif	/* _V_TRANSLATION_FUNCTIONS_H_ */
