/* 
 * File:   TranslationFunctions.cpp
 * Author: S.Panin
 * 
 * Created on 11 Апрель 2010 г., 22:07
 */
//------------------------------------------------------------------------------
#include "TranslationFunctions.h"
//------------------------------------------------------------------------------
namespace v_translation // BEGIN NAMESPACE vTranslationFunctions
{
//------------------------------------------------------------------------------
QMap<QString,QString>     get_all_translations()
{
       const QString str = "%1. %2";
       int cx =0;
       QMap<QString,QString> result;
       result.insert(str.arg(++cx).arg("English"), vMain::user().translation()->file_default());

       const QStringList& list =  QDir( const_paths::translation() ).entryList( QStringList(const_exp::translation()), QDir::Files | QDir::NoSymLinks);
       for (QStringList::const_iterator it=list.begin(); it !=list.end(); ++it)
       {
             QTranslator translator;
             translator.load( *it, const_paths::translation() );
             const QString language = translator.translate("vtr::Language", "English", "Interface language");
             if ( !language.isEmpty() ) result.insert(str.arg(++cx).arg(language), *it );
            
       }
       return result;
}
//------------------------------------------------------------------------------
bool test()
{
        const QMap<QString,QString>& map = get_all_translations();
        Q_ASSERT(map.find("1. English") != map.end() );
        Q_ASSERT(map.size() );
        return true;
}
//------------------------------------------------------------------------------
}; // END NAMESPACE vTranslationFunctions
