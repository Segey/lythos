// -*- C++ -*-
/* 
 * File:   Translation.h
 * Author: S.Panin
 *
 * Created on 11 Апрель 2010 г., 21:20
 */
//------------------------------------------------------------------------------
#ifndef _V_TRANSLATION_H_
#define	_V_TRANSLATION_H_
//------------------------------------------------------------------------------
#include "TranslationFunctions.h"
//------------------------------------------------------------------------------
class vTranslation
{
    typedef                         QString                                     type_name;
    typedef                         vTranslation                                class_name;

    type_name                                                                   language_;
    QTranslator                                                                 translation_;
    QApplication*                                                               application_;

public:
    explicit                        vTranslation( ) : language_(file_default()) {}
    void                            load_file(const type_name& language)
    {
        language_ = language;
        translation_.load(const_paths::translation() + language_);
        application_->installTranslator(&translation_);
    }
    type_name                       file() const                                { return language_; }
    static type_name                file_default()                              { return "base"; }
    void                            set_application(QApplication* app)          { application_ =app; }

    bool test()
    {
        Q_ASSERT( application_ );
        Q_ASSERT( language_ == file() );
        return true;
    }

};
//------------------------------------------------------------------------------
#endif	/* _V_TRANSLATION_H_ */

