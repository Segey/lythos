/* 
 * File:   SingletonEGBD.h
 * Author: S.Panin
 *
 * Created on Вт июня 29 2010, 22:41:51
 */
//------------------------------------------------------------------------------
#ifndef _V_SINGLETON_EG_BD_V_
#define	_V_SINGLETON_EG_BD_V_
//------------------------------------------------------------------------------
#include "../Action/ActionBD.h"
//------------------------------------------------------------------------------
namespace go{ namespace singleton{
//------------------------------------------------------------------------------
class EG_BD : public vActionBD
{
    typedef                     vActionBD                                       inherited;
    typedef                     EG_BD                                           class_name;
    
public:
    EG_BD(QWidget* parent =0) : vActionBD(parent)
    {
        inherited::database_name()      = "EG";
        inherited::file_ext()           = const_exp::bd_exercise_guide();
        inherited::default_database()   = const_file_default::bd_exercise_guide();
        inherited::dll_script()         = const_script::bd_exercise_guide();
    }
};
//------------------------------------------------------------------------------
}}
//------------------------------------------------------------------------------
#endif	/* _V_SINGLETON_EG_BD_V_ */
