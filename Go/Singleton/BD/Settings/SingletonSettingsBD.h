/* 
 * File:   vSingletonSettingsBD.h
 * Author: S.Panin
 *
 * Created on 22 Марта 2010 г., 22:59
 */
//------------------------------------------------------------------------------
#ifndef _V_SINGLETON_SETTINGS_BD_H_
#define	_V_SINGLETON_SETTINGS_BD_H_
//------------------------------------------------------------------------------
#include "../Action/ActionBD.h"
//------------------------------------------------------------------------------
class vSingletonSettingsBD : public ActionBD
{
    typedef                     ActionBD                                       inherited;
    typedef                     vSingletonSettingsBD                            class_name;
    
public:
    vSingletonSettingsBD(QWidget* parent =0) : ActionBD(parent)
    {
        inherited::database_name()      = "Settings";
        inherited::file_ext()           = const_exp::bd_settings();
        inherited::default_database()   = const_file_default::bd_settings();
        inherited::dll_script()         = const_script::bd_settings();
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_SINGLETON_SETTINGS_BD_H_ */
