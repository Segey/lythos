/* 
 * File:   vSingletonBaseBD.h
 * Author: S.Panin
 *
 * Created on 9 Июль 2009 г., 10:18
 */
//------------------------------------------------------------------------------
#ifndef _V_SINGLETON_BD_H_
#define	_V_SINGLETON_BD_H_
//------------------------------------------------------------------------------
#include "../Action/ActionBD.h"
//------------------------------------------------------------------------------
class vSingletonBaseBD : public ActionBD
{
    typedef                     ActionBD                                       inherited;
    typedef                     vSingletonBaseBD                                class_name;
    
public:
    vSingletonBaseBD(QWidget* parent =0) : ActionBD(parent)
    {
        inherited::database_name()      = "Base";
        inherited::file_ext()           = const_exp::bd_base();
        inherited::default_database()   = const_file_default::bd_base();
        inherited::dll_script()         = const_script::bd_base();
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_SINGLETON_BD_H_ */
