/* 
 * File:   VBD.cpp
 * Author: S.Panin
 * 
 * Created on 5 Июль 2009 г., 17:05
 */
//------------------------------------------------------------------------------
#include "ActionBD.h"
//------------------------------------------------------------------------------
ActionBD::ActionBD(QWidget* parent /*=0*/)
        : parent_(parent), new_act_(0), open_act_(0), save_act_(0), save_as_act_(0)
{
        create_actions();
        instance_signals();
        afterConnect(false);
        translate();
}
//------------------------------------------------------------------------------
bool ActionBD::Connect()
{
        return inherited::connect(get_database_filename(), database_name_);
}
//------------------------------------------------------------------------------
QString ActionBD::get_database_filename() const
{
        return QSettings().value("/Base/Database", default_database_).toString();
}
//------------------------------------------------------------------------------
void ActionBD::set_database_filename(const QString& filename)
{
        QSettings().setValue("/Base/Database", filename);
}
//------------------------------------------------------------------------------
void ActionBD::afterConnect(bool value)
{
        save_act_->setEnabled(value);
        save_as_act_->setEnabled(value);
}
//------------------------------------------------------------------------------
QString  ActionBD::getDialogFilters()
{
        return vtr::Filter::tr("Database file %1All files %2").arg("(" + file_ext_ +");;").arg("(*.*)");
}
//------------------------------------------------------------------------------
void ActionBD::instance_signals()
{
        QObject::connect(new_act_, SIGNAL(triggered()), this, SLOT(executeActionNew()));
        QObject::connect(open_act_, SIGNAL(triggered()), this, SLOT(executeActionOpen()));
        QObject::connect(save_act_, SIGNAL(triggered()), this, SLOT(executeActionSave()));
        QObject::connect(save_as_act_, SIGNAL(triggered()), this, SLOT(executeActionSaveAs()));
}
//------------------------------------------------------------------------------
void ActionBD::create_actions()
{       
        new_act_        = vAction::createNewDatabase(parent_);
        open_act_       = vAction::createOpenDatabase(parent_);
        save_act_       = vAction::createSaveDatabase(parent_);
        save_as_act_    = vAction::createSaveAsDatabase(parent_);
}
//------------------------------------------------------------------------------
void ActionBD::executeActionNew()
{
        QString file_name = QFileDialog::getSaveFileName(0, vtr::Dialog::tr("Choose the database file name"), get_database_filename(), getDialogFilters());
        if(file_name.isEmpty()) return;

        QApplication::setOverrideCursor(Qt::WaitCursor);
        inherited::disconnect();
        QFile::remove(file_name);
        bool success;
        //= (inherited::create_database(file_script_, file_name, database_name_) && (inherited::connect(file_name, database_name_)));
        QApplication::restoreOverrideCursor();
        if(success)
        {
                set_database_filename(file_name);
                QMessageBox::information(parent_, program::name_full(), vtr::Message::tr("Database '%1' is successfully created!").arg(QFileInfo(file_name).fileName()), QMessageBox::Ok);
        }
}
//------------------------------------------------------------------------------
int ActionBD::executeActionOpen()
{
        QString file_name = QFileDialog::getOpenFileName(0, vtr::Dialog::tr("Open"), get_database_filename(), getDialogFilters());
        if(file_name.isEmpty()) return 0;

        inherited::disconnect();
        if(!inherited::check_connect(file_name, database_name_)) return QMessageBox::critical(0, program::name_full(), vtr::Message::tr("%1 could not open '%2' because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn't correctly decoded).").arg(program::name()).arg(QFileInfo(file_name).fileName()), QMessageBox::Ok);

        inherited::connect(file_name, database_name_);
        set_database_filename(file_name);
        return QMessageBox::information(parent_, program::name_full(), vtr::Message::tr("Database '%1' is successfully opened!").arg(QFileInfo(file_name).fileName()), QMessageBox::Ok);
}
//------------------------------------------------------------------------------
void ActionBD::executeActionSave()
{
        save_act_->setEnabled(false);
}
//------------------------------------------------------------------------------
int ActionBD::executeActionSaveAs()
{
        QString file_name = QFileDialog::getSaveFileName(0, vtr::Dialog::tr("Save as..."), get_database_filename(), getDialogFilters());
        if(file_name.isEmpty()) return 0;

        QFile::remove(file_name);
        if(!QFile::copy(inherited::database_filename(), file_name))  return QMessageBox::critical(0, program::name_full(), vtr::Message::tr("Error while trying to save database file '%1' !").arg(QFileInfo(file_name).fileName()), QMessageBox::Ok);

        return QMessageBox::information(parent_, program::name_full(), vtr::Message::tr("Database '%1' is successfully saved!").arg(QFileInfo(file_name).fileName()), QMessageBox::Ok);
}
//------------------------------------------------------------------------------
bool ActionBD::test()
{
        Q_ASSERT(new_act_);
        Q_ASSERT(open_act_);
        Q_ASSERT(save_act_);
        Q_ASSERT(save_as_act_);
        Q_ASSERT( !new_act_->icon().isNull() );
        Q_ASSERT( !open_act_->icon().isNull() );
        Q_ASSERT( !save_act_->icon().isNull() );
        Q_ASSERT( !save_as_act_->icon().isNull() );
        Q_ASSERT( !new_act_->text().isEmpty() );
        Q_ASSERT( !open_act_->text().isEmpty() );
        Q_ASSERT( !save_act_->text().isEmpty() );
        Q_ASSERT( !save_as_act_->text().isEmpty() );
        return true;
}
//------------------------------------------------------------------------------