/**
    \file   ActionBD.h
    \brief  Файл работы с базой данных

    \author S.Panin
    \date   Created on 5 Июль 2009 г., 17:05
 */
//------------------------------------------------------------------------------
#ifndef _V_ACTION_BD_H_
#define	_V_ACTION_BD_H_
//------------------------------------------------------------------------------
#include "../../../BD/new_bd/base_bd.h"
#include "../../../Strategy/message/strategy_message.h"
#include "../../../BuiltType/Action/Action.h"

//------------------------------------------------------------------------------
class ActionBD : QObject, go::base_bd<go::strategy::message::window>, go::noncopyable
{
Q_OBJECT

    typedef             go::base_bd<go::strategy::message::window>              inherited;
    typedef             ActionBD                                                class_name;
    
private:
    QWidget*                                                                    parent_;

    QString                                                                     database_name_;
    QString                                                                     default_database_;
    QString                                                                     file_ext_;
    QString                                                                     file_script_;

    QAction*                                                                    new_act_;
    QAction*                                                                    open_act_;
    QAction*                                                                    save_act_;
    QAction*                                                                    save_as_act_;
    

    QString             get_database_filename() const;
    void                set_database_filename(const QString& filename);
    QString             getDialogFilters();
    bool                setView(const bool Value);
    void                instance_signals();
    void                create_actions();

    virtual void        afterConnect(bool value);

 public slots:
    void                executeActionNew();
    int                 executeActionOpen();
    void                executeActionSave();
    int                 executeActionSaveAs();

protected:
    QString&            database_name()                                         {return database_name_;}
    QString&            default_database()                                      {return default_database_;}
    QString&            file_ext()                                              {return file_ext_;}
    QString&            dll_script()                                            {return file_script_;}
    
public:
   ActionBD            (QWidget* parent =0);
   virtual              ~ActionBD()                                             {}
   bool                 Connect();
   QSqlDatabase         database() const                                        {return inherited::database(database_name_);}
   QString              database_filename() const                               {return inherited::database_filename();}
   QAction*             action_new() const                                      {return new_act_;}
   QAction*             action_open() const                                     {return open_act_;}
   QAction*             action_save() const                                     {return save_act_;}
   QAction*             action_save_as() const                                  {return save_as_act_;}
   void                 translate()                                             {}
   bool                 test();
};
//------------------------------------------------------------------------------
#endif	/* _V_ACTION_BD_H_ */

