/**
 \file  BaseBD.h
 \brief Файл для работы с базой данных
 \note Deprecated. use go::base_bd

 \author S.Panin
 \date Created on 7 Июль 2009 г., 11:52
 */
//------------------------------------------------------------------------------
#ifndef _V_BASE_BD_H_
#define	_V_BASE_BD_H_
//------------------------------------------------------------------------------
/**
\class vBaseBD
\brief Основной абстрактный интерфейс для работы с базой данных
\note Производные классы в обязательном порядке должны переопределить функцию afterConnect(bool value).\n
 Которая вызывается после установления удачного соединения.

Пример создания ngx_str_t
\code
ngx_str_t buf;
buf.data = ngx_alloc(255, log);
buf.len =255;
\endcode
 */
class vBaseBD
{
    typedef                 vBaseBD                                             class_name;

    QSqlDatabase                                                                bd_;
    QString                                                                     database_filename_;

    bool                    set_after_connect(bool value)                       {afterConnect(value); return value;}
    static bool             check_correctBD(const QSqlDatabase& bd, const QString& database_name);

protected:
    virtual void            afterConnect(bool value)                            =0;

public:
    virtual                 ~vBaseBD()                                          {}
/**
\fn static bool             check_connect(const QString& filename, const QString& connection_name_);
\brief Статическая функция проверки возможности подключения к базе данных SQLite
\param filename Имя файла базы данных
\param database_name Имя базы данных 
\result bool Удалось подключиться к базе данных или нет
\warning База данных в обязательном порядке должна содержать таблицу info  как минимум с двумя полями:\n
Первое поле - Имя программы возращаемое функцией program::name();\n
Второе поле - Имя базы данных database_name (т.е второй параметр передаваемый в функцию);
*/
    static bool             check_connect(const QString& filename, const QString& database_name);

/**
\fn bool                    connect(const QString& filename, const QString& database_name);
\brief Статическая функция проверки возможности подключения к базе данных SQLite
\param filename Имя файла базы данных
\param database_name Имя базы данных
\result bool Удалось подключиться к базе данных или нет. В случае успеха вызывается абстрактная виртуальная функция afterConnect(bool value).
*/
    bool                    connect(const QString& filename, const QString& database_name);
    
/**
\fn static bool             create_database(const QString& sourseScript, const QString& targetFile, const QString& connection_name_);
\brief Статическая функция для создания базы данных из скрипта. Скриптом служит обычный пустой файл с базой данных.
\param filename Имя файла базы данных
\param database_name Имя базы данных
\result bool Удалось подключиться к базе данных или нет. В случае успеха вызывается абстрактная виртуальная функция afterConnect(bool value).
*/
    static bool             create_database(const QString& sourseScript, const QString& targetFile, const QString& connection_name_);
    void                    disconnect(const QString&)                          { bd_.close();}
    QSqlDatabase            database(const QString& connection_name_) const     {return bd_.database(connection_name_);}
    const QString&          database_filename() const                           {return database_filename_;}
    bool                    is_connect()                                        {return bd_.isValid();}
};
//------------------------------------------------------------------------------
#endif	/* _V_BASE_BD_H_ */
