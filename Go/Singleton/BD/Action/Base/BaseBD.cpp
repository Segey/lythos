/* 
 * File:   vBaseBD.cpp
 * Author: S.Panin
 * 
 * Created on 7 Июль 2009 г., 11:52
 */
//------------------------------------------------------------------------------
#include "BaseBD.h"
//------------------------------------------------------------------------------
bool vBaseBD::check_correctBD(const QSqlDatabase& bd, const QString& database_name)
{
        QSqlQuery query(bd);
        query.exec("SELECT program, database_name FROM Info");
        query.next();
        return (query.value(0).toString() == QString(program::name()) && (query.value(1).toString() == database_name));
}
//------------------------------------------------------------------------------
bool vBaseBD::check_connect(const QString& filename, const QString& database_name)
{     
        if(!QFile::exists(filename)) return false;

        bool b = false;
        {
                QSqlDatabase bd = QSqlDatabase::addDatabase("QSQLITE", "Other");
                bd.setDatabaseName(filename);
                b = !bd.open() ? false : check_correctBD(bd, database_name);
        }
        QSqlDatabase::removeDatabase("Other");
        return b;
}
//------------------------------------------------------------------------------
bool vBaseBD::connect(const QString& database_filename, const QString& connection_name)
{
        if(!vBaseBD::check_connect(database_filename, connection_name))
                {
                QMessageBox::critical(0, program::name_full(), vtr::Message::tr("%1 could not open '%2' because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn't correctly decoded).").arg(program::name()).arg(QFileInfo(database_filename).fileName()),QMessageBox::Ok);
                return false;
                }
        disconnect(connection_name);
        bd_ =QSqlDatabase::addDatabase("QSQLITE", connection_name);
        bd_.setDatabaseName(database_filename);
        database_filename_ = database_filename;
        return set_after_connect(bd_.open());
}
//------------------------------------------------------------------------------
bool vBaseBD::create_database(const QString& sourseScript, const QString& targetFile, const QString& database_name)
{
        if( !QFile::exists(sourseScript) ) {QApplication::restoreOverrideCursor(); QMessageBox::critical(0, program::name_full(), vtr::Message::tr("%1 could not open '%2' because it Is either not a supported file type or because the file has been damaged!").arg(program::name()).arg(sourseScript),QMessageBox::Ok); return false;}
        QFile::remove(targetFile);
        if( !QFile::copy(sourseScript, targetFile) )      {QApplication::restoreOverrideCursor(); QMessageBox::critical(0, program::name_full(), vtr::Message::tr("Error while trying to create database file '%1' !").arg(targetFile),QMessageBox::Ok);  return false; }
        return check_connect(targetFile, database_name);
}