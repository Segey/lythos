/**
    \file   const.h
    \brief  Файл констант проекта

    \author S.Panin
    \date   Created on 3 Июль 2009 г., 12:03
 */
//------------------------------------------------------------------------------
#ifndef _V_CONST_H_
#define	_V_CONST_H_
//------------------------------------------------------------------------------
/** \brief Модуль Основных данных программы. /Deprecated/ */
struct program
{
    static QString                  name_organisation()                         { return "Lythos"; }
    static QString                  name()                                      { return "Lythos BFM"; }
    static QString                  version()                                   { return "3.0"; }
    static QString                  name_full()                                 { return "Lythos BFM v.3.0"; }
    static QString                  name_total()                                { return "Lythos Bodybuilding / Fitness Manager"; }
    static QString                  name_help_file()                            { return "Help.chm"; }
    static QString                  url_homepage()                              { return "http://lythos.com/"; }
    static QString                  url_buy()                                   { return "http://lythos.com/buy.html"; }
    static QString                  mail()                                      { return "support@lythos.com"; }
    static QString                  url_bug()                                   { return "http://lythos.com/bug"; }
    /** \brief Автор программы.  */
    static QString                  author()                                    { return "Sergey Panin"; }
};
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
    /** \namespace go::consts  */
    namespace consts {

/** \brief Модуль Основных данных программы.*/
struct program
{
    static QString                  name_organisation()                         { return "Lythos"; }
    static QString                  name()                                      { return "Lythos BFM"; }
    static QString                  version()                                   { return "3.0"; }
    static const char*              name_full()                                 { return "Lythos BFM v.3.0"; }
    static QString                  name_total()                                { return "Lythos Bodybuilding / Fitness Manager"; }
    static QString                  name_help_file()                            { return "Help.chm"; }
    static QString                  url_homepage()                              { return "http://lythos.com/"; }
    static QString                  url_buy()                                   { return "http://lythos.com/buy.html"; }
    static QString                  mail()                                      { return "support@lythos.com"; }
    static QString                  url_bug()                                   { return "http://lythos.com/bug"; }
    /** \brief Автор программы.  */
    static QString                  author()                                    { return "Sergey Panin"; }
};

/** \brief Модуль расширений.*/
struct exp
{
    /** \brief Расширение библиотеки справочника упражнений.  */
    static QString                  eg()                                        { return "eg"; }
};
//------------------------------------------------------------------------------
        /** \namespace go::consts::bd  */
        namespace db {
//------------------------------------------------------------------------------
/** \brief база данных Base. */
struct base
{
    /** \brief Название базы данных.  */
    static QString                  name()                                      { return "Base"; }
    /** \brief Последняя версия базы данных.  */
    static double                   version()                                   { return 3.0; }
};
//------------------------------------------------------------------------------
/** \brief база данных Settings. */
struct settings
{
    /** \brief Название базы данных.  */
    static QString                  name()                                      { return "Settings"; }
    /** \brief Последняя версия базы данных.  */
    static double                   version()                                   { return 3.0; }
};
//------------------------------------------------------------------------------
        }; // end namespace bd

//------------------------------------------------------------------------------
        /** \namespace go::consts::module  */
        namespace module {
//------------------------------------------------------------------------------
/**
 \struct LD
 \brief Модуль LD.
*/
struct LD
{
    /** \brief Полное название модуля EG.  */
    static QString                  name_full()                                 { return program::name_full(); }
    /** \brief Только  название модуля EG для работы в командной строке.  */
    static QString                  name_console()                              { return "LD"; }
    /** \brief Название модуля EG для базы данных.  */
    static QString                  db_name()                                   { return "Base"; }
    /** \brief Последняя версия базы данных Справочника упражнений.  */
    static double                   db_version()                                { return 3.0; }
};
//------------------------------------------------------------------------------
 /**
 \struct eg
 \brief Модуль констант справочника упражнений.
*/
struct eg
{
    /** \brief Полное название модуля EG.  */
    static QString                  name_full()                                 { return "Exercises Guide v.3.0"; }
    /** \brief Только  название модуля EG для работы в командной строке.  */
    static QString                  name_console()                              { return "eg"; }
    /** \brief Название модуля EG для базы данных.  */
    static QString                  db_name()                                   { return "eg"; }
    /** \brief Последняя версия базы данных Справочника упражнений.  */
    static double                   db_version()                                { return 3.0; }
};
//------------------------------------------------------------------------------
/**
 \struct eg_creater
 \brief Модуля констант "Создателя" справочников упражнений.
*/
struct eg_creater
{
    /** \brief Полное название модуля eg_creater.  */
    static QString                  name_full()                                 { return "Exercises Guide Creater v.3.0"; }
    /** \brief Только  название модуля eg_creater для работы в командной строке.  */
    static QString                  name_console()                              { return "egc"; }
};
//------------------------------------------------------------------------------
        }; // end namespace module
    }; // end namespace consts
}; // end namespace go
//------------------------------------------------------------------------------
struct const_libs
{
    static QString                  st001ng()                                   { return QCoreApplication::applicationDirPath () + "/St001ng"; }
    static QString                  f12nt()                                     { return QCoreApplication::applicationDirPath () + "/f12nt"; }
    static QString                  mb1810()                                    { return QCoreApplication::applicationDirPath () + "/mb1810"; }
    static QString                  in1256t()                                   { return QCoreApplication::applicationDirPath () + "/In1256t"; }
};
//------------------------------------------------------------------------------
struct const_instance_libs
{
    static QString                  da1128()                                    { return QCoreApplication::applicationDirPath () + "/da1128"; }
    static QString                  di1245()                                    { return QCoreApplication::applicationDirPath () + "/di1245"; }
    static QString                  ldmdmp26()                                  { return QCoreApplication::applicationDirPath () + "/ldmdmp26"; }
    static QString                  ldmdpl1223()                                { return QCoreApplication::applicationDirPath () + "/ldmdpl1223"; }
    static QString                  ldmdpr41()                                  { return QCoreApplication::applicationDirPath () + "/ldmdpr41"; }
    static QString                  ldpi99()                                    { return QCoreApplication::applicationDirPath () + "/ldpi99"; }
    static QString                  ldrh300()                                   { return QCoreApplication::applicationDirPath () + "/ldrh300"; }
    static QString                  ldrhd740()                                  { return QCoreApplication::applicationDirPath () + "/ldrhd740"; }
    static QString                  ldrhp022()                                  { return QCoreApplication::applicationDirPath () + "/ldrhp022"; }
    static QString                  tw1313()                                    { return QCoreApplication::applicationDirPath () + "/tw1313"; }
    static QString                  ldmhwd0046()                                { return QCoreApplication::applicationDirPath () + "/ldmhwd0046"; }
};
//------------------------------------------------------------------------------
struct const_paths
{
    static QString                  proportion()                                { return QCoreApplication::applicationDirPath() + QString(QLatin1String("%1Resource%1Proportion%1")).arg(QDir::separator()); }
    static QString                  translation()                               { return QCoreApplication::applicationDirPath() + QString(QLatin1String("%1Resource%1Language%1")).arg(QDir::separator()); }
    static QString                  exercises_guides()                          { return QCoreApplication::applicationDirPath() + QString(QLatin1String("%1Resource%1ExercisesGuides%1")).arg(QDir::separator()); }
    bool test()
    {
        Q_ASSERT(QDir().exists(proportion()));
        Q_ASSERT(QDir().exists(translation()));
        Q_ASSERT(QDir().exists(exercises_guides()));
        return true;
    }
};
//------------------------------------------------------------------------------
struct const_exp
{
    static QString                  proportion()                                { return "lp"; }
    static QString                  bd_settings()                               { return "*.lsdb *.lsdb2 *.lsdb3"; }
    static QString                  bd_base()                                   { return "*.ldb *.ldb2 *.ldb3"; }
    static QString                  bd_exercise_guide()                         { return "*.legdb *.legdb2 *.legdb3"; }
    static QString                  translation()                               { return "*.qm"; }
};
//------------------------------------------------------------------------------
struct const_file_default
{
    static QString                  bd_exercise_guide()                         { return QCoreApplication::applicationDirPath() + "/exercises.legdb"; }
    static QString                  bd_settings()                               { return QCoreApplication::applicationDirPath() + "/settings.lsdb"; }
    static QString                  bd_base()                                   { return QCoreApplication::applicationDirPath() + "/base.ldb"; }
};
//------------------------------------------------------------------------------
struct const_script
{
    static QString                  bd_exercise_guide()                         { return QLatin1String("bde54.scr"); }
    static QString                  bd_settings()                               { return QLatin1String("bds02.scr"); }
    static QString                  bd_base()                                   { return QLatin1String("bdb23.scr"); }
};
//------------------------------------------------------------------------------
struct const_test_file
{
    static QString                  bullet_section()                            { return "/home/dix/Projects/Lythos/Resources/Test/vBulletSection.HTM"; }
    static QString                  align_section()                             { return "/home/dix/Projects/Lythos/Resources/Test/vAlignSection.HTM"; }
    static QString                  style_section()                             { return "/home/dix/Projects/Lythos/Resources/Test/vStyleSection.HTM"; }
    static QString                  font_section()                              { return "/home/dix/Projects/Lythos/Resources/Test/vFontSection.HTM"; }
    static QString                  image_new_png()                             { return QString("/home/dix/ProjectsLythos//Resources/Test/new.png"); }
    static QString                  xml_marseilles()                            { return "/home/dix/Projects/Lythos/Resources/Test/Marseilles_R.lp"; }
};
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
    /** \namespace go::consts  */
    namespace consts {

        /** \namespace go::consts  */
    namespace test {
/**
 \struct eg_creater
 \brief Модуль тестовых файлов.
*/
struct eg_creater
{
    static QString                  one_line_bracket()                          { return "/home/dix/Projects/Lythos/Resources/Test/eg_creator/one_line_bracket.html"; }
    static QString                  no_end()                                    { return "/home/dix/Projects/Lythos/Resources/Test/eg_creator/not_end.html"; }
    static QString                  set_error()                                 { return "/home/dix/Projects/Lythos/Resources/Test/eg_creator/set_error.html"; }
    static QString                  set()                                       { return "/home/dix/Projects/Lythos/Resources/Test/eg_creator/set.html"; }
};
//------------------------------------------------------------------------------
        }; // end namespace test
    }; // end namespace consts
}; // end namespace go
//------------------------------------------------------------------------------
struct media_type
{
    enum { image =1, video =2, audio =3};
};
//------------------------------------------------------------------------------
struct const_settings
{
    static QString                  image()                                     { return "/image"; }
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define  GROUP_LICENCE  10
#define  SiNGLE_LICENCE  1
#define  LICENCE  GROUP_LICENCE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define OVERRIDE_CURSOR(_a) {QApplication::setOverrideCursor(Qt::WaitCursor); { _a; }  QApplication::restoreOverrideCursor();}
#define RESTORE_OVERRIDE_CURSOR(_a) { QApplication::restoreOverrideCursor(); _a; return false;}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//w = (sqrt(5) + 1)/2 = 1,6180339
#define SM(_a)  {QMessageBox msgBox; msgBox.setText(QObject::trUtf8(_a)); msgBox.exec();}
#define SMI(_a) {QMessageBox msgBox; msgBox.setText(QString::number(_a)); msgBox.exec();}
#define SMS(_a)  {QMessageBox msgBox; msgBox.setText(_a); msgBox.exec();}
//template <typename T> void SMC(T a) { QString Text; for (auto it =a.begin(); it !=a.end(); ++it)  Text += QString("%1. - %2%3").arg(std::distance(a.begin(), it) +1 ).arg(*it).arg("\n"); QMessageBox msgBox; msgBox.setText(Text); msgBox.exec();}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define TRUE_MESSAGE(_a)  {_a; return true;}
#define FALSE_MESSAGE(_a)  {_a; return false;}
#define VOID_MESSAGE(_a)  {_a; return;}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define CLASS_TEST(test, class_name) class_name.test();         qDebug() <<qPrintable( test.text(#class_name));
#define CLASS_TEST_REF(test, class_name) class_name->test();    qDebug() <<qPrintable( test.text(#class_name));
#define CLASS_TEST_STATIC(test, class_name) class_name::test(); qDebug() <<qPrintable( test.text(#class_name));
//------------------------------------------------------------------------------
#endif	/* _V_CONST_H_ */

