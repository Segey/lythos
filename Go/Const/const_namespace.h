// -*- C++ -*-
/* 
 * File:   const_namespace.h
 * Author: S.Panin
 *
 * Created on Сб июня 19 2010, 01:52:29
 */
//------------------------------------------------------------------------------
#ifndef _V_CONST_NAMESPACE_H_
#define	_V_CONST_NAMESPACE_H_
//------------------------------------------------------------------------------
#define ANONYMOUS_BEGIN_NAMESPACE namespace {
#define ANONYMOUS_END_NAMESPACE };

#define GO_BEGIN_NAMESPACE namespace go{
#define GO_END_NAMESPACE };

#define PANEL_BEGIN_NAMESPACE namespace panel {
#define PANEL_END_NAMESPACE };

#define BOOK_BEGIN_NAMESPACE namespace book {
#define BOOK_END_NAMESPACE };

#define INSTANCE_BEGIN_NAMESPACE namespace instance {
#define INSTANCE_END_NAMESPACE };

#define DIALOG_BEGIN_NAMESPACE namespace dialog {
#define DIALOG_END_NAMESPACE };

#define PREFERENCES_BEGIN_NAMESPACE namespace preferences {
#define PREFERENCES_END_NAMESPACE };
//------------------------------- Go -------------------------------------------
#define GO_PANEL_BEGIN_NAMESPACE GO_BEGIN_NAMESPACE PANEL_BEGIN_NAMESPACE
#define GO_PANEL_END_NAMESPACE }; };
//----------------------------- Lythos -----------------------------------------
#define LYTHOS_BEGIN_NAMESPACE namespace lythos {
#define LYTHOS_END_NAMESPACE };
//------------------------------- EG -------------------------------------------
#define EG_BEGIN_NAMESPACE namespace veg {
#define EG_END_NAMESPACE };

#define PAGES_BEGIN_NAMESPACE namespace pages {
#define PAGES_END_NAMESPACE };

#define EG_BOOK_BEGIN_NAMESPACE EG_BEGIN_NAMESPACE  BOOK_BEGIN_NAMESPACE
#define EG_BOOK_END_NAMESPACE   }; };

#define EG_TOPICCHOOSER_BEGIN_NAMESPACE EG_BEGIN_NAMESPACE  namespace topic_chooser {
#define EG_TOPICCHOOSER_END_NAMESPACE }; };

#define EG_ADDBOOKMARK_BEGIN_NAMESPACE EG_BEGIN_NAMESPACE  namespace add_bookmark {
#define EG_ADDBOOKMARK_END_NAMESPACE }; };

#define EG_DIALOG_PREFERENCES_BEGIN_NAMESPACE EG_BEGIN_NAMESPACE DIALOG_BEGIN_NAMESPACE PREFERENCES_BEGIN_NAMESPACE
#define EG_DIALOG_PREFERENCES_END_NAMESPACE }; }; };

#define EG_DIALOG_PREFERENCES_PAGES_BEGIN_NAMESPACE EG_BEGIN_NAMESPACE  DIALOG_BEGIN_NAMESPACE  PREFERENCES_BEGIN_NAMESPACE  PAGES_BEGIN_NAMESPACE
#define EG_DIALOG_PREFERENCES_PAGES_END_NAMESPACE }; }; }; };
//------------------------------------------------------------------------------
#endif	/* _V_CONST_NAMESPACE_H_ */

