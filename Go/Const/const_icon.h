// -*- C++ -*-
/* 
 * File:   const_icons.h
 * Author: S.Panin
 *
 * Created on 4 Февраль 2010 г., 10:41
 */
//------------------------------------------------------------------------------
#ifndef _CONST_ICONS_H_
#define	_CONST_ICONS_H_
//------------------------------------------------------------------------------
#include <QIcon>
//------------------------------------------------------------------------------
struct const_icon_16
{
    static QIcon                new_()                                          { return QIcon(QString::fromUtf8(":/icons_16/new.png")); }
    static QIcon                open()                                          { return QIcon(QString::fromUtf8(":/icons_16/open.png")); }
    static QIcon                print()                                         { return QIcon(QString::fromUtf8(":/icons_16/print.png")); }
    static QIcon                program()                                       { return QIcon(QString::fromUtf8(":/icons_16/program.png")); }
    static QIcon                save()                                          { return QIcon(QString::fromUtf8(":/icons_16/save.png")); }
    static QIcon                save_as()                                       { return QIcon(QString::fromUtf8(":/icons_16/save_as.png")); }
    static QIcon                show_window()                                   { return QIcon(QString::fromUtf8(":/icons_16/show_window.png")); }
    static QIcon                thanks()                                        { return QIcon(QString::fromUtf8(":/icons_16/thanks.png")); }
};
//------------------------------------------------------------------------------
struct const_icon_32
{
    static QIcon                new_()                                          { return QIcon(QString::fromUtf8(":/icons_32/new.png")); }
    static QIcon                new_image()                                     { return QIcon(QString::fromUtf8(":/icons_32/new_image.png")); }
    static QIcon                open()                                          { return QIcon(QString::fromUtf8(":/icons_32/open.png")); }
    static QIcon                save()                                          { return QIcon(QString::fromUtf8(":/icons_32/save.png")); }
    static QIcon                save_as()                                       { return QIcon(QString::fromUtf8(":/icons_32/save_as.png")); }
    static QIcon                new_database()                                  { return QIcon(QString::fromUtf8(":/icons_32/new_database.png")); }
    static QIcon                open_database()                                 { return QIcon(QString::fromUtf8(":/icons_32/open_database.png")); }
    static QIcon                save_database()                                 { return QIcon(QString::fromUtf8(":/icons_32/save_database.png")); }
    static QIcon                save_as_database()                              { return QIcon(QString::fromUtf8(":/icons_32/save_as_database.png")); }
    static QIcon                cut()                                           { return QIcon(QString::fromUtf8(":/icons_32/cut.png")); }
    static QIcon                copy()                                          { return QIcon(QString::fromUtf8(":/icons_32/copy.png")); }
    static QIcon                paste()                                         { return QIcon(QString::fromUtf8(":/icons_32/paste.png")); }
    static QIcon                clear()                                         { return QIcon(QString::fromUtf8(":/icons_32/clear.png")); }
    static QIcon                clear_all()                                     { return QIcon(QString::fromUtf8(":/icons_32/clear_all.png")); }
    static QIcon                bold()                                          { return QIcon(QString::fromUtf8(":/icons_32/bold.png")); }
    static QIcon                italic()                                        { return QIcon(QString::fromUtf8(":/icons_32/italic.png")); }
    static QIcon                underline()                                     { return QIcon(QString::fromUtf8(":/icons_32/underline.png")); }
    static QIcon                left()                                          { return QIcon(QString::fromUtf8(":/icons_32/left.png")); }
    static QIcon                center()                                        { return QIcon(QString::fromUtf8(":/icons_32/center.png")); }
    static QIcon                right()                                         { return QIcon(QString::fromUtf8(":/icons_32/right.png")); }
    static QIcon                justify()                                       { return QIcon(QString::fromUtf8(":/icons_32/justify.png")); }
    static QIcon                redo()                                          { return QIcon(QString::fromUtf8(":/icons_32/redo.png")); }
    static QIcon                undo()                                          { return QIcon(QString::fromUtf8(":/icons_32/undo.png")); }
    static QIcon                print()                                         { return QIcon(QString::fromUtf8(":/icons_32/print.png")); }
    static QIcon                print_preview()                                 { return QIcon(QString::fromUtf8(":/icons_32/print_preview.png")); }
    static QIcon                exit()                                          { return QIcon(QString::fromUtf8(":/icons_32/exit.png")); }
    static QIcon                user()                                          { return QIcon(QString::fromUtf8(":/icons_32/user.png")); }
    static QIcon                about()                                         { return QIcon(QString::fromUtf8(":/icons_32/about.png")); }
    static QIcon                help()                                          { return QIcon(QString::fromUtf8(":/icons_32/help.png")); }
    static QIcon                color_red()                                     { return QIcon(QString::fromUtf8(":/icons_32/color_red.png")); }
    static QIcon                color_green()                                   { return QIcon(QString::fromUtf8(":/icons_32/color_green.png")); }
    static QIcon                color_blue()                                    { return QIcon(QString::fromUtf8(":/icons_32/color_blue.png")); }
    static QIcon                color_cyan()                                    { return QIcon(QString::fromUtf8(":/icons_32/color_cyan.png")); }
    static QIcon                color_magenta()                                 { return QIcon(QString::fromUtf8(":/icons_32/color_magenta.png")); }
    static QIcon                color_yellow()                                  { return QIcon(QString::fromUtf8(":/icons_32/color_yellow.png")); }
    static QIcon                color_gray()                                    { return QIcon(QString::fromUtf8(":/icons_32/color_gray.png")); }
    static QIcon                color_black()                                   { return QIcon(QString::fromUtf8(":/icons_32/color_black.png")); }
    static QIcon                chart_histogram()                               { return QIcon(QString::fromUtf8(":/icons_32/chart_histogram.png")); }
    static QIcon                chart_pie()                                     { return QIcon(QString::fromUtf8(":/icons_32/chart_pie.png")); }
    static QIcon                chart_text()                                    { return QIcon(QString::fromUtf8(":/icons_32/chart_text.png")); }

    static QIcon                add_group()                                     { return QIcon(QString::fromUtf8(":/icons_32/add_group.png")); }
    static QIcon                add_item()                                      { return QIcon(QString::fromUtf8(":/icons_32/add_item.png")); }
    static QIcon                erase_group()                                   { return QIcon(QString::fromUtf8(":/icons_32/erase_group.png")); }
    static QIcon                erase_item()                                    { return QIcon(QString::fromUtf8(":/icons_32/erase_item.png")); }
    static QIcon                edit()                                          { return QIcon(QString::fromUtf8(":/icons_32/edit.png")); }
};
//------------------------------------------------------------------------------
struct const_pixmap_32
{
    static QPixmap               new_()                                         { return QPixmap(QString::fromUtf8(":/icons_32/new.png")); }
    static QPixmap               edit()                                         { return QPixmap(QString::fromUtf8(":/icons_32/edit.png")); }
};
//------------------------------------------------------------------------------
struct const_pixmap_64
{
    static QPixmap              user()                                          { return QPixmap(QString::fromUtf8(":/icons_64/user.png")); }
    static QPixmap              photos()                                        { return QPixmap(QString::fromUtf8(":/icons_64/photos.png")); }
    static QPixmap              record_history()                                { return QPixmap(QString::fromUtf8(":/icons_64/record_history.png")); }
    static QPixmap              record_history_dialog()                         { return QPixmap(QString::fromUtf8(":/icons_64/record_history_dialog.png")); }
    static QPixmap              measurement_history()                           { return QPixmap(QString::fromUtf8(":/icons_64/measurement_history.png")); }
    static QPixmap              measurement_history_dialog()                    { return QPixmap(QString::fromUtf8(":/icons_64/measurement_history_dialog.png")); }
    static QPixmap              measurement_history_method()                    { return QPixmap(QString::fromUtf8(":/icons_64/measurement_history_method.png")); }
    static QPixmap              options()                                       { return QPixmap(QString::fromUtf8(":/icons_64/photos.png")); }
    static QPixmap              information()                                   { return QPixmap(QString::fromUtf8(":/icons_64/information.png")); }
};
//------------------------------------------------------------------------------
struct const_pixmap_panels
{
    static QPixmap              measurement_history()                           { return QPixmap(QString::fromUtf8(":/panels/measurement_history.png")); }
    static QPixmap              options()                                       { return QPixmap(QString::fromUtf8(":/panels/options.png")); }
    static QPixmap              personal_info()                                 { return QPixmap(QString::fromUtf8(":/panels/personal_info.png")); }
    static QPixmap              photogallery()                                  { return QPixmap(QString::fromUtf8(":/panels/photogallery.png")); }
    static QPixmap              record_history()                                { return QPixmap(QString::fromUtf8(":/panels/record_history.png")); }
};
//------------------------------------------------------------------------------
struct const_pixmap_introduction
{
    static QPixmap              top()                                           { return QPixmap(QString::fromUtf8(":/introduction/top.png")); }
    static QPixmap              bottom()                                        { return QPixmap(QString::fromUtf8(":/introduction/bottom.png")); }
    static QPixmap              about_background()                              { return QPixmap(QString::fromUtf8(":/introduction/about_background.png")); }
    static QPixmap              about_icon()                                    { return QPixmap(QString::fromUtf8(":/introduction/about_icon.png")); }
    static QPixmap              about_text()                                    { return QPixmap(QString::fromUtf8(":/introduction/about_text.png")); }
};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
struct const_pixmap_default
{
    static QPixmap            user()                                            { return QPixmap(QString::fromUtf8(":/default/user.png")); }
};
//------------------------------------------------------------------------------
#endif	/* _CONST_ICONS_H_ */

