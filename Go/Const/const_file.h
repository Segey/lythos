// -*- C++ -*-
/* 
 * File:   const_file.h
 * Author: S.Panin
 *
 * Created on Пт июня 4 2010, 14:45:21
 */
//------------------------------------------------------------------------------
#ifndef _V_CONST_FILE_H_
#define	_V_CONST_FILE_H_
//------------------------------------------------------------------------------
#include "const_ext.h"
//------------------------------------------------------------------------------
class const_file_image
{
    static QString                      create_text(const QString& ext)         { return QString("*.%1").arg(ext); }

public:
//    static QString                      bmp()                                   { return create_text(const_ext_image::bmp()); }
//    static QString                      png()                                   { return create_text(const_ext_image::png()); }
//    static QString                      jpg()                                   { return create_text(const_ext_image::jpg()); }
//    static QString                      jpeg()                                  { return create_text(const_ext_image::jpeg()); }
//    static QString                      jpe()                                   { return create_text(const_ext_image::jpe()); }
//    static QString                      gif()                                   { return create_text(const_ext_image::gif()); }
};
//------------------------------------------------------------------------------
#endif	/* _V_CONST_FILE_H_ */

