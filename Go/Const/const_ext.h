// -*- C++ -*-
/* 
 * File:   const_ext.h
 * Author: S.Panin
 *
 * Created on Пт июня 4 2010, 14:18:25
 */
//------------------------------------------------------------------------------
#ifndef _V_CONST_EXT_H_
#define	_V_CONST_EXT_H_
//------------------------------------------------------------------------------
#include "const_namespace.h"
//------------------------------------------------------------------------------
GO_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
struct const_ext
{
      static QString                      pdf()                                 { return QLatin1String(".pdf"); }
};
//------------------------------------------------------------------------------
struct const_ext_image
{
//    static QString                      bmp()                                   { return QString("BMP"); }
//    static QString                      png()                                   { return QString("PNG"); }
//    static QString                      jpg()                                   { return QString("JPG"); }
//    static QString                      jpeg()                                  { return QString("JPEG");}
//    static QString                      jpe()                                   { return QString("JPE"); }
//    static QString                      gif()                                   { return QString("GIF"); }
};
//------------------------------------------------------------------------------
struct const_ext_video
{
//    static QString                      avi()                                   { return QString("AVI"); }
};
//------------------------------------------------------------------------------
struct const_ext_audio
{
//    static QString                      mp3()                                   { return QString("MP3"); }
};
//------------------------------------------------------------------------------
GO_END_NAMESPACE
//------------------------------------------------------------------------------
#endif	/* _V_CONST_EXT_H_ */

