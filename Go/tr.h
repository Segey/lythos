/**
    \file   tr.h
    \brief  Файл tr.h

    \author S.Panin
    \date   Created on 2 Январь 2010 г., 13:28
 */
//------------------------------------------------------------------------------
#ifndef _TR_H_
#define	_TR_H_
//------------------------------------------------------------------------------
#include <QObject>
//------------------------------------------------------------------------------
/** \namespace vtr
 \note Depregated.
 */
namespace vtr
{
    struct Action       : public QObject { Q_OBJECT };
    struct Book         : public QObject { Q_OBJECT };
    struct Body         : public QObject { Q_OBJECT };
    struct BD           : public QObject { Q_OBJECT };
    struct Button       : public QObject { Q_OBJECT };
    struct Bullet       : public QObject { Q_OBJECT };
    struct Calc         : public QObject { Q_OBJECT };
    struct Copyright    : public QObject { Q_OBJECT };
    struct Color        : public QObject { Q_OBJECT };
    struct Chart        : public QObject { Q_OBJECT };
    struct Date         : public QObject { Q_OBJECT };
    struct Dialog       : public QObject { Q_OBJECT };
    struct Docs         : public QObject { Q_OBJECT };
    struct Image        : public QObject { Q_OBJECT };
    struct Editor       : public QObject { Q_OBJECT };
    struct Error        : public QObject { Q_OBJECT };
    struct Exercise     : public QObject { Q_OBJECT };
    struct Filter       : public QObject { Q_OBJECT };
    struct Language     : public QObject { Q_OBJECT };
    struct LD           : public QObject { Q_OBJECT };
    struct Panel        : public QObject { Q_OBJECT };
    struct Record       : public QObject { Q_OBJECT };
    struct Mea          : public QObject { Q_OBJECT };
    struct Method       : public QObject { Q_OBJECT };
    struct Message      : public QObject { Q_OBJECT };
    struct User         : public QObject { Q_OBJECT };
    struct Training     : public QObject { Q_OBJECT };
    struct XML          : public QObject { Q_OBJECT };
}
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::tr  */
    namespace tr {
//------------------------------------------------------------------------------
    struct Action       : public QObject { Q_OBJECT };
    struct Book         : public QObject { Q_OBJECT };
    struct Body         : public QObject { Q_OBJECT };
    struct BD           : public QObject { Q_OBJECT };
    struct Button       : public QObject { Q_OBJECT };
    struct Bullet       : public QObject { Q_OBJECT };
    struct Calc         : public QObject { Q_OBJECT };
    struct Copyright    : public QObject { Q_OBJECT };
    struct Color        : public QObject { Q_OBJECT };
    struct Chart        : public QObject { Q_OBJECT };
    struct Date         : public QObject { Q_OBJECT };
    struct Dialog       : public QObject { Q_OBJECT };
    struct Docs         : public QObject { Q_OBJECT };
    struct Image        : public QObject { Q_OBJECT };
    struct Editor       : public QObject { Q_OBJECT };
    struct Error        : public QObject { Q_OBJECT };
    struct Exercise     : public QObject { Q_OBJECT };
    struct Filter       : public QObject { Q_OBJECT };
    struct Language     : public QObject { Q_OBJECT };
    struct LD           : public QObject { Q_OBJECT };
    struct Panel        : public QObject { Q_OBJECT };
    struct Record       : public QObject { Q_OBJECT };
    struct Mea          : public QObject { Q_OBJECT };
    struct Method       : public QObject { Q_OBJECT };
    struct Message      : public QObject { Q_OBJECT };
    struct User         : public QObject { Q_OBJECT };
    struct Training     : public QObject { Q_OBJECT };
    struct XML          : public QObject { Q_OBJECT };
//------------------------------------------------------------------------------
    }; // end namespace go::tr
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _TR_H_ */

