/* 
 * File:   SuccessorsTab.h
 * Author: S.Panin
 *
 * Created on 2 Сентября 2009 г., 15:50
 */
//------------------------------------------------------------------------------
#ifndef _V_SUCCESSORS_TAB_H_
#define	_V_SUCCESSORS_TAB_H_
//------------------------------------------------------------------------------
#include "../go/noncopyable.h"
//------------------------------------------------------------------------------
class vSuccessorsTab : go::noncopyable
{
    typedef                             vSuccessorsTab                           class_name;
    
protected:
    virtual QWidget*                    do_toWidget()                           =0;
    virtual bool                        do_check()                              =0;
    virtual bool                        do_save()                               =0;
    virtual bool                        do_load()                               =0;

public:
    virtual                             ~vSuccessorsTab()                        {}
    QWidget*                            toWidget()                              {return do_toWidget();}
    bool                                check()                                 {return do_check();}
    bool                                save()                                  {return do_save();}
    bool                                load()                                  {return do_load();}
    virtual bool                        test()                                  =0;
};
//------------------------------------------------------------------------------
#endif	/* _V_SUCCESSORS_TAB_H_ */
