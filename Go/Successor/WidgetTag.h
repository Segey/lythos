// -*- C++ -*-
/* 
 * File:   WidgetTag.h
 * Author: S.Panin
 *
 * Created on 22 Февраль 2010 г., 23:30
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_TAG_H_
#define	_V_WIDGET_TAG_H_
//------------------------------------------------------------------------------
template<typename T, typename U = int>
class vWidgetTag : public T
{
    typedef                         T                                           inherited;
    U                                                                           tag_;

public:
    template<typename P> explicit   vWidgetTag(P* parent, const U& tag) :  inherited(parent), tag_(tag)  { }
    U                               get_tag()                                   { return tag_; }
    const U&                        get_tag() const                             { return tag_; }
    const U&                        get_ctag() const                            { return tag_; }
    void                            set_tag(const U& tag)                       { tag_ =tag; }
};
//------------------------------------------------------------------------------
#endif	/* _V_WIDGET_ACTION_H_ */

