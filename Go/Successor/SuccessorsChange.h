/* 
 * File:   SuccessorsChange.h
 * Author: S.Panin
 *
 * Created on 2 Сентября 2009 г., 15:50
 */
//------------------------------------------------------------------------------
#ifndef _V_SUCCESSORS_CHANGE_H_
#define	_V_SUCCESSORS_CHANGE_H_
//------------------------------------------------------------------------------
#include "SuccessorsTab.h"
//------------------------------------------------------------------------------
class vSuccessorsChange : public vSuccessorsTab
{
    typedef                             vSuccessorsTab                           inherited;
    typedef                             vSuccessorsChange                        class_name;
    
protected:
    virtual void                        do_change_state()                       =0;

public:
    virtual                             ~vSuccessorsChange()                     {}
    void                                change_state()                          {do_change_state();}
};
//------------------------------------------------------------------------------
#endif	/* _V_SUCCESSORS_CHANGE_H_ */
