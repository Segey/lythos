/*
 * File:   BDEraseWhereStrategy.h
 * Author: S.Panin
 *
 * Created on Ср июня 16 2010, 21:34:54
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_ERASE_WHERE_STRATEGY_H_
#define	_V_BD_ERASE_WHERE_STRATEGY_H_
//------------------------------------------------------------------------------
namespace vbd {
    //--------------------------------------------------------------------------
    namespace erase {
        //----------------------------------------------------------------------
        namespace strategy_where {
//------------------------------------------------------------------------------
class no
{
    typedef                     no                                              class_name;

protected:
    QString                     expression( const QString& table) const
    {
        return QString("DELETE FROM %1;").arg(table);
    }

    void                        add_wheres(QSqlQuery&)                          {}
};
//------------------------------------------------------------------------------
class yes
{
    typedef                     yes                                             class_name;

    typedef                     QMultiHash<QString,QVariant>                    where_type;

    where_type                                                                  map_where;

protected:
    QString                     expression(const QString& table) const
    {
        const QString names_where = vbd::function::names_where(map_where.keys());
        return QString("DELETE FROM %1 WHERE (%2);").arg(table).arg(names_where);
    }
    
    void                        add_wheres(QSqlQuery& query)
    {
        QList<QVariant> wheres =map_where.values();
        std::for_each(wheres.begin(), wheres.end(), boost::bind(&QSqlQuery::addBindValue, &query, _1, QSql::In));
    }

public:
    void                        add_where(const QString& text, const QVariant& value)   { map_where.insert(text,value); }
};
        //----------------------------------------------------------------------
        };
    //--------------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_ERASE_WHERE_STRATEGY_H_ */
