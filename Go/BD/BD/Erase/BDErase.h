/*
 * File:   BDErase.h
 * Author: S.Panin
 *
 * Created on Вт июня 8 2010, 15:16:05
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_ERASE_H_
#define	_V_BD_ERASE_H_
//------------------------------------------------------------------------------
#include "BDEraseWhereStrategy.h"
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
template<typename Z =erase::strategy_where::no, typename T =strategy::no>
class erase_t : private T, public Z
{
    typedef                     T                                               strategy_type;
    typedef                     Z                                               strategy_where;
    typedef                     erase_t<strategy_where, strategy_type>          class_type;

    QSqlDatabase                                                                database_;
    QString                                                                     table_;

public:
    explicit        erase_t(const QSqlDatabase& database, const QString& table) : database_(database), table_(table)                {}
    bool operator()()
    {
        QSqlQuery query(database_);
        query.prepare( strategy_where::expression(table_) );
        strategy_where::add_wheres(query);
        strategy_type::execute(query);
        return query.exec( );
    }
};
typedef erase_t<vbd::erase::strategy_where::no,     vbd::strategy::no>     erase_all;
typedef erase_t<vbd::erase::strategy_where::yes,    vbd::strategy::no>     erase_where;
//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
#endif	/* _V_BD_ERASE_H_ */
