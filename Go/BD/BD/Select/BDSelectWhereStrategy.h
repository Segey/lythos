/*
 * File:   BDSelectWhereStrategy.h
 * Author: S.Panin
 *
 * Created on Ср июня 16 2010, 09:59:50
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_SELECT_WHERE_STRATEGY_H_
#define	_V_BD_SELECT_WHERE_STRATEGY_H_
//------------------------------------------------------------------------------
namespace vbd {
    //--------------------------------------------------------------------------
    namespace select {
        //----------------------------------------------------------------------
        namespace strategy_where {
//------------------------------------------------------------------------------
class no
{
    typedef                     no                                              class_name;

protected:
    QString                     expression(const QList<QString>& list_value, const QString& table) const
    {
        const QString names = vbd::function::names_select(list_value);
        return QString("SELECT %2 FROM %1;").arg(table).arg(names);
    }

    void                        add_wheres(QSqlQuery&)                          {}
};
//------------------------------------------------------------------------------
class yes
{
    typedef                     yes                                             class_name;

    typedef                     QMultiHash<QString,QVariant>                    where_type;

    where_type                                                                  map_where;

protected:
    QString                     expression(const QList<QString>& list_value, const QString& table) const
    {
        const QString names = vbd::function::names_select(list_value);
        const QString names_where = vbd::function::names_where(map_where.keys());
        return QString("SELECT %2 FROM %1 WHERE (%3);").arg(table).arg(names).arg(names_where);
    }
    
    void                        add_wheres(QSqlQuery& query)
    {
        QList<QVariant> wheres =map_where.values();
        std::for_each(wheres.begin(), wheres.end(), boost::bind(&QSqlQuery::addBindValue, &query, _1, QSql::In));
    }

public:
    void                        add_where(const QString& text, const QVariant& value)   { map_where.insert(text,value); }
};
        //----------------------------------------------------------------------
        };
    //--------------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_SELECT_WHERE_STRATEGY_H_ */
