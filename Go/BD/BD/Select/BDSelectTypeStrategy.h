/*
 * File:   BDSelectTypeStrategy.h
 * Author: S.Panin
 *
 * Created on Ср июня 9 2010, 21:17:41
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_SELECT_TYPE_STRATEGY_H_
#define	_V_BD_SELECT_TYPE_STRATEGY_H_
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
    namespace select {
    //--------------------------------------------------------------------------
        namespace strategy_type {
//------------------------------------------------------------------------------
template<typename T>
class value
{
    typedef                     T                                               value_type;
    typedef                     value<value_type>                               class_name;

protected:
    typedef                     value_type                                      result_type;

    result_type                                                                 result_;

    void                        execute(const QSqlQuery& query, const QList<QString>& )      { result_ =query.value(0).value<value_type>();  }
};
//------------------------------------------------------------------------------
template<>
class value<QVariant>
{
    typedef                     QVariant                                        value_type;
    typedef                     value<QVariant>                                 class_name;

protected:
    typedef                     value_type                                      result_type;

    result_type                                                                 result_;

    void                        execute(const QSqlQuery& query, const QList<QString>& )      { result_ =query.value(0); }
};
//------------------------------------------------------------------------------
template<>
class value<QList<QVariant> >
{
    typedef                     QList<QVariant>                                 value_type;
    typedef                     value<value_type >                              class_name;

protected:
    typedef                     value_type                                      result_type;
    
    value_type                                                                  result_;

    void                        execute(QSqlQuery& query, const QList<QString>&)
    {
        result_.push_back(query.value(0));

        while(query.next())
            result_.push_back(query.value(0));
    }
};
//------------------------------------------------------------------------------
template<>
class value<QList<QString> >
{
    typedef                     QList<QString>                                  value_type;
    typedef                     value<value_type >                              class_name;

protected:
    typedef                     value_type                                      result_type;

    result_type                                                                 result_;

    void                        execute(QSqlQuery& query, const QList<QString>&)
    {
        result_.push_back(query.value(0).value<value_type::value_type >());

        while(query.next())
            result_.push_back(query.value(0).value<value_type::value_type > ());
    }
};
//------------------------------------------------------------------------------
        };
        //----------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_SELECT_TYPE_STRATEGY_H_ */
