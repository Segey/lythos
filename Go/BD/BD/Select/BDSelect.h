/*
 * File:   BDSelect.h
 * Author: S.Panin
 *
 * Created on Вт июня 8 2010, 23:55:25
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_SELECT_H_
#define	_V_BD_SELECT_H_
//------------------------------------------------------------------------------
#include "BDSelectTypeStrategy.h"
#include "BDSelectWhereStrategy.h"
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
template<typename T, typename Z =select::strategy_where::no, typename U =vbd::strategy::no>
class select_t :  private select::strategy_type::value<T>, private U, public Z
{
    typedef                     T                                               value_result;
    typedef                     Z                                               strategy_where;
    typedef                     U                                               strategy_type;
    typedef                     select::strategy_type::value<value_result>      strategy_result;
    typedef                     select_t<T, Z, U>                               class_type;
    typedef                     QList<QString>                                  value_type;

public:
    typedef                     typename strategy_result::result_type           result_type;

private:
    QSqlDatabase                                                                database_;
    QString                                                                     table_;
    value_type                                                                  list_value;

public:
    explicit            select_t(const QSqlDatabase& database, const QString& table) : database_(database), table_(table)  {  }
    void                add_value(const QString& text)                          { list_value.push_back(text); }
    const result_type&  result() const                                          { return strategy_result::result_; }
    bool operator()()
    {       
        QSqlQuery query(database_);
        query.prepare( strategy_where::expression(list_value, table_) );
        strategy_where::add_wheres(query);
        strategy_type::execute(query);
        if( false ==query.exec() || false==query.next() ) return false;
        strategy_result::execute(query,list_value);
        return true;
    }
};
using namespace select;
typedef select_t<QVariant,          strategy_where::yes,   strategy::no>    select_where_variant;
typedef select_t<int,               strategy_where::yes,   strategy::no>    select_where_int;
typedef select_t<QString,           strategy_where::yes,   strategy::no>    select_where_string;
//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
#endif	/* _V_BD_SELECT_H_ */
