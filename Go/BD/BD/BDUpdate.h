/*
 * File:   BDUpdate.h
 * Author: S.Panin
 *
 * Created on Вт июня 8 2010, 21:26:59
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_UPDATE_H_
#define	_V_BD_UPDATE_H_
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
template<typename T>
class update_t : private T
{
    typedef                     T                                               strategy_type;
    typedef                     update_t<strategy_type>                         class_type;
    typedef                     QMultiHash<QString,QVariant>                    value_type;

    QSqlDatabase                                                                database_;
    QString                                                                     table_;
    value_type                                                                  map_value;
    value_type                                                                  map_where;

    QString                     expression() const
    {
        const QString names = vbd::function::names_update(map_value.keys());
        const QString names_where = vbd::function::names_where(map_where.keys());
        return QString("UPDATE %1 SET %2 WHERE (%3);").arg(table_).arg(names).arg(names_where);
    }

public:
    explicit        update_t(const QSqlDatabase& database, const QString& table) : database_(database), table_(table)  {}
    void            add_value(const QString& text, const QVariant& value)       { map_value.insert(text,value); }
    void            add_where(const QString& text, const QVariant& value)       { map_where.insert(text,value); }
    bool operator()()
    {
        QList<QVariant> values =map_value.values();
        QList<QVariant> wheres =map_where.values();

        QSqlQuery query(database_);
        query.prepare( expression() );        
        std::for_each(values.begin(), values.end(), boost::bind(&QSqlQuery::addBindValue, &query, _1, QSql::In));
        std::for_each(wheres.begin(), wheres.end(), boost::bind(&QSqlQuery::addBindValue, &query, _1, QSql::In));
        strategy_type::execute(query);
        return query.exec();
    }
};
typedef update_t<vbd::strategy::no>     update;
//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
#endif	/* _V_BD_UPDATE_H_ */
