/*
 * File:   BDData.h
 * Author: S.Panin
 *
 * Created on Вт июня 8 2010, 09:45:26
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_DATA_H_
#define	_V_BD_DATA_H_
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
    namespace function {
//------------------------------------------------------------------------------
static QString names_insert(const QList<QString>& list)
{
    return QStringList(list).join(QString(", "));
}
static QString names_update(const QList<QString>& list)
{
    QString result = QStringList(list).join(QString("=? , "));
    return result += "=?";
}
static QString names_select(const QList<QString>& list)
{
    return names_insert(list);
}
static QString names_where(const QList<QString>& list)
{
     QString result =QStringList(list).join(QString("=? AND "));
     return result +="=?";
}
static QString questions(int count)
{
    QStringList result;
    for(int x = 0; x != count; ++x) result << QChar('?');
    return result.join(QChar(','));
}
//------------------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_DATA_H_ */
