/*
 * File:   BDInsert.h
 * Author: S.Panin
 *
 * Created on Пн июня 7 2010, 21:27:56
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_INSERT_H_
#define	_V_BD_INSERT_H_
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
template<typename T>
class insert_t : private T
{
    typedef                     T                                               strategy_type;
    typedef                     insert_t<strategy_type>                         class_type;
    typedef                     QMultiHash<QString,QVariant>                    value_type;

    QSqlDatabase                                                                database_;
    QString                                                                     table_;
    value_type                                                                  map_value;

    QString                     expression() const
    {
        const QString names = vbd::function::names_insert(map_value.keys());
        const QString questions = vbd::function::questions(map_value.size());
        return QString("INSERT INTO %1 (%2) VALUES (%3);").arg(table_).arg(names).arg(questions);
    }

public:
    explicit        insert_t(const QSqlDatabase& database, const QString& table) : database_(database), table_(table)                {}
    void            add_value(const QString& text, const QVariant& value)       { map_value.insert(text,value); }
    bool operator()()
    {
        QList<QVariant> list =map_value.values();

        QSqlQuery query(database_);
        query.prepare( expression() );        
        std::for_each(list.begin(), list.end(), boost::bind(&QSqlQuery::addBindValue, &query, _1, QSql::In));
        strategy_type::execute(query);
        return query.exec();
    }
};
typedef insert_t<vbd::strategy::no>     insert;
//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
#endif	/* _V_BD_INSERT_H_ */
