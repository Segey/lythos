/*
 * File:   BDStrategy.h
 * Author: S.Panin
 *
 * Created on Вт июня 8 2010, 22:38:59
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_STRATEGY_H_
#define	_V_BD_STRATEGY_H_
//------------------------------------------------------------------------------
namespace vbd {
//------------------------------------------------------------------------------
    namespace strategy {
//------------------------------------------------------------------------------
struct no
{
    typedef                     no                                              class_name;
    void                        execute(const QSqlQuery&)                       { }
};
//------------------------------------------------------------------------------
struct error
{
    typedef                     error                                           class_name;
    void                        execute(const QSqlQuery& query)                 { qDebug() << query.lastError(); }
};
//------------------------------------------------------------------------------
struct query
{
    typedef                     query                                           class_name;
    void                        execute(const QSqlQuery& query)                 { qDebug() << query.lastQuery(); }
};
//------------------------------------------------------------------------------
struct all_error
{
    typedef                     all_error                                       class_name;
    void                        execute(const QSqlQuery& query)                 { qDebug() << query.lastQuery(); qDebug() << query.lastError(); }
};
//------------------------------------------------------------------------------
    };
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_STRATEGY_H_ */
