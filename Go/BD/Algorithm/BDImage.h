/* 
 * File:   BDImage.h
 * Author: S.Panin
 *
 * Created on 31 Июль 2009 г., 14:07
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_IMAGE_H_
#define	_V_BD_IMAGE_H_
//------------------------------------------------------------------------------
#include "../../Algorithm/Image.h"
//------------------------------------------------------------------------------
namespace v_bd_image
{
inline bool add(const QSqlDatabase& database, const QString& inquiry, const QString& filename, const std::vector<QVariant>& value)
    {
        QSqlQuery query(database);
        query.prepare(inquiry);
        query.addBindValue( v_image::to_byte_array(filename));
        std::for_each(value.begin(), value.end(), boost::bind(&QSqlQuery::addBindValue, query, _1, QSql::In));
        return query.exec();
    }

inline bool add(const QSqlDatabase& database, const QString& inquiry, const QString& filename, const QVariant& value)
    {
        std::vector<QVariant> vec = { value };
        return add(database, inquiry, filename, vec);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_IMAGE_H_ */
