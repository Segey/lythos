/* 
 * File:   BD.h
 * Author: S.Panin
 *
 * Created on 31 Июль 2009 г., 14:07
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_H_
#define	_V_BD_H_
//------------------------------------------------------------------------------
namespace v_bd
{
inline bool exists(const QSqlDatabase& database, const QString& inquiry)
    {
        QSqlQuery query( database );
        query.prepare(inquiry);
        query.addBindValue(vMain::user().id());
        return query.exec() && query.next() ? !query.isNull(0) : false;
    }

inline bool exists(const QSqlDatabase& database, const QString& inquiry, const QVariant& first)
    {
        QSqlQuery query( database );
        query.prepare(inquiry);
        query.addBindValue(first);
        query.addBindValue(vMain::user().id());
        return query.exec() && query.next() ? !query.isNull(0) : false;
    }

inline bool exists(const QSqlDatabase& database, const QString& inquiry, const QVariant& first, const QVariant& second)
    {
        QSqlQuery query( database );
        query.prepare(inquiry);
        query.addBindValue(first);
        query.addBindValue(second);
        query.addBindValue(vMain::user().id());
        return query.exec() && query.next() ? !query.isNull(0) : false;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_BD_H_ */
