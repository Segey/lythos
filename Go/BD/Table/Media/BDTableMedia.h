/*
 * File:   BDTableMedia.h
 * Author: S.Panin
 *
 * Created on Вс июня 6 2010, 13:20:54
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_TABLE_MEDIA_H_
#define	_V_BD_TABLE_MEDIA_H_
//------------------------------------------------------------------------------
#include "../../../Algorithm/Image.h"
#include "../MediaType/BDTableMediaType.h"
//------------------------------------------------------------------------------
namespace vbd
{ // NAMESPACE VBD_BEGIN
//------------------------------------------------------------------------------
    namespace table
    { // NAMESPACE TABLE_BEGIN
    //--------------------------------------------------------------------------
        namespace media
        { // NAMESPACE MEDIA_BEGIN
        //----------------------------------------------------------------------
struct insert
{
    QString         file_;
    QString         name_;
    QString         description_;
    insert(const QString& file, const QString& name =QString(), const QString& description =QString()) : file_(file), name_(name), description_(description) {}
};
//------------------------------------------------------------------------------
struct select
{
    bool            result_;
    QByteArray      image_;
    QString         name_;
    QString         description_;
    select(bool result, const QByteArray& image =QByteArray(), const QString& name =QString(), const QString& description =QString())
            : result_(result), image_(image), name_(name), description_(description) {}
};
        //----------------------------------------------------------------------
        }; // NAMESPACE MEDIA_END
        
//------------------------------------------------------------------------------
class Media
{
    typedef                     Media                                           class_name;
    typedef                     vbd::table::MediaType::media_type_id            media_type_id;

public:
    typedef                     vbd::table::media::insert                       insert_type;
    typedef                     vbd::table::media::select                       select_type;
    typedef                     std::pair<bool,QString>                         select_name_type;
    typedef                     vbd::type::id                                   id_type;
    typedef                     id_type                                         media_type;

private:
    QSqlQuery                                                                   query_;

    static QString              table()                                         { return QString("Media"); }
    static QString              field_id()                                      { return QString("ID"); }
    static QString              field_media_type_id()                           { return QString("MediaTypeID"); }
    static QString              field_name()                                    { return QString("Name"); }
    static QString              field_description()                             { return QString("Description"); }
    static QString              field_hash()                                    { return QString("Hash"); }
    static QString              field_media()                                   { return QString("Media"); }


    media_type_id               get_media_type_id(const insert_type& record) const { return vbd::table::MediaType(query_).media_type(record.file_); }
    QString                     hash(const QString& image_filename);

    bool                        add(const insert_type& record);
    bool                        update(const insert_type& record, int id);
    bool                        insert(const insert_type& record);

public:
    explicit                    Media(const QSqlQuery& query) : query_(query) {}
    media_type                  media(const insert_type& record);
    id_type                     select_id(const insert_type& record);
    select_name_type            select_name(const id_type& id);
    select_type                 select(const id_type& id);
    bool test();
};
    //--------------------------------------------------------------------------
    }; // NAMESPACE TABLE_END
//------------------------------------------------------------------------------
}; // NAMESPACE VBD_END
//------------------------------------------------------------------------------
#endif	/* _V_EG_MEDIA_H_ */
