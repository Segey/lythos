/*
 * File:   BDTableMedia.cpp
 * Author: S.Panin
 *
 * Created on Вс июня 6 2010, 13:20:54
 */
//------------------------------------------------------------------------------
#include "BDTableMedia.h"
//------------------------------------------------------------------------------
QString vbd::table::Media::hash(const QString& image_filename)
{
        QFile file(image_filename);
        file.open(QIODevice::ReadOnly);
        QByteArray array_mid = file.read(2048);
        return QCryptographicHash::hash(array_mid, QCryptographicHash::Md5).toBase64();
}
//------------------------------------------------------------------------------
vbd::table::Media::media_type vbd::table::Media::media(const insert_type& record)
{
        const id_type type = select_id(record);
        if ( type.first ) return media_type(type);
        return insert(record) ? media_type(select_id(record)) : media_type(false,0);
}
//------------------------------------------------------------------------------
vbd::table::Media::id_type vbd::table::Media::select_id(const insert_type& record)
{
        vbd::eg<vbd::select_where_int > select(table());
        select.add_value(field_id());
        select.add_where(field_name(),          record.name_);
        select.add_where(field_hash(),          hash(record.file_));
        return select() ? id_type(true, select.result()) : id_type(false,0);
}
//------------------------------------------------------------------------------
vbd::table::Media::select_type vbd::table::Media::select(const id_type& id)
{
//        if(false ==id.first) return select_type(false);
//
//        typedef vbd::eg<vbd::select_where_variants> type_select;
//        type_select query(table());
//        query.add_value(field_media());
//        query.add_value(field_name());
//        query.add_value(field_description());
//        query.add_where(field_id(), id.second);
//        if ( false ==query() ) return select_type(false);
//        type_select::result_type result =query.result();
//        return select_type(true, result.front().toByteArray(), result.at(1).toString(), result.at(2).toString());
}
//------------------------------------------------------------------------------
vbd::table::Media::select_name_type vbd::table::Media::select_name(const id_type& id)
{
        if(false ==id.first) return select_name_type(false,QString());

        vbd::eg<vbd::select_where_variant> query(table());
        query.add_value(field_name());
        query.add_where(field_id(), id.second);
        if ( false ==query() ) return select_name_type(false,QString());
        return select_name_type(true, query.result().toString());
}
//------------------------------------------------------------------------------
bool vbd::table::Media::add(const insert_type& record)
{
        const id_type type = select_id(record);
        return type.first ? update(record, type.second) : insert(record);
}
//------------------------------------------------------------------------------
bool vbd::table::Media::insert(const insert_type& record)
{
        const media_type_id type = get_media_type_id(record);
        if( false ==type.first ) return false;

        vbd::eg<vbd::insert> insert(table());
        insert.add_value(field_media_type_id(), type.second);
        insert.add_value(field_name(),          record.name_);
        insert.add_value(field_description(),   record.description_);
        insert.add_value(field_hash(),          hash(record.file_));
        insert.add_value(field_media(),         v_image::to_byte_array(record.file_));
        return insert();
}
//------------------------------------------------------------------------------
bool vbd::table::Media::update(const insert_type& record, int id)
{       
        const media_type_id type = get_media_type_id(record);
        if(false == type.first) return false;

        vbd::eg<vbd::update > update(table());
        update.add_value(field_media_type_id(), type.second);
        update.add_value(field_name(),          record.name_);
        update.add_value(field_description(),   record.description_);
        update.add_value(field_hash(),          hash(record.file_));
        update.add_value(field_media(),         v_image::to_byte_array(record.file_));
        update.add_where(field_id(),            id);
        return update();
}
//------------------------------------------------------------------------------
bool vbd::table::Media::test()
{
        const QString file =QString(const_test_file::image_new_png());

        insert_type type =insert_type(file,QString("New Name"),QString("Full Description"));
        Q_ASSERT( add(type) );
        select_type result =select( select_id(type) );
        Q_ASSERT( result.result_);
        Q_ASSERT( result.name_          ==QString("New Name"));
        Q_ASSERT( result.description_   ==QString("Full Description") );

        type =insert_type(file,QString("Cool"),QString("D777escription"));
        Q_ASSERT( add(type) );
        result =select( select_id(type) );
        Q_ASSERT( result.result_);
        Q_ASSERT( result.name_          ==QString("Cool"));
        Q_ASSERT( result.description_   ==QString("D777escription") );

        id_type select1 =select_id(type);
        Q_ASSERT( select1.first );
        Q_ASSERT( select1.second );

        type =insert_type(file,QString("Boost"),QString("Dopa"));
        Q_ASSERT( update(type,select1.second) );
        result =select( select_id(type) );
        Q_ASSERT( result.result_);
        Q_ASSERT( result.name_          ==QString("Boost"));
        Q_ASSERT( result.description_   ==QString("Dopa") );

        vbd::eg<vbd::erase_all> bd( table() );
        return bd();
}
//------------------------------------------------------------------------------