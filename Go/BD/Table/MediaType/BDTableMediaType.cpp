/*
 * File:   BDTableMediaType.cpp
 * Author: S.Panin
 *
 * Created on Сб июня 5 2010, 22:41:29
 */
//------------------------------------------------------------------------------
#include "BDTableMediaType.h"
//------------------------------------------------------------------------------
int vbd::table::MediaType::image_type(const QString& ext)
{
        int type = 4;
        if      (vFile::is_ext_image(ext)) type = 1;
        else if (vFile::is_ext_audio(ext)) type = 2;
        else if (vFile::is_ext_video(ext)) type = 3;
        return type;
}
//------------------------------------------------------------------------------
bool vbd::table::MediaType::add(const record_type& record)
{
        const id_type type = select(record);
        return type.first ? update(record, type.second) : insert(record);
}
//------------------------------------------------------------------------------
bool vbd::table::MediaType::update(const record_type& record, int id)
{
        vbd::eg<vbd::update> update(bd_table());
        update.add_value(bd_field_type(), record.type_);
        update.add_value(bd_field_ext(), record.ext_);
        update.add_where(bd_field_id(), id);
        return update();
}
//------------------------------------------------------------------------------
vbd::table::MediaType::id_type  vbd::table::MediaType::select(const record_type& record)
{
        vbd::eg<vbd::select_where_int> select(bd_table());
        select.add_value(bd_field_id());
        select.add_where(bd_field_type(), record.type_);
        select.add_where(bd_field_ext(),  record.ext_);
        return select() ? id_type(true, select.result()) : id_type(false,0);
}
//------------------------------------------------------------------------------
bool vbd::table::MediaType::insert(const record_type& record)
{
        vbd::eg<vbd::insert> insert(bd_table());
        insert.add_value(bd_field_type(), record.type_);
        insert.add_value(bd_field_ext(),  record.ext_);
        return insert();
}
//------------------------------------------------------------------------------
vbd::table::MediaType::media_type_id vbd::table::MediaType::media_type(const QString& image)
{
        const id_type  type = select(image);
        if ( type.first ) return type;
        return insert(image) ? media_type_id(select(image)) : media_type_id(false,0);
}
//------------------------------------------------------------------------------
bool vbd::table::MediaType::test()
{
        typedef vbd::eg<vbd::select_t<QList<QVariant>, vbd::select::strategy_where::yes, vbd::strategy::no> >  select_query_type;

        const QString file =QString(const_test_file::image_new_png());
        record_type type =record(file);
        Q_ASSERT(type.ext_ == QString("PNG"));
        Q_ASSERT(type.type_ == 1);
        
        type =record_type(8365,QString("ffFFF4"));
        Q_ASSERT( insert(type) );

        id_type  select1 =select(type);
        Q_ASSERT( select1.first );
        Q_ASSERT( select1.second );

        type =record_type(3,QString("f8"));
        Q_ASSERT( update(type, select1.second) );
        Q_ASSERT( select1.first );
        Q_ASSERT( select1.second );
        
        select_query_type select(bd_table());
        select.add_value(bd_field_type());
        select.add_value(bd_field_ext());
        select.add_where(bd_field_id(), select1.second);
        Q_ASSERT( select() );
        select_query_type::result_type result =select.result();
        Q_ASSERT( result.front().toInt() ==3  );
        Q_ASSERT( result.back().toString() ==QString("f8") );
  
        return vbd::eg<vbd::erase_all> ( bd_table() ).operator()();
}
//------------------------------------------------------------------------------