/*
 * File:   BDTableMediaType.h
 * Author: S.Panin
 *
 * Created on Сб июня 5 2010, 22:41:29
 */
//------------------------------------------------------------------------------
#ifndef _V_BD_TABLE_MEDIA_TYPE_H_
#define	_V_BD_TABLE_MEDIA_TYPE_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/File.h"
#include "../../../Const/const_file.h"
#include "../../../../Go/BD/BD/EG/EGBD.h"
//------------------------------------------------------------------------------
namespace vbd
{ // NAMESPACE VBD_BEGIN
//------------------------------------------------------------------------------
    namespace table
    { // NAMESPACE TABLE_BEGIN
//------------------------------------------------------------------------------
namespace {
struct vRecord
{
    typedef                     vRecord                                         class_name;
    
    int             type_;
    QString         ext_;
    vRecord(int type, const QString& ext) :  type_(type), ext_(ext) {}
};
};
//------------------------------------------------------------------------------
class MediaType
{
    typedef                     MediaType                                       class_name;
    typedef                     vRecord                                         record_type;

public:
    typedef                     vbd::type::id                                   id_type;
    typedef                     id_type                                         media_type_id;

private:
    QSqlQuery                                                                   query_;

    static QString              bd_table()                                      { return QString("MediaType"); }
    static QString              bd_field_type()                                 { return QString("Type"); }
    static QString              bd_field_ext()                                  { return QString("Ext"); }
    static QString              bd_field_id()                                   { return QString("ID"); }

    static int                  image_type(const QString& ext);
    static record_type          record(const QString& image)                    { const QString ext=vFile::ext(image); return record_type(image_type(ext), ext); }


    bool                        add(const record_type& record);
    bool                        update(const record_type& record, int id);
    bool                        insert(const record_type& record);
    id_type                     select(const record_type& record);

public:
    explicit                    MediaType(const QSqlQuery& query) : query_(query) {}
    bool                        add(const QString& image)                       { return add(record(image));        }
    bool                        update(const QString& image, int id)            { return update(record(image),id);  }
    bool                        insert(const QString& image)                    { return insert(record(image));     }
    id_type                     select(const QString& image)                    { return select(record(image));     }
    media_type_id               media_type(const QString& image);
    bool test();
};
    //--------------------------------------------------------------------------
    }; // NAMESPACE TABLE_END
//------------------------------------------------------------------------------
}; // NAMESPACE VBD_END
//------------------------------------------------------------------------------
#endif	/* _V_BD_TABLE_MEDIA_TYPE_H_ */
