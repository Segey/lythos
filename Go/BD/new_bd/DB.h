/**
 \file  DB.h
 \brief Файл для работы с базой данных.

 \author S.Panin
 \date Created on 19 January 2011 г., 01:22
 */
//------------------------------------------------------------------------------
#ifndef _DB_H_
#define	_DB_H_
//------------------------------------------------------------------------------
#include "DBSelect.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/**
\brief Основной класс для работы с любой базой данных.
\param S - Стратегия сообщений об ошибках.
\see go::base_bd;
 */

template<typename S>
class DB
{
    typedef                 S                                                   strategy_type;
    typedef                 DB<strategy_type>                                   class_name;

public:
    typedef                 go::db::Select<S>                                   select_type;
private:
    select_type                                                                 select_;

public:
/**
\brief Конструктор.
\param file_name - Имя файла базы данных.
\param database_name - Имя базы данных.
*/
    explicit                DB(const QString& file_name, const QString& database_name) : select_(file_name, database_name) {}


/**
\brief Функция получения ссылки на объект извлечения данных Select.
\result select_type& - Ссылка на объект Select.
*/
    select_type&            select()                                            {return select_;}
};
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_H_ */
