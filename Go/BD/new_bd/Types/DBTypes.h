/**
    \file   DBTypes.h
    \brief  Все типы данных для работы с db.

    \author S. Panin
    \date   Created on 19 January 2011 г., 01:03
 */
//------------------------------------------------------------------------------
#ifndef _DB_TYPES_H_
#define	_DB_TYPES_H_
//------------------------------------------------------------------------------
#include "DBTypeOne.h"
#include "DBTypeState.h"
#include "DBTypeGeometry.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace db {
//------------------------------------------------------------------------------
typedef                 go::db::type::One<QString>                              OneString;
typedef                 go::db::type::FormState                                 FormState;
typedef                 go::db::type::FormGeometry                              FormGeometry;
//------------------------------------------------------------------------------
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_TYPES_H_ */