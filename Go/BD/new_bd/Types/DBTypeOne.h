/**
    \file   DBTypeOne.h
    \brief  Одинарный тип данных.

    \author S. Panin
    \date   Created on 16 January 2011 г., 22:27
 */
//------------------------------------------------------------------------------
#ifndef _DB_TYPE_ONE_H_
#define	_DB_TYPE_ONE_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace db {
        /** \namespace go::db::type  */
        namespace type {

/**
\brief Тип данных One используемый в db.
\param T -тип данных.
\note Одинарный тип данных. Извлекается только одно значение типа QVariant.
\see go::db::Select;
*/
template<typename T>
class One
{
    typedef                         T                                           type_name;
public:
    typedef                         One<type_name>                              class_name;

private:
     type_name                                                                  value_;

public:
/**
  \brief Конструктор
  \param value - Значение.
*/
explicit One(const type_name& value=type_name()) : value_(value) {}

/**
 \brief Функция возвращает извлеченные данные из db.
 \result QString& - Возвращаемые из db данные по ссылке.
 */
const type_name&                value() const                                   { return value_; }

/**
 \brief Функция добавляет данные.
*/
void                            add(const QVariant& text)                       { value_ = text.value<type_name>(); }
};
//------------------------------------------------------------------------------
        }; // end namespace go::db::type
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_TYPE_ONE_H_ */