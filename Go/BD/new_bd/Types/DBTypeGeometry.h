/**
    \file   DBTypeGeometry.h
    \brief  Тип данных 'Геометрия формы'.

    \author S. Panin
    \date   Created on 2 February 2011 г., 00:36
 */
//------------------------------------------------------------------------------
#ifndef _DB_TYPE_GEOMETRY_H_
#define	_DB_TYPE_GEOMETRY_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace db {
        /** \namespace go::db::type  */
        namespace type {

/**
 \code
go::EGApp::settings_select_type& select = go::EGApp::settings().select();

go::db::FormGeometry geometry(this);
select(QString("SELECT geometry FROM forms WHERE user_id =%1 AND name=%2").arg(EGApp::uid()).arg(form_name()),geometry);
if (false ==geometry.is_done())  setGeometry(built_type::Form::ideal_size(QDesktopWidget().size()*0.75));
 \endcode
*/
class FormGeometry
{
public:
    typedef                         QMainWindow                                 form_name;
    typedef                         FormGeometry                                class_name;
    typedef                         QByteArray                                  value_type;

private:
     form_name*                                                                 form_;
     bool                                                                       is_done_;

public:
/**
  \brief Конструктор
  \param value - Значение.
*/
explicit FormGeometry(form_name* form) : form_(form), is_done_(false)  {}

/**
 \brief Функция возвращает извлеченные данные из db.
 \result QString& - Возвращаемые из db данные по ссылке.
 */
const value_type                value() const                                   { return form_->saveGeometry().toBase64(); }
value_type                      value()                                         { return value(); }

bool                            is_done() const                                 {return is_done_;}

/**
 \brief Функция добавляет данные.
*/
void                            add(const QVariant& text)
{
    if ((is_done_ = (false == text.isNull()))) form_->restoreState(QByteArray::fromBase64(text.value<QByteArray>()));
}
};
//------------------------------------------------------------------------------
        }; // end namespace go::db::type
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_TYPE_GEOMETRY_H_ */