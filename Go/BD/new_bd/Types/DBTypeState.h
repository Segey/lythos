/**
    \file   DBTypeState.h
    \brief  Тип данных 'Состояние формы'.

    \author S. Panin
    \date   Created on 2 February 2011 г., 00:36
 */
//------------------------------------------------------------------------------
#ifndef _DB_TYPE_STATE_H_
#define	_DB_TYPE_STATE_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace db {
        /** \namespace go::db::type  */
        namespace type {

/**
\code
go::EGApp::settings_select_type& select = go::EGApp::settings().select();

go::db::FormState state(this);
select(QString("SELECT state FROM forms WHERE user_id =%1 AND name=%2").arg(EGApp::uid()).arg(form_name()),state);
\endcode
 */
class FormState
{
public:
    typedef                         QMainWindow                                 form_name;
    typedef                         FormState                                   class_name;
    typedef                         QByteArray                                  value_type;

private:
     form_name*                                                                 form_;

public:
/**
  \brief Конструктор
  \param form - Форма.
*/
explicit FormState(form_name* form) : form_(form) {}

/**
 \brief Функция возвращает извлеченные данные из db.
 \result QString& - Возвращаемые из db данные по ссылке.
 */
const value_type                value() const                                   { return form_->saveGeometry().toBase64(); }
value_type                      value()                                         { return value(); }

/**
 \brief Функция добавляет данные.
*/
void                            add(const QVariant& text)
{
    if (false == text.isNull()) form_->restoreState(QByteArray::fromBase64(text.value<QByteArray>()));
}
};
//------------------------------------------------------------------------------
        }; // end namespace go::db::type
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_TYPE_STATE_H_ */