/**
    \file   BD.h
    \brief  Основной файл для манипулирования данными с базой данных. // Deprecated use DB /доделать create_database;/

    \author S. Panin
    \date   Created on 19 December 2010 г., 01:24
 */
//------------------------------------------------------------------------------
#ifndef _H_BD_H_
#define	_H_BD_H_
//------------------------------------------------------------------------------
#include "base_bd.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/**
\brief Основной класс для манипулирования данными с базой данных.
\note Доработать для соответствия с локалями пользователей.

 \code
 1). #include "../Go/const.h"
     typedef  go::db::Source<go::db::source::EG_Creater>::class_name  source;
     go::DB<go::strategy::message::console> db (console.file(), source::database());
     db.database();

 2).if (go::DB<go::strategy::message::console>::create_database(source::list() ,file,source::database())) return true;
 \endcode
*/

template<typename S>
class DB : private base_bd<S>
{
        typedef                     base_bd<S>                                  inherited;
public:
        typedef                     DB                                          class_name;
        
private:
        bool                                                                    is_good_;
        
    public:
        using inherited::create_database;
        using inherited::database;

/**
  \brief Конструктор
  \param filename Имя файла базы данных
  \param database_name Имя базы данных
*/
        explicit DB(const QString& filename, const QString& database_name) : is_good_(false)
        {
            is_good_ = inherited::connect(filename, database_name);
        }
        
        /** \brief Виртуальный деструктор */
        virtual ~ DB()                                                          { inherited::disconnect(); }

        /** \brief Функция проверки лоступности базы данных для работы (Аналогична функции done() ) */
        operator bool()                                                         { return done(); }

        /** \brief Функция проверки лоступности базы данных для работы */
        bool done()                                                             { return is_good_; }
    };
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _H_BD_H_ */