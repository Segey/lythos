/**
 \file  Source.h
 \brief Основной файл для создания базы данных из скриптов Source.h.

 \author S.Panin
 \date Created on 20 December 2010 г., 23:06
 */
//------------------------------------------------------------------------------
#ifndef _BD_SOURCE_H_
#define	_BD_SOURCE_H_
//------------------------------------------------------------------------------
#include "../../const.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

    /** \namespace go::db  */
    namespace db {

/**
 \struct Source<T>
 \brief Основной класс для создания базы данных из скрипта передаваемого в качестве аргумента шаблона.
 \param T - класс с конкреными сырцами для создания бд.

 \note Шаблонный параметр структуры Source. Должен содержать статические функции-член.
 <pre>
 static QStringList source();
 static double  version();
 static QString database();
 </pre>
 
 \code
 typedef go::db::Source<go::db::source::EG_Creater>::class_name source;
 bool ok = DB::create_database(source::list() ,console.file().c_str(),source::database());
 \endcode
*/

template<typename T>
struct Source : public T
{
    typedef                         T                                           source_type;
    typedef                         Source<source_type>                         class_name;
    typedef                         QStringList                                 result_type;

/**
\brief Статическая функция возвращающая скрипты создания таблиц базы данных.
\result result_type - Список сырцов.

*/
    static result_type             list()
    {
        QStringList list = source_type::source();
        list.push_back("CREATE TABLE program_info ( program varchar(30) PRIMARY KEY NOT NULL UNIQUE, database varchar(30) NOT NULL UNIQUE, version real NOT NULL UNIQUE );");
        list.push_back(QString("INSERT INTO program_info (program, database, version) VALUES  ('%1','%2','%3')").arg(program::name()).arg(source_type::database()).arg(source_type::version()) );
        return list;
    }

};
//------------------------------------------------------------------------------
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _BD_SOURCE_H_ */
