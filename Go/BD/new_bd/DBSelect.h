/**
    \file   DBSelect.h
    \brief  Основной файл для извлечения данных из базы данных.

    \author S. Panin
    \date   Created on 16 January 2010 г., 21:00
 */
//------------------------------------------------------------------------------
#ifndef _DB_SELECT_H_
#define	_DB_SELECT_H_
//------------------------------------------------------------------------------
#include "base_bd.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::db  */
    namespace db {

/**
\brief Основной класс для извлечения данных из базы данных.
\param S - Стратегия сообщений об ошибках.
\see go::strategy::message::base;

 Example code:
 \code
 go::db::Select<db::type::One,strategy::message::none> db ("exercises.eg", go::consts::module::eg::db_name());
 if(db)
 {
    db("SELECT title FROM database_info");
    std::cout << db.value().value().toString().toStdString();
 }
 \endcode

 Интерфейс типа T:
 functions:
  - add(const QString& );

*/

template<typename S>
class Select : private base_bd<S>
{
        typedef                     S                                           message_strategy;
        typedef                     base_bd<message_strategy>                   inherited;

public:
        typedef                     Select<message_strategy>                    class_name;

private:
        bool                                                                    is_good_;

    public:
        using inherited::create_database;
        using inherited::database;

/**
  \brief Конструктор
  \param filename Имя файла базы данных
  \param database_name Имя базы данных
*/
explicit Select(const QString& filename, const QString& database_name) : is_good_(false)
{
    is_good_ = inherited::connect(filename, database_name);
}

/** \brief Виртуальный деструктор */
virtual                 ~ Select()                                              { inherited::disconnect(); }

/** \brief Функция проверки лоступности базы данных для работы (Аналогична функции done() ). */
operator                bool() const                                            { return done(); }

/** \brief Функция проверки лоступности базы данных для работы. */
bool                    done() const                                            { return is_good_; }

/**
 \brief Функтор. Выполнение запроса к db.
 \param request - Строка запроса к db.
 \param values  - Данные передаваемые по ссылки.
 \result bool   - Удачность извлечения данных.
 \note Строка в обязательном порядке должна соответствовать типу извлекаемых данных.
*/
template<typename T>
bool operator()(const QString& request, T& values)
{
    QSqlQuery query(request, inherited::database());

    for (uint x=0; query.next(); ++x)
         values.add(query.value(x));

    return true;
}
};
//------------------------------------------------------------------------------
    }; // end namespace go::db
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DB_SELECT_H_ */