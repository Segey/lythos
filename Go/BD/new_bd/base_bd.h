/**
 \file  base_bd.h
 \brief Файл для работы с базой данных

 \author S.Panin
 \date Created on 20 December 2010 г., 02:28
 */
//------------------------------------------------------------------------------
#ifndef _BASE_BD_H_
#define	_BASE_BD_H_
//------------------------------------------------------------------------------
#include <algorithm>
#include "../../Strategy/message/strategy_message.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/**
\class base_bd
\brief Основной класс для работы с базой данных.
\param S - Стратегия сообщений об ошибках.
\see go::strategy::message::base;
 */

template<typename S>
class base_bd
{
    typedef                 S                                                   strategy_type;
    typedef                 base_bd<strategy_type>                              class_name;

    QSqlDatabase                                                                bd_;
    QString                                                                     database_filename_;
    QString                                                                     database_name_;

    static bool             check_correct_bd(const QSqlDatabase& bd, const QString& database_name)
    {
        QSqlQuery query(bd);
        query.exec("SELECT program, database FROM program_info");
        query.next();
        return (query.value(0).toString() == QString(program::name()) && (query.value(1).toString() == database_name));
    }

    static bool             show_error_message(const QString& target_file)
    {
            QString str = vtr::Message::tr("Error while trying to create database file '%1' !").arg(target_file);
            strategy_type::show(str, strategy::message::base::critical);
            return false;
    }


public:
    virtual                 ~base_bd() =0;
/**
\brief Статическая функция проверки возможности подключения к базе данных SQLite
\param filename - Имя файла базы данных.
\param database_name - Имя базы данных.
\result bool Удалось подключиться к базе данных или нет.
\warning База данных в обязательном порядке должна содержать таблицу info  как минимум с двумя полями:\n
Первое поле - Имя программы возращаемое функцией program::name();\n
Второе поле - Имя базы данных database_name (т.е второй параметр передаваемый в функцию);
*/
    static bool             check_connect(const QString& filename, const QString& database_name)
    {
        
        if(false == QFile::exists(filename)) return false;

        bool b = false;
        {
                QSqlDatabase bd = QSqlDatabase::addDatabase("QSQLITE", "Other");
                bd.setDatabaseName(filename);
                b = !bd.open() ? false : check_correct_bd(bd, database_name);
        }
        QSqlDatabase::removeDatabase("Other");
        return b;
    }

/**
\brief Функция для соединения с базой данных SQLite
\param filename - Имя файла базы данных.
\param database_name - Имя базы данных.
\result bool - Удалось подключиться к базе данных или нет.
*/
    bool                    connect(const QString& filename, const QString& database_name)
    {
        if(false == class_name::check_connect(filename, database_name))
        {
            QString str = vtr::Message::tr("%1 could not open '%2' because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn't correctly decoded).").arg(program::name()).arg(filename);
            strategy_type::show(str, strategy::message::base::critical);
            return false;
        }

        disconnect();
        bd_ =QSqlDatabase::addDatabase("QSQLITE", database_name);
        bd_.setDatabaseName(filename);
        database_filename_ = filename;
        database_name_ = database_name;
        return bd_.open();
    }
    
/**
\brief Статическая функция для создания базы данных из скрипта. Скриптом служит обычный пустой файл с базой данных.
\param list - Список инструкций на языке SQL.
\param target_file - Имя создаваемого файла базы данных.
\param database_name - Имя базы данных.
\result bool - Удалось создать базу данных или нет.
 */
    static bool             create_database(const QStringList& list, const QString& target_file, const QString& database_name)
    {       
        QFile::remove(target_file);

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","Create");
        db.setDatabaseName(target_file);

        if( false == db.open()) return show_error_message(target_file);

        size_t done =0;
        QSqlQuery query(db);

        std::for_each(list.begin(), list.end(), [&](const QString& line)
        {
                done += !query.exec(line);
        });

        return !done && check_connect(target_file, database_name);
    }

/** \brief Функция для отсоединения от базы данных. */
    void                    disconnect()                                        { bd_.close();}

/**
\brief Функция получения ссылки на базу данных.
\param database_name - имя
\result QSqlDatabase - Ссылка на базу данных.
*/
    QSqlDatabase            database() const                                    {return bd_.database(database_name_);}

/**
\brief Функция получения ссылки на базу данных.
\param database_name - имя
\result QSqlDatabase - Ссылка на базу данных.
*/
    QSqlDatabase            database(const QString& database_name) const        {return bd_.database(database_name);}

/**
\brief Константная функция получения имени файла базы данных.
\result QString& - Имя файла.
*/
    const QString&          database_filename() const                           {return database_filename_;}

/**
\brief Константная функция определения существует ли соединение с базой данных.
\result  bool - существует соединение или нет.
*/
    bool                    is_connect() const                                  {return bd_.isValid();}
};
//------------------------------------------------------------------------------
template<typename S>
base_bd<S>::~base_bd()
{
}
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _BASE_BD_H_ */
