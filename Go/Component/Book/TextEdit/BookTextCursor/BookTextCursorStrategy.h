/* 
 * File:   BookTextCursor.h
 * Author: S.Panin
 *
 * Created on Ср июня 2 2010, 10:31:57
 */
//------------------------------------------------------------------------------
#ifndef _V_BOOK_TEXT_CURSOR_H_
#define	_V_BOOK_TEXT_CURSOR_H_
//------------------------------------------------------------------------------
class vBookTextCursorStrategy : private QTextCursor
{
    typedef                     QTextCursor                                     inherited;
    typedef                     vBookTextCursorStrategy                         class_name;

    void                        add_block()                                     { inherited::insertBlock(); }

public:
    explicit                    vBookTextCursorStrategy(QTextEdit* text_edit) : inherited( text_edit->document() )  {}
    void                        eding_begin()                                   { inherited::beginEditBlock(); }
    void                        eding_end()                                     { inherited::endEditBlock(); }
    void                        add_html(const QString& text)                   { inherited::insertHtml(text); }
    void                        add_block_html(const QString& text)             { add_block(); add_html(text); }
    void                        add_image(const QString& image_filename);
    void                        add_image_html(const QString& image_filename, const QString& text);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_BOOK_TEXT_CURSOR_H_ */
