/* 
 * File:   BookTextCursor.cpp
 * Author: S.Panin
 * 
 * Created on Ср июня 2 2010, 10:31:57
 */
//------------------------------------------------------------------------------
#include "BookTextCursor.h"
//------------------------------------------------------------------------------
void vBookTextCursor::add_image(const QString& image_filename)
{
       // inherited::insertImage(image_filename);
//        QTextImageFormat img;
//        img.setWidth(100);
//        img.setName(image_filename);
        QImage img(image_filename);
        img.setText(image_filename, image_filename);
        inherited::insertImage(img);
}
//------------------------------------------------------------------------------
void vBookTextCursor::add_image_html(const QString& image_filename, const QString& text)
{
        add_image(image_filename);
        add_html(text);
}
//------------------------------------------------------------------------------
bool vBookTextCursor::test()
{
//        Q_ASSERT(name_add_group_.isEmpty());
//        Q_ASSERT(description_add_group_.isEmpty());
//        Q_ASSERT(name_add_item_.isEmpty());
//        Q_ASSERT(description_add_item_.isEmpty());
//        Q_ASSERT(name_erase_group_.isEmpty());
//        Q_ASSERT(description_erase_group_.isEmpty());
//        Q_ASSERT(name_erase_item_.isEmpty());
//        Q_ASSERT(description_erase_item_.isEmpty());
//        Q_ASSERT(default_name_item_.isEmpty());
        return true;
}
//------------------------------------------------------------------------------