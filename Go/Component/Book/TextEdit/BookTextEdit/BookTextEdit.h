/* 
 * File:   BookTextEdit.h
 * Author: S.Panin
 *
 * Created on Чт июня 3 2010, 10:46:54
 */
//------------------------------------------------------------------------------
#ifndef _V_BOOK_TEXT_EDIT_H_
#define	_V_BOOK_TEXT_EDIT_H_
//------------------------------------------------------------------------------
template<typename T>
class vBookTextEdit : public T
{
    typedef                     T                                               inherited;
    typedef                     vBookTextEdit                                   class_name;

public:
    void                        refresh()                                       { inherited::refresh(); }
    bool test()                                                                 { return true; }
};
//------------------------------------------------------------------------------
#endif	/* _V_BOOK_TEXT_EDIT_H_ */
