/* 
 * File:   BookGroupItems.h
 * Author: S.Panin
 *
 * Created on 20 Май 2010 г., 14:13
 */
//------------------------------------------------------------------------------
#ifndef _V_BOOK_GROUP_ITEMS_H_
#define	_V_BOOK_GROUP_ITEMS_H_
//------------------------------------------------------------------------------
class vBookGroupItems : public QTreeWidgetItem
{
    typedef                     QTreeWidgetItem                                 inherited;
    typedef                     vBookGroupItems                                 class_name;

protected:
    virtual size_t              do_id() const                                   =0;

public:
    explicit                    vBookGroupItems(QTreeWidgetItem * parent);
    virtual                    ~vBookGroupItems()                               { qDebug() << "vBookGroupItems"; }

    virtual size_t              id() const                                      { return do_id(); }
};
//------------------------------------------------------------------------------
#endif	/* _V_BOOK_GROUP_ITEMS_H_ */
