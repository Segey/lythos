/* 
 * File:   BookItem.h
 * Author: S.Panin
 *
 * Created on 19 Май 2010 г., 15:57
 */
//------------------------------------------------------------------------------
#ifndef _V_BOOK_ITEM_H_
#define	_V_BOOK_ITEM_H_
//------------------------------------------------------------------------------
class vBookItem : public QTreeWidgetItem
{
    typedef                         QTreeWidgetItem                             inherited;
    typedef                         vBookItem                                   class_name;

    void                            instance_signals();
    virtual size_t                  do_id() const                               =0;
    virtual void                    do_load( QWidget* )                         =0;
    virtual void                    do_clear(  )                                =0;

public:
    explicit                        vBookItem(QTreeWidgetItem * parent);
    virtual                         ~vBookItem()                                {}

    size_t                          id() const                                  { return do_id(); }
    void                            load( QWidget* parent)                      { do_load(parent); }
    void                            clear( )                                    { do_clear(); }

};
//------------------------------------------------------------------------------
#endif	/* _V_BOOK_ITEM_H_ */
