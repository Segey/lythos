/* 
 * File:   EditListPanelInstance.h
 * Author: S.Panin
 *
 * Created on 25 Июнь 2010 г., 12:53
 */
//------------------------------------------------------------------------------
#ifndef _V_EDITLISTPANELINSTANCE_H_V_
#define	_V_EDITLISTPANELINSTANCE_H_V_
//------------------------------------------------------------------------------
#include "../../../Instance/Lay.h"
#include "../../../Instance/FormInstance/FormInstance.h"
//------------------------------------------------------------------------------
namespace go{ namespace panel{ namespace edit_list {
//------------------------------------------------------------------------------
struct Instance : private go::noncopyable
{
    typedef                     Instance                                        class_name;

    QLabel*                                                                     label_caption_;
    QLineEdit*                                                                  edit_;
    QListWidget*                                                                list_;

    void                        instance_widgets();
    void                        instance_buddies();
    void                        instance_images()                               {}
    QLayout*                    instance_top_layout()                           { return 0; }
    QLayout*                    instance_middle_layout();
    QLayout *                   instance_bottom_layout()                        { return 0; }
    void                        translate()                                     {}
    bool                        test();
} ;
//------------------------------------------------------------------------------
}; }; };
//------------------------------------------------------------------------------
#endif	/* _V_EDITLISTPANELINSTANCE_H_V_ */
