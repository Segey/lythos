/* 
 * File:   PanelImage.cpp
 * Author: S.Panin
 * 
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#include "EditListPanel.h"
//------------------------------------------------------------------------------
using namespace go::panel;
//------------------------------------------------------------------------------
EditList::EditList(QWidget* parent /*=0*/)
        : inherited(parent)
{
        go::Instance::form< inherited, instance > ( this, this );
        instance_signals();
}
//------------------------------------------------------------------------------
void EditList::instance_signals()
{
        connect(instance::list_, SIGNAL(currentTextChanged(const QString&)), SLOT(selected(const QString&)));
}
//------------------------------------------------------------------------------
void EditList::selected(const QString& text)
{
        instance::edit_->setText(text);
        emit(on_select(text));
}
//------------------------------------------------------------------------------
void EditList::set_index(const QString& text)
{
        const QList< QListWidgetItem* > list = instance::list_->findItems(text, Qt::MatchCaseSensitive);
        if( false == list.empty() ) instance::list_->setCurrentItem(list.front());
}
//------------------------------------------------------------------------------
bool EditList::test()
{
        instance::test();

        const QStringList labels = QStringList() <<QLatin1String("1") <<QLatin1String("2") <<QLatin1String("3") << QLatin1String("4");

        set_items(labels);
        set_index(QLatin1String("1"));
        Q_ASSERT(current_text() == QLatin1String("1"));
        set_index(QLatin1String("5"));
        Q_ASSERT(current_text() == QLatin1String("1"));
        set_index(QLatin1String("3"));
        Q_ASSERT(current_text() == QLatin1String("3"));
        return true;
}
//------------------------------------------------------------------------------