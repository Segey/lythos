/* 
 * File:   PanelEditList.h
 * Author: S.Panin
 *
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#ifndef _V_PANEL_EDIT_LIST_H_V_
#define	_V_PANEL_EDIT_LIST_H_V_
//------------------------------------------------------------------------------
#include "../../../Instance/Instance2.h"
#include "EditListPanelInstance.h"
//------------------------------------------------------------------------------
namespace go {  namespace panel {
//------------------------------------------------------------------------------
class EditList : public QWidget, private edit_list::Instance
{
Q_OBJECT

    typedef                     EditList                                        class_name;
    typedef                     QWidget                                         inherited;
    typedef                     edit_list::Instance                             instance;

    void                        instance_signals();
    template<typename T> void   set_items(const QStringList& labels, const T& index)
    {
        instance::list_->clear();
        instance::list_->addItems(labels);
        set_index (index);
    }

public slots:
    void                        selected(const QString& text);


signals:
    void                        on_select(const QString& text);

public:
    explicit                    EditList(QWidget* parent = 0);
    void                        set_items(const QStringList& labels, int index =0)          { set_items<int>(labels, index); }
    void                        set_items(const QStringList& labels, const QString& index)  { set_items<QString>(labels, index); }
    void                        set_caption(const QString& text)                { instance::label_caption_->setText(text); }
    void                        set_index (const QString& text);
    void                        set_index (int index)                           { instance::list_->setCurrentRow (index ); }
    QString                     current_text()                                  { return instance::edit_->text(); }
    bool test();
};
//------------------------------------------------------------------------------
}; };
//------------------------------------------------------------------------------
#endif	/* _V_PANEL_EDIT_LIST_H_V_ */
