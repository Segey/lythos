/* 
 * File:   EditListPanelInstance.cpp
 * Author: S.Panin
 * 
 * Created on 25 Июнь 2010 г., 12:53
 */
//------------------------------------------------------------------------------
#include "EditListPanelInstance.h"
//------------------------------------------------------------------------------
using namespace go::panel::edit_list;
//------------------------------------------------------------------------------
void Instance::instance_widgets()
{
        label_caption_  = new QLabel;
        edit_           = new QLineEdit;
        list_           = new QListWidget;

        instance_buddies();
}
//------------------------------------------------------------------------------
void Instance::instance_buddies()
{
        label_caption_->setBuddy(edit_);
}
//------------------------------------------------------------------------------
QLayout* Instance::instance_middle_layout()
{
        vLay lay;
        lay.add_caption_bevel_first(label_caption_);
        lay.add_unary(edit_, false);
        lay.add_unary(list_, false);
        return lay.clayout();
}
//------------------------------------------------------------------------------
bool Instance::test()
{
        Q_ASSERT(label_caption_);
        Q_ASSERT(edit_);
        Q_ASSERT(list_);

        return true;
}
