/* 
 * File:   PanelPresentation.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 11:07
 */
//------------------------------------------------------------------------------
#ifndef _V_PANEL_PRESENTATION_H_
#define	_V_PANEL_PRESENTATION_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Label.h"
//------------------------------------------------------------------------------
class vPanelPresentation : public QWidget
{
Q_OBJECT

    typedef                     QWidget                                         inherited;
    typedef                     vPanelPresentation                              class_name;

    QLabel*                                                                     icon_label_;
    QLabel*                                                                     caption_Label_;
    QLabel*                                                                     text_label_;

    void                        instance_layout();
    void                        instance_widgets();
    void                        instanceIconLabel();
    void                        instanceCaptionLabel();
    QFont                       instance_caption_font();
    void                        instanceTextLabel();
    void                        instance_form();

public:
    explicit                    vPanelPresentation(QWidget * parent = 0);
    virtual                     ~vPanelPresentation()                           {}
    void                        setCaption(const QString& text)                 {if (caption_Label_) return caption_Label_->setText(text); }
    QString                     caption() const                                 {return caption_Label_->text();}
    void                        setText(const QString& text)                    {if (text_label_) text_label_->setText(text);}
    QString                     text() const                                    {return text_label_->text();}
    void                        setPixmap(const QPixmap& pixmap)                {if (icon_label_)  icon_label_->setPixmap(pixmap);}
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_PRESENTATION_PANEL_H_ */
