/* 
 * File:   PanelPresentation.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 11:07
 */
//------------------------------------------------------------------------------
#include "PanelPresentation.h"
//------------------------------------------------------------------------------
vPanelPresentation::vPanelPresentation(QWidget* parent /*=0*/)
            : QWidget (parent), icon_label_(0), caption_Label_(0), text_label_(0)
{
        instance_widgets();
        instance_layout();
        instance_form();
}
//------------------------------------------------------------------------------
void vPanelPresentation::instance_widgets()
{   
        instanceIconLabel();
        instanceCaptionLabel();
        instanceTextLabel();
}
//------------------------------------------------------------------------------
void vPanelPresentation::instanceIconLabel()
{
        icon_label_ = new QLabel;
        icon_label_->setPixmap(const_pixmap_panels::personal_info());
        icon_label_->setFixedSize(icon_label_->sizeHint());
}
//------------------------------------------------------------------------------
void vPanelPresentation::instanceCaptionLabel()
{
        caption_Label_ = new QLabel;
        caption_Label_->setFont( instance_caption_font() );
}
//------------------------------------------------------------------------------
QFont vPanelPresentation::instance_caption_font()
{
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        return font;
}
//------------------------------------------------------------------------------
void vPanelPresentation::instanceTextLabel()
{
        text_label_ = new QLabel;
        vLabel::set_word_wrap(text_label_);
}
//------------------------------------------------------------------------------
void vPanelPresentation::instance_layout()
{
        QVBoxLayout* layout = new QVBoxLayout;
        layout->addStretch();
        layout->addWidget(caption_Label_);
        layout->addWidget(text_label_);

        vLay lay(new QGridLayout(this), 2);
        lay.add_binary_2_layout(icon_label_, layout , false);
}
//------------------------------------------------------------------------------
void vPanelPresentation::instance_form()
{
        inherited::setMaximumHeight(sizeHint().height());
        inherited::setMinimumHeight(inherited::maximumHeight());
}
//------------------------------------------------------------------------------
bool vPanelPresentation::test()
{
        Q_ASSERT(icon_label_);
        Q_ASSERT(caption_Label_);
        Q_ASSERT(text_label_);
        Q_ASSERT(!icon_label_->pixmap()->isNull());
        QFont font =instance_caption_font();
        Q_ASSERT(font.bold());
        Q_ASSERT(font.pointSize() ==16);
        return true;
}
//------------------------------------------------------------------------------