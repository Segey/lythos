/* 
 * File:   PanelRichEditInstance.h
 * Author: S.Panin
 *
 * Created on Вс мая 23 2010, 14:36:55
 */
//------------------------------------------------------------------------------
#ifndef _V_PANEL_RICH_EDIT_INSTANCE_H_
#define	_V_PANEL_RICH_EDIT_INSTANCE_H_
//------------------------------------------------------------------------------
#include "../../Section/Edit/SectionEdit.h"
#include "../../Section/Style/SectionStyle.h"
#include "../../Section/Alignment/SectionAlignment.h"
#include "../../Section/Font/SectionFont.h"
#include "../../Section/Bullet/SectionBullet.h"
//------------------------------------------------------------------------------
struct vPanelRichEditInstance
{
    typedef                     vPanelRichEditInstance                          class_name;

    QToolBar*                                                                   tool_bar_;
    QTextEdit*                                                                  text_edit_;

    vSectionEdit*                                                               section_edit_;
    vSectionStyle*                                                              section_style_;
    vSectionAlignment*                                                          section_align_;
    vSectionFont*                                                               section_font_;
    vSectionBullet*                                                             section_bullet_;

    void                        instance_widgets();
    void                        read_options();
    QLayout*                    instance_top_layout()                           { return 0; }
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout()                        { return 0; }
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate()                                     {}

    void                        instance_section();
    void                        instance_section_edit();
    void                        instance_section_style();
    void                        instance_section_alignment();
    void                        instance_section_font();
    void                        instance_section_bullet();
};
//------------------------------------------------------------------------------
#endif	/* _V_PANEL_RICH_EDIT_INSTANCE_H_ */
