/* 
 * File:   MainWindowRichEdit.h
 * Author: S.Panin
 *
 * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#ifndef _V_PANEL_RICH_EDIT_H_
#define	_V_PANEL_RICH_EDIT_H_
//------------------------------------------------------------------------------
#include "PanelRichEditInstance.h"
//------------------------------------------------------------------------------
class vPanelRichEdit :  public QWidget, private vPanelRichEditInstance
{
Q_OBJECT

    typedef                         QWidget                                     inherited;
    typedef                         vPanelRichEdit                              class_name;
    typedef                         vPanelRichEditInstance                      class_instance;

    QString                                                                     text_;
    
    void                            instance_signals();

private slots:
    void                            cursorPositionChanged();
    void                            currentCharFormatChanged(const QTextCharFormat& format);
    void                            condition_changed();

signals:
    void                            on_condition_changed();
    void                            on_condition_restored();

public:
    explicit                        vPanelRichEdit(QWidget* parent = 0);
    QString                         text_html() const                           { return text_edit_->toHtml(); }
    QString                         text() const                                { return text_edit_->toPlainText(); }
    void                            set_text(const QString& text)               { text_ =text; text_edit_->setPlainText(text); }
    void                            set_html(const QString& text)               { text_edit_->setHtml(text); text_ =text_edit_->toPlainText(); }
    bool                            is_condition_restored() const               { return text() == text_; }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_PANEL_RICH_EDIT_H_ */
