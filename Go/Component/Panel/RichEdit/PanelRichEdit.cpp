/* 
 * File:   MainWindowRichEdit.cpp
 * Author: S.Panin
 * 
 * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#include "PanelRichEdit.h"
//------------------------------------------------------------------------------
vPanelRichEdit::vPanelRichEdit(QWidget* parent /*=0*/)
                : QWidget(parent)
{      
//        v_instance::instance< inherited, class_instance > (this, this);
        instance_signals();     
}
//------------------------------------------------------------------------------
void vPanelRichEdit::instance_signals()
{
        connect(text_edit_,     SIGNAL(cursorPositionChanged()), SLOT(cursorPositionChanged()));
        connect(text_edit_,     SIGNAL(currentCharFormatChanged(const QTextCharFormat&)), SLOT(currentCharFormatChanged(const QTextCharFormat&)));
        connect(text_edit_,     SIGNAL(textChanged ()),          SLOT( condition_changed()));
}
//------------------------------------------------------------------------------
void vPanelRichEdit::cursorPositionChanged()
{
        section_align_->alignment_changed(text_edit_->alignment());
        section_bullet_->style_changed(text_edit_->textCursor().currentList());
}
//------------------------------------------------------------------------------
void vPanelRichEdit::currentCharFormatChanged(const QTextCharFormat& format)
{
        section_font_->font_changed(format.font());
        section_style_->font_changed(format.font());
        section_bullet_->font_changed(format.foreground().color());
}
//------------------------------------------------------------------------------
void vPanelRichEdit::condition_changed()
{
        emit( on_condition_changed() );
        if ( is_condition_restored() ) emit( on_condition_restored() );       
}
//------------------------------------------------------------------------------
bool vPanelRichEdit::test()
{
        Q_ASSERT(section_edit_  ->test());
        Q_ASSERT(section_style_ ->test());
        Q_ASSERT(section_align_ ->test());
        Q_ASSERT(section_font_  ->test());
        Q_ASSERT(section_bullet_->test());

        const QString example ="909";
        set_text(example);
        Q_ASSERT(example ==text() );
        Q_ASSERT( is_condition_restored() );

        const QString example2 ="9709";
        set_html(example2);
        Q_ASSERT(example2 ==text() );
        Q_ASSERT( is_condition_restored() );
        return true;
}
//------------------------------------------------------------------------------