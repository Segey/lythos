/* 
 * File:   PanelRichEditInstance.cpp
 * Author: S.Panin
 * 
 * Created on Вс мая 23 2010, 14:36:55
 */
//------------------------------------------------------------------------------
#include "PanelRichEditInstance.h"
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_widgets()
{
        tool_bar_  = new QToolBar;
        text_edit_ = new QTextEdit;      

        instance_section();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section()
{
        instance_section_edit();
        instance_section_style();
        instance_section_alignment();
        instance_section_font();
        instance_section_bullet();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section_edit()
{
        section_edit_ = new vSectionEdit(text_edit_);
        section_edit_->instance();
        tool_bar_->addActions(section_edit_->actions());
        tool_bar_->addSeparator();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section_style()
{
        section_style_ = new vSectionStyle(text_edit_);
        section_style_->instance();
        tool_bar_->addActions(section_style_->actions());
        tool_bar_->addSeparator();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section_alignment()
{
        if(QApplication::isLeftToRight()) section_align_ = new vSectionAlignmentLeft(text_edit_);
        else section_align_ = new vSectionAlignmentRight(text_edit_);
        section_align_->instance();
        tool_bar_->addActions(section_align_->actions());
        tool_bar_->addSeparator();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section_font()
{
        QToolBar* toolbar = new QToolBar;
        section_font_ = new vSectionFont(text_edit_);
        section_font_->instance_on_toolbar(toolbar);
        tool_bar_->addWidget(toolbar);
        tool_bar_->addSeparator();
}
//------------------------------------------------------------------------------
void vPanelRichEditInstance::instance_section_bullet()
{
        QToolBar* toolbar = new QToolBar;
        section_bullet_ = new vSectionBullet(text_edit_);
        section_bullet_->instance_on_toolbar(toolbar);
        tool_bar_->addWidget(toolbar);
}
//------------------------------------------------------------------------------
QLayout* vPanelRichEditInstance::instance_middle_layout()
{
        vLay lay(new QGridLayout);
        lay.add_unary(tool_bar_,false);
        lay.add_unary(text_edit_,false);
        return lay.clayout();
}
