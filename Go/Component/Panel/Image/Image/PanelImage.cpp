/* 
 * File:   PanelImage.cpp
 * Author: S.Panin
 * 
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#include "PanelImage.h"
//------------------------------------------------------------------------------
vPanelImage::vPanelImage(QWidget* parent /*=0*/)
                : inherited(parent),image_panel_(0),image_description_label_(0),image_date_label_(0),tag_(0)
{      
        instance_widgets();
        instance_layout();
        instance_signals();
        setWindowTitle("[*]");
}
//------------------------------------------------------------------------------
void vPanelImage::afterAddImage(const QString& file_name)
{
        QString text = QInputDialog::getText(0, program::name_full(), vtr::Dialog::tr ("New Name a Image:"), QLineEdit::Normal, QFileInfo(file_name).fileName());

        image_description_label_->setText(text);
        image_date_label_->setText(QDateTime::currentDateTime().toString());
        inherited::setWindowModified(true);
        visibleLabels(true);
        on_update_image_(text);
}
//------------------------------------------------------------------------------
void vPanelImage::afterClearImage()
{       
        image_description_label_->setText("");
        image_date_label_->setText("");
        setWindowModified(true);
        visibleLabels(false);
        on_image_clear_();
}
//------------------------------------------------------------------------------
void vPanelImage::instance_signals()
{
        image_panel_->connect_clear_image(boost::bind(&vPanelImage::afterClearImage, this));
        image_panel_->connect_update_image(boost::bind(&vPanelImage::afterAddImage, this, _1));
}
//------------------------------------------------------------------------------
void vPanelImage::instance_widgets()
{
        setAutoFillBackground(true);

        image_panel_                    = new vWidgetImage;
        image_description_label_        = new QLabel;
        image_date_label_               = new QLabel;
}
//------------------------------------------------------------------------------
void vPanelImage::instance_layout()
{
        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(image_panel_);
        layout->addWidget(image_description_label_);
        layout->addWidget(image_date_label_);
        visibleLabels(false);

        setMaximumSize(sizeHint());
}
//------------------------------------------------------------------------------
void vPanelImage::visibleLabels(bool value)
{
        image_description_label_->setVisible(value);
        image_date_label_->setVisible(value);
}
//------------------------------------------------------------------------------
void vPanelImage::set_data(const QString& date, const QString& description, const QPixmap& pixmap)
{
        if(!pixmap) return;

        image_date_label_->setText(date);
        image_description_label_->setText(description);
        image_panel_->set_pixmap(pixmap);
        visibleLabels(true);
}
//------------------------------------------------------------------------------
/*virtual*/ void vPanelImage::mousePressEvent(QMouseEvent* event)
{
        if(event->button() == Qt::RightButton) return;
                    if(image_panel_->image()->geometry().width() >= image_panel_->image()->sizeHint().width() ||
                image_panel_->image()->geometry().height() >= image_panel_->image()->sizeHint().height()) return;

        vDialogImageBlow *blow = new vDialogImageBlow(image_panel_->image(), this);
        blow->blow_up();
      //  QWidget::mousePressEvent(event);
}
//------------------------------------------------------------------------------
bool vPanelImage::test()
{
        Q_ASSERT(image_panel_);
        Q_ASSERT(image_description_label_);
        Q_ASSERT(image_date_label_);
        Q_ASSERT(image_description_label_->text().isEmpty());
        Q_ASSERT(image_date_label_->text().isEmpty());

        set_data("10", "Hello", 0);
        Q_ASSERT(image_description_label_->text().isEmpty());
        Q_ASSERT(image_date_label_->text().isEmpty());
        
        set_data("10", "Hello", QPixmap(const_test_file::image_new_png()));
      //  Q_ASSERT(image_date_label_->text() == "10");
    //    Q_ASSERT(image_description_label_->text() == "Hello");
        return true;
}