/* 
 * File:   PanelImage.h
 * Author: S.Panin
 *
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#ifndef _V_PANEL_IMAGE_H_
#define	_V_PANEL_IMAGE_H_
//------------------------------------------------------------------------------
#include "../../../Widget/Image/Image/WidgetImage.h"
#include "../../../Dialog/ImageBlow/DialogImageBlow.h"
#include "PanelImage.h"
//------------------------------------------------------------------------------
class vPanelImage : public QWidget
{
    typedef                     boost::signals2::signal<void ()>                signal_image_clear;
    typedef                     boost::signals2::signal<void (const QString&)>  signal_update_image;
    typedef                     QWidget                                         inherited;
    typedef                     vPanelImage                                     class_name;

private:
    signal_image_clear                                                          on_image_clear_;
    signal_update_image                                                         on_update_image_;
    vWidgetImage*                                                               image_panel_;
    QLabel*                                                                     image_description_label_;
    QLabel*                                                                     image_date_label_;
    int                                                                         tag_;

    void                        instance_signals();
    void                        instance_widgets();
    void                        instance_layout();
    void                        visibleLabels(bool value);
    void                        afterAddImage(const QString& file_name);
    void                        afterClearImage();

protected:
    virtual void                mousePressEvent(QMouseEvent* event);

public:
    explicit                    vPanelImage(QWidget* parent = 0);
    void                        connect_clear_image(signal_image_clear::slot_type slot) {on_image_clear_.connect(slot);}
    void                        connect_update_image(signal_update_image::slot_type slot) {on_update_image_.connect(slot);}
    int&                        tag()                                           {return tag_;}
    const int&                  tag() const                                     {return tag_;}
    const int&                  ctag() const                                    {return tag_;}
    QString                     get_image_date() const                          {return image_date_label_->text();}
    QString                     get_image_description() const                   {return image_description_label_->text();}
    const QString&              file_name() const                               {return image_panel_->file_name();}
    void                        set_data(const QString& date, const QString& description, const QPixmap& pixmap);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_PANEL_IMAGE_H_ */
