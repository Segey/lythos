/**
    \file   Console.h
    \brief  Файл Console.h

    \author dix75
    \date   Created on 27 Ноябрь 2010 г., 22:07
 */
//------------------------------------------------------------------------------
#ifndef _CONSOLE_H_
#define	_CONSOLE_H_
//------------------------------------------------------------------------------
#include "ConsoleKey.h"
#include <boost/program_options.hpp>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/**
 \class Console
 \brief Главный класс для работы с консолью

 Интерфейсный класс

 \code Пример:
        go::Console<go::ConsoleEGCreator> console (argc, argv);
        if ( false == console.execute()) return EXIT_FAILURE;
        if (console.is_test())  test(argc, argv);
 \endcode
        
 */

using namespace go::console;
using namespace boost::program_options;

template<typename T>
class Console : public T
{
    typedef                         T                                           inherited;
    typedef                         Console<inherited>                          class_name;
    
    const int                                                                   argc_;
    char**                                                                      argv_;
    bool                                                                        done_;
    bool                                                                        test_;

    static boost::program_options::options_description base()
    {
        options_description desc("Generic option");
        desc.add_options()
            (Key::author().first().c_str(),     Key::author().second().c_str())
            (Key::help().first().c_str(),       Key::help().second().c_str())
            (Key::usage().first().c_str(),      Key::usage().second().c_str())
            (Key::version().first().c_str(),    Key::version().second().c_str())
            (Key::test().first().c_str(),       Key::test().second().c_str())
             ;
        return desc;
    }

     /**
     \brief Получение данных о возможности работы программы далее.
    \note Функция принимает и возвращает одно и тоже булевое значение.<br />
     Такое решение было сделано с целью увеличения быстродействия и снижения кол-во кода.
    \result bool - Возвращаемое значение говорит о возможности/невозможности работы программы далее.
     */
    bool set_done(bool b)                                                       {done_ = b; return done(); }

    template<typename O>
    bool  display_help(O& ostr,const variables_map& vm, const options_description& desc)
    {
        if (!vm.count(KeyLarge::help().c_str())) return true;
        
        ostr << inherited::usage() << desc << std::endl;
        return false;
    }

    template<typename O>
    bool   display_author(O& ostr, const variables_map& vm)
    {
        if (!vm.count(KeyLarge::author().c_str())) return true;

        ostr << "Author: " << program::author().toStdString() <<std::endl;
        ostr <<std::endl;
        ostr << "mailto: " << program::mail().toStdString() <<std::endl;
        return false;
    }

    template<typename O>
    bool   display_version(O& ostr, const variables_map& vm)
    {
        if (!vm.count(KeyLarge::version().c_str())) return true;

        ostr << "program: "<< program::name_full().toStdString() <<std::endl;
        ostr << "module: " << inherited::module_name_full().toStdString() <<std::endl;
        ostr << std::endl;
        ostr << "mailto: " << program::mail().toStdString() <<std::endl;
        ostr << "web: " << program::url_homepage().toStdString() <<std::endl;
        return false;
    }

    template<typename O>
    bool   display_usage(O& ostr, const variables_map& vm)
    {
        if (!vm.count(KeyLarge::usage().c_str())) return true;

        ostr << inherited::usage_full();
        return false;
    }

    void   display_test(const variables_map& vm)
    {
        if (vm.count(KeyLarge::test().c_str())) test_ = true;
    }
    
public:
    /** \brief Конструктор
    \param argc - кол-во аргументов.
    \param argv - массив параметров.
    */
    Console(int argc, char* argv[]) : argc_(argc), argv_(argv), done_(true), test_(false) {}

    /**
    \brief Основная функция класса она должна вызываться всегда.
    \result bool - Возвращаемое значение говорит о возможности/невозможности работы программы далее.
    */
    bool execute()
    {
        try {
        options_description desc = class_name::base(), all;
        all.add(desc).add(inherited::help());

        variables_map vm;
        store(parse_command_line(argc_, argv_, all), vm);
        notify(vm);

        if (false == display_help(std::cout, vm, all)) return set_done(false);
        if (false == display_author(std::cout, vm)) return set_done(false);
        if (false == display_version(std::cout, vm)) return set_done(false);
        if (false == display_usage(std::cout, vm)) return set_done(false);
        display_test(vm);
        if (false == inherited::execute(vm)) return set_done(false);

         return set_done(true);
        }
        catch(std::exception& e)
        {
            std::cerr << "error: " << e.what() << "\n";
            return set_done(false);
        }
        catch(...)
        {
            std::cerr << "Exception of unknown type!\n";
            return set_done(false);
        }
    }

    /**
    \brief Получение данных о возможности работы программы далее.
    \result bool - Возвращаемое значение говорит о возможности/невозможности работы программы далее.
     */
    bool done() const                                                           { return done_;}
    /**
    \brief Получение данных о возможности вывода тестовой информации.
    \result bool - Возвращаемое значение говорит о возможности/невозможности вывода тестовой информации.
     */
    bool is_test() const                                                        { return test_;}

    /** \brief модульный тест. */
    bool test()
    {
        Q_ASSERT_X(inherited::test(),"inherited_test","Returned false should be true");

        const int argc=4;
        char *argv [] = {(char*)"iii", (char*)"--author", (char*)"--test", (char*)"-v"};

        variables_map vm;
        store(parse_command_line(argc, argv, class_name::base()), vm);
        notify(vm);

        std::stringstream s;
        display_author(s, vm);
        display_version(s, vm);
        s << std::ends;

        Q_ASSERT(s.str().find("Author: Sergey Panin") !=std::string::npos);
        Q_ASSERT(s.str().find("mailto: support@lythos.com") !=std::string::npos);

        Q_ASSERT(test_ ==false);
        display_test(vm);
        Q_ASSERT(test_);
        Q_ASSERT(s.str().find("program:") !=std::string::npos);
        Q_ASSERT(s.str().find(qPrintable(program::name_full())) !=std::string::npos);


        const int argc_2=2;
        char *argv_2 [] = {(char*)"iii", (char*)"--help"};

        variables_map vm_2;
        store(parse_command_line(argc_2, argv_2, class_name::base()), vm_2);
        notify(vm_2);

        std::stringstream s_2;
        display_help(s_2, vm_2, class_name::base());
        s << std::ends;

        Q_ASSERT(s_2.str().find("Generic option") !=std::string::npos);
        Q_ASSERT(s_2.str().find("Give a short usage message and exit") !=std::string::npos);
        Q_ASSERT(s_2.str().find("Display information about the author and exit") !=std::string::npos);
        Q_ASSERT(s_2.str().find("Display this information and exit") !=std::string::npos);
        Q_ASSERT(s_2.str().find("Give a short usage message and exit") !=std::string::npos);
        Q_ASSERT(s_2.str().find("Display test information and exit") !=std::string::npos);

        return true;
    }
};
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _CONSOLE_H_ */
