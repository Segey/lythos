/**
    \file   ConsoleKey.h
    \brief  Описание файла ConsoleKey.h.
    
    \author dix75
    \date   Created on 23 Ноябрь 2010 г., 0:21
 */
//------------------------------------------------------------------------------
#ifndef _CONSOLE_KEY_H_
#define	_CONSOLE_KEY_H_
//------------------------------------------------------------------------------
#include <boost/compressed_pair.hpp>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/** \namespace go::console  */
namespace console {

/**
 \struct KeyLarge

  Структура ключей c максимальной длиной для работы программы в консольном режиме.
 */
struct KeyLarge
{
/** \brief Ключ 'Автор программы' */
        static std::string author()                                             { return "author"; }
/** \brief Ключ 'Помощь' */
        static std::string help()                                               { return "help"; }
/** \brief Ключ 'Версия' */
        static std::string version()                                            { return "version"; }
/** \brief Ключ 'Использование' */
        static std::string usage()                                              { return "usage"; }
/** \brief Ключ 'Тестирование' */
        static std::string test()                                               { return "test"; }
};

struct Key
{
    typedef boost::compressed_pair<std::string, std::string>                    pair;
/** \brief Ключ 'Автор программы' */
        static pair author()                                                    { return pair("author,a","Display information about the author and exit"); }
/** \brief Ключ 'Помощь' */
        static pair help()                                                      { return pair("help,h","Display this information and exit"); }
/** \brief Ключ 'Версия' */
        static pair version()                                                   { return pair("version,v","Give a short usage message and exit"); }
/** \brief Ключ 'Использование' */
        static pair usage()                                                     { return pair("usage,u", "Display version information and exit"); }
/** \brief Ключ 'Тестирование' */
        static pair test()                                                      { return pair("test,t", "Display test information and exit"); }
};
//------------------------------------------------------------------------------
    } // end namespace console
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _CONSOLE_KEY_H_ */

