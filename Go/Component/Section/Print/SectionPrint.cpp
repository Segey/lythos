/* 
 * File:   SectionPrint.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionPrint.h"
//------------------------------------------------------------------------------
void vSectionPrint::instance()
{
        create_actions();
        create_toolbar();
        instance_signals();
        translate();
}
//------------------------------------------------------------------------------
void vSectionPrint::create_actions()
{
        print_act_ = vAction::createPrint(this);
        print_preview_act_ = vAction::createPrintPreview(this);
}
//------------------------------------------------------------------------------
void vSectionPrint::create_toolbar()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Print"));
        toolbar_->addAction(print_act_);
}
//------------------------------------------------------------------------------
void vSectionPrint::instance_signals()
{
        connect(print_act_, SIGNAL(triggered()), SLOT(file_print()));
        connect(print_preview_act_, SIGNAL(triggered()), SLOT(file_print_preview()));
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionPrint::actions()
{
        return QList<QAction*>() << print_act_ << print_preview_act_;
}
//------------------------------------------------------------------------------
void vSectionPrint::file_print()
{
        vInterfase::print(text_edit_, vtr::Button::tr("Print Document"));
}
//------------------------------------------------------------------------------
void vSectionPrint::file_print_preview()
{
        QPrinter printer(QPrinter::HighResolution);
        QPrintPreviewDialog preview(&printer, this);
        connect(&preview, SIGNAL(paintRequested(QPrinter *)), SLOT(print_preview(QPrinter *)));
        preview.exec();
}
//------------------------------------------------------------------------------
void vSectionPrint::print_preview(QPrinter *printer)
{
        text_edit_->print(printer);
}
//------------------------------------------------------------------------------
void vSectionPrint::translate()
{
        toolbar_->setWindowTitle(vtr::Dialog::tr("Print"));
}
//------------------------------------------------------------------------------
bool vSectionPrint::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(print_act_);
        Q_ASSERT(print_preview_act_);
        Q_ASSERT(print_act_->isEnabled());
        Q_ASSERT(!print_act_->icon().isNull());
        Q_ASSERT(!print_act_->text().isEmpty());
        Q_ASSERT(print_preview_act_->isEnabled());
        Q_ASSERT(!print_preview_act_->icon().isNull());
        Q_ASSERT(!print_preview_act_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------