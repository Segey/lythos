/**
    \file   SectionPrint.h
    \brief  Файл SectionPrint.h

    \author S.Panin
    \date   Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _SECTION_PRINT_H_
#define	_SECTION_PRINT_H_
//------------------------------------------------------------------------------
#include "../../../Algorithm/Interfase.h"
#include "../../../BuiltType/Action/Action.h"
//------------------------------------------------------------------------------
/** \brief Derpecated */
class vSectionPrint : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionPrint                               class_name;

    QTextEdit*                                                                  text_edit_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    print_act_;
    QAction*                                                                    print_preview_act_;

    void                            create_actions();
    void                            instance_signals();
    void                            create_toolbar();

private slots:
    void                            file_print();
    void                            file_print_preview();
    void                            print_preview(QPrinter* printer);

public:
    vSectionPrint(QTextEdit* text_edit, QWidget* parent =0) : inherited(parent), text_edit_(text_edit), toolbar_(0), print_act_(0), print_preview_act_(0)   {}
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::section  */
    namespace section {
//------------------------------------------------------------------------------
/** \brief Секция Print */
class Print : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         Print                                       class_name;

    QTextEdit*                                                                  text_edit_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    print_act_;
    QAction*                                                                    print_preview_act_;

    void                            create_actions()
    {
        print_act_ = vAction::createPrint(this);
        print_preview_act_ = vAction::createPrintPreview(this);
    }
    void                            instance_signals()
    {
        connect(print_act_, SIGNAL(triggered()), SLOT(file_print()));
        connect(print_preview_act_, SIGNAL(triggered()), SLOT(file_print_preview()));
    }
    void                            create_toolbar()
    {
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Print"));
        toolbar_->addAction(print_act_);
    }

private slots:
    void                            file_print()                                { vInterfase::print(text_edit_, go::tr::Button::tr("Print Document")); }
    void                            file_print_preview()
    {
        QPrinter printer(QPrinter::HighResolution);
        QPrintPreviewDialog preview(&printer, this);
        connect(&preview, SIGNAL(paintRequested(QPrinter *)), SLOT(print_preview(QPrinter *)));
        preview.exec();
    }
    void                            print_preview(QPrinter* printer)            { text_edit_->print(printer); }

public:
     /**
     \brief Конструктор.
     \param main_window - указатель на главное окно.
     \param parent - указатель на родителя. По умолчанию - отсутствует.
     */
    Print(QTextEdit* text_edit, QWidget* parent =0) : inherited(parent), text_edit_(text_edit), toolbar_(0), print_act_(0), print_preview_act_(0)   {}
    /** \brief Инстанцирование. */
    void                            instance()
    {
        create_actions();
        create_toolbar();
        instance_signals();
        translate();
    }
    QToolBar*                       toolbar() const                             {return toolbar_;}
        /**
     \brief Список всех действий (actions).
     \result QList<QAction*> - список actions.
    */
    QList<QAction*>                 actions()                                   { return QList<QAction*>() << print_act_ << print_preview_act_; }
    /** \brief Перевод. */
    void                            translate()                                 { toolbar_->setWindowTitle(go::tr::Dialog::tr("Print")); }
    /** \brief Тест. */
    bool                            test()
    {
        Q_ASSERT(text_edit_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(print_act_);
        Q_ASSERT(print_preview_act_);
        Q_ASSERT(print_act_->isEnabled());
        Q_ASSERT(!print_act_->icon().isNull());
        Q_ASSERT(!print_act_->text().isEmpty());
        Q_ASSERT(print_preview_act_->isEnabled());
        Q_ASSERT(!print_preview_act_->icon().isNull());
        Q_ASSERT(!print_preview_act_->text().isEmpty());
        return true;
    }
};
//------------------------------------------------------------------------------
    }; // end namespace section
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _SECTION_PRINT_H_ */
