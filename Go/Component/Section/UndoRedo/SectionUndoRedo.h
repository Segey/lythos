/* 
 * File:   SectionUndoRedo.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_UNDO_REDO_H_
#define	_V_SECTION_UNDO_REDO_H_
//------------------------------------------------------------------------------
class vSectionUndoRedo : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionUndoRedo                            class_name;

    QTextEdit*                                                                  text_edit_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    undo_act_;
    QAction*                                                                    redo_act_;

    void                            create();
    void                           instance_signals();
public:
    vSectionUndoRedo(QTextEdit* text_edit, QWidget* parent =0) : inherited(parent), text_edit_(text_edit), toolbar_(0), undo_act_(0), redo_act_(0)  {}
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_UNDO_REDO_H_ */
