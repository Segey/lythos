/* 
 * File:   SectionUndoRedo.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionUndoRedo.h"
//------------------------------------------------------------------------------
void vSectionUndoRedo::instance()
{
        create();
        instance_signals();
        translate();
}
//------------------------------------------------------------------------------
void vSectionUndoRedo::create()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Undo/Redo"));

        undo_act_ = vAction::createUndo(this);
        toolbar_->addAction(undo_act_);

        redo_act_ = vAction::createRedo(this);
        toolbar_->addAction(redo_act_);

        undo_act_->setEnabled(text_edit_->document()->isUndoAvailable());
        redo_act_->setEnabled(text_edit_->document()->isRedoAvailable());
}
//------------------------------------------------------------------------------
void vSectionUndoRedo::instance_signals()
{
        connect(text_edit_->document(), SIGNAL(undoAvailable(bool)), undo_act_, SLOT(setEnabled(bool)));
        connect(text_edit_->document(), SIGNAL(redoAvailable(bool)), redo_act_, SLOT(setEnabled(bool)));
        connect(undo_act_, SIGNAL(triggered()), text_edit_, SLOT(undo()));
        connect(redo_act_, SIGNAL(triggered()), text_edit_, SLOT(redo()));
}
//------------------------------------------------------------------------------
void vSectionUndoRedo::translate()
{
        toolbar_->setWindowTitle(vtr::Button::tr("Undo/Redo"));
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionUndoRedo::actions()
{
        return QList<QAction*>() << undo_act_ << redo_act_;
}
//------------------------------------------------------------------------------
bool vSectionUndoRedo::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(undo_act_);
        Q_ASSERT(redo_act_);
        Q_ASSERT(!undo_act_->icon().isNull());
        Q_ASSERT(!undo_act_->text().isEmpty());
        Q_ASSERT(!redo_act_->icon().isNull());
        Q_ASSERT(!redo_act_->text().isEmpty());
        text_edit_->document()->isUndoAvailable() ? Q_ASSERT(undo_act_->isEnabled()) : Q_ASSERT(!undo_act_->isEnabled());
        text_edit_->document()->isRedoAvailable() ? Q_ASSERT(redo_act_->isEnabled()) : Q_ASSERT(!redo_act_->isEnabled());
        return true;
}
//------------------------------------------------------------------------------