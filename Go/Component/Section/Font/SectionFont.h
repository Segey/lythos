/* 
 * File:   SectionFont.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_FONT_H_
#define	_V_SECTION_FONT_H_
//------------------------------------------------------------------------------
#include "../../../Test/TestAlgorithm.h"
#include "../../../Test/TestAlgorithm.h"
//------------------------------------------------------------------------------
class vSectionFont : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionFont                                class_name;

    QTextEdit*                                                                  text_edit_;
    QFontComboBox*                                                              combobox_font_;
    QComboBox*                                                                  combobox_font_size_;

    void                            create_widgets();
    void                            instance_widgets(QToolBar* toolbar);
    void                            instance_signals();
    void                            setFormat(const QTextCharFormat& format);

private slots:
    void                            text_family(const QString& font);
    void                            text_size(const QString& font);

public:
    explicit                        vSectionFont(QTextEdit* text_edit, QWidget* parent =0) : inherited(parent), text_edit_(text_edit), combobox_font_(0), combobox_font_size_(0)   {}
    void                            instance_on_toolbar(QToolBar* toolbar);
    QList<QWidget*>                 widgets() const;
    void                            font_changed(const QFont& font);
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_FONT_H_ */
