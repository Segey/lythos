/* 
 * File:   SectionFont.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionFont.h"
//------------------------------------------------------------------------------
void vSectionFont::instance_on_toolbar(QToolBar* toolbar)
{
        create_widgets();
        instance_widgets(toolbar);
        instance_signals();

        combobox_font_size_->setCurrentIndex(combobox_font_size_->findText(QString::number(QApplication::font().pointSize())));
}
//------------------------------------------------------------------------------
void vSectionFont::create_widgets()
{
        combobox_font_ = new QFontComboBox;

        combobox_font_size_ = new QComboBox;
        combobox_font_size_->setEditable(true);
        foreach(int size, QFontDatabase().standardSizes()) combobox_font_size_->addItem(QString::number(size));
}
//------------------------------------------------------------------------------
void vSectionFont::instance_widgets(QToolBar* toolbar)
{
        toolbar->setObjectName(QString::fromUtf8("Font"));
        toolbar->setWindowTitle(vtr::Button::tr("Font"));

        toolbar->addWidget(combobox_font_);
        toolbar->addWidget(combobox_font_size_);
}
//------------------------------------------------------------------------------
void vSectionFont::instance_signals()
{
        connect(combobox_font_, SIGNAL(activated(const QString &)), this, SLOT(text_family(const QString&)));
        connect(combobox_font_size_, SIGNAL(activated(const QString &)), this, SLOT(text_size(const QString&)));
}
//------------------------------------------------------------------------------
void vSectionFont::text_family(const QString& font)
{
        QTextCharFormat format;
        format.setFontFamily(font);
        setFormat(format);
}
//------------------------------------------------------------------------------
void vSectionFont::setFormat(const QTextCharFormat& format)
{
        QTextCursor cursor = text_edit_->textCursor();
        if(!cursor.hasSelection()) cursor.select(QTextCursor::WordUnderCursor);
        cursor.mergeCharFormat(format);
        text_edit_->mergeCurrentCharFormat(format);
}
//------------------------------------------------------------------------------
void vSectionFont::text_size(const QString& p)
{
        if(p.toFloat() <= 0) return;

        QTextCharFormat fmt;
        fmt.setFontPointSize(p.toFloat());
        setFormat(fmt);
}
//------------------------------------------------------------------------------
void vSectionFont::font_changed(const QFont& font)
{
        combobox_font_->setCurrentIndex(combobox_font_->findText(QFontInfo(font).family()));
        combobox_font_size_->setCurrentIndex(combobox_font_size_->findText(QString::number(font.pointSize())));
}
//------------------------------------------------------------------------------
QList<QWidget*> vSectionFont::widgets() const
{
        return QList<QWidget*>() << combobox_font_ << combobox_font_size_;
}
//------------------------------------------------------------------------------
bool vSectionFont::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(combobox_font_);
        Q_ASSERT(combobox_font_size_);
        Q_ASSERT(vTestAlgorithm::load_text(text_edit_, const_test_file::font_section()));
        text_edit_->moveCursor(QTextCursor::NextCharacter);
        const QString family =QFontInfo(text_edit_->currentCharFormat().font()).family();
        Q_ASSERT(QFontInfo(text_edit_->currentCharFormat().font()).family() == family);
        Q_ASSERT(text_edit_->currentCharFormat().font().pointSize() == 8);
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(QFontInfo(text_edit_->currentCharFormat().font()).family() == family);
        Q_ASSERT(text_edit_->currentCharFormat().font().pointSize() == 16);
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(QFontInfo(text_edit_->currentCharFormat().font()).family() != family);
        Q_ASSERT(text_edit_->currentCharFormat().font().pointSize() == 10);
        text_edit_->clear();
        return true;
}