/* 
 * File:   SectionFile.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionFile.h"
//------------------------------------------------------------------------------
vSectionFile::vSectionFile(QWidget* parent /*=0*/)
        : inherited(parent), toolbar_(0), new_act_(0), open_act_(0), save_act_(0), save_as_act_(0)
{      
}
//------------------------------------------------------------------------------
void vSectionFile::instance()
{
        create_actions();
        create_toolbar();        
        instance_signals();
        translate();
        save_act_->setEnabled(false);
}
//------------------------------------------------------------------------------
void vSectionFile::create_actions()
{
        new_act_        =vAction::createNew(this);
        open_act_       =vAction::createOpen(this);
        save_act_       =vAction::createSave(this);
        save_as_act_    =vAction::createSaveAs(this);
}
//------------------------------------------------------------------------------
void vSectionFile::create_toolbar()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("File"));
        toolbar_->addAction(new_act_);
        toolbar_->addAction(open_act_);
        toolbar_->addAction(save_act_);
}
//------------------------------------------------------------------------------
void vSectionFile::instance_signals()
{
        connect(new_act_, SIGNAL(triggered()), SLOT(file_new()));
        connect(open_act_, SIGNAL(triggered()), SLOT(file_open()));
        connect(save_act_, SIGNAL(triggered()), SLOT(file_save()));
        connect(save_as_act_, SIGNAL(triggered()), SLOT(file_save_as()));
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionFile::actions()
{
        return QList<QAction*>() << new_act_ << open_act_ << save_act_ << save_as_act_;
}
//------------------------------------------------------------------------------
void vSectionFile::file_new()
{
       if ( New() ) save_act_->setEnabled(true);
}
//------------------------------------------------------------------------------
void vSectionFile::file_open()
{
       if ( Open() ) save_act_->setEnabled(false);
}
//------------------------------------------------------------------------------
void vSectionFile::file_save()
{
        Save();
        save_act_->setEnabled(false);
}
//------------------------------------------------------------------------------
void vSectionFile::file_save_as()
{
        SaveAs();
}
//------------------------------------------------------------------------------
void vSectionFile::do_translate()
{
        toolbar_->setWindowTitle(vtr::Button::tr("File"));
}
//------------------------------------------------------------------------------