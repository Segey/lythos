/* 
 * File:   SectionFileText.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionFileText.h"
//------------------------------------------------------------------------------
bool vSectionFileText::New()
{
        text_edit_->clear();
        return true;
}
//------------------------------------------------------------------------------
bool vSectionFileText::Open()
{
        QString fileName = QFileDialog::getOpenFileName(this, vtr::Button::tr("Open File..."), vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<1>(), vDialog::getReadRichFilters());
        if(fileName.isEmpty() || !load_file(fileName)) return false;

        vSettings::set(settings_table(), settings::make(settings_key(), fileName));
        emit (on_window_modified(false));
        return true;
}
//------------------------------------------------------------------------------
void vSectionFileText::Save()
{
        emit (on_save(text_edit_->toHtml()));
        emit (on_window_modified(false));
}
//------------------------------------------------------------------------------
bool vSectionFileText::SaveAs()
{
        QString fileName = QFileDialog::getSaveFileName(this, vtr::Dialog::tr("Save as..."), vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<1>(), vDialog::getWriteRichFilters());
        if(fileName.isEmpty() || !save_file(fileName)) return false;

        vSettings::set(settings_table(), settings::make(settings_key(), fileName));
        emit (on_window_modified(false));
        return true;
}
//------------------------------------------------------------------------------
void vSectionFileText::text_changed()
{
        set_save_act_enabled(true);
}
//------------------------------------------------------------------------------
bool vSectionFileText::save_file(QString file)
{
        if(vFile::vFile::ext_exists(file, ".ODT") || vFile::ext_exists(file, ".HTM") || vFile::ext_exists(file, ".HTML")) return save_html(file);
        if(vFile::ext_exists(file, ".PDF")) return save_pdf(file);
        if(vFile::ext_exists(file, ".TXT")) return save_text(file);

        if(QFileInfo(file).suffix().isEmpty()) file.append(".txt");
        return save_text(file);
}
//------------------------------------------------------------------------------
bool vSectionFileText::save_html(const QString& fileName)
{
        QTextDocumentWriter writer(fileName);
        if(writer.write(text_edit_->document())) return true;

        QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Cannot write file '%1'.").arg(fileName));
        return false;
}
//------------------------------------------------------------------------------
bool vSectionFileText::save_pdf(const QString& fileName)
{
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        OVERRIDE_CURSOR(text_edit_->document()->print(&printer));
        return true;
}
//------------------------------------------------------------------------------
bool vSectionFileText::save_text(const QString& fileName)
{
        QFile file(fileName);
        if(!file.open(QFile::WriteOnly | QFile::Text))
        {
                QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Cannot write file %1:\n%2.").arg(fileName) .arg(file.errorString()));
                return false;
        }

        QTextStream out(&file);
        OVERRIDE_CURSOR(out << text_edit_->toPlainText());
        return true;
}
//------------------------------------------------------------------------------
void vSectionFileText::load(const QString& text)
{
        text_edit_->setHtml(text);
        emit (on_window_modified(false));
}
//------------------------------------------------------------------------------
bool vSectionFileText::load_file(const QString& file_name)
{
        QFile file(file_name);
        if(!file.open(QFile::ReadOnly)) return false;

        QByteArray data = file.readAll();
        QTextCodec* codec = Qt::codecForHtml(data);
        QString str = codec->toUnicode(data);

        if(Qt::mightBeRichText(str)) text_edit_->setHtml(str);
        else
        {
                str = QString::fromLocal8Bit(data);
                text_edit_->setPlainText(str);
        }
        
        emit (on_window_modified(false));
        return true;
}
//------------------------------------------------------------------------------
bool vSectionFileText::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(inherited::toolbar());
        const QString base_file_name = const_test_file::bullet_section();
        Q_ASSERT(load_file(base_file_name));
        Q_ASSERT(!text_edit_->document()->isEmpty());

        QString file_name("text.TXT");
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        file_name = "text.PDF";
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        file_name = "text.ODT";
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
 //       Q_ASSERT(QFileInfo(file_name).size() == 1315);
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        file_name = "text.HTM";
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
     //   Q_ASSERT(QFileInfo(file_name).size() == 2408);
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        file_name = "text.HTML";
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
   //     Q_ASSERT(QFileInfo(file_name).size() == 2408);
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        file_name = "text.HTA";
        Q_ASSERT(save_file(file_name));
        Q_ASSERT(QFile::exists(file_name));
     //   Q_ASSERT(QFileInfo(file_name).size() == 89);
        Q_ASSERT(QFile::remove(file_name));
        Q_ASSERT(!QFile::exists(file_name));

        text_edit_->clear();

        vMain::user().set_test_id();
        Q_ASSERT( vSettings::set(settings_table(), settings::make(settings_key(), QString("fil0e999Name"))));
        Q_ASSERT(vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<0>());
        Q_ASSERT(vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<1>() == "fil0e999Name");

        QSqlQuery query(vMain::settingsbd().database());
        return query.exec("DELETE FROM Path WHERE UserID =0");
}
//------------------------------------------------------------------------------