/* 
 * File:   SectionFileText.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_FILE_TEXT_H_
#define	_V_SECTION_FILE_TEXT_H_
//------------------------------------------------------------------------------
#include "../../../../BuiltType/File.h"
#include "../SectionFile.h"
//------------------------------------------------------------------------------
class vSectionFileText : public vSectionFile
{
Q_OBJECT

    typedef                         vSectionFile                                inherited;
    typedef                         vSectionFileText                            class_name;

    QTextEdit*                                                                  text_edit_;

    static QString                  settings_table()                            { return "Path"; }
    static QString                  settings_key()                              { return "DirText"; }
    bool                            save_file(QString fileName);
    bool                            save_html(const QString& fileName);
    bool                            save_pdf(const QString& fileName);
    bool                            save_text(const QString& fileName);
    
protected:
    virtual bool                    New();
    virtual bool                    Open();
   
    virtual bool                    SaveAs();
    virtual void                    do_translate()                              { inherited::do_translate(); }

signals:
    void                            on_save(const QString& text);
    void                            on_window_modified(bool);

public:
    vSectionFileText( QTextEdit* text_edit) :  inherited(0) , text_edit_(text_edit)  {}
    void                            text_changed();
    void                            load(const QString& text);    
    bool                            load_file(const QString& file_name);
    bool                            test();
    virtual void                    Save();
    
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_FILE_TEXT_H_ */
