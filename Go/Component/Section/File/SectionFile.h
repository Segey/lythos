/* 
 * File:   SectionFile.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_FILE_H_
#define	_V_SECTION_FILE_H_
//------------------------------------------------------------------------------
class vSectionFile : public QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionFile                                class_name;

    QToolBar*                                                                   toolbar_;
    QAction*                                                                    new_act_;
    QAction*                                                                    open_act_;
    QAction*                                                                    save_act_;
    QAction*                                                                    save_as_act_;

    void                            create_actions();
    void                            instance_signals();
    void                            create_toolbar();

private slots:
    void                            file_new();
    void                            file_open();
    void                            file_save();
    void                            file_save_as();

protected:
    virtual bool                    New() =0;
    virtual bool                    Open() =0;
    virtual void                    Save() =0;
    virtual bool                    SaveAs() =0;
    virtual void                    do_translate() =0;
    void                            set_save_act_enabled(bool value)            {save_act_->setEnabled(value);}

public:
    vSectionFile(QWidget* parent =0);
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions();
    void                            translate()                                 { do_translate(); }
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_FILE_H_ */
