/* 
 * File:   SectionBullet.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_BULLET_H_
#define	_V_SECTION_BULLET_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
#include "../../../Test/TestAlgorithm.h"
//------------------------------------------------------------------------------
class vSectionBullet : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionBullet                              class_name;

    QTextEdit*                                                                  text_edit_;
    QComboBox*                                                                  bullet_combobox_;
    QAction*                                                                    color_act_;

    void                            create_actions();
    void                            create_widgets(QToolBar* toolbar);
    void                            instance_signals();
    void                            setFormat(const QTextCharFormat& format);
    QStringList                     get_bullets();
    QTextListFormat::Style          get_list_format(int style_index);
    void                            clear_bullet();
    QTextListFormat                 set_new_format(QTextCursor& cursor);
    void                            colorChanged(const QColor &color);

private slots:
    void                            text_bullet(int style_index);
    void                            text_color();

public:
    vSectionBullet(QTextEdit* text_edit, QWidget* parent =0) : inherited(parent), text_edit_(text_edit), bullet_combobox_(0), color_act_(0)   {}
    void                            instance_on_toolbar(QToolBar* toolbar);
    void                            font_changed(const QColor& color);
    void                            style_changed(QTextList* list);
    QList<QAction*>                 actions();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_BULLET_SECTION_H_ */
