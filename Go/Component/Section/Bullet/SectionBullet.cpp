/* 
 * File:   UndoRedoSection.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionBullet.h"
//------------------------------------------------------------------------------
void vSectionBullet::instance_on_toolbar(QToolBar* toolbar)
{
        create_actions();
        create_widgets(toolbar);
        instance_signals();
        colorChanged(text_edit_->textColor());
}
//------------------------------------------------------------------------------
void vSectionBullet::create_actions()
{
        color_act_ = vAction::createColor(this);
        colorChanged(Qt::black);
}
//------------------------------------------------------------------------------
void vSectionBullet::create_widgets(QToolBar* toolbar)
{
        bullet_combobox_ = new QComboBox;
        bullet_combobox_->addItems(get_bullets());
        toolbar->addWidget(bullet_combobox_);
        toolbar->setObjectName(QString::fromUtf8("List"));
        toolbar->setWindowTitle(vtr::Button::tr("List"));
        toolbar->addSeparator();
        toolbar->addAction(color_act_);
}
//------------------------------------------------------------------------------
void vSectionBullet::instance_signals()
{
        connect(bullet_combobox_, SIGNAL(activated(int)), this, SLOT(text_bullet(int)));
        connect(color_act_, SIGNAL(triggered()), this, SLOT(text_color()));
}
//------------------------------------------------------------------------------
QStringList vSectionBullet::get_bullets()
{
        QStringList Result;
        Result << vtr::Bullet::tr("Standard");
        Result << vtr::Bullet::tr("Bullet List (Disc)");
        Result << vtr::Bullet::tr("Bullet List (Circle)");
        Result << vtr::Bullet::tr("Bullet List (Square)");
        Result << vtr::Bullet::tr("Ordered List (Decimal)");
        Result << vtr::Bullet::tr("Ordered List (Alpha lower)");
        Result << vtr::Bullet::tr("Ordered List (Alpha upper)");
        return Result;
}
//------------------------------------------------------------------------------
void vSectionBullet::text_color()
{
        QColor col = QColorDialog::getColor(text_edit_->textColor(), this);
        if(!col.isValid()) return;
        QTextCharFormat fmt;
        fmt.setForeground(col);
        setFormat(fmt);
        colorChanged(col);
}
//------------------------------------------------------------------------------
void vSectionBullet::setFormat(const QTextCharFormat& format)
{
        QTextCursor cursor = text_edit_->textCursor();
        if(!cursor.hasSelection()) cursor.select(QTextCursor::WordUnderCursor);
        cursor.mergeCharFormat(format);
        text_edit_->mergeCurrentCharFormat(format);
}
//------------------------------------------------------------------------------
void vSectionBullet::colorChanged(const QColor &color)
{
        QPixmap pix(20, 20);
        pix.fill(color);
        color_act_->setIcon(pix);
}
//------------------------------------------------------------------------------
void vSectionBullet::text_bullet(int style_index)
{
        if(!style_index) return clear_bullet();

        QTextCursor cursor = text_edit_->textCursor();
        cursor.beginEditBlock();
        QTextListFormat listFmt = cursor.currentList() ? cursor.currentList()->format() : set_new_format(cursor);
        listFmt.setStyle(get_list_format(style_index));
        cursor.createList(listFmt);
        cursor.endEditBlock();
}
//------------------------------------------------------------------------------
void vSectionBullet::clear_bullet()
{
        QTextBlockFormat block;
        block.setObjectIndex(0);
        text_edit_->textCursor().mergeBlockFormat(block);
}
//------------------------------------------------------------------------------
QTextListFormat vSectionBullet::set_new_format(QTextCursor& cursor)
{
        QTextListFormat result;
        result.setIndent(cursor.blockFormat().indent() + 1);

        QTextBlockFormat block_format;
        block_format.setIndent(0);
        cursor.setBlockFormat(block_format);
        return result;
}
//------------------------------------------------------------------------------
QTextListFormat::Style vSectionBullet::get_list_format(int style_index)
{
        switch(style_index)
        {
        default:
        case 1: return QTextListFormat::ListDisc;
        case 2: return QTextListFormat::ListCircle;
        case 3: return QTextListFormat::ListSquare;
        case 4: return QTextListFormat::ListDecimal;
        case 5: return QTextListFormat::ListLowerAlpha;
        case 6: return QTextListFormat::ListUpperAlpha;
        }
}
//------------------------------------------------------------------------------
void vSectionBullet::font_changed(const QColor& color)
{
        colorChanged(color);
}
//------------------------------------------------------------------------------
void vSectionBullet::style_changed(QTextList* list)
{
        int index = list ? list->format().style() : 0;
        bullet_combobox_->setCurrentIndex(qAbs(index));
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionBullet::actions()
{
        return QList<QAction*>() << color_act_;
}
//------------------------------------------------------------------------------
bool vSectionBullet::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(bullet_combobox_);
        Q_ASSERT(color_act_);
        Q_ASSERT(color_act_->isEnabled());
        Q_ASSERT(!color_act_->icon().isNull());
        Q_ASSERT(!color_act_->text().isEmpty());
        Q_ASSERT(bullet_combobox_->count() == 7);
        Q_ASSERT(vTestAlgorithm::load_text(text_edit_,  const_test_file::bullet_section()));
        Q_ASSERT(bullet_combobox_->currentIndex() == 0);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#000000");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 6);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#ff0000");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 5);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#005500");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 4);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#00ff00");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 1);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#000000");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 2);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#000000");
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(bullet_combobox_->currentIndex() == 3);
        Q_ASSERT(text_edit_->currentCharFormat().foreground().color().name() =="#00aaff");
        text_edit_->clear();
        return true;
}
//------------------------------------------------------------------------------