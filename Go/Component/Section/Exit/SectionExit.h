/**
    \file   SectionExit.h
    \brief  Файл SectionExit.h

    \author S.Panin
    \date   Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _SECTION_EXIT_H_
#define	_SECTION_EXIT_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
//------------------------------------------------------------------------------
/** \brief Derpecated */
class vSectionExit : private QWidget
{
    typedef                         QWidget                                     inherited;
    typedef                         vSectionExit                                class_name;

    QMainWindow*                                                                main_window_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    exit_act_;

    void                            create_actions();
    void                            instance_signals();
    void                            create_toolbar();

public:
    vSectionExit(QMainWindow* main_window, QWidget* parent =0) : inherited(parent), main_window_(main_window), toolbar_(0), exit_act_(0)  {}
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::section  */
    namespace section {
//------------------------------------------------------------------------------
/** \brief Секция Exit */
class Exit : private QWidget
{
    typedef                         QWidget                                     inherited;
    typedef                         Exit                                        class_name;

    QMainWindow*                                                                main_window_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    exit_act_;

    void                            create_actions()                            { exit_act_ = vAction::createExit(this); }
    void                            instance_signals()                          { connect(exit_act_, SIGNAL(triggered()), main_window_, SLOT(close())); }
    void                            create_toolbar()
    {
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Exit"));
        toolbar_->addAction(exit_act_);
    }

public:
    /** 
     \brief Конструктор.
     \param main_window - указатель на главное окно.
     \param parent - указатель на родителя. По умолчанию - отсутствует.
     */
    Exit(QMainWindow* main_window, QWidget* parent =0) : inherited(parent), main_window_(main_window), toolbar_(0), exit_act_(0)  {}
    /** \brief Инстанцирование. */
    void                            instance()
    {
        create_actions();
        create_toolbar();
        instance_signals();
        translate();
    }
    /**
     \brief Панель инструментов.
     \result QToolBar* - указатель на панель инструментов.
    */
    QToolBar*                       toolbar() const                             { return toolbar_; }
    /**
     \brief Список всех действий (actions).
     \result QList<QAction*> - список actions.
    */
    QList<QAction*>                 actions()                                   { return QList<QAction*>() << exit_act_; }
    /** \brief Перевод. */
    void                            translate()                                 { toolbar_->setWindowTitle(go::tr::Button::tr("Exit")); }
    /** \brief Тест. */
    bool test()
    {
        Q_ASSERT(main_window_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(exit_act_);
        Q_ASSERT(exit_act_->isEnabled());
        Q_ASSERT(!exit_act_->icon().isNull());
        Q_ASSERT(!exit_act_->text().isEmpty());
        return true;
    }
};
//------------------------------------------------------------------------------
    }; // end namespace section
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _SECTION_EXIT_H_ */