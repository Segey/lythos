/* 
 * File:   SectionExit.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionExit.h"
//------------------------------------------------------------------------------
void vSectionExit::instance()
{
        create_actions();
        create_toolbar();
        instance_signals();
        translate();
}
//------------------------------------------------------------------------------
void vSectionExit::create_actions()
{
        exit_act_ = vAction::createExit(this);
}
//------------------------------------------------------------------------------
void vSectionExit::create_toolbar()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Exit"));
        toolbar_->addAction(exit_act_);
}
//------------------------------------------------------------------------------
void vSectionExit::instance_signals()
{
        connect(exit_act_, SIGNAL(triggered()), main_window_, SLOT(close()));
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionExit::actions()
{
        return QList<QAction*>() << exit_act_;
}
//------------------------------------------------------------------------------
void vSectionExit::translate()
{
        toolbar_->setWindowTitle(vtr::Button::tr("Exit"));
}
//------------------------------------------------------------------------------
bool vSectionExit::test()
{
        Q_ASSERT(main_window_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(exit_act_);
        Q_ASSERT(exit_act_->isEnabled());
        Q_ASSERT(!exit_act_->icon().isNull());
        Q_ASSERT(!exit_act_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------