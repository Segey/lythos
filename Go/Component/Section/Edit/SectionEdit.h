/* 
 * File:   SectionEdit.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_EDIT_H_
#define	_V_SECTION_EDIT_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
//------------------------------------------------------------------------------
class vSectionEdit : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionEdit                                class_name;

    QTextEdit*                                                                  text_edit_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    cut_act_;
    QAction*                                                                    copy_act_;
    QAction*                                                                    paste_act_;
    QAction*                                                                    clear_act_;

    void                            create_actions();
    void                            instance_signals();
    void                            create_toolbar();

private slots:
     void                           clipboardDataChanged()                      { paste_act_->setEnabled(!QApplication::clipboard()->text().isEmpty()); }

public:
    vSectionEdit(QTextEdit* text_edit, QWidget* parent =0);
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions() const                             { return QList<QAction*>() << cut_act_ << copy_act_ << paste_act_ << clear_act_; }
    void                            translate()                                 { toolbar_->setWindowTitle(vtr::Button::tr("Edit")); }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_EDIT_H_ */
