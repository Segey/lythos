/* 
 * File:   SectionEdit.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionEdit.h"
//------------------------------------------------------------------------------
vSectionEdit::vSectionEdit(QTextEdit* text_edit, QWidget* parent /*=0*/)
                : inherited(parent), text_edit_(text_edit), toolbar_(0), cut_act_(0), copy_act_(0), paste_act_(0), clear_act_(0)
{
}
//------------------------------------------------------------------------------
void vSectionEdit::instance()
{
        create_actions();
        create_toolbar();
        instance_signals();
        translate();

        cut_act_->setEnabled(false);
        copy_act_->setEnabled(false);
        clipboardDataChanged();
}
//------------------------------------------------------------------------------
void vSectionEdit::create_actions()
{
        cut_act_        = vAction::createCutWithTip(this);
        copy_act_       = vAction::createCopyWithTip(this);
        paste_act_      = vAction::createPasteWithTip(this);
        clear_act_      = vAction::createClearText(this);
}
//------------------------------------------------------------------------------
void vSectionEdit::create_toolbar()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Edit"));
        toolbar_->addAction(cut_act_);
        toolbar_->addAction(copy_act_);
        toolbar_->addAction(paste_act_);
}
//------------------------------------------------------------------------------
void vSectionEdit::instance_signals()
{
        connect(cut_act_, SIGNAL(triggered()), text_edit_, SLOT(cut()));
        connect(copy_act_, SIGNAL(triggered()), text_edit_, SLOT(copy()));
        connect(paste_act_, SIGNAL(triggered()), text_edit_, SLOT(paste()));
        connect(clear_act_, SIGNAL(triggered()), text_edit_, SLOT(clear()));
        connect(text_edit_, SIGNAL(copyAvailable(bool)), cut_act_, SLOT(setEnabled(bool)));
        connect(text_edit_, SIGNAL(copyAvailable(bool)), copy_act_, SLOT(setEnabled(bool)));
        connect(QApplication::clipboard(), SIGNAL(dataChanged()), SLOT(clipboardDataChanged()));
}
//------------------------------------------------------------------------------
bool vSectionEdit::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(cut_act_);
        Q_ASSERT(!cut_act_->icon().isNull());
        Q_ASSERT(!cut_act_->text().isEmpty());
        Q_ASSERT(copy_act_);
        Q_ASSERT(!copy_act_->icon().isNull());
        Q_ASSERT(!copy_act_->text().isEmpty());
        Q_ASSERT(paste_act_);
        Q_ASSERT(!paste_act_->icon().isNull());
        Q_ASSERT(!paste_act_->text().isEmpty());
        Q_ASSERT(clear_act_);
        Q_ASSERT(!clear_act_->icon().isNull());
        Q_ASSERT(!clear_act_->text().isEmpty());
        Q_ASSERT(clear_act_->isEnabled());
        QApplication::clipboard()->text().isEmpty()  ? Q_ASSERT(!paste_act_->isEnabled()) : Q_ASSERT(paste_act_->isEnabled());
        QApplication::clipboard()->setText("Hello");
        Q_ASSERT(paste_act_->isEnabled());
        return true;
}
//------------------------------------------------------------------------------