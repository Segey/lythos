/* 
 * File:   SectionStyle.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionStyle.h"
//------------------------------------------------------------------------------
vSectionStyle::vSectionStyle(QTextEdit* text_edit, QWidget* parent /*=0*/)
            : inherited(parent), text_edit_(text_edit), toolbar_(0), bold_act_(0), italic_act_(0), underline_act_(0)
{

}
//------------------------------------------------------------------------------
void vSectionStyle::instance()
{
        create_actions();
        instance_checkable();
        create_toolbar();
        instance_signals();
        translate();
}
//------------------------------------------------------------------------------
void vSectionStyle::create_actions()
{
        bold_act_       = vAction::createBold(this);
        italic_act_     = vAction::createItalic(this);
        underline_act_  = vAction::createUnderline(this);
}
//------------------------------------------------------------------------------
void vSectionStyle::instance_checkable()
{
        bold_act_->setCheckable(true);
        italic_act_->setCheckable(true);
        underline_act_->setCheckable(true);
}
//------------------------------------------------------------------------------
void vSectionStyle::create_toolbar()
{
        toolbar_ = new QToolBar(this);
        toolbar_->setObjectName(QString::fromUtf8("Style"));
        toolbar_->addAction(bold_act_);
        toolbar_->addAction(italic_act_);
        toolbar_->addAction(underline_act_);
}
//------------------------------------------------------------------------------
void vSectionStyle::instance_signals()
{
        connect(bold_act_, SIGNAL(triggered()), this, SLOT(text_bold()));
        connect(italic_act_, SIGNAL(triggered()), this, SLOT(text_italic()));
        connect(underline_act_, SIGNAL(triggered()), this, SLOT(text_underline()));
}
//------------------------------------------------------------------------------
void vSectionStyle::text_bold()
{
        QTextCharFormat fmt;
        fmt.setFontWeight(bold_act_->isChecked() ? QFont::Bold : QFont::Normal);
        setFormat(fmt);
}
//------------------------------------------------------------------------------
void vSectionStyle::text_italic()
{
        QTextCharFormat fmt;
        fmt.setFontItalic(italic_act_->isChecked());
        setFormat(fmt);
}
//------------------------------------------------------------------------------
void vSectionStyle::text_underline()
{
        QTextCharFormat fmt;
        fmt.setFontUnderline(underline_act_->isChecked());
        setFormat(fmt);
}
//------------------------------------------------------------------------------
void vSectionStyle::setFormat(const QTextCharFormat& format)
{
        QTextCursor cursor = text_edit_->textCursor();
        if(!cursor.hasSelection()) cursor.select(QTextCursor::WordUnderCursor);
        cursor.mergeCharFormat(format);
        text_edit_->mergeCurrentCharFormat(format);
}
//------------------------------------------------------------------------------
void vSectionStyle::font_changed(const QFont& font)
{
        bold_act_->setChecked(font.bold());
        italic_act_->setChecked(font.italic());
        underline_act_->setChecked(font.underline());
}
//------------------------------------------------------------------------------
QList<QAction*> vSectionStyle::actions()
{
        return QList<QAction*>() << bold_act_ << italic_act_ << underline_act_;
}
//------------------------------------------------------------------------------
void vSectionStyle::translate()
{
        toolbar_->setWindowTitle(vtr::Button::tr("Style"));
}
//------------------------------------------------------------------------------
bool vSectionStyle::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(bold_act_);
        Q_ASSERT(italic_act_);
        Q_ASSERT(underline_act_);
        Q_ASSERT(bold_act_->isEnabled());
        Q_ASSERT(!bold_act_->icon().isNull());
        Q_ASSERT(!bold_act_->text().isEmpty());
        Q_ASSERT(italic_act_->isEnabled());
        Q_ASSERT(!italic_act_->icon().isNull());
        Q_ASSERT(!italic_act_->text().isEmpty());
        Q_ASSERT(underline_act_->isEnabled());
        Q_ASSERT(!underline_act_->icon().isNull());
        Q_ASSERT(!underline_act_->text().isEmpty());
        Q_ASSERT(vTestAlgorithm::load_text(text_edit_, const_test_file::style_section()));
        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(!text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(!text_edit_->currentCharFormat().font().underline());
        text_edit_->moveCursor(QTextCursor::NextBlock);

        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(!text_edit_->currentCharFormat().font().underline());
        text_edit_->moveCursor(QTextCursor::NextBlock);

        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(text_edit_->currentCharFormat().font().underline());
        text_edit_->moveCursor(QTextCursor::NextBlock);

        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(!text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(text_edit_->currentCharFormat().font().underline());
        text_edit_->moveCursor(QTextCursor::NextBlock);

        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(!text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(!text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(!text_edit_->currentCharFormat().font().underline());
        text_edit_->moveCursor(QTextCursor::NextBlock);

        text_edit_->moveCursor(QTextCursor::NextCharacter);
        Q_ASSERT(!text_edit_->currentCharFormat().font().bold());
        Q_ASSERT(!text_edit_->currentCharFormat().font().italic());
        Q_ASSERT(text_edit_->currentCharFormat().font().underline());

        text_edit_->clear();
        return true;
}

//------------------------------------------------------------------------------