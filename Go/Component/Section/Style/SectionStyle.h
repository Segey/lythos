/* 
 * File:   SectionStyle.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_STYLE_H_
#define	_V_SECTION_STYLE_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
#include "../../../Test/TestAlgorithm.h"
//------------------------------------------------------------------------------
class vSectionStyle : private QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionStyle                               class_name;

    QTextEdit*                                                                  text_edit_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    bold_act_;
    QAction*                                                                    italic_act_;
    QAction*                                                                    underline_act_;

    void                            create_actions();
    void                            instance_checkable();
    void                            instance_signals();
    void                            create_toolbar();
    void                            setFormat(const QTextCharFormat& format);

private slots:
    void                            text_bold();
    void                            text_italic();
    void                            text_underline();

public:
    vSectionStyle(QTextEdit* text_edit, QWidget* parent =0);
    void                            font_changed(const QFont& font);
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_STYLE_H_ */
