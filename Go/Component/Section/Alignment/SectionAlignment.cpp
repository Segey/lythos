/* 
 * File:   SectionAlignment.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "SectionAlignment.h"
//------------------------------------------------------------------------------
vSectionAlignment::vSectionAlignment(QTextEdit* text_edit, QWidget* parent /*= 0*/)
        : inherited(parent), text_edit_(text_edit), action_group_(0), toolbar_(0),
        left_act_(0), center_act_(0), right_act_(0), justify_act_(0)
{
}
//------------------------------------------------------------------------------
void vSectionAlignment::instance()
{
        create_actions();
        create_action_group();
        create_toolbar();
        instance_checkable();
        instance_signals();
        translate();
        alignment_changed(text_edit_->alignment());
}
//------------------------------------------------------------------------------
void vSectionAlignment::create_actions()
{
        action_group_ = new QActionGroup(this);
        left_act_ = vAction::createLeft(this);
        center_act_ = vAction::createCenter(this);
        right_act_ = vAction::createRight(this);
        justify_act_ = vAction::createJustify(this);
}
//------------------------------------------------------------------------------
void vSectionAlignment::instance_checkable()
{
        left_act_->setCheckable(true);
        center_act_->setCheckable(true);
        right_act_->setCheckable(true);
        justify_act_->setCheckable(true);
}
//------------------------------------------------------------------------------
void vSectionAlignment::instance_signals()
{
        connect(action_group_, SIGNAL(triggered(QAction *)), this, SLOT(text_align(QAction *)));
}
//------------------------------------------------------------------------------
void vSectionAlignment::text_align(QAction* action)
{
        if(action == left_act_) text_edit_->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
        else if(action == center_act_) text_edit_->setAlignment(Qt::AlignHCenter);
        else if(action == right_act_) text_edit_->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
        else if(action == justify_act_) text_edit_->setAlignment(Qt::AlignJustify);
}
//------------------------------------------------------------------------------
void vSectionAlignment::alignment_changed(Qt::Alignment action)
{
        if(action & Qt::AlignLeft) left_act_->setChecked(true);
        else if(action & Qt::AlignHCenter) center_act_->setChecked(true);
        else if(action & Qt::AlignRight) right_act_->setChecked(true);
        else if(action & Qt::AlignJustify) justify_act_->setChecked(true);
}
//------------------------------------------------------------------------------
void vSectionAlignment::translate()
{
        toolbar_->setObjectName(QString::fromUtf8("Align"));
        toolbar_->setWindowTitle(vtr::Button::tr("Align"));
}
//------------------------------------------------------------------------------
bool vSectionAlignment::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(action_group_);
        Q_ASSERT(toolbar_);
        Q_ASSERT(left_act_);
        Q_ASSERT(center_act_);
        Q_ASSERT(right_act_);
        Q_ASSERT(justify_act_);
        Q_ASSERT(!left_act_->icon().isNull());
        Q_ASSERT(!left_act_->text().isEmpty());
        Q_ASSERT(!center_act_->icon().isNull());
        Q_ASSERT(!center_act_->text().isEmpty());
        Q_ASSERT(!right_act_->icon().isNull());
        Q_ASSERT(!right_act_->text().isEmpty());
        Q_ASSERT(!justify_act_->icon().isNull());
        Q_ASSERT(!justify_act_->text().isEmpty());
        Q_ASSERT(vTestAlgorithm::load_text(text_edit_, const_test_file::align_section()));
        Q_ASSERT(center_act_->isChecked());
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(left_act_->isChecked());
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(right_act_->isChecked());
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(justify_act_->isChecked());
        text_edit_->moveCursor(QTextCursor::NextBlock);
        Q_ASSERT(justify_act_->isChecked());
        text_edit_->clear();
        return true;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void vSectionAlignmentLeft::createToolbar()
{
        inherited::toolbar_ = new QToolBar(this);
        inherited::toolbar_->addAction(inherited::left_act_);
        inherited::toolbar_->addAction(inherited::center_act_);
        inherited::toolbar_->addAction(inherited::right_act_);
        inherited::toolbar_->addAction(inherited::justify_act_);
}
//------------------------------------------------------------------------------
            /*virtual*/ void vSectionAlignmentLeft::createActionGroup()
{
        inherited::action_group_->addAction(inherited::left_act_);
        inherited::action_group_->addAction(inherited::center_act_);
        inherited::action_group_->addAction(inherited::right_act_);
        inherited::action_group_->addAction(inherited::justify_act_);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void vSectionAlignmentRight::createToolbar()
{
        inherited::toolbar_ = new QToolBar(this);
        inherited::toolbar_->addAction(inherited::right_act_);
        inherited::toolbar_->addAction(inherited::center_act_);
        inherited::toolbar_->addAction(inherited::left_act_);
        inherited::toolbar_->addAction(inherited::justify_act_);
}
//------------------------------------------------------------------------------
            /*virtual*/ void vSectionAlignmentRight::createActionGroup()
{
        inherited::action_group_->addAction(inherited::right_act_);
        inherited::action_group_->addAction(inherited::center_act_);
        inherited::action_group_->addAction(inherited::left_act_);
        inherited::action_group_->addAction(inherited::justify_act_);
}