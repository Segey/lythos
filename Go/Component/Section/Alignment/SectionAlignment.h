/* 
 * File:   SectionAlignment.h
 * Author: S.Panin
 *
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_SECTION_ALIGNMENT_H_
#define	_V_SECTION_ALIGNMENT_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
#include "../../../Test/TestAlgorithm.h"
//------------------------------------------------------------------------------
class vSectionAlignment : public QWidget
{
Q_OBJECT
    typedef                         QWidget                                     inherited;
    typedef                         vSectionAlignment                           class_name;

    void                            create_actions();
    void                            instance_checkable();
    void                            instance_signals();
    void                            create_toolbar()                            { createToolbar(); }
    void                            create_action_group()                       { createActionGroup(); }

private slots:
    void                            text_align(QAction* action);

protected:
    QTextEdit*                                                                  text_edit_;
    QActionGroup*                                                               action_group_;
    QToolBar*                                                                   toolbar_;
    QAction*                                                                    left_act_;
    QAction*                                                                    center_act_;
    QAction*                                                                    right_act_;
    QAction*                                                                    justify_act_;

protected:
    virtual void                    createToolbar() =0;
    virtual void                    createActionGroup() =0;

public:
    vSectionAlignment(QTextEdit* text_edit, QWidget* parent =0);
    void                            alignment_changed(Qt::Alignment action);
    void                            instance();
    QToolBar*                       toolbar() const                             {return toolbar_;}
    QList<QAction*>                 actions()                                   {return action_group_->actions();}
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
class vSectionAlignmentLeft : public vSectionAlignment
{
Q_OBJECT
    typedef                         vSectionAlignment                               inherited;
    typedef                         vSectionAlignmentLeft                           class_name;

protected:
    virtual void                    createToolbar();
    virtual void                    createActionGroup();

public:
    vSectionAlignmentLeft(QTextEdit* text_edit, QWidget* parent =0) : inherited(text_edit, parent)  {}
};
//------------------------------------------------------------------------------
class vSectionAlignmentRight : public vSectionAlignment
{
Q_OBJECT
    typedef                         vSectionAlignment                               inherited;
    typedef                         vSectionAlignmentRight                          class_name;

protected:
    virtual void                    createToolbar();
    virtual void                    createActionGroup();

public:
    vSectionAlignmentRight(QTextEdit* text_edit, QWidget* parent =0) : inherited(text_edit, parent)  {}
};
//------------------------------------------------------------------------------
#endif	/* _V_SECTION_ALIGNMENT_H_ */
