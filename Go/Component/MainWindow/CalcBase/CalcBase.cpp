/* 
 * File:   CalcBase.cpp
 * Author: S.Panin
 * 
 * Created on 22 Апреля 2010 г., 14:24
 */
//------------------------------------------------------------------------------
#include "CalcBase.h"
//------------------------------------------------------------------------------
void vCalcBase::show()
{
        ::vMainWindowInstance< vCalcBaseInstance > (this).instance();

        instance_signals();
        settings_read();
        inherited::show();
}
//------------------------------------------------------------------------------
void vCalcBase::settings_read()
{
} 
//------------------------------------------------------------------------------
void vCalcBase::settings_write() const
{
}
//------------------------------------------------------------------------------
void vCalcBase::instance_signals()
{
        connect(dock_weight_->widget(),         SIGNAL(returnPressed()),                                SLOT(on_execute()));
        connect(dock_rep_->widget(),            SIGNAL(returnPressed()),                                SLOT(on_execute()));
        connect(dock_calculate_->button(),      SIGNAL(clicked(bool)),                                  SLOT(on_execute()));
        connect(dock_rep_->widget(),            SIGNAL(textChanged (const QString&)),                   SLOT(on_execute()));
        connect(dock_weight_->widget(),         SIGNAL(textChanged (const QString&)),                   SLOT(on_execute()));
        connect(menu_method_,                   SIGNAL( on_method_changed(v_calc_base::methods) ),      SLOT(on_execute()));
}
//------------------------------------------------------------------------------
void vCalcBase::on_execute()
{
        bool first_done, second_done;
        const double first =dock_weight_->widget()->text().toDouble(&first_done);
        const double second =dock_rep_->widget()->text().toDouble(&second_done);
        lcd_number_->display( ( first_done && second_done && first > 0 && second > 0) ? do_execute(menu_method_->method(), first, second) : 0);
}
//------------------------------------------------------------------------------
bool vCalcBase::test()
{
        Q_ASSERT(menu_file_);
        Q_ASSERT(menu_file_);
        Q_ASSERT(menu_edit_);
        Q_ASSERT(menu_view_);
        Q_ASSERT(menu_method_);
        Q_ASSERT(menu_help_);
        Q_ASSERT(dock_calculate_);
        Q_ASSERT(dock_weight_);
        Q_ASSERT(dock_rep_);
        Q_ASSERT(lcd_number_);
//        Q_ASSERT(!settings_table_.isEmpty());
//        Q_ASSERT(!settings_key_state_.isEmpty());
//        Q_ASSERT(!settings_key_form_.isEmpty());
//        Q_ASSERT(!window_title_.isEmpty());
//        Q_ASSERT(!title_weight_.isEmpty());
//        Q_ASSERT(!title_per_.isEmpty());
        return true;
}
//------------------------------------------------------------------------------