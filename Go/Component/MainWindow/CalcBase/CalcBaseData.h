// -*- C++ -*-
/* 
 * File:   CalcBaseData.h
 * Author: S.Panin
 *
 * Created on 27 Апрель 2010 г., 0:19
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_DATA_H_
#define	_V_CALC_BASE_DATA_H_
//------------------------------------------------------------------------------
namespace v_calc_base { // NAMESPACE BEGIN v_calc_base
//------------------------------------------------------------------------------
    enum methods { no =0, brzycki =1, apple =2, lander =3 };
//------------------------------------------------------------------------------
} // NAMESPACE END v_calc_base
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_DATA_H_ */

