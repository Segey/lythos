/*
 * File:   CalcBaseInstance.cpp
 * Author: S.Panin
 *
 * Created on 22 Апреля 2010 г., 23:04
 */
//------------------------------------------------------------------------------
#include "CalcBaseInstance.h"
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_form()
{
        parent_->setWindowIcon(const_icon_16::program());
        parent_->setGeometry(100,100,440,30);
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_widgets()
{
        parent_->lcd_number_ = new QLCDNumber;
        parent_->setCentralWidget( parent_->lcd_number_ );

        instance_dock_first();
        instance_dock_second();
        instance_dock_execute();
        instance_menu_file();
        instance_menu_edit();
        instance_menu_view();
        instance_menu_method();
        instance_menu_help();
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_dock_first()
{
        parent_->dock_weight_ = new vCalcBase::dock_type(parent_);
        parent_->addDockWidget(Qt::LeftDockWidgetArea, parent_->dock_weight_);
        parent_->dock_weight_->setObjectName(QString::fromUtf8("DockWeight"));
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_dock_second()
{
        parent_->dock_rep_ = new vCalcBase::dock_type(parent_);
        parent_->addDockWidget(Qt::LeftDockWidgetArea, parent_->dock_rep_);
        parent_->dock_rep_->setObjectName(QString::fromUtf8("DockRep"));
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_dock_execute()
{
        parent_->dock_calculate_ = new vCalcBaseDockCalculate(parent_);
        parent_->addDockWidget(Qt::BottomDockWidgetArea, parent_->dock_calculate_ );
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_menu_file()
{
        parent_->menu_file_ = new vCalcBaseMenuFile(parent_);
        parent_->menuBar()->addMenu( parent_->menu_file_ );
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_menu_edit()
{
        parent_->menu_edit_ = new vCalcBaseMenuEdit(parent_->dock_weight_->widget(), parent_->dock_rep_->widget(), parent_);
        parent_->menuBar()->addMenu( parent_->menu_edit_ );
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_menu_view()
{
        parent_->menu_view_ = new vCalcBaseMenuView(parent_->dock_weight_, parent_->dock_rep_, parent_->dock_calculate_, parent_) ;
        parent_->menuBar()->addMenu( parent_->menu_view_ );
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_menu_method()
{
        parent_->menu_method_ = new vCalcBaseMenuMethod(parent_);
        parent_->menuBar()->addMenu(parent_->menu_method_);
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::instance_menu_help()
{
        parent_->menu_help_ = new vMenuHelp(parent_);
        parent_->menu_help_->instance();
        parent_->menuBar()->addMenu(parent_->menu_help_);
}
//------------------------------------------------------------------------------
void vCalcBaseInstance::translate()
{
        parent_->dock_weight_->setWindowTitle(parent_->title_first_ );
        parent_->dock_rep_->setWindowTitle(parent_->title_second_ );
        parent_->menu_file_->setTitle(vtr::Button::tr("&File"));
        parent_->menu_edit_->set_title(vtr::Button::tr("&Edit"));
        parent_->menu_view_->setTitle(vtr::Button::tr("&View"));
        parent_->menu_method_->setTitle(vtr::Method::tr("&Method"));
        parent_->dock_calculate_->button()->setText(vtr::Button::tr("&Calculate") );
        parent_->menu_view_->set_action_text(0, parent_->title_first_);
        parent_->menu_view_->set_action_text(1, parent_->title_second_);
        parent_->menu_view_->set_action_text(2, vtr::Button::tr("&Calculate"));
        parent_->setWindowTitle(parent_->window_title_);  
}
