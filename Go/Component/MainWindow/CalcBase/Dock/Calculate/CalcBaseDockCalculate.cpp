/* 
 * File:   vCalcBaseDockCalculate.cpp
 * Author: S.Panin
 * 
 * Created on Пн апр. 26 2010, 22:24:19
 */
//------------------------------------------------------------------------------
#include "CalcBaseDockCalculate.h"
//------------------------------------------------------------------------------
vCalcBaseDockCalculate::vCalcBaseDockCalculate(QWidget* parent)
                : inherited(parent), parent_(parent), button_(0)
{
        create_widgets();
        instance_widgets();
}
//------------------------------------------------------------------------------
void vCalcBaseDockCalculate::create_widgets()
{
        button_ = new QPushButton(parent_);
        dock_type::setObjectName(QString::fromUtf8("DockCalculate"));        
}
//------------------------------------------------------------------------------
void vCalcBaseDockCalculate::instance_widgets()
{
        QWidget* widget =new QWidget(parent_);
        QHBoxLayout* lay =new QHBoxLayout(widget);
        lay->addStretch();
        lay->addWidget(button_);
        dock_type::setWidget(widget);
}
//------------------------------------------------------------------------------
bool vCalcBaseDockCalculate::test()
{
        Q_ASSERT(parent_);
        Q_ASSERT(button_);
        return true;
}
//------------------------------------------------------------------------------