/* 
 * File:   vCalcBaseDockCalculate.h
 * Author: S.Panin
 *
 * Created on Пн апр. 26 2010, 22:24:34
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_DOCK_CALCULATE_H_
#define	_V_CALC_BASE_DOCK_CALCULATE_H_
//------------------------------------------------------------------------------
class vCalcBaseDockCalculate : public QDockWidget
{
    typedef                         QDockWidget                                 inherited;
    typedef                         vCalcBaseDockCalculate                      class_name;
    typedef                         QDockWidget                                 dock_type;
    typedef                         QPushButton*                                widget_type;

    QWidget*                                                                    parent_;
    widget_type                                                                 button_;

    void                            create_widgets();
    void                            instance_widgets();

public:
    explicit                        vCalcBaseDockCalculate(QWidget* parent);
    widget_type                     button() const                              { return button_; }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_DOCK_CALCULATE_H_ */
