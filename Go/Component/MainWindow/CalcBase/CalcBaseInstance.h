/*
 * File:   CalcBaseInstance.h
 * Author: S.Panin
 *
 * Created on 22 Апреля 2010 г., 23:04
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_INSTANCE_H_
#define	_V_CALC_BASE_INSTANCE_H_
//------------------------------------------------------------------------------
class vCalcBase;
//------------------------------------------------------------------------------
class vCalcBaseInstance
{
    typedef                     vCalcBaseInstance                               class_name;
    typedef                     vCalcBase                                       parent;

    parent*                                                                     parent_;

    void                        instance_dock_first();
    void                        instance_dock_second();
    void                        instance_dock_execute();
    
    void                        instance_menu_file();
    void                        instance_menu_edit();
    void                        instance_menu_view();
    void                        instance_menu_method();
    void                        instance_menu_help();

protected:
    void                        instance_form();
    void                        instance_widgets();

public:
    explicit                    vCalcBaseInstance(parent* p) : parent_(p)       {}
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_INSTANCE_H_ */
