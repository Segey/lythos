/* 
 * File:   CalcBase.h
 * Author: S.Panin
 *
 * Created on 22 Апреля 2010 г., 14:24
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_H_
#define	_V_CALC_BASE_H_
//------------------------------------------------------------------------------
#include "../../../Instance/MainWindowInstance.h"
#include "../../../Component/Dock/Dock.h"
#include "../../../Component/Menu/Edit/MenuEdit.h"
#include "../../../Component/Menu/View/MenuView.h"
#include "../../../Component/Menu/Help/MenuHelp.h"

#include "CalcBaseData.h"
#include "Menu/File/CalcBaseMenuFile.h"
#include "Menu/Edit/CalcBaseMenuEdit.h"
#include "Menu/View/CalcBaseMenuView.h"
#include "Menu/Method/CalcBaseMenuMethod.h"
#include "Dock/Calculate/CalcBaseDockCalculate.h"

#include "CalcBaseInstance.h"
//------------------------------------------------------------------------------
class vCalcBase :  public QMainWindow
{
Q_OBJECT

friend class vCalcBaseInstance;
    typedef                         QMainWindow                                 inherited;
    typedef                         vCalcBase                                   class_name;
    typedef                         vDock<QLineEdit>                            dock_type;

    vCalcBaseMenuFile*                                                          menu_file_;
    vCalcBaseMenuEdit*                                                          menu_edit_;
    vCalcBaseMenuView*                                                          menu_view_;
    vCalcBaseMenuMethod*                                                        menu_method_;
    vMenuHelp*                                                                  menu_help_;
    vCalcBaseDockCalculate*                                                     dock_calculate_;
    dock_type*                                                                  dock_weight_;
    dock_type*                                                                  dock_rep_;
    QLCDNumber*                                                                 lcd_number_;

    QString                                                                     settings_table_;
    QString                                                                     settings_key_state_;
    QString                                                                     settings_key_form_;
    QString                                                                     window_title_;
    QString                                                                     title_first_;
    QString                                                                     title_second_;

    void                            settings_read();
    void                            settings_write() const;    
    void                            instance_signals();

protected:
     virtual double                 do_execute(v_calc_base::methods method, double first, double second) =0;
     void                           set_settings_table(const QString& text)     { settings_table_ =text; }
     void                           set_settings_key_state(const QString& text) { settings_key_state_ =text; }
     void                           set_settings_key_form(const QString& text)  { settings_key_form_ =text; }
     void                           set_window_title(const QString& text)       { window_title_ =text; }
     void                           set_title_first(const QString& text)        { title_first_ =text; }
     void                           set_title_second(const QString& text)       { title_second_ = text; }

private slots:
    void                            on_execute();

public:
    explicit                        vCalcBase() : QMainWindow (0)               {}
    virtual                         ~vCalcBase()                                {}
    void                            show();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_H_ */
