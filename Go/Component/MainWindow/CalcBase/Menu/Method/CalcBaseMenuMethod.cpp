/* 
 * File:   CalcBaseMenuMethod.cpp
 * Author: S.Panin
 * 
 * Created on Вт апр. 27 2010, 00:02:26
 */
//------------------------------------------------------------------------------
#include "CalcBaseMenuMethod.h"
#include "../../../../../BuiltType/ActionGroup.h"
//------------------------------------------------------------------------------
vCalcBaseMenuMethod::vCalcBaseMenuMethod(QWidget* parent)
                : inherited(parent),  method_(v_calc_base::no), act_group_(0)
{
        create_actions();
        instance_signals();
        menu_type::actions().front()->setChecked(true);
}
//------------------------------------------------------------------------------
void vCalcBaseMenuMethod::create_actions()
{
        QAction* const act_brzycki = menu_type::addAction(vtr::Method::tr("&Brzyck's method"));
        act_brzycki->setData(v_calc_base::brzycki);

        QAction* const act_apple = menu_type::addAction(vtr::Method::tr("&Apple's method"));
        act_apple->setData(v_calc_base::apple);

        QAction* const act_lander = menu_type::addAction(vtr::Method::tr("&Lander's method"));
        act_lander->setData(v_calc_base::lander);
        
        act_group_ =v_action_group::make_group(menu_type::actions(), this, 0);
}
//------------------------------------------------------------------------------
void vCalcBaseMenuMethod::instance_signals()
{
        connect(act_group_, SIGNAL(triggered(QAction* )),  SLOT(change_method(QAction*)));
}
//------------------------------------------------------------------------------
void vCalcBaseMenuMethod::change_method(QAction* act)
{
        v_calc_base::methods method =static_cast<v_calc_base::methods>(act->data().toInt());
        method_ = method;
        emit(on_method_changed(method));
}
//------------------------------------------------------------------------------
bool vCalcBaseMenuMethod::test()
{
        Q_ASSERT(act_group_);
        Q_ASSERT(menu_type::actions().size() ==3);
        return true;
}
//------------------------------------------------------------------------------