/* 
 * File:   CalcBaseMenuMethod.h
 * Author: S.Panin
 *
 * Created on Вт апр. 27 2010, 00:02:26
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_MENU_METHOD_H_
#define	_V_CALC_BASE_MENU_METHOD_H_
//------------------------------------------------------------------------------
class vCalcBaseMenuMethod : public QMenu
{
Q_OBJECT
    typedef                         QMenu                                       inherited;
    typedef                         vCalcBaseMenuMethod                         class_name;
    typedef                         QMenu                                       menu_type;

private:  
    v_calc_base::methods                                                        method_;
    QActionGroup*                                                               act_group_;

    void                            create_actions();
    void                            instance_signals();

private slots:
    void                            change_method(QAction* act);
    
signals:
    void                            on_method_changed( v_calc_base::methods );

public:
    explicit                        vCalcBaseMenuMethod(QWidget* parent);
    v_calc_base::methods            method() const                              { return method_; }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_MENU_METHOD_H_ */
