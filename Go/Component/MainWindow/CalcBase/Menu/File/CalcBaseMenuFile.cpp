/* 
 * File:   CalcBaseMenuFile.cpp
 * Author: S.Panin
 * 
 * Created on Пн апр. 26 2010, 22:24:19
 */
//------------------------------------------------------------------------------
#include "CalcBaseMenuFile.h"
//------------------------------------------------------------------------------
vCalcBaseMenuFile::vCalcBaseMenuFile(QWidget* parent)
                : inherited(parent), parent_(parent), act_exit_(0)
{
        create_actions();
        instance_signals();
}
//------------------------------------------------------------------------------
void vCalcBaseMenuFile::create_actions()
{
        act_exit_ = vAction::createExit(this);
        menu_type::addAction(act_exit_);
}
//------------------------------------------------------------------------------
void vCalcBaseMenuFile::instance_signals()
{
        connect(act_exit_, SIGNAL(triggered(bool)),  parent_, SLOT(close()));
}
//------------------------------------------------------------------------------
bool vCalcBaseMenuFile::test()
{
        Q_ASSERT(act_exit_);
        return true;
}
//------------------------------------------------------------------------------