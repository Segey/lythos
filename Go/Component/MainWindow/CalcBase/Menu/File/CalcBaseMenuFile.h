/* 
 * File:   CalcBaseMenuFile.h
 * Author: S.Panin
 *
 * Created on Пн апр. 26 2010, 22:24:34
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_MENU_FILE_H_
#define	_V_CALC_BASE_MENU_FILE_H_
//------------------------------------------------------------------------------
class vCalcBaseMenuFile : public QMenu
{
Q_OBJECT
    typedef                         QMenu                                       inherited;
    typedef                         vCalcBaseMenuFile                           class_name;
    typedef                         QMenu                                       menu_type;

public:
    typedef                         QAction*                                    value_type;
    typedef                         const value_type&                           const_reference;

private:
    QWidget*                                                                    parent_;    
    value_type                                                                  act_exit_;

    void                            create_actions();
    void                            instance_signals();

public:
    explicit                        vCalcBaseMenuFile(QWidget* parent);
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_MENU_FILE_H_ */
