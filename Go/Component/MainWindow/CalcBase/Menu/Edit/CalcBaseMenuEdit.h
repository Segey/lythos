/* 
 * File:   vCalcBaseMenuEdit.h
 * Author: S.Panin
 *
 * Created on Пн апр. 26 2010, 01:40:12
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_MENU_EDIT_H_
#define	_V_CALC_BASE_MENU_EDIT_H_
//------------------------------------------------------------------------------
class vCalcBaseMenuEdit : public go::vMenuEdit
{
Q_OBJECT
    typedef                         go::vMenuEdit                               inherited;
    typedef                         vCalcBaseMenuEdit                           class_name;
    typedef                         QMenu                                       menu_type;
    typedef                         inherited::value_type                       value_type;
    typedef                         inherited::const_reference                  const_reference;

    QLineEdit*                                                                  edit_first_;
    QLineEdit*                                                                  edit_second_;

    void                            instance_signals();
    void                            translate()                                 { menu_type::setTitle(vtr::Button::tr("Edit")); }
    void                            instance_data();
    template<typename T>  void      on_click( T func ) const                    { edit_first_->hasFocus() ?  (edit_first_->*func)() : (edit_second_->*func)();  }

private slots:
     void                           on_click_redo() const                       { on_click(&QLineEdit::redo); }
     void                           on_click_undo() const                       { on_click(&QLineEdit::undo); }
     void                           on_click_cut() const                        { on_click(&QLineEdit::cut); }
     void                           on_click_copy() const                       { on_click(&QLineEdit::copy); }
     void                           on_click_paste() const                      { on_click(&QLineEdit::paste); }
     void                           on_click_clear() const                      { on_click(&QLineEdit::clear); }
     void                           on_clipboard_changed() const                { inherited::action_paste()->setEnabled(!QApplication::clipboard()->text().isEmpty()); }
     void                           on_data_changed() const;

public:
    explicit                        vCalcBaseMenuEdit( QLineEdit* edit_first, QLineEdit* edit_second, QWidget* parent);
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_MENU_EDIT_H_ */
