/* 
 * File:   vCalcBaseMenuEdit.cpp
 * Author: S.Panin
 * 
 * Created on Пн апр. 26 2010, 01:40:12
 */
//------------------------------------------------------------------------------
#include "CalcBaseMenuEdit.h"
//------------------------------------------------------------------------------
vCalcBaseMenuEdit::vCalcBaseMenuEdit( QLineEdit* edit_first, QLineEdit* edit_second, QWidget* parent)
                : inherited(parent), edit_first_(edit_first), edit_second_(edit_second)
{
        instance_signals();
        translate();
        instance_data();
}
//------------------------------------------------------------------------------
void vCalcBaseMenuEdit::instance_signals()
{
        connect(inherited::action_undo(),       SIGNAL(triggered()),  SLOT(on_click_undo()));
        connect(inherited::action_redo(),       SIGNAL(triggered()),  SLOT(on_click_redo()));
        connect(inherited::action_cut(),        SIGNAL(triggered()),  SLOT(on_click_cut()));
        connect(inherited::action_copy(),       SIGNAL(triggered()),  SLOT(on_click_copy()));
        connect(inherited::action_paste(),      SIGNAL(triggered()),  SLOT(on_click_paste()));
        connect(inherited::action_clear(),      SIGNAL(triggered()),  SLOT(on_click_clear()));
        connect(edit_first_,                    SIGNAL(editingFinished ()), SLOT(on_data_changed()));
        connect(edit_second_,                   SIGNAL(editingFinished ()), SLOT(on_data_changed()));
        connect(QApplication::clipboard(),      SIGNAL(dataChanged()), SLOT(on_clipboard_changed()));
}
//------------------------------------------------------------------------------
void vCalcBaseMenuEdit::instance_data()
{
        inherited::set_all_enabled(false);
        on_clipboard_changed();
}
//------------------------------------------------------------------------------
void vCalcBaseMenuEdit::on_data_changed() const
{
        inherited::action_copy()->setEnabled( edit_first_->hasFocus() ? edit_first_->hasSelectedText(): edit_second_->hasSelectedText());
        inherited::action_cut()->setEnabled( edit_first_->hasFocus() ? edit_first_->hasSelectedText(): edit_second_->hasSelectedText());
        inherited::action_undo()->setEnabled( edit_first_->hasFocus() ? edit_first_->isUndoAvailable(): edit_second_->isUndoAvailable());
        inherited::action_redo()->setEnabled( edit_first_->hasFocus() ? edit_first_->isRedoAvailable(): edit_second_->isRedoAvailable());
        inherited::action_clear()->setEnabled( edit_first_->hasFocus() ? !edit_first_->text().isEmpty() : !edit_second_->text().isEmpty());
}
//------------------------------------------------------------------------------
bool vCalcBaseMenuEdit::test()
{
        Q_ASSERT(inherited::test());
        Q_ASSERT(edit_first_);
        Q_ASSERT(edit_second_);
        return true;
}
//------------------------------------------------------------------------------