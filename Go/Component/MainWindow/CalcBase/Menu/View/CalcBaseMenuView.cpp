/* 
 * File:   MenuView.cpp
 * Author: S.Panin
 * 
 * Created on Пн апр. 26 2010, 01:54:24
 */
//------------------------------------------------------------------------------
#include "CalcBaseMenuView.h"
//------------------------------------------------------------------------------
vCalcBaseMenuView::vCalcBaseMenuView(window_type* first, window_type* second, window_type* calculate, QWidget* parent)
                : inherited(parent), window_weight_(first), window_rep_(second), window_calculate_(calculate)
{
        create_actions();
        inherited::set_all_checkable(true);
        instance_signals();
}
//------------------------------------------------------------------------------
void vCalcBaseMenuView::create_actions()
{
        menu_type::addAction(" ");
        menu_type::addAction(" ");
        menu_type::addAction(" ");
}
//------------------------------------------------------------------------------
void vCalcBaseMenuView::instance_signals()
{
        connect(inherited::at(0),        SIGNAL(triggered(bool)),                SLOT(on_visible_action_weight(bool)));
        connect(inherited::at(1),        SIGNAL(triggered(bool)),                SLOT(on_visible_action_rep(bool)));
        connect(inherited::at(2),        SIGNAL(triggered(bool)),                SLOT(on_visible_action_calculate(bool)));
        connect(window_weight_,          SIGNAL(visibilityChanged (bool)),       SLOT(on_visible_window_weight(bool)));
        connect(window_rep_,             SIGNAL(visibilityChanged(bool)),        SLOT(on_visible_window_rep(bool)));
        connect(window_calculate_,       SIGNAL(visibilityChanged(bool)),        SLOT(on_visible_window_calculate(bool)));
}
//------------------------------------------------------------------------------
bool vCalcBaseMenuView::test()
{
        Q_ASSERT(inherited::test());
        Q_ASSERT(window_weight_);
        Q_ASSERT(window_rep_);
        Q_ASSERT(window_calculate_);
        Q_ASSERT(actions().size() >2);
        Q_ASSERT(front()->isCheckable());
        Q_ASSERT(actions()[1]->isCheckable());
        Q_ASSERT(back()->isCheckable());
        return true;
}
//------------------------------------------------------------------------------