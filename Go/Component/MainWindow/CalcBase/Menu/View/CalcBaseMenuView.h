/* 
 * File:   vCalcBaseMenuView.h
 * Author: S.Panin
 *
 * Created on Пн апр. 26 2010, 01:54:24
 */
//------------------------------------------------------------------------------
#ifndef _V_CALC_BASE_MENU_VIEW_H_
#define	_V_CALC_BASE_MENU_VIEW_H_
//------------------------------------------------------------------------------
class vCalcBaseMenuView : public go::vMenuView
{
Q_OBJECT

    typedef                         go::vMenuView                               inherited;
    typedef                         vCalcBaseMenuView                           class_name;
    typedef                         QMenu                                       menu_type;
    typedef                         QDockWidget                                 window_type;

public:
    typedef                         inherited::value_type                       value_type;
    typedef                         inherited::const_reference                  const_reference;

private:
    window_type*                                                                window_weight_;
    window_type*                                                                window_rep_;
    window_type*                                                                window_calculate_;

    void                            create_actions();
    void                            instance_signals();

private slots: 
     void                           on_visible_action_weight(bool visible)      { window_weight_->setVisible(visible); }
     void                           on_visible_action_rep(bool visible)         { window_rep_->setVisible(visible); }
     void                           on_visible_action_calculate(bool visible)   { window_calculate_->setVisible(visible); }
     void                           on_visible_window_weight(bool visible)      { inherited::at(0)->setChecked(visible); }
     void                           on_visible_window_rep(bool visible)         { inherited::at(1)->setChecked(visible); }
     void                           on_visible_window_calculate(bool visible)   { inherited::at(2)->setChecked(visible); }

public:
    explicit                        vCalcBaseMenuView(window_type* first, window_type* second, window_type* calculate, QWidget* parent);
    void                            set_action_text(size_t index, const QString& str)   { menu_type::actions()[index]->setText(str); }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_CALC_BASE_MENU_VIEW_H_ */
