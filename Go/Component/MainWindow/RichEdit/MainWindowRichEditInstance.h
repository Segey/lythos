/* 
 * File:   MainWindowRichEditInstance.h
 * Author: S.Panin
 *
  * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_WINDOW_RICH_EDIT_INSTANCE_H_
#define	_V_MAIN_WINDOW_RICH_EDIT_INSTANCE_H_
//------------------------------------------------------------------------------
class vMainWindowRichEdit;
//------------------------------------------------------------------------------
class vMainWindowRichEditInstance
{
    typedef                     vMainWindowRichEditInstance                     class_name;
    typedef                     vMainWindowRichEdit*                            inherited;

    inherited                                                                   parent_;
    
    void                        createFileMenu();
    void                        createEditMenu();
    void                        createFormatMenu();
    void                        instanceHelpMenu();

protected:
    void                        instance_form();
    void                        instance_widgets();
    void                        read_options()                                  {}

public:
    explicit                    vMainWindowRichEditInstance(inherited parent) : parent_(parent)              {}
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_WINDOW_RICH_EDIT_INSTANCE_H_ */
