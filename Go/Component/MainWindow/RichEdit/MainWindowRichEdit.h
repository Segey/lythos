/* 
 * File:   MainWindowRichEdit.h
 * Author: S.Panin
 *
 * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_WINDOW_RICH_EDIT_H_
#define	_V_MAIN_WINDOW_RICH_EDIT_H_
//------------------------------------------------------------------------------
#include "../../../Instance/MainWindowInstance.h"
#include "../../Menu/Help/MenuHelp.h"
#include "../../Section/Edit/SectionEdit.h"
#include "../../Section/Exit/SectionExit.h"
#include "../../Section/Font/SectionFont.h"
#include "../../Section/Print/SectionPrint.h"
#include "../../Section/Style/SectionStyle.h"
#include "../../Section/Bullet/SectionBullet.h"
#include "../../Section/UndoRedo/SectionUndoRedo.h"
#include "../../Section/File/Text/SectionFileText.h"
#include "../../Section/Alignment/SectionAlignment.h"
#include "MainWindowRichEditInstance.h"
//------------------------------------------------------------------------------
class vMainWindowRichEdit :  public QMainWindow
{
Q_OBJECT

friend class vMainWindowRichEditInstance;
    typedef                         QMainWindow                                 inherited;
    typedef                         vMainWindowRichEdit                         class_name;

    QTextEdit*                                                                  text_edit_;
    
    vSectionFileText*                                                           file_section_;
    vSectionPrint*                                                              print_section_;
    vSectionExit*                                                               exit_section_;

    vSectionUndoRedo*                                                           undo_section_;
    vSectionEdit*                                                               edit_section_;
    
    vSectionStyle*                                                              style_section_;
    vSectionAlignment*                                                          align_section_;
    vSectionFont*                                                               font_section_;
    vSectionBullet*                                                             bullet_section_;

    QMenu*                                                                      file_menu_;
    QMenu*                                                                      edit_menu_;
    QMenu*                                                                      format_menu_;
    vMenuHelp*                                                                  help_menu_;

    static QString                  settings_table()                            { return ("RichEdit"); }
    static QString                  settings_key_state()                        { return ("FormState"); }
    static QString                  settings_key_form()                         { return ("FormSize"); }
    void                            settings_read();
    void                            settings_write() const;
    
    void                            instance_signals();

protected:
    virtual void                    save(const QString& text)                   =0;
    virtual void                    closeEvent(QCloseEvent *event);

private slots:
    void                            textChanged();
    void                            cursorPositionChanged();
    void                            currentCharFormatChanged(const QTextCharFormat& format);
    void                            save_execute(const QString& text);
    void                            window_modified(bool value);

public:
    explicit                        vMainWindowRichEdit(QWidget* parent = 0);
    virtual                         ~vMainWindowRichEdit()                      { }
    void                            load_file(const QString& file_name);
    void                            load(const QString& text);
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_WINDOW_RICH_EDIT_H_ */
