/* 
 * File:   MainWindowRichEditInstance.cpp
 * Author: S.Panin
 * 
 * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#include "MainWindowRichEditInstance.h"
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::instance_form()
{
        parent_->setWindowIcon(const_icon_16::program());
        parent_->setMinimumSize(QDesktopWidget().size()*0.75);
        parent_->setStatusBar(new QStatusBar);
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::instance_widgets()
{
        parent_->text_edit_ = new QTextEdit;
        parent_->setCentralWidget(parent_->text_edit_);

        createFileMenu();
        createEditMenu();
        createFormatMenu();
        instanceHelpMenu();
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::createFileMenu()
{
        parent_->file_menu_ = new QMenu(parent_);
        parent_->menuBar()->addMenu(parent_->file_menu_);

        parent_->file_section_ = new vSectionFileText(parent_->text_edit_);
        parent_->file_section_->instance();
        parent_->addToolBar(parent_->file_section_->toolbar());
        parent_->file_menu_->addActions(parent_->file_section_->actions());
        parent_->file_menu_->addSeparator();

        parent_->print_section_ = new vSectionPrint(parent_->text_edit_);
        parent_->print_section_->instance();
        parent_->addToolBar(parent_->print_section_->toolbar());
        parent_->file_menu_->addActions(parent_->print_section_->actions());
        parent_->file_menu_->addSeparator();

        parent_->exit_section_ = new vSectionExit(parent_);
        parent_->exit_section_->instance();
        parent_->file_menu_->addActions(parent_->exit_section_->actions());
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::createEditMenu()
{
        parent_->edit_menu_ = new QMenu(parent_);
        parent_->menuBar()->addMenu(parent_->edit_menu_);

        parent_->undo_section_ = new vSectionUndoRedo(parent_->text_edit_);
        parent_->undo_section_->instance();
        parent_->addToolBar(parent_->undo_section_->toolbar());
        parent_->edit_menu_->addActions(parent_->undo_section_->actions());
        parent_->edit_menu_->addSeparator();

        parent_->edit_section_ = new class vSectionEdit(parent_->text_edit_);
        parent_->edit_section_->instance();
        parent_->addToolBar(parent_->edit_section_->toolbar());
        parent_->edit_menu_->addActions(parent_->edit_section_->actions());
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::createFormatMenu()
{
        parent_->format_menu_ = new QMenu(parent_);
        parent_->menuBar()->addMenu(parent_->format_menu_);

        parent_->style_section_ = new vSectionStyle(parent_->text_edit_);
        parent_->style_section_->instance();
        parent_->addToolBar(parent_->style_section_->toolbar());
        parent_->format_menu_->addActions(parent_->style_section_->actions());
        parent_->format_menu_->addSeparator();

        if(QApplication::isLeftToRight()) parent_->align_section_ = new vSectionAlignmentLeft(parent_->text_edit_);
        else parent_->align_section_ = new vSectionAlignmentRight(parent_->text_edit_);
        parent_->align_section_->instance();
        parent_->addToolBar(parent_->align_section_->toolbar());
        parent_->format_menu_->addActions(parent_->align_section_->actions());
        parent_->format_menu_->addSeparator();

        parent_->addToolBarBreak(Qt::TopToolBarArea);

        QToolBar* toolbar_font = new QToolBar(parent_);
        parent_->font_section_ = new vSectionFont(parent_->text_edit_);
        parent_->font_section_->instance_on_toolbar(toolbar_font);
        parent_->addToolBar(toolbar_font);

        QToolBar* toolbar_bullet = new QToolBar(parent_);
        parent_->bullet_section_ = new vSectionBullet(parent_->text_edit_);
        parent_->bullet_section_->instance_on_toolbar(toolbar_bullet);
        parent_->addToolBar(toolbar_bullet);
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::instanceHelpMenu()
{
        parent_->help_menu_ = new vMenuHelp(parent_);
        parent_->help_menu_->instance();
        parent_->menuBar()->addMenu(parent_->help_menu_);
}
//------------------------------------------------------------------------------
void vMainWindowRichEditInstance::translate()
{
        parent_->file_menu_->setTitle(vtr::Button::tr("&File"));
        parent_->edit_menu_->setTitle(vtr::Button::tr("&Edit"));
        parent_->format_menu_->setTitle(vtr::Button::tr("F&ormat"));
        parent_->setWindowTitle(QString(program::name_full()) + "[*]");
}
