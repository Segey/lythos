/* 
 * File:   RichEditSimple.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_WINDOW_RICH_EDIT_SIMPLE_H_
#define	_V_MAIN_WINDOW_RICH_EDIT_SIMPLE_H_
//------------------------------------------------------------------------------
#include "../MainWindowRichEdit.h"
//------------------------------------------------------------------------------
class vMainWindowRichEditSimple : public vMainWindowRichEdit
{
    typedef                             vMainWindowRichEdit                     inherited;
    typedef                             vMainWindowRichEditSimple               class_name;

    QTextEdit*                                                                  text_edit_;
    
protected :
    virtual void                        save(const QString& text)               { text_edit_->document()->setHtml(text); }

public:
    explicit                            vMainWindowRichEditSimple(QTextEdit* text_edit, QWidget* parent = 0) : inherited(parent), text_edit_(text_edit)  {}
    bool test()                                                                 { Q_ASSERT(text_edit_);   inherited::test();    return true;}
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_WINDOW_RICH_EDIT_SIMPLE_H_ */
