/* 
 * File:   MainWindowRichEdit.cpp
 * Author: S.Panin
 * 
 * Created on Чт мая 13 2010, 16:36:51
 */
//------------------------------------------------------------------------------
#include "MainWindowRichEdit.h"
//------------------------------------------------------------------------------
vMainWindowRichEdit::vMainWindowRichEdit(QWidget* parent /*=0*/)
                : QMainWindow(parent)
{      
        ::vMainWindowInstance< vMainWindowRichEditInstance > (this).instance();
        
        instance_signals();
        settings_read();
        inherited::setWindowModified(false);        
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::settings_read()
{
        QRect rect =  vDialog::form_size_calculation(this);
        boost::tuple<bool, QByteArray, QRect> array = vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), rect));
        inherited::restoreState(array.get<1>());
        inherited::setGeometry(array.get<2>());
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::settings_write() const
{
        vSettings::set(settings_table(), settings::make(settings_key_state(), inherited::saveState()), settings::make_form_size(settings_key_form(), inherited::geometry()));
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::load(const QString& text)
{
        file_section_->load(text);
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::instance_signals()
{
        connect(text_edit_,     SIGNAL(textChanged()), SLOT(textChanged()));
        connect(text_edit_,     SIGNAL(cursorPositionChanged()), SLOT(cursorPositionChanged()));
        connect(text_edit_,     SIGNAL(currentCharFormatChanged(const QTextCharFormat&)), SLOT(currentCharFormatChanged(const QTextCharFormat&)));
        connect(file_section_,  SIGNAL(on_save(const QString&)), SLOT(save_execute(const QString&)));
        connect(file_section_,  SIGNAL(on_window_modified(bool)), SLOT(window_modified(bool)));
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::window_modified(bool value)
{
        inherited::setWindowModified(value);
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::save_execute(const QString& text)
{
        save(text);
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::textChanged()
{
        file_section_->text_changed();
        window_modified(true);;
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::cursorPositionChanged()
{
        align_section_->alignment_changed(text_edit_->alignment());
        bullet_section_->style_changed(text_edit_->textCursor().currentList());
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::currentCharFormatChanged(const QTextCharFormat& format)
{
        font_section_->font_changed(format.font());
        style_section_->font_changed(format.font());
        bullet_section_->font_changed(format.foreground().color());
}
//------------------------------------------------------------------------------
void vMainWindowRichEdit::load_file(const QString& file_name)
{
        if(!QFile::exists(file_name)) return;
        file_section_->load_file(file_name);
}
//------------------------------------------------------------------------------
/*virtual*/ void vMainWindowRichEdit::closeEvent(QCloseEvent *event)
{       
        settings_write();
        if(!isWindowModified()) return;

        QMessageBox::StandardButton  click_button =QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Save changes to the document before quitting?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (click_button == QMessageBox::Cancel) event->ignore();
        if (click_button == QMessageBox::Save) { QApplication::processEvents(); file_section_->Save();}
}
//------------------------------------------------------------------------------
bool vMainWindowRichEdit::test()
{
        Q_ASSERT(!file_menu_    ->isEmpty());
        Q_ASSERT(!edit_menu_    ->isEmpty());
        Q_ASSERT(!format_menu_  ->isEmpty());
        Q_ASSERT(file_menu_     ->actions().count() == 9);
        Q_ASSERT(edit_menu_     ->actions().count() == 7);
        Q_ASSERT(format_menu_   ->actions().count() == 10);
        Q_ASSERT(file_section_  ->test());
        Q_ASSERT(print_section_ ->test());
        Q_ASSERT(exit_section_  ->test());
        Q_ASSERT(undo_section_  ->test());
        Q_ASSERT(edit_section_  ->test());
        Q_ASSERT(style_section_ ->test());
        Q_ASSERT(align_section_ ->test());
        Q_ASSERT(font_section_  ->test());
        Q_ASSERT(bullet_section_->test());
        Q_ASSERT(help_menu_     ->test());

        vMain::user().set_test_id();
        QByteArray array(inherited::saveState());
        QRect rect =  vDialog::form_size_calculation(this);
        Q_ASSERT( vSettings::set(settings_table(), settings::make(settings_key_state(), array), settings::make_form_size(settings_key_form(), rect)) );
        settings_write();
        settings_read();
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<0>() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<1>() == array );
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<2>() == rect );
        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------