/* 
 * File:   WidgetImage.cpp
 * Author: S.Panin
 * 
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#include "WidgetImage.h"
//------------------------------------------------------------------------------
vWidgetImage::vWidgetImage(QWidget* parent /*=0*/)
                : QWidget(parent), file_name_(QString()), image_(0)
{      
        instance_widgets();
        instance_layout();
        inherited::setWindowTitle("[*]");
}
//------------------------------------------------------------------------------
void vWidgetImage::instance_widgets()
{
        inherited::setAutoFillBackground(true);
        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(showContextMenu(const QPoint &)));

        image_ = new QLabel;
        image_->setPixmap( const_pixmap_default::user() );
        image_->setFrameShape(QFrame::StyledPanel);
        image_->setScaledContents(true);
}
//------------------------------------------------------------------------------
void vWidgetImage::instance_layout()
{
        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(image_);

        inherited::setMaximumSize(sizeHint());
}
//------------------------------------------------------------------------------
void vWidgetImage::showContextMenu(const QPoint &position)
{
        QMenu pum (this);
        createActionUpdateImage(&pum);
        pum.addSeparator();
        createActionClearImage(&pum);
        pum.exec(image_->mapToGlobal(position));
}
//------------------------------------------------------------------------------
void vWidgetImage::createActionUpdateImage(QMenu* pum)
{
        QAction* update_image_ =vAction::createNewImage(this);
        connect(update_image_, SIGNAL(triggered(bool)), SLOT(updateImage()));
        pum->addAction(update_image_);
}
//------------------------------------------------------------------------------
void vWidgetImage::createActionClearImage(QMenu* pum)
{
        QAction* clear_image_ =vAction::createClearImage(this);
        connect(clear_image_, SIGNAL(triggered(bool)), SLOT(clearImage()));
        pum->addAction(clear_image_);
        if (file_name_.isEmpty()) clear_image_->setEnabled(false);
}
//------------------------------------------------------------------------------
void vWidgetImage::updateImage()
{
        QString filename = QFileDialog::getOpenFileName(0, vtr::Dialog::tr("Open"), vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<1>(), vDialog::getReadWriteImageFilters());
        if (filename.isEmpty()) return;
        vSettings::set(settings_table(), settings::make(settings_key(), filename));
        file_name_ =filename;
        image_->setPixmap(QPixmap(file_name_));
        on_update_image_(file_name_);
        inherited::setWindowModified(true);
}
//------------------------------------------------------------------------------
void vWidgetImage::clearImage()
{
        if( QMessageBox::warning(image_, program::name_full(), vtr::Message::tr("Are you sure you want to send the image to the Recycle Bin?!"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes) return;
        image_->setPixmap(QPixmap(get_default_image_name()));
        file_name_.clear();
        on_image_clear_();
        inherited::setWindowModified(true);
}
//------------------------------------------------------------------------------
void vWidgetImage::set_pixmap(const QPixmap& pixmap)
{
        file_name_ = pixmap.isNull() ? "" : get_default_image_name();
        pixmap.isNull() ?  image_->setPixmap(QPixmap(get_default_image_name())) : image_->setPixmap(pixmap);
}
//------------------------------------------------------------------------------
bool vWidgetImage::test()
{
        Q_ASSERT(image_);
        Q_ASSERT(!image_->pixmap()->isNull());
        
        Q_ASSERT(file_name_ == "");
        set_pixmap(0);
        Q_ASSERT(file_name_ ==  "");

        vMain::user().set_test_id();
        vSettings::set(settings_table(), settings::make(settings_key(), QString("print")));
        Q_ASSERT(vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<0>());
        Q_ASSERT(vSettings::get(settings_table(), settings::make(settings_key(), QString())).get<1>() == "print");

        QSqlQuery query(vMain::settingsbd().database());
        return query.exec("DELETE FROM Path WHERE UserID =0");
}
//------------------------------------------------------------------------------