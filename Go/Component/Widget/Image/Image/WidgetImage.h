/* 
 * File:   WidgetImage.h
 * Author: S.Panin
 *
 * Created on 19 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_IMAGE_H_
#define	_V_WIDGET_IMAGE_H_
//------------------------------------------------------------------------------
#include "../../../../BuiltType/Dialog.h"
#include "../../../../BuiltType/Action/Action.h"
#include "../../../../Classes/BD/Settings/Settings.h"
//------------------------------------------------------------------------------
class vWidgetImage : public QWidget
{
Q_OBJECT
    typedef                     boost::signals2::signal<void ()>                signal_image_clear;
    typedef                     boost::signals2::signal<void (const QString&)>  signal_update_image;

    typedef                     QWidget                                         inherited;
    typedef                     vWidgetImage                                    class_name;

    static QString              settings_table()                                { return "Path"; }
    static QString              settings_key()                                  { return "DirImage"; }

public:
    signal_image_clear                                                          on_image_clear_;
    signal_update_image                                                         on_update_image_;
    QString                                                                     file_name_;
    QLabel*                                                                     image_;

    void                        instance_widgets();
    void                        instance_layout();
    void                        createActionUpdateImage(QMenu* pum);
    void                        createActionClearImage(QMenu* pum);
    static QString              get_default_image_name()                        {return ":/default/user.png";}

private slots:
    void                        showContextMenu(const QPoint &position);
    void                        updateImage();
    void                        clearImage();        
   
public:
    explicit                    vWidgetImage(QWidget* parent = 0);
    virtual                     ~vWidgetImage()                                  {}
    void                        connect_clear_image(signal_image_clear::slot_type slot) {on_image_clear_.connect(slot);}
    void                        connect_update_image(signal_update_image::slot_type slot) {on_update_image_.connect(slot);}
    const QString&              file_name() const                               {return file_name_;}
    void                        set_pixmap(const QPixmap& pixmap);
    QLabel*                     image()                                         {return image_;}
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_WIDGET_IMAGE_H_ */
