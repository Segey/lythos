/* 
 * File:   ImageAddInstance.h
 * Author: S.Panin
 *
 * Created on 17 Май 2010 г., 23:17
 */
//------------------------------------------------------------------------------
#ifndef _V_IMAGE_ADD_INSTANCE_H_
#define	_V_IMAGE_ADD_INSTANCE_H_
//------------------------------------------------------------------------------
struct vImageAddInstance : private go::noncopyable
{
    typedef                     vImageAddInstance                               class_name;

    QLabel*                                                                     label_;
    QLineEdit*                                                                  edit_;
    QPushButton*                                                                button_;

    void                        instance_form()                                 {}
    void                        instance_widgets();
    QBoxLayout*                 instance_top_layout()                           { return 0; }
    QBoxLayout*                 instance_middle_layout();
    QBoxLayout*                 instance_bottom_layout()                        { return 0; }
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_ADD_INSTANCE_H_ */
