/* 
 * File:   ImageAdd.h
 * Author: S.Panin
 *
 * Created on 17 Май 2010 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_IMAGE_ADD_H_
#define	_V_IMAGE_ADD_H_
//------------------------------------------------------------------------------
#include "/home/dix75/Projects/Go/Instance/Instance2.h"
#include "/home/dix75/Projects/Go/BuiltType/Dialog.h"
#include "ImageAddInstance.h"
//------------------------------------------------------------------------------
class vImageAdd : public QWidget, protected vImageAddInstance
{
Q_OBJECT
    
    typedef                         QWidget                                     class_widget;
    typedef                         vImageAdd                                   class_name;
    typedef                         vImageAddInstance                           class_instance;

private:
    QString                                                                     path_;
    QString                                                                     image_filename_;

    void                            instance_signals();
    
protected:
    explicit                        vImageAdd(QWidget* parent = 0);

private slots:
    void                            condition_changed();
    virtual void                    open_dialog();

signals:
    void                            on_condition_changed();
    void                            on_condition_restored();
    void                            on_added_correct_image_path();

public:
    static  vImageAdd*              makeImage(QWidget* parent = 0);
    static  vImageAdd*              makeIcon(QWidget* parent = 0);
    bool                            is_image_empty() const                      { return edit_->text().isEmpty(); }
    bool                            is_image_exists() const                     { return QFile::exists( edit_->text()); }
    void                            set_default_path(const QString& str)        { path_ = str;}
    void                            set_image_filename(const QString& str)      { image_filename_ =str;  edit_->setText(str);}
    QString                         image_filename() const                      { return edit_->text();}
    bool                            is_condition_restored() const               { return image_filename_ == image_filename(); }
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_ADD_H_ */
