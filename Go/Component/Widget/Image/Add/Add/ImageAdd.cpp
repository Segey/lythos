/* 
 * File:   ImageAdd.cpp
 * Author: S.Panin
 * 
 * Created on 17 Май 2010 г., 22:15
 */
//------------------------------------------------------------------------------
#include "ImageAdd.h"
//------------------------------------------------------------------------------
vImageAdd*  vImageAdd::makeImage(QWidget* parent)
{
        vImageAdd* result = new vImageAdd(parent);
        result->label_->setText(vtr::Image::tr("&Image:"));
        return result;
}
//------------------------------------------------------------------------------
vImageAdd*  vImageAdd::makeIcon(QWidget* parent)
{
        vImageAdd* result = new vImageAdd(parent);
        result->label_->setText(vtr::Image::tr("&Icon:"));
        return result;
}
//------------------------------------------------------------------------------
vImageAdd::vImageAdd(QWidget* parent/*=0*/)
        : class_widget(parent)
{    
//        v_instance::instance< class_widget, class_instance > (this, this);
        instance_signals();
}
//------------------------------------------------------------------------------
void vImageAdd::instance_signals()
{
        connect(button_,        SIGNAL(clicked(bool)),                  SLOT(open_dialog()));
        connect(edit_,          SIGNAL(editingFinished()),              SLOT(condition_changed()));
        connect(edit_,          SIGNAL(returnPressed()),                SLOT(condition_changed()));
        connect(edit_,          SIGNAL(textChanged ( const QString& )), SLOT(condition_changed()));
}
//------------------------------------------------------------------------------
void vImageAdd::open_dialog()
{
//        QString filename = QFileDialog::getOpenFileName(0, vtr::Dialog::tr("Add a image"), vSettings::get(settings_table(), settings::make(settings_key(), QString())).get < 1 > (), vDialog::getReadWriteImageFilters());
//        if(filename.isEmpty()) return;
//        vSettings::set(settings_table(), settings::make(settings_key(), filename));
        QString filename = QFileDialog::getOpenFileName(0, vtr::Dialog::tr("Add a image"), path_, vDialog::getReadWriteImageFilters());
        path_ =filename;
        edit_->setText(filename);
}
//------------------------------------------------------------------------------
void vImageAdd::condition_changed()
{
        if( is_image_exists())          emit( on_added_correct_image_path() );
        if( is_condition_restored() )   emit( on_condition_restored() );
        emit( on_condition_changed() );
}
//------------------------------------------------------------------------------
bool vImageAdd::test()
{
        Q_ASSERT(button_);
        Q_ASSERT(edit_);
        Q_ASSERT(label_);
        Q_ASSERT(false ==label_->text().isEmpty());
        Q_ASSERT(label_->buddy());
        const QString example =edit_->text();
        Q_ASSERT("..." ==button_->text() );
        set_image_filename("892");
        Q_ASSERT("892" ==image_filename() );
        set_image_filename(example);
        return true;
}
//------------------------------------------------------------------------------