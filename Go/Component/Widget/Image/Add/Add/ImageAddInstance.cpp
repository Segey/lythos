/* 
 * File:   ImageAddInstance.cpp
 * Author: S.Panin
 * 
 * Created on 17 Май 2010 г., 23:17
 */
//------------------------------------------------------------------------------
#include "ImageAddInstance.h"
//------------------------------------------------------------------------------
void vImageAddInstance::instance_widgets()
{
        label_  = new QLabel;
        edit_   = new QLineEdit;
        button_ = new QPushButton;

        label_->setBuddy(button_);
}
//------------------------------------------------------------------------------
QBoxLayout* vImageAddInstance::instance_middle_layout()
{
        QHBoxLayout* lay = new QHBoxLayout;
        lay->addWidget(label_);
        lay->addWidget(edit_);
        lay->addWidget(button_);
        return lay;
}
//------------------------------------------------------------------------------
void vImageAddInstance::translate()
{
        button_->setText("...");
}
