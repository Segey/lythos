/* 
 * File:   ImageAdd.cpp
 * Author: S.Panin
 * 
 * Created on 17 Май 2010 г., 22:15
 */
//------------------------------------------------------------------------------
#include "ImageAddUser.h"
//------------------------------------------------------------------------------
vImageAddUser::make_name  vImageAddUser::makeImage(QWidget* parent)
{
        make_name result = make_name(new vImageAddUser(parent));
        result->label_->setText(vtr::Image::tr("&Image:"));
        return result;
}
//------------------------------------------------------------------------------
vImageAddUser::make_name  vImageAddUser::makeIcon(QWidget* parent)
{
        make_name result = make_name(new vImageAddUser(parent));
        result->label_->setText(vtr::Image::tr("&Icon:"));
        return result;
}
//------------------------------------------------------------------------------
void vImageAddUser::open_dialog()
{
        QString filename = QFileDialog::getOpenFileName(0, vtr::Dialog::tr("Add a image"), vSettings::get(settings_table(), settings::make(settings_key(), QString())).get < 1 > (), vDialog::getReadWriteImageFilters());
        if(filename.isEmpty()) return;
        vSettings::set(settings_table(), settings::make(settings_key(), filename));
        edit_->setText(filename);
        condition_changed();
}
//------------------------------------------------------------------------------
bool vImageAddUser::test()
{
        Q_ASSERT(inherited::test());
        Q_ASSERT(button_);
        Q_ASSERT(edit_);
        Q_ASSERT(label_);
        Q_ASSERT(false ==label_->text().isEmpty());
        Q_ASSERT(label_->buddy());
        const QString example =edit_->text();
        Q_ASSERT("..." ==button_->text() );
        set_image_file_name("82");
        Q_ASSERT("82" ==image_file_name() );
        set_image_file_name(example);
        return true;
}
//------------------------------------------------------------------------------