/* 
 * File:   ImageAdd.h
 * Author: S.Panin
 *
 * Created on 17 Май 2010 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _V_IMAGE_ADD_USER_H_
#define	_V_IMAGE_ADD_USER_H_
//------------------------------------------------------------------------------
#include "../Add/ImageAdd.h"
//------------------------------------------------------------------------------
class vImageAddUser : public vImageAdd
{
Q_OBJECT
    
    typedef                         vImageAdd                                   inherited;
    typedef                         vImageAddUser                               class_name;

public:
    typedef                         boost::shared_ptr<class_name>               make_name;

protected:
    explicit                        vImageAddUser(QWidget* parent = 0) : inherited(parent) {}

private slots:
    virtual void                    open_dialog();

public:
    static  make_name               makeImage(QWidget* parent = 0);
    static  make_name               makeIcon(QWidget* parent = 0);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_ADD_USER_H_ */
