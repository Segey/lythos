// -*- C++ -*-
/* 
 * File:   WidgetLine.h
 * Author: S.Panin
 *
 * Created on 22 Февраль 2010 г., 23:30
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_LINE_H_
#define	_V_WIDGET_LINE_H_
//------------------------------------------------------------------------------
struct vWidgetLine
{
    static QFrame*          VLine(QWidget* parent)
    {
        QFrame* line = new QFrame(parent);
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line->setObjectName(QString::fromUtf8("vLine_frame"));
        return line;
    }
    
     static QFrame*          HLine(QWidget* parent)
    {
        QFrame* line = new QFrame(parent);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line->setObjectName(QString::fromUtf8("hLine_frame"));
        return line;
    }

    bool test()
    {
        QFrame* vline = VLine(NULL);
        Q_ASSERT(vline);
        Q_ASSERT(vline->frameShape() == QFrame::VLine);
        Q_ASSERT(vline->frameShadow() == QFrame::Sunken);
	
	QFrame* hline = HLine(NULL);
        Q_ASSERT(hline);
        Q_ASSERT(hline->frameShape() == QFrame::HLine);
        Q_ASSERT(hline->frameShadow() == QFrame::Sunken);

        delete vline;
        return true;
    }

};
//------------------------------------------------------------------------------
#endif	/* _V_WIDGET_LINE_H_ */

