/* 
 * File:   WidgetAnchor.cpp
 * Author: S.Panin
 * 
 * Created on 17 Май 2010 г., 22:15
 */
//------------------------------------------------------------------------------
#include "WidgetAnchor.h"
//------------------------------------------------------------------------------
vWidgetAnchor::vWidgetAnchor(QWidget* parent/*=0*/)
        : class_widget(parent)
{    
        v_instance::instance< class_widget, class_instance > (this, this);
        instance_signals();
}
//------------------------------------------------------------------------------
void vWidgetAnchor::instance_signals()
{
        connect(edit_anchor_item_, SIGNAL(editingFinished()),   SLOT(condition_changed()));
        connect(edit_anchor_item_, SIGNAL(returnPressed()),     SLOT(condition_changed()));
        connect(edit_anchor_item_, SIGNAL(textChanged ( const QString& )), SLOT(condition_changed()));
}
//------------------------------------------------------------------------------
void vWidgetAnchor::condition_changed()
{
        emit( on_condition_changed() );
        if ( is_condition_restored() ) emit( on_condition_restored() );
}
//------------------------------------------------------------------------------
bool vWidgetAnchor::test()
{
        Q_ASSERT(edit_anchor_item_);
        Q_ASSERT(edit_anchor_page_);
        Q_ASSERT(false ==label_anchor_item_->text().isEmpty());
        Q_ASSERT(label_anchor_item_->buddy());
        Q_ASSERT(false ==label_anchor_page_->text().isEmpty());
        Q_ASSERT(label_anchor_page_->buddy());

        const QString example ="909";
        set_anchor_item(example);
        Q_ASSERT(example ==anchor_item_old() );
        Q_ASSERT(example ==anchor_item_new() );
        Q_ASSERT( is_condition_restored() );

        const QString example2 ="9709";
        set_anchor_page(example2);
        Q_ASSERT(example2 ==anchor_page() );
        
        return true;
}
//------------------------------------------------------------------------------