/* 
 * File:   WidgetAnchor.h
 * Author: S.Panin
 *
 * Created on Чт мая 27 2010, 00:59:56
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_ANCHOR_H_
#define	_V_WIDGET_ANCHOR_H_
//------------------------------------------------------------------------------
#include "/home/dix75/Projects/Go/Instance/Instance2.h"
#include "WidgetAnchorInstance.h"
//------------------------------------------------------------------------------
class vWidgetAnchor : public QWidget, private vWidgetAnchorInstance
{
Q_OBJECT
    
    typedef                         QWidget                                     class_widget;
    typedef                         vWidgetAnchor                               class_name;
    typedef                         vWidgetAnchorInstance                       class_instance;

    QString                                                                     anchor_item_;

    void                            instance_signals();

private slots:
    void                            condition_changed();

signals:
    void                            on_condition_changed();
    void                            on_condition_restored();

public:
    explicit                        vWidgetAnchor(QWidget* parent = 0);
    bool                            migalo()                                    { return vInterfase::migalo(edit_anchor_item_); }
    void                            set_anchor_page(const QString& str)         { edit_anchor_page_->setText(str); }
    QString                         anchor_page() const                         { return edit_anchor_page_->text(); }
    void                            set_anchor_item(const QString& str)         { anchor_item_ =str; edit_anchor_item_->setText(str); }
    QString                         anchor_item_old() const                     { return anchor_item_; }
    QString                         anchor_item_new() const                     { return edit_anchor_item_->text(); }
    bool                            is_condition_restored() const               { return anchor_item_new() == anchor_item_old(); }
    bool                            is_anchor_new() const                       { return false ==is_condition_restored(); }
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_ADD_H_ */
