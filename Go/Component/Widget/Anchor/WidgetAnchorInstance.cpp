/* 
 * File:   WidgetAnchorInstance.cpp
 * Author: S.Panin
 * 
 * Created on 17 Май 2010 г., 23:17
 */
//------------------------------------------------------------------------------
#include "WidgetAnchorInstance.h"
//------------------------------------------------------------------------------
void vWidgetAnchorInstance::instance_widgets()
{
        label_anchor_item_      = new QLabel;
        edit_anchor_item_       = new QLineEdit;
        label_anchor_page_      = new QLabel;
        edit_anchor_page_       = new QLineEdit;

        edit_anchor_page_       ->setEnabled(false);
        instance_buddy();
}
//------------------------------------------------------------------------------
void vWidgetAnchorInstance::instance_buddy()
{
        label_anchor_item_      ->setBuddy(edit_anchor_item_);
        label_anchor_page_      ->setBuddy(edit_anchor_page_);
}
//------------------------------------------------------------------------------
QLayout* vWidgetAnchorInstance::instance_middle_layout()
{
        vLay lay(new QGridLayout, 4);
        lay.add_binary_label(label_anchor_item_, edit_anchor_item_, label_anchor_page_, edit_anchor_page_, false );
        return lay.clayout();
}
//------------------------------------------------------------------------------
void vWidgetAnchorInstance::translate()
{
        label_anchor_item_      ->setText(vtr::XML::tr("Anc&hor:"));
        label_anchor_page_      ->setText(vtr::XML::tr("Page anchor:"));
}
