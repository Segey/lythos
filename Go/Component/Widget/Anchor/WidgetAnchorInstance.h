/* 
 * File:   WidgetAnchorInstance.h
 * Author: S.Panin
 *
 * Created on 17 Май 2010 г., 23:17
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_ANCHOR_INSTANCE_H_
#define	_V_WIDGET_ANCHOR_INSTANCE_H_
//------------------------------------------------------------------------------
struct vWidgetAnchorInstance : private go::noncopyable
{
    typedef                     vWidgetAnchorInstance                           class_name;

    QLabel*                                                                     label_anchor_item_;
    QLineEdit*                                                                  edit_anchor_item_;
    QLabel*                                                                     label_anchor_page_;
    QLineEdit*                                                                  edit_anchor_page_;

    void                        instance_form()                                 {}
    void                        instance_buddy();
    void                        instance_widgets();
    QLayout*                    instance_top_layout()                           { return 0; }
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout()                        { return 0; }
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_ADD_INSTANCE_H_ */
