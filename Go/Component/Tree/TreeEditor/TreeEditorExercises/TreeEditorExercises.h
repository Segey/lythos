/* 
 * File:   TreeEditorExercises.h
 * Author: S.Panin
 *
 * Created on Вт мая 11 2010, 11:56:00
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_EDITOR_EXERCISES_H_
#define	_V_TREE_EDITOR_EXERCISES_H_
//------------------------------------------------------------------------------
#include "../TreeEditor/TreeEditor.h"
//------------------------------------------------------------------------------
class vTreeEditorExercises : public vTreeEditor
{
Q_OBJECT

    typedef                     QTreeWidget                                     tree_widget;
    typedef                     vTreeEditor                                     inherited;
    typedef                     vTreeEditorExercises                            class_name;

public:
    explicit vTreeEditorExercises(QTreeWidget* parent = 0): inherited (parent)
    {
        tree_widget::setObjectName(QString::fromUtf8("tree_editor_exercises"));
        inherited::set_name_add_group(vtr::Exercise::tr("New Group of Exercises"), vtr::Exercise::tr("New Group of Exercises"));
        inherited::set_name_add_item(vtr::Exercise::tr("New Exercise"), vtr::Exercise::tr("New Exercise"));
        inherited::set_name_erase_group(vtr::Exercise::tr("Remove Group"), vtr::Exercise::tr("Remove Group"));
        inherited::set_name_erase_item(vtr::Exercise::tr("Remove Exercise"), vtr::Exercise::tr("Remove Exercise"));
        inherited::set_name_default_item(vtr::Body::tr("Expercise"));
    }
    virtual                    ~vTreeEditorExercises()                          {}

    bool test()
    {
        return inherited::test();
    }

};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_EDITOR_EXERCISES_H_ */
