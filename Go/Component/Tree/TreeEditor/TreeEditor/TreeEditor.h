/* 
 * File:   vTreeEditor.h
 * Author: S.Panin
 *
 * Created on 10 Май 2010 г., 23:12
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_EDITOR_H_
#define	_V_TREE_EDITOR_H_
//------------------------------------------------------------------------------
#include "PuM/TreeEditorPuM.h"
//------------------------------------------------------------------------------
class vTreeEditor : public QTreeWidget
{
Q_OBJECT
    typedef                     QTreeWidget                                     inherited;
    typedef                     vTreeEditor                                     class_name;
    typedef                     std::vector<QString>                            data_item;

    QString                                                                     name_add_group_;
    QString                                                                     description_add_group_;
    QString                                                                     name_add_item_;
    QString                                                                     description_add_item_;
    QString                                                                     name_erase_group_;
    QString                                                                     description_erase_group_;
    QString                                                                     name_erase_item_;
    QString                                                                     description_erase_item_;
    QString                                                                     default_name_item_;

    void                        instance();
    void                        instance_signals();

    
private slots:
    void                        click_show_context_menu(const QPoint& );
    void                        counts();
    
signals:
    void                        on_items_changed(int group, int item);
    void                        on_click_new_group(const QString& name_group);
    void                        on_click_new_item(const QString& name_group, const QString& name_item);
    void                        on_click_edit_group(const QString& name_old, const QString& name_new);
    void                        on_click_edit_item(const QString& name_group, const QString& name_item_old, const QString& name_item_new);
    void                        on_click_erase_group(const QString& name_group);
    void                        on_click_erase_item(const QString& name_group, const QString& name_item);

public:
    explicit                    vTreeEditor(QTreeWidget* parent = 0);
    virtual                    ~vTreeEditor()                                   {}
    void                        set_title(const QString& text)                  { inherited::model()->setHeaderData(0, Qt::Horizontal, QVariant(text), Qt::EditRole); }
    void                        set_name_add_group(const QString& name, const QString& description)            { name_add_group_ = name; description_add_group_= description;}
    void                        set_name_add_item(const QString& name, const QString& description)             { name_add_item_ = name; description_add_item_= description;}
    void                        set_name_erase_group(const QString& name, const QString& description)          { name_erase_group_ = name; description_erase_group_= description;}
    void                        set_name_erase_item(const QString& name, const QString& description)           { name_erase_item_ = name; description_erase_item_= description;}
    void                        set_name_default_item(const QString& name)                                     { default_name_item_ = name; }
    void                        add_data(const QString& group_name, const data_item& items);

    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_EDITOR_H_ */
