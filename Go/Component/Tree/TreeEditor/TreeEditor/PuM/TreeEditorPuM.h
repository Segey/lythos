/* 
 * File:   TreeEditorPuM.h
 * Author: S.Panin
 *
 * Created on 7 Май 2010 г., 22:28
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_EDITOR_PUM_H_
#define	_V_TREE_EDITOR_PUM_H_
//------------------------------------------------------------------------------
#include "../../../../../BuiltType/Action/WithText/ActionWithText.h"
//------------------------------------------------------------------------------
class vTreeEditorPuM : public QMenu
{
Q_OBJECT

    typedef                     QMenu                                           inherited;
    typedef                     vTreeEditorPuM                                  class_name;
    typedef                     QTreeView                                       parent_name;

    parent_name*                                                                parent_;
    QString                                                                     name_add_group_;
    QString                                                                     description_add_group_;
    QString                                                                     name_add_item_;
    QString                                                                     description_add_item_;
    QString                                                                     name_erase_group_;
    QString                                                                     description_erase_group_;
    QString                                                                     name_erase_item_;
    QString                                                                     description_erase_item_;
    QString                                                                     default_name_item_;

    std::pair<bool,QString>     get_name(const QString& title, const QString& name, const QString& default_text);
    void                        instance_group();
    void                        instance_item();
    bool                        enabled_action_group() const;

    void                        instance_action_new_group();
    void                        instance_action_new_item();
    void                        instance_action_edit_group();
    void                        instance_action_edit_item();
    void                        instance_action_erase_group();
    void                        instance_action_erase_item();

signals:
    void                        on_click_new_group(const QString& name_group);
    void                        on_click_new_item(const QString& name_group, const QString& name_item);
    void                        on_click_edit_group(const QString& name_old, const QString& name_new);
    void                        on_click_edit_item(const QString& name_group, const QString& name_item_old, const QString& name_item_new);
    void                        on_click_erase_group(const QString& name_group);
    void                        on_click_erase_item(const QString& name_group, const QString& name_item);
    void                        on_items_changed();

private slots:
    void                        click_new_folder();
    void                        click_new_item();
    void                        click_edit_group();
    void                        click_edit_item();
    void                        click_erase_group();
    void                        click_erase_item();

public:
    explicit                    vTreeEditorPuM(parent_name* parent) : parent_(parent) {}
    void                        set_name_add_group(const QString& name, const QString& description)            { name_add_group_ = name; description_add_group_= description;}
    void                        set_name_add_item(const QString& name, const QString& description)             { name_add_item_ = name; description_add_item_= description;}
    void                        set_name_erase_group(const QString& name, const QString& description)          { name_erase_group_ = name; description_erase_group_= description;}
    void                        set_name_erase_item(const QString& name, const QString& description)           { name_erase_item_ = name; description_erase_item_= description;}
    void                        set_name_default_item(const QString& name)                                     { default_name_item_ = name; }
    void                        instance();

    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_EDITOR_PUM_H_ */
