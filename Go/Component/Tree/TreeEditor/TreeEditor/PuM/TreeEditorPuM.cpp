/* 
 * File:   TreeEditorPuM.cpp
 * Author: S.Panin
 * 
 * Created on 7 Май 2010 г., 22:28
 */
//------------------------------------------------------------------------------
#include "TreeEditorPuM.h"
//------------------------------------------------------------------------------
std::pair<bool, QString> vTreeEditorPuM::get_name(const QString& title, const QString& name, const QString& default_text)
{
        bool done = false;
        const QString& text = QInputDialog::getText(this, QString("%1 - %2").arg(program::name()).arg(title), name, QLineEdit::Normal, default_text, &done);
        return std::make_pair((true == done && false == text.isEmpty()), text);
}
//------------------------------------------------------------------------------
bool vTreeEditorPuM::enabled_action_group() const
{
        const QModelIndex index =parent_->selectionModel()->currentIndex();
        return false==parent_->selectionModel()->selection().isEmpty() && index.isValid() && QModelIndex() ==index.parent();
 }
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance()
{
        QModelIndex() ==parent_->selectionModel()->currentIndex().parent() ?  instance_group() : instance_item();
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_group()
{
        instance_action_new_group();
        instance_action_new_item();
        instance_action_edit_group();
        addSeparator();
        instance_action_erase_group();
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_item()
{
        instance_action_new_group();
        instance_action_new_item();
        instance_action_edit_item();
        addSeparator();
        instance_action_erase_item();
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_new_group()
{
        QAction* action = vActionWithText::add_group(this, QString("%1...").arg (name_add_group_), description_add_group_);
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_new_folder()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_new_folder()
{
    const std::pair<bool,QString> input_data= get_name(name_add_group_, vtr::Dialog::tr("The Group name:"),"");

    QAbstractItemModel * const model = parent_->model();
    const int count_row =model->rowCount();

    if ( false== input_data.first || false ==model->insertRow(count_row) )   return;

    const QModelIndex index =model->index(count_row, 0);
    model->setData(model->index(count_row, 0), input_data.second, Qt::EditRole);

    if ( false ==model->insertRow(0,index))  { parent_->model()->removeRow(count_row, index.parent()); return; }
    model->setData(model->index(0, 0, index), QString("[%1 1]").arg(default_name_item_), Qt::EditRole);

    parent_->selectionModel()->setCurrentIndex(model->index(0, 0, index), QItemSelectionModel::ClearAndSelect);
    emit(on_click_new_group( input_data.second));
    emit(on_items_changed());
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_new_item()
{
        QAction* action = vActionWithText::add_item(this, QString("%1...").arg(name_add_item_), description_add_item_);
        action->setEnabled( false==parent_->selectionModel()->selection().isEmpty() && parent_->selectionModel()->currentIndex().isValid());
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_new_item()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_new_item()
{
    QModelIndex parent = parent_->selectionModel()->currentIndex().parent();
    if (parent ==QModelIndex() ) parent = parent_->selectionModel()->currentIndex();

    QAbstractItemModel * const model = parent_->model();
    const int count_row =model->rowCount(parent);
    const std::pair<bool,QString> input_data= get_name(name_add_item_, vtr::Dialog::tr("The Item name:"),"");

    if (false ==input_data.first || false ==model->insertRow(count_row, parent))   return;

    QModelIndex index =model->index(count_row, 0, parent);
    model->setData(index, input_data.second, Qt::EditRole);

    parent_->selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect);
    emit(on_click_new_item( parent_->model()->data(index.parent()).toString(), input_data.second));
    emit(on_items_changed());
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_edit_group()
{
        QAction* action = vAction::rename(this);
        action->setEnabled( enabled_action_group());
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_edit_group()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_edit_group()
{
        const QModelIndex index = parent_->selectionModel()->currentIndex();
        const QString name_old =parent_->model()->data(index).toString();
        const std::pair<bool,QString> input_data= get_name(name_add_group_, vtr::Dialog::tr("The Group name:"), name_old);

        if(false == input_data.first  || name_old ==input_data.second ||index.parent() != QModelIndex() ) return;
        parent_->model()->setData(index, input_data.second);
        emit(on_click_edit_group( name_old, input_data.second));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_edit_item()
{
        QAction* action = vAction::rename(this);
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_edit_item()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_edit_item()
{ 
        const QModelIndex index = parent_->selectionModel()->currentIndex();
        const QString name_group = parent_->model()->data(index.parent()).toString();
        const QString name_item_old =parent_->model()->data(index).toString();

        const std::pair<bool,QString> input_data= get_name(name_add_item_, vtr::Dialog::tr("The Item name:"),name_item_old);
        if(false == input_data.first  || name_item_old ==input_data.second || index.parent() ==QModelIndex() ) return;

        parent_->model()->setData(index, input_data.second);
        emit(on_click_edit_item(name_group, name_item_old, input_data.second));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_erase_group()
{
        QAction* action = vActionWithText::erase_group(this, name_erase_group_, description_erase_group_);
        action->setEnabled( enabled_action_group());
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_erase_group()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_erase_group()
{
        const QModelIndex index = parent_->selectionModel()->currentIndex();
        const QString name_group = parent_->model()->data(index).toString();
        if ( QMessageBox::warning(parent_, program::name_full(), QString("%1 '%2'?").arg(name_erase_group_).arg(name_group), QMessageBox::Ok | QMessageBox::Cancel) !=QMessageBox::Ok) return;
        if(index.parent() != QModelIndex()) return;
        parent_->model()->removeRow(index.row(), index.parent());
        emit(on_click_erase_group(name_group));
        emit(on_items_changed());
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::instance_action_erase_item()
{
        QAction* action = vActionWithText::erase_group(this, name_erase_item_, description_erase_item_);
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_erase_item()));
}
//------------------------------------------------------------------------------
void vTreeEditorPuM::click_erase_item()
{
        const QModelIndex index = parent_->selectionModel()->currentIndex();
        const QString name_group = parent_->model()->data(index.parent()).toString();
        const QString name_item = parent_->model()->data(index).toString();

        if ( QMessageBox::warning(parent_, program::name_full(), QString("%1 '%2'?").arg(name_erase_item_).arg(name_item), QMessageBox::Ok | QMessageBox::Cancel) !=QMessageBox::Ok) return;
        if(index.parent() == QModelIndex()) return;

        parent_->model()->removeRow(index.row(), index.parent());
        emit(on_click_erase_item(name_group, name_item));
        emit(on_items_changed());
}
//------------------------------------------------------------------------------
bool vTreeEditorPuM::test()
{
        Q_ASSERT(parent_);
        Q_ASSERT(name_add_group_.isEmpty());
        Q_ASSERT(description_add_group_.isEmpty());
        Q_ASSERT(name_add_item_.isEmpty());
        Q_ASSERT(description_add_item_.isEmpty());
        Q_ASSERT(name_erase_group_.isEmpty());
        Q_ASSERT(description_erase_group_.isEmpty());
        Q_ASSERT(name_erase_item_.isEmpty());
        Q_ASSERT(description_erase_item_.isEmpty());
        Q_ASSERT(default_name_item_.isEmpty());
        return true;
}
