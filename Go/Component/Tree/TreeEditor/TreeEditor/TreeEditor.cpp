/* 
 * File:   vTreeEditor.cpp
 * Author: S.Panin
 * 
 * Created on 10 Май 2010 г., 23:12
 */
//------------------------------------------------------------------------------
#include "TreeEditor.h"
//------------------------------------------------------------------------------
vTreeEditor::vTreeEditor(QTreeWidget* parent/*=0*/)
        : inherited(parent)
{
        instance();
        instance_signals();
}
//------------------------------------------------------------------------------
void vTreeEditor::instance()
{
        inherited::setAlternatingRowColors(true);
        inherited::setSelectionBehavior(QAbstractItemView::SelectItems);
        inherited::setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        inherited::setAnimated(false);
        inherited::setAllColumnsShowFocus(true);
        inherited::setContextMenuPolicy(Qt::CustomContextMenu);
}
//------------------------------------------------------------------------------
void vTreeEditor::instance_signals()
{
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(click_show_context_menu(const QPoint&)));
}
//------------------------------------------------------------------------------
void vTreeEditor::click_show_context_menu(const QPoint& position)
{
        vTreeEditorPuM pum(this);
        pum.set_name_add_group(name_add_group_, description_add_group_);
        pum.set_name_add_item(name_add_item_, description_add_item_);
        pum.set_name_erase_group(name_erase_group_, description_erase_group_);
        pum.set_name_erase_item(name_erase_item_, description_erase_item_);
        pum.set_name_default_item(default_name_item_);
        pum.instance();
        connect(&pum, SIGNAL(on_items_changed()), SLOT(counts()));
        connect(&pum, SIGNAL(on_click_new_group(const QString&)),                                       SIGNAL(on_click_new_group(const QString&)));
        connect(&pum, SIGNAL(on_click_new_item(const QString& , const QString& )),                      SIGNAL(on_click_new_item(const QString& , const QString& )));
        connect(&pum, SIGNAL(on_click_edit_group(const QString&, const QString&)),                      SIGNAL(on_click_edit_group(const QString&, const QString&)));
        connect(&pum, SIGNAL(on_click_edit_item(const QString&, const QString&, const QString&)),       SIGNAL(on_click_edit_item(const QString&, const QString&, const QString&)));
        connect(&pum, SIGNAL(on_click_erase_group(const QString&) ),                                    SIGNAL(on_click_erase_group(const QString&)));
        connect(&pum, SIGNAL(on_click_erase_item(const QString&, const QString&)),                      SIGNAL(on_click_erase_item(const QString&, const QString&)));
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
void vTreeEditor::counts()
{
        int count=0;
        for(int it = 0; it != model()->rowCount(); ++it)
        {
                count +=model()->rowCount( model()->index( it, 0, QModelIndex()) );
        }

        emit( on_items_changed(model()->rowCount(), count));
}
//------------------------------------------------------------------------------
void vTreeEditor::add_data(const QString& group_name, const data_item& items)
{
        const int count_row = model()->rowCount();

        if( false == model()->insertRow(count_row)) return;

        const QModelIndex index = model()->index(count_row, 0);
        model()->setData(model()->index(count_row, 0), group_name, Qt::EditRole);

        for(data_item::const_iterator it =items.begin(); it!=items.end(); ++it)
        {
                const int id =std::distance(items.begin(),it);
                if(false == model()->insertRow(id, index))                      { model()->removeRow(count_row, index.parent());  return; }
                model()->setData(model()->index(id, 0, index), *it, Qt::EditRole);
        }

        counts();
}
//------------------------------------------------------------------------------
bool vTreeEditor::test()
{
        Q_ASSERT(name_add_group_.isEmpty());
        Q_ASSERT(description_add_group_.isEmpty());
        Q_ASSERT(name_add_item_.isEmpty());
        Q_ASSERT(description_add_item_.isEmpty());
        Q_ASSERT(name_erase_group_.isEmpty());
        Q_ASSERT(description_erase_group_.isEmpty());
        Q_ASSERT(name_erase_item_.isEmpty());
        Q_ASSERT(description_erase_item_.isEmpty());
        Q_ASSERT(default_name_item_.isEmpty());
        return true;
}
//------------------------------------------------------------------------------