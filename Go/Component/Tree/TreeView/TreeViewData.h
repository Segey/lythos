// -*- C++ -*-
/* 
 * File:   ViewData.h
 * Author: S.Panin
 *
 * Created on 13 Марта 2010 г., 13:13
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_VIEW_DATA_H_
#define	_V_TREE_VIEW_DATA_H_
//------------------------------------------------------------------------------
struct vTreeViewData
{

    static QString                      name_dll()                              { return const_instance_libs::tw1313(); }
    QTreeWidget*                                                                tree_view_;
    QPushButton*                                                                new_button_;
    QPushButton*                                                                open_button_;
    QPushButton*                                                                delete_button_;

    void translate()
    {
        new_button_->setText(vtr::Button::tr("&New..."));
        open_button_->setText(vtr::Button::tr("&Open..."));
        delete_button_->setText(vtr::Button::tr("&Delete"));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_VIEW_DATA_H__ */

