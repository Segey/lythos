/* 
 * File:   TreeView.cpp
 * Author: S.Panin
 * 
 * Created on 13 Марта 2010 г., 10:13
 */
//------------------------------------------------------------------------------
#include "TreeView.h"
//------------------------------------------------------------------------------
vTreeView::vTreeView(QWidget* parent/*=0*/)
                : inherited(parent), data_(new data_name)
{      
        vDllInstance::instance(this, data_.get());
        data_->translate();
}
//------------------------------------------------------------------------------
void vTreeView::instance()
{
        toTreeWidget()->setHeaderLabels( do_get_header_labels_full() );
        instance_signals();
        do_settings_read();
        enabled_buttons();
}
//------------------------------------------------------------------------------
void vTreeView::instance_signals()
{
        connect(data_->tree_view_->header(),    SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(show_header_context_menu(const QPoint &)));
        connect(data_->tree_view_,              SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(show_body_context_menu(const QPoint &)));
        connect(data_->new_button_,             SIGNAL(clicked()), SLOT(new_record()));
        connect(data_->open_button_,            SIGNAL(clicked()), SLOT(open_record()));
        connect(data_->delete_button_,          SIGNAL(clicked()), SLOT(delete_record()));
        connect(data_->tree_view_,              SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), SLOT(open_record_with_item(QTreeWidgetItem*, int)));
        connect(data_->tree_view_,              SIGNAL(currentItemChanged ( QTreeWidgetItem *, QTreeWidgetItem *)), SLOT(enabled_buttons()));
}
//------------------------------------------------------------------------------
/*slot*/ void vTreeView::show_header_context_menu(const QPoint &position)
{
        vTreeViewHeaderPuM pum(data_->tree_view_);
        pum.set_items(do_get_header_labels_full(), do_get_header_labels_default());
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
/*slot*/ void vTreeView::show_body_context_menu(const QPoint &position)
{
        vTreeViewBodyPuM pum(data_->tree_view_);
        connect(&pum, SIGNAL(on_click_new()),           SLOT(new_record()));
        connect(&pum, SIGNAL(on_click_open()),          SLOT(open_record()));
        connect(&pum, SIGNAL(on_click_delete()),        SLOT(delete_record()));
        connect(&pum, SIGNAL(on_click_clear_all()),     SLOT(clear_all_records()));
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
/*slot*/void vTreeView::enabled_buttons()
{
      data_->open_button_->setEnabled(data_->tree_view_->currentItem());
      data_->delete_button_->setEnabled(data_->tree_view_->currentItem());
}
//------------------------------------------------------------------------------
QString   vTreeView::get_value(QTreeWidgetItem* item)
{
        int index = vbtTreeWidget::get_header_item_index(data_->tree_view_->headerItem(), do_get_header_base_column_name()) ;
        return index < 0 ? QString() : item->text(index);
}
//------------------------------------------------------------------------------
/*slot*/void vTreeView::delete_record()
{
        if ( !data_->tree_view_->currentItem() ) return;
        if(QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Are you sure you want to remove the selected record?!"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes) return;
       
        if ( do_record_delete( get_value(data_->tree_view_->currentItem())) )  load();        
}
//------------------------------------------------------------------------------
/*slot*/void vTreeView::clear_all_records()
{  
        if(QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Are you sure you want to remove all records?!"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) != QMessageBox::Yes) return;
        OVERRIDE_CURSOR( do_records_clear_all() );
}
//------------------------------------------------------------------------------
bool vTreeView::load()
{
        QApplication::setOverrideCursor(Qt::WaitCursor);
        data_->tree_view_->clear();
        bool b =do_load();
        QApplication::restoreOverrideCursor();
        return b;
}
//------------------------------------------------------------------------------
bool vTreeView::test()
{
        Q_ASSERT(data_->tree_view_);
        Q_ASSERT(data_->new_button_);
        Q_ASSERT(data_->open_button_);
        Q_ASSERT(data_->delete_button_);
        Q_ASSERT(data_->new_button_->text()     == vtr::Button::tr("&New..."));
        Q_ASSERT(data_->open_button_->text()    == vtr::Button::tr("&Open..."));
        Q_ASSERT(data_->delete_button_->text()  == vtr::Button::tr("&Delete"));
        return true;
}
//------------------------------------------------------------------------------