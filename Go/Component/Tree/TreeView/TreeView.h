/*
 * File:   TreeView.h
 * Author: S.Panin
 *
 * Created on 13 Марта 2010 г., 10:13
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_VIEW_H_
#define	_V_TREE_VIEW_H_
//------------------------------------------------------------------------------
#include "../../PuM/ThreeView/Body/TreeViewBodyPuM.h"
#include "../../PuM/ThreeView/Header/TreeViewHeaderPuM.h"
#include "TreeViewData.h"
//------------------------------------------------------------------------------
class vTreeView : public QWidget
{
Q_OBJECT
    typedef                     QWidget                                         inherited;
    typedef                     vTreeView                                       class_name;
    typedef                     vTreeViewData                                   data_name;

    boost::shared_ptr<data_name>                                                data_;

    void                        instance_signals();
    QString                     get_value(QTreeWidgetItem* item);
 
private slots:
    void                        show_header_context_menu(const QPoint &position);
    void                        show_body_context_menu(const QPoint &position);
    void                        new_record()                                        { if ( do_record_new() ) load(); }
    void                        open_record()                                       { if ( do_record_open_with_item(get_value(data_->tree_view_->currentItem())) )  load(); }
    void                        open_record_with_item(QTreeWidgetItem* item, int )  { if ( do_record_open_with_item(get_value(item)) ) load();}
    void                        delete_record();
    void                        clear_all_records();
    void                        enabled_buttons();

protected:
    virtual QString             do_get_header_base_column_name()                            =0;
    virtual QStringList         do_get_header_labels_full()                                 =0;
    virtual QStringList         do_get_header_labels_default()                              =0;
    virtual bool                do_load()                                                   =0;
    virtual bool                do_record_new()                                             =0;
    virtual bool                do_record_open_with_item(const QString& name)               =0;
    virtual bool                do_record_delete(const QString& name)                       =0;
    virtual bool                do_records_clear_all()                                      =0;
    virtual bool                do_settings_read()                                          =0;
 
public:
    explicit                    vTreeView(QWidget* parent =0);
    virtual                     ~vTreeView()                                    { }
    QTreeWidget*                toTreeWidget() const                            {return data_->tree_view_;}
    void                        instance();
    bool                        load();
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_VIEW_H_ */
