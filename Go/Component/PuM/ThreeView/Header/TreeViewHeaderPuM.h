/*
 * File:   TreeViewPuM.h
 * Author: S.Panin
 *
 * Created on 13 Марта 2010 г., 02:17
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_VIEW_HEADER_PUM_H_
#define _V_TREE_VIEW_HEADER_PUM_H_
//------------------------------------------------------------------------------
class vTreeViewHeaderPuM : public QMenu
{
Q_OBJECT

    typedef                     QMenu                                           inherited;
    typedef                     vTreeViewHeaderPuM                              class_name;
    typedef                     QTreeWidget                                     parent_name;
    typedef                     QStringList                                     data_name;

    parent_name*                                                                parent_;
    data_name                                                                   data_;
    data_name                                                                   data_default_;

    void                        instance();
    void                        instance_items();
    void                        instance_default();
    void                        instance_show_all();
    bool                        is_show_data(const data_name& data) const       { for ( int i=0 ; i < parent_->header()->count() ; ++i ) if ( parent_->header()->isSectionHidden(i) &&  is_exist_in_data_default(data, i) ) return false;  return true;   }
    bool                        is_exist_in_data_default(const data_name& data, size_t index) const    { return std::find(data.begin(), data.end(), parent_->headerItem()->text(index)) != data.end();  }
    bool                        is_show_item_one() const                        { int count=0; for ( int i=0 ; i < parent_->header()->count() ; ++i ) if ( parent_->header()->isSectionHidden(i) ==false && (++count > 1) ) return false;   return true;  }


signals:
    void                        on_click_item(const QAction* item);
    void                        on_click_item_show_all();
    void                        on_click_item_default();

private slots:
    void                        click_item();
    void                        click_item_show_all();
    void                        click_item_default();

public:
    explicit                    vTreeViewHeaderPuM(parent_name* parent) : inherited(parent), parent_(parent) {}
    void                        set_items(const QStringList& data, const QStringList& data_default=QStringList() )    { data_ = data; data_default.isEmpty() ? data_default_=data : data_default_=data_default;  }
    QAction*                    exec(const QPoint &pos, QAction *at=0)          {  instance(); return inherited::exec(pos, at); }
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_VIEW_HEADER_PUM_H_ */
