/*
 * File:   TreeViewPuM.cpp
 * Author: S.Panin
 *
 * Created on 13 Марта 2010 г., 02:17
 */
//------------------------------------------------------------------------------
#include "TreeViewHeaderPuM.h"
//------------------------------------------------------------------------------
void  vTreeViewHeaderPuM::instance()
{
        instance_items();
        addSeparator();
        instance_default();
        instance_show_all();
}
//------------------------------------------------------------------------------
void vTreeViewHeaderPuM::instance_items()
{
        int index = 0;
        QAction* action =0;
        bool is_show_one =is_show_item_one();

        for(QStringList::const_iterator it = data_.begin(); it != data_.end(); ++it)
        {
                index = vbtTreeWidget::get_header_item_index(parent_->headerItem(), *it);
                if(index < 0) continue;

                action = vAction::create(QIcon(), *it, *it, this, QKeySequence::UnknownKey);
                action->setCheckable(true);                
                if(parent_->header()->isSectionHidden(index) == false) 
                        {
                        action->setChecked(true);
                        if (is_show_one) action->setDisabled(true);
                        }
                action->setData(index);
                addAction(action);               
                connect(action, SIGNAL(triggered(bool)), SLOT(click_item()));
        }
}
//------------------------------------------------------------------------------
void vTreeViewHeaderPuM::instance_show_all()
{
        QAction* action= vActionChart::createShowAll(this);
        addAction(action);

        if ( is_show_data(data_) ) action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_item_show_all()));
}
//------------------------------------------------------------------------------
void vTreeViewHeaderPuM::instance_default()
{
        QAction* action= vAction::createDefault(this);
        addAction(action);

        if ( is_show_data(data_default_) ) action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_item_default()));
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewHeaderPuM::click_item()
{
        QAction* action = (QAction*)sender();
        parent_->header()->setSectionHidden(action->data().toInt(), !action->isChecked() );
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewHeaderPuM::click_item_show_all()
{
        for ( int i=0 ; i < parent_->header()->count() ; ++i )
                   parent_->header()->setSectionHidden(i, false);
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewHeaderPuM::click_item_default()
{
        for ( int i=0 ; i < parent_->header()->count() ; ++i )
                 if(is_exist_in_data_default(data_default_, i))  parent_->header()->setSectionHidden(i, false);
                 else parent_->header()->setSectionHidden(i, true);
}
//------------------------------------------------------------------------------
bool vTreeViewHeaderPuM::test()
{
        Q_ASSERT(parent_);
        QStringList data         = QStringList() << vtr::Exercise::tr("Exercise") << vtr::Record::tr("Record Date") << vtr::Record::tr("Record Weight (%1):").arg(vMain::user().locale().weight()->kgs()) << vtr::Record::tr("Record Repetitions") << vtr::Record::tr("Record Notes");
        QStringList data_default = QStringList() << vtr::Record::tr("Record Date") << vtr::Record::tr("Record Weight (%1):").arg(vMain::user().locale().weight()->kgs()) << vtr::Record::tr("Record Notes");

        set_items(data);
        Q_ASSERT(data_ == data_default_);
        
        set_items(data, data_default);
        Q_ASSERT(data_ != data_default_);

        struct Test
        {
              QTreeWidget*      parent_;
              explicit  Test(QTreeWidget* parent) :  parent_(parent) { }
              int count_visible()
                {
                        int count = 0;
                        for(int i = 0; i < parent_->header()->count(); ++i)
                                if(parent_->header()->isSectionHidden(i) == false) ++count;
                        return count;
                }
        }t(parent_);

        parent_->setHeaderLabels(data_);
        Q_ASSERT(5 == t.count_visible());
        Q_ASSERT(parent_->header()->count() == t.count_visible());

        Q_ASSERT(-1 == vbtTreeWidget::get_header_item_index(parent_->headerItem(), "111"));
        Q_ASSERT(0 == vbtTreeWidget::get_header_item_index(parent_->headerItem(), vtr::Exercise::tr("Exercise")));
        Q_ASSERT(2 == vbtTreeWidget::get_header_item_index(parent_->headerItem(), vtr::Record::tr("Record Weight (%1):").arg(vMain::user().locale().weight()->kgs())));

        Q_ASSERT(is_show_data(data_));
        Q_ASSERT(is_show_data(data_default));
        Q_ASSERT(is_show_item_one() ==false);

        click_item_default();
        Q_ASSERT(3 == t.count_visible());

        click_item_show_all();
        Q_ASSERT(5 == t.count_visible());

        data_default_.insert(3,"000");
     
        click_item_default();
        Q_ASSERT(3 == t.count_visible());

        click_item_show_all();
        Q_ASSERT(5 == t.count_visible());

        data_default_.clear();

        click_item_default();
        Q_ASSERT(false == t.count_visible());

        return true;
}