/*
 * File:   vTreeViewBodyPuM.cpp
 * Author: S.Panin
 *
 * Created on 17 Марта 2010 г., 00:17
 */
//------------------------------------------------------------------------------
#include "TreeViewBodyPuM.h"
//------------------------------------------------------------------------------
vTreeViewBodyPuM::vTreeViewBodyPuM(parent_name* parent)
                : inherited(parent), parent_(parent)
{
        instance_action_new();
        instance_action_open();       
        instance_action_delete();
        addSeparator();
        instance_action_clear_all();
}
//------------------------------------------------------------------------------
void vTreeViewBodyPuM::instance_action_new()
{
        QAction* action= vAction::createRecordNew(this);
        addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_action_new()));
}
//------------------------------------------------------------------------------
void vTreeViewBodyPuM::instance_action_open()
{
        QAction* action= vAction::createRecordOpen(this);
        addAction(action);

        action->setEnabled(parent_->currentItem());
        connect(action, SIGNAL(triggered(bool)), SLOT(click_action_open()));
}
//------------------------------------------------------------------------------
void vTreeViewBodyPuM::instance_action_delete()
{
        QAction* action= vAction::createRecordDelete(this);
        addAction(action);

        action->setEnabled(parent_->currentItem());
        connect(action, SIGNAL(triggered(bool)), SLOT(click_action_delete()));
}
//------------------------------------------------------------------------------
void vTreeViewBodyPuM::instance_action_clear_all()
{
        QAction* action= vAction::createClearAll(this);
        addAction(action);

        action->setEnabled(parent_->currentItem());
        connect(action, SIGNAL(triggered(bool)), SLOT(click_action_clear_all()));
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewBodyPuM::click_action_new()
{
       emit( on_click_new() );
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewBodyPuM::click_action_open()
{
       emit( on_click_open() );
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewBodyPuM::click_action_delete()
{
       emit( on_click_delete() );
}
//------------------------------------------------------------------------------
/*signal*/ void  vTreeViewBodyPuM::click_action_clear_all()
{
       emit( on_click_clear_all() );
}
//------------------------------------------------------------------------------
bool vTreeViewBodyPuM::test()
{
        Q_ASSERT(parent_);

        return true;
}