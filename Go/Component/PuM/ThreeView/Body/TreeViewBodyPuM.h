/*
 * File:   TreeViewBodyPuM.h
 * Author: S.Panin
 *
 * Created on 17 Марта 2010 г., 00:17
 */
//------------------------------------------------------------------------------
#ifndef _V_TREE_VIEW_BODY_PUM_H_
#define _V_TREE_VIEW_BODY_PUM_H_
//------------------------------------------------------------------------------
class vTreeViewBodyPuM : public QMenu
{
Q_OBJECT

    typedef                     QMenu                                           inherited;
    typedef                     vTreeViewBodyPuM                                class_name;
    typedef                     QTreeWidget                                     parent_name;

    parent_name*                                                                parent_;

    void                        instance_action_new();
    void                        instance_action_open();
    void                        instance_action_delete();
    void                        instance_action_clear_all();

signals:
    void                        on_click_new();
    void                        on_click_open();
    void                        on_click_delete();
    void                        on_click_clear_all();

private slots:
    void                        click_action_new();
    void                        click_action_open();
    void                        click_action_delete();
    void                        click_action_clear_all();

public:
    explicit                    vTreeViewBodyPuM(parent_name* parent);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_TREE_VIEW_BODY_PUM_H_ */
