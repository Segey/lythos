/**
    \file   MenuHelp.h
    \brief  Файл MenuHelp.h

    \author S.Panin
    \date   Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#ifndef _MENU_HELP_H_
#define	_MENU_HELP_H_
//------------------------------------------------------------------------------
#include "../../../tr.h"
#include "../../../BuiltType/Action/Action.h"
#include "../../Dialog/About/DialogAbout.h"
//------------------------------------------------------------------------------
/** \brief Depregated */
class vMenuHelp : public QMenu
{
Q_OBJECT
    typedef                         QMenu                                       inherited;
    typedef                         vMenuHelp                                   class_name;

    QAction*                                                                    help_act_;
    QAction*                                                                    homepage_act_;
    QAction*                                                                    registration_act_;
    QAction*                                                                    about_act_;

    void                            create_actions();
    void                            instance_signals();
    void                            instance_actions();

private slots:
    void                            open_help();
    void                            open_homepage();
    void                            open_registration();
    void                            open_about();

public:
    explicit                        vMenuHelp(QWidget* parent =0) : inherited(parent), help_act_(0), homepage_act_(0), registration_act_(0), about_act_(0)  {}
    void                            instance();
    void                            translate();
    bool                            test();
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::menu  */
    namespace menu {
/** brief Меню Help */
class Help : public QMenu
{
Q_OBJECT
    typedef                         QMenu                                       inherited;
    typedef                         Help                                        class_name;

    QAction*                                                                    help_act_;
    QAction*                                                                    homepage_act_;
    QAction*                                                                    registration_act_;
    QAction*                                                                    about_act_;

    void                            create_actions()
    {
        help_act_           = vAction::createHelp(this);
        homepage_act_       = vAction::createHomePage(this);
        registration_act_   = vAction::createRegistration(this);
        about_act_          = vAction::createAbout(this);
    }
    void                            instance_signals()
    {
        connect(help_act_,              SIGNAL(triggered()), SLOT(open_help()));
        connect(homepage_act_,          SIGNAL(triggered()), SLOT(open_homepage()));
        connect(registration_act_,      SIGNAL(triggered()), SLOT(open_registration()));
        connect(about_act_,             SIGNAL(triggered()), SLOT(open_about()));
    }
    void                            instance_actions()
    {
        inherited::addAction(help_act_);
        inherited::addSeparator();
        inherited::addAction(homepage_act_);
        inherited::addAction(registration_act_);
        inherited::addSeparator();
        inherited::addAction(about_act_);
    }

private slots:
    void                            open_help()                                 { OVERRIDE_CURSOR(QDesktopServices::openUrl(QUrl(program::name_help_file()));) }
    void                            open_homepage()                             { OVERRIDE_CURSOR(QDesktopServices::openUrl(QUrl(program::url_homepage()));) }
    void                            open_registration()                         {/*_XXX_*/}
    void                            open_about()
    {
        vDialogAbout dialog;
        dialog.exec();
    }

public:
    /** 
     \brief Конструктор.
     \param parent - указатель на родителя. По умолчанию - отсутствует.
     */
    explicit                        Help(QWidget* parent =0) : inherited(parent), help_act_(0), homepage_act_(0), registration_act_(0), about_act_(0)  {}
    /** \brief Инстанцирование */
    void                            instance()
    {
        create_actions();
        instance_actions();
        instance_signals();
        translate();
    }
    /** \brief Перевод */
    void                            translate()                                 { inherited::setTitle(tr::Button::tr("&Help")); }
    /** \brief тест */
    bool                            test()
    {
        Q_ASSERT(help_act_);
        Q_ASSERT(homepage_act_);
        Q_ASSERT(registration_act_);
        Q_ASSERT(about_act_);
        Q_ASSERT(help_act_->isEnabled());
        Q_ASSERT(!help_act_->icon().isNull());
        Q_ASSERT(!help_act_->text().isEmpty());
        Q_ASSERT(homepage_act_->isEnabled());
        Q_ASSERT(homepage_act_->icon().isNull());
        Q_ASSERT(!homepage_act_->text().isEmpty());
        Q_ASSERT(registration_act_->isEnabled());
        Q_ASSERT(registration_act_->icon().isNull());
        Q_ASSERT(!registration_act_->text().isEmpty());
        Q_ASSERT(about_act_->isEnabled());
        Q_ASSERT(!about_act_->icon().isNull());
        Q_ASSERT(!about_act_->text().isEmpty());
        Q_ASSERT(QFile::exists(program::name_help_file()));
        Q_ASSERT(vDialogAbout().test());
        return true;
    }
};
//------------------------------------------------------------------------------
    }; // end namespace menu
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _HELP_MENU_H_ */
