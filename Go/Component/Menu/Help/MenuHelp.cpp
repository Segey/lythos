/* 
 * File:   MenuHelp.cpp
 * Author: S.Panin
 * 
 * Created on 6 Август 2009 г., 22:15
 */
//------------------------------------------------------------------------------
#include "MenuHelp.h"
//------------------------------------------------------------------------------
void vMenuHelp::instance()
{
        create_actions();
        instance_actions();
        instance_signals();
        translate();
}
//------------------------------------------------------------------------------
void vMenuHelp::create_actions()
{
        help_act_ = vAction::createHelp(this);
        homepage_act_ = vAction::createHomePage(this);
        registration_act_ = vAction::createRegistration(this);
        about_act_ = vAction::createAbout(this);
}
//------------------------------------------------------------------------------
void vMenuHelp::instance_actions()
{
        inherited::addAction(help_act_);
        inherited::addSeparator();
        inherited::addAction(homepage_act_);
        inherited::addAction(registration_act_);
        inherited::addSeparator();
        inherited::addAction(about_act_);
}
//------------------------------------------------------------------------------
void vMenuHelp::instance_signals()
{
        connect(help_act_,              SIGNAL(triggered()), SLOT(open_help()));
        connect(homepage_act_,          SIGNAL(triggered()), SLOT(open_homepage()));
        connect(registration_act_,      SIGNAL(triggered()), SLOT(open_registration()));
        connect(about_act_,             SIGNAL(triggered()), SLOT(open_about()));
}
//------------------------------------------------------------------------------
void vMenuHelp::open_help()
{
        OVERRIDE_CURSOR(QDesktopServices::openUrl(QUrl(program::name_help_file()));)
}
//------------------------------------------------------------------------------
void vMenuHelp::open_homepage()
{
        OVERRIDE_CURSOR(QDesktopServices::openUrl(QUrl(program::url_homepage()));)
}
//------------------------------------------------------------------------------
void vMenuHelp::open_registration(){
        //_XXX_
}
//------------------------------------------------------------------------------
void vMenuHelp::open_about()
{
        vDialogAbout dialog;
        dialog.exec();
}
//------------------------------------------------------------------------------
void vMenuHelp::translate()
{
        inherited::setTitle(vtr::Button::tr("&Help"));
}
//------------------------------------------------------------------------------
bool vMenuHelp::test()
{
        Q_ASSERT(help_act_);
        Q_ASSERT(homepage_act_);
        Q_ASSERT(registration_act_);
        Q_ASSERT(about_act_);
        Q_ASSERT(help_act_->isEnabled());
        Q_ASSERT(!help_act_->icon().isNull());
        Q_ASSERT(!help_act_->text().isEmpty());
        Q_ASSERT(homepage_act_->isEnabled());
        Q_ASSERT(homepage_act_->icon().isNull());
        Q_ASSERT(!homepage_act_->text().isEmpty());
        Q_ASSERT(registration_act_->isEnabled());
        Q_ASSERT(registration_act_->icon().isNull());
        Q_ASSERT(!registration_act_->text().isEmpty());
        Q_ASSERT(about_act_->isEnabled());
        Q_ASSERT(!about_act_->icon().isNull());
        Q_ASSERT(!about_act_->text().isEmpty());
        Q_ASSERT(QFile::exists(program::name_help_file()));
        Q_ASSERT(vDialogAbout().test());
        return true;
}
//------------------------------------------------------------------------------