/* 
 * File:   MenuView.h
 * Author: S.Panin
 *
 * Created on Пн апр. 26 2010, 01:54:24
 */
//------------------------------------------------------------------------------
#ifndef _V_MENU_VIEW_H_
#define	_V_MENU_VIEW_H_
//------------------------------------------------------------------------------
#include "../../../go/noncopyable.h"
#include "../../../BuiltType/Action/Action.h"
//------------------------------------------------------------------------------
namespace go { // NAMESPACE BEGIN GO
//------------------------------------------------------------------------------
class vMenuView : public QMenu, go::noncopyable
{
    typedef                         QMenu                                       inherited;
    typedef                         vMenuView                                   class_name;
    typedef                         QMenu                                       menu_type;

protected:
    typedef                         QAction*                                    value_type;
    typedef                         value_type                                  const_reference;

public:
    explicit                        vMenuView(QWidget* parent =0) : inherited(parent) {}
    void                            set_title(const QString& str)               { inherited::setTitle(str); }
    void                            set_all_checkable(bool yes)                 { QList<value_type> tmp =menu_type::actions(); std::for_each(tmp.begin(), tmp.end(), boost::bind(&QAction::setCheckable, _1, yes)); }
    const_reference                 front() const                               { return menu_type::actions().front(); }
    const_reference                 back() const                                { return menu_type::actions().back(); }
    const_reference                 at(size_t index) const                      { return menu_type::actions().at(index); }
    bool                            test()
    {
        menu_type::addAction("Cool");
        menu_type::addAction("22");
        Q_ASSERT(front());
        Q_ASSERT(back());
        Q_ASSERT(menu_type::actions().size() >1);
        set_all_checkable(true);
        Q_ASSERT(front()->isCheckable());
        Q_ASSERT(back()->isCheckable());
        return true;
    }
};
//------------------------------------------------------------------------------
}  // NAMESPACE END GO
//------------------------------------------------------------------------------
#endif	/* _V_MENU_EDIT_H_ */
