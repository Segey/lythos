/* 
 * File:   MenuEdit.cpp
 * Author: S.Panin
 * 
 * Created on Сб апр. 24 2010, 14:30:04
 */
//------------------------------------------------------------------------------
#include "MenuEdit.h"
//------------------------------------------------------------------------------
namespace go { // NAMESPACE BEGIN GO
//------------------------------------------------------------------------------
vMenuEdit::vMenuEdit(QWidget* parent /*=0*/)
                : inherited(parent), act_undo_(0), act_redo_(0), act_cut_(0), act_copy_(0), act_paste_(0), act_clear_(0)
{
        create_actions();
        instance_actions();
}
//------------------------------------------------------------------------------
void vMenuEdit::create_actions()
{
        act_undo_       = vAction::createUndo(this);
        act_redo_       = vAction::createRedo(this);
        act_cut_        = vAction::createCutWithTip(this);
        act_copy_       = vAction::createCopyWithTip(this);
        act_paste_      = vAction::createPasteWithTip(this);
        act_clear_      = vAction::createClearText(this);
}
//------------------------------------------------------------------------------
void vMenuEdit::instance_actions()
{
        inherited::addAction(act_undo_);
        inherited::addAction(act_redo_);
        inherited::addSeparator();
        inherited::addAction(act_cut_);
        inherited::addAction(act_copy_);
        inherited::addAction(act_paste_);
        inherited::addAction(act_clear_);
}
//------------------------------------------------------------------------------
void vMenuEdit::set_all_enabled(bool yes)
{
        act_undo_       ->setEnabled(yes);
        act_redo_       ->setEnabled(yes);
        act_cut_        ->setEnabled(yes);
        act_copy_       ->setEnabled(yes);
        act_paste_      ->setEnabled(yes);
        act_clear_      ->setEnabled(yes);
}
//------------------------------------------------------------------------------
bool vMenuEdit::test()
{
        Q_ASSERT(act_undo_);
        Q_ASSERT(!act_undo_->icon().isNull());
        Q_ASSERT(!act_undo_->text().isEmpty());
        Q_ASSERT(act_redo_);
        Q_ASSERT(!act_redo_->icon().isNull());
        Q_ASSERT(!act_redo_->text().isEmpty());
        Q_ASSERT(act_cut_);
        Q_ASSERT(!act_cut_->icon().isNull());
        Q_ASSERT(!act_cut_->text().isEmpty());
        Q_ASSERT(act_copy_);
        Q_ASSERT(!act_copy_->icon().isNull());
        Q_ASSERT(!act_copy_->text().isEmpty());
        Q_ASSERT(act_paste_);
        Q_ASSERT(!act_paste_->icon().isNull());
        Q_ASSERT(!act_paste_->text().isEmpty());
        Q_ASSERT(act_clear_);
        Q_ASSERT(!act_clear_->icon().isNull());
        Q_ASSERT(!act_clear_->text().isEmpty());
        Q_ASSERT(inherited::actions().size() == 7);
        Q_ASSERT(front()== action_undo());
        Q_ASSERT(back()==action_clear());
        return true;
}
//------------------------------------------------------------------------------
}  // NAMESPACE END GO
//------------------------------------------------------------------------------