/* 
 * File:   MenuEdit.h
 * Author: S.Panin
 *
 * Created on Сб апр. 24 2010, 14:30:00
 */
//------------------------------------------------------------------------------
#ifndef _V_MENU_EDIT_H_
#define	_V_MENU_EDIT_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Action.h"
//------------------------------------------------------------------------------
namespace go { // NAMESPACE BEGIN GO
//------------------------------------------------------------------------------
class vMenuEdit : public QMenu
{
    typedef                         QMenu                                       inherited;
    typedef                         vMenuEdit                                   class_name;

protected:
    typedef                         QAction*                                    value_type;
    typedef                         const value_type&                           const_reference;

private:
    value_type                                                                  act_undo_;
    value_type                                                                  act_redo_;
    value_type                                                                  act_cut_;
    value_type                                                                  act_copy_;
    value_type                                                                  act_paste_;
    value_type                                                                  act_clear_;

    void                            create_actions();
    void                            instance_actions();

protected:
    const_reference                 action_undo() const                         { return act_undo_; }
    const_reference                 action_redo() const                         { return act_redo_; }
    const_reference                 action_cut() const                          { return act_cut_; }
    const_reference                 action_copy() const                         { return act_copy_; }
    const_reference                 action_paste() const                        { return act_paste_; }
    const_reference                 action_clear() const                        { return act_clear_; }

public:
    explicit                        vMenuEdit(QWidget* parent =0);
    virtual                         ~vMenuEdit()                                {}
    void                            set_title(const QString& str)               { inherited::setTitle(str); }
    const_reference                 front() const                               { return act_undo_; }
    const_reference                 back() const                                { return act_clear_; }
    void                            set_all_enabled(bool yes);
    bool                            test();
};
//------------------------------------------------------------------------------
}  // NAMESPACE END GO
//------------------------------------------------------------------------------
#endif	/* _V_MENU_EDIT_H_ */
