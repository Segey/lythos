/* 
 * File:   DialogInputImageRichText.h
 * Author: S.Panin
 *
 * Created on Вт мая 25 2010, 13:07:33
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_INPUT_IMAGE_RICHTEXT_H_
#define	_V_DIALOG_INPUT_IMAGE_RICHTEXT_H_
//------------------------------------------------------------------------------
#include "../../../../Algorithm/Interfase.h"
#include "DialogInputImageRichTextInstance.h"
//------------------------------------------------------------------------------
namespace vdialog
{ // NAMESPACE VDIALOG_BEGIN
//------------------------------------------------------------------------------
class ImageRichText : public QDialog, protected vdialog::image_rich_text::Instance
{
Q_OBJECT

    typedef                             QDialog                                 inherited;
    typedef                             ImageRichText                           class_name;
    typedef                             vdialog::image_rich_text::Instance      class_instance;


    void                                instance_form();
    void                                instance_signals();
    bool                                is_condition_restored() const;    
    bool                                save_before_close();
    bool                                is_name_containts_point();
    bool                                is_imagefile_correct() const;

protected:
    virtual void                        closeEvent(QCloseEvent *event);

private slots:
    void                                save();
    void                                condition_changed();

public:
    explicit                            ImageRichText();
    QString                             image_filename() const                  { return image_add_->image_filename(); }
    QString                             text_html() const                       { return rich_edit_->text_html(); }
    void                                set_window_modified(const bool b);
    bool                                test();
};
//------------------------------------------------------------------------------
}; // NAMESPACE VDIALOG_END
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INPUT_IMAGE_RICHTEXT_H_ */
