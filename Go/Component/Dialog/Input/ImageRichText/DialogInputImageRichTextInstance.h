/* 
 * File:   DialogInputImageRichTextInstance.h
 * Author: S.Panin
 *
 * Created on Вт мая 25 2010, 14:07:40
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_INPUT_IMAGE_RICHTEXT_INSTANCE_H_
#define	_V_DIALOG_INPUT_IMAGE_RICHTEXT_INSTANCE_H_
//------------------------------------------------------------------------------
#include "../../../../Instance/FormInstance/FormInstance.h"
#include "../../../Panel/RichEdit/PanelRichEdit.h"
#include "../../../Widget/Image/Add/Add/ImageAdd.h"
//------------------------------------------------------------------------------
namespace vdialog
{ // NAMESPACE VDIALOG_BEGIN
//------------------------------------------------------------------------------
    namespace image_rich_text
    { // NAMESPACE IMAGE_RICH_TEXT_BEGIN
//------------------------------------------------------------------------------
struct Instance : private go::noncopyable
{
    typedef                     Instance               class_name;

    QLabel*                                                                     label_cap_icon_;
    QLabel*                                                                     label_cap_;
    QLabel*                                                                     label_image_add_;
    vImageAdd*                                                                  image_add_;
    vPanelRichEdit*                                                             rich_edit_;
    QLabel*                                                                     label_rich_edit_;
    QPushButton*                                                                button_ok_;
    QPushButton*                                                                button_cancel_;

    void                        instance_widgets();
    void                        read_options()                                  {}
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout();
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate()                                     {}
};
    //------------------------------------------------------------------------------
    }; // IMAGE_RICH_TEXT_BEGIN
//------------------------------------------------------------------------------
}; // NAMESPACE VDIALOG_END
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INPUT_IMAGE_RICHTEXT_INSTANCE_H_ */
