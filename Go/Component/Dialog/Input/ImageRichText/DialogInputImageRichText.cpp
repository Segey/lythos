/* 
 * File:   DialogInputImageRichText.cpp
 * Author: S.Panin
 * 
 * Created on Вт мая 25 2010, 13:07:33
 */
//------------------------------------------------------------------------------
#include "DialogInputImageRichText.h"
//------------------------------------------------------------------------------
using namespace vdialog;
//------------------------------------------------------------------------------
ImageRichText::ImageRichText()
        : QDialog(0)
{
        v_instance::instance< inherited, class_instance > (this, this);
        instance_form();
        instance_signals();
}
//------------------------------------------------------------------------------
void ImageRichText::instance_form()
{
        setWindowTitle(QString(program::name_full()) + "[*]");
        setWindowIcon(QIcon(const_icon_16::program()));
        vDialog::size_hint(this);
        inherited::setWindowModified(false);
}
//------------------------------------------------------------------------------
void ImageRichText::instance_signals()
{
        connect(button_cancel_, SIGNAL(clicked()),                      SLOT(close()));
        connect(button_ok_,     SIGNAL(clicked()),                      SLOT(save()));
        connect(image_add_,     SIGNAL(on_condition_changed()),         SLOT(condition_changed()));
        connect(rich_edit_,     SIGNAL(on_condition_changed()),         SLOT(condition_changed()));      
}
//------------------------------------------------------------------------------
void ImageRichText::save()
{
        if (save_before_close()) accept();
}
//------------------------------------------------------------------------------
bool ImageRichText::save_before_close()
{
        if ( is_condition_restored() || false ==is_name_containts_point() ) return false;
        set_window_modified(false);
        return true;
}
//------------------------------------------------------------------------------
bool ImageRichText::is_name_containts_point()
{
        if ( false ==is_imagefile_correct() ) return vInterfase::migalo(image_add_);
        if ( false ==vFile::is_name_containts_point( image_add_->image_filename() )) return true;
        QMessageBox::warning(this, program::name_full(), vtr::Message::tr("There was an error when performing operation with the file. \n The file  name  contains a dot.\n Change the name of the file and try to execute operation again."), QMessageBox::Ok);
        return false;
}
//------------------------------------------------------------------------------
void ImageRichText::condition_changed()
{
        set_window_modified( false==is_condition_restored() && is_imagefile_correct() );
}
//------------------------------------------------------------------------------
bool ImageRichText::is_condition_restored() const
{
        return image_add_->is_condition_restored() && rich_edit_->is_condition_restored();
}
//------------------------------------------------------------------------------
bool ImageRichText::is_imagefile_correct() const
{
        return image_add_->is_image_exists() || image_add_->is_image_empty();
}
//------------------------------------------------------------------------------
void ImageRichText::set_window_modified(const bool b)
{
        setWindowModified(b);
        button_ok_->setEnabled(b);
}
//------------------------------------------------------------------------------
/*virtual*/ void ImageRichText::closeEvent(QCloseEvent *event)
{
        if(false == isWindowModified()) return;

        QMessageBox::StandardButton click_button = QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if( click_button == QMessageBox::Cancel ) event->ignore();
        if( click_button == QMessageBox::Save && false ==save_before_close() ) event->ignore();
}
//------------------------------------------------------------------------------
bool ImageRichText::test()
{
        Q_ASSERT(image_add_);
        Q_ASSERT(rich_edit_);
        Q_ASSERT(false ==button_cancel_->text().isEmpty());
        Q_ASSERT(false ==button_ok_->text().isEmpty());
        Q_ASSERT(false ==label_cap_icon_->pixmap()->isNull());
        Q_ASSERT(false ==label_cap_->text().isEmpty());
        Q_ASSERT(false ==label_image_add_->text().isEmpty());
        Q_ASSERT(false ==label_rich_edit_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------