/* 
 * File:   DialogListAnchorRichText.cpp
 * Author: S.Panin
 * 
 * Created on Вт мая 25 2010, 13:07:33
 */
//------------------------------------------------------------------------------
#include "DialogListAnchorRichEdit.h"
//------------------------------------------------------------------------------
using namespace vdialog;
//------------------------------------------------------------------------------
ListAnchorRichText::ListAnchorRichText()
        : QDialog(0)
{
        v_instance::instance< inherited, class_instance > (this, this);
        instance_form();
        instance_signals();
}
//------------------------------------------------------------------------------
void ListAnchorRichText::instance_form()
{
        setWindowTitle(QString(program::name_full()) + "[*]");
        setWindowIcon(QIcon(const_icon_16::program()));
        vDialog::size_hint(this);
        setWindowModified(false);
}
//------------------------------------------------------------------------------
void ListAnchorRichText::instance_signals()
{
        connect(button_cancel_, SIGNAL(clicked()),                                              SLOT(close()));
        connect(button_ok_,     SIGNAL(clicked()),                                              SLOT(save()));
        connect(anchor_,        SIGNAL(on_condition_changed()),                                 SLOT(condition_changed()));
        connect(rich_edit_,     SIGNAL(on_condition_changed()),                                 SLOT(condition_changed()));
        connect(list_widget_,   SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), SLOT(load()));
}
//------------------------------------------------------------------------------
bool ListAnchorRichText::is_condition_restored() const
{
        return anchor_->is_condition_restored() && rich_edit_->is_condition_restored() && list_widget_->currentItem() != 0;
}
//------------------------------------------------------------------------------
void ListAnchorRichText::set_condition_changed(const bool b)
{
        setWindowModified(b);
        button_ok_->setEnabled(b);
}
//------------------------------------------------------------------------------
bool ListAnchorRichText::save()
{
        if (  false ==list_widget_->count() || is_condition_restored()  ) return false;
        if ( anchor_->anchor_item_new().isEmpty()) return anchor_->migalo();
        if ( anchor_->is_anchor_new() && false ==list_widget_->findItems(anchor_->anchor_item_new(), Qt::MatchCaseSensitive).empty() ) return !QMessageBox::critical(0, vtr::Message::tr("Error - %1").arg (program::name_full()), vtr::Message::tr("It is impossible to update the information!\nThe anchor with a name '%1' is already present at the list!").arg(anchor_->anchor_item_new()), QMessageBox::Ok);
        if ( false ==do_save( anchor_->anchor_item_old(), anchor_->anchor_item_new(), rich_edit_->text_html() ) ) return false;

        
        set_condition_changed(false);
        setResult( QDialog::Accepted );
        if ( anchor_->is_anchor_new() ) update_anchors();
        return true;
}
//------------------------------------------------------------------------------
void  ListAnchorRichText::update_anchors()
{
        QString text = list_widget_->currentItem()->text() == anchor_->anchor_item_old() ? anchor_->anchor_item_new() : list_widget_->currentItem()->text();
        list_widget_->setCurrentItem(0);
        set_list_items(do_list_update());
        QList<QListWidgetItem *> items = list_widget_->findItems(text, Qt::MatchCaseSensitive);
        list_widget_->setCurrentItem(false == items.empty() ? items.front() : list_widget_->item(0));
}
//------------------------------------------------------------------------------
void ListAnchorRichText::condition_changed()
{
          set_condition_changed( false==is_condition_restored() );
}
//------------------------------------------------------------------------------
void ListAnchorRichText::load()
{
        if ( list_widget_->currentItem() == 0) return;
        if ( isWindowModified() )
        {
                QMessageBox::StandardButton button = QMessageBox::warning(this, vtr::Dialog::tr("Information - %1").arg(program::name_full()), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard);
                if(button == QMessageBox::Save) save();
        }
        anchor_->set_anchor_item( list_widget_->currentItem()->text() );
        rich_edit_->set_html( do_load(list_widget_->currentItem()->text()) );
        set_condition_changed(false);
}
//------------------------------------------------------------------------------
/*virtual*/ void ListAnchorRichText::closeEvent(QCloseEvent *event)
{
        if(false == isWindowModified()) return;

        QMessageBox::StandardButton click_button = QMessageBox::warning(this, vtr::Dialog::tr("Information - %1").arg(program::name_full()), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(click_button == QMessageBox::Cancel) event->ignore();
        if(click_button == QMessageBox::Save && false ==save() ) event->ignore();
}
//------------------------------------------------------------------------------
bool ListAnchorRichText::test()
{
        Q_ASSERT(anchor_);
        Q_ASSERT(rich_edit_);
        Q_ASSERT(false == button_cancel_ ->text().isEmpty());
        Q_ASSERT(false == button_ok_ ->text().isEmpty());
        Q_ASSERT(false == label_cap_icon_->pixmap()->isNull());
        Q_ASSERT(false == label_cap_ ->text().isEmpty());
        Q_ASSERT(false == label_anchor_ ->text().isEmpty());
        Q_ASSERT(false == label_rich_edit_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------