/* 
 * File:   DialogListAnchorRichText.h
 * Author: S.Panin
 *
 * Created on Чт мая 27 2010, 00:27:37
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_LIST_ANCHOR_RICHTEXT_H_
#define	_V_DIALOG_LIST_ANCHOR_RICHTEXT_H_
//------------------------------------------------------------------------------
#include "../../../../Algorithm/Interfase.h"
#include "DialogListAnchorRichEditInstance.h"
//------------------------------------------------------------------------------
namespace vdialog
{
//------------------------------------------------------------------------------
class ListAnchorRichText : public QDialog, protected vdialog::list_anchor_richtext::Instance
{
Q_OBJECT

    typedef                             QDialog                                 inherited;
    typedef                             ListAnchorRichText                      class_name;
    typedef                             vdialog::list_anchor_richtext::Instance class_instance;

    void                                instance_form();
    void                                instance_signals();
    bool                                is_condition_restored() const;
    void                                set_condition_changed(const bool b);
    void                                update_anchors();

protected:
    virtual void                        closeEvent(QCloseEvent *event);
    virtual bool                        do_save( const QString& anchor_old, const QString& anchor_new, const QString& text) =0;
    virtual QString                     do_load( const QString& term_name ) =0;
    virtual QStringList                 do_list_update( ) =0;

private slots:
    void                                condition_changed();
    void                                load();
    bool                                save();

public:
    explicit                            ListAnchorRichText();
    void                                set_list_items(const QStringList& list) { list_widget_->clear(); list_widget_->addItems(list); }
    void                                set_achor_page(const QString& str)      { anchor_->set_anchor_page(str);}
    bool                                test();
};
//------------------------------------------------------------------------------
}; 
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_LIST_ANCHOR_RICHTEXT_H_ */
