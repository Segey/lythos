/* 
 * File:   DialogListAnchorRichTextInstance.h
 * Author: S.Panin
 *
 * Created on Чт мая 27 2010, 00:44:06
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_LIST_ANCHOR_RICHTEXT_INSTANCE_H_
#define	_V_DIALOG_LIST_ANCHOR_RICHTEXT_INSTANCE_H_
//------------------------------------------------------------------------------
#include "../../../../Instance/FormInstance/FormInstance.h"
#include "../../../Panel/RichEdit/PanelRichEdit.h"
#include "../../../Widget/Anchor/WidgetAnchor.h"
namespace vdialog
{
//------------------------------------------------------------------------------
    namespace list_anchor_richtext
    { 
//------------------------------------------------------------------------------
struct Instance : private go::noncopyable
{
    typedef                     Instance               class_name;

    QLabel*                                                                     label_cap_icon_;
    QLabel*                                                                     label_cap_;
    QLabel*                                                                     label_list_widget_;
    QListWidget*                                                                list_widget_;
    QLabel*                                                                     label_anchor_;
    vWidgetAnchor*                                                              anchor_;
    vPanelRichEdit*                                                             rich_edit_;
    QLabel*                                                                     label_rich_edit_;
    QPushButton*                                                                button_ok_;
    QPushButton*                                                                button_cancel_;

    void                        instance_widgets();
    void                        read_options()                                  {}
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout();
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate()                                     {}
};
    //--------------------------------------------------------------------------
    }; 
//------------------------------------------------------------------------------
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_LIST_ANCHOR_RICHTEXT_INSTANCE_H_ */
