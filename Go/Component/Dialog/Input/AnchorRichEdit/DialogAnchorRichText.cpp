/* 
 * File:   DialogInputImageRichText.cpp
 * Author: S.Panin
 * 
 * Created on Вт мая 25 2010, 13:07:33
 */
//------------------------------------------------------------------------------
#include "DialogAnchorRichText.h"
//------------------------------------------------------------------------------
using namespace vdialog;
//------------------------------------------------------------------------------
AnchorRichText::AnchorRichText()
        : QDialog(0)
{
        v_instance::instance< inherited, class_instance > (this, this);
        instance_form();
        instance_signals();
}
//------------------------------------------------------------------------------
void AnchorRichText::instance_form()
{
        setWindowTitle(QString(program::name_full()) + "[*]");
        setWindowIcon(QIcon(const_icon_16::program()));
        vDialog::size_hint(this);
        setWindowModified(false);
}
//------------------------------------------------------------------------------
void AnchorRichText::instance_signals()
{
        connect(button_cancel_, SIGNAL(clicked()),                      SLOT(close()));
        connect(button_ok_,     SIGNAL(clicked()),                      SLOT(save()));
        connect(anchor_,        SIGNAL(on_condition_changed()),         SLOT(condition_changed()));
        connect(rich_edit_,     SIGNAL(on_condition_changed()),         SLOT(condition_changed()));
}
//------------------------------------------------------------------------------
bool AnchorRichText::is_condition_restored() const
{
        return anchor_->is_condition_restored() && rich_edit_->is_condition_restored();
}
//------------------------------------------------------------------------------
void AnchorRichText::set_condition_changed(const bool b)
{
        setWindowModified(b);
        button_ok_->setEnabled(b);
}
//------------------------------------------------------------------------------
void AnchorRichText::save()
{
        if (save_before_close()) accept();
}
//------------------------------------------------------------------------------
bool AnchorRichText::save_before_close()
{
        if ( is_condition_restored() ) return false;
        if ( list_.indexOf(anchor()) != -1 && anchor_->is_anchor_new()) return!QMessageBox::critical(0, QString ("%1 - %2").arg (label_cap_->text()).arg(program::name_full()) , vtr::Message::tr("The anchor with a name '%1' is already present at the list!").arg(anchor()), QMessageBox::Ok);
        if ( anchor_->anchor_item_new().isEmpty() ) return anchor_->migalo();

        set_condition_changed(false);
        return true;
}
//------------------------------------------------------------------------------
void AnchorRichText::condition_changed()
{
          set_condition_changed( false==is_condition_restored() );
}
//------------------------------------------------------------------------------
/*virtual*/ void AnchorRichText::closeEvent(QCloseEvent *event)
{
        if(false == isWindowModified()) return;

        QMessageBox::StandardButton click_button = QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if( click_button == QMessageBox::Cancel ) event->ignore();
        if( click_button == QMessageBox::Save && false ==save_before_close() ) event->ignore();
}
//------------------------------------------------------------------------------
bool AnchorRichText::test()
{
        Q_ASSERT(anchor_);
        Q_ASSERT(rich_edit_);
        Q_ASSERT(false ==button_cancel_ ->text().isEmpty());
        Q_ASSERT(false ==button_ok_     ->text().isEmpty());
        Q_ASSERT(false ==label_cap_icon_->pixmap()->isNull());
        Q_ASSERT(false ==label_cap_     ->text().isEmpty());
        Q_ASSERT(false ==label_anchor_  ->text().isEmpty());
        Q_ASSERT(false ==label_rich_edit_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------