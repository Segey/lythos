/* 
 * File:   DialogAnchorRichTextInstance.cpp
 * Author: S.Panin
 * 
 * Created on Чт мая 27 2010, 00:44:11
 */
//------------------------------------------------------------------------------
#include "DialogAnchorRichTextInstance.h"
//------------------------------------------------------------------------------
using namespace vdialog::anchor_rich_text;
//------------------------------------------------------------------------------
void Instance::instance_widgets()
{
        label_cap_              = new QLabel;
        label_cap_icon_         = new QLabel;
        label_anchor_           = new QLabel;
        anchor_                 = new vWidgetAnchor;
        label_rich_edit_        = new QLabel;
        rich_edit_              = new vPanelRichEdit;             
        button_ok_              = new QPushButton;
        button_cancel_          = new QPushButton;

        button_ok_->setDefault(true);
        button_ok_->setEnabled(false);
}
//------------------------------------------------------------------------------
QLayout* Instance::instance_top_layout()
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(label_cap_icon_, label_cap_);
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* Instance::instance_middle_layout()
{
        vLay lay(new QGridLayout);
        lay.add_caption_bevel_first(label_anchor_);
        lay.add_unary(anchor_, false);
        lay.add_empty(9, false);
        lay.add_caption_bevel_first(label_rich_edit_);
        lay.add_unary(rich_edit_ , false);
        return lay.clayout();
}
//------------------------------------------------------------------------------
QLayout* Instance::instance_bottom_layout()
{
        vBottomInstance BottomInstance;
        BottomInstance.add_bevel();
        BottomInstance.add_buttons(button_ok_, button_cancel_);
        return BottomInstance.clayout_with_size_hint();
}