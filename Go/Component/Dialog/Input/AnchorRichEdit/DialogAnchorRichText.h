/* 
 * File:   DialogAnchorRichText.h
 * Author: S.Panin
 *
 * Created on Чт мая 27 2010, 00:27:37
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_ANCHOR_RICHTEXT_H_
#define	_V_DIALOG_ANCHOR_RICHTEXT_H_
//------------------------------------------------------------------------------
#include "../../../../Algorithm/Interfase.h"
#include "DialogAnchorRichTextInstance.h"
//------------------------------------------------------------------------------
namespace vdialog
{ 
//------------------------------------------------------------------------------
class AnchorRichText : public QDialog, protected anchor_rich_text::Instance
{
Q_OBJECT

    typedef                             QDialog                                 inherited;
    typedef                             AnchorRichText                          class_name;
    typedef                             anchor_rich_text::Instance              class_instance;

    void                                instance_form();
    void                                instance_signals();
    bool                                is_condition_restored() const;
    void                                set_condition_changed(const bool b);
    bool                                save_before_close();

protected:
    virtual void                        closeEvent(QCloseEvent *event);

private slots:
    void                                save();
    void                                condition_changed();

public:
    explicit                            AnchorRichText();
    virtual                            ~AnchorRichText()                        { }
    QString                             anchor() const                          { return anchor_->anchor_item_new();}
    QString                             html() const                            { return rich_edit_->text_html();}
    void                                set_list_existing_anchors(const QStringList& list) {list_ =list;}
    void                                set_achor_page(const QString& str)      {anchor_->set_anchor_page(str);}
    bool                                test();
};
//------------------------------------------------------------------------------
}; 
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INPUT_ANCHOR_RICHTEXT_H_ */
