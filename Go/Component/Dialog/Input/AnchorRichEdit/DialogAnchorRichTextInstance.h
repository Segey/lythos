/* 
 * File:   DialogAnchorRichTextInstance.h
 * Author: S.Panin
 *
 * Created on Чт мая 27 2010, 00:44:06
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_ANCHOR_RICHTEXT_INSTANCE_H_
#define	_V_DIALOG_ANCHOR_RICHTEXT_INSTANCE_H_
//------------------------------------------------------------------------------
#include "../../../../Instance/FormInstance/FormInstance.h"
#include "../../../Panel/RichEdit/PanelRichEdit.h"
#include "../../../Widget/Anchor/WidgetAnchor.h"
namespace vdialog
{ // NAMESPACE VDIALOG BEGIN
//------------------------------------------------------------------------------
    namespace anchor_rich_text
    { // NAMESPACE VEG_BEGIN
//------------------------------------------------------------------------------
struct Instance : private go::noncopyable
{
    typedef                     Instance                                        class_name;

    QLabel*                                                                     label_cap_icon_;
    QLabel*                                                                     label_cap_;
    QLabel*                                                                     label_anchor_;
    vWidgetAnchor*                                                              anchor_;
    vPanelRichEdit*                                                             rich_edit_;
    QLabel*                                                                     label_rich_edit_;
    QPushButton*                                                                button_ok_;
    QPushButton*                                                                button_cancel_;

    QStringList                                                                 list_;

    void                        instance_widgets();
    void                        read_options()                                  {}
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout();
    void                        instance_images()                               {}
    void                        instance_tab_order()                            {}
    void                        translate()                                     {}
};
    //------------------------------------------------------------------------------
    }; // NAMESPACE ANCHOR_RICH_TEXT
//------------------------------------------------------------------------------
}; // NAMESPACE VEG_END
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INPUT_IMAGE_RICHTEXT_INSTANCE_H_ */
