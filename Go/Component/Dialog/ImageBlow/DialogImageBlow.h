/* 
 * File:   vDialogImageBlow.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_IMAGE_BLOW_H_
#define _V_DIALOG_IMAGE_BLOW_H_
//------------------------------------------------------------------------------
class vDialogImageBlow : public QDialog
{
    typedef                         QDialog                                     inherited;
    typedef                         vDialogImageBlow                            class_name;

    static const int  time_count_ =100;
    int                                                                         min_width_;
    int                                                                         min_height_;
    int                                                                         max_width_;
    int                                                                         max_height_;
    int                                                                         left_;
    int                                                                         top_;
    QLabel*                                                                     parent_;
    QLabel*                                                                     image_;

    void                            instance_layout();
    void                            instance_image();
    std::pair<int,int>              calculate_distance();

protected:
    virtual void                    mousePressEvent(QMouseEvent* event);
   
public:
    vDialogImageBlow(QLabel* image, QWidget* parent);
    void                            blow_up();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_BLOW_EFFECT_H_ */
