/* 
 * File:   vDialogImageBlow.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "DialogImageBlow.h"
//------------------------------------------------------------------------------
vDialogImageBlow::vDialogImageBlow(QLabel* image, QWidget* parent)
                : QDialog(parent,  Qt::FramelessWindowHint),  parent_(image)
{
        image->setVisible(false);

        min_width_ = image->geometry().width();
        min_height_ = image->geometry().height();
        max_width_ = image->sizeHint().width();
        max_height_ = image->sizeHint().height();
        left_ = image->mapToGlobal(QPoint(0, 0)).x();
        top_ = image->mapToGlobal(QPoint(0, 0)).y();

        instance_image();
        instance_layout();
}
//------------------------------------------------------------------------------
std::pair<int,int> vDialogImageBlow::calculate_distance()
{
        int half_width =QDesktopWidget().screenGeometry().width() / 2;
        int half_height =QDesktopWidget().screenGeometry().height() / 2;

        if (max_width_ >  half_width || max_height_ > half_height) return std::make_pair(half_width - min_width_, half_height - min_height_);
        else return std::make_pair(max_width_ - min_width_, max_height_ - min_height_);
}
//------------------------------------------------------------------------------
void vDialogImageBlow::instance_layout()
{
        inherited::setGeometry(left_, top_, min_width_, min_height_);

        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(image_);
}
//------------------------------------------------------------------------------
void vDialogImageBlow::instance_image()
{
        image_ = new QLabel;
        image_->setFrameShape(QFrame::StyledPanel);
        image_->setScaledContents(true);
        image_->setMinimumSize(min_width_, min_height_);
        if(parent_->pixmap()) image_->setPixmap(*parent_->pixmap());
}
//------------------------------------------------------------------------------
void vDialogImageBlow::blow_up()
{
        show();

        const int weight = calculate_distance().first / time_count_;
        const int height = calculate_distance().second / time_count_;
        const int weight_ = weight/2;
        const int height_ = height/2;

        for(int x = 0; x < time_count_; ++x)
        {
                QApplication::processEvents();
                inherited::setGeometry(left_ - weight_ * x, top_ - height_ * x, min_width_ + weight *x, min_height_ + height * x);
                QApplication::processEvents();
        }
}
//------------------------------------------------------------------------------
/*virtual*/ void vDialogImageBlow::mousePressEvent(QMouseEvent* event)
{
        inherited::hide();
        parent_->setVisible(true);
        inherited::mousePressEvent(event);
}
//------------------------------------------------------------------------------
bool vDialogImageBlow::test()
{
         Q_ASSERT (parent_);
         Q_ASSERT (image_);
         Q_ASSERT (!image_->pixmap()->isNull());
        return true;
}

//------------------------------------------------------------------------------