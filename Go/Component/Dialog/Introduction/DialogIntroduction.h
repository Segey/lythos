/* 
 * File:   DialogIntroduction.h
 * Author: S.Panin
 *
 * Created on 13 Август 2009 г., 13:28
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_INTRODUCTION_H_
#define	_V_DIALOG_INTRODUCTION_H_
//------------------------------------------------------------------------------
#include "../../../go/noncopyable.h"
#include "DialogIntroductionData.h"
//------------------------------------------------------------------------------
class vDialogIntroduction : public QDialog, go::noncopyable
{
Q_OBJECT

    typedef                             QDialog                                 inherited;
    typedef                             vDialogIntroduction                     class_name;
    typedef                             vDialogIntroductionData                 data_name;

    boost::shared_ptr<data_name>                                                data_;


    void                                instance_form();
    void                                instance_signals();
    QString                             add_signature(const QString& text);

private slots:
    void                                print();
    void                                buy();

public:
    explicit vDialogIntroduction();
    bool                                test();
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INTRODUCTION_H_ */
