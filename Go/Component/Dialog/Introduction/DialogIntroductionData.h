// -*- C++ -*-
/* 
 * File:   DialogIntroductionData.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 12:44
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_INTRODUCTION_DATA_H_
#define	_V_DIALOG_INTRODUCTION_DATA_H_
//------------------------------------------------------------------------------
struct vDialogIntroductionData
{

    static QString                          name_dll()                          { return const_instance_libs::di1245(); }

    QLabel*                                                                     top_image_label_;
    QLabel*                                                                     text_label_;
    QLabel*                                                                     bottom_image_label_;
    QPushButton*                                                                buy_button_;
    QPushButton*                                                                print_button_;
    QPushButton*                                                                thanks_button_;

    void instance_images()
    {
        buy_button_         ->setIcon(const_icon_16::program());
        print_button_       ->setIcon(const_icon_16::print());
        thanks_button_      ->setIcon(const_icon_16::thanks());
        top_image_label_    ->setPixmap(const_pixmap_introduction::top());
        bottom_image_label_ ->setPixmap(const_pixmap_introduction::bottom());
    }

    void translate()
    {
        buy_button_     ->setText(vtr::Button::tr("&How To Buy %1").arg(program::name()));
        print_button_   ->setText(vtr::Button::tr("&Print..."));
        thanks_button_  ->setText(vtr::Button::tr("&Thanks"));
        text_label_     ->setText(vtr::Message::tr("Dear Madam,\nDear Sir.\n\n   Thank you very much for evaluating %1. I am sure you will soon find %1 to be an assistant you don't want to be without, when working with trainings.\n\n   %1 tracks everything related to bodybuilding and fitness: track exercises, meals, measurements, record results, general health and more! You can quickly and easily work with your product information directly in %1. Easily track and visualize progress, know if your training workouts are effective, and reach goals faster. Includes a  journal for recording information and four major tools to view and analyze progress.\n\n   %1 is highly customizable. Whether you're a bodybuilder, a cyclist, training for a marathon, or just want to monitor and control your body weight, %1 has everything you need.").arg(program::name_total()));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_INTRODUCTION_DATA_H_ */

