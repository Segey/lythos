/* 
 * File:   DialogIntroduction.cpp
 * Author: S.Panin
 * 
 * Created on 13 Август 2009 г., 13:28
 */
//------------------------------------------------------------------------------
#include "DialogIntroduction.h"
//------------------------------------------------------------------------------
vDialogIntroduction::vDialogIntroduction()
        : QDialog(0, Qt::WindowSystemMenuHint), data_(new data_name)
{
        vDllInstance::instance(this, data_.get());
        instance_form();
        instance_signals();
}
//------------------------------------------------------------------------------
void vDialogIntroduction::instance_form()
{
        setWindowTitle(QString(program::name_full()) + vtr::Copyright::tr(" [trial version]"));
        data_->instance_images();
        data_->translate();
        setWindowIcon(QIcon(const_icon_16::program()));
        setFixedSize(sizeHint());
}
//------------------------------------------------------------------------------
void vDialogIntroduction::instance_signals()
{
        connect(data_->thanks_button_, SIGNAL(clicked()),       SLOT(close()));
        connect(data_->print_button_,  SIGNAL(clicked()), this, SLOT(print()));
        connect(data_->buy_button_,    SIGNAL(clicked()), this, SLOT(buy()));
}
//------------------------------------------------------------------------------
QString vDialogIntroduction::add_signature(const QString& text)
{
        QString Text(text);
        Text.append(vtr::Copyright::tr("\n\nSincerely,\n     Panin S."));
        return Text;
}
//------------------------------------------------------------------------------
void vDialogIntroduction::print()
{
        boost::shared_ptr<QTextEdit> text_edit(new QTextEdit(this));
        text_edit->document()->setPlainText(add_signature(data_->text_label_->text()));

        vInterfase::print(text_edit.get(), vtr::Dialog::tr("Print Introduction"));
}
//------------------------------------------------------------------------------
void vDialogIntroduction::buy()
{
        OVERRIDE_CURSOR(QDesktopServices::openUrl(QUrl(program::url_buy()));)
}
//------------------------------------------------------------------------------
bool vDialogIntroduction::test()
{
        Q_ASSERT(data_->top_image_label_);
        Q_ASSERT(data_->text_label_);
        Q_ASSERT(data_->bottom_image_label_);
        Q_ASSERT(data_->buy_button_);
        Q_ASSERT(data_->print_button_);
        Q_ASSERT(data_->thanks_button_);
        Q_ASSERT(!data_->top_image_label_->pixmap()->isNull());
        Q_ASSERT(data_->top_image_label_->text().isEmpty());
        Q_ASSERT(!data_->bottom_image_label_->pixmap()->isNull());
        Q_ASSERT(data_->bottom_image_label_->text().isEmpty());
        Q_ASSERT(!data_->buy_button_->icon().isNull());
        Q_ASSERT(!data_->buy_button_->text().isEmpty());
        Q_ASSERT(!data_->print_button_->icon().isNull());
        Q_ASSERT(!data_->print_button_->text().isEmpty());
        Q_ASSERT(!data_->thanks_button_->icon().isNull());
        Q_ASSERT(!data_->thanks_button_->text().isEmpty());
        Q_ASSERT(add_signature(QString()) == vtr::Copyright::tr("\n\nSincerely,\n     Panin S."));
        return true;
}
//------------------------------------------------------------------------------