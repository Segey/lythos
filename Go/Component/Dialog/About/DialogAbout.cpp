/* 
 * File:   AboutDialog.cpp
 * Author: S.Panin
 * 
 * Created on 13 Август 2009 г., 13:28
 */
//------------------------------------------------------------------------------
#include "DialogAbout.h"
//------------------------------------------------------------------------------
vDialogAbout::vDialogAbout()
      : QDialog(0, Qt::WindowCloseButtonHint), data_(new data_name)
{
        vDllInstance::instance(this, data_.get());
        instance_form();
        set_user_info();
        
}
//------------------------------------------------------------------------------
void vDialogAbout::instance_form()
{
        data_->instance_images();
        data_->translate();
        setWindowIcon(const_icon_16::program());
        setWindowTitle(program::name_full());
        if(sizeHint().width() > 0) setFixedSize(data_->background_label_->sizeHint() + QSize(20,0));
}
//------------------------------------------------------------------------------
void vDialogAbout::set_user_info()
{
//vMain().userID.get_registration_info();
        //_XXX_
}
//------------------------------------------------------------------------------
bool vDialogAbout::test()
{
        Q_ASSERT(data_->background_label_);
        Q_ASSERT(data_->text_label_);
        Q_ASSERT(data_->icon_label_);
        Q_ASSERT(data_->user_info_label_);
        Q_ASSERT(data_->user_licence_label_);
        Q_ASSERT(data_->warning_label_);
        Q_ASSERT(data_->copyright_label_);
        Q_ASSERT(windowTitle() == program::name_full());
        Q_ASSERT(!data_->background_label_->pixmap()->isNull());
        Q_ASSERT(data_->background_label_->text().isEmpty());
        Q_ASSERT(!data_->text_label_->pixmap()->isNull());
        Q_ASSERT(data_->text_label_->text().isEmpty());
        Q_ASSERT(!data_->icon_label_->pixmap()->isNull());
        Q_ASSERT(data_->icon_label_->text().isEmpty());
        Q_ASSERT(!data_->user_info_label_->text().isEmpty());
        Q_ASSERT(!data_->warning_label_->text().isEmpty());
        Q_ASSERT(!data_->copyright_label_->text().isEmpty());
        return true;
}
//------------------------------------------------------------------------------