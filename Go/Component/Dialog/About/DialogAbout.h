/* 
 * File:   AboutDialog.h
 * Author: S.Panin
 *
 * Created on 13 Август 2009 г., 13:28
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_ABOUT_H_
#define	_V_DIALOG_ABOUT_H_
//------------------------------------------------------------------------------
#include <boost/shared_ptr.hpp>
#include "../../../const.h"
#include "../../../Const/const_icon.h"
#include "../../../go/noncopyable.h"
#include "../../../Instance/DllInstance.h"
#include "DialogAboutData.h"
//------------------------------------------------------------------------------
class vDialogAbout : public QDialog, go::noncopyable
{
    Q_OBJECT

    typedef                             QDialog                                 inherited;
    typedef                             vDialogAbout                            class_name;
    typedef                             vDialogAboutData                        data_name;

    boost::shared_ptr<data_name>                                                data_;

    void                                set_user_info();
    void                                instance_form();

public:
    explicit vDialogAbout();
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_ABOUT_H_ */
