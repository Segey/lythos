// -*- C++ -*-
/* 
 * File:   DialogAboutData.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 11:33
 */
//------------------------------------------------------------------------------
#ifndef _V_DIALOG_ABOUT_DATA_H_
#define	_V_DIALOG_ABOUT_DATA_H_
//------------------------------------------------------------------------------
#include "../../../const.h"
#include "../../../Const/const_icon.h"
//------------------------------------------------------------------------------
struct vDialogAboutData
{

    static QString                      name_dll()                              { return const_instance_libs::da1128(); }
    QLabel*                                                                     background_label_;
    QLabel*                                                                     text_label_;
    QLabel*                                                                     icon_label_;
    QLabel*                                                                     user_info_label_;
    QLabel*                                                                     user_licence_label_;
    QLabel*                                                                     warning_label_;
    QLabel*                                                                     copyright_label_;

    void instance_images()
    {
        background_label_->setPixmap(const_pixmap_introduction::about_background());
        icon_label_->setPixmap      (const_pixmap_introduction::about_icon());
        text_label_->setPixmap      (const_pixmap_introduction::about_text());
    }
    void translate()
    {
        user_info_label_    ->setText(vtr::Copyright::tr("This product is licensed to:"));
        warning_label_      ->setText(vtr::Copyright::tr("Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law."));
        copyright_label_    ->setText(vtr::Copyright::tr("Copyright (C) 2005-2010 Lythos Software Inc. All rights reserved."));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_DIALOG_ABOUT_DATA_H_ */

