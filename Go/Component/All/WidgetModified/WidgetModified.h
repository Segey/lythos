// -*- C++ -*-
/* 
 * File:   WidgetModified.h
 * Author: S.Panin
 *
 * Created on 5 Январь 2010 г., 0:00
 */
//------------------------------------------------------------------------------
#ifndef _V_WIDGET_MODIFIED_H_
#define	_V_WIDGET_MODIFIED_H_
//------------------------------------------------------------------------------
#include "../../../BuiltType/String.h"
//------------------------------------------------------------------------------
template<typename T>
class vWidgetModified : public T
{
    typedef                             T                                       inherited;
    typedef                             vWidgetModified                         class_name;

    bool                                                                        enabled_;

public:
    explicit                            vWidgetModified(QWidget* parent =0) : T(parent), enabled_(true) {}
    void                                set_modified_enabled(bool value)        { enabled_ = value; }
    bool                                is_modified_enabled() const             { return enabled_; }
    void                                setWindowModified(bool value)
    {
        if (  enabled_ == false ) return;
        inherited::setWindowModified(value);
        inherited::setTitle( go::string::get_string_modified(inherited::title(),value));
    }
    virtual                             ~vWidgetModified() {}
};
//------------------------------------------------------------------------------
#endif	/* _V_WIDGET_MODIFIED_H_ */

