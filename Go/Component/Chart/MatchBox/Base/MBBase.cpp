/* 
 * File:   vMB.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------

#include "MBBase.h"
//------------------------------------------------------------------------------
void vMBBase::paintEvent(QPaintEvent*)
{
        QPainter painter(this);
        painter.scale(inherited::width() / 100.0, inherited::height() / 100.0);
        painter.setRenderHint(QPainter::Antialiasing, true);
        canvas_clear(&painter);
        border_draw(&painter);

        const int caption_height = font_height(caption_font()) + little_ * 2;
        caption_draw(&painter, QRect(0, 0, width(), caption_height));
        height_without_caption_ = height_max_ - caption_height;
        
        vArea area = do_calculate_areas();
        do_draw_ox(&painter, area.ox);
        do_draw_oy(&painter, area.oy);
        do_draw_graph(&painter, area.graph);
        do_draw_values(&painter, area.values);
}
//------------------------------------------------------------------------------
void vMBBase::border_draw(QPainter* painter)
{
        painter->setPen(QPen(Qt::black, 0, Qt::SolidLine));
        painter->setBrush(Qt::white);
        painter->drawRect(0, 0, width_max_, height_max_);
}
//------------------------------------------------------------------------------
void vMBBase::text_draw(QPainter* painter, const QFont& font, const QRectF& rect, const QString& text, Qt::Alignment align)
{
        painter->setFont(font);
        painter->setPen(QPen(Qt::black, 0, Qt::SolidLine));
        painter->drawText(rect, align, text);
}
//------------------------------------------------------------------------------
vMBBase::ValuesString vMBBase::values_minmax_string() const
{
        const Values values = value_minmax();
        return vMBBase::ValuesString(QString::number(values.get<0>()), QString::number(values.get<1>()));
}
//------------------------------------------------------------------------------
vMBBase::ValuesString vMBBase::values_percent_unsort() const
{
        double first =100.0; double second =100.0;
        if (value_first_.value > value_second_.value)  second = 100.0 * value_second_.value / vValidator::get_not_zero(value_first_.value);
        else first = 100.0 *value_first_.value / vValidator::get_not_zero(value_second_.value);

        if ( !value_second_.value && !value_first_.value ) { first =0 ; second =0; }
        if ( value_second_.value < 0 ) { second =0; }
        if ( value_first_.value < 0) { first =0 ; }
        
        const QString text_first        = first  > 10 ? QString("%1%").arg(QString::number(first,'g',4))  : QString("%1%").arg(QString::number(first,'g',3));
        const QString text_second       = second > 10 ? QString("%1%").arg(QString::number(second,'g',4)) : QString("%1%").arg(QString::number(second,'g',3));
        return  vMBBase::ValuesString(text_first, text_second );
}
//------------------------------------------------------------------------------
vMBBase::ValuesString vMBBase::values_percent_minmax() const
{
        double first =100.0; double second =100.0;
        if (value_first_.value > value_second_.value)  second = 100.0 * value_second_.value / vValidator::get_not_zero(value_first_.value); 
        else first = 100.0 *value_first_.value / vValidator::get_not_zero(value_second_.value);

        if ( !value_second_.value && !value_first_.value ) { first =0 ; second =0; }
        if ( value_second_.value < 0 ) { second =0; }
        if ( value_first_.value < 0) { first =0 ; }

        const QString text_first        = first  > 10 ? QString("%1%").arg(QString::number(first,'g',4))  : QString("%1%").arg(QString::number(first,'g',3));
        const QString text_second       = second > 10 ? QString("%1%").arg(QString::number(second,'g',4)) : QString("%1%").arg(QString::number(second,'g',3));
        return first > second ? vMBBase::ValuesString(text_second, text_first) : vMBBase::ValuesString(text_first, text_second );
}
//------------------------------------------------------------------------------
vMBBase::ValuesString vMBBase::values_percent_minmax_together() const
{
        const double summ               = value_first_.value + value_second_.value;
        double first                   = 100.0 *  (summ ? value_first_.value : 1) / vValidator::get_not_zero(summ ? summ : 2 );
        double second                  = 100.0 - first;
        const QString text_first        = first  > 10 ? QString("%1%").arg(QString::number(first,'g',4))  : QString("%1%").arg(QString::number(first,'g',3));
        const QString text_second       = second > 10 ? QString("%1%").arg(QString::number(second,'g',4)) : QString("%1%").arg(QString::number(second,'g',3));

//        if ( value_second_.value < 0 ) { second =0; }
//        if ( value_first_.value < 0) { first =0 ; }

        return first > second ? vMBBase::ValuesString(text_second, text_first) : vMBBase::ValuesString(text_first, text_second );
}
//------------------------------------------------------------------------------
void vMBBase::set_graph_colors(vGraphColors color)
{
    switch(color)
        {
        default:
        case vGraphColors::red:       graph_colors_ =std::make_pair(Qt::red,     Qt::darkRed);      graph_colors_id_ = vGraphColors::red;     break;
        case vGraphColors::green:     graph_colors_ =std::make_pair(Qt::green,   Qt::darkGreen);    graph_colors_id_ = vGraphColors::green;   break;
        case vGraphColors::blue:      graph_colors_ =std::make_pair(Qt::blue,    Qt::darkBlue);     graph_colors_id_ = vGraphColors::blue;    break;
        case vGraphColors::cyan:      graph_colors_ =std::make_pair(Qt::cyan,    Qt::darkCyan);     graph_colors_id_ = vGraphColors::cyan;    break;
        case vGraphColors::magenta:   graph_colors_ =std::make_pair(Qt::magenta, Qt::darkMagenta);  graph_colors_id_ = vGraphColors::magenta; break;
        case vGraphColors::yellow:    graph_colors_ =std::make_pair(Qt::yellow,  Qt::darkYellow);   graph_colors_id_ = vGraphColors::yellow;  break;
        case vGraphColors::gray:      graph_colors_ =std::make_pair(Qt::white,   Qt::lightGray);    graph_colors_id_ = vGraphColors::gray;    break;
        case vGraphColors::black:     graph_colors_ =std::make_pair(Qt::white,   Qt::black);        graph_colors_id_ = vGraphColors::black;   break;
        }

    update();
}
//------------------------------------------------------------------------------
QLinearGradient vMBBase::graph_gradient()
{
        QLinearGradient gradient(0, 0, 0, 100);
        gradient.setColorAt(0.1, graph_colors().get<0>());
        gradient.setColorAt(1.0, graph_colors().get<1>());
        return gradient;
}
//------------------------------------------------------------------------------
QString vMBBase::get_settings() const
{
    vMB_base::type_settings::head_type type =do_get_class_name();
    vMB_base::type_settings::inherited::head_type color =get_chart_colors();

    QString settings;
    QTextStream stream(&settings);
    stream  << type <<" " <<  color;

    return settings;
}
//------------------------------------------------------------------------------
bool vMBBase::test()
{
        value_first_.value      = 20;
        value_second_.value     = 100;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(16.67) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(83.33) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(20) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(20) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = 66;
        value_second_.value     = 5;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(7.04) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(92.96) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(7.58) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(7.58) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = 44;
        value_second_.value     = 99;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(30.77) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(69.23) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(44.44) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(44.44) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = 0;
        value_second_.value     = 0;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(50) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(50) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(0) );

        value_first_.value      = 0.00;
        value_second_.value     = 9;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = 4;
        value_second_.value     = 0;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = 1;
        value_second_.value     = 8;
        Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(11.11) );
        Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(88.89) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(12.5) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(12.5) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = -11;
        value_second_.value     = 8;
      //  Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(11.11) );
      //  Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(88.89) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(100) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(100) );

        value_first_.value      = -33;
        value_second_.value     = -98;
     //   Q_ASSERT( values_percent_minmax_together().get<0>() == QString("%1%").arg(11.11) );
     //   Q_ASSERT( values_percent_minmax_together().get<1>() == QString("%1%").arg(88.89) );
        Q_ASSERT( values_percent_unsort().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_unsort().get<1>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<0>() == QString("%1%").arg(0) );
        Q_ASSERT( values_percent_minmax().get<1>() == QString("%1%").arg(0) );
//
//        SMS(values_percent_minmax().get<0>());
//        SMS(values_percent_minmax().get<1>());
        return true;
}
//------------------------------------------------------------------------------