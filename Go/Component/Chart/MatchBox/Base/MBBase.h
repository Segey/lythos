/* 
 * File:   MBBase.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_BASE_H_
#define _V_MB_BASE_H_
//------------------------------------------------------------------------------
struct  vMB_base : private go::noncopyable
{
    typedef                boost::tuple<unsigned short, unsigned short> type_settings;
    enum                   vHierarchyType                                       { type_histogram = 1, type_pie = 2, type_text =3 };
    enum                   vHierarchyStyle                                      { style_simple =10, style_percentage = 20, style_scale = 30 };
    enum                   vHierarchy                                           { histogram_percentage = type_histogram + style_percentage, histogram_scale = type_histogram + style_scale, histogram_simple = type_histogram +style_simple,  pie_percentage = type_pie + style_percentage,  pie_scale = type_pie+ style_scale,  pie_simple = type_pie +style_simple,  text_percentage =type_text + style_percentage,  text_scale =type_text+ style_scale, text_simple =type_text+style_simple };
    enum                   vGraphColors                                         { red = 1, green = 2, blue = 3, cyan = 4, magenta = 5, yellow = 6, gray = 7, black = 8 };

    static int             maximumHeight()                                      { return 150; }
    static int             minimumHeight()                                      { return 80; }

protected:
    static const int height_max_                                                = 100;
    static const int width_max_                                                 = 100;
    static const int little_                                                    = 2;
    static const int middle_                                                    = 4;
    static const int large_                                                     = 6;
    static const int caption_font_height_                                       = 6;
    static const int values_font_height_                                        = 5;
};
//------------------------------------------------------------------------------
class vMBBase : public QWidget, public vMB_base
{
            typedef         QWidget                                             inherited;
            typedef         vMBBase                                             class_name;

public:
    struct vValue    {
        QString         caption;
        double          value;
        vValue() : caption(QString()), value(0) { }
        vValue(const QString& Caption, int Value) : caption(Caption), value(Value){ }
    };

protected:
    struct vData;
    struct vArea;
    typedef                     boost::tuple<QString,double>                    ValueType;
    typedef                     boost::tuple<QColor, QColor>                    GraphColors;
    typedef                     boost::tuple<double, double>                    Values;
    typedef                     boost::tuple<QString, QString>                  ValuesString;

private:
    inherited                                                                   parent_;
    QString                                                                     caption_;
    vValue                                                                      value_first_;
    vValue                                                                      value_second_;
    GraphColors                                                                 graph_colors_;
    vGraphColors                                                                graph_colors_id_;
    int                                                                         height_without_caption_;


    QFont                       caption_font()                                  { QFont font; font.setPointSize(caption_font_height_); font.setBold(true);  return font; }
    void                        canvas_clear(QPainter* painter)                 { painter->eraseRect(0, 0, 100, 100); }
    void                        border_draw(QPainter* painter);
    void                        caption_draw(QPainter* painter, const QRect& rect) { text_draw(painter, caption_font(), rect, caption_); }

protected:
    virtual vArea               do_calculate_areas()                                    = 0;
    virtual void                do_draw_ox(QPainter* painter, const vData& data)        = 0;
    virtual void                do_draw_oy(QPainter* painter, const vData& data)        = 0;
    virtual void                do_draw_graph(QPainter* painter, const vData& data)     = 0;
    virtual void                do_draw_values(QPainter* painter, const vData& data)    = 0;
    virtual vHierarchy          do_get_class_name() const                               = 0;
    virtual vHierarchyType      do_get_class_type() const                               = 0;
    virtual vHierarchyStyle     do_get_class_style() const                              = 0;

protected:
    int&                        height()                                        { return height_without_caption_; }
    static int                  width()                                         { return width_max_; }
    static int                  little()                                        { return little_; }
    static int                  middle()                                        { return middle_; }
    static int                  large()                                         { return large_; }
    static int                  max_height()                                    { return height_max_; }
    int                         caption_height()                                { return height_max_ - height_without_caption_; }
    vValue&                     value_first()                                   { return value_first_; }
    vValue&                     value_second()                                  { return value_second_; }
    Values                      value_minmax() const                            { return boost::minmax(value_first_.value, value_second_.value); }
    ValuesString                values_minmax_string() const;
    ValuesString                values_percent_unsort() const;
    ValuesString                values_percent_minmax() const;
    ValuesString                values_percent_minmax_together() const;
    GraphColors&                graph_colors()                                  { return graph_colors_; }
    static void                 text_draw(QPainter* painter, const QFont& font, const QRectF& rect, const QString& text, Qt::Alignment = Qt::AlignCenter);
    QFont                       value_font()                                    { QFont font; font.setPointSize(values_font_height_); return font; }
    QSize                       font_size(const QFont& font, const QString& text) {return vDLL<QSize>(const_libs::f12nt(), "size")(QSize(), font, text); }
    int                         font_height(const QFont& font)                  { return vDLL<int>(const_libs::f12nt(), "height")(int(), font); }
    QLinearGradient             graph_gradient();

public:
    explicit                    vMBBase(inherited* parent, vGraphColors color = vMBBase::red)  : parent_(parent) { set_graph_colors(color); }
    virtual                     ~vMBBase()                                          { }
    virtual void                paintEvent(QPaintEvent *);
    void                        set_graph_colors(vGraphColors color);
    vGraphColors                get_chart_colors() const                        { return graph_colors_id_; }
    virtual QSize               sizeHint() const                                { return QSize(vMB_base::maximumHeight(), vMB_base::maximumHeight()); }
    virtual QSize               minimumSizeHint() const                         { return QSize(vMB_base::minimumHeight(), vMB_base::minimumHeight()); }
    void                        set_caption(const QString& caption)             { caption_ = caption; }
    const QString&              get_caption()                                   { return caption_; }
    void                        set_value_first(const QString& caption, double value)  { value_first_.caption = caption; value_first_.value = value; update(); }
    ValueType                   get_value_first()                               { return boost::make_tuple(value_first_.caption, value_first_.value); }
    void                        set_value_second(const QString& caption, double value) { value_second_.caption = caption; value_second_.value = value; update();}
    ValueType                   get_value_second()                              { return boost::make_tuple(value_second_.caption, value_second_.value); }
    QString                     get_settings() const;
    vHierarchy                  get_class_name() const                          { return do_get_class_name(); }
    vHierarchyType              get_class_type() const                          { return do_get_class_type(); }
    vHierarchyStyle             get_class_style() const                         { return do_get_class_style(); }
    bool test();
};
//------------------------------------------------------------------------------
struct vMBBase::vData
{
    QRect rect;
    QFont font;
    vData() = default;
    vData(const QRect& Rect, const QFont & Font) : rect(Rect), font(Font){ }
};
//------------------------------------------------------------------------------
struct vMBBase::vArea
{
    vData ox;
    vData oy;
    vData graph;
    vData values;
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_BASE_H_ */
