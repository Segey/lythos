/* 
 * File:   MBBaseStatic.h
 * Author: S.Panin
 *
 * 02 Марта 2010 г., 22:29
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_BASE_STATIC_H
#define _V_MB_BASE_STATIC_H

#include "MBBase.h"

//------------------------------------------------------------------------------
struct vMBBaseStatic
{

static vMBBase*  make_MB(const QString& settings_input)
{
    vMB_base::type_settings::head_type            type  =0;
    vMB_base::type_settings::inherited::head_type color =0;
    QString settings = settings_input;

    QTextStream stream(&settings);
    stream >> type >> color;

    vMBBase* mb =NULL;

    switch (type)
    {
    default:
        case vMBBase::histogram_simple:         mb = new vMBHistogramSimple(0);      break;
        case vMBBase::histogram_percentage:     mb = new vMBHistogramPercentage(0);  break;
        case vMBBase::histogram_scale:          mb = new vMBHistogramScale(0);       break;

        case vMBBase::pie_simple:               mb = new vMBPieSimple(0);            break;
        case vMBBase::pie_percentage:           mb = new vMBPiePercentage(0);        break;
        case vMBBase::pie_scale:                mb = new vMBPieScale(0);             break;

        case vMBBase::text_simple:              mb = new vMBTextSimple(0);           break;
        case vMBBase::text_percentage:          mb = new vMBTextPercentage(0);       break;
        case vMBBase::text_scale:               mb = new vMBTextScale(0);            break;
    };

    mb->set_graph_colors(static_cast<vMBBase::vGraphColors>(color));
    return mb;
}

bool test()
{
    vMBBase* mb =make_MB("");
    Q_ASSERT(mb->get_settings() == "11 1");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::red);
    delete mb;

    mb =make_MB("0 0");
    Q_ASSERT(mb->get_settings() == "11 1");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::red);
    delete mb;

    mb =make_MB(" 999 2.2");
    Q_ASSERT(mb->get_settings() == "11 2");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::green);
    delete mb;

    mb =make_MB("10 6");
    Q_ASSERT(mb->get_settings() == "11 6");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::yellow);
    delete mb;

    mb =make_MB("  22 3 ");
    Q_ASSERT(mb->get_settings() == "22 3");
    Q_ASSERT(mb->get_class_name()   == vMBBase::pie_percentage);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_percentage);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_pie);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::blue);
    delete mb;

    mb =make_MB("23 4j");
    Q_ASSERT(mb->get_settings() == "23 4");
    Q_ASSERT(mb->get_class_name()   == vMBBase::text_percentage);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_percentage);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_text);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::cyan);
    delete mb;

    mb =make_MB("24 5");
    Q_ASSERT(mb->get_settings() == "11 5");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::magenta);
    delete mb;

    mb =make_MB("30 6");
    Q_ASSERT(mb->get_settings() == "11 6");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::yellow);
    delete mb;

    mb =make_MB("31 7");
    Q_ASSERT(mb->get_settings() == "31 7");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_scale);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_scale);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::gray);
    delete mb;

    mb =make_MB("32 8");
    Q_ASSERT(mb->get_settings() == "32 8");
    Q_ASSERT(mb->get_class_name()   == vMBBase::pie_scale);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_scale);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_pie);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::black);
    delete mb;

    mb =make_MB("33 9");
    Q_ASSERT(mb->get_settings() == "33 1");
    Q_ASSERT(mb->get_class_name()   == vMBBase::text_scale);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_scale);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_text);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::red);
    delete mb;

    mb =make_MB("34 6");
    Q_ASSERT(mb->get_settings() == "11 6");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::yellow);
    delete mb;

    mb =make_MB("40 6");
    Q_ASSERT(mb->get_settings() == "11 6");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::yellow);
    delete mb;

    mb =make_MB("11 7");
    Q_ASSERT(mb->get_settings() == "11 7");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::gray);
    delete mb;

    mb =make_MB("12 8");
    Q_ASSERT(mb->get_settings() == "12 8");
    Q_ASSERT(mb->get_class_name()   == vMBBase::pie_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_pie);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::black);
    delete mb;

    mb =make_MB("13 09");
    Q_ASSERT(mb->get_settings() == "13 1");
    Q_ASSERT(mb->get_class_name()   == vMBBase::text_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_text);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::red);
    delete mb;

    mb =make_MB("14 6");
    Q_ASSERT(mb->get_settings() == "11 6");
    Q_ASSERT(mb->get_class_name()   == vMBBase::histogram_simple);
    Q_ASSERT(mb->get_class_style()  == vMBBase::style_simple);
    Q_ASSERT(mb->get_class_type()   == vMBBase::type_histogram);
    Q_ASSERT(mb->get_chart_colors() == vMBBase::yellow);
    delete mb;

    return true;
}

};
//------------------------------------------------------------------------------
#endif	/* _V_MB_BASE_STATIC_H */
