/* 
 * File:   MBHistogram.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBHistogram.h"
//------------------------------------------------------------------------------
void vMBHistogram::do_draw_ox(QPainter* painter, const vData& data)
{
        painter->drawLine(data.rect.x(), data.rect.y(), data.rect.width() + data.rect.x(), data.rect.y());

        QRect left = data.rect;
        left.setWidth( left.width() >>1 );
        inherited::text_draw(painter, data.font, left , inherited::value_first().caption);

        QRect right = data.rect;
        right.setX(left.width() + right.x() );
        inherited::text_draw(painter, data.font, right, inherited::value_second().caption);
}
//------------------------------------------------------------------------------
bool vMBHistogram::test()
{
        Q_ASSERT (inherited::test());
        Q_ASSERT (value_scale_max(10) !=10);
        Q_ASSERT (value_scale_max(0) ==10);
        Q_ASSERT (value_scale_max(3) ==10);

        Q_ASSERT(vMBHistogram::get_real_column_height(10, 30, 30) == 10);
        Q_ASSERT(vMBHistogram::get_real_column_height(30, 10, 30) == 10);
        Q_ASSERT(vMBHistogram::get_real_column_height(30, 10, 60) == 20);
        Q_ASSERT(vMBHistogram::get_real_column_height(30, 0, 60) == 0);
        Q_ASSERT(vMBHistogram::get_real_column_height(0, 0, 60) == 0);



      //  SMI(vMBHistogram::get_real_column_height(0, -10, 60));
        return true;
}
//------------------------------------------------------------------------------