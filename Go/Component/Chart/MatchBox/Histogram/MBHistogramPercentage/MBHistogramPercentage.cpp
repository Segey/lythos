/* 
 * File:   MBHistogramPercentage.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBHistogramPercentage.h"
//------------------------------------------------------------------------------
vMBBase::vArea vMBHistogramPercentage::do_calculate_areas()
{
        vMBBase::vArea result;

        const QSize font_size = vMBBase::font_size(vMBBase::value_font(), "100%");
        text_size_ = QSize(font_size.width() + vMBBase::little()*2, font_size.height() + vMBBase::little()*2);
        result.ox = vMBBase::vData(QRect(text_size_.width(), vMBBase::max_height() - text_size_.height(), vMBBase::width() - text_size_.width() - vMBBase::large(), text_size_.height() ), vMBBase::value_font());

        count_percent_lines_ = inherited::count_percent_lines(vMBBase::height()  - text_size_.height(), text_size_.height());
        distance_ = (vMBBase::height() - text_size_.height()*1.5  ) / count_percent_lines_;
        int new_height =distance_ * count_percent_lines_;
        
        result.graph = vMBBase::vData(QRect(text_size_.width(), vMBBase::max_height() - text_size_.height() - new_height, vMBBase::width() - text_size_.width() - vMBBase::large(), new_height), vMBBase::value_font());
        return result;
}
//------------------------------------------------------------------------------
void vMBHistogramPercentage::do_draw_graph(QPainter* painter, const vData& data)
{
        percent_lines_draw(painter, data);
        graph_draw(painter, data);
}
//------------------------------------------------------------------------------
void vMBHistogramPercentage::graph_draw(QPainter* painter, const vData& data)
{
        const Columns columns = inherited::get_columns_height(vMBBase::value_first().value , vMBBase::value_second().value, data.rect.height());
        painter->setBrush(vMBBase::graph_gradient());

        if(vMBBase::value_first().value >0)     painter->drawRect(data.rect.x() + data.rect.width() / 8, data.rect.y() + data.rect.height() - columns.get<0>(), data.rect.width() / 4, columns.get<0>());
        if(vMBBase::value_second().value >0)    painter->drawRect(data.rect.width() / 2 + data.rect.x() + data.rect.width() / 8, data.rect.y() + data.rect.height() - columns.get<1>(), data.rect.width() / 4, columns.get<1>());
}
//------------------------------------------------------------------------------
void vMBHistogramPercentage::percent_lines_draw(QPainter* painter, const vData& data)
{
        for(int x = 0; x <= count_percent_lines_; ++x)
        {
                const int step=data.rect.y() + x*distance_;
                painter->drawLine(data.rect.x(), step , data.rect.width() + data.rect.x(), step);

                const QRect right(0,step - text_size_.height()/2, data.rect.x(), text_size_.height());
                vMBBase::text_draw(painter, data.font, right, QString("%1%").arg(100 - 100/count_percent_lines_*x));
        }
}
//------------------------------------------------------------------------------
bool vMBHistogramPercentage::test()
{
        Q_ASSERT(inherited::test());
        Q_ASSERT( do_get_class_name()   == vMB_base::histogram_percentage );
        Q_ASSERT( do_get_class_type()   == vMB_base::type_histogram );
        Q_ASSERT( do_get_class_style()  == vMB_base::style_percentage);
        return true;
}
//------------------------------------------------------------------------------