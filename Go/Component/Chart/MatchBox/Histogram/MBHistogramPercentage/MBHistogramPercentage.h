/* 
 * File:   MBHistogramPercentage.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_HISTOGRAM_PERCENTAGE_H
#define _V_MB_HISTOGRAM_PERCENTAGE_H
//------------------------------------------------------------------------------
class vMBHistogramPercentage : public vMBHistogram
{
    typedef                     vMBHistogram                                    inherited;
    typedef                     vMBBase::vData                                  Data;
    typedef                     vMBHistogramPercentage                             class_name;

    QSize                                                                       text_size_;
    int                                                                         count_percent_lines_;
    int                                                                         distance_;

    virtual vMBBase::vArea      do_calculate_areas();
    virtual void                do_draw_oy(QPainter*, const Data&)              {}
    virtual void                do_draw_graph(QPainter* painter, const Data& data);
    virtual void                do_draw_values(QPainter*, const Data&)          {}
    virtual vHierarchy          do_get_class_name() const                       { return vMB_base::histogram_percentage; }
    virtual vHierarchyType      do_get_class_type() const                       { return vMB_base::type_histogram; }
    virtual vHierarchyStyle     do_get_class_style() const                      { return vMB_base::style_percentage; }

    void                        percent_lines_draw(QPainter* painter, const vData& data);
    void                        graph_draw(QPainter* painter, const vData& data);

public:
    explicit                    vMBHistogramPercentage(QWidget* parent) : inherited(parent)   {}
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V__MB_HISTOGRAM_PERCENTAGE_H */
