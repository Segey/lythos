/* 
 * File:   vMBHistogramScale.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBHistogramScale.h"
//------------------------------------------------------------------------------
vMBBase::vArea vMBHistogramScale::do_calculate_areas()
{
        vMBBase::vArea result;

        value_scale_max_ = inherited::value_scale_max(std::max(vMBBase::value_first().value, vMBBase::value_second().value));
        const QSize font_size = vMBBase::font_size(vMBBase::value_font(), QString::number( value_scale_max_));

        text_size_ = QSize(font_size.width() + vMBBase::little()*2, font_size.height() + vMBBase::little()*2);
        result.ox = vMBBase::vData(QRect(text_size_.width(), vMBBase::max_height() - text_size_.height(), vMBBase::width() - text_size_.width() - vMBBase::large(), text_size_.height() ), vMBBase::value_font());
       
        count_percent_lines_ = inherited::count_percent_lines(vMBBase::height()  - text_size_.height(), text_size_.height());
        distance_ = (vMBBase::height() - text_size_.height()) / count_percent_lines_;
        const int new_height =distance_ * count_percent_lines_;

        result.oy = vMBBase::vData(QRect(0, vMBBase::caption_height() , text_size_.width(), vMBBase::max_height() - text_size_.height() ), vMBBase::value_font());
        result.graph = vMBBase::vData(QRect(text_size_.width(), vMBBase::max_height() - text_size_.height() - new_height, vMBBase::width() - text_size_.width() - vMBBase::large(), new_height), vMBBase::value_font());
        return result;
}
//------------------------------------------------------------------------------
void  vMBHistogramScale::do_draw_oy(QPainter* painter, const Data& data)
{
        painter->drawLine(data.rect.x() +data.rect.width(), data.rect.y(), data.rect.x()+data.rect.width(), data.rect.height());
}
//------------------------------------------------------------------------------
void vMBHistogramScale::do_draw_graph(QPainter* painter, const vData& data)
{
        painter->setBrush(vMBBase::graph_gradient());
        draw_percent_lines(painter, data);
        draw_graph(painter, data);
}
//------------------------------------------------------------------------------
void vMBHistogramScale::draw_graph(QPainter* painter, const vData& data)
{       
        const int left_height = inherited::get_real_column_height(vMBBase::value_first().value ,  value_scale_max_, data.rect.height());
        const int right_height = inherited::get_real_column_height(vMBBase::value_second().value, value_scale_max_, data.rect.height());

        draw_values(painter, data, left_height, right_height);
        draw_columns(painter, data, left_height, right_height);
}
//------------------------------------------------------------------------------
void vMBHistogramScale::draw_columns(QPainter* painter, const vData& data, int left_height, int right_height)
{
     if(vMBBase::value_first().value >0)   painter->drawRect(data.rect.x() + data.rect.width() / 8, data.rect.y() + data.rect.height() - left_height, data.rect.width() / 4, left_height);
     if(vMBBase::value_second().value >0)  painter->drawRect(data.rect.width() / 2 + data.rect.x() + data.rect.width() / 8, data.rect.y() + data.rect.height() - right_height, data.rect.width() / 4, right_height);
}
//------------------------------------------------------------------------------
void vMBHistogramScale::draw_values(QPainter* painter, const vData& data, int left_height, int right_height)
{
        const int text_height = vMBBase::font_height( vMBBase::value_font() )+ vMBBase::little()*2;

        QRect left(data.rect.x(), data.rect.y() + data.rect.height() - left_height , data.rect.width()/2, - text_height);
        if(vMBBase::value_first().value < 0) left.moveCenter(QPoint(left.center().x(), left.center().y() +left_height));
        vMBBase::text_draw(painter, data.font, left , QString::number(vMBBase::value_first().value));

        QRect right(data.rect.width()/2 + data.rect.x(), data.rect.y()+ data.rect.height() - right_height , data.rect.width()/2,  - text_height);
        if(vMBBase::value_second().value < 0) right.moveCenter(QPoint(right.center().x(), right.center().y() +right_height));
        vMBBase::text_draw(painter, data.font, right, QString::number(vMBBase::value_second().value));
}
//------------------------------------------------------------------------------
void vMBHistogramScale::draw_percent_lines(QPainter* painter, const vData& data)
{
        for(int x = 0; x <= count_percent_lines_; ++x)
        {
                const QRect right(0, data.rect.y() + data.rect.height() - x*distance_ - text_size_.height()/2, data.rect.x(), text_size_.height());
                vMBBase::text_draw(painter, data.font, right, QString("%1").arg(value_scale_max_/count_percent_lines_ *x));
        }
}
//------------------------------------------------------------------------------
bool vMBHistogramScale::test()
{
       Q_ASSERT (!count_percent_lines_);
       Q_ASSERT (!value_scale_max_);
       Q_ASSERT (!distance_);
       Q_ASSERT (!text_size_.width());
       Q_ASSERT (!text_size_.height());

       do_calculate_areas();

       Q_ASSERT (count_percent_lines_);
       Q_ASSERT (value_scale_max_);
       Q_ASSERT (distance_);
       Q_ASSERT (text_size_.width());
       Q_ASSERT (text_size_.height());
 
       Q_ASSERT( do_get_class_name()   == vMB_base::histogram_scale );
       Q_ASSERT( do_get_class_type()   == vMB_base::type_histogram );
       Q_ASSERT( do_get_class_style()  == vMB_base::style_scale);

        return true;
}
//------------------------------------------------------------------------------