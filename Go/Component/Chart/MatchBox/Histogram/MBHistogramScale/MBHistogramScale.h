/* 
 * File:   vMBHistogramScale.h
 * Author: S.Panin
 *
 * Created on 6 Ноября 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_HISTOGRAM_SCALE_H
#define _V_MB_HISTOGRAM_SCALE_H
//------------------------------------------------------------------------------
class vMBHistogramScale : public vMBHistogram
{
    typedef                     vMBHistogram                                    inherited;
    typedef                     vMBBase::vData                                  Data;
    typedef                     vMBHistogramScale                              class_name;

    QSize                                                                       text_size_;
    int                                                                         count_percent_lines_;
    int                                                                         value_scale_max_;
    double                                                                      distance_;

    virtual vMBBase::vArea      do_calculate_areas();
    virtual void                do_draw_oy(QPainter* painter, const Data& data);
    virtual void                do_draw_graph(QPainter* painter, const Data& data);
    virtual void                do_draw_values(QPainter*, const Data&)          {}
    virtual vHierarchy          do_get_class_name() const                       { return vMB_base::histogram_scale; }
    virtual vHierarchyType      do_get_class_type() const                       { return vMB_base::type_histogram; }
    virtual vHierarchyStyle     do_get_class_style() const                      { return vMB_base::style_scale; }

    void                        draw_columns(QPainter* painter, const vData& data, int left_height, int right_height);
    void                        draw_values(QPainter* painter, const vData& data, int left_height, int right_height);
    void                        draw_percent_lines(QPainter* painter, const vData& data);
    void                        draw_graph(QPainter* painter, const vData& data);

public:
    explicit                    vMBHistogramScale(QWidget* parent) : inherited(parent) , text_size_(0,0), count_percent_lines_(0), value_scale_max_(0), distance_(0) {}
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_HISTOGRAM_SCALE_H */
