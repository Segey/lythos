/* 
 * File:   ImageBlowEffect.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_SIMPLE_HISTOGRAM_MB_H
#define _V_SIMPLE_HISTOGRAM_MB_H
//------------------------------------------------------------------------------
#include "../MBHistogram.h"
//------------------------------------------------------------------------------
class vMBHistogramSimple : public vMBHistogram
{
    typedef                     vMBHistogram                                    inherited;
    typedef                     vMBBase::vData                                  Data;
    typedef                     vMBHistogramSimple                              class_name;

    virtual vMBBase::vArea      do_calculate_areas();
    virtual void                do_draw_oy(QPainter*, const Data&)              {}
    virtual void                do_draw_graph(QPainter* painter, const Data& data);
    virtual void                do_draw_values(QPainter*, const Data&)          {}
    virtual vHierarchy          do_get_class_name() const                       { return vMB_base::histogram_simple; }
    virtual vHierarchyType      do_get_class_type() const                       { return vMB_base::type_histogram; }
    virtual vHierarchyStyle     do_get_class_style() const                      { return vMB_base::style_simple; }

public:
    explicit                    vMBHistogramSimple(QWidget* parent) : inherited(parent)   {}
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_SIMPLE_HISTOGRAM_MB_H */
