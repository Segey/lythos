/* 
 * File:   ImageBlowEffect.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBHistogramSimple.h"
//------------------------------------------------------------------------------
vMBBase::vArea vMBHistogramSimple::do_calculate_areas()
{
        vMBBase::vArea result;

        const int ox_height = vMBBase::font_height( vMBBase::value_font() )+ vMBBase::little()*2;
        result.ox = vMBBase::vData(QRect(vMBBase::large(), vMBBase::max_height() - ox_height, vMBBase::width() - vMBBase::large()*2, ox_height), vMBBase::value_font());

        result.graph = vMBBase::vData(QRect(vMBBase::large(), vMBBase::caption_height(), vMBBase::width() - vMBBase::large()*2, vMBBase::height()  - ox_height), vMBBase::value_font());
        return result;
}
//------------------------------------------------------------------------------
void vMBHistogramSimple::do_draw_graph(QPainter* painter, const vData& data)
{
        painter->setBrush(vMBBase::graph_gradient());
        const int text_height = vMBBase::font_height( vMBBase::value_font() )+ vMBBase::little()*2;
        const Columns columns = inherited::get_columns_height(vMBBase::value_first().value , vMBBase::value_second().value, data.rect.height()  - text_height);
        const int h_left = vMBBase::value_first().value >0      ? data.rect.y() + data.rect.height() - columns.get<0>() : data.rect.y() + data.rect.height();
        const int h_right = vMBBase::value_second().value >0    ? data.rect.y() + data.rect.height() - columns.get<1>() : data.rect.y() + data.rect.height();

        if(vMBBase::value_first().value >0)     painter->drawRect(data.rect.x() + data.rect.width()/8, h_left, data.rect.width()/4, columns.get<0>() );
        if(vMBBase::value_second().value >0)    painter->drawRect(data.rect.width()/2 + data.rect.x()  + data.rect.width()/8, h_right, data.rect.width()/4, columns.get<1>() );

        const QRect left(data.rect.x(), h_left , data.rect.width()/2, - text_height);
        vMBBase::text_draw(painter, data.font, left , QString::number(vMBBase::value_first().value));

        QRect right(data.rect.width()/2 + data.rect.x(),h_right, data.rect.width()/2,  - text_height);
        vMBBase::text_draw(painter, data.font, right, QString::number(vMBBase::value_second().value));
}
//------------------------------------------------------------------------------
bool vMBHistogramSimple::test()
{
        Q_ASSERT (inherited::test());
        Q_ASSERT( do_get_class_name()   == vMB_base::histogram_simple );
        Q_ASSERT( do_get_class_type()   == vMB_base::type_histogram );
        Q_ASSERT( do_get_class_style()  == vMB_base::style_simple);
        return true;
}
//------------------------------------------------------------------------------