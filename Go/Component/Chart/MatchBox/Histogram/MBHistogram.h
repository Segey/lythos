/* 
 * File:   MBHistogram.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_HISTOGRAM_H_
#define _V_MB_HISTOGRAM_H_
//------------------------------------------------------------------------------
class vMBHistogram : public vMBBase
{
    typedef                     vMBBase                                         inherited;
    typedef                     vMBHistogram                                    class_name;
    typedef                     inherited::vData                                Data;

    virtual vMBBase::vArea      do_calculate_areas()                                    =0;
    virtual void                do_draw_ox(QPainter* painter, const Data& data);
    virtual void                do_draw_oy(QPainter* painter, const Data& data)         =0;
    virtual void                do_draw_graph(QPainter* painter, const Data& data)      =0;
    virtual void                do_draw_values(QPainter* painter, const Data& data)     =0;
    virtual vHierarchy          do_get_class_name() const                               =0;
    virtual vHierarchyType      do_get_class_type() const                               =0;
    virtual vHierarchyStyle     do_get_class_style() const                              =0;

protected:
    typedef                     boost::tuple<int,int>                               Columns;
    static int                  get_real_column_height(int lhs, int rhs, int height){ return std::min(lhs,rhs)*height/vValidator::get_not_zero(std::max(lhs,rhs)); }
    static Columns              get_columns_height(int lhs, int rhs, int height)    { return lhs > rhs ?  Columns(height, rhs *height /vValidator::get_not_zero(lhs) ) : Columns(lhs *height /vValidator::get_not_zero(rhs)  , height); }
    static int                  count_percent_lines(int height, int font_height)    { return vDLL<int>(const_libs::mb1810(), "count_percent_lines")(5, height, font_height); }
    static int                  value_scale_max(int value)                          { return vDLL<int>(const_libs::mb1810(), "max_value")(10, value); }

public:
    explicit                    vMBHistogram(QWidget* parent) : inherited(parent)   {}
    virtual                     ~vMBHistogram()                                     {}
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_HISTOGRAM_MB_H_ */
