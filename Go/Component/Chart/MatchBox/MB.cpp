/* 
 * File:   vMB.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MB.h"
#include "MBPuM.h"
//------------------------------------------------------------------------------
vMB::vMB(QWidget* parent, const QString& settings)
    : inherited(parent), mb_base_(vMBBaseStatic::make_MB(settings) )
{
        instance();
}
//------------------------------------------------------------------------------
void vMB::instance()
{
        inherited::setAutoRaise(true);
        inherited::setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(show_context_menu(const QPoint &)));
        layout_ = new QVBoxLayout(this);
        layout_->addWidget(mb_base_.get());
}
//------------------------------------------------------------------------------
void  vMB::create_chart(vMBBase* mb)
{
        mb->set_graph_colors(mb_base_->get_chart_colors());
        mb->set_value_first(mb_base_->get_value_first().get<0>(),       mb_base_->get_value_first().get<1>());
        mb->set_value_second(mb_base_->get_value_second().get<0>(),     mb_base_->get_value_second().get<1>());
        mb->set_caption( get_caption() );
        mb_base_.reset(mb);
        layout_->addWidget(mb_base_.get());
        emit( on_state_changed() );
}
//------------------------------------------------------------------------------
void vMB::show_context_menu(const QPoint &position)
{
        vMBPuM pum (this);
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
bool vMB::test()
{
        Q_ASSERT( inherited::autoRaise() );
        Q_ASSERT( inherited::contextMenuPolicy() == Qt::CustomContextMenu );
        Q_ASSERT( inherited::contextMenuPolicy() == Qt::CustomContextMenu );
        Q_ASSERT( mb_base_->test());
        Q_ASSERT( mb_base_->get_class_name()   == vMB_base::histogram_simple );
        Q_ASSERT( mb_base_->get_class_type()   == vMB_base::type_histogram );
        Q_ASSERT( mb_base_->get_class_style()  == vMB_base::style_simple);
        Q_ASSERT( mb_base_->get_chart_colors() == vMB_base::red);
        return true;
}
//------------------------------------------------------------------------------