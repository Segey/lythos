/* 
 * File:   MBPuM.h
 * Author: S.Panin
 *
 * Created on 23 Февраля 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_PUM_H
#define _V_MB_PUM_H
//------------------------------------------------------------------------------
class vMB;
//------------------------------------------------------------------------------
#include "../../../BuiltType/Action/Chart/ActionChart.h"
//------------------------------------------------------------------------------
class vMBPuM : public QMenu
{
Q_OBJECT

    typedef                     QMenu                                           inherited;
    typedef                     vMBPuM                                          class_name;
    typedef                     vMB                                             Parent;
    typedef                     boost::array<QAction*, 8>                       ActionsColor;
    enum                        vMenuType                                       { menu_color =1, menu_type =2, menu_style =3 };

    Parent*                                                                     parent_;

    void                        update_chart(size_t type, size_t color);
    ActionsColor                get_action_colors();
    template <typename T> void  instance_actions(const T& array, const QString& name,  size_t index, vMenuType type);    
    void                        instance_actions_type();
    void                        instance_actions_style();
    void                        instance_actions_color();
    void                        instance_action_default();
    void                        instance_chart(QMenu* menu, QActionGroup* group, QAction* action, vMenuType type);

private slots:
    void                        click_set_color();
    void                        click_set_style();
    void                        click_set_type();
    void                        click_set_default();

public:
    explicit                    vMBPuM(Parent* parent);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* vMBPuM */
