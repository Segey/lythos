/* 
 * File:   MBPiePercentage.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBPiePercentage.h"
//------------------------------------------------------------------------------
bool vMBPiePercentage::test()
{
        Q_ASSERT( inherited::test() );
        Q_ASSERT( do_get_class_name()   == vMB_base::pie_percentage );
        Q_ASSERT( do_get_class_type()   == vMB_base::type_pie );
        Q_ASSERT( do_get_class_style()  == vMB_base::style_percentage);
        Q_ASSERT( do_get_values().get<0>()   == vMBBase::values_percent_minmax_together().get<0>() );
        Q_ASSERT( do_get_values().get<1>()   == vMBBase::values_percent_minmax_together().get<1>() );
        return true;
}
//------------------------------------------------------------------------------