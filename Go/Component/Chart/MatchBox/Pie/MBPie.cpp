/* 
 * File:   MBPie.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBPie.h"
//------------------------------------------------------------------------------
vMBBase::vArea vMBPie::do_calculate_areas()
{
        vMBBase::vArea  result;
        result.graph    = vMBBase::vData(QRect(vMBBase::large(), vMBBase::caption_height() + vMBBase::large(), vMBBase::width() - vMBBase::large()*2, vMBBase::height() - vMBBase::large()*2), vMBBase::value_font());
        result.values   = result.graph;
        return          result;
}
//------------------------------------------------------------------------------
void vMBPie::do_draw_graph(QPainter* painter, const Data& data)
{
        const int Angle = get_angle();

        painter->setPen(get_pen());
        painter->setBrush(get_background_color());
        painter->drawPie(data.rect, Angle, 360.0 * 16);

        painter->setBrush(get_pouring_color());
        if(vMBBase::value_second().value >0) painter->drawPie(data.rect, 0.0, Angle);
}
//------------------------------------------------------------------------------
void vMBPie::do_draw_values(QPainter* painter, const Data& data)
{
        const int Angle = get_angle();
        vMBBase::ValuesString values = get_values();

        const QRectF point_min = get_point_value(data.rect, Angle / 16, values.get< 0 >() , false);
        if(vMBBase::value_first().value >0) vMBBase::text_draw(painter, vMBBase::value_font(), point_min,  values.get< 0 >() );

        const QRectF point_max = get_point_value(data.rect, Angle / 16, values.get< 1 >(), true);
        if(vMBBase::value_second().value >0) vMBBase::text_draw(painter, vMBBase::value_font(), point_max,  values.get< 1 >() );
}
//------------------------------------------------------------------------------
QLinearGradient vMBPie::get_pouring_color()
{
        QLinearGradient gradient(0, 0, 0, 100);
        gradient.setColorAt(0.1, Qt::white);
        gradient.setColorAt(0.8, Qt::transparent);
        return gradient;
}
//------------------------------------------------------------------------------
QRectF vMBPie::get_point_value(const QRect& graph_rect, int Angle, const QString& value_text, bool is_max)
{       
       const QSize font_size = vMBBase::font_size(vMBBase::value_font(), value_text);
       if (is_max) return vDLL<QRectF>(const_libs::mb1810(), "pie_point_value_max")(QRectF(), graph_rect, Angle, font_size);
             else  return vDLL<QRectF>(const_libs::mb1810(), "pie_point_value_min")(QRectF(), graph_rect, Angle, font_size);
}
//------------------------------------------------------------------------------
bool vMBPie::test()
{
        Q_ASSERT (inherited::test());

        vMBBase::value_first().value    =55;
        vMBBase::value_second().value   =89;
        Q_ASSERT(get_angle() == 2200 );

        vMBBase::value_first().value    =0;
        vMBBase::value_second().value   =0;
        Q_ASSERT(get_angle() == 0 );

        vMBBase::value_first().value    =100;
        vMBBase::value_second().value   =0;
        Q_ASSERT(get_angle() == 0 );

        vMBBase::value_first().value    =100;
        vMBBase::value_second().value   =50;
        Q_ASSERT(get_angle() == 1920 );

        vMBBase::value_first().value    =333;
        vMBBase::value_second().value   =77;
        Q_ASSERT(get_angle() == 1081 );

          //      SMI(get_angle());
          //      Q_ASSERT(get_angle() == 0 );

       // SMI(vValidator::get_not_zero( (vMBBase::value_first().value + vMBBase::value_second().value) * vMBBase::value_minmax().get<0>() * 16) );

        return true;
}
//------------------------------------------------------------------------------