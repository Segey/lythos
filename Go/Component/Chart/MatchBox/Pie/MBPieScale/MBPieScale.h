/* 
 * File:   vMBPieScale.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_PIE_SCALE_H
#define _V_MB_PIE_SCALE_H
//------------------------------------------------------------------------------
class vMBPieScale : public vMBPie
{
    typedef                         vMBPie                                      inherited;
    typedef                         vMBPieScale                                class_name;

protected:
    virtual void                    do_draw_graph(QPainter* painter, const Data& data)  { inherited::do_draw_graph(painter, data); }
    virtual void                    do_draw_values(QPainter* painter, const Data& data) { inherited::do_draw_values(painter, data); }
    virtual vMBBase::ValuesString   do_get_values()                                     { return vMBBase::values_minmax_string();}
    virtual vHierarchy              do_get_class_name() const                           { return vMB_base::pie_scale; }
    virtual vHierarchyType          do_get_class_type() const                           { return vMB_base::type_pie; }
    virtual vHierarchyStyle         do_get_class_style() const                          { return vMB_base::style_scale; }

public:
    explicit                        vMBPieScale(QWidget* parent) : inherited(parent)   {}
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_PIE_SCALE_H */
