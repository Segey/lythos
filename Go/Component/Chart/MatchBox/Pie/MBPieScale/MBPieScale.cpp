/* 
 * File:   vMBPieScale.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBPieScale.h"
//------------------------------------------------------------------------------
bool vMBPieScale::test()
{
    Q_ASSERT( inherited::test() );
    Q_ASSERT( do_get_class_name()   == vMB_base::pie_scale );
    Q_ASSERT( do_get_class_type()   == vMB_base::type_pie );
    Q_ASSERT( do_get_class_style()  == vMB_base::style_scale );
    return true;
}
//------------------------------------------------------------------------------