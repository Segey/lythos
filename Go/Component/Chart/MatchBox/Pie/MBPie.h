/* 
 * File:   MBPie.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_PIE_H
#define _V_MB_PIE_H
//------------------------------------------------------------------------------
class vMBPie : public vMBBase
{
    typedef                         vMBBase                                     inherited;
    typedef                         vMBPie                                      class_name;
    vMBBase::ValuesString           get_values()                                { return do_get_values(); }

protected:
    typedef                         inherited::vData                            Data;

    virtual vMBBase::vArea          do_calculate_areas();
    virtual void                    do_draw_ox(QPainter*, const Data&)          {}
    virtual void                    do_draw_oy(QPainter*, const Data&)          {}
    virtual void                    do_draw_graph(QPainter* painter, const Data& data)  =0;
    virtual void                    do_draw_values(QPainter* painter, const Data& data) =0;
    virtual vHierarchy              do_get_class_name() const                           =0;
    virtual vHierarchyType          do_get_class_type() const                           =0;
    virtual vHierarchyStyle         do_get_class_style() const                          =0;
    virtual vMBBase::ValuesString   do_get_values()                                     =0;

    int                             get_angle()                                 { return  360.0 * vMBBase::value_minmax().get<0>() * 16/ vValidator::get_not_zero( (vMBBase::value_first().value + vMBBase::value_second().value) ); }
    QLinearGradient                 get_background_color()                      { return vMBBase::graph_gradient(); }
    static QLinearGradient          get_pouring_color();
    static QColor                   get_pen()                                   { return  Qt::transparent; }
    QRectF                          get_point_value(const QRect& rect, int Angle, const QString& value_text, bool is_max);

public:
    explicit                        vMBPie(QWidget* parent) : inherited(parent) {}
    virtual                         ~vMBPie()                                   { }
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_TEXT_H */
