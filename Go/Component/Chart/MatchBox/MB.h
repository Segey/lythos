/* 
 * File:   ImageBlowEffect.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_H
#define _V_MB_H
//------------------------------------------------------------------------------
#include "Base/MBBase.h"
#include "MBPuM.h"
//------------------------------------------------------------------------------
struct vMB : public QToolButton
{
Q_OBJECT
    friend class vMBPuM;

    typedef                     QToolButton                                     inherited;
    typedef                     vMB                                             class_name;

    boost::shared_ptr<vMBBase>                                                  mb_base_;
    QVBoxLayout*                                                                layout_;

    void                        instance();
    void                        create_chart(vMBBase* mb);

signals:
    void                        on_state_changed();

private slots:
    void                        show_context_menu(const QPoint& position);

public:
    QSize                       sizeHint() const                                { return mb_base_->sizeHint(); }
    QSize                       minimumSizeHint() const                         { return mb_base_->minimumSizeHint(); }
    explicit                    vMB(QWidget* parent, const QString& settings);
    void                        set_graph_colors(vMBBase::vGraphColors color)   { mb_base_->set_graph_colors(color); emit( on_state_changed() );}
    vMBBase::vGraphColors       get_graph_colors() const                        { return mb_base_->get_chart_colors(); }
    void                        set_caption(const QString& caption)             { mb_base_->set_caption(caption); }
    const QString&              get_caption() const                             { return mb_base_->get_caption(); }
    void                        set_value_first(const QString& caption, double value)  { mb_base_->set_value_first(caption, value);}
    void                        set_value_second(const QString& caption, double value) { mb_base_->set_value_second(caption, value); }
    QString                     get_settings() const                            { return mb_base_->get_settings(); }
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_H */
