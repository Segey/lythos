/* 
 * File:   vMB.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBPuM.h"
#include "MB.h"
//------------------------------------------------------------------------------
vMBPuM::vMBPuM(Parent* parent)
          : inherited(parent), parent_(parent)
{
        instance_actions_type();
        instance_actions_style();
        instance_actions_color();
        addSeparator();
        instance_action_default();
}
//------------------------------------------------------------------------------
void  vMBPuM::instance_chart(QMenu* menu, QActionGroup* group, QAction* action, vMBPuM::vMenuType type)
{
        action->setCheckable(true);
        menu->addAction(action);
        group->addAction(action);
        if (type == menu_color) connect(action, SIGNAL(triggered(bool)), SLOT(click_set_color()));
        else if (type == menu_style) connect(action, SIGNAL(triggered(bool)), SLOT(click_set_style()));
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_set_type()));
}
//------------------------------------------------------------------------------
vMBPuM::ActionsColor vMBPuM::get_action_colors()
{
     vMBPuM::ActionsColor array
        { {
            vAction::createColorRed(this),      vAction::createColorGreen(this),        vAction::createColorBlue(this),
            vAction::createColorCyan(this),     vAction::createColorMagenta(this),      vAction::createColorYellow(this),
            vAction::createColorGray(this),     vAction::createColorBlack(this)
        } };
        
        array[0]->setData( vMBBase::red  );     array[1]->setData( vMBBase::green );
        array[2]->setData( vMBBase::blue );     array[3]->setData( vMBBase::cyan );
        array[4]->setData( vMBBase::magenta );  array[5]->setData( vMBBase::yellow );
        array[6]->setData( vMBBase::gray );     array[7]->setData( vMBBase::black );

        return array;
}
//------------------------------------------------------------------------------
template <typename T>
void vMBPuM::instance_actions(const T& array, const QString& name, size_t index, vMBPuM::vMenuType type)
{
        QMenu* menu = inherited::addMenu(name);
        QActionGroup* group = new QActionGroup(this);

        std::for_each(array.begin(), array.end(), boost::bind(&vMBPuM::instance_chart, this, menu, group, _1, type));

        if (index < array.size() ) array[index]->setChecked(true);
}
//------------------------------------------------------------------------------
void vMBPuM::instance_actions_type()
{
        boost::array<QAction*, 3> array = {{ vActionChart::createTypeHistogram(this), vActionChart::createTypePie(this), vActionChart::createTypeText(this) }};
        array[0]->setData( vMBBase::type_histogram );
        array[1]->setData( vMBBase::type_pie ); 
        array[2]->setData( vMBBase::type_text );
        instance_actions(array, vtr::Chart::tr("Chart &Type"),  parent_->mb_base_->get_class_type() -1, vMBPuM::menu_type);
}
//------------------------------------------------------------------------------
void vMBPuM::instance_actions_style()
{
        boost::array<QAction*, 3> array = {{ vActionChart::createStyleSimple(this), vActionChart::createStylePercentage(this), vActionChart::createStyleScale(this) }};
        array[0]->setData( vMBBase::style_simple );
        array[1]->setData( vMBBase::style_percentage ); 
        array[2]->setData( vMBBase::style_scale );
        instance_actions(array, vtr::Chart::tr("Chart &Style"),  parent_->mb_base_->get_class_style()/10 -1, vMBPuM::menu_style);
}
//------------------------------------------------------------------------------
void vMBPuM::instance_actions_color()
{
        QMenu* menu = inherited::addMenu(vtr::Chart::tr("Chart &Color"));
        QActionGroup* group = new QActionGroup(this);

        vMBPuM::ActionsColor array = get_action_colors();
        std::for_each(array.begin(), array.end(), boost::bind(&vMBPuM::instance_chart, this, menu, group, _1, menu_color));

        if( parent_->mb_base_->get_class_type() == vMBBase::type_text)
        {
                std::for_each(array.begin(), array.end(), boost::bind(&QAction::setEnabled, _1, false));
                size_t index = vMBBase::black -1;
                if ( index < array.size() ) array[index]->setChecked(true);
                return;
        }

        size_t index = parent_->mb_base_->get_chart_colors() -1;
        if ( index < array.size() ) array.at(index)->setChecked(true);
}
//------------------------------------------------------------------------------
void vMBPuM::instance_action_default()
{
        QAction* action= vAction::createDefault(this);
        inherited::addAction(action);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_set_default()));
}
//------------------------------------------------------------------------------
/*signal*/ void  vMBPuM::click_set_color()
{
        parent_->mb_base_->set_graph_colors( static_cast<vMBBase::vGraphColors>(((QAction*)sender())->data().toInt()) );
}
//------------------------------------------------------------------------------
/*signal*/ void  vMBPuM::click_set_style()
{
        update_chart( parent_->mb_base_->get_class_type() + ((QAction*)sender())->data().toInt(), parent_->mb_base_->get_chart_colors() );
}
//------------------------------------------------------------------------------
/*signal*/ void  vMBPuM::click_set_type()
{
        update_chart( parent_->mb_base_->get_class_style() + ((QAction*)sender())->data().toInt(), parent_->mb_base_->get_chart_colors() );
}
           //------------------------------------------------------------------------------
/*signal*/ void  vMBPuM::click_set_default()
{
        update_chart( 0, 0 );
}
//------------------------------------------------------------------------------
void  vMBPuM::update_chart(size_t type, size_t color)
{
        QString settings;
        QTextStream stream(&settings);
        stream << type <<" "<< color;

        parent_->create_chart(vMBBaseStatic::make_MB(settings));
        parent_->set_graph_colors(static_cast<vMBBase::vGraphColors>(color));
}
//------------------------------------------------------------------------------
bool vMBPuM::test()
{
        Q_ASSERT(parent_);
        QList<QAction*> list =inherited::actions();
        Q_ASSERT(list.size() == 5);
        Q_ASSERT(list[0]->menu()->actions().size() == 3);
        Q_ASSERT(list[1]->menu()->actions().size() == 3);
        Q_ASSERT(list[2]->menu()->actions().size() == 8);
        Q_ASSERT(list[3]->menu() ==0 );
        Q_ASSERT(list[3]->isSeparator());
        Q_ASSERT(list[4]->menu() ==0 );
        Q_ASSERT(list[4]->text() ==vtr::Button::tr("Default") );

        Q_ASSERT(list[0]->menu()->actions()[0]->data().toInt()  == vMBBase::type_histogram );
        Q_ASSERT(list[0]->menu()->actions()[1]->data().toInt()  == vMBBase::type_pie );
        Q_ASSERT(list[0]->menu()->actions()[2]->data().toInt()  == vMBBase::type_text );
        Q_ASSERT(list[0]->menu()->actions()[0]->text()          == vtr::Chart::tr("&Histogram") );
        Q_ASSERT(list[0]->menu()->actions()[1]->text()          == vtr::Chart::tr("&Pie") );
        Q_ASSERT(list[0]->menu()->actions()[2]->text()          == vtr::Chart::tr("&Text") );

        Q_ASSERT(list[1]->menu()->actions()[0]->data().toInt()  == vMBBase::style_simple );
        Q_ASSERT(list[1]->menu()->actions()[1]->data().toInt()  == vMBBase::style_percentage );
        Q_ASSERT(list[1]->menu()->actions()[2]->data().toInt()  == vMBBase::style_scale );
        Q_ASSERT(list[1]->menu()->actions()[0]->text()          == vtr::Chart::tr("&Simple") );
        Q_ASSERT(list[1]->menu()->actions()[1]->text()          == vtr::Chart::tr("&Percentage") );
        Q_ASSERT(list[1]->menu()->actions()[2]->text()          == vtr::Chart::tr("S&cale") );

        Q_ASSERT(list[2]->menu()->actions()[0]->data().toInt()  == vMBBase::red  );
        Q_ASSERT(list[2]->menu()->actions()[1]->data().toInt()  == vMBBase::green );
        Q_ASSERT(list[2]->menu()->actions()[2]->data().toInt()  == vMBBase::blue );
        Q_ASSERT(list[2]->menu()->actions()[3]->data().toInt()  == vMBBase::cyan );
        Q_ASSERT(list[2]->menu()->actions()[4]->data().toInt()  == vMBBase::magenta );
        Q_ASSERT(list[2]->menu()->actions()[5]->data().toInt()  == vMBBase::yellow );
        Q_ASSERT(list[2]->menu()->actions()[6]->data().toInt()  == vMBBase::gray );
        Q_ASSERT(list[2]->menu()->actions()[7]->data().toInt()  == vMBBase::black );       
        Q_ASSERT(list[2]->menu()->actions()[0]->text()          == vtr::Color::tr("&Red")  );
        Q_ASSERT(list[2]->menu()->actions()[1]->text()          == vtr::Color::tr("&Green") );
        Q_ASSERT(list[2]->menu()->actions()[2]->text()          == vtr::Color::tr("&Blue") );
        Q_ASSERT(list[2]->menu()->actions()[3]->text()          == vtr::Color::tr("&Cyan") );
        Q_ASSERT(list[2]->menu()->actions()[4]->text()          == vtr::Color::tr("&Magenta") );
        Q_ASSERT(list[2]->menu()->actions()[5]->text()          == vtr::Color::tr("&Yellow") );
        Q_ASSERT(list[2]->menu()->actions()[6]->text()          == vtr::Color::tr("Gr&ay") );
        Q_ASSERT(list[2]->menu()->actions()[7]->text()          == vtr::Color::tr("B&lack") );

        parent_->set_caption("Привет");
        parent_->mb_base_->set_value_first("привет", 20.0);
        parent_->mb_base_->set_value_second("hello", 25.0);
        Q_ASSERT(parent_->mb_base_->get_value_first().get<0>()  == "привет" );
        Q_ASSERT(parent_->mb_base_->get_value_second().get<0>() == "hello" );
        Q_ASSERT(parent_->mb_base_->get_value_first().get<1>()  == 20.0);
        Q_ASSERT(parent_->mb_base_->get_value_second().get<1>() == 25.0);

        update_chart(vMBBase::type_histogram + vMBBase::style_simple, vMBBase::red);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::red);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::histogram_simple);

        update_chart(vMBBase::type_histogram + vMBBase::style_scale, vMBBase::black);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::black);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::histogram_scale);

        update_chart(vMBBase::type_histogram + vMBBase::style_percentage, vMBBase::yellow);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::yellow);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::histogram_percentage);

        Q_ASSERT(parent_->mb_base_->get_value_first().get<0>()  == "привет" );
        Q_ASSERT(parent_->mb_base_->get_value_second().get<0>() == "hello" );
        Q_ASSERT(parent_->mb_base_->get_value_first().get<1>()  == 20.0);
        Q_ASSERT(parent_->mb_base_->get_value_second().get<1>() == 25.0);

        parent_->mb_base_->set_value_first("", 33);
        parent_->mb_base_->set_value_second("hello77", 66);

        update_chart(vMBBase::type_pie + vMBBase::style_simple, vMBBase::gray);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::gray);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::pie_simple);

        update_chart(vMBBase::type_pie + vMBBase::style_scale, vMBBase::black);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::black);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::pie_scale);

        update_chart(vMBBase::type_pie + vMBBase::style_percentage, vMBBase::magenta);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::magenta);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::pie_percentage);

        Q_ASSERT(parent_->mb_base_->get_value_first().get<0>()  == "" );
        Q_ASSERT(parent_->mb_base_->get_value_second().get<0>() == "hello77" );
        Q_ASSERT(parent_->mb_base_->get_value_first().get<1>()  == 33);
        Q_ASSERT(parent_->mb_base_->get_value_second().get<1>() == 66);

        Q_ASSERT(parent_->get_caption() =="Привет");

        parent_->mb_base_->set_value_first("uuu", 0);
        parent_->mb_base_->set_value_second("7", 999);

        update_chart(vMBBase::type_text + vMBBase::style_simple, vMBBase::gray);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::gray);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::text_simple);

        update_chart(vMBBase::type_text + vMBBase::style_scale, vMBBase::black);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::black);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::text_scale);

        update_chart(vMBBase::type_text + vMBBase::style_percentage, vMBBase::magenta);
        Q_ASSERT(parent_->get_graph_colors()            == vMBBase::magenta);
        Q_ASSERT(parent_->mb_base_->get_class_name()    == vMBBase::text_percentage);

        Q_ASSERT(parent_->mb_base_->get_value_first().get<0>()  == "uuu" );
        Q_ASSERT(parent_->mb_base_->get_value_second().get<0>() == "7" );
        Q_ASSERT(parent_->mb_base_->get_value_first().get<1>()  == 0);
        Q_ASSERT(parent_->mb_base_->get_value_second().get<1>() == 999);

        return true;
}