/* 
 * File:   MBText.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_TEXT_H
#define _V_MB_TEXT_H
//------------------------------------------------------------------------------
class vMBText : public vMBBase
{
    typedef                         vMBBase                                     inherited;
    typedef                         inherited::vData                            Data;
    typedef                         vMBText                                     class_name;
    typedef                         boost::tuple<QFont, QFont>                  Fonts;


    Fonts                           get_fonts();
    QFont                           first_font(const QFont& font)               { return do_get_first_font(font); }
    QFont                           second_font(const QFont& font)              { return do_get_second_font(font); }
    QSize                           get_font_size()                             { return do_get_font_size(); }
    vMBBase::ValuesString           get_values()                                { return do_get_values(); }

protected:
    virtual vMBBase::vArea          do_calculate_areas();
    virtual void                    do_draw_ox(QPainter*, const Data&)          {}
    virtual void                    do_draw_oy(QPainter*, const Data&)          {}
    virtual void                    do_draw_graph(QPainter* painter, const Data& data);
    virtual void                    do_draw_values(QPainter*, const Data&)      {}

    virtual QFont                   do_get_first_font(const QFont& font)        =0;
    virtual QFont                   do_get_second_font(const QFont& font)       =0;
    virtual QSize                   do_get_font_size()                          =0;
    virtual vMBBase::ValuesString   do_get_values()                             =0;
    static QString                  value_text_with_hyphen(const QString& value) {return QString(" - %1").arg(value); }

public:
    explicit                        vMBText(QWidget* parent) : inherited(parent) {}
    virtual                         ~vMBText()                                  {}
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_TEXT_H */
