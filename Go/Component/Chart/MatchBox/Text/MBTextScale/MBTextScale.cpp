/* 
 * File:   MBTextScale.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBTextScale.h"
//------------------------------------------------------------------------------
QFont vMBTextScale::do_get_second_font(const QFont& font)
{
        QFont result=font;
        result.setUnderline(true);
        return result;
}
//------------------------------------------------------------------------------
bool vMBTextScale::test()
{
    Q_ASSERT (inherited::test());
    Q_ASSERT( do_get_class_name()   == vMB_base::text_scale );
    Q_ASSERT( do_get_class_type()   == vMB_base::type_text );
    Q_ASSERT( do_get_class_style()  == vMB_base::style_scale );
    return true;
}
//------------------------------------------------------------------------------