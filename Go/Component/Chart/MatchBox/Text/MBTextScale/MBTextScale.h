/* 
 * File:   MBTextScale.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_TEXT_SCALE_H
#define _V_MB_TEXT_SCALE_H
//------------------------------------------------------------------------------
class vMBTextScale : public vMBText
{
    typedef                         vMBText                                     inherited;
    typedef                         vMBTextScale                               class_name;

protected:
    virtual QFont                   do_get_first_font(const QFont& font)        {return font;}
    virtual QFont                   do_get_second_font(const QFont& font);
    virtual QSize                   do_get_font_size()                          { return inherited::do_get_font_size(); }
    virtual vMBBase::ValuesString   do_get_values()                             { return inherited::do_get_values(); }
    virtual vHierarchy              do_get_class_name() const                   { return vMB_base::text_scale; }
    virtual vHierarchyType          do_get_class_type() const                   { return vMB_base::type_text; }
    virtual vHierarchyStyle         do_get_class_style() const                  { return vMB_base::style_scale; }
public:
    explicit                        vMBTextScale(QWidget* parent) : inherited(parent)   {}
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_TEXT_SCALE_H */
