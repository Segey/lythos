/* 
 * File:   ImageBlowEffect.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBTextSimple.h"
//------------------------------------------------------------------------------
bool vMBTextSimple::test()
{
    Q_ASSERT (inherited::test());
    Q_ASSERT( do_get_class_name()   == vMB_base::text_simple );
    Q_ASSERT( do_get_class_type()   == vMB_base::type_text );
    Q_ASSERT( do_get_class_style()  == vMB_base::style_simple );
    return true;
}
//------------------------------------------------------------------------------