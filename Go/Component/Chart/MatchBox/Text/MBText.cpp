/* 
 * File:   MBText.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBText.h"
//------------------------------------------------------------------------------
vMBBase::vArea vMBText::do_calculate_areas()
{
        vMBBase::vArea result;
        result.graph = vMBBase::vData(QRect(vMBBase::large(), vMBBase::caption_height(), vMBBase::width() - vMBBase::large()*2, vMBBase::height()  - vMBBase::caption_height() - vMBBase::large()), vMBBase::value_font());
        return result;
}
//------------------------------------------------------------------------------
void vMBText::do_draw_graph(QPainter* painter, const Data& data)
{
        const QSize font_size   = get_font_size();
        const int emptiness     = data.rect.height() / 3;
        const int text_width    = data.rect.width() - font_size.width();

        const Fonts fonts =get_fonts();
        const vMBBase::ValuesString values = get_values();

        const QRect first_text_rect(vMBBase::large(), data.rect.y() +emptiness, text_width , font_size.height());
        vMBBase::text_draw(painter, fonts.get<0>(), first_text_rect ,vMBBase::value_first().caption ,Qt::AlignLeft);

        const QRect second_text_rect(vMBBase::large(), data.rect.y() +emptiness*2 + font_size.height(), text_width , font_size.height());
        vMBBase::text_draw(painter, fonts.get<1>(), second_text_rect, vMBBase::value_second().caption ,Qt::AlignLeft);

        const QRect first_value_rect(vMBBase::large()+text_width, data.rect.y() +emptiness, font_size.width() , font_size.height());
        vMBBase::text_draw(painter, fonts.get<0>(), first_value_rect ,value_text_with_hyphen(values.get<0>()) , Qt::AlignLeft);

        const QRect second_value_rect(vMBBase::large()+text_width, data.rect.y() +emptiness*2 + font_size.height(), font_size.width() , font_size.height());
        vMBBase::text_draw(painter, fonts.get<1>(), second_value_rect, value_text_with_hyphen(values.get<1>()) , Qt::AlignLeft);
}
//------------------------------------------------------------------------------
vMBText::Fonts  vMBText::get_fonts()
{
        if (vMBBase::value_first().value > vMBBase::value_second().value) return Fonts( second_font(vMBBase::value_font()) ,first_font(vMBBase::value_font()) );
             return Fonts(first_font(vMBBase::value_font()), second_font(vMBBase::value_font()));
}
//------------------------------------------------------------------------------
QSize vMBText::do_get_font_size()
{
        const int value_max = vMBBase::value_minmax().get < 1 >();
        return vMBBase::font_size(vMBBase::value_font(), value_text_with_hyphen(QString::number(value_max)));
}
//------------------------------------------------------------------------------
vMBBase::ValuesString vMBText::do_get_values()
{
        return vMBBase::ValuesString(QString::number(vMBBase::value_first().value), QString::number(vMBBase::value_second().value));
}
//------------------------------------------------------------------------------
bool vMBText::test()
{
        Q_ASSERT (inherited::test());
        const QString Text ="Hello";
        Q_ASSERT (vMBText::value_text_with_hyphen(Text) ==" - Hello");
        return true;
}
//------------------------------------------------------------------------------