/* 
 * File:   MBTextPercentage.h
 * Author: S.Panin
 *
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#ifndef _V_MB_TEXT_PERCENTAGE_H
#define _V_MB_TEXT_PERCENTAGE_H
//------------------------------------------------------------------------------
class vMBTextPercentage : public vMBText
{
    typedef                         vMBText                                     inherited;
    typedef                         vMBTextPercentage                              class_name;

protected:
    virtual QFont                   do_get_first_font(const QFont& font)        {return font;}
    virtual QFont                   do_get_second_font(const QFont& font);
    virtual QSize                   do_get_font_size();
    virtual vMBBase::ValuesString   do_get_values()                             { return vMBBase::values_percent_unsort(); }
    virtual vHierarchy              do_get_class_name() const                   { return vMB_base::text_percentage; }
    virtual vHierarchyType          do_get_class_type() const                   { return vMB_base::type_text; }
    virtual vHierarchyStyle         do_get_class_style() const                  { return vMB_base::style_percentage; }
public:
    explicit                        vMBTextPercentage(QWidget* parent) : inherited(parent)   {}
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MB_TEXT_PERCENTAGE_H */
