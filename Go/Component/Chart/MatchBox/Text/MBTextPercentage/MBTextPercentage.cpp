/* 
 * File:   vMBTextPercentage.cpp
 * Author: S.Panin
 * 
 * Created on 3 Август 2009 г., 0:35
 */
//------------------------------------------------------------------------------
#include "MBTextPercentage.h"
//------------------------------------------------------------------------------
QFont vMBTextPercentage::do_get_second_font(const QFont& font)
{
        QFont result=font;
        result.setBold(true);
        return result;
}
//------------------------------------------------------------------------------
QSize vMBTextPercentage::do_get_font_size()
{
        const QString Text= vMBBase::values_percent_minmax().get < 1 > ();
        return vMBBase::font_size(do_get_second_font(vMBBase::value_font()), value_text_with_hyphen(Text));
}
//------------------------------------------------------------------------------
bool vMBTextPercentage::test()
{
    Q_ASSERT (inherited::test());
    Q_ASSERT( do_get_class_name()   == vMB_base::text_percentage );
    Q_ASSERT( do_get_class_type()   == vMB_base::type_text );
    Q_ASSERT( do_get_class_style()  == vMB_base::style_percentage);
    return true;
}
//------------------------------------------------------------------------------