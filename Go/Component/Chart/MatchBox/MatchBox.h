// -*- C++ -*-
/* 
 * File:   incChart.h
 * Author: S.Panin
 *
 * Created on 30 Апрель 2010 г., 0:40
 */
//------------------------------------------------------------------------------
#ifndef _V_MATCH_BOX_H_
#define	_V_MATCH_BOX_H_
//------------------------------------------------------------------------------
#include "../../../go/noncopyable.h"
#include "../../../Strategy/DLL/Dll.h"
#include "../../../Algorithm/Validator.h"
#include "../../../Component/Chart/MatchBox/Base/MBBase.h"
#include "../../../Component/Chart/MatchBox/Histogram/MBHistogram.h"
#include "../../../Component/Chart/MatchBox/Histogram/MBHistogramPercentage/MBHistogramPercentage.h"
#include "../../../Component/Chart/MatchBox/Histogram/MBHistogramScale/MBHistogramScale.h"
#include "../../../Component/Chart/MatchBox/Histogram/MBHistogramSimple/MBHistogramSimple.h"
#include "../../../Component/Chart/MatchBox/Pie/MBPie.h"
#include "../../../Component/Chart/MatchBox/Pie/MBPiePercentage/MBPiePercentage.h"
#include "../../../Component/Chart/MatchBox/Pie/MBPieScale/MBPieScale.h"
#include "../../../Component/Chart/MatchBox/Pie/MBPieSimple/MBPieSimple.h"
#include "../../../Component/Chart/MatchBox/Text/MBText.h"
#include "../../../Component/Chart/MatchBox/Text/MBTextPercentage/MBTextPercentage.h"
#include "../../../Component/Chart/MatchBox/Text/MBTextScale/MBTextScale.h"
#include "../../../Component/Chart/MatchBox/Text/MBTextSimple/MBTextSimple.h"
#include "../../../Component/Chart/MatchBox/Base/MBBaseStatic.h"
#include "../../../Component/Chart/MatchBox/MB.h"
//------------------------------------------------------------------------------
#endif	/* _V_MATCH_BOX_H_ */

