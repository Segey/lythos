/* 
 * File:   Dock.h
 * Author: S.Panin
 *
 * Created on Вт апр. 27 2010, 11:07:02
 */
//------------------------------------------------------------------------------
#ifndef _V_DOCK_H_
#define	_V_DOCK_H_
//------------------------------------------------------------------------------
template<typename T>
class vDock : public QDockWidget
{
    typedef                         QDockWidget                                 inherited;
    typedef                         vDock                                       class_name;
    typedef                         QDockWidget                                 dock_type;
    typedef                         T                                           value_type;
    typedef                         value_type*                                 pointer;

    QWidget*                                                                    parent_;
    pointer                                                                     widget_;

public:
    explicit                        vDock(QWidget* parent)
            : inherited(parent), parent_(parent), widget_(0)
    {
        widget_ = new value_type(parent_);
        inherited::setWidget(widget_);
    }
    pointer                         widget() const                              { return widget_; }
    bool                            test()
    {
        Q_ASSERT(parent_);
        Q_ASSERT(widget_);
        return true;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_DOCK_H_ */
