/* 
 * File:   DockRichEdit.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#ifndef _V_DOCK_RICH_EDIT_H_
#define	_V_DOCK_RICH_EDIT_H_
//------------------------------------------------------------------------------
#include "../../MainWindow/RichEdit/Successors/MainWindowRichEditSimple.h"
//------------------------------------------------------------------------------
class vDockRichEdit : public QWidget
{
Q_OBJECT

    typedef                             QWidget                                 inherited;
    typedef                             vDockRichEdit                           class_name;

    QTextEdit*                                                                  text_edit_;
    QLabel*                                                                     text_label_;
    QPushButton*                                                                button_;
    QFrame*                                                                     bevel_;
    boost::shared_ptr<vMainWindowRichEditSimple>                                rich_edit_;

    void                                instance();
    void                                instance_bevel_layout();
    void                                instance_base_layout();
    void                                create_bevel();
    void                                create_button();
    void                                InstanceSignals();

signals:
    void                                textChanged();

private slots:
    void                                dotextChanged();
    void                                showRichEdit();

public:
    explicit                            vDockRichEdit(QWidget* parent = 0);
    bool                                test();
    void                                load(const QString& text);
    QString                             get_text()                              {return text_edit_->toHtml();}
};
//------------------------------------------------------------------------------
#endif	/* _V_DOCK_RICHEDIT_H_ */
