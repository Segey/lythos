/* 
 * File:   DockRichEdit.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#include "DockRichEdit.h"
//------------------------------------------------------------------------------
vDockRichEdit::vDockRichEdit(QWidget* parent /*=0*/)
                : inherited(parent),  text_edit_(0), text_label_(0), button_(0), bevel_(0)
{
        instance();
        InstanceSignals();
}
//------------------------------------------------------------------------------
void vDockRichEdit::instance()
{
        create_bevel();
        text_label_ = new QLabel;
        text_edit_ = new QTextEdit;
        create_button();

        instance_bevel_layout();
        instance_base_layout();
}
//------------------------------------------------------------------------------
void vDockRichEdit::create_bevel()
{
        bevel_ = new QFrame;
        bevel_->setFrameShape(QFrame::StyledPanel);
}
//------------------------------------------------------------------------------
void vDockRichEdit::create_button()
{
        button_ = new QPushButton;
        button_->setFlat(true);
        button_->setIcon(const_icon_16::show_window());
        button_->setToolTip(vtr::Dialog::tr("Edit in new window..."));
}
//------------------------------------------------------------------------------
void vDockRichEdit::instance_bevel_layout()
{
        QHBoxLayout* bevel_layout = new QHBoxLayout(bevel_);
        bevel_layout->setMargin(1);
        bevel_layout->addWidget(text_label_);
        bevel_layout->addWidget(button_, 0, Qt::AlignAbsolute | Qt::AlignRight);
}
//------------------------------------------------------------------------------
void vDockRichEdit::instance_base_layout()
{
        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->setMargin(0);
        layout->setSpacing(1);
        layout->addWidget(bevel_);
        layout->addWidget(text_edit_);
}
//------------------------------------------------------------------------------
void vDockRichEdit::InstanceSignals()
{
        connect(text_edit_, SIGNAL(textChanged()), SLOT(dotextChanged()));
        connect(button_, SIGNAL(clicked()), SLOT(showRichEdit()));
}
//------------------------------------------------------------------------------
void vDockRichEdit::dotextChanged()
{
        emit(textChanged());
}
//------------------------------------------------------------------------------
void vDockRichEdit::showRichEdit()
{
        rich_edit_.reset(new vMainWindowRichEditSimple(text_edit_, this));
        rich_edit_->load(text_edit_->toHtml());
        OVERRIDE_CURSOR(rich_edit_->show());
}
//------------------------------------------------------------------------------
void vDockRichEdit::load(const QString& text)
{
        text_edit_->document()->setHtml(text);
}
//------------------------------------------------------------------------------
bool vDockRichEdit::test()
{
        Q_ASSERT(text_edit_);
        Q_ASSERT(text_label_);
        Q_ASSERT(button_);
        Q_ASSERT(bevel_);
        Q_ASSERT(!rich_edit_.unique());
        Q_ASSERT(!rich_edit_.get());
        Q_ASSERT(!button_->toolTip().isEmpty());
        Q_ASSERT(!button_->icon().isNull());
        Q_ASSERT(button_->isFlat());
        Q_ASSERT(bevel_->frameShape() ==QFrame::StyledPanel);
        return true;
}