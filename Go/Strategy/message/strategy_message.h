/**
 \file  strategy_message.h
 \brief Файл стратегий сообщений

 \author S.Panin
 \date Created on 20 December 2010 г., 02:28
 */
//------------------------------------------------------------------------------
#ifndef _STRATEGY_MESSAGE_H_
#define	_STRATEGY_MESSAGE_H_
//------------------------------------------------------------------------------
#include <iostream>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

    /** \namespace go::strategy  */
    namespace strategy {
        /** \namespace go::strategy::message  */
        namespace message {
//------------------------------------------------------------------------------
/**
\brief Базовая стратегия.
\note Использование данной стратегии возможно лишь, только в наследовании.
 */
class base
{
    typedef                     base                                            class_name;

public:
    enum message_type { none =1, critical =2, info =3 };
    
protected:
/**
\brief Защищенный конструктор.
*/
    base() {}
};



/**
\brief Ничего не делающая стратегия.
\note Использование данной стратегии возможно лишь, только в случаи необходимости не выводить сообщения.
 */
struct none : public base
{

    typedef                     none                                            class_name;
/**
\brief Статическая функция показа сообщения.
\param message - Текст сообщения.
*/
    static void                 show(const QString&, base::message_type)                    { }

};

/** \brief Стратегия для вывода сообщений на консоль. */
struct console : public base
{

    typedef                     console                                         class_name;
/**
\brief Статическая функция показа сообщения.
\param message - Текст сообщения.
*/
    static void                 show(const QString& message, base::message_type type)
    {
        QString str;
        if (type == base::critical) str = " - Critical error: ";
        std::cout << str.toStdString() << message.toStdString() << std::endl;
    }
};

/** \brief Стратегия для вывода сообщений в окне. */
struct window : public base
{

    typedef                     window                                          class_name;
/**
\brief Статическая функция показа сообщения.
\param message - Текст сообщения.
*/
    static void                 show(const QString& message, base::message_type type)
    {
        if (type == base::critical) QMessageBox::critical(0, program::name_full(), message ,QMessageBox::Ok);
    }
};
//------------------------------------------------------------------------------
        }; // end namespace go::strategy::message
    }; // end namespace go::strategy
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _STRATEGY_MESSAGE_H_ */
