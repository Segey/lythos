// -*- C++ -*-
/* 
 * File:   StrategyDll.h
 * Author: S.Panin
 *
 * Created on 11 Январь 2010 г., 14:37
 */
//------------------------------------------------------------------------------
#ifndef _V_DLL_H_
#define	_V_DLL_H_
//------------------------------------------------------------------------------
#include "vStrategyDll.h"
//------------------------------------------------------------------------------
template <typename R, typename T =vdll::NoMessage, template<typename> class U = vdll::Default >
class vDLL
{
    typedef                             R                                       result_type;
    typedef                             T                                       strategy_message;
    typedef                             U<R>                                    strategy_result;
    
    QString                                                                     name_library_;
    QString                                                                     name_function_;

    result_type  execute_strategy(const  QLibrary& lib, const result_type& result)  { strategy_message()(lib); return strategy_result()(result);}
    
public:
    explicit vDLL(const QString & name_library, const QString & name_function) : name_library_(name_library), name_function_(name_function) {}
    void     set_name_function(const QString& name_function)                    {name_function_ = name_function;}
    result_type operator()(const result_type& result)
    {
        typedef result_type (*LIB)( );
        
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        return func ?  func() : execute_strategy(lib, result);
    }

template <typename T1> result_type operator()(const result_type& result, const T1& value)
    {
    typedef result_type (*LIB)(T1);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        return func ?  func(value) : execute_strategy(lib, result);
    }

template <typename T1, typename T2 > result_type operator()(const result_type& result, const T1& value_1, const T2& value_2)
    {
        typedef result_type (*LIB)(T1, T2);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        return func ?  func(value_1, value_2) : execute_strategy(lib, result);
    }

template <typename T1, typename T2, typename T3 > result_type operator()(const result_type& result, const T1& value_1, const T2& value_2, const T3& value_3)
    {
        typedef result_type (*LIB)(T1,T2, T3);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        return func ?  func(value_1, value_2, value_3) : execute_strategy(lib, result);
    }
};
//------------------------------------------------------------------------------
template <typename T, template<typename> class U>
class vDLL<void,T,U>
{

    typedef                             T                                       strategy_message;
    typedef                             U<void>                                 strategy_result;

    QString                                                                     name_library_;
    QString                                                                     name_function_;

    void  execute_strategy(const  QLibrary& lib)                                { strategy_message()(lib);}

public:
    explicit vDLL(const QString & name_library, const QString & name_function) : name_library_(name_library), name_function_(name_function) {}
    void     set_name_function(const QString& name_function)                    {name_function_ = name_function;}
    void operator()()
    {
        typedef void (*LIB)( );

        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        func ?  func() : execute_strategy(lib);
    }

template <typename T1> void operator()( const T1& value)
    {
    typedef void (*LIB)(T1);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        func ?  func(value) : execute_strategy(lib);
    }

template <typename T1, typename T2 > void operator()(const T1& value_1, const T2& value_2)
    {
        typedef void (*LIB)(T1, T2);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        func ?  func(value_1, value_2) : execute_strategy(lib);
    }

template <typename T1, typename T2, typename T3 > void operator()( const T1& value_1, const T2& value_2, const T3& value_3)
    {
        typedef void (*LIB)(T1, T2, T3);
        QLibrary lib(name_library_);
        LIB func = (LIB)lib.resolve(qPrintable(name_function_));
        func ?  func(value_1, value_2, value_3) : execute_strategy(lib);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_DLL_H_ */

