// -*- C++ -*-
/* 
 * File:   vStrategyDll.h
 * Author: S.Panin
 *
 * Created on 11 Январь 2010 г., 14:45
 */
//------------------------------------------------------------------------------
#ifndef _V_STRATEGY_DLL_H_
#define	_V_STRATEGY_DLL_H_
//------------------------------------------------------------------------------
#include "../../../Go/const.h"
#include "../../../Go/tr.h"
//------------------------------------------------------------------------------
namespace vdll
{       // START namespace vdll
//------------------------------------------------------------------------------
template<typename R>
class Default
{
    typedef                             R                                       result_type;
    
public:
    result_type operator()(const result_type& result)                           {  return result; }
};
//------------------------------------------------------------------------------
template<>
class Default<void>
{
    typedef                             void                                       result_type;

public:
    void operator()()                                                           {}
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
struct NoMessage
{
    void operator()(const  QLibrary&)                                           {  }
};
//------------------------------------------------------------------------------
struct Message
{
    void operator()(const  QLibrary& lib)                                       {  QMessageBox::critical(0, program::name_full(), lib.errorString() + vtr::Message::tr("%1Try to reinstall to fix this problem.").arg('\n'), QMessageBox::Ok); }
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
}       // END namespace vdll
//------------------------------------------------------------------------------
#endif	/* _V_STRATEGY_DLL_H */

