/**
    \file   path.h
    \brief  Работа с путями

    \author S.Panin
    \date   Created on 8 January 2011 г., 14:26
 */
//------------------------------------------------------------------------------
#ifndef _PATH_H_
#define	_PATH_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
/** \brief path  */
struct path {
//------------------------------------------------------------------------------
    static QString      absolute ( )                        { return QDir().absolutePath(); }
    static QString      absolute ( const QString& dir)      { return QDir(dir).absolutePath(); }
    static std::string  absolute ( const std::string& dir)  { return QDir(dir.c_str()).absolutePath().toStdString(); }
    static std::string  absolute ( const char* dir)         { return QDir(dir).absolutePath().toStdString(); }
};
//------------------------------------------------------------------------------
/** \brief path  */
struct file {
//------------------------------------------------------------------------------
    static QString      absolute ( const QString& file)     { return QDir().absoluteFilePath(file); }
    static std::string  absolute ( const std::string& file) { return QDir().absoluteFilePath(file.c_str()).toStdString(); }
    static std::string  absolute ( const char* file)        { return QDir().absoluteFilePath(file).toStdString(); }
};
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _PATH_H_ */