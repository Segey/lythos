/* 
 * File:   Image.h
 * Author: S.Panin
 *
 * Created on 31 Июль 2009 г., 14:07
 */
//------------------------------------------------------------------------------
#ifndef _V_IMAGE_H_
#define	_V_IMAGE_H_
//------------------------------------------------------------------------------
namespace v_image
{
inline QByteArray to_byte_array(const QString& filename)
    {
        QPixmap px(filename);
        QByteArray batmp;
        QBuffer bftmp(&batmp);
        bftmp.open(QIODevice::WriteOnly);
        px.save(&bftmp, QFileInfo(filename).completeSuffix().toAscii());
        return batmp;
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_IMAGE_H_ */
