/**
    \file   ConsoleEGCreater.h
    \brief  Файл проверки выражений, чисел и др.

    \author S. Panin
    \date   Created on 12 Июль 2009 г., 22:07
 */
//------------------------------------------------------------------------------
#ifndef _V_VALIDATOR_H_
#define	_V_VALIDATOR_H_
//------------------------------------------------------------------------------
#include "../Algorithm/Interfase.h"
#include <boost/compressed_pair.hpp>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
    /**
     \struct validator
     \brief Структура проверки и перевода значений из типа QString в любой другой.
     \note Доработать для соответствия с локалями пользователей.
     */
    struct validator
    {
        typedef                     boost::compressed_pair<bool,int>            type_int;
        typedef                     boost::compressed_pair<bool,double>         type_double;

        /** \brief Функция проверки и перевода значений из типа QString в тип int */
        static type_int             is_int(const QString& s)
        {
            bool ok = false;
            int dec = s.toInt(&ok, 10);
            return type_int(ok,dec);
        }

        /** \brief Функция проверки и перевода значений из типа QString в тип double */
        static type_double          is_double(const QString& s)
        {
            bool ok = false;
            double dec = s.toDouble(&ok);
            return type_double(ok,dec);
        }
    };
//------------------------------------------------------------------------------
}; // end namespace go

struct vValidator
{

static bool         check_int(QString text, int start, int end)
    {
        if ( text.isEmpty() ) return false;
        int value = 0;
        boost::shared_ptr<QIntValidator> validator(new QIntValidator(start, end, 0));
        return validator->validate(text, value) != QValidator::Invalid;
    }
//------------------------------------------------------------------------------
static bool         check_double(QString text, double start, double end, int decimals =0)
    {
        if ( text.isEmpty() ) return false;
        int value = 0;
        boost::shared_ptr<QDoubleValidator> validator(new QDoubleValidator(start, end, decimals, 0));
        return validator->validate(text, value) != QValidator::Invalid;
    }
//------------------------------------------------------------------------------
template <typename T =int>
static inline T get_not_zero(const T& number)
{
    return number ? number : 1;
}
//------------------------------------------------------------------------------
template <typename T, typename F> static bool   is_number_with_migalo(T* , F, const bool );
    //------------------------------------------------------------------------------
template <typename F>  static bool              is_number_with_migalo(QLineEdit* widget,  F func, const bool empty)
    {
       return func(widget->text(), empty) ? true : vInterfase::migalo(widget);
    }
    //------------------------------------------------------------------------------
template <typename T, typename F>  static bool  is_number_with_migalo(T begin,  T end, F func, const bool empty)
    {
    return std::count_if(begin, end, boost::bind(&is_number_with_migalo<F>, _1, func, empty)) ==std::distance(begin, end);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_VALIDATOR_H_ */

