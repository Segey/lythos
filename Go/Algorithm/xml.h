/**
    \file   xml.h
    \brief  Файл для работы с XML

    \author S.Panin
    \date   Created on 7 Января 2011 г., 01:42
 */
//------------------------------------------------------------------------------
#ifndef _XML_H_
#define	_XML_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/** \brief Алгоритмы для работы с xml  */
struct xml {
//------------------------------------------------------------------------------
static QString  element ( const QString& file_name, const QString& element_name,  const QString& default_name = QString() )
{
        QDomDocument domDoc;
        QFile file(file_name);
        if( file.open(QIODevice::ReadOnly) && domDoc.setContent(&file) )
        {
                const QDomElement node = domDoc.documentElement();
                QDomNode domNode = node.firstChild();

                while( !domNode.isNull() && domNode.isElement() )
                {
                        const QDomElement next = domNode.toElement();
                        if(!next.isNull() && next.tagName() == element_name ) return next.text();
                        domNode = domNode.nextSibling();
                }
        }
        return default_name;
}
//------------------------------------------------------------------------------
static QString  attribute( const QString& file_name, const QString& element_name, const QString& attribute_name,  const QString& default_name = QString() )
{
        QDomDocument domDoc;
        QFile file(file_name);
        if( file.open(QIODevice::ReadOnly) && domDoc.setContent(&file) )
        {
                const QDomElement node = domDoc.documentElement();
                QDomNode domNode = node.firstChild();

                while( !domNode.isNull() && domNode.isElement() )
                {
                        const QDomElement next = domNode.toElement();
                        if(!next.isNull() && next.tagName() == element_name )
                                        return next.attribute(attribute_name, default_name);
                        domNode = domNode.nextSibling();
                }
        }
        return default_name;
}
};
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _XML_H_ */