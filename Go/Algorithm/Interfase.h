/* 
 * File:   Interfase.h
 * Author: Panin S.
 *
 * Created on 12 Июль 2009 г., 22:07
 */
//------------------------------------------------------------------------------
#ifndef _INTERFASE_H_
#define	_INTERFASE_H_
//------------------------------------------------------------------------------
class vInterfase
{

    template <typename T, class qPalette> static void           chage_palette(T * sender, const qPalette& palette)
    {
        QMutex mutex;
        mutex.lock();

        QWaitCondition pause;

        sender->setPalette(palette);
        pause.wait(&mutex, 80);
        QApplication::processEvents();
    }

public:
//------------------------------------------------------------------------------
    template <typename T> static bool           migalo(T* sender)
    {
        if(!sender) return false;
        QPalette new_pal = sender->palette();
        QPalette old_pal = sender->palette();

        new_pal.setColor(QPalette::Active, QPalette::Base, Qt::red);
        for(int i = 0; i < 2; ++i)
        {
            chage_palette(sender, new_pal);
            chage_palette(sender, old_pal);
        }
        return false;
    }

//------------------------------------------------------------------------------
    template <typename T> static void           vMigalo(T* sender)
    {
        migalo(sender);
    }
    
//------------------------------------------------------------------------------
    template <class qTextEdit, class qString> static void    print(qTextEdit* text_edit, const qString& window_title)
    {
        QApplication::setOverrideCursor(Qt::WaitCursor);

        QPrinter printer(QPrinter::HighResolution);
        boost::shared_ptr<QPrintDialog> dialog(new QPrintDialog(&printer));
        if(text_edit->textCursor().hasSelection()) dialog->addEnabledOption(QAbstractPrintDialog::PrintSelection);
        dialog->setWindowTitle(window_title);

        QApplication::restoreOverrideCursor();
        if(dialog->exec() == QDialog::Accepted) text_edit->print(&printer);
    }
};
//------------------------------------------------------------------------------
#endif	/* _INTERFASE_H_ */

