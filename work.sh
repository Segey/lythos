#! /bin/sh
FILE=./Stat/`date +%m_%Y`.stat
awk -f ./Stat/get_stat.awk `find . -name "*.cpp" -o -name "*.h"` | tee -a $FILE
cat $FILE | ./Stat/month.awk
exit 0
