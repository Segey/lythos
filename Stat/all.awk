#! /usr/bin/awk -f
 BEGIN {
	min=40;
	max=0;
	min_bool =0;
	max_bool =0;
	min_array[10];
	max_array[10];
	DATE_BEGUN="24.06.2009";
	}
       /Текущая дата/ { 
		      if ($6 <min ) {  min =$6; min_bool=1; } else min_bool =0;
		      if ($6 >max ) {  max =$6; max_bool=1; } else max_bool =0;
		      if (!month) month=$5;
		      }
      /Текущий проект/ {
		      if ( 1 == min_bool ) min_array[0] =$4;
		      if ( 1 == max_bool ) max_array[0] =$4;
		      }	  
      /Общее кол-во файлов \*\.h/ {
				  if ( 1 == min_bool ) min_array[1] =$NF;
				  if ( 1 == max_bool ) max_array[1] =$NF;
				  }
      /Общее кол-во файлов \*\.cpp/ {
				  if ( 1 == min_bool ) min_array[2] =$NF;
				  if ( 1 == max_bool ) max_array[2] =$NF;
				  }		  
      /Общее кол-во тестируемых классов в проекте/ {
				  if ( 1 == min_bool ) min_array[3] =$NF;
				  if ( 1 == max_bool ) max_array[3] =$NF;
				  }	
      /Общее кол-во классов в проекте/ {
				  if ( 1 == min_bool ) min_array[4] =$NF;
				  if ( 1 == max_bool ) max_array[4] =$NF;
				  }
      /Общее кол-во структур в проекте/ {
				  if ( 1 == min_bool ) min_array[5] =$NF;
				  if ( 1 == max_bool ) max_array[5] =$NF;
				  }
      /Общее кол-во пространств имён в проекте/ {
				  if ( 1 == min_bool ) min_array[6] =$NF;
				  if ( 1 == max_bool ) max_array[6] =$NF;
				  }
      /Общее кол-во строк в проекте/ {
				  if ( 1 == min_bool ) min_array[7] =$NF;
				  if ( 1 == max_bool ) max_array[7] =$NF;
				  }
      /Общее кол-во символов в проекте/ {
				  if ( 1 == min_bool ) min_array[8] =$NF;
				  if ( 1 == max_bool ) max_array[8] =$NF;
				  }	      

 END {
      print "------------------------------------------------------------------------------------------------";      
      print "  Статистика за период с " DATE_BEGUN " по " max " " month " 2010 года";
      print "  Выполнено проектов\t\t\t\t= " max_array[0];
      print "  Добавлено файлов *.h\t\t\t\t= " max_array[1];    
      print "  Добавлено файлов *.cpp\t\t\t= " max_array[2];
      print "  Всего добавлено файлов в проект\t\t= " max_array[2] + max_array[1];
      print "  Добавлено тестируемых классов\t\t\t= " max_array[3];
      print "  Добавлено классов в проект\t\t\t= " max_array[4];
      print "  Добавлено структур в проект\t\t\t= " max_array[5];
      print "  Добавлено пространств имён в проект\t\t= " max_array[6];
      print "  Всего добавлено строк в проект\t\t= " max_array[7];     
      print "  Всего добавлено символов в проект\t\t= " max_array[8];                      
     }
#exit 0