#! /usr/bin/awk -f
# Статистика по проекту
BEGIN {
      "date +%d_%m_%Y.stat" | getline FILE;
      "date" | getline DATE;
      "cat version.txt |  awk -F= '{print $NF}' | awk '{print $1}'"  | getline PROJECT;
      "find . -name \"*.h\" | grep -nc '.'"  | getline FILES_H;
      "find . -name \"*.cpp\" | grep -nc '.'"  | getline FILES_CPP;

print "------------------------------------------------------------------------------------------------";
print "  Текущая дата\t\t\t\t\t\t\t", " = " DATE ;
print "  Текущий проект\t\t\t\t\t\t", " = " PROJECT ;
print "  Общее кол-во файлов *.h\t\t\t\t\t", " = " FILES_H ;
print "  Общее кол-во файлов *.cpp\t\t\t\t\t", " = " FILES_CPP ;
print "  Общее кол-во файлов проекта\t\t\t\t\t", " = " FILES_CPP + FILES_H;
      }
      /CLASS_TES[ASTICREF_]+\(/ { ++COUNT_TEST; }
     { if ($1 =="namespace" && $0 !~/;/)  array_namespace[$2]=$2; }   
     { if ($1 =="class" && $0 !~/;/)  array_class[$2]=$2; }   
     { if ($1 =="struct" && $0 !~/;/)  array_struct[$2]=$2; }
     { COUNT_CHARS +=length($0); }        

END {
     for (h in array_class)  ++COUNT_CLASS;     
     for (h in array_struct)  ++COUNT_STRUCT;   
     for (h in array_namespace)  ++COUNT_NAME;   
     print "  Общее кол-во тестируемых классов в проекте \t\t\t", " = " COUNT_TEST;   
     print "  Общее кол-во классов в проекте \t\t\t\t", " = " COUNT_CLASS;
     print "  Общее кол-во структур в проекте \t\t\t\t", " = " COUNT_STRUCT;
     print "  Общее кол-во пространств имён в проекте \t\t\t", " = " COUNT_NAME;
     print "  Общее кол-во строк в проекте \t\t\t\t\t", " = " NR;
     print "  Общее кол-во символов в проекте \t\t\t\t", " = " COUNT_CHARS;      
    }
#exit 0