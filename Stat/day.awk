#! /usr/bin/awk -f
 BEGIN {
	min=40;
	max=0;
	min_bool =0;
	max_bool =0;
	min_array[2];
	max_array[2];
	}
       /Текущая дата/ { 
		      if ($6 <min ) {  min =$6; min_bool=1; } else min_bool =0;
		      if ($6 >=max ) {  max =$6; max_bool=1; } else max_bool =0;
		      if (!month) month=$5;
		      }
      /Общее кол-во строк в проекте/ {
				  if ( 1 == min_bool ) min_array[0] =$NF;
				  if ( 1 == max_bool ) max_array[0] =$NF;
				  }
      /Общее кол-во символов в проекте/ {
				  if ( 1 == min_bool ) min_array[1] =$NF;
				  if ( 1 == max_bool ) max_array[1] =$NF;
				  }	      

 END {
      print "------------------------------------------------------------------";      
      print "  Статистика за " max " " month " 2010 года";
      print "  Всего / Добавлено строк\t\t= " max_array[0] " (" max_array[0] - min_array[0] ") ";     
      print "  Всего / Добавлено символов\t\t= " max_array[1] " (" max_array[1] - min_array[1] ") ";                       
     }
#exit 0