/*
 * File:   main.cpp
 * Author: dix75
 *
 * Created on 9 Январь 2011 г., 21:59
 */
//------------------------------------------------------------------------------
#include <QtGui>
#include "classes/Main.h"
#include "classes/EGApp.h"
#include "../Go/Strategy/message/strategy_message.h"
#include "../Go/BD/new_bd/DBSelect.h"
#include "../Go/BD/new_bd/Types/DBTypes.h"
#include "../Go/const.h"
//------------------------------------------------------------------------------
using namespace go;
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
        QApplication app(argc, argv);
        go::EGApp eg_app;
        go::Main Main;
        Main.load("");
        Main.show();

//        go::db::OneString one;
//        go::EGApp::settings_select_type& select = eg_app.settings().select();
//        select("SELECT program FROM program_info", one);
//        std::cout << one.value().toStdString();
        
    //    go::db::FormState state(&Main);
      //  go::EGApp::settings_select_type& select = eg_app.settings().select();
      //  select("SELECT state FROM forms WHERE user_id =1 AND name=main_eg",state);
       // std::cout << state.value().toStdString();



                            //    std::cout << saveGeometry().toBase64().data()<<std::endl;
      //  std::cout << Main.saveState().toBase64().data()<<std::endl;
        return app.exec();
}
