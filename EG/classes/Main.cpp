/**
    \file   Main.cpp
    \brief  Файл Main.cpp

    \author S.Panin
    \date   Created on 9 Январь 2011 г., 22:53
 */
//------------------------------------------------------------------------------
#include "../../Go/Instance/MainWindowInstance.h"
#include "Main.h"
#include "classes/EGApp.h"
#include "../Go/BD/new_bd/DBSelect.h"
#include "../Go/BD/new_bd/Types/DBTypes.h"
#include "../Go/BuiltType/Form.h"
//------------------------------------------------------------------------------
using namespace go;
//------------------------------------------------------------------------------
Main::Main()
{
        instance::MainWindow< main::Instance > (this).instance();
        
        instance_signals();
        settings_read();
        inherited::setWindowModified(false);
}
//------------------------------------------------------------------------------
void Main::settings_read()
{
        go::EGApp::settings_select_type& select = go::EGApp::settings().select();

        go::db::FormState state(this);
        select(QString("SELECT state FROM forms WHERE user_id =%1 AND name=%2").arg(EGApp::uid()).arg(form_name()),state);

        go::db::FormGeometry geometry(this);
        select(QString("SELECT geometry FROM forms WHERE user_id =%1 AND name=%2").arg(EGApp::uid()).arg(form_name()),geometry);
        if (false ==geometry.is_done())  setGeometry(built_type::Form::ideal_size(QDesktopWidget().size()*0.75));
}
//------------------------------------------------------------------------------
void Main::settings_write() const
{

        //        vSettings::set(settings_table(), settings::make(settings_key_state(), inherited::saveState()), settings::make_form_size(settings_key_form(), inherited::geometry()));
}
//------------------------------------------------------------------------------
void Main::load(const QString& text)
{
        //        file_section_->load(text);
}
//------------------------------------------------------------------------------
void Main::instance_signals()
{
        //        connect(text_edit_,     SIGNAL(textChanged()), SLOT(textChanged()));
        //        connect(text_edit_,     SIGNAL(cursorPositionChanged()), SLOT(cursorPositionChanged()));
        //        connect(text_edit_,     SIGNAL(currentCharFormatChanged(const QTextCharFormat&)), SLOT(currentCharFormatChanged(const QTextCharFormat&)));
        //        connect(file_section_,  SIGNAL(save(const QString&)), SLOT(save_execute(const QString&)));
}
//------------------------------------------------------------------------------
/*virtual*/ void Main::closeEvent(QCloseEvent *event)
{
        settings_write();
        if(!isWindowModified()) return;
        //
        //        QMessageBox::StandardButton  click_button =QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Save changes to the document before quitting?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        //        if (click_button == QMessageBox::Cancel) event->ignore();
        //        if (click_button == QMessageBox::Save) { QApplication::processEvents(); file_section_->Save();}
}
//------------------------------------------------------------------------------
bool Main::test() const
{
        //        Q_ASSERT(!file_menu_    ->isEmpty());
        //        Q_ASSERT(!edit_menu_    ->isEmpty());
        //        Q_ASSERT(!format_menu_  ->isEmpty());
        //        Q_ASSERT(file_menu_     ->actions().count() == 9);
        //        Q_ASSERT(edit_menu_     ->actions().count() == 7);
        //        Q_ASSERT(format_menu_   ->actions().count() == 10);
        //        Q_ASSERT(file_section_  ->test());
        //        Q_ASSERT(print_section_ ->test());
        //        Q_ASSERT(exit_section_  ->test());
        //        Q_ASSERT(undo_section_  ->test());
        //        Q_ASSERT(edit_section_  ->test());
        //        Q_ASSERT(style_section_ ->test());
        //        Q_ASSERT(align_section_ ->test());
        //        Q_ASSERT(font_section_  ->test());
        //        Q_ASSERT(bullet_section_->test());
        //        Q_ASSERT(help_menu_     ->test());
        //
        //        vMain::user().set_test_id();
        //        QByteArray array(inherited::saveState());
        //        QRect rect =  vDialog::form_size_calculation(this);
        //        Q_ASSERT( vSettings::set(settings_table(), settings::make(settings_key_state(), array), settings::make_form_size(settings_key_form(), rect)) );
        //        settings_write();
        //        settings_read();
        //        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<0>() );
        //        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<1>() == array );
        //        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_state(), QByteArray()), settings::make_form_size(settings_key_form(), QRect())).get<2>() == rect );
        //        setWindowModified(false);
        //        QSqlQuery query(vMain::settingsbd().database());
        //        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------
