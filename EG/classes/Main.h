/**
    \file   Main.h
    \brief  Файл Main.h

    \author S.Panin
    \date   Created on 9 Январь 2011 г., 22:53
 */
//------------------------------------------------------------------------------
#ifndef _MAIN_H_
#define	_MAIN_H_
//------------------------------------------------------------------------------
#include <QtGui>
#include "MainInstance.h"
#include "../../Go/Component/Menu/Help/MenuHelp.h"
#include "../../Go/Component/Section/Exit/SectionExit.h"
#include "../../Go/Component/Section/Print/SectionPrint.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
//------------------------------------------------------------------------------
class Main : public QMainWindow
{
    Q_OBJECT

friend class go::main::Instance;

    typedef                         QMainWindow                                 inherited;
    typedef                         Main                                        class_name;

    inherited*                                                                  parent_;
    QTextEdit*                                                                  text_edit_;
    QTreeWidget*                                                                tree_widget_;
    QMenu*                                                                      menu_file_;
    menu::Help*                                                                 menu_help_;
    section::Exit*                                                              section_exit_;
    section::Print*                                                             section_print_;

    static QString                  form_name()                                 { return "main_eg";  }
 //   static QString                  settings_key_state()                        { return ("FormState"); }
 //   static QString                  settings_key_form()                         { return ("FormSize"); }
    void                            settings_read();
    void                            settings_write() const;
    void                            instance_signals();

protected:
    virtual void closeEvent(QCloseEvent *event);

public:
    explicit                        Main();
    void                            load(const QString& text);
    bool                            test() const;
};
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_H_ */
