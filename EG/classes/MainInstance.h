/**
    \file   MainInstance.h
    \brief  Файл MainInstance.h

    \author S.Panin
    \date   Created on 9 Январь 2011 г., 23:04
 */
//------------------------------------------------------------------------------
#ifndef _MAIN_INSTANCE_H_
#define	_MAIN_INSTANCE_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

class Main;

    /** \namespace go::main  */
    namespace main {
//------------------------------------------------------------------------------
/**
    \brief Основной файл настроек(инстанцирования) базовой формы модуля 'EG'.
 
    Класс настроек в обязательном порядке содержит указатель на инстанцируемый объект  (parent_).
    \warning Инстанцируемый объект 'EG' в обязательном порядке должен быть наследован от QMainWindow.
    \note Обязательные функции-член.
 <pre>
  - instance_form();
  - instance_widgets();
  - translate();
 </pre>
*/
class Instance
{
    typedef                         Main                                        inherited;
    typedef                         Instance                                    class_name;
    
    inherited*                                                                  parent_;

    void                            create_menu_file();
    void                            create_menu_edit();
    void                            create_menu_format();
    void                            create_menu_help();

protected:
    /** \brief Инстанцирование формы. */
    void                        instance_form();
    /** \brief Инстанцирование виджетов. */
    void                        instance_widgets();
    /** \brief Чтение настроек формы из бд. */
    void                        options_read()                                  {}

public:
    /**
     \brief Конструктор.
     \param parent - Указатель на Родительский компонент 'Main'.
     */
    explicit                    Instance(inherited* parent) : parent_(parent)   {}
    /** \brief Перевод компонетов формы. */
    void                        translate();
};
//------------------------------------------------------------------------------
    }; // end namespace go::main
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _MAININSTANCE_H_ */
