/**
    \file   MainInstance.cpp
    \brief  Файл MainInstance.cpp

    \author S.Panin
    \date   Created on 9 Январь 2011 г., 23:04
 */
//------------------------------------------------------------------------------
#include <QtGui>
#include "../../Go/tr.h"
#include "../../Go/const.h"
#include "../../Go/Const/const_icon.h"
#include "../../Go/Component/Menu/Help/MenuHelp.h"
#include "Main.h"
#include "MainInstance.h"
//------------------------------------------------------------------------------
using namespace go::main;
using namespace go::section;
//------------------------------------------------------------------------------
void Instance::instance_form()
{
        parent_->setWindowIcon(const_icon_16::program());
        parent_->setStatusBar(new QStatusBar);
}
//------------------------------------------------------------------------------
void Instance::instance_widgets()
{
        QSplitter *splitter = new QSplitter;
        parent_->text_edit_ = new QTextEdit;
        parent_->tree_widget_ = new QTreeWidget;
        
        splitter->addWidget(parent_->tree_widget_);
        splitter->addWidget(parent_->text_edit_);
        parent_->setCentralWidget(splitter);
        
        create_menu_file();
        //createEditMenu();
        //createFormatMenu();
        create_menu_help();
}
//------------------------------------------------------------------------------
void Instance::create_menu_file()
{
        QMenu* file = parent_->menu_file_ = new QMenu(parent_);
        parent_->menuBar()->addMenu(file);

        //parent_->file_section_ = new vSectionFileText(parent_, parent_->text_edit_);
        //parent_->file_section_->instance();
        //parent_->addToolBar(parent_->file_section_->toolbar());
        //parent_->menu_file_->addActions(parent_->file_section_->actions());
        file->addSeparator();

        Print* print = parent_->section_print_ = new Print(parent_->text_edit_);
        print->instance();
        parent_->addToolBar(print->toolbar());
        file->addActions(print->actions());
        file->addSeparator();
        
        Exit* exit = parent_->section_exit_ = new Exit(parent_);
        exit->instance();
        file->addActions(exit->actions());
}
//------------------------------------------------------------------------------
void Instance::create_menu_edit()
{
        //parent_->edit_menu_ = new QMenu(parent_);
        //parent_->menuBar()->addMenu(parent_->edit_menu_);
        //
        //parent_->undo_section_ = new vSectionUndoRedo(parent_->text_edit_);
        //parent_->undo_section_->instance();
        //parent_->addToolBar(parent_->undo_section_->toolbar());
        //parent_->edit_menu_->addActions(parent_->undo_section_->actions());
        //parent_->edit_menu_->addSeparator();
        //
        //parent_->edit_section_ = new class vSectionEdit(parent_->text_edit_);
        //parent_->edit_section_->instance();
        //parent_->addToolBar(parent_->edit_section_->toolbar());
        //parent_->edit_menu_->addActions(parent_->edit_section_->actions());
}
//------------------------------------------------------------------------------
void Instance::create_menu_format()
{
        //parent_->format_menu_ = new QMenu(parent_);
        //parent_->menuBar()->addMenu(parent_->format_menu_);
        //
        //parent_->style_section_ = new vSectionStyle(parent_->text_edit_);
        //parent_->style_section_->instance();
        //parent_->addToolBar(parent_->style_section_->toolbar());
        //parent_->format_menu_->addActions(parent_->style_section_->actions());
        //parent_->format_menu_->addSeparator();
        //
        //if(QApplication::isLeftToRight()) parent_->align_section_ = new vSectionAlignmentLeft(parent_->text_edit_);
        //else parent_->align_section_ = new vSectionAlignmentRight(parent_->text_edit_);
        //parent_->align_section_->instance();
        //parent_->addToolBar(parent_->align_section_->toolbar());
        //parent_->format_menu_->addActions(parent_->align_section_->actions());
        //parent_->format_menu_->addSeparator();
        //
        //parent_->addToolBarBreak(Qt::TopToolBarArea);
        //
        //parent_->font_section_ = new vSectionFont(parent_->text_edit_);
        //parent_->font_section_->instance();
        //parent_->addToolBar(parent_->font_section_->toolbar());
        //
        //parent_->bullet_section_ = new vSectionBullet(parent_->text_edit_);
        //parent_->bullet_section_->instance();
        //parent_->addToolBar(parent_->bullet_section_->toolbar());
        //parent_->format_menu_->addActions(parent_->bullet_section_->actions());
}
//------------------------------------------------------------------------------
void Instance::create_menu_help()
{
        parent_->menu_help_ = new menu::Help(parent_);
        parent_->menu_help_->instance();
        parent_->menuBar()->addMenu(parent_->menu_help_);
}
//------------------------------------------------------------------------------
void Instance::translate()
{
        parent_->menu_file_->setTitle(tr::Button::tr("&File"));
        //parent_->edit_menu_->setTitle(vtr::Button::tr("&Edit"));
        //parent_->format_menu_->setTitle(vtr::Button::tr("F&ormat"));
        parent_->setWindowTitle(QString(consts::program::name_full()) + "[*]");
}
