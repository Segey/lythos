/**
    \file   EGApp.h
    \brief  Файл Appolication для EG

    \author S.Panin
    \date   Created on 19 Январь 2011 г., 0:08
 */
//------------------------------------------------------------------------------
#ifndef _EG_APP_H_
#define	_EG_APP_H_
//------------------------------------------------------------------------------
#include "../../Go/BD/new_bd/DB.h"
#include "../../Go/Strategy/message/strategy_message.h"
#include "../../Go/const.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
//------------------------------------------------------------------------------
/**
\brief Базовый класс EG.
*/
class EGApp
{
public:
    typedef                     go::DB<go::strategy::message::window>           settings_type;
    typedef                     settings_type::select_type                      settings_select_type;
private:
    static QString              settings_name()                                 { return go::consts::db::settings::name(); }
  //  go::db::Select<go::db::type::One,strategy::message::> db ("settings.lsdb", go::consts::db::settings::name());
public:
    explicit                    EGApp() {}
/**
\brief Функция получения синглетона доступа к базе данных Settings.
\result settings_type& - Ссылка на объект сиглентон базы данных Settings.

 Пример использования Select:
\code
go::db::OneString one;
go::EGApp::settings_select_type& select = eg_app.settings().select();
select("SELECT program FROM program_info", one);
\endcode
*/
    static settings_type&        settings()                                     { static settings_type settings_("settings.lsdb", settings_name()); return settings_; }
/**
\brief Уникальный идентификатор пользователя.
\warning Необходимо переделать.
*/
    static uint                  uid()                                          { return 1; }
};
//------------------------------------------------------------------------------
}; // end namespace go
//------------------------------------------------------------------------------
#endif	/* _EG_APP_H_ */
