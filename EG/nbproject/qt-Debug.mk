#############################################################################
# Makefile for building: dist/Debug/GNU-Linux-x86/EG
# Generated by qmake (2.01a) (Qt 4.7.0) on: Sun Mar 20 12:22:46 2011
# Project:  nbproject/qt-Debug.pro
# Template: app
# Command: /home/dix/qtsdk-2010.05/qt/bin/qmake VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc-4.5
CXX           = g++-4.5
DEFINES       = -DQT_WEBKIT_LIB -DQT_SQL_LIB -DQT_XML_LIB -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -pipe -g -Wall -W -D_REENTRANT $(DEFINES)
CXXFLAGS      = -pipe -std=gnu++0x -g -Wall -W -D_REENTRANT $(DEFINES)
INCPATH       = -I../../../qtsdk-2010.05/qt/mkspecs/linux-g++ -Inbproject -I../../../qtsdk-2010.05/qt/include/QtCore -I../../../qtsdk-2010.05/qt/include/QtGui -I../../../qtsdk-2010.05/qt/include/QtXml -I../../../qtsdk-2010.05/qt/include/QtSql -I../../../qtsdk-2010.05/qt/include/QtWebKit -I../../../qtsdk-2010.05/qt/include -I. -Inbproject -I.
LINK          = g++
LFLAGS        = -Wl,-rpath,/home/dix/qtsdk-2010.05/qt/lib
LIBS          = $(SUBLIBS)  -L/home/dix/qtsdk-2010.05/qt/lib -lQtWebKit -lQtSql -L/home/dix/qtsdk-2010.05/qt/lib -lQtXml -lQtGui -L/usr/X11R6/lib -lQtCore -lpthread 
AR            = ar cqs
RANLIB        = 
QMAKE         = /home/dix/qtsdk-2010.05/qt/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = build/Debug/GNU-Linux-x86/

####### Files

SOURCES       = ../Go/BuiltType/Action/WithText/ActionWithText.cpp \
		../Go/BuiltType/Action/Body/ActionBody.cpp \
		../Go/Component/Section/Print/SectionPrint.cpp \
		classes/EGApp.cpp \
		main.cpp \
		../Go/BuiltType/Action/Chart/ActionChart.cpp \
		../Go/Component/Menu/Help/MenuHelp.cpp \
		../Go/Component/Dialog/About/DialogAbout.cpp \
		../Go/BuiltType/Action/Action.cpp \
		classes/MainInstance.cpp \
		classes/Main.cpp moc_SectionPrint.cpp \
		moc_tr.cpp \
		moc_DialogAbout.cpp \
		moc_Main.cpp \
		moc_MenuHelp.cpp \
		qrc_icons_16.cpp
OBJECTS       = build/Debug/GNU-Linux-x86/ActionWithText.o \
		build/Debug/GNU-Linux-x86/ActionBody.o \
		build/Debug/GNU-Linux-x86/SectionPrint.o \
		build/Debug/GNU-Linux-x86/EGApp.o \
		build/Debug/GNU-Linux-x86/main.o \
		build/Debug/GNU-Linux-x86/ActionChart.o \
		build/Debug/GNU-Linux-x86/MenuHelp.o \
		build/Debug/GNU-Linux-x86/DialogAbout.o \
		build/Debug/GNU-Linux-x86/Action.o \
		build/Debug/GNU-Linux-x86/MainInstance.o \
		build/Debug/GNU-Linux-x86/Main.o \
		build/Debug/GNU-Linux-x86/moc_SectionPrint.o \
		build/Debug/GNU-Linux-x86/moc_tr.o \
		build/Debug/GNU-Linux-x86/moc_DialogAbout.o \
		build/Debug/GNU-Linux-x86/moc_Main.o \
		build/Debug/GNU-Linux-x86/moc_MenuHelp.o \
		build/Debug/GNU-Linux-x86/qrc_icons_16.o
DIST          = ../../qtsdk-2010.05/qt/mkspecs/common/g++.conf \
		../../qtsdk-2010.05/qt/mkspecs/common/unix.conf \
		../../qtsdk-2010.05/qt/mkspecs/common/linux.conf \
		../../qtsdk-2010.05/qt/mkspecs/qconfig.pri \
		../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri \
		../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/debug.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/qt.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/moc.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/resources.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/uic.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/lex.prf \
		../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf \
		nbproject/qt-Debug.pro
QMAKE_TARGET  = EG
DESTDIR       = dist/Debug/GNU-Linux-x86/
TARGET        = dist/Debug/GNU-Linux-x86/EG

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c -include build/Debug/GNU-Linux-x86/EG $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: qttmp-Debug.mk $(TARGET)

$(TARGET):  $(OBJECTS)  
	@$(CHK_DIR_EXISTS) dist/Debug/GNU-Linux-x86/ || $(MKDIR) dist/Debug/GNU-Linux-x86/ 
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

qttmp-Debug.mk: nbproject/qt-Debug.pro  ../../../qtsdk-2010.05/qt/mkspecs/common/g++.conf \
		../../../qtsdk-2010.05/qt/mkspecs/common/unix.conf \
		../../../qtsdk-2010.05/qt/mkspecs/common/linux.conf \
		../../../qtsdk-2010.05/qt/mkspecs/qconfig.pri \
		../../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/debug.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/moc.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/resources.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/uic.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/lex.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf \
		/home/dix/qtsdk-2010.05/qt/lib/libQtWebKit.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtSql.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtCore.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtXml.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtGui.prl
	$(QMAKE) VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro
../../../qtsdk-2010.05/qt/mkspecs/common/g++.conf:
../../../qtsdk-2010.05/qt/mkspecs/common/unix.conf:
../../../qtsdk-2010.05/qt/mkspecs/common/linux.conf:
../../../qtsdk-2010.05/qt/mkspecs/qconfig.pri:
../../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri:
../../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/debug.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/qt.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/moc.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/resources.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/uic.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/lex.prf:
../../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf:
/home/dix/qtsdk-2010.05/qt/lib/libQtWebKit.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtSql.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtCore.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtXml.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtGui.prl:
qmake:  FORCE
	@$(QMAKE) VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro

dist: 
	@$(CHK_DIR_EXISTS) nbproject/build/Debug/GNU-Linux-x86/EG1.0.0 || $(MKDIR) nbproject/build/Debug/GNU-Linux-x86/EG1.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) nbproject/build/Debug/GNU-Linux-x86/EG1.0.0/ && $(COPY_FILE) --parents classes/EGApp.h ../Go/Component/Section/Print/SectionPrint.h ../Go/BuiltType/Action/WithText/ActionWithText.h ../Go/BuiltType/Action/Body/ActionBody.h ../Go/BuiltType/Action/Chart/ActionChart.h ../Go/tr.h ../Go/Component/Dialog/About/DialogAboutData.h ../Go/Component/Dialog/About/DialogAbout.h ../Go/const.h classes/Main.h classes/MainInstance.h pch.h ../Go/Component/Menu/Help/MenuHelp.h ../Go/BuiltType/Action/Action.h ../Go/Const/const_icon.h nbproject/build/Debug/GNU-Linux-x86/EG1.0.0/ && $(COPY_FILE) --parents ../Resources/Images/icons_16.qrc nbproject/build/Debug/GNU-Linux-x86/EG1.0.0/ && $(COPY_FILE) --parents ../Go/BuiltType/Action/WithText/ActionWithText.cpp ../Go/BuiltType/Action/Body/ActionBody.cpp ../Go/Component/Section/Print/SectionPrint.cpp classes/EGApp.cpp main.cpp ../Go/BuiltType/Action/Chart/ActionChart.cpp ../Go/Component/Menu/Help/MenuHelp.cpp ../Go/Component/Dialog/About/DialogAbout.cpp ../Go/BuiltType/Action/Action.cpp classes/MainInstance.cpp classes/Main.cpp nbproject/build/Debug/GNU-Linux-x86/EG1.0.0/ && (cd `dirname nbproject/build/Debug/GNU-Linux-x86/EG1.0.0` && $(TAR) EG1.0.0.tar EG1.0.0 && $(COMPRESS) EG1.0.0.tar) && $(MOVE) `dirname nbproject/build/Debug/GNU-Linux-x86/EG1.0.0`/EG1.0.0.tar.gz . && $(DEL_FILE) -r nbproject/build/Debug/GNU-Linux-x86/EG1.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) build/Debug/GNU-Linux-x86/EG.gch/c build/Debug/GNU-Linux-x86/EG.gch/c++
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) qttmp-Debug.mk


###### Prefix headers
build/Debug/GNU-Linux-x86/EG.gch/c: pch.h ../Go/go_full.h \
		../Go/stl.h \
		../Go/boost.h \
		../Go/const.h \
		../Go/Const/const_icon.h \
		../Go/Const/const_ext.h \
		../Go/Const/const_namespace.h \
		../Go/Const/const_file.h \
		../Go/data.h \
		../Go/tr.h \
		../Go/go/noncopyable.h \
		../Go/Test/Test.h \
		../Go/BuiltType/String.h \
		../Go/BuiltType/Number.h \
		../Go/Classes/Html/Html.h \
		../Go/Classes/Html/Href/HtmlHref.h \
		../Go/Classes/Html/Anchor/HtmlAnchor.h \
		../Go/Component/Panel/EditList/EditListPanel.h \
		../Go/Instance/Instance2.h \
		../Go/Component/Panel/EditList/EditListPanelInstance.h \
		../Go/Instance/Lay.h \
		../Go/Instance/FormInstance/FormInstance.h
	@$(CHK_DIR_EXISTS) build/Debug/GNU-Linux-x86/EG.gch/ || $(MKDIR) build/Debug/GNU-Linux-x86/EG.gch/ 
	$(CC) $(CFLAGS) $(INCPATH) -x c-header -c pch.h -o build/Debug/GNU-Linux-x86/EG.gch/c

build/Debug/GNU-Linux-x86/EG.gch/c++: pch.h ../Go/go_full.h \
		../Go/stl.h \
		../Go/boost.h \
		../Go/const.h \
		../Go/Const/const_icon.h \
		../Go/Const/const_ext.h \
		../Go/Const/const_namespace.h \
		../Go/Const/const_file.h \
		../Go/data.h \
		../Go/tr.h \
		../Go/go/noncopyable.h \
		../Go/Test/Test.h \
		../Go/BuiltType/String.h \
		../Go/BuiltType/Number.h \
		../Go/Classes/Html/Html.h \
		../Go/Classes/Html/Href/HtmlHref.h \
		../Go/Classes/Html/Anchor/HtmlAnchor.h \
		../Go/Component/Panel/EditList/EditListPanel.h \
		../Go/Instance/Instance2.h \
		../Go/Component/Panel/EditList/EditListPanelInstance.h \
		../Go/Instance/Lay.h \
		../Go/Instance/FormInstance/FormInstance.h
	@$(CHK_DIR_EXISTS) build/Debug/GNU-Linux-x86/EG.gch/ || $(MKDIR) build/Debug/GNU-Linux-x86/EG.gch/ 
	$(CXX) $(CXXFLAGS) $(INCPATH) -x c++-header -c pch.h -o build/Debug/GNU-Linux-x86/EG.gch/c++

check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all: moc_SectionPrint.cpp moc_tr.cpp moc_DialogAbout.cpp moc_Main.cpp moc_MenuHelp.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_SectionPrint.cpp moc_tr.cpp moc_DialogAbout.cpp moc_Main.cpp moc_MenuHelp.cpp
moc_SectionPrint.cpp: ../Go/Algorithm/Interfase.h \
		../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		../Go/Component/Section/Print/SectionPrint.h
	/home/dix/qtsdk-2010.05/qt/bin/moc $(DEFINES) $(INCPATH) ../Go/Component/Section/Print/SectionPrint.h -o moc_SectionPrint.cpp

moc_tr.cpp: ../Go/tr.h
	/home/dix/qtsdk-2010.05/qt/bin/moc $(DEFINES) $(INCPATH) ../Go/tr.h -o moc_tr.cpp

moc_DialogAbout.cpp: ../Go/const.h \
		../Go/Const/const_icon.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/tr.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		../Go/Component/Dialog/About/DialogAbout.h
	/home/dix/qtsdk-2010.05/qt/bin/moc $(DEFINES) $(INCPATH) ../Go/Component/Dialog/About/DialogAbout.h -o moc_DialogAbout.cpp

moc_Main.cpp: classes/MainInstance.h \
		../Go/Component/Menu/Help/MenuHelp.h \
		../Go/tr.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Const/const_icon.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		../Go/Component/Section/Exit/SectionExit.h \
		../Go/Component/Section/Print/SectionPrint.h \
		../Go/Algorithm/Interfase.h \
		classes/Main.h
	/home/dix/qtsdk-2010.05/qt/bin/moc $(DEFINES) $(INCPATH) classes/Main.h -o moc_Main.cpp

moc_MenuHelp.cpp: ../Go/tr.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Const/const_icon.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		../Go/Component/Menu/Help/MenuHelp.h
	/home/dix/qtsdk-2010.05/qt/bin/moc $(DEFINES) $(INCPATH) ../Go/Component/Menu/Help/MenuHelp.h -o moc_MenuHelp.cpp

compiler_rcc_make_all: qrc_icons_16.cpp
compiler_rcc_clean:
	-$(DEL_FILE) qrc_icons_16.cpp
qrc_icons_16.cpp: ../Resources/Images/icons_16.qrc
	/home/dix/qtsdk-2010.05/qt/bin/rcc -name icons_16 ../Resources/Images/icons_16.qrc -o qrc_icons_16.cpp

compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean compiler_rcc_clean 

####### Compile

build/Debug/GNU-Linux-x86/ActionWithText.o: ../Go/BuiltType/Action/WithText/ActionWithText.cpp ../Go/BuiltType/Action/WithText/ActionWithText.h \
		../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/ActionWithText.o ../Go/BuiltType/Action/WithText/ActionWithText.cpp

build/Debug/GNU-Linux-x86/ActionBody.o: ../Go/BuiltType/Action/Body/ActionBody.cpp ../Go/BuiltType/Action/Body/ActionBody.h \
		../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/ActionBody.o ../Go/BuiltType/Action/Body/ActionBody.cpp

build/Debug/GNU-Linux-x86/SectionPrint.o: ../Go/Component/Section/Print/SectionPrint.cpp ../Go/Component/Section/Print/SectionPrint.h \
		../Go/Algorithm/Interfase.h \
		../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/SectionPrint.o ../Go/Component/Section/Print/SectionPrint.cpp

build/Debug/GNU-Linux-x86/EGApp.o: classes/EGApp.cpp classes/EGApp.h \
		../Go/BD/new_bd/DB.h \
		../Go/BD/new_bd/DBSelect.h \
		../Go/BD/new_bd/base_bd.h \
		../Go/Strategy/message/strategy_message.h \
		../Go/const.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/EGApp.o classes/EGApp.cpp

build/Debug/GNU-Linux-x86/main.o: main.cpp classes/Main.h \
		classes/MainInstance.h \
		../Go/Component/Menu/Help/MenuHelp.h \
		../Go/tr.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Const/const_icon.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		../Go/Component/Section/Exit/SectionExit.h \
		../Go/Component/Section/Print/SectionPrint.h \
		../Go/Algorithm/Interfase.h \
		classes/EGApp.h \
		../Go/BD/new_bd/DB.h \
		../Go/BD/new_bd/DBSelect.h \
		../Go/BD/new_bd/base_bd.h \
		../Go/Strategy/message/strategy_message.h \
		../Go/BD/new_bd/Types/DBTypes.h \
		../Go/BD/new_bd/Types/DBTypeOne.h \
		../Go/BD/new_bd/Types/DBTypeState.h \
		../Go/BD/new_bd/Types/DBTypeGeometry.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/main.o main.cpp

build/Debug/GNU-Linux-x86/ActionChart.o: ../Go/BuiltType/Action/Chart/ActionChart.cpp ../Go/BuiltType/Action/Chart/ActionChart.h \
		../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/ActionChart.o ../Go/BuiltType/Action/Chart/ActionChart.cpp

build/Debug/GNU-Linux-x86/MenuHelp.o: ../Go/Component/Menu/Help/MenuHelp.cpp ../Go/Component/Menu/Help/MenuHelp.h \
		../Go/tr.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Const/const_icon.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/MenuHelp.o ../Go/Component/Menu/Help/MenuHelp.cpp

build/Debug/GNU-Linux-x86/DialogAbout.o: ../Go/Component/Dialog/About/DialogAbout.cpp ../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/Const/const_icon.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/tr.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/DialogAbout.o ../Go/Component/Dialog/About/DialogAbout.cpp

build/Debug/GNU-Linux-x86/Action.o: ../Go/BuiltType/Action/Action.cpp ../Go/BuiltType/Action/Action.h \
		../Go/tr.h \
		../Go/Const/const_icon.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/Action.o ../Go/BuiltType/Action/Action.cpp

build/Debug/GNU-Linux-x86/MainInstance.o: classes/MainInstance.cpp ../Go/tr.h \
		../Go/const.h \
		../Go/Const/const_icon.h \
		../Go/Component/Menu/Help/MenuHelp.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		classes/Main.h \
		classes/MainInstance.h \
		../Go/Component/Section/Exit/SectionExit.h \
		../Go/Component/Section/Print/SectionPrint.h \
		../Go/Algorithm/Interfase.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/MainInstance.o classes/MainInstance.cpp

build/Debug/GNU-Linux-x86/Main.o: classes/Main.cpp ../Go/Instance/MainWindowInstance.h \
		classes/Main.h \
		classes/MainInstance.h \
		../Go/Component/Menu/Help/MenuHelp.h \
		../Go/tr.h \
		../Go/BuiltType/Action/Action.h \
		../Go/Const/const_icon.h \
		../Go/Component/Dialog/About/DialogAbout.h \
		../Go/const.h \
		../Go/go/noncopyable.h \
		../Go/Instance/DllInstance.h \
		../Go/Strategy/DLL/Dll.h \
		../Go/Strategy/DLL/vStrategyDll.h \
		../Go/Component/Dialog/About/DialogAboutData.h \
		../Go/Component/Section/Exit/SectionExit.h \
		../Go/Component/Section/Print/SectionPrint.h \
		../Go/Algorithm/Interfase.h \
		../Go/BD/new_bd/Types/DBTypes.h \
		../Go/BD/new_bd/Types/DBTypeOne.h \
		../Go/BD/new_bd/Types/DBTypeState.h \
		../Go/BD/new_bd/Types/DBTypeGeometry.h \
		build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/Main.o classes/Main.cpp

build/Debug/GNU-Linux-x86/moc_SectionPrint.o: moc_SectionPrint.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/moc_SectionPrint.o moc_SectionPrint.cpp

build/Debug/GNU-Linux-x86/moc_tr.o: moc_tr.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/moc_tr.o moc_tr.cpp

build/Debug/GNU-Linux-x86/moc_DialogAbout.o: moc_DialogAbout.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/moc_DialogAbout.o moc_DialogAbout.cpp

build/Debug/GNU-Linux-x86/moc_Main.o: moc_Main.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/moc_Main.o moc_Main.cpp

build/Debug/GNU-Linux-x86/moc_MenuHelp.o: moc_MenuHelp.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/moc_MenuHelp.o moc_MenuHelp.cpp

build/Debug/GNU-Linux-x86/qrc_icons_16.o: qrc_icons_16.cpp build/Debug/GNU-Linux-x86/EG.gch/c++
	$(CXX) -c -include build/Debug/GNU-Linux-x86/EG $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/qrc_icons_16.o qrc_icons_16.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

