# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Debug/GNU-Linux-x86
TARGET = EG
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += debug 
QT = core gui sql xml webkit
SOURCES += ../Go/BuiltType/Action/WithText/ActionWithText.cpp ../Go/BuiltType/Action/Body/ActionBody.cpp ../Go/Component/Section/Print/SectionPrint.cpp classes/EGApp.cpp main.cpp ../Go/BuiltType/Action/Chart/ActionChart.cpp ../Go/Component/Menu/Help/MenuHelp.cpp ../Go/Component/Dialog/About/DialogAbout.cpp ../Go/BuiltType/Action/Action.cpp classes/MainInstance.cpp classes/Main.cpp
HEADERS += classes/EGApp.h ../Go/Component/Section/Print/SectionPrint.h ../Go/BuiltType/Action/WithText/ActionWithText.h ../Go/BuiltType/Action/Body/ActionBody.h ../Go/BuiltType/Action/Chart/ActionChart.h ../Go/tr.h ../Go/Component/Dialog/About/DialogAboutData.h ../Go/Component/Dialog/About/DialogAbout.h ../Go/const.h classes/Main.h classes/MainInstance.h pch.h ../Go/Component/Menu/Help/MenuHelp.h ../Go/BuiltType/Action/Action.h ../Go/Const/const_icon.h
FORMS +=
RESOURCES += ../Resources/Images/icons_16.qrc
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc-4.5
QMAKE_CXX = g++-4.5
DEFINES += 
INCLUDEPATH += 
LIBS += 
QMAKE_CXXFLAGS += -std=gnu++0x
CONFIG += precompile_header
CONFIG +=console
PRECOMPILED_HEADER =pch.h
