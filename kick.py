#! /usr/bin/env python3.1
import os;
import re;
import sys;
from configparser import ConfigParser;
from datetime import datetime;
from optparse import OptionParser;
#help(optparse );

path = os.getcwd();
rexs = [r'[^\.hg]/.*\.o$',r'[^\.hg]/.*c\+\+$', r'[^\.hg]/moc_.*\.cpp$', r'[^\.hg]/qrc_.*\.cpp$', r'[^\.hg].*/LD$', r'[^\.hg].*/EG_Creater$'];

class project():
    def __init__(self):    self.cx_ = 0;
    @staticmethod
    def name(): return 'Project';
    @staticmethod
    def ver(): return path + '/version.txt';
    @staticmethod
    def stat(): return path + '/statistics.txt';
    def version(self): return self.cx_;
    def update(self):
        self.update_version();
        self.update_statistics();
    def update_version(self):
        config = ConfigParser();
        config.read(self.ver());
        if 'Program' in config.sections():
            self.cx_ = config.getint('Program', 'version') + 1;
            config.set('Program', 'version', self.cx_);
            config.write(open(self.ver(),'w'));
    def update_statistics(self):
        f = open(self.stat(),'a');
        f.write("\nPROJECT={0} {1}".format(self.cx_,datetime.now()));
        f.close();

def del_files():
    cx =0;
    for dir_path, dir_names, files in os.walk(path):
        for file in files :
            str = os.path.join(dir_path, file);
            for rex in rexs :
                if (None != re.search(rex, str)):
                    os.remove(str);
                    cx +=1;
    return cx;

parser = OptionParser(description="kick - Kicked and commit program.", prog="kick", version="kick - 0.03.\nCopyright 9.01.2011.", usage="%prog -m=Message")
parser.add_option("-m",  dest="message", help="Mesasge to send hg commit");
(options, args) = parser.parse_args()

if (not options.message) :
    parser.print_help();
    exit(1);

p=project();
p.update();
cx = del_files();
print("Begin:\n - Project {0};".format(p.version()));
print(" - Removed - {0} files;".format(cx));
os.system('hg add');
os.system('hg ci -m "{0}"'.format(options.message));
os.system('hg push');
os.system('hg st');
print("Well done.".format(cx));