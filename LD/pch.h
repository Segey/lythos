// -*- C++ -*-
/* 
 * File:   pch.h
 * Author: S.Panin
 *
 * Created on 8 Январь 2010 г., 22:55
 */
//------------------------------------------------------------------------------
#ifndef _V_PCH_H_
#define	_V_PCH_H_
//------------------------------------------------------------------------------
#include "../Go/go_full.h"
#include "../Go/go/noncopyable.h"
#include "../Go/Test/Test.h"

#include "Classes/Main/vMain.h"
#include "Classes/LD/LD.h"
//------------------------------------------------------------------------------
#endif	/* _V_PCH_H_ */

