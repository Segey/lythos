/**
    \file   Source_Settings.h
    \brief  Файл Source_Settings.h

    \author S.Panin
    \date   Created on 1 Январь 2011 г., 11:49
 */
//------------------------------------------------------------------------------
#ifndef _V_SOURCE_SETTINGS_H_
#define	_V_SOURCE_SETTINGS_H_
//------------------------------------------------------------------------------
#include "../../Go/const.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

    /** \namespace go::db  */
    namespace db {

    /** \namespace go::db::source  */
    namespace source {

/** \brief Скрипт для создания базы данных Настроек. */
class Settings
{
    typedef                             Settings                                class_name;

    static QString forms()
    {
        return "CREATE TABLE forms ( "
        "form_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
        "user_id INTEGER  NOT NULL, "
        "name STRING NOT NULL, "
        "geometry TEXT NULL, "
        "state TEXT NULL "
        ")";
    }

    static QString LD()
    {
        return "CREATE TABLE LD ( "
        "UserID    integer NOT NULL, "
        "FormSize  text, "
        "FormShow  boolean "
        ")";
    }

    static QString MeasurementHistory()
    {
        return "CREATE TABLE MeasurementHistory ( "
        "UserID      integer NOT NULL, "
        "FormSize            text, "
        "DialogFormSize      text, "
        "HeaderList          text, "
        "PanelsMBs           text "
        ")";
    }

    static QString Path()
    {
        return "CREATE TABLE Path ( "
        "UserID    integer NOT NULL, "
        "DirImage  text, "
        "DirText   text "
        ");";
    }

    static QString RecordHistory()
    {
        return "CREATE TABLE RecordHistory ( "
        "UserID      integer NOT NULL, "
        "FormSize    text, "
        "HeaderList  text "
        ");";
    }

    static QString RichEdit()
    {
        return "CREATE TABLE RichEdit ( "
        "UserID      integer NOT NULL, "
        "FormSize    text, "
        "FormState   blob "
        ");";
    }

public:
    /**
     \brief Статическая функция для получения строк скриптов.
     \result QStringList - список строк скриптов для создания таблиц базы данных  database().
     */
    static QStringList                  source()
    {
        QStringList list;
        list.push_back(class_name::forms());
        list.push_back(class_name::LD());
        list.push_back(class_name::MeasurementHistory());
        list.push_back(class_name::Path());
        list.push_back(class_name::RecordHistory());
        list.push_back(class_name::RichEdit());
        return list;
    }
    /** \brief Последняя версия базы данных Настроек. */
    static double                       version()                               { return consts::bd::settings::version(); }
    /** \brief Имя базы данных Настроек. */
    static QString                      database()                              { return consts::bd::settings::name(); }
};
//------------------------------------------------------------------------------
        } // end namespace go::db::source
    } // end namespace go::db
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _V_SOURCE_SETTINGS_H_ */

