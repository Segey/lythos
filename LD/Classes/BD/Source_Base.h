/**
    \file   Source_LD.h
    \brief  Файл c сырцами LD Source_LD.h

    \author S.Panin
    \date   Created on 23 Декабрь 2010 г., 1:26
 */
//------------------------------------------------------------------------------
#ifndef _SOURCE_LD_H_
#define	_SOURCE_LD_H_
//------------------------------------------------------------------------------
#include "../../Go/const.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

    /** \namespace go::db  */
    namespace db {

    /** \namespace go::db::source  */
    namespace source {

/** \brief Скрипт для создания базы Base. */
class Base
{
    typedef                             Base                                    class_name;

    static QString MeasurementHistory()
    {
        return "CREATE TABLE MeasurementHistory ( "
        "userID                integer NOT NULL, "
        "MeasurementHistoryID  integer NOT NULL, "
        "Ghost                 boolean NOT NULL DEFAULT 0, "
        "HistoryDate           date NOT NULL, "
        "Growth                float(20), "
        "Weight                float(20), "
        "Neck                  float(20), "
        "Shoulders             float(20), "
        "Chest                 float(20), "
        "BicepLeft             float(20), "
        "BicepRight            float(20), "
        "ForearmLeft           float(20), "
        "ForearmRight          float(20), "
        "WristLeft             float(20), "
        "WristRight            float(20), "
        "Abdomen               float(20), "
        "Waist                 float(20), "
        "Hips                  float(20), "
        "ThighLeft             float(20), "
        "ThighRight            float(20), "
        "CalfLeft              float(20), "
        "CalfRight             float(20) "
        ")";
    }

    static QString Options()
    {
        return "CREATE TABLE Options ( "
        "UserID       integer NOT NULL, "
        "Language     integer NOT NULL, "
        "Country      integer NOT NULL, "
        "Translation  text NOT NULL "
        ")";
    }

    static QString PersonalInformation()
    {
        return "CREATE TABLE PersonalInformation ( "
        "UserID       userid NOT NULL, "
        "Sex          integer DEFAULT 0, "
        "DateofBirth  date, "
        "Height       text, "
        "Country      text, "
        "Region       text, "
        "City         text, "
        "Address      text, "
        "EMail        text, "
        "ICQ          text, "
        "Phone        text, "
        "Mobule       text, "
        "Doctor       text, "
        "Instructor   text, "
        "Image        blob"
        ");";
    }

    static QString PhotoGallery()
    {
        return "CREATE TABLE PhotoGallery ( "
        "userID            integer NOT NULL, "
        "ImagePosition     integer, "
        "ImageDate         date, "
        "ImageDescription  text, "
        "Image             blob "
        ");";
    }

    static QString SignUp()
    {
        return "CREATE TABLE SignUp ( "
        "userID        integer PRIMARY KEY AUTOINCREMENT NOT NULL, "
        "userName      text NOT NULL UNIQUE, "
        "userPassword  text NOT NULL "
        ");";
    }

    static QString RecordHistory()
    {
        return "CREATE TABLE RecordHistory ( "
        "userID       integer NOT NULL, "
        "Ghost        boolean NOT NULL DEFAULT 0, "
        "Exercise     text NOT NULL, "
        "RecordDate   date NOT NULL, "
        "Weight       integer NOT NULL, "
        "Repetitions  integer NOT NULL, "
        "Note         blob "
        ");";
    }

public:
    /**
     \brief Статическая функция для получения строк скриптов.
     \result QStringList - список строк скриптов для создания таблиц базы данных  database().
     */
    static QStringList                  source()
    {
        QStringList list;
        list.push_back(class_name::MeasurementHistory());
        list.push_back(class_name::Options());
        list.push_back(class_name::PersonalInformation());
        list.push_back(class_name::PhotoGallery());
        list.push_back(class_name::RecordHistory());
        list.push_back(class_name::SignUp());
        return list;
    }
    /** \brief Последняя версия базы данных Основная. */
    static double                       version()                               { return consts::bd::base::version(); }
    /** \brief Имя базы данных Основная. */
    static QString                      database()                              { return consts::bd::base::name(); }
};
//------------------------------------------------------------------------------
        } // end namespace go::db::source
    } // end namespace go::db
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _SOURCE_LD_H_ */

