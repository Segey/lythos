/* 
 * File:   vBossLicence.h
 * Author: S.Panin
 *
 * Created on 23 Июль 2009 г., 14:13
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_LICENCE_H
#define	_V_MAIN_LICENCE_H
//------------------------------------------------------------------------------
#include "Classes/Main/MainDialog.h"
#include "Classes/LD/ClientLD/ClientLDDialog.h"
class vMain;
//------------------------------------------------------------------------------
template <int Max> class vLicence {};
//------------------------------------------------------------------------------
template <>
class vLicence<GROUP_LICENCE>
{

    typedef                         vLicence                                    class_name;

private:
    QApplication*                                                               application_;
    boost::shared_ptr<vMainDialog>                                              base_dialog_;
   // boost::shared_ptr<vClientLDDialog>                                          personal_office_dialog_;
public:
   // explicit  vBossLicence          (QApplication* application) :  application_(application)                {}
    void                            instance()
    {
//     base_dialog_.reset( new vBossDialog);
//    // application_->setActiveWindow(base_dialog_.get());
//     base_dialog_->show();
    }

    void new_user()
    {
//        base_dialog_->hide();
//        personal_office_dialog_.reset(new vClientLDDialog);
//        personal_office_dialog_->show();
    //   base_dialog_->show();
    }
    //virtual ~vvBossLicence() {}
};
//------------------------------------------------------------------------------
template <>
class vLicence<SiNGLE_LICENCE>
{

    typedef                         vLicence                                    class_name;

private:
    vMain*                                                                      main_;

public:
    explicit  vLicence              (vMain* Main) :  main_(Main)                {}
    void                            instance()                                  {}
    //virtual ~vvBossLicence() {}
};

//template <>
//void vBossLicence<GROUP_LICENCE>::new_user()
//    {
//     base_dialog_->hide();
//     vClientLDDialog* dialog =new vClientLDDialog;
//    dialog->show();
////base_dialog_->hide();
//          //  base_dialog_.reset( new vBossDialog);
//    // application_->setActiveWindow(base_dialog_.get());
//   //  base_dialog_->show();
//    }
//
//template<typename T>
//class one {};
//
//template<int>
//class one
//{
//    void print();
//};
//template<>
//void one<int>::print() {}

//------------------------------------------------------------------------------
#endif	/* _V_VBOSSLICENCE_H */