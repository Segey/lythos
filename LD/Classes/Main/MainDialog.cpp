/* 
 * File:   vMainDialog.cpp
 * Author: S.Panin
 * 
 * Created on 1 Июль 2009 г., 15:54
 */
//------------------------------------------------------------------------------
#include "MainDialog.h"
//------------------------------------------------------------------------------
vMainDialog::vMainDialog(QWidget* parent /*= 0*/)
                : QDialog(parent, Qt::WindowTitleHint), username_icon_label_(0), username_label_(0), select_user_label_(0), select_user_combobox_(0),
                password_label_(0), password_edit_(0), copyright_(0), new_user_button_(0), continue_button_(0), exit_button_(0)
{
        ::vInstance<vMainInstance > (this).instance();
        enableContinueButton(password_edit_->text());
        instance_signals();
}
//------------------------------------------------------------------------------
void vMainDialog::instance_signals()
{
        connect(exit_button_, SIGNAL(clicked()), SLOT(close()));
        connect(password_edit_, SIGNAL(textChanged(const QString&)), SLOT(enableContinueButton(const QString&)));
        connect(continue_button_, SIGNAL(clicked()), SLOT(continueButtonClicked()));
        connect(new_user_button_, SIGNAL(clicked()), SLOT(newUserButtonClicked()));
}
//------------------------------------------------------------------------------
void vMainDialog::enableContinueButton(const QString& text)
{
        continue_button_->setEnabled(select_user_combobox_->currentIndex() >-1 && !text.isEmpty());
}
//------------------------------------------------------------------------------
void vMainDialog::addUserNames(const QStringList& names)
{
        select_user_combobox_->clear();
        select_user_combobox_->addItems(names);
}
//------------------------------------------------------------------------------
void vMainDialog::continueButtonClicked()
{
        doClickContinue(select_user_combobox_->currentText(), password_edit_->text());
        password_edit_->setText("");
}
//------------------------------------------------------------------------------
void vMainDialog::newUserButtonClicked()
{
        password_edit_->setText("");
        doClickNewUser();
}
//------------------------------------------------------------------------------
bool vMainDialog::test()
{
        Q_ASSERT(username_icon_label_);
        Q_ASSERT(username_label_);
        Q_ASSERT(select_user_label_);
        Q_ASSERT(select_user_combobox_);
        Q_ASSERT(password_label_);
        Q_ASSERT(password_edit_);
        Q_ASSERT(copyright_);
        Q_ASSERT(new_user_button_);
        Q_ASSERT(continue_button_);
        Q_ASSERT(exit_button_);

        Q_ASSERT(username_icon_label_->text().isEmpty());
        Q_ASSERT(!username_label_->text().isEmpty());
        Q_ASSERT(!select_user_label_->text().isEmpty());
        Q_ASSERT(!password_label_->text().isEmpty());
        Q_ASSERT(!copyright_->text().isEmpty());
        Q_ASSERT(!new_user_button_->text().isEmpty());
        Q_ASSERT(!continue_button_->text().isEmpty());
        Q_ASSERT(!exit_button_->text().isEmpty());
        Q_ASSERT( new_user_button_->isEnabled() );
        Q_ASSERT( !continue_button_->isEnabled() );
        Q_ASSERT( exit_button_->isEnabled() );
        Q_ASSERT(password_edit_->maxLength() ==64);
        Q_ASSERT( (void*)select_user_label_->buddy() == (void*)select_user_combobox_);
        Q_ASSERT( (void*)password_label_->buddy() == (void*)password_edit_);

        enableContinueButton("La-la");
        Q_ASSERT( continue_button_->isEnabled() );
        enableContinueButton("");
        Q_ASSERT( !continue_button_->isEnabled() );

        select_user_combobox_->clear();
         Q_ASSERT( !select_user_combobox_->count() );
        addUserNames( QStringList() <<"La" <<"La");
        Q_ASSERT( select_user_combobox_->count() ==2);
        return true;
}
//------------------------------------------------------------------------------