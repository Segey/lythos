//------------------------------------------------------------------------------
#include "MainInstance.h"
//------------------------------------------------------------------------------
void vMainInstance::instance_form()
{
        parent_->setFixedSize(480,0);
}
//------------------------------------------------------------------------------
void vMainInstance::instance_widgets()
{
        parent_->username_label_         = new QLabel;
        parent_->username_icon_label_    = new QLabel;
        parent_->select_user_label_      = new QLabel;
        parent_->password_label_         = new QLabel;
        parent_->copyright_              = new QLabel;
        parent_->password_edit_          = new QLineEdit;
        parent_->select_user_combobox_   = new QComboBox;
        parent_->new_user_button_        = new QPushButton;
        parent_->continue_button_        = new QPushButton;
        parent_->exit_button_            = new QPushButton;
        
        instance_set_buddy();
}
//------------------------------------------------------------------------------
void vMainInstance::instance_set_buddy()
{
        parent_->continue_button_->setDefault(true);
        parent_->select_user_label_->setBuddy(parent_->select_user_combobox_);
        parent_->password_label_->setBuddy(parent_->password_edit_);
        parent_->password_edit_->setMaxLength(64);
        parent_->password_edit_->setEchoMode(QLineEdit::Password);
}
//------------------------------------------------------------------------------
void vMainInstance::instance_images()
{
        parent_->username_icon_label_->setPixmap(const_pixmap_64::user());
}
//------------------------------------------------------------------------------
QLayout* vMainInstance::instance_top_layout()
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(parent_->username_icon_label_, parent_->username_label_);
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* vMainInstance::instance_middle_layout()
{

        vLay lay_left(new QGridLayout, 2);
        lay_left.add_stretch(true);
        lay_left.add_unary_label(parent_->select_user_label_,     parent_->select_user_combobox_, false);
        lay_left.add_unary_label(parent_->password_label_,        parent_->password_edit_);

        QVBoxLayout* lay_right = new QVBoxLayout();
        lay_right->addWidget(parent_->new_user_button_);
        lay_right->addWidget(parent_->continue_button_);
        lay_right->addWidget(parent_->exit_button_);

        vLay lay(new QGridLayout, 2);
        lay.add_binary_layouts(lay_left.clayout(), lay_right);
        return lay.clayout();
}
//------------------------------------------------------------------------------
QLayout* vMainInstance::instance_bottom_layout()
{
        vBottomInstance BottomInstance;       
        BottomInstance.add_stretch();
        BottomInstance.add_widget(parent_->copyright_);
        return BottomInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
void vMainInstance::translate()
{
        parent_->setWindowTitle(vtr::Dialog::tr("Sign In"));
        parent_->username_label_->setText(vtr::Dialog::tr("User Name and password"));
        parent_->select_user_label_->setText(vtr::Dialog::tr("&Select User:"));
        parent_->password_label_->setText(vtr::Dialog::tr("&Password:"));
        parent_->copyright_->setText(vtr::Copyright::tr("Copyright (C) 2005-2010 Lythos Software Inc. All rights reserved."));
        parent_->new_user_button_->setText(vtr::Button::tr("&New User"));
        parent_->continue_button_->setText(vtr::Button::tr("&Continue"));
        parent_->exit_button_->setText(vtr::Dialog::tr("&Exit"));
}
