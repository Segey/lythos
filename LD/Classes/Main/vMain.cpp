/* 
 * File:   Main.cpp
 * Author: S.Panin
 * 
 * Created on 24 Июль 2009 г., 16:32
 */
//------------------------------------------------------------------------------
#include "vMain.h"
//------------------------------------------------------------------------------
vMain::vMain()
{
        inherited::new_user_button()->setEnabled(basebd().Connect());
        inherited::addUserNames(v_user::get_all_user_names());
        settingsbd().Connect();
}
//------------------------------------------------------------------------------
/*virtual*/ void vMain::doClickNewUser()
{
        showDialog(true);
}
//------------------------------------------------------------------------------
/*virtual*/ void vMain::doClickContinue(const QString& user_name, const QString& password)
{
        if(v_user::check_password(user_name, password) ==false ) return vInterfase::vMigalo(inherited::password_edit());

        user().load(user_name);
        showDialog(false);
}
//------------------------------------------------------------------------------
void vMain::showDialog(bool isNewUser)
{
        inherited::hide();
        boost::shared_ptr<vClientLDDialog> dialog(isNewUser ? vClientLDDialog::make_NewUserClientLDDialog() : vClientLDDialog::make_ExistsUserClientLDDialog());
        dialog->exec();
        inherited::show();
        inherited::addUserNames(v_user::get_all_user_names());
}
//------------------------------------------------------------------------------
bool vMain::test()
{
        inherited::test();
        return true;
}
//------------------------------------------------------------------------------
