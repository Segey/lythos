/* 
 * File:   VBossDialog.h
 * Author: S.Panin
 *
 * Created on 1 Июль 2009 г., 15:54
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_DIALOG_H_
#define	_V_MAIN_DIALOG_H_
//------------------------------------------------------------------------------
#include "MainInstance.h"
//------------------------------------------------------------------------------
class vMainDialog : public QDialog
{
Q_OBJECT
    friend  class vMainInstance;
    typedef                     QDialog                                         inherited;
    typedef                     vMainDialog                                     class_name;

private:
        QLabel* 								username_icon_label_;
        QLabel* 								username_label_;
        QLabel* 								select_user_label_;
        QComboBox*                                                              select_user_combobox_;
        QLabel* 								password_label_;
        QLineEdit* 								password_edit_;
        QLabel* 								copyright_;
        QPushButton* 								new_user_button_;
        QPushButton* 								continue_button_;
        QPushButton* 								exit_button_;

        void                    instance_signals();

protected:
        virtual void            doClickNewUser()                                =0;
        virtual void            doClickContinue(const QString& user_name, const QString& password)           =0;
        void                    addUserNames(const QStringList& names);
        QLineEdit*              password_edit()                                 {return password_edit_;}
        QPushButton*            new_user_button()                               {return new_user_button_;}
        
private slots:
        void                    enableContinueButton(const QString&);
        void                    continueButtonClicked();
        void                    newUserButtonClicked();

public:
    explicit                    vMainDialog(QWidget* owner =0);
    virtual                     ~vMainDialog()                                  {}
    bool                        test();

};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_DIALOG_H_ */

