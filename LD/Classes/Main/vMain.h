/* 
 * File:   Main.h
 * Author: S.Panin
 *
 * Created on 24 Июль 2009 г., 16:32
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_H_
#define	_V_MAIN_H_
//------------------------------------------------------------------------------
#include "MainDialog.h"
#include "../../../Go/Singleton/BD/Base/SingletonBaseBD.h"
#include "../../../Go/Singleton/BD/Settings/SingletonSettingsBD.h"
#include "../../../Go/Singleton/User/SingletonUser.h"
//------------------------------------------------------------------------------
class vMain : public vMainDialog
{
        typedef                         vMainDialog                             inherited;
        typedef                         vMain                                   class_name;

        void                            showDialog(bool isNewUser);

protected:
        virtual void                    doClickNewUser();
        virtual void                    doClickContinue(const QString& user_name, const QString& password);

public:
        explicit vMain();
        static vSingletonBaseBD&        basebd()                                {static vSingletonBaseBD basebd;            return basebd;}
        static vSingletonSettingsBD&    settingsbd()                            {static vSingletonSettingsBD bd;            return bd;}
        static vSingletonUser&          user()                                  {static vSingletonUser user;                return user;}
        static QApplication&            application()                           {static QApplication app(0,0);              return app; }
        bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MAIN_H_ */
