/*
 * File:   VBossInstance.h
 * Author: S.Panin
 *
 * Created on 24 Июнь 2009 г., 23:09
 */
//------------------------------------------------------------------------------
#ifndef _V_MAIN_INSTANCE_H
#define	_V_MAIN_INSTANCE_H
//------------------------------------------------------------------------------
class vMainDialog;
//------------------------------------------------------------------------------
class vMainInstance
{
    typedef                 vMainInstance                                       class_name;
    
    vMainDialog*                                                                parent_;


protected:
    vMainDialog*            parent()                                            {return parent_;}
    const vMainDialog*      parent() const                                      {return parent_;}
    void                    instance_form();
    void                    instance_widgets();
    void                    instance_set_buddy();
    QLayout*                instance_top_layout();
    QLayout*                instance_middle_layout();
    QLayout*                instance_bottom_layout();
    void                    instance_images();
    void                    translate();
    
public:
    explicit                vMainInstance(vMainDialog* parent) : parent_(parent){ }
};
//------------------------------------------------------------------------------
#endif /* _V_MAIN_INSTANCE_H */
