/* 
 * File:   RecordHistory.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#include "RecordHistory.h"
#include "RecordHistoryData.h"
//------------------------------------------------------------------------------
vRecordHistory::vRecordHistory(vClientLDDialog* parent)
         : parent_(parent), data_(new data_name)
{     
        vDllInstance::instance(this, data_.get());
        data_->translate();
        data_->tree_view_->instance();
        inherited::setWindowModified(false);
        inherited::setWindowTitle("[*]");
}
//------------------------------------------------------------------------------
vRecordHistory* vRecordHistory::make_new(vClientLDDialog* parent)
{
     vRecordHistory* result = new vRecordHistory(parent);
     result->data_->tree_view_->setEnabled(false);
     return result;
}
//------------------------------------------------------------------------------
vRecordHistory* vRecordHistory::make_exists(vClientLDDialog* parent)
{
     return new vRecordHistory(parent);
}
//------------------------------------------------------------------------------
bool vRecordHistory::test()
{
        Q_ASSERT(data_->username_icon_label_);
        Q_ASSERT(data_->username_label_);
        Q_ASSERT(data_->cap_label_);
        Q_ASSERT(data_->tree_view_);
        Q_ASSERT(parent_);
        Q_ASSERT(!data_->username_icon_label_->pixmap()->isNull());
        Q_ASSERT(data_->username_icon_label_->text().isEmpty());
        Q_ASSERT(!data_->cap_label_->text().isEmpty());
        Q_ASSERT(!data_->username_label_->text().isEmpty());
        data_->tree_view_->setEnabled(false);
        Q_ASSERT(!data_->tree_view_->isEnabled());
        do_change_state();
        Q_ASSERT(data_->tree_view_->isEnabled());
        return true;
}
