/* 
 * File:   PersonalInfo.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#ifndef _V_ADD_RECORD_HISTORY_DIALOG_H
#define	_V_ADD_RECORD_HISTORY_DIALOG_H
//------------------------------------------------------------------------------
#include "LDRecordHistoryDialogData.h"
//------------------------------------------------------------------------------
class vLDRecordHistoryDialog : public QDialog, go::noncopyable
{
Q_OBJECT

    typedef                         QDialog                                     inherited;
    typedef                         vLDRecordHistoryDialog                      class_name;
    typedef                         vLDRecordHistoryDialogData                  data_name;

    boost::shared_ptr<data_name>                                                data_;

    static QString                  settings_table()                            { return ("RecordHistory"); }
    static QString                  settings_key()                              { return ("FormSize"); }
    void                            settings_read();
    void                            settings_write() const;

    void                            instance_form();
    void                            instance_signals();
    bool                            all_calculate();
    void                            set_enabled(bool value);
    bool                            calculate_progress();
    bool                            calculate_remains_days();
    int                             round_to_multiple_ten(int count_days)       { return vDLL<int>(const_libs::in1256t(), "round_to_multiple_ten")(count_days, count_days); }

protected:
    virtual void                    closeEvent(QCloseEvent *event);
    virtual bool                    do_load(const QString& exercise) =0;
    virtual bool                    do_save() =0;

    QLineEdit*                      exercise_edit() const                       {return data_->exercise_edit_;}
    vLDRecordHistoryDialogPanel*    left_panel() const                          {return data_->left_panel_;}
    vLDRecordHistoryDialogPanel*    right_panel() const                         {return data_->right_panel_;}

public slots:
    void                            save();
    void                            exerciseEditStateChanged();
    void                            leftPanelStateChanged();
    void                            RightPanelStateChanged();

public:
    explicit                        vLDRecordHistoryDialog(QWidget* owner =0);
    virtual                         ~vLDRecordHistoryDialog()                   { settings_write(); }
    bool                            check();
    bool                            load(const QString& exercise);
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_ADD_RECORD_HISTORY_DIALOG_H */
