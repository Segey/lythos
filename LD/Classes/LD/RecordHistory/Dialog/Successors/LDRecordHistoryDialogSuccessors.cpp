/* 
 * File:   RichEdit.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#include "LDRecordHistoryDialogSuccessors.h"
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryDialogSuccessorsNew::do_save()
{
        if( v_bd::exists(vMain::basebd().database(),"SELECT Exercise FROM RecordHistory WHERE (Exercise =? AND UserID =?);", inherited::exercise_edit()->text()))
        {
                RESTORE_OVERRIDE_CURSOR(QMessageBox::information(0, program::name_full(), vtr::BD::tr("Exercise '%1' is already contained in the database!\nEnter a new Exercise or change the existing record with data!").arg(inherited::exercise_edit()->text()), QMessageBox::Ok));
                return false;
        }
        return save(false, inherited::left_panel()) && save(true, inherited::right_panel());
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogSuccessorsNew::save(bool is_ghost, vLDRecordHistoryDialogPanel* panel)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO RecordHistory (userID, Ghost, Exercise, RecordDate, Weight, Repetitions, Note) VALUES (?,?,?,?,?,?,?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(is_ghost);
        query.addBindValue(inherited::exercise_edit()->text() );
        query.addBindValue(panel->get_date() );
        query.addBindValue(panel->get_weight() );
        query.addBindValue(panel->get_repetitions() );
        query.addBindValue(panel->get_notes() );
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogSuccessorsNew::test()
{
        inherited::test();

        boost::shared_ptr<vLDRecordHistoryDialogPanel> panel(new vLDRecordHistoryDialogPanel);
        panel->set_date(QDate::currentDate());
        panel->set_weight("88");
        panel->set_repetitions("9");
        panel->set_notes("Gy _ Gy");
        inherited::exercise_edit()->setText("0");

        vMain::user().set_test_id();
        
        save(true, panel.get());
        Q_ASSERT(v_bd::exists(vMain::basebd().database(), "SELECT userID FROM RecordHistory WHERE (UserID =?);"));

        QSqlQuery query(vMain::basebd().database());
        return query.exec("DELETE FROM RecordHistory WHERE UserID =0");
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryDialogSuccessorsOpen::do_load(const QString& exercise)
{
        inherited::exercise_edit()->setText(exercise);
        previous_exercise_ =exercise;
        return load_form(exercise, false) && load_form(exercise, true);
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogSuccessorsOpen::load_form(const QString& exercise, bool is_ghost)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT RecordDate, Weight, Repetitions, Note FROM RecordHistory WHERE (UserID =? AND Ghost =? AND Exercise =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(is_ghost);
        query.addBindValue(exercise);
        if (!query.exec()) return false;
        while(query.next()) is_ghost ? update_panel(query, inherited::right_panel()) : update_panel(query, inherited::left_panel());
        return true;
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialogSuccessorsOpen::update_panel(const QSqlQuery& query, vLDRecordHistoryDialogPanel* panel)
{
        panel->set_date(query.value(0).toDate());
        panel->set_weight(query.value(1).toString());
        panel->set_repetitions(query.value(2).toString());
        panel->set_notes(query.value(3).toString());
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryDialogSuccessorsOpen::do_save()
{
        return save(false, inherited::left_panel()) && save(true, inherited::right_panel());
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogSuccessorsOpen::save(bool is_ghost, vLDRecordHistoryDialogPanel* panel)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE RecordHistory SET Exercise =?, RecordDate =?, Weight =?, Repetitions =? , Note =? WHERE (Exercise =? AND Ghost =? AND UserID =?);");
        query.addBindValue(inherited::exercise_edit()->text() );
        query.addBindValue(panel->get_date() );
        query.addBindValue(panel->get_weight() );
        query.addBindValue(panel->get_repetitions() );
        query.addBindValue(panel->get_notes() );
        query.addBindValue(  previous_exercise_ );
        query.addBindValue(is_ghost);
        query.addBindValue(vMain::user().id());
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogSuccessorsOpen::test()
{
        inherited::test();

        boost::shared_ptr<vLDRecordHistoryDialogPanel> panel(new vLDRecordHistoryDialogPanel);
        panel->set_date(QDate::currentDate());
        panel->set_weight("88");
        panel->set_repetitions("9");
        panel->set_notes("Gy _ Gy");
        inherited::exercise_edit()->setText("0");
        previous_exercise_ = "0";

        vMain::user().set_test_id();
        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO RecordHistory (userID, Ghost, Exercise, RecordDate, Weight, Repetitions) VALUES (0,?,'0',?,0,0);");
        query.addBindValue(true);
        query.addBindValue(QDate::currentDate());
        query.exec();

        save(true, panel.get());
        Q_ASSERT(v_bd::exists(vMain::basebd().database(),"SELECT userID FROM RecordHistory WHERE (UserID =?);"));

        return query.exec("DELETE FROM RecordHistory WHERE UserID =0");
        return true;
}