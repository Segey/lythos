/* 
 * File:   RichEdit.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_RECORD_HISTORY_DIALOG_SUCCESSORS_H_
#define	_V_LD_RECORD_HISTORY_DIALOG_SUCCESSORS_H_
//------------------------------------------------------------------------------
#include "../Panel/LDRecordHistoryDialogPanel.h"
#include "../LDRecordHistoryDialog.h"
//------------------------------------------------------------------------------
class vLDRecordHistoryDialogSuccessorsNew : public vLDRecordHistoryDialog
{
    typedef                             vLDRecordHistoryDialog                       inherited;
    typedef                             vLDRecordHistoryDialogSuccessorsNew                class_name;

    bool                                save(bool is_ghost, vLDRecordHistoryDialogPanel* panel);

protected:
    virtual bool                        do_load(const QString& )                {return true;}
    virtual bool                        do_save();

public:
    explicit                            vLDRecordHistoryDialogSuccessorsNew(QWidget* parent = 0) : inherited(parent)  {}
    bool                                test();
};
//------------------------------------------------------------------------------
class vLDRecordHistoryDialogSuccessorsOpen : public vLDRecordHistoryDialog
{
    typedef                             vLDRecordHistoryDialog                       inherited;
    typedef                             vLDRecordHistoryDialogSuccessorsOpen               class_name;

    QString                                                                     previous_exercise_;

    bool                                load_form(const QString& exercise, bool is_ghost);
    void                                update_panel(const QSqlQuery& query, vLDRecordHistoryDialogPanel* panel);
    bool                                save(bool is_ghost, vLDRecordHistoryDialogPanel* panel);

protected:
    virtual bool                        do_load(const QString& exercise);
    virtual bool                        do_save();

public:
    explicit                            vLDRecordHistoryDialogSuccessorsOpen(QWidget* parent = 0) : inherited(parent)  {}
    bool                                test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_RECORD_HISTORY_DIALOG_SUCCESSORS_H_ */
