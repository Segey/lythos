// -*- C++ -*-
/* 
 * File:   LDRecordHistoryDialogData.h
 * Author: S.Panin
 *
 * Created on 19 Январь 2010 г., 7:43
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_RECORD_HISTORY_DIALOG_DATA_H
#define	_V_LD_RECORD_HISTORY_DIALOG_DATA_H
//------------------------------------------------------------------------------
struct vLDRecordHistoryDialogData
{

    static QString                      name_dll()                              { return const_instance_libs::ldrhd740(); }

    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    QLabel*                                                                     exercise_label_;
    QLineEdit*                                                                  exercise_edit_;
    QPushButton*                                                                exercise_button_;
    vLDRecordHistoryDialogPanel*                                                left_panel_;
    vLDRecordHistoryDialogPanel*                                                right_panel_;
    QPushButton*                                                                update_button_;
    QPushButton*                                                                cancel_button_;

    explicit                            vLDRecordHistoryDialogData() : left_panel_(new vLDRecordHistoryDialogPanel), right_panel_(new vLDRecordHistoryDialogPanel) {}

    void translate()
    {
        username_icon_label_    ->setPixmap(const_pixmap_64::record_history_dialog());
        username_label_         ->setText(vtr::Dialog::tr("Add/Edit a Personal Record"));
        cap_label_              ->setText(vtr::Exercise::tr("Select Exercise"));
        exercise_label_         ->setText(vtr::Exercise::tr("&Exercise"));
        exercise_button_        ->setText(vtr::Button::tr("&Open"));
        update_button_          ->setText(vtr::Button::tr("&Save"));
        cancel_button_          ->setText(vtr::Button::tr("&Cancel"));
        left_panel_             ->setTitle(vtr::Record::tr("My Current Result"));
        right_panel_            ->setTitle(vtr::Record::tr("My Desired Result"));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_RECORD_HISTORY_DIALOG_DATA_H */

