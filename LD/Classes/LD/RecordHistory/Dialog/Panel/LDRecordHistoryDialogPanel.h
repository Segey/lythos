/* 
 * File:   RecordHistoryPanel.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_RECORD_HISTORY_DIALOG_PANEL_H_
#define	_V_LD_RECORD_HISTORY_DIALOG_PANEL_H_
//------------------------------------------------------------------------------
#include "../../../../../../Go/Component/Dock/RichEdit/DockRichEdit.h"
#include "../../../../../../Go/Component/All/WidgetModified/WidgetModified.h"
#include "LDRecordHistoryDialogPanelData.h"
//------------------------------------------------------------------------------
class vLDRecordHistoryDialogPanel : public vWidgetModified<QGroupBox>, go::noncopyable
{
Q_OBJECT
    typedef                         vWidgetModified<QGroupBox>                  inherited;
    typedef                         vLDRecordHistoryDialogPanel                 class_name;
    typedef                         vLDRecordHistoryDialogPanelData             data_name;

    QWidget*                                                                    parent_;
    boost::shared_ptr<data_name>                                                data_;

    void                        instance_signals();

signals:
    void                        stateChanged();

private slots:
    void                        enableForm();
    
public:
    explicit                    vLDRecordHistoryDialogPanel(QWidget* parent =0);
    void                        set_visible_progress_bar(bool visible)          { data_->set_visible_progress_bar(visible); }
    void                        setWindowModified(bool value);
    void                        set_date(const QDate& date)                     {data_->date_edit_->setDate(date);}
    QDate                       get_date()                                      {return data_->date_edit_->date();}
    void                        set_weight(const QString& value)                {data_->weight_combobox_->setEditText(value);}
    int                         get_weight()                                    {return vMain::user().locale().Int().from_string( data_->weight_combobox_->currentText() ); }
    void                        set_repetitions(const QString& value)           {data_->repetitions_combobox_->setEditText(value);}
    int                         get_repetitions()                               {return vMain::user().locale().Int().from_string( data_->repetitions_combobox_->currentText() );}
    void                        set_notes(const QString& text)                  {data_->notes_text_edit_->load(text);}
    QString                     get_notes()                                     {return data_->notes_text_edit_->get_text();}
    void                        set_progress_text(const QString& text)          {data_->notes_progress_label_->setText(text);}
    QProgressBar*               get_progress_bar()                              {return data_->notes_progress_bar_;}
    bool                        check();    
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_RECORD_HISTORY_DIALOG_PANEL_H_ */
