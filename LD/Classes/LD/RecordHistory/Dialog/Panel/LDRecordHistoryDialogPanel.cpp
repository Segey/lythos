/* 
 * File:   RecordHistoryPanel.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#include "LDRecordHistoryDialogPanel.h"
//------------------------------------------------------------------------------
vLDRecordHistoryDialogPanel::vLDRecordHistoryDialogPanel(QWidget* parent/*=0*/)
            : inherited(parent), parent_(parent), data_(new data_name)
{
        vDllInstance::instance(this, data_.get());
        data_->translate();
        set_visible_progress_bar(false);
        instance_signals();
        inherited::setWindowTitle("[*]");
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialogPanel::instance_signals()
{
        connect(data_->date_edit_,             SIGNAL(editingFinished()), SLOT(enableForm()));
        connect(data_->weight_combobox_,       SIGNAL(currentIndexChanged(const QString &)), SLOT(enableForm()));
        connect(data_->repetitions_combobox_,  SIGNAL(currentIndexChanged(const QString &)), SLOT(enableForm()));
        connect(data_->notes_text_edit_,       SIGNAL(textChanged()), SLOT(enableForm()));
        connect(data_->weight_combobox_,       SIGNAL(editTextChanged ( const QString& )), SLOT(enableForm()));
        connect(data_->repetitions_combobox_,  SIGNAL(editTextChanged ( const QString& )), SLOT(enableForm()));
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialogPanel::enableForm()
{
        setWindowModified(true);
        emit(stateChanged());
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialogPanel::setWindowModified(bool value)
{
        inherited::setWindowModified(value);
        inherited::setTitle( go::string::get_string_modified(inherited::title(),value));
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogPanel::check()
{
        bool result = true;
        if(!vValidator::check_int(data_->weight_combobox_->currentText(), 0, data_->max_weight())) result = vInterfase::migalo(data_->weight_combobox_);
        if(!vValidator::check_int(data_->repetitions_combobox_->currentText(), 0, data_->max_repetitions())) result = vInterfase::migalo(data_->repetitions_combobox_);
        return result;
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialogPanel::test()
{
        Q_ASSERT(data_->cap_label_);
        Q_ASSERT(data_->date_label_);
        Q_ASSERT(data_->date_edit_);
        Q_ASSERT(data_->cap_record_label_);
        Q_ASSERT(data_->weight_label_);
        Q_ASSERT(data_->weight_combobox_);
        Q_ASSERT(data_->repetitions_label_);
        Q_ASSERT(data_->repetitions_combobox_);
        Q_ASSERT(data_->cap_notes_label_);
        Q_ASSERT(data_->notes_text_edit_);
        Q_ASSERT(data_->notes_progress_label_);
        Q_ASSERT(data_->notes_progress_bar_);

        Q_ASSERT(!data_->cap_label_->text().isEmpty());
        Q_ASSERT(!data_->date_label_->text().isEmpty());
        Q_ASSERT(!data_->cap_record_label_->text().isEmpty());
        Q_ASSERT(!data_->weight_label_->text().isEmpty());
        Q_ASSERT(!data_->repetitions_label_->text().isEmpty());
        Q_ASSERT(!data_->cap_notes_label_->text().isEmpty());
        Q_ASSERT(data_->notes_text_edit_->test());
        Q_ASSERT( (void*)data_->date_label_->buddy() == (void*)data_->date_edit_);
        Q_ASSERT( (void*)data_->weight_label_->buddy() == (void*)data_->weight_combobox_);
        Q_ASSERT( (void*)data_->repetitions_label_->buddy() == (void*)data_->repetitions_combobox_);

        data_->set_visible_progress_bar(false);
        Q_ASSERT(!data_->notes_progress_label_->isVisible());
        Q_ASSERT(!data_->notes_progress_bar_->isVisible());

        Q_ASSERT( check());
        data_->weight_combobox_->setItemText(0,"-55");
        data_->weight_combobox_->setCurrentIndex(0);
        Q_ASSERT( !check());
        data_->weight_combobox_->setItemText(0,"0");
        data_->weight_combobox_->setCurrentIndex(0);
        Q_ASSERT( check());
        data_->weight_combobox_->setItemText(0,"2001");
        data_->weight_combobox_->setCurrentIndex(0);
        Q_ASSERT( !check());
        data_->weight_combobox_->setItemText(0,"0");
        data_->weight_combobox_->setCurrentIndex(0);
        Q_ASSERT( check());

        data_->repetitions_combobox_->setItemText(0,"-5");
        data_->repetitions_combobox_->setCurrentIndex(0);
        Q_ASSERT( !check());
        data_->repetitions_combobox_->setItemText(0,"0");
        data_->repetitions_combobox_->setCurrentIndex(0);
        Q_ASSERT( check());
        data_->repetitions_combobox_->setItemText(0,"201");
        data_->repetitions_combobox_->setCurrentIndex(0);
        Q_ASSERT( !check());
        data_->repetitions_combobox_->setItemText(0,"0");
        data_->repetitions_combobox_->setCurrentIndex(0);
        Q_ASSERT( check());
        return true;
}
//------------------------------------------------------------------------------