// -*- C++ -*-
/* 
 * File:   LDRecordHistoryPanelData.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 9:27
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_RECORD_HISTORY_DIALOG_PANEL_DATA_H_
#define	_V_LD_RECORD_HISTORY_DIALOG_PANEL_DATA_H_
//------------------------------------------------------------------------------
struct vLDRecordHistoryDialogPanelData
{
    static QString                      name_dll()                              { return const_instance_libs::ldrhp022(); }
    static int                          max_weight()                            { return 2000; }
    static int                          max_repetitions()                       { return 200; }

    QLabel*                                                                     cap_label_;
    QLabel*                                                                     date_label_;
    QDateEdit*                                                                  date_edit_;
    QLabel*                                                                     cap_record_label_;
    QLabel*                                                                     weight_label_;
    QComboBox*                                                                  weight_combobox_;
    QLabel*                                                                     repetitions_label_;
    QComboBox*                                                                  repetitions_combobox_;
    QLabel*                                                                     cap_notes_label_;
    vDockRichEdit*                                                              notes_text_edit_;
    QLabel*                                                                     notes_progress_label_;
    QProgressBar*                                                               notes_progress_bar_;

    explicit vLDRecordHistoryDialogPanelData() : notes_text_edit_(new vDockRichEdit)  { }

    void translate()
    {
        cap_label_->setText         (vtr::Record::tr("Enter the Date of the Record"));
        date_label_->setText        (vtr::Date::tr("&Date:"));
        cap_record_label_->setText  (vtr::Record::tr("Enter Record"));
        weight_label_->setText      (vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        repetitions_label_->setText (vtr::Training::tr("&Repetitions:"));
        cap_notes_label_->setText   (vtr::Record::tr("Enter Record Notes"));
    }

    void set_visible_progress_bar(bool visible)
    {
        notes_progress_label_->setVisible(visible);
        notes_progress_bar_->setVisible(visible);
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_RECORD_HISTORY_DIALOG_PANEL_DATA_H_ */

