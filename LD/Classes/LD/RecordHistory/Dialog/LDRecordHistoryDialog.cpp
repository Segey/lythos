/* 
 * File:   PersonalInfo.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#include "LDRecordHistoryDialog.h"
//------------------------------------------------------------------------------
vLDRecordHistoryDialog::vLDRecordHistoryDialog(QWidget* parent/*=0*/)
         : QDialog(parent, Qt::WindowTitleHint), data_(new data_name)
{
        vDllInstance::instance(this, data_.get());
        instance_form();
        instance_signals();
        set_enabled(false);
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::instance_form()
{
        data_->translate();
        vDialog::window_title(this);
        settings_read();
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::settings_read()
{
        QRect rect_default =  vDialog::form_size_calculation(this);
        QRect rect = vSettings::get(settings_table(), settings::make_form_size(settings_key(), rect_default)).get<1>();
        inherited::setGeometry(rect);
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::settings_write() const
{
        vSettings::set(settings_table(), settings::make_form_size(settings_key(), geometry()));
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::instance_signals()
{
        connect(data_->cancel_button_,  SIGNAL(clicked()), SLOT(close()));
        connect(data_->update_button_,  SIGNAL(clicked()), SLOT(save()));
        connect(data_->left_panel_,     SIGNAL(stateChanged()), SLOT(leftPanelStateChanged()));
        connect(data_->right_panel_,    SIGNAL(stateChanged()), SLOT(RightPanelStateChanged()));
        connect(data_->exercise_edit_,  SIGNAL(textChanged(const QString &)), SLOT(exerciseEditStateChanged()));
}
//------------------------------------------------------------------------------
/*virtual*/ void vLDRecordHistoryDialog::closeEvent(QCloseEvent *event)
{       
        if(!isWindowModified()) return;

        QMessageBox::StandardButton  click_button =QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(click_button == QMessageBox::Cancel) event->ignore();
        if(click_button == QMessageBox::Save) save();
}
//------------------------------------------------------------------------------
/*slot*/void vLDRecordHistoryDialog::exerciseEditStateChanged()
{
        inherited::setWindowModified(true);
        data_->update_button_->setEnabled(true);
}
//------------------------------------------------------------------------------
/*slot*/void vLDRecordHistoryDialog::leftPanelStateChanged()
{
        inherited::setWindowModified(true);
        data_->left_panel_->setWindowModified(true);
        data_->update_button_->setEnabled(true);
}
//------------------------------------------------------------------------------
/*slot*/void vLDRecordHistoryDialog::RightPanelStateChanged()
{
        inherited::setWindowModified(true);
        data_->right_panel_->setWindowModified(true);
        data_->update_button_->setEnabled(true);
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::set_enabled(bool value)
{
        inherited::setWindowModified(value);
        data_->left_panel_->setWindowModified(value);
        data_->right_panel_->setWindowModified(value);
        data_->update_button_->setEnabled(value);
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::check()
{
        bool result = data_->exercise_edit_->text().isEmpty() ? vInterfase::migalo(data_->exercise_edit_) : true;
        if ( !data_->left_panel_->check() ||  !data_->right_panel_->check() ) result =false;
        return result;
}
//------------------------------------------------------------------------------
void vLDRecordHistoryDialog::save()
{
        if(!inherited::isWindowModified() || !check() || !do_save() ) return;
        OVERRIDE_CURSOR(all_calculate());
        QMessageBox::information(this, program::name_full(), vtr::Message::tr("Your data are successfully updated!"), QMessageBox::Ok);
        accept();
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::load(const QString& exercise)
{
        bool is_good_load= do_load(exercise);
        set_enabled(false);
        if (is_good_load) all_calculate();
        return is_good_load;
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::all_calculate()
{
        bool is_correct_calculate = calculate_progress() && calculate_remains_days();
        data_->left_panel_->set_visible_progress_bar(is_correct_calculate);
        data_->right_panel_->set_visible_progress_bar(is_correct_calculate);
        return is_correct_calculate;
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::calculate_progress()
{
        if(data_->right_panel_->get_weight() == 0 || data_->right_panel_->get_weight() < data_->left_panel_->get_weight()) return false;

        double  count =data_->left_panel_->get_weight() * 100.0 / data_->right_panel_->get_weight();
        data_->left_panel_->set_progress_text(vtr::Date::tr("Progress:"));
        data_->left_panel_->get_progress_bar()->setValue(count);
        data_->left_panel_->get_progress_bar()->setFormat(QString(" %1%").arg( count >10.0 ? QString::number(count,'g',5) : QString::number(count,'g',4)));
        return true;
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::calculate_remains_days()
{     
        int count_days =data_->left_panel_->get_date().daysTo(data_->right_panel_->get_date());
        if (count_days ==0) return false;
        
        data_->right_panel_->set_progress_text(vtr::Date::tr("Period left:"));
        data_->right_panel_->get_progress_bar()->setMaximum(round_to_multiple_ten(count_days));
        data_->right_panel_->get_progress_bar()->setValue(count_days);
        data_->right_panel_->get_progress_bar()->setFormat(vtr::Date::tr(" %1 day(days)").arg(count_days));
        return true;
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryDialog::test()
{
        Q_ASSERT(data_->username_icon_label_);
        Q_ASSERT(data_->username_label_);
        Q_ASSERT(data_->cap_label_);
        Q_ASSERT(data_->exercise_label_);
        Q_ASSERT(data_->exercise_edit_);
        Q_ASSERT(data_->exercise_button_);
        Q_ASSERT(data_->left_panel_);
        Q_ASSERT(data_->right_panel_);
        Q_ASSERT(data_->update_button_);
        Q_ASSERT(data_->cancel_button_);

        Q_ASSERT(!data_->username_icon_label_->pixmap()->isNull());
        Q_ASSERT(!data_->username_label_->text().isEmpty());
        Q_ASSERT(!data_->cap_label_->text().isEmpty());
        Q_ASSERT(!data_->exercise_label_->text().isEmpty());
        Q_ASSERT(!data_->exercise_button_->text().isEmpty());
        Q_ASSERT(!data_->update_button_->text().isEmpty());
        Q_ASSERT(!data_->cancel_button_->text().isEmpty());

        data_->right_panel_->set_weight("0");
        Q_ASSERT( !calculate_progress() );
        Q_ASSERT( !calculate_remains_days() );

        data_->right_panel_->set_weight("100");
        data_->left_panel_->set_weight("10");
        Q_ASSERT( calculate_progress() );

        data_->right_panel_->set_weight("10");
        data_->left_panel_->set_weight("10");
        Q_ASSERT( calculate_progress() );

        data_->right_panel_->set_date(QDate::currentDate());
        data_->left_panel_->set_date(QDate::currentDate());
        Q_ASSERT( !calculate_remains_days() );

        QDate date1(1999,1,1);
        QDate date2(1999,11,11);
        data_->right_panel_->set_date(date1);
        data_->left_panel_->set_date(date2);
        Q_ASSERT( calculate_remains_days() );

        Q_ASSERT( (void*)data_->exercise_label_->buddy() == (void*)data_->exercise_edit_);

        vMain::user().set_test_id();
        QRect rect(10,17,30,220);
        Q_ASSERT( vSettings::set(settings_table(), settings::make_form_size(settings_key(), rect)) );
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<0>() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<1>() == rect );

        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------