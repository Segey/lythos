// -*- C++ -*-
/* 
 * File:   RecordHistoryData.h
 * Author: S.Panin
 *
 * Created on 19 Январь 2010 г., 13:04
 */
//------------------------------------------------------------------------------
#ifndef _V_RECOR_DHISTORY_DATA_H_
#define	_V_RECOR_DHISTORY_DATA_H_
//------------------------------------------------------------------------------
struct vRecordHistoryData
{
    static QString                      name_dll()                              { return const_instance_libs::ldrh300(); }
    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    vLDRecordHistoryTreeView*                                                   tree_view_;

    explicit vRecordHistoryData() :  tree_view_(new vLDRecordHistoryTreeView)   {}

    void translate()
    {
        username_icon_label_    ->setPixmap(const_pixmap_64::record_history());
        username_label_         ->setText(vtr::Record::tr("Personal Records History"));
        cap_label_              ->setText(vtr::Record::tr("Personal Records"));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_RECOR_DHISTORY_DATA_H_ */

