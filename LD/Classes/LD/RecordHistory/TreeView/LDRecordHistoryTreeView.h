/* 
 * File:   LDRecordHistoryTreeView.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_RECORD_HISTORY_TREE_VIEW_H_
#define	_V_LD_RECORD_HISTORY_TREE_VIEW_H_
//------------------------------------------------------------------------------
#include "../../../Main/vMain.h"
#include "../../../../../Go/BuiltType/TreeWidget.h"
#include "../../../../../Go/go/containers/algorithm.h"
#include "../../../../../Go/Classes/BD/Settings/Settings.h"
#include "../../../../../Go/Component/Tree/TreeView/TreeView.h"
#include "../Dialog/Successors/LDRecordHistoryDialogSuccessors.h"
//------------------------------------------------------------------------------
class vLDRecordHistoryTreeView : public vTreeView
{
    typedef                     vTreeView                                       inherited;
    typedef                     vLDRecordHistoryTreeView                        class_name;

    static QString              settings_table()                                { return "RecordHistory"; }
    static QString              settings_key()                                  { return "HeaderList"; }
    void                        add_items(const QSqlQuery& query);
    void                        write_settings();

protected:
    virtual QString             do_get_header_base_column_name()                { return vtr::Exercise::tr("Exercise"); }
    virtual QStringList         do_get_header_labels_full()                     { return QStringList() << vtr::Exercise::tr("Exercise") << vtr::Record::tr("Record Date") << vtr::Record::tr("Record Weight (%1):").arg(vMain::user().locale().weight()->kgs()) << vtr::Record::tr("Record Repetitions") << vtr::Record::tr("Record Notes"); }
    virtual QStringList         do_get_header_labels_default()                  { return QStringList() << vtr::Exercise::tr("Exercise") << vtr::Record::tr("Record Date") << vtr::Record::tr("Record Weight (%1):").arg(vMain::user().locale().weight()->kgs()) << vtr::Record::tr("Record Repetitions"); }
    virtual bool                do_load();
    virtual bool                do_record_new();
    virtual bool                do_record_open_with_item(const QString& name);
    virtual bool                do_record_delete(const QString& name);
    virtual bool                do_records_clear_all();
    virtual bool                do_settings_read();
    
public:
    explicit                    vLDRecordHistoryTreeView(QWidget* parent =0) : inherited(parent) {}
                                ~vLDRecordHistoryTreeView()                     {write_settings();}
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_RECORD_HISTORY_TREE_VIEW_H_ */
