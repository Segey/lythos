/* 
 * File:   LDRecordHistoryTreeView.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#include "LDRecordHistoryTreeView.h"
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_settings_read()
{
        QStringList list = vSettings::get(settings_table(), settings::make_stringlist(settings_key(), QStringList() << "300" << "100" << "100" << "120")).get<1>();
        vbtTreeWidget::set_header_items_width_and_hidden(toTreeWidget(), list);
        return true;
}
//------------------------------------------------------------------------------
void vLDRecordHistoryTreeView::write_settings()
{
        QStringList items =vbtTreeWidget::get_header_items_widths(toTreeWidget());
        vSettings::set(settings_table(), settings::make_stringlist(settings_key(), items));
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_record_new()
{
        vLDRecordHistoryDialogSuccessorsNew dialog;
        return dialog.exec() == QDialog::Accepted;
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_record_open_with_item(const QString& name)
{
        vLDRecordHistoryDialogSuccessorsOpen dialog;
        dialog.load(name);
        return dialog.exec() == QDialog::Accepted;
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_record_delete(const QString& name)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("DELETE FROM RecordHistory WHERE (UserID =? AND Exercise =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(name);
        return query.exec();
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_records_clear_all()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("DELETE FROM RecordHistory WHERE UserID =?;");
        query.addBindValue(vMain::user().id());
        return query.exec();
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDRecordHistoryTreeView::do_load()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT Exercise, RecordDate, Weight, Repetitions, Note FROM RecordHistory WHERE (UserID =? AND Ghost =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(false);
        if (query.exec() ==false) return false;
        while(query.next()) add_items(query);
        return true;
}
//------------------------------------------------------------------------------
void vLDRecordHistoryTreeView::add_items(const QSqlQuery& query)
{
        boost::shared_ptr<QTextEdit> text_edit(new QTextEdit);

        QTreeWidgetItem* item = new QTreeWidgetItem(inherited::toTreeWidget());
        item->setText(0, query.value(0).toString());
        item->setText(1, query.value(1).toString());
        item->setText(2, query.value(2).toString());
        item->setText(3, query.value(3).toString());

        text_edit->setHtml(query.value(4).toString());
        item->setText(4, vDLL<QString>( const_libs::st001ng(), "s_truncate")(text_edit->toPlainText(), text_edit->toPlainText(), 127));
        inherited::toTreeWidget()->setItemExpanded(item, true);
}
//------------------------------------------------------------------------------
bool vLDRecordHistoryTreeView::test()
{
        inherited::test();
        QStringList  list_full =do_get_header_labels_full();
        QStringList  list_default =do_get_header_labels_default();
        Q_ASSERT(go::is(list_full.begin(), list_full.end(), list_default.begin(), list_default.end() ));
        Q_ASSERT(do_get_header_base_column_name()  == vtr::Exercise::tr("Exercise"));

        vMain::user().set_test_id();

        QStringList list_set_width = QStringList() << "900" <<"78" <<"977";
        Q_ASSERT(vSettings::set(settings_table(), settings::make_stringlist(settings_key(), list_set_width)));

        Q_ASSERT(vSettings::get(settings_table(), settings::make_stringlist(settings_key())).get<0>());
        QStringList list_get_width = vSettings::get(settings_table(), settings::make_stringlist(settings_key())).get<1>();
        Q_ASSERT(list_set_width == list_get_width);

        QSqlQuery query(vMain::settingsbd().database());
        return query.exec("DELETE FROM RecordHistory WHERE UserID =0");
}
//------------------------------------------------------------------------------