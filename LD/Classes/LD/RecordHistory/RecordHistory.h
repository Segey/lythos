/* 
 * File:   RecordHistory.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#ifndef _V_RECORD_HISTORY_H_
#define	_V_RECORD_HISTORY_H_
//------------------------------------------------------------------------------
#include "../../../../Go/Successor/SuccessorsChange.h"
#include "TreeView/LDRecordHistoryTreeView.h"
#include "RecordHistoryData.h"
//------------------------------------------------------------------------------
class vRecordHistory : public QWidget , public vSuccessorsChange
{
    typedef                     QWidget                                         inherited;
    typedef                     vRecordHistory                                  class_name;
    typedef                     vRecordHistoryData                              data_name;

    vClientLDDialog*                                                            parent_;
    boost::shared_ptr<data_name>                                                data_;


    virtual void                        do_change_state()                       { data_->tree_view_->setEnabled(true); }
    virtual QWidget*                    do_toWidget()                           {return this;}
    virtual bool                        do_check()                              {return true;}
    virtual bool                        do_save()                               {return true;}
    virtual bool                        do_load()                               {return data_->tree_view_->load();}

public:
    explicit                            vRecordHistory(vClientLDDialog* owner);
    static vRecordHistory*              make_new(vClientLDDialog* parent);
    static vRecordHistory*              make_exists(vClientLDDialog* parent);
    virtual bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_RECORD_HISTORY_H_ */
