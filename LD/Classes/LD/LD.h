/* 
 * File:   vLD.h
 * Author: S.Panin
 *
 * Created on 13 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_H_
#define	_V_LD_H_
//------------------------------------------------------------------------------
#include "ClientLD/ClientLDDialog.h"
#include "Options/LDOptions.h"
#include "../../../Go/Component/Panel/Presentation/PanelPresentation.h"
#include "../../../Go/Successor/SuccessorsChange.h"
#include "PhotoGallery/PhotoGallery.h"
#include "PersonalInfo/LDPersonalInfo.h"
#include "RecordHistory/RecordHistory.h"
#include "MeasurementHistory/LDMeasurementHistory.h"
//------------------------------------------------------------------------------
class vClientLDDialog;
//------------------------------------------------------------------------------
class vLD : public vPanelPresentation
{
Q_OBJECT
    typedef                         vPanelPresentation                          inherited;
    typedef                         vSuccessorsChange                           inherited_change;
    typedef                         vLD                                         class_name;

    vClientLDDialog*                                                            parent_;
    boost::shared_ptr<inherited_change>                                         ld_;
    int                                                                         page_index_;

    vLD(vClientLDDialog* parent) : parent_(parent) {leaveEvent(0);}

protected:
    virtual void                    enterEvent(QEvent* event);
    virtual void                    leaveEvent(QEvent* event);
    virtual void                    mousePressEvent(QMouseEvent* event);

public:                                   
    void                            setPageIndex(int page_index)                {page_index_ = page_index;}
    QWidget*                        toWidget()                                  {return ld_->toWidget();}
    bool                            check()                                     {return ld_->check();}
    bool                            load()                                      {return ld_->load();}
    bool                            save()                                      {return ld_->save();}
    void                            change_state()                              {ld_->change_state();}
    static void                     create_PhotoGallery(vLD* result);
    static void                     create_RecordHistory(vLD* result);
    static void                     create_MeasurementHistory(vLD* result);
    static void                     create_Options(vLD* result);
    static vLD*                     make_PersonalInfo(vClientLDDialog* parent, bool is_new_user);
    static vLD*                     make_NewPhotoGallery(vClientLDDialog* parent);
    static vLD*                     make_ExistsPhotoGallery(vClientLDDialog* parent);
    static vLD*                     make_NewRecordHistory(vClientLDDialog* parent);
    static vLD*                     make_ExistsRecordHistory(vClientLDDialog* parent);
    static vLD*                     make_NewMeasurementHistory(vClientLDDialog* parent);
    static vLD*                     make_ExistsMeasurementHistory(vClientLDDialog* parent);
    static vLD*                     make_NewOptions(vClientLDDialog* parent);
    static vLD*                     make_ExistsOptions(vClientLDDialog* parent);
};
//------------------------------------------------------------------------------
#endif	/* _VLD_H_ */
