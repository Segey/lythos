/* 
 * File:   LDPersonalInfoStateUserExist.h
 * Author: S.Panin
 *
 * Created on 27 Июль 2009 г., 23:43
 */
//------------------------------------------------------------------------------
#ifndef _V_PERSONAL_INFO_STATE_H_
#define	_V_PERSONAL_INFO_STATE_H_
//------------------------------------------------------------------------------
#include "LDState.h"
//------------------------------------------------------------------------------
class vLDPersonalInfo;
//------------------------------------------------------------------------------
class vLDPersonalInfoStateUserExist : public vLDState<vLDPersonalInfo>
{
    typedef                         vLDPersonalInfoStateUserExist               class_name;
    typedef                         vLDState<vLDPersonalInfo>                   inherited;

    virtual bool                    do_save(vLDPersonalInfo* parent);
    virtual bool                    do_load(vLDPersonalInfo* parent);
    virtual bool                    do_check(vLDPersonalInfo* parent);

public:
    static class_name*              instance()                                  {return new class_name;}
    virtual  bool                   test(vLDPersonalInfo* parent);
};
//------------------------------------------------------------------------------
class vLDPersonalInfoStateUserNew : public vLDState<vLDPersonalInfo>
{
    typedef                         vLDPersonalInfoStateUserNew                 class_name;
    typedef                         vLDState<vLDPersonalInfo>                   inherited;

    virtual bool                    do_save(vLDPersonalInfo* parent);
    virtual bool                    do_load(vLDPersonalInfo*)                   { return true;}
    virtual bool                    do_check(vLDPersonalInfo* parent);

public:
    static class_name*              instance()                                  {return new class_name; }
    virtual  bool                   test(vLDPersonalInfo* parent);
};
//------------------------------------------------------------------------------
#endif	/* _V_PERSONAL_INFORMATION_STATE_H_ */
