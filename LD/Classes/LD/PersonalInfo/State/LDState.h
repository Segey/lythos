/* 
 * File:   LDState.h
 * Author: S.Panin
 *
 * Created on 28 Июль 2009 г., 12:40
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_STATE_H
#define	_V_LD_STATE_H
//------------------------------------------------------------------------------
template<typename T>
class vLDState
{
    typedef                         vLDState                                    class_name;

protected:
    void                            change_state(T* parent, vLDState* state)    {parent->change_state(state);}
    virtual bool                    do_save(T* parent)                          =0;
    virtual bool                    do_load(T*  parent)                         =0;
    virtual bool                    do_check(T* parent)                         =0;
public:
    virtual bool                    save(T* parent)                             { return do_save(parent); }
    virtual bool                    load(T*  parent)                            { return do_load(parent); }
    virtual bool                    check(T* parent)                            { return do_check(parent); }
    virtual bool                    test(T* parent)                             =0;
};
//------------------------------------------------------------------------------
#endif	/* _V_LDSTATE_H */
