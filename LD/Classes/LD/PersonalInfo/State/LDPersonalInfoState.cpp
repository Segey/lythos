/* 
 * File:   LDState.cpp
 * Author: S.Panin
 * 
 * Created on 27 Июль 2009 г., 23:43
 */
//------------------------------------------------------------------------------
#include "LDPersonalInfoState.h"
//------------------------------------------------------------------------------
/*virtual*/bool vLDPersonalInfoStateUserExist::do_check(vLDPersonalInfo* parent)
{
        if(v_user::exists(parent->data_->name_edit_->text()) && (vMain::user().name() != parent->data_->name_edit_->text()))
        return !QMessageBox::warning(0, program::name_full(), vtr::Message::tr("The user with a name '%1' already exists!").arg(parent->data_->name_edit_->text()), QMessageBox::Ok);
        else return true;
}
//------------------------------------------------------------------------------
/*virtual*/bool vLDPersonalInfoStateUserExist::do_save(vLDPersonalInfo* parent)
{
        if ( !vMain::user().profile_update( parent->data_->name_edit_->text(), parent->data_->password_edit_->text()) ) return false;
        
        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE PersonalInformation SET  Sex=?, DateofBirth=?, Height =? WHERE UserID =?;");
        query.addBindValue( parent->data_->sex_combobox_->currentIndex() );
        query.addBindValue( parent->data_->date_of_birth_edit_->date() );
        query.addBindValue( parent->data_->height_edit_->text() );
        query.addBindValue( vMain::user().id() );
        return query.exec();  
}
 //------------------------------------------------------------------------------
/*virtual*/ bool vLDPersonalInfoStateUserExist::do_load(vLDPersonalInfo* parent)
{
        parent->data_->name_edit_->setText(vMain::user().name());
        parent->data_->password_edit_->setText(vMain::user().password());
        parent->data_->re_password_edit_->setText(parent->data_->password_edit_->text());

        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT Sex, DateofBirth, Height, Country, Region, City,  Address, EMail, ICQ, Phone, Mobule, Doctor, Instructor, Image FROM PersonalInformation WHERE UserID =?;");
        query.addBindValue( vMain::user().id() );
        if ( !query.exec()) return false;
        query.next();
        parent->data_->sex_combobox_->setCurrentIndex   (query.value(0).toInt());
        parent->data_->date_of_birth_edit_->setDate     (query.value(1).toDate());
        parent->data_->height_edit_->setText            (query.value(2).toString());
        parent->data_->country_edit_->setText           (query.value(3).toString());
        parent->data_->state_region_edit_->setText      (query.value(4).toString());
        parent->data_->city_town_edit_->setText         (query.value(5).toString());
        parent->data_->address_edit_->setText           (query.value(6).toString());
        parent->data_->email_edit_->setText             (query.value(7).toString());
        parent->data_->icq_edit_->setText               (query.value(8).toString());
        parent->data_->phone_edit_->setText             (query.value(9).toString());
        parent->data_->mobule_edit_->setText            (query.value(10).toString());
        parent->data_->doctor_edit_->setText            (query.value(11).toString());
        parent->data_->instructor_edit_->setText        (query.value(12).toString());      

        QPixmap pictureFromDb; pictureFromDb.loadFromData(query.value(13).toByteArray());
        parent->data_->caption_icon_image_panel_->set_pixmap(pictureFromDb);
        return true;
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDPersonalInfoStateUserExist::test(vLDPersonalInfo* parent)
{

        vMain::user().set_test_id();
        QSqlQuery query(vMain::basebd().database());
        query.exec("INSERT INTO PersonalInformation (UserID) VALUES (0)");

        parent->data_->sex_combobox_->setCurrentIndex(1);
        parent->data_->date_of_birth_edit_->setDate(QDate::currentDate());
        parent->data_->height_edit_->setText("180");
        do_save(parent);
        parent->data_->sex_combobox_->setCurrentIndex(0);
        parent->data_->date_of_birth_edit_->setDate( QDate(1980,12,12) );
        parent->data_->height_edit_->setText("");
        do_load(parent);

        Q_ASSERT( parent->data_->sex_combobox_->currentIndex() == 1);
        Q_ASSERT( parent->data_->date_of_birth_edit_->date() == QDate::currentDate());
        Q_ASSERT( parent->data_->height_edit_->text() == "180");
        return query.exec("DELETE FROM PersonalInformation WHERE UserID =0");
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*virtual*/bool vLDPersonalInfoStateUserNew::do_check(vLDPersonalInfo* parent)
{
        if(! v_user::exists(parent->data_->name_edit_->text())) return true;
        return !QMessageBox::warning(0, program::name_full(), vtr::Message::tr("The user with a name '%1' already exists!").arg(parent->data_->name_edit_->text()), QMessageBox::Ok);
}
//------------------------------------------------------------------------------
/*virtual*/bool vLDPersonalInfoStateUserNew::do_save(vLDPersonalInfo* parent)
{
        int id = v_user::create_new(parent->data_->name_edit_->text(), parent->data_->password_edit_->text());
        if (0 ==id) return false;
        
        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO PersonalInformation (UserID, Sex, DateofBirth, Height) VALUES (?, ?, ?, ?);");
        query.addBindValue( id );
        query.addBindValue( parent->data_->sex_combobox_->currentIndex() );
        query.addBindValue( parent->data_->date_of_birth_edit_->date() );
        query.addBindValue( parent->data_->height_edit_->text() );
        if (!query.exec()) return false;

        vMain::user().load(parent->data_->name_edit_->text());
        parent->change_state(vLDPersonalInfoStateUserExist::instance());
        return true;
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDPersonalInfoStateUserNew::test(vLDPersonalInfo* parent)
{

        parent->data_->name_edit_->setText(">>?xxx?");
        parent->data_->password_edit_->setText("pass");

        parent->data_->sex_combobox_->setCurrentIndex(1);
        parent->data_->date_of_birth_edit_->setDate(QDate::currentDate());
        parent->data_->height_edit_->setText("190");

        do_save(parent);
        parent->data_->sex_combobox_->setCurrentIndex(0);
        parent->data_->date_of_birth_edit_->setDate( QDate(1980,12,12) );
        parent->data_->height_edit_->setText("");

        boost::shared_ptr<vLDPersonalInfoStateUserExist> state (vLDPersonalInfoStateUserExist::instance());
        state->load(parent);
        Q_ASSERT( parent->data_->sex_combobox_->currentIndex() == 1);
        Q_ASSERT( parent->data_->date_of_birth_edit_->date() == QDate::currentDate());
        Q_ASSERT( parent->data_->height_edit_->text() == "190");

        QSqlQuery query(vMain::basebd().database());
        return query.exec("DELETE FROM SignUp WHERE UserName ='>>?xxx?';");
}