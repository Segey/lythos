// -*- C++ -*-
/* 
 * File:   PersonalInfoData.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 21:42
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_PERSONAL_INFO_DATA_H_
#define	_V_LD_PERSONAL_INFO_DATA_H_
//------------------------------------------------------------------------------
struct vLDPersonalInfoData
{
    static QString                      name_dll()                              { return const_instance_libs::ldpi99(); }
    
    QLabel*                                                                     caption_label_;
    vWidgetImage*                                                               caption_icon_image_panel_;

    QLabel*                                                                     cap_contact_label_;
    QLabel*                                                                     country_label_;
    QLineEdit*                                                                  country_edit_;
    QLabel*                                                                     state_region_label_;
    QLineEdit*                                                                  state_region_edit_;
    QLabel*                                                                     city_town_label_;
    QLineEdit*                                                                  city_town_edit_;
    QLabel*                                                                     address_label_;
    QLineEdit*                                                                  address_edit_;
    QLabel*                                                                     email_label_;
    QLineEdit*                                                                  email_edit_;
    QLabel*                                                                     icq_label_;
    QLineEdit*                                                                  icq_edit_;
    QLabel*                                                                     phone_label_;
    QLineEdit*                                                                  phone_edit_;
    QLabel*                                                                     mobule_label_;
    QLineEdit*                                                                  mobule_edit_;
    QLabel*                                                                     cap_personal_label_;
    QLabel*                                                                     name_label_;
    QLineEdit*                                                                  name_edit_;
    QLabel*                                                                     password_label_;
    QLineEdit*                                                                  password_edit_;
    QLabel*                                                                     re_password_label_;
    QLineEdit*                                                                  re_password_edit_;
    QLabel*                                                                     sex_label_;
    QComboBox*                                                                  sex_combobox_;
    QLabel*                                                                     date_of_birth_label_;
    QDateEdit*                                                                  date_of_birth_edit_;
    QLabel*                                                                     height_label_;
    QLineEdit*                                                                  height_edit_;

    QLabel*                                                                     cap_addition_label_;
    QLabel*                                                                     doctor_label_;
    QLineEdit*                                                                  doctor_edit_;
    QLabel*                                                                     instructor_label_;
    QLineEdit*                                                                  instructor_edit_;
    vLDPersonalInfoData() : caption_icon_image_panel_( new vWidgetImage) {}

    void translate()
    {
        caption_label_      ->setText(vtr::LD::tr("Personal information"));
        cap_contact_label_  ->setText(vtr::LD::tr("Contact info"));
        country_label_      ->setText(vtr::LD::tr("&Country:"));
        state_region_label_ ->setText(vtr::LD::tr("State/Re&gion:"));
        city_town_label_    ->setText(vtr::LD::tr("C&ity/Town:"));
        address_label_      ->setText(vtr::LD::tr("&Address:"));
        email_label_        ->setText(vtr::LD::tr("&E-mail:"));
        icq_label_          ->setText(vtr::LD::tr("IC&Q:"));
        phone_label_        ->setText(vtr::LD::tr("Phone n&umber:"));
        mobule_label_       ->setText(vtr::LD::tr("&Mobule:"));
        sex_combobox_       ->addItems(QStringList() << vtr::Body::tr("Male") << vtr::Body::tr("Female"));
        cap_personal_label_ ->setText(vtr::LD::tr("Personal info"));
        name_label_         ->setText(vtr::Dialog::tr("&Name:"));
        password_label_     ->setText(vtr::Dialog::tr("&Password:"));
        re_password_label_  ->setText(vtr::Dialog::tr("&Re-enter password:"));
        sex_label_          ->setText(vtr::Body::tr("Se&x:"));
        date_of_birth_label_->setText(vtr::Date::tr("&Date of Birth:"));
        height_label_       ->setText(vtr::Body::tr("&Height (%1):").arg(vMain::user().locale().height()->cms()));
        cap_addition_label_ ->setText(vtr::LD::tr("Additional info"));
        doctor_label_       ->setText(vtr::LD::tr("Doc&tor:"));
        instructor_label_   ->setText(vtr::LD::tr("Instruct&or:"));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_PERSONAL_INFO_DATA_H_ */

