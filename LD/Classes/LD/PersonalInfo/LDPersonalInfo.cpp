/* 
 * File:   PersonalInfo.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#include "LDPersonalInfo.h"
//------------------------------------------------------------------------------
vLDPersonalInfo::vLDPersonalInfo(vClientLDDialog* parent, bool is_new_user)
       : parent_(parent), data_(new data_name)
{        
        is_new_user ? make_user_new() : make_user_exists();
        vDllInstance::instance(this, data_.get());        
        data_->translate();
        instance_signals();
        
        inherited::setWindowTitle("[*]");
        inherited::setWindowModified(false);
}
//------------------------------------------------------------------------------
void vLDPersonalInfo::instance_signals()
{
        connect(data_->country_edit_,           SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->state_region_edit_,      SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->city_town_edit_,         SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->address_edit_,           SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->email_edit_,             SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->icq_edit_,               SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->phone_edit_,             SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->mobule_edit_,            SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->sex_combobox_,           SIGNAL(currentIndexChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->name_edit_,              SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->password_edit_,          SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->re_password_edit_,       SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->date_of_birth_edit_,     SIGNAL(editingFinished()), SLOT(enableSaveButton()));
        connect(data_->height_edit_,            SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->doctor_edit_,            SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));
        connect(data_->instructor_edit_,        SIGNAL(textChanged(const QString &)), SLOT(enableSaveButton()));

        data_->caption_icon_image_panel_->connect_clear_image(boost::bind(&class_name::enableSaveButton, this));
        data_->caption_icon_image_panel_->connect_update_image(boost::bind(&class_name::enableSaveButton, this));
}
//------------------------------------------------------------------------------
void vLDPersonalInfo::enableSaveButton()
{
        inherited::setWindowModified(true);
        parent_->enableSaveButton();
}
//------------------------------------------------------------------------------
bool vLDPersonalInfo::do_check()
{
        bool result = true;
        if(data_->name_edit_->text().isEmpty())  result = vInterfase::migalo(data_->name_edit_);

        if(data_->password_edit_->text() != data_->re_password_edit_->text() || data_->password_edit_->text().isEmpty())
        {
                result =vInterfase::migalo(data_->password_edit_);
                vInterfase::migalo(data_->re_password_edit_);
        }
        return result ? state_->check(this) : false; 
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDPersonalInfo::save()
{
        if(data_->caption_icon_image_panel_->isWindowModified()) v_bd_image::add(vMain::basebd().database(), QString("UPDATE PersonalInformation SET Image =? WHERE UserID =?;"), data_->caption_icon_image_panel_->file_name(), vMain::user().id());

        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE PersonalInformation SET Country =?, Region =?, City=?, Address=?, EMail =?, ICQ =?, Phone =?, Mobule =?, Doctor=?, Instructor =? WHERE UserID =?;");
        query.addBindValue(data_->country_edit_->text());
        query.addBindValue(data_->state_region_edit_->text());
        query.addBindValue(data_->city_town_edit_->text());
        query.addBindValue(data_->address_edit_->text());
        query.addBindValue(data_->email_edit_->text());
        query.addBindValue(data_->icq_edit_->text());
        query.addBindValue(data_->phone_edit_->text());
        query.addBindValue(data_->mobule_edit_->text());
        query.addBindValue(data_->doctor_edit_->text());
        query.addBindValue(data_->instructor_edit_->text());
        query.addBindValue(vMain::user().id());
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDPersonalInfo::test()
{
        Q_ASSERT(data_->caption_label_);
        Q_ASSERT(data_->caption_icon_image_panel_);
        Q_ASSERT(!data_->caption_label_->text().isEmpty());
        Q_ASSERT(data_->caption_icon_image_panel_->test());
        Q_ASSERT(data_->cap_personal_label_);
        Q_ASSERT(data_->doctor_label_);
        Q_ASSERT(data_->doctor_edit_);
        Q_ASSERT(data_->instructor_label_);
        Q_ASSERT(data_->instructor_edit_);
        Q_ASSERT(!data_->cap_personal_label_->text().isEmpty());
        Q_ASSERT(!data_->doctor_label_->text().isEmpty());
        Q_ASSERT(!data_->instructor_label_->text().isEmpty());
        Q_ASSERT(data_->doctor_edit_->maxLength() ==128);
        Q_ASSERT(data_->instructor_edit_->maxLength() ==128);
        Q_ASSERT( (void*)data_->doctor_label_->buddy() == (void*)data_->doctor_edit_);
        Q_ASSERT( (void*)data_->instructor_label_->buddy() == (void*)data_->instructor_edit_);
        Q_ASSERT(data_->cap_contact_label_);
        Q_ASSERT(data_->country_label_);
        Q_ASSERT(data_->country_edit_);
        Q_ASSERT(data_->state_region_label_);
        Q_ASSERT(data_->state_region_edit_);
        Q_ASSERT(data_->city_town_label_);
        Q_ASSERT(data_->city_town_edit_);
        Q_ASSERT(data_->address_label_);
        Q_ASSERT(data_->address_edit_);
        Q_ASSERT(data_->email_label_);
        Q_ASSERT(data_->email_edit_);
        Q_ASSERT(data_->icq_label_);
        Q_ASSERT(data_->icq_edit_);
        Q_ASSERT(data_->phone_label_);
        Q_ASSERT(data_->phone_edit_);
        Q_ASSERT(data_->mobule_label_);
        Q_ASSERT(data_->mobule_edit_);
        Q_ASSERT(!data_->cap_contact_label_->text().isEmpty());
        Q_ASSERT(!data_->country_label_->text().isEmpty());
        Q_ASSERT(!data_->state_region_label_->text().isEmpty());
        Q_ASSERT(!data_->city_town_label_->text().isEmpty());
        Q_ASSERT(!data_->address_label_->text().isEmpty());
        Q_ASSERT(!data_->email_label_->text().isEmpty());
        Q_ASSERT(!data_->icq_label_->text().isEmpty());
        Q_ASSERT(!data_->phone_label_->text().isEmpty());
        Q_ASSERT(!data_->mobule_label_->text().isEmpty());
        Q_ASSERT(data_->country_edit_->maxLength() == 128);
        Q_ASSERT(data_->state_region_edit_->maxLength() == 128);
        Q_ASSERT(data_->city_town_edit_->maxLength() == 128);
        Q_ASSERT(data_->state_region_edit_->maxLength() == 128);
        Q_ASSERT(data_->address_edit_->maxLength() == 128);
        Q_ASSERT((void*)data_->country_label_->buddy() == (void*)data_->country_edit_);
        Q_ASSERT((void*)data_->state_region_label_->buddy() == (void*)data_->state_region_edit_);
        Q_ASSERT((void*)data_->city_town_label_->buddy() == (void*)data_->city_town_edit_);
        Q_ASSERT((void*)data_->address_label_->buddy() == (void*)data_->address_edit_);
        Q_ASSERT((void*)data_->email_label_->buddy() == (void*)data_->email_edit_);
        Q_ASSERT((void*)data_->icq_label_->buddy() == (void*)data_->icq_edit_);
        Q_ASSERT((void*)data_->phone_label_->buddy() == (void*)data_->phone_edit_);
        Q_ASSERT((void*)data_->mobule_label_->buddy() == (void*)data_->mobule_edit_);
        Q_ASSERT(data_->cap_addition_label_);
        Q_ASSERT(data_->name_label_);
        Q_ASSERT(data_->name_edit_);
        Q_ASSERT(data_->password_label_);
        Q_ASSERT(data_->password_edit_);
        Q_ASSERT(data_->re_password_label_);
        Q_ASSERT(data_->re_password_edit_);
        Q_ASSERT(data_->sex_label_);
        Q_ASSERT(data_->sex_combobox_);
        Q_ASSERT(data_->date_of_birth_label_);
        Q_ASSERT(data_->date_of_birth_edit_);
        Q_ASSERT(data_->height_label_);
        Q_ASSERT(data_->height_edit_);
        Q_ASSERT(!data_->cap_addition_label_->text().isEmpty());
        Q_ASSERT(!data_->name_label_->text().isEmpty());
        Q_ASSERT(!data_->password_label_->text().isEmpty());
        Q_ASSERT(!data_->re_password_label_->text().isEmpty());
        Q_ASSERT(!data_->sex_label_->text().isEmpty());
        Q_ASSERT(!data_->date_of_birth_label_->text().isEmpty());
        Q_ASSERT(!data_->height_label_->text().isEmpty());
        Q_ASSERT(data_->sex_combobox_->count() ==2);
        Q_ASSERT(data_->name_edit_->maxLength() ==128);
        Q_ASSERT(data_->password_edit_->maxLength() ==64);
        Q_ASSERT(data_->re_password_edit_->maxLength() ==64);
        Q_ASSERT(data_->height_edit_->maxLength() ==10);
        Q_ASSERT( (void*)data_->name_label_->buddy() == (void*)data_->name_edit_);
        Q_ASSERT( (void*)data_->password_label_->buddy() == (void*)data_->password_edit_);
        Q_ASSERT( (void*)data_->re_password_label_->buddy() == (void*)data_->re_password_edit_);
        Q_ASSERT( (void*)data_->sex_label_->buddy() == (void*)data_->sex_combobox_);
        Q_ASSERT( (void*)data_->date_of_birth_label_->buddy() == (void*)data_->date_of_birth_edit_);
        Q_ASSERT( (void*)data_->height_label_->buddy() == (void*)data_->height_edit_);

        change_state(vLDPersonalInfoStateUserExist::instance());
        vMain::user().set_test_id();
        QSqlQuery query(vMain::basebd().database());
        query.exec("INSERT INTO PersonalInformation (UserID) VALUES (0)");
        data_->doctor_edit_->setText("Sector");
        data_->instructor_edit_->setText("Instructor Grigorich");
        data_->country_edit_->setText("Russia");
        data_->state_region_edit_->setText("Penza str.");
        data_->city_town_edit_->setText("Kuznetsk");
        data_->address_edit_->setText("Pravda street");
        data_->email_edit_->setText("lythos@lythos.com");
        data_->icq_edit_->setText("77777777");
        data_->phone_edit_->setText("77777777888");
        data_->mobule_edit_->setText("77777777890");
        do_save();
        data_->doctor_edit_->setText("");
        data_->instructor_edit_->setText("");
        data_->country_edit_->setText("");
        data_->state_region_edit_->setText("");
        data_->city_town_edit_->setText("");
        data_->address_edit_->setText("");
        data_->email_edit_->setText("");
        data_->icq_edit_->setText("");
        data_->phone_edit_->setText("");
        data_->mobule_edit_->setText("");
        do_load();
        Q_ASSERT(data_->doctor_edit_->text() == "Sector");
        Q_ASSERT(data_->instructor_edit_->text() =="Instructor Grigorich");
        Q_ASSERT( data_->country_edit_->text() == "Russia");
        Q_ASSERT( data_->state_region_edit_->text() == "Penza str.");
        Q_ASSERT( data_->city_town_edit_->text() == "Kuznetsk");
        Q_ASSERT( data_->address_edit_->text() == "Pravda street");
        Q_ASSERT( data_->email_edit_->text() == "lythos@lythos.com");
        Q_ASSERT( data_->icq_edit_->text() == "77777777");
        Q_ASSERT( data_->phone_edit_->text() == "77777777888");
        Q_ASSERT( data_->mobule_edit_->text() == "77777777890");
        query.exec("DELETE FROM PersonalInformation WHERE UserID =0");

       Q_ASSERT(!do_check());
        data_->name_edit_->setText("Sos");
        Q_ASSERT(!do_check());
        data_->password_edit_->setText("Sisa");
        data_->re_password_edit_->setText("Sisa");
        Q_ASSERT(do_check());
        data_->re_password_edit_->setText("Sisaa");
        Q_ASSERT(!do_check());
       
        change_state(vLDPersonalInfoStateUserNew::instance());
        state_->test(this);
        change_state(vLDPersonalInfoStateUserExist::instance());
        state_->test(this);
        return true;
}
//------------------------------------------------------------------------------