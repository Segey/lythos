/* 
 * File:   PersonalInfo.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_PERSONAL_INFO_H_
#define	_V_LD_PERSONAL_INFO_H_
//------------------------------------------------------------------------------
#include "../../../../Go/BD/Algorithm/BDImage.h"
#include "../../../../Go/Successor/SuccessorsChange.h"
#include "../../../../Go/Component/Widget/Image/Image/WidgetImage.h"
#include "State/LDPersonalInfoState.h"
#include "LDPersonalInfoData.h"
//------------------------------------------------------------------------------
class vLDPersonalInfo : public QWidget , public vSuccessorsChange
{
Q_OBJECT

    friend class vLDState<vLDPersonalInfo>;
    friend class vLDPersonalInfoStateUserExist;
    friend class vLDPersonalInfoStateUserNew;
    typedef                             QWidget                                 inherited;
    typedef                             vLDPersonalInfo                         class_name;
    typedef                             vLDState<vLDPersonalInfo>               state_type;
    typedef                             vLDPersonalInfoData                     data_name;

    vClientLDDialog*                                                            parent_;
    boost::shared_ptr<data_name>                                                data_;
    boost::shared_ptr<state_type>                                               state_;

private slots:
    void                                enableSaveButton();
    
private:
    void                                instance_signals();
    bool                                save();
    void                                change_state(state_type* state)         { state_.reset(state);}
    virtual QWidget*                    do_toWidget()                           {return this;}
    virtual bool                        do_check();
    virtual bool                        do_save()                               { return state_->save(this) ? save() : false; }
    virtual bool                        do_load()                               { return state_->load(this); }
    void                                make_user_new()                         { state_.reset(vLDPersonalInfoStateUserNew::instance()); }
    void                                make_user_exists()                      { state_.reset(vLDPersonalInfoStateUserExist::instance()); }

protected:
    virtual void                        do_change_state()                       {}

public:
    explicit                            vLDPersonalInfo(vClientLDDialog* owner, bool is_new_user);
    static class_name*                  make_isNewUser(vClientLDDialog* parent)      {return new class_name(parent, true);}
    static class_name*                  make_isExistsUser(vClientLDDialog* parent)   {return new class_name(parent, false);}
    bool                                test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_PERSONAL_INFO_H_ */
