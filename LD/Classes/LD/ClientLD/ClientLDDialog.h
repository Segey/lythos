/* 
 * File:   ClientLDDialog.h
 * Author: S.Panin
 *
 * Created on 13 Июль 2009 г., 12:10
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_CLIENT_DIALOG_H_
#define	_V_LD_CLIENT_DIALOG_H_
//------------------------------------------------------------------------------
#include "ClientLDDialogInstance.h"
//------------------------------------------------------------------------------
class vLD;
//------------------------------------------------------------------------------
class vClientLDDialog : public QDialog
{
Q_OBJECT
    friend  class vClientLDDialogInstance;
    typedef                     QDialog                                         inherited;
    typedef                     vClientLDDialog                                 class_name;
    
    std::vector<boost::shared_ptr<vLD> >                                        LDs;
    QTabWidget*                                                                 pages_;
    QScrollArea*                                                                scroll_area_;
    QVBoxLayout*                                                                page_layout_;
    QCheckBox*                                                                  checkbox_show_dialog_;
    QPushButton*                                                                button_save_;

    static QString              settings_table()                                { return ("LD"); }
    static QString              settings_key_show()                             { return ("FormShow"); }
    static QString              settings_key_form()                             { return ("FormSize"); }
    void                        settings_read();
    void                        settings_write() const;
    void                        instance_signals();
    void                        page_modified(bool value, int page_index);
    void                        createNewUserLDs();
    void                        createExistsUserLDs();
    void                        createLD(vLD* ld);
    void                        setAllPagesWindowModified(bool);

private slots:
    bool                        saveClicked();

protected:
    virtual void                closeEvent(QCloseEvent *);

public:
    explicit                   vClientLDDialog(bool is_new_user, QWidget* parent =0);
    static vClientLDDialog*    make_NewUserClientLDDialog();
    static vClientLDDialog*    make_ExistsUserClientLDDialog();
    void                       setCurrentIndex(int page_index_)                 {pages_->setCurrentIndex(page_index_);}
    void                       enableSaveButton();
    void                       save()                                           {saveClicked();}
    void                       load();
    int                        exec()                                           {return inherited::exec();}
    void                       change_state();
    bool                       test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_CLIENT_DIALOG_H_ */
