/* 
 * File:   ClientLDDialog.cpp
 * Author: S.Panin
 * 
 * Created on 13 Июль 2009 г., 12:10
 */
//------------------------------------------------------------------------------
#include "Classes/LD/LD.h"
#include "ClientLDDialog.h"
#include "ClientLDDialogInstance.h"
//------------------------------------------------------------------------------
vClientLDDialog::vClientLDDialog(bool , QWidget* parent /*=0*/)
               : QDialog(parent, Qt::WindowTitleHint), pages_(0), scroll_area_(0), page_layout_(0), button_save_(0)
{
        ::vInstance<vClientLDDialogInstance > (this).instance();
        instance_signals();
        settings_read();
}
//------------------------------------------------------------------------------
void vClientLDDialog::settings_read()
{
        QRect rect =  vDialog::form_size_calculation(this);       
        boost::tuple<bool, QRect, bool> array = vSettings::get(settings_table(), settings::make_form_size(settings_key_form(), rect), settings::make(settings_key_show(), true));
        inherited::setGeometry(array.get<1>());
        checkbox_show_dialog_->setChecked(array.get<2>());
}
//------------------------------------------------------------------------------
void vClientLDDialog::settings_write() const
{
        vSettings::set(settings_table(),  settings::make_form_size(settings_key_form(), inherited::geometry()), settings::make(settings_key_show(), checkbox_show_dialog_->isChecked()));
}
//------------------------------------------------------------------------------
vClientLDDialog* vClientLDDialog::make_NewUserClientLDDialog()
{
        QApplication::setOverrideCursor(Qt::WaitCursor);
        vClientLDDialog* result = new vClientLDDialog(true);
        result->createNewUserLDs();
        result->checkbox_show_dialog_->setHidden(true);
        QApplication::restoreOverrideCursor();
        return result;
}
//------------------------------------------------------------------------------
vClientLDDialog* vClientLDDialog::make_ExistsUserClientLDDialog()
{
        QApplication::setOverrideCursor(Qt::WaitCursor);
        vClientLDDialog* result = new vClientLDDialog(true);
        result->createExistsUserLDs();
        result->load();
        result->setWindowModified(false);
        QApplication::restoreOverrideCursor();
        return result;
}
//------------------------------------------------------------------------------
/*virtual*/ void vClientLDDialog::closeEvent(QCloseEvent *event)
{
        settings_write();
        if(!isWindowModified()) return;

        QMessageBox::StandardButton  click_button =QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Save changes to the Personal Office document before quitting?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(click_button == QMessageBox::Cancel) event->ignore();
        if(click_button == QMessageBox::Save && !saveClicked()) event->ignore();
}
//------------------------------------------------------------------------------
void vClientLDDialog::instance_signals()
{
        connect(button_save_, SIGNAL(clicked()), SLOT(saveClicked()));
}
//------------------------------------------------------------------------------
void vClientLDDialog::createNewUserLDs()
{
        createLD(vLD::make_PersonalInfo(this, true));
        createLD(vLD::make_NewPhotoGallery(this));
        createLD(vLD::make_NewRecordHistory(this));
        createLD(vLD::make_NewMeasurementHistory(this));
        createLD(vLD::make_NewOptions(this));
        page_layout_->addStretch();
}
//------------------------------------------------------------------------------
void vClientLDDialog::createExistsUserLDs()
{
        createLD(vLD::make_PersonalInfo(this, false));
        createLD(vLD::make_ExistsPhotoGallery(this));
        createLD(vLD::make_ExistsRecordHistory(this));
        createLD(vLD::make_ExistsMeasurementHistory(this));
        createLD(vLD::make_ExistsOptions(this));
        page_layout_->addStretch();
}
//------------------------------------------------------------------------------
void vClientLDDialog::createLD(vLD* ld)
{
        ld->setPageIndex(pages_->count());
        page_layout_->addWidget(ld);
        pages_->addTab(ld->toWidget(),ld->caption());
        LDs.push_back(boost::shared_ptr<vLD > (ld));
}
//------------------------------------------------------------------------------
void vClientLDDialog::page_modified(bool value, int page_index)
{
        pages_->setTabText(page_index, go::string::get_string_modified(pages_->tabText(page_index),value));
}
//------------------------------------------------------------------------------
bool vClientLDDialog::saveClicked()
{
        bool Result = true;
        QApplication::setOverrideCursor(Qt::WaitCursor);
        if(std::accumulate(LDs.begin(), LDs.end(), 0, boost::bind(std::plus<int>(), _1, boost::bind(&vLD::check, _2))) != static_cast<int>(LDs.size())) Result = false;
        if(std::accumulate(LDs.begin(), LDs.end(), 0, boost::bind(std::plus<int>(), _1, boost::bind(&vLD::save, _2))) != static_cast<int>(LDs.size())) Result = false;
        QApplication::restoreOverrideCursor();
        if(Result)
        {
                setAllPagesWindowModified(false);
                QMessageBox::information(this, program::name_full(), vtr::Message::tr("Your data are successfully updated!"), QMessageBox::Ok);
        }
        return Result;
}
//------------------------------------------------------------------------------
void vClientLDDialog::setAllPagesWindowModified(bool modified)
{
        button_save_->setEnabled(modified);
        QDialog::setWindowModified(modified);
        for(int x = 0; x < pages_->count(); ++x)  page_modified(modified, x);
}
//------------------------------------------------------------------------------
void vClientLDDialog::enableSaveButton()
{
        button_save_->setEnabled(true);
        QDialog::setWindowModified(true);
        page_modified(true, pages_->currentIndex());
}
//------------------------------------------------------------------------------
void vClientLDDialog::load()
{
        if(std::accumulate(LDs.begin(), LDs.end(), 0, boost::bind(std::plus<int>(), _1, boost::bind(&vLD::load, _2))) != static_cast<int>(LDs.size())) return;
        setAllPagesWindowModified(false);
}
//------------------------------------------------------------------------------
void vClientLDDialog::change_state()
{
        std::for_each(LDs.begin(), LDs.end(), boost::bind(&vLD::change_state, _1));
}
//------------------------------------------------------------------------------
bool vClientLDDialog::test()
{
        Q_ASSERT(pages_);
        Q_ASSERT(scroll_area_);
        Q_ASSERT(page_layout_);
        Q_ASSERT(button_save_);
        Q_ASSERT(!button_save_->isEnabled());
        Q_ASSERT(LDs.size() ==5);
        Q_ASSERT(pages_->count() ==6);

        vMain::user().set_test_id();
        QRect rect =  vDialog::form_size_calculation(this);
        Q_ASSERT( vSettings::set(settings_table(), settings::make(settings_key_show(), checkbox_show_dialog_->isChecked() ), settings::make_form_size(settings_key_form(), rect)) );
        settings_write();
        settings_read();
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_show(), bool()), settings::make_form_size(settings_key_form(), QRect())).get<0>() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_show(), bool()), settings::make_form_size(settings_key_form(), QRect())).get<1>() == checkbox_show_dialog_->isChecked() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make(settings_key_show(), bool()), settings::make_form_size(settings_key_form(), QRect())).get<2>() ==  rect );
        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------