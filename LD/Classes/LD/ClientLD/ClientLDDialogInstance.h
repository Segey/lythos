/* 
 * File:   ClientLDDialogInstance.h
 * Author: S.Panin
 *
 * Created on 13 Июль 2009 г., 12:54
 */
//------------------------------------------------------------------------------
#ifndef _V_CLIENT_LD_DIALOG_INSTANCE_H
#define	_V_CLIENT_LD_DIALOG_INSTANCE_H
//------------------------------------------------------------------------------
class vClientLDDialog;
//------------------------------------------------------------------------------
class vClientLDDialogInstance
{
    typedef                     vClientLDDialogInstance                         class_name;
    typedef                     vClientLDDialog*                                dialog_type;

    vClientLDDialog*                                                            parent_;

protected:
    dialog_type&                parent()                                        {return parent_; }
    dialog_type                 parent() const                                  {return parent_; }
    void                        instance_form()                                 {}
    void                        instance_widgets();
    QLayout*                    instance_top_layout()                           {return 0;}
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout();
    void                        instance_images()                               {}
    void                        instance_scroll_area();

public:
    explicit                    vClientLDDialogInstance(dialog_type parent = 0) : parent_(parent)  {}
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_CLIENT_LD_DIALOG_INSTANCE_H */
