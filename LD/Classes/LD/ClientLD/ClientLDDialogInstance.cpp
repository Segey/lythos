/* 
 * File:   ClientLDDialogInstance.cpp
 * Author: S.Panin
 * 
 * Created on 13 Июль 2009 г., 12:54
 */
//------------------------------------------------------------------------------
#include "ClientLDDialog.h"
#include "ClientLDDialogInstance.h"
//------------------------------------------------------------------------------
void vClientLDDialogInstance::instance_widgets()
{
        parent_->pages_                 = new QTabWidget;
        QWidget* widget                 = new QWidget;
        parent_->scroll_area_           = new QScrollArea;
        parent_->page_layout_           = new QVBoxLayout(widget);
        parent_->button_save_           = new QPushButton;
        parent_->checkbox_show_dialog_  = new QCheckBox;

        parent_->scroll_area_->setWidget(widget);
        instance_scroll_area();
}
//------------------------------------------------------------------------------
void vClientLDDialogInstance::instance_scroll_area()
{
        parent_->scroll_area_->resize(500,500);
        parent_->scroll_area_->setWidgetResizable(true);
        parent_->button_save_->setEnabled(false);
}
//------------------------------------------------------------------------------
QLayout* vClientLDDialogInstance::instance_middle_layout()
{
        parent_->pages_->addTab(parent_->scroll_area_,vtr::Dialog::tr("Main"));
        QHBoxLayout* center_layout = new QHBoxLayout;
        center_layout->addWidget(parent_->pages_);
        return center_layout;
}
//------------------------------------------------------------------------------
QLayout* vClientLDDialogInstance::instance_bottom_layout()
{
        vBottomInstance BottomInstance;
        BottomInstance.add_label_buttons(parent_->checkbox_show_dialog_, parent_->button_save_);
        return BottomInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
void vClientLDDialogInstance::translate()
{
        parent_->setWindowTitle(vtr::Dialog::tr("Personal office [*]"));
        parent_->button_save_->setText(vtr::Button::tr("&Save"));
        parent_->checkbox_show_dialog_->setText(vtr::Dialog::tr("Show this dialog at startup"));
}
