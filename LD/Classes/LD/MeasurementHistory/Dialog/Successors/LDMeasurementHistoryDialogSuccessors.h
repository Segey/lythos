/* 
 * File:   LDMeasurementHistoryDialogSuccessors.h
 * Author: S.Panin
 *
 * Created on 6 Января 2010 г., 18:52
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENTS_DIALOG_SUCCESSORS_H_
#define	_V_LD_MEASUREMENTS_DIALOG_SUCCESSORS_H_
//------------------------------------------------------------------------------
#include "../../../../../../Go/BD/Algorithm/BD.h"
#include "../LDMeasurementHistoryDialog.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogSuccessorsNew : public vLDMeasurementHistoryDialog
{
    typedef                     vLDMeasurementHistoryDialog                           inherited;
    typedef                     vLDMeasurementHistoryDialogSuccessorsNew        class_name;

    static int                  get_id();

protected:
    virtual bool                do_load(const QDate&)                           {return true;}
    virtual bool                do_save();

public:
    bool                        save_panel_current( int max) const;
    bool                        save_panel_desired( int max) const;
    bool                        test();
};
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogSuccessorsOpen : public vLDMeasurementHistoryDialog
{
    typedef                      vLDMeasurementHistoryDialog                    inherited;
    typedef                      vLDMeasurementHistoryDialogSuccessorsOpen      class_name;

    QDate                                                                       current_date_;

    static int                   get_id(const QDate& date);
    bool                         save_panel_current(int index) const;
    bool                         save_panel_desired(int index) const;
    int                          load_panel_current(const QDate& date);
    bool                         load_panel_desired(int index);

protected:
    virtual bool                 do_load(const QDate& exercise);
    virtual bool                 do_save();

public:
    bool                         test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENTS_DIALOG_SUCCESSORS_H_ */
