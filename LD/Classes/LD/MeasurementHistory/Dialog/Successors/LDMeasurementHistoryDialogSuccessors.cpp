/* 
 * File:   LDMeasurementHistoryDialogSuccessors.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 18:52
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogSuccessors.h"
//------------------------------------------------------------------------------
/*static*/ int vLDMeasurementHistoryDialogSuccessorsNew::get_id()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT MAX (MeasurementHistoryID) FROM MeasurementHistory;");
        if (!query.exec()) return -1;
        query.next();
        return query.value(0).toInt();
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDMeasurementHistoryDialogSuccessorsNew::do_save()
{
if( v_bd::exists(vMain::basebd().database(), "SELECT HistoryDate FROM MeasurementHistory WHERE (HistoryDate =? AND UserID =?);", inherited::panel_current()->get_date()))
        {
                RESTORE_OVERRIDE_CURSOR(QMessageBox::information(0, program::name_full(), vtr::BD::tr("The database already has a record for the current date!\nEnter a new date or change the existing record with data!"), QMessageBox::Ok));
        }
        int index = get_id() +1;
        return save_panel_current(index) && save_panel_desired(index);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsNew::save_panel_current( int index) const
{
        vLDMeasurementHistoryDialogPanelCurrent::data_name data =inherited::panel_current()->get_data();

        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO MeasurementHistory (userID, MeasurementHistoryID, Ghost, HistoryDate, Growth, Weight, Neck, Shoulders, Chest, BicepLeft,"
                      "BicepRight, ForearmLeft, ForearmRight, WristLeft, WristRight, Abdomen, Waist, Hips, ThighLeft, ThighRight, CalfLeft, CalfRight)"
                      "VALUES (?,?,'false',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(index);
        query.addBindValue( inherited::panel_current()->get_date() );
        query.addBindValue( data.growth_ );
        query.addBindValue( data.weight_ );
        query.addBindValue( data.neck_ );
        query.addBindValue( data.shoulders_  );
        query.addBindValue( data.chest_  );
        query.addBindValue( data.bicep_left_  );
        query.addBindValue( data.bicep_right_  );
        query.addBindValue( data.forearm_left_  );
        query.addBindValue( data.forearm_right_  );
        query.addBindValue( data.wrist_left_  );
        query.addBindValue( data.wrist_right_  );
        query.addBindValue( data.abdomen_  );
        query.addBindValue( data.waist_  );
        query.addBindValue( data.hips_  );
        query.addBindValue( data.thigh_left_  );
        query.addBindValue( data.thigh_right_  );
        query.addBindValue( data.calf_left_  );
        query.addBindValue( data.calf_right_  );
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsNew::save_panel_desired( int index) const
{
        vLDMeasurementHistoryDialogPanelDesired::data_name data =inherited::panel_desired()->get_data();

        QSqlQuery query(vMain::basebd().database());
        query.prepare("INSERT INTO MeasurementHistory (userID, MeasurementHistoryID, Ghost, HistoryDate, Growth, Weight, Neck, Shoulders, Chest, BicepLeft,"
                      " ForearmLeft, WristLeft, Abdomen, Waist, Hips, ThighLeft, CalfLeft)"
                      "VALUES (?,?,'true',?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(index);
        query.addBindValue( inherited::panel_desired()->get_date() );
        query.addBindValue( data.growth_ );
        query.addBindValue( data.weight_ );
        query.addBindValue( data.neck_ );
        query.addBindValue( data.shoulders_  );
        query.addBindValue( data.chest_  );
        query.addBindValue( data.bicep_  );
        query.addBindValue( data.forearm_ );
        query.addBindValue( data.wrist_ );       
        query.addBindValue( data.abdomen_  );
        query.addBindValue( data.waist_  );
        query.addBindValue( data.hips_  );
        query.addBindValue( data.thigh_ );
        query.addBindValue( data.calf_ );
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsNew::test()
{      
        inherited::test();

        QDate date_current (2010,10,5);
        QDate date_desired (2013,11,9);
        int index = get_id() +1;

        vLDMeasurementHistoryDialogPanelCurrent::data_name data_current;
        data_current.growth_            = QString("%1").arg(70.7);
        data_current.abdomen_           = QString("%1").arg(10.7);
        data_current.bicep_left_        = QString("%1").arg(20.10);
        data_current.bicep_right_       = QString("%1").arg(77.76);
        data_current.calf_left_         = QString("%1").arg(55.7);
        data_current.calf_right_        = QString("%1").arg(330.05);
        data_current.chest_             = QString("%1").arg(88.88);
        data_current.forearm_left_      = QString("%1").arg(33.33);
        data_current.forearm_right_     = QString("%1").arg(11.77);
        data_current.neck_              = QString("%1").arg(4.797);
        data_current.shoulders_         = QString("%1").arg(1000.77);
        data_current.thigh_left_        = QString("%1").arg(40.77);
        data_current.thigh_right_       = QString("%1").arg(1770.577);
        data_current.waist_             = QString("%1").arg(199.797);
        data_current.hips_              = QString("%1").arg(1000.797);
        data_current.weight_            = QString("%1").arg(210.767);
        data_current.wrist_left_        = QString("%1").arg(10555.79997);
        data_current.wrist_right_       = QString("%1").arg(1000.7700);
        inherited::panel_current()->set_data(data_current);
        inherited::panel_current()->set_date(date_current);

        vLDMeasurementHistoryDialogPanelDesired::data_name data_desired;
        data_desired.growth_            = QString("%1").arg(70.97);
        data_desired.abdomen_           = QString("%1").arg(107.7);
        data_desired.bicep_             = QString("%1").arg(2.05);
        data_desired.calf_              = QString("%1").arg(5.7);
        data_desired.chest_             = QString("%1").arg(8.78);
        data_desired.forearm_           = QString("%1").arg(15.77);
        data_desired.neck_              = QString("%1").arg(47.97);
        data_desired.shoulders_         = QString("%1").arg(1080.77);
        data_desired.thigh_             = QString("%1").arg(170.577);
        data_desired.waist_             = QString("%1").arg(19.797);
        data_desired.hips_              = QString("%1").arg(10.797);
        data_desired.weight_            = QString("%1").arg(21.767);
        data_desired.wrist_             = QString("%1").arg(140.7700);
        inherited::panel_desired()->set_data(data_desired);
        inherited::panel_desired()->set_date(date_desired);

        vMain::user().set_test_id();
        save_panel_current( index);
        save_panel_desired( index);
        
        inherited::panel_current()->set_data(vLDMeasurementHistoryDialogPanelCurrent::data_name());
        inherited::panel_current()->set_date(QDate());
        inherited::panel_desired()->set_data(vLDMeasurementHistoryDialogPanelDesired::data_name());
        inherited::panel_desired()->set_date(QDate());

        vLDMeasurementHistoryDialogSuccessorsOpen dialog;
        dialog.load(date_current);
        vLDMeasurementHistoryDialogPanelCurrent::data_name data_current_get = dialog.panel_current()->get_data();

        Q_ASSERT(data_current_get.growth_            == QString("%1").arg(70.7));
        Q_ASSERT(data_current_get.abdomen_           == QString("%1").arg(10.7));
        Q_ASSERT(data_current_get.bicep_left_        == QString("%1").arg(20.10));
        Q_ASSERT(data_current_get.bicep_right_       == QString("%1").arg(77.76));
        Q_ASSERT(data_current_get.calf_left_         == QString("%1").arg(55.7));
        Q_ASSERT(data_current_get.calf_right_        == QString("%1").arg(330.05));
        Q_ASSERT(data_current_get.chest_             == QString("%1").arg(88.88));
        Q_ASSERT(data_current_get.forearm_left_      == QString("%1").arg(33.33));
        Q_ASSERT(data_current_get.forearm_right_     == QString("%1").arg(11.77));
        Q_ASSERT(data_current_get.neck_              == QString("%1").arg(4.797));
        Q_ASSERT(data_current_get.shoulders_         == QString("%1").arg(1000.77));
        Q_ASSERT(data_current_get.thigh_left_        == QString("%1").arg(40.77));
        Q_ASSERT(data_current_get.thigh_right_       == QString("%1").arg(1770.577));
        Q_ASSERT(data_current_get.waist_             == QString("%1").arg(199.797));
        Q_ASSERT(data_current_get.hips_              == QString("%1").arg(1000.797));
        Q_ASSERT(data_current_get.weight_            == QString("%1").arg(210.767));
        Q_ASSERT(data_current_get.wrist_left_        == QString("%1").arg(10555.79997));
        Q_ASSERT(data_current_get.wrist_right_       == QString("%1").arg(1000.7700));
        Q_ASSERT(dialog.panel_current()->get_date()       == date_current);


        vLDMeasurementHistoryDialogPanelDesired::data_name data_desired_get = dialog.panel_desired()->get_data();

        Q_ASSERT(data_desired_get.growth_            == QString("%1").arg(70.97));
        Q_ASSERT(data_desired_get.abdomen_           == QString("%1").arg(107.7));
        Q_ASSERT(data_desired_get.bicep_             == QString("%1").arg(2.05));
        Q_ASSERT(data_desired_get.calf_              == QString("%1").arg(5.7));
        Q_ASSERT(data_desired_get.chest_             == QString("%1").arg(8.78));
        Q_ASSERT(data_desired_get.forearm_           == QString("%1").arg(15.77));
        Q_ASSERT(data_desired_get.neck_              == QString("%1").arg(47.97));
        Q_ASSERT(data_desired_get.shoulders_         == QString("%1").arg(1080.77));
        Q_ASSERT(data_desired_get.thigh_             == QString("%1").arg(170.577));
        Q_ASSERT(data_desired_get.waist_             == QString("%1").arg(19.797));
        Q_ASSERT(data_desired_get.hips_              == QString("%1").arg(10.797));
        Q_ASSERT(data_desired_get.weight_            == QString("%1").arg(21.767));
        Q_ASSERT(data_desired_get.wrist_             == QString("%1").arg(140.7700));
        Q_ASSERT(dialog.panel_desired()->get_date()       == date_desired);

        QSqlQuery query1(vMain::basebd().database());
        return query1.exec("DELETE FROM MeasurementHistory WHERE UserID =0");
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*static*/ int vLDMeasurementHistoryDialogSuccessorsOpen::get_id(const QDate& date)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT MeasurementHistoryID FROM MeasurementHistory WHERE ( UserID =? AND HistoryDate =?) ;");
        query.addBindValue(vMain::user().id());
        query.addBindValue( date );
        if (!query.exec()) return -1;
        query.next();
        return query.value(0).toInt();
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDMeasurementHistoryDialogSuccessorsOpen::do_load(const QDate& date)
{     
        current_date_ =date;
        int index =load_panel_current(date);
        return index >=0  && load_panel_desired(index);
}
//------------------------------------------------------------------------------
int vLDMeasurementHistoryDialogSuccessorsOpen::load_panel_current(const QDate& date)
{
        vLDMeasurementHistoryDialogPanelCurrent::data_name data;

        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT MeasurementHistoryID, Growth, Weight, Neck, Shoulders, Chest, BicepLeft, BicepRight, ForearmLeft,  ForearmRight, WristLeft, WristRight, Abdomen, "
                      "Waist, Hips, ThighLeft, ThighRight, CalfLeft, CalfRight FROM MeasurementHistory WHERE (UserID =? AND Ghost ='false' AND HistoryDate =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(date);      
        if (!query.exec()) return -1;
        query.next();

        int index               = query.value(0).toInt();
        data.growth_            = query.value(1).toString();
        data.weight_            = query.value(2).toString();
        data.neck_              = query.value(3).toString();
        data.shoulders_         = query.value(4).toString();
        data.chest_             = query.value(5).toString();
        data.bicep_left_        = query.value(6).toString();
        data.bicep_right_       = query.value(7).toString();
        data.forearm_left_      = query.value(8).toString();
        data.forearm_right_     = query.value(9).toString();
        data.wrist_left_        = query.value(10).toString();
        data.wrist_right_       = query.value(11).toString();
        data.abdomen_           = query.value(12).toString();
        data.waist_             = query.value(13).toString();
        data.hips_              = query.value(14).toString();
        data.thigh_left_        = query.value(15).toString();
        data.thigh_right_       = query.value(16).toString();
        data.calf_left_         = query.value(17).toString();
        data.calf_right_        = query.value(18).toString();

        inherited::panel_current()->set_date(date);
        inherited::panel_current()->set_data(data);
        return index;
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsOpen::load_panel_desired(int index)
{
        vLDMeasurementHistoryDialogPanelDesired::data_name data;

        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT HistoryDate, Growth, Weight, Neck, Shoulders, Chest, BicepLeft,  ForearmLeft, WristLeft,  Abdomen, "
                      "Waist, Hips, ThighLeft,  CalfLeft FROM MeasurementHistory WHERE (UserID =? AND Ghost ='true' AND MeasurementHistoryID =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(index);
        if (!query.exec()) return false;
        query.next();

        QDate date              = query.value(0).toDate();
        data.growth_            = query.value(1).toString();
        data.weight_            = query.value(2).toString();
        data.neck_              = query.value(3).toString();
        data.shoulders_         = query.value(4).toString();
        data.chest_             = query.value(5).toString();
        data.bicep_             = query.value(6).toString();
        data.forearm_           = query.value(7).toString();
        data.wrist_             = query.value(8).toString();
        data.abdomen_           = query.value(9).toString();
        data.waist_             = query.value(10).toString();
        data.hips_              = query.value(11).toString();
        data.thigh_             = query.value(12).toString();
        data.calf_              = query.value(13).toString();

        inherited::panel_desired()->set_date(date);
        inherited::panel_desired()->set_data(data);
        return true;
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDMeasurementHistoryDialogSuccessorsOpen::do_save()
{
         int index=get_id( current_date_ );
         return save_panel_current(index) && save_panel_desired(index);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsOpen::save_panel_current(int index) const
{
        vLDMeasurementHistoryDialogPanelCurrent::data_name data =inherited::panel_current()->get_data();

        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE MeasurementHistory SET HistoryDate =?, Growth =?, Weight =?, Neck =?, Shoulders =?, Chest =?, BicepLeft =?,"
                      "BicepRight =?, ForearmLeft =?, ForearmRight =?, WristLeft =?, WristRight =?, Abdomen =?, Waist =?, Hips =?, ThighLeft =?, ThighRight =?, CalfLeft =?, CalfRight =?"
                      "WHERE (UserID =? AND Ghost ='false' AND MeasurementHistoryID =?);");

        query.addBindValue( inherited::panel_current()->get_date() );
        query.addBindValue( data.growth_ );
        query.addBindValue( data.weight_ );
        query.addBindValue( data.neck_ );
        query.addBindValue( data.shoulders_  );
        query.addBindValue( data.chest_  );
        query.addBindValue( data.bicep_left_  );
        query.addBindValue( data.bicep_right_  );
        query.addBindValue( data.forearm_left_  );
        query.addBindValue( data.forearm_right_  );
        query.addBindValue( data.wrist_left_  );
        query.addBindValue( data.wrist_right_  );
        query.addBindValue( data.abdomen_  );
        query.addBindValue( data.waist_  );
        query.addBindValue( data.hips_  );
        query.addBindValue( data.thigh_left_  );
        query.addBindValue( data.thigh_right_  );
        query.addBindValue( data.calf_left_  );
        query.addBindValue( data.calf_right_  );
        query.addBindValue(vMain::user().id());
        query.addBindValue(index);
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsOpen::save_panel_desired(int index) const
{
        vLDMeasurementHistoryDialogPanelDesired::data_name data =inherited::panel_desired()->get_data();

        QSqlQuery query(vMain::basebd().database());
        query.prepare("UPDATE MeasurementHistory SET HistoryDate =?, Growth =?, Weight =?, Neck =?, Shoulders =?, Chest =?, BicepLeft =?,"
                      "ForearmLeft =?, WristLeft =?, Abdomen =?, Waist =?, Hips =?, ThighLeft =?, CalfLeft =?"
                      "WHERE (UserID =? AND Ghost ='true' AND MeasurementHistoryID =?);");
        
        query.addBindValue( inherited::panel_desired()->get_date() );
        query.addBindValue( data.growth_ );
        query.addBindValue( data.weight_ );
        query.addBindValue( data.neck_ );
        query.addBindValue( data.shoulders_  );
        query.addBindValue( data.chest_  );
        query.addBindValue( data.bicep_  );
        query.addBindValue( data.forearm_ );
        query.addBindValue( data.wrist_ );       
        query.addBindValue( data.abdomen_  );
        query.addBindValue( data.waist_  );
        query.addBindValue( data.hips_  );
        query.addBindValue( data.thigh_ );
        query.addBindValue( data.calf_ );
        query.addBindValue(vMain::user().id());
        query.addBindValue(index);
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogSuccessorsOpen::test()
{
        inherited::test();
        
        QDate date_current (2000,1,1);
        QDate date_desired (2019,3,3);
                
        vLDMeasurementHistoryDialogSuccessorsNew panel;      
        panel.panel_current()->set_date(date_current);
        panel.panel_desired()->set_date(date_desired);
        
        int index = get_id(date_current);
        
        vMain::user().set_test_id();
        panel.save_panel_current(index);
        panel.save_panel_desired(index);

        vLDMeasurementHistoryDialogPanelCurrent::data_name data_current;
        data_current.growth_            = QString("%1").arg(7.7);
        data_current.abdomen_           = QString("%1").arg(10.2);
        data_current.bicep_left_        = QString("%1").arg(120.50);
        data_current.bicep_right_       = QString("%1").arg(2.7);
        data_current.calf_left_         = QString("%1").arg(575.7);
        data_current.calf_right_        = QString("%1").arg(3130.60);
        data_current.chest_             = QString("%1").arg(8.18);
        data_current.forearm_left_      = QString("%1").arg(33.2);
        data_current.forearm_right_     = QString("%1").arg(171.787);
        data_current.neck_              = QString("%1").arg(49.797);
        data_current.shoulders_         = QString("%1").arg(1.77);
        data_current.thigh_left_        = QString("%1").arg(42.77);
        data_current.thigh_right_       = QString("%1").arg(190.577);
        data_current.waist_             = QString("%1").arg(169.7947);
        data_current.hips_              = QString("%1").arg(13.797);
        data_current.weight_            = QString("%1").arg(2.09);
        data_current.wrist_left_        = QString("%1").arg(07.797);
        data_current.wrist_right_       = QString("%1").arg(66.70);
        inherited::panel_current()->set_data(data_current);
        inherited::panel_current()->set_date(date_current);

        vLDMeasurementHistoryDialogPanelDesired::data_name data_desired;
        data_desired.growth_            = QString("%1").arg(70.97);
        data_desired.abdomen_           = QString("%1").arg(107.7);
        data_desired.bicep_             = QString("%1").arg(2.02);
        data_desired.calf_              = QString("%1").arg(5.7);
        data_desired.chest_             = QString("%1").arg(8.78);
        data_desired.forearm_           = QString("%1").arg(15.77);
        data_desired.neck_              = QString("%1").arg(47.97);
        data_desired.shoulders_         = QString("%1").arg(1080.77);
        data_desired.thigh_             = QString("%1").arg(170.577);
        data_desired.waist_             = QString("%1").arg(19.797);
        data_desired.hips_              = QString("%1").arg(10.797);
        data_desired.weight_            = QString("%1").arg(21.767);
        data_desired.wrist_             = QString("%1").arg(140.7700);
        inherited::panel_desired()->set_data(data_desired);
        inherited::panel_desired()->set_date(date_desired);

        current_date_= date_current;
        do_save();

        inherited::panel_current()->set_data(vLDMeasurementHistoryDialogPanelCurrent::data_name());
        inherited::panel_current()->set_date(QDate());
        inherited::panel_desired()->set_data(vLDMeasurementHistoryDialogPanelDesired::data_name());
        inherited::panel_desired()->set_date(QDate());

        inherited::load(date_current);
        vLDMeasurementHistoryDialogPanelCurrent::data_name data_current_get = inherited::panel_current()->get_data();

        Q_ASSERT(data_current_get.growth_            == QString("%1").arg(7.7));
        Q_ASSERT(data_current_get.abdomen_           == QString("%1").arg(10.2));
        Q_ASSERT(data_current_get.bicep_left_        == QString("%1").arg(120.50));
        Q_ASSERT(data_current_get.bicep_right_       == QString("%1").arg(2.7));
        Q_ASSERT(data_current_get.calf_left_         == QString("%1").arg(575.7));
        Q_ASSERT(data_current_get.calf_right_        == QString("%1").arg(3130.60));
        Q_ASSERT(data_current_get.chest_             == QString("%1").arg(8.18));
        Q_ASSERT(data_current_get.forearm_left_      == QString("%1").arg(33.2));
        Q_ASSERT(data_current_get.forearm_right_     == QString("%1").arg(171.787));
        Q_ASSERT(data_current_get.neck_              == QString("%1").arg(49.797));
        Q_ASSERT(data_current_get.shoulders_         == QString("%1").arg(1.77));
        Q_ASSERT(data_current_get.thigh_left_        == QString("%1").arg(42.77));
        Q_ASSERT(data_current_get.thigh_right_       == QString("%1").arg(190.577));
        Q_ASSERT(data_current_get.waist_             == QString("%1").arg(169.7947));
        Q_ASSERT(data_current_get.hips_              == QString("%1").arg(13.797));
        Q_ASSERT(data_current_get.weight_            == QString("%1").arg(2.09));
        Q_ASSERT(data_current_get.wrist_left_        == QString("%1").arg(07.797));
        Q_ASSERT(data_current_get.wrist_right_       == QString("%1").arg(66.70));
        Q_ASSERT(inherited::panel_current()->get_date()       == date_current);


        vLDMeasurementHistoryDialogPanelDesired::data_name data_desired_get = inherited::panel_desired()->get_data();

        Q_ASSERT(data_desired_get.growth_            == QString("%1").arg(70.97));
        Q_ASSERT(data_desired_get.abdomen_           == QString("%1").arg(107.7));
        Q_ASSERT(data_desired_get.bicep_             == QString("%1").arg(2.02));
        Q_ASSERT(data_desired_get.calf_              == QString("%1").arg(5.7));
        Q_ASSERT(data_desired_get.chest_             == QString("%1").arg(8.78));
        Q_ASSERT(data_desired_get.forearm_           == QString("%1").arg(15.77));
        Q_ASSERT(data_desired_get.neck_              == QString("%1").arg(47.97));
        Q_ASSERT(data_desired_get.shoulders_         == QString("%1").arg(1080.77));
        Q_ASSERT(data_desired_get.thigh_             == QString("%1").arg(170.577));
        Q_ASSERT(data_desired_get.waist_             == QString("%1").arg(19.797));
        Q_ASSERT(data_desired_get.hips_              == QString("%1").arg(10.797));
        Q_ASSERT(data_desired_get.weight_            == QString("%1").arg(21.767));
        Q_ASSERT(data_desired_get.wrist_             == QString("%1").arg(140.7700));
        Q_ASSERT(inherited::panel_desired()->get_date()       == date_desired);

        QSqlQuery query(vMain::basebd().database());
        return query.exec("DELETE FROM MeasurementHistory WHERE UserID =0");
}