/* 
 * File:   LDMeasurementHistoryInstance.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:38
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogInstance.h"
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogInstance::instance_widgets()
{
        parent_->setContextMenuPolicy(Qt::CustomContextMenu);
        parent_->label_username_        = new QLabel;
        parent_->label_username_icon_   = new QLabel;
        parent_->label_cap_             = new QLabel;
        parent_->panel_current_         = new vLDMeasurementHistoryDialogPanelCurrent;
        parent_->panel_desired_         = new vLDMeasurementHistoryDialogPanelDesired;
        parent_->panel_mbs_             = new vLDMeasurementHistoryDialogPanelMBs(parent_->panel_current_ ,parent_->panel_desired_);
        parent_->button_method_         = new QPushButton;
        parent_->button_save_           = new QPushButton;
        parent_->button_cancel_         = new QPushButton;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogInstance::instance_images()
{
        parent_->label_username_icon_->setPixmap(const_pixmap_64::measurement_history_dialog());
}
//------------------------------------------------------------------------------
QLayout* vLDMeasurementHistoryDialogInstance::instance_top_layout()
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(parent_->label_username_icon_, parent_->label_username_);
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* vLDMeasurementHistoryDialogInstance::instance_middle_layout()
{
        QHBoxLayout* layout = new QHBoxLayout;
        layout->addWidget(parent_->panel_current_, 2);
        layout->addWidget(parent_->panel_desired_, 1);

        vLay lay(new QGridLayout, 2);
        lay.add_caption_bevel_first(parent_->label_cap_);
        lay.add_layout(layout, false);
        lay.add_unary(parent_->panel_mbs_, false);
        return lay.clayout();
}
//------------------------------------------------------------------------------
QLayout* vLDMeasurementHistoryDialogInstance::instance_bottom_layout()
{
        vBottomInstance BottomInstance;
        BottomInstance.add_bevel();
        BottomInstance.add_label_buttons(parent_->button_method_, parent_->button_save_, parent_->button_cancel_);
        return BottomInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogInstance::translate()
{
        parent_->label_username_->setText(vtr::Dialog::tr("Add/Edit a Measurement Record"));
        parent_->button_method_ ->setText(vtr::Button::tr("&Add Ideal Proportions..."));
        parent_->label_cap_     ->setText(vtr::Dialog::tr("Measurement Record"));
        parent_->button_save_   ->setText(vtr::Button::tr("&Save"));
        parent_->button_cancel_ ->setText(vtr::Button::tr("&Cancel"));
        parent_->panel_current_ ->setTitle(vtr::Panel::tr("My Current Result"));
        parent_->panel_desired_ ->setTitle(vtr::Panel::tr("My Desired Result"));
        parent_->panel_mbs_     ->setTitle(vtr::Panel::tr("Measurement Summary"));

        vDialog::window_title(parent_);
}