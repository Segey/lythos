/* 
 * File:   LDMeasurementHistoryDialogMethod.cpp
 * Author: S.Panin
 * 
 * Created on 18 Декабрь 2009 г., 23:11
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogMethod.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogMethod::vLDMeasurementHistoryDialogMethod()
            : inherited(0, Qt::WindowTitleHint), data_(new Data)
{
        vDllInstance::instance(this, data_.get());
        data_->translate();
        vDialog::window_title(this);
        settings_read();
        instance_signals();       
        load();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::settings_read()
{
        QRect rect_default =  vDialog::form_size_calculation(this);
        QRect rect = vSettings::get(settings_table(), settings::make_form_size(settings_key(), rect_default)).get<1>();
        inherited::setGeometry(rect);
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::settings_write() const
{
        vSettings::set(settings_table(), settings::make_form_size(settings_key(), geometry()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::instance_signals() const
{
        connect(data_->method_combo_box_,               SIGNAL(currentIndexChanged(const QString&)), SLOT(method_change(const QString&)));
        connect(data_->growth_combo_box_,               SIGNAL(currentIndexChanged(const QString&)), SLOT(growth_change(const QString&)));
        connect(data_->cancel_button_,                  SIGNAL(clicked()), SLOT(close()));
        connect(data_->save_as_template_button_,        SIGNAL(clicked()), SLOT(save_as_template()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::load()
{
        QStringList file_names = QDir(data_->path()).entryList(QStringList(QString("*." + data_->exp())), QDir::Files | QDir::NoSymLinks);
        std::transform(file_names.constBegin(), file_names.constEnd(), file_names.begin(), boost::bind(std::plus<QStringList::value_type>(), data_->path(), _1 ) );
        
        data_->map_ = vXMLProportion::get_methods(file_names);
        data_->method_combo_box_->clear();
        std::for_each(data_->map_.begin(), data_->map_.end(), boost::bind(&QComboBox::addItem, data_->method_combo_box_,
                                                boost::bind(&Map::value_type::first, _1), QVariant()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::method_change(const QString& method_name)
{
        data_->sex_combo_box_->clear();
        const QString& file_name = (data_->map_.find(method_name))->second;
        data_->sex_combo_box_->clear();
        data_->sex_combo_box_->addItem( vXMLProportion::get_sex(file_name) ? vtr::Body::tr("Male") : vtr::Body::tr("Female"));
        data_->growth_combo_box_->clear();
        data_->growth_combo_box_->addItems(vXMLProportion::get_growths(file_name));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogMethod::growth_change(const QString& growth)
{
        const QString& file_name = (data_->map_.find(data_->method_combo_box_->currentText()))->second;
        data_->set_data( vXMLProportion::get_proportions(file_name, growth) );
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogMethod::test()
{
        Q_ASSERT(data_->path() == const_paths::proportion());
        Q_ASSERT(data_->exp() == const_exp::proportion());
        const QStringList file_names = QDir(data_->path()).entryList(QStringList(QString("*." + data_->exp())), QDir::Files | QDir::NoSymLinks);

        Q_ASSERT(file_names.empty() ==false);
        Q_ASSERT(file_names.size() >1);
        Q_ASSERT(data_->method_label_);
        Q_ASSERT(data_->method_combo_box_);
        Q_ASSERT(data_->growth_label_);
        Q_ASSERT(data_->growth_combo_box_);
        Q_ASSERT(data_->sex_label_);
        Q_ASSERT(data_->sex_combo_box_);
        Q_ASSERT(data_->weight_label_);
        Q_ASSERT(data_->weight_edit_);
        Q_ASSERT(data_->neck_label_);
        Q_ASSERT(data_->neck_edit_);
        Q_ASSERT(data_->shoulders_label_);
        Q_ASSERT(data_->shoulders_edit_);
        Q_ASSERT(data_->chest_label_);
        Q_ASSERT(data_->chest_edit_);
        Q_ASSERT(data_->bicep_label_);
        Q_ASSERT(data_->bicep_edit_);
        Q_ASSERT(data_->forearm_label_);
        Q_ASSERT(data_->forearm_edit_);
        Q_ASSERT(data_->wrist_label_);
        Q_ASSERT(data_->wrist_edit_);
        Q_ASSERT(data_->abdomen_label_);
        Q_ASSERT(data_->abdomen_edit_);
        Q_ASSERT(data_->waist_label_);
        Q_ASSERT(data_->waist_edit_);
        Q_ASSERT(data_->hips_label_);
        Q_ASSERT(data_->hips_edit_);
        Q_ASSERT(data_->thigh_label_);
        Q_ASSERT(data_->thigh_edit_);
        Q_ASSERT(data_->calf_label_);
        Q_ASSERT(data_->calf_edit_);
        Q_ASSERT(data_->map_.size());
        Q_ASSERT(data_->username_icon_label_);
        Q_ASSERT(data_->username_label_);
        Q_ASSERT(data_->cap_label_);
        Q_ASSERT(data_->panel_);
        Q_ASSERT(data_->save_as_template_button_);
        Q_ASSERT(data_->cancel_button_);

        vMain::user().set_test_id();
        QRect rect(0,170,390,0);
        Q_ASSERT( vSettings::set(settings_table(), settings::make_form_size(settings_key(), rect)) );
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<0>() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<1>() == rect );

        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------