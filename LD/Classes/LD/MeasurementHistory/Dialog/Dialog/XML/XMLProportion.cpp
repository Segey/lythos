/* 
 * File:   XMLProportion.cpp
 * Author: S.Panin
 * 
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#include <map>
#include "XMLProportion.h"
//------------------------------------------------------------------------------
QString  vXMLProportion::attribute( const QString& file_name, const QString& attribute_name,  const QString& default_name)
{
        QDomDocument domDoc;
        QFile file(file_name);
        if( file.open(QIODevice::ReadOnly) && domDoc.setContent(&file) )
        {
                const QDomElement node = domDoc.documentElement();
                QDomNode domNode = node.firstChild();

                while( !domNode.isNull() && domNode.isElement() )
                {
                        const QDomElement next = domNode.toElement();
                        if(!next.isNull() && next.tagName() == method_name() )
                                        return next.attribute(attribute_name, default_name);
                        domNode = domNode.nextSibling();
                }
        }
        return default_name;
}
//------------------------------------------------------------------------------
vXMLProportion::Data  vXMLProportion::get_methods(const QStringList& files)
{
        Data result;

        for(QStringList::const_iterator it = files.constBegin(); it != files.constEnd(); ++it)
        {
                const QString method = get_method(*it);
                if(method != no_name()) result.insert(std::make_pair(method, *it));
        }

        return result;
}
//------------------------------------------------------------------------------
QStringList   vXMLProportion::get_growths(const QString& file_name)
{
        QDomDocument domDoc;
        QFile file(file_name);
        QStringList data;
        if( file.open(QIODevice::ReadOnly) && domDoc.setContent(&file) )
        {
                const QDomElement node = domDoc.documentElement();
                QDomNode domNode = node.firstChild();
                while( !domNode.isNull() && domNode.isElement() )
                {
                        const QDomElement next = domNode.toElement();
                        if(!domNode.toElement().isNull() && domNode.toElement().tagName() == record_name() )
                        {
                                bool done =false;
                                double number = next.attribute(height_name(), "0").toDouble(&done);
                                if ( done ) data.push_back( QString::number(number) );
                        }
                        domNode = domNode.nextSibling();
                }
        }
        std::sort(data.begin(), data.end());
        return data;
}
//------------------------------------------------------------------------------
vXMLProportion::data_name  vXMLProportion::get_proportions(const QString& file_name,  const QString& growth )
{
        QDomDocument domDoc;
        QFile file(file_name);
        data_name data;
        if( file.open(QIODevice::ReadOnly) && domDoc.setContent(&file) )
        {
                const QDomElement node = domDoc.documentElement();
                QDomNode domNode = node.firstChild();
                while( !domNode.isNull() && domNode.isElement() )
                {
                        const QDomElement next = domNode.toElement();
                        if(!domNode.toElement().isNull() && domNode.toElement().tagName() == record_name() )
                        {
                                if (next.attribute(height_name(), "0") == growth )
                                {
                                        data.growth_    = growth;
                                        data.abdomen_   = v_user::get_string_is_double(next.attribute("Abdomen", ""), "");
                                        data.bicep_     = v_user::get_string_is_double(next.attribute("Biceps", ""), "");
                                        data.calf_      = v_user::get_string_is_double(next.attribute("Calves", ""), "");
                                        data.chest_     = v_user::get_string_is_double(next.attribute("Chest", ""), "");
                                        data.forearm_   = v_user::get_string_is_double(next.attribute("Forearms", ""), "");
                                        data.hips_      = v_user::get_string_is_double(next.attribute("Hips", ""), "");
                                        data.neck_      = v_user::get_string_is_double(next.attribute("Neck", ""), "");
                                        data.shoulders_ = v_user::get_string_is_double(next.attribute("Shoulders", ""), "");
                                        data.thigh_     = v_user::get_string_is_double(next.attribute("Thighs", ""), "");
                                        data.waist_     = v_user::get_string_is_double(next.attribute("Waist", ""), "");
                                        data.weight_    = v_user::get_string_is_double(next.attribute("Weight", ""), "");
                                        data.wrist_     = v_user::get_string_is_double(next.attribute("Wrist", ""), "");
                                }
                        }
                        domNode = domNode.nextSibling();
                }
        }
        return data;
}
//------------------------------------------------------------------------------
bool vXMLProportion::test()
{
        const QString file_name = const_test_file::xml_marseilles();
        const QString method_name = "Marseilles R.";
        const Data data = get_methods(QStringList() << file_name );

        Q_ASSERT_X(data.size(),"check","File /home/dix/Projects/Lythos/Resources/Test/Marseilles_R.lp not Found");
        Q_ASSERT(data.begin()->first == method_name);
        Q_ASSERT(data.begin()->second == file_name);

        Q_ASSERT(get_sex(file_name));
        Q_ASSERT(get_method(file_name) == method_name);

        const QStringList list =get_growths(file_name);
        Q_ASSERT(list.size() == 9);
        Q_ASSERT(list.at(0) == "165");
        Q_ASSERT(list.at(3) == "172");
        Q_ASSERT(list.at(8) == "185");

        data_name proportion= get_proportions(file_name, "165");
        Q_ASSERT(proportion.growth_    =="165");
        Q_ASSERT(proportion.abdomen_    =="13");
        Q_ASSERT(proportion.bicep_      =="305");
        Q_ASSERT(proportion.calf_       =="35");
        Q_ASSERT(proportion.chest_      =="105");
        Q_ASSERT(proportion.forearm_    =="30");
        Q_ASSERT(proportion.hips_       =="55");
        Q_ASSERT(proportion.neck_       =="350");
        Q_ASSERT(proportion.shoulders_  =="9");
        Q_ASSERT(proportion.thigh_      =="42");
        Q_ASSERT(proportion.waist_      =="75");
        Q_ASSERT(proportion.weight_     =="65");
        Q_ASSERT(proportion.wrist_      =="77");

        return true;
}
//------------------------------------------------------------------------------