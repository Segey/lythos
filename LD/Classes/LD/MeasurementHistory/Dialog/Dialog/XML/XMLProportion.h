/* 
 * File:   XMLProportion.h
 * Author: S.Panin
 *
 * Created on 5 Август 2009 г., 14:09
 */
//------------------------------------------------------------------------------
#ifndef _V_XML_PROPORTION_H_
#define	_V_XML_PROPORTION_H_
//------------------------------------------------------------------------------
class vXMLProportion : go::noncopyable
{
    typedef                     std::map<QString, QString>                      Data;
    typedef                     vLDMeasurementHistoryDataShort::data_name       data_name;
    typedef                     vXMLProportion                                  class_name;

private:
    static QString              attribute( const QString& file_name, const QString& attribute_name,  const QString& default_name);
    static QString              method_name()                                   { return  "Method"; }
    static QString              record_name()                                   { return  "Record"; }
    static QString              height_name()                                   { return  "Height"; }
    static QString              no_name()                                       { return  "No_Name"; }
    static QString              get_method(const QString& file_name)            { return attribute(file_name,"Name",class_name::no_name()); }
    static QString              get_measurement_system(const QString& file_name){ return attribute (file_name, "Measurement_system","Metric");}

public:
    static Data                 get_methods(const QStringList& files);
    static bool                 get_sex(const QString& file_name)               { return attribute (file_name,"Sex","Male") =="Male";}
    static QStringList          get_growths(const QString& file_name);
    static data_name            get_proportions(const QString& file_name,  const QString& growth );
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_XML_PROPORTION_H_ */
