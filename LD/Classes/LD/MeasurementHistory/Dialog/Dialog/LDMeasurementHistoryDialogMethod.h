/* 
 * File:   LDMeasurementHistoryDialogMethod.h
 * Author: S.Panin
 *
 * Created on 18 Декабрь 2009 г., 23:11
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_H_
//------------------------------------------------------------------------------
#include "XML/XMLProportion.h"
#include "LDMeasurementHistoryDialogMethodData.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogMethod : public QDialog, go::noncopyable
{
    Q_OBJECT
    typedef                     QDialog                                         inherited;
    typedef                     vLDMeasurementHistoryDialogMethod               class_name;
    typedef                     std::map<QString, QString>                      Map;
    typedef                     vLDMeasurementHistoryDataShort::data_name       data_name;
    typedef                     vLDMeasurementHistoryDialogMethodData           Data;

    boost::shared_ptr<Data>                                                     data_;

    static QString                  settings_table()                            { return ("MeasurementHistory"); }
    static QString                  settings_key()                              { return ("DialogFormSize"); }
    void                            settings_read();
    void                            settings_write() const;
    void                            instance_signals() const;
    void                            load();
    template<class C, class S>void  set_combo_box(C* combo_box, const S& data)  { combo_box->clear(); combo_box->addItem(data);}

private slots:
    void                            method_change(const QString&);
    void                            growth_change(const QString&);
    void                            save_as_template()                          { accept(); }

public:
    explicit                        vLDMeasurementHistoryDialogMethod();
                                    ~vLDMeasurementHistoryDialogMethod()        { settings_write(); }
    data_name                       get_data() const                            {return data_->get_data(); }
    bool    test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_H_ */
