// -*- C++ -*-
/* 
 * File:   LDMeasurementHistoryDialogMethodData.h
 * Author: S.Panin
 *
 * Created on 14 Январь 2010 г., 13:43
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_DATA_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_DATA_H_
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDialogMethodData
{
typedef                     vLDMeasurementHistoryDataShort::data_name           data_name;
    static QString                 path()                                       { return const_paths::proportion();}
    static QString                 exp()                                        { return const_exp::proportion();}
    static QString                 name_dll()                                   { return const_instance_libs::ldmdmp26(); }
    std::map<QString, QString>                                                  map_;
    QLabel*                                                                     method_label_;
    QComboBox*                                                                  method_combo_box_;
    QLabel*                                                                     growth_label_;
    QComboBox*                                                                  growth_combo_box_;
    QLabel*                                                                     sex_label_;
    QComboBox*                                                                  sex_combo_box_;
    QLabel*                                                                     weight_label_;
    QLineEdit*                                                                  weight_edit_;
    QLabel*                                                                     neck_label_;
    QLineEdit*                                                                  neck_edit_;
    QLabel*                                                                     shoulders_label_;
    QLineEdit*                                                                  shoulders_edit_;
    QLabel*                                                                     chest_label_;
    QLineEdit*                                                                  chest_edit_;
    QLabel*                                                                     bicep_label_;
    QLineEdit*                                                                  bicep_edit_;
    QLabel*                                                                     forearm_label_;
    QLineEdit*                                                                  forearm_edit_;
    QLabel*                                                                     wrist_label_;
    QLineEdit*                                                                  wrist_edit_;
    QLabel*                                                                     abdomen_label_;
    QLineEdit*                                                                  abdomen_edit_;
    QLabel*                                                                     waist_label_;
    QLineEdit*                                                                  waist_edit_;
    QLabel*                                                                     hips_label_;
    QLineEdit*                                                                  hips_edit_;
    QLabel*                                                                     thigh_label_;
    QLineEdit*                                                                  thigh_edit_;
    QLabel*                                                                     calf_label_;
    QLineEdit*                                                                  calf_edit_;

    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    QPushButton*                                                                save_as_template_button_;
    QPushButton*                                                                cancel_button_;
    QGroupBox*                                                                  panel_;

void  set_data(const data_name& data)
{
        abdomen_edit_           ->setText(data.abdomen_);
        bicep_edit_             ->setText(data.bicep_);
        calf_edit_              ->setText(data.calf_);
        chest_edit_             ->setText(data.chest_);
        forearm_edit_           ->setText(data.forearm_);
        hips_edit_              ->setText(data.hips_);
        neck_edit_              ->setText(data.neck_);
        shoulders_edit_         ->setText(data.shoulders_);
        thigh_edit_             ->setText(data.thigh_);
        waist_edit_             ->setText(data.waist_);
        weight_edit_            ->setText(data.weight_);
        wrist_edit_             ->setText(data.wrist_);
}
//------------------------------------------------------------------------------
data_name  get_data() const
{
        data_name  data;
        data.abdomen_          = abdomen_edit_->text();
        data.bicep_            = bicep_edit_->text();
        data.calf_             = calf_edit_->text();
        data.chest_            = chest_edit_->text();
        data.forearm_          = forearm_edit_->text();
        data.hips_             = hips_edit_->text();
        data.neck_             = neck_edit_->text();
        data.shoulders_        = shoulders_edit_->text();
        data.thigh_            = thigh_edit_->text();
        data.waist_            = waist_edit_->text();
        data.weight_           = weight_edit_->text();
        data.wrist_            = wrist_edit_->text();
        data.growth_           = growth_combo_box_->currentText();
        return data;
}
//------------------------------------------------------------------------------
void translate()
{
        const QString cms=vMain::user().locale().height()->cms();
        method_label_   ->setText(vtr::Method::tr("&Method:"));
        growth_label_   ->setText(vtr::Body::tr("&Growth (%1):").arg(cms));
        sex_label_      ->setText(vtr::Body::tr("Se&x:"));
        weight_label_   ->setText(vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        neck_label_     ->setText(vtr::Body::tr("&Neck (%1):").arg(cms));
        shoulders_label_->setText(vtr::Body::tr("&Shoulders (%1):").arg(cms));
        chest_label_    ->setText(vtr::Body::tr("&Chest (%1):").arg(cms));
        bicep_label_    ->setText(vtr::Body::tr("&Biceps (%1):").arg(cms));
        forearm_label_  ->setText(vtr::Body::tr("&Forearms (%1):").arg(cms));
        wrist_label_    ->setText(vtr::Body::tr("W&rist (%1):").arg(cms));
        abdomen_label_  ->setText(vtr::Body::tr("&Abdomen (%1):").arg(cms));
        waist_label_    ->setText(vtr::Body::tr("Wa&ist (%1):").arg(cms));
        hips_label_     ->setText(vtr::Body::tr("Hi&ps (%1):").arg(cms));
        thigh_label_    ->setText(vtr::Body::tr("&Thigh (%1):").arg(cms));
        calf_label_     ->setText(vtr::Body::tr("Ca&lf (%1):").arg(cms));
        username_label_ ->setText(vtr::Dialog::tr("Add/Edit a Ideal Proportions"));
        save_as_template_button_->setText(go::string::get_string_with_trim(vtr::Button::tr("&Save as Template")));
        cancel_button_  ->setText(vtr::Button::tr("&Cancel"));
        username_icon_label_->setPixmap(const_pixmap_64::measurement_history_method());
}
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_METHOD_DATA_H_ */

