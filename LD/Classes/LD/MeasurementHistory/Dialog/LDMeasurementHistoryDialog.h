/* 
 * File:   LDMeasurementHistoryDialog.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_H_
//------------------------------------------------------------------------------
#include "../../../../../Go/Instance/Lay.h"
#include "../../../../../Go/BuiltType/Dialog.h"
#include "../../../../../Go/Instance/FormInstance/FormInstance.h"
#include "PanelCurrent/LDMeasurementHistoryDialogPanelCurrent.h"
#include "PanelDesired/LDMeasurementHistoryDialogPanelDesired.h"
#include "PanelMBs/LDMeasurementHistoryDialogPanelMBs.h"
#include "Dialog/LDMeasurementHistoryDialogMethod.h"
#include "LDMeasurementHistoryDialogInstance.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialog : public QDialog, go::noncopyable
{
Q_OBJECT
    friend class vLDMeasurementHistoryDialogInstance;

    typedef                     QDialog                                         inherited;
    typedef                     vLDMeasurementHistoryDialog                     class_name;
    typedef                     vLDMeasurementHistoryDialogPanelCurrent               PanelCurrent;
    typedef                     vLDMeasurementHistoryDialogPanelDesired               PanelDesired;
    typedef                     vLDMeasurementHistoryDialogPanelMBs                   PanelMBs;

    QLabel*                                                                     label_username_icon_;
    QLabel*                                                                     label_username_;
    QLabel*                                                                     label_cap_;
    PanelCurrent*                                                               panel_current_;
    PanelDesired*                                                               panel_desired_;
    PanelMBs*                                                                   panel_mbs_;
    QPushButton*                                                                button_method_;
    QPushButton*                                                                button_save_;
    QPushButton*                                                                button_cancel_;

    static QString              settings_table()                                { return ("MeasurementHistory"); }
    static QString              settings_key()                                  { return ("FormSize"); }
    void                        instance_signals();
    void                        set_enabled(bool value);
    void                        settings_read();
    void                        settings_write() const;

protected:
    virtual void                closeEvent(QCloseEvent *event);
    virtual bool                do_load(const QDate& )                          =0;
    virtual bool                do_save()                                       =0;


public slots:
    void                        save();
    void                        openDialogMethod();
    void                        panelStateChanged();
    void                        show_context_menu(const QPoint &position);


public:
    explicit                   vLDMeasurementHistoryDialog();
    virtual                    ~vLDMeasurementHistoryDialog()                   {  }
    bool                       check();
    bool                       load(const QDate& date);
    PanelCurrent*              panel_current() const                            {return panel_current_;}
    PanelDesired*              panel_desired() const                            {return panel_desired_;}
    bool                                                                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENTS_DIALOG_H_ */
