/* 
 * File:   LDMeasurementsDialogPanelMBs.h
 * Author: S.Panin
 *
 * Created on 11 Февраль 2010 г., 15:01
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_H_
//------------------------------------------------------------------------------
#include "../../../../../../Go/Component/Chart/MatchBox/MatchBox.h"
#include "LDMeasurementHistoryDialogPanelMBsPuM.h"
#include "LDMeasurementHistoryDialogPanelMBsInstance.h"
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDialogPanelMBsChartData
{
    QString                                                                     name_;
    QString                                                                     current_name_;
    QString                                                                     desired_name_;
    vLDMeasurementHistoryDialogPanelMBsChartData(const QString& name, const QString& current_name, const QString& desired_name = "Goal")
                : name_(name), current_name_(current_name), desired_name_(desired_name) {}
};
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelMBs : public vWidgetModified<QGroupBox>
{
 Q_OBJECT
    friend  class vLDMeasurementHistoryDialogPanelMBsPuM;
    friend  class vLDMeasurementHistoryDialogPanelMBsInstance;
    typedef                         vWidgetModified<QGroupBox>                          inherited;
    typedef                         vLDMeasurementHistoryData                           id_name;
    typedef                         vLDMeasurementHistoryDialogPanelMBs                       class_name;
    typedef                         QString                                             ID;
    typedef                         QMap<ID, boost::shared_ptr<vMB> >                   Data;
    typedef                         QMap<ID, vLDMeasurementHistoryDialogPanelMBsChartData>    Charts;
    typedef                         QMap<ID, QString>                                   DataSettings;

    Data                                                                        data_;
    Charts                                                                      charts_;
    vLDMeasurementHistoryDialogPanelCurrent*                                          current_panel_;
    vLDMeasurementHistoryDialogPanelDesired*                                          desired_panel_;
    QHBoxLayout*                                                                layout_;
    QScrollArea*                                                                scroll_area_;

    static QString                  settings_table()                            { return ("MeasurementHistory"); }
    static QString                  settings_key()                              { return ("PanelsMBs"); }
    void                            instance_signals();
    void                            add_panels(const DataSettings& data);
    static DataSettings             panel_all();
    static DataSettings             panel_default();
    bool                            find(const ID& id) const                    { return data_.find(id) != data_.end(); }
    QString                         get_chart_caption(const ID& id) const;
    boost::tuple<QString,QString>   get_chart_os_name(const ID& id) const;
    QString                         get_chart_settings(const ID& id) const;
    double                          get_current_data(const ID& id) const        { return current_panel_->get_data(id); }
    double                          get_desired_data(const ID& id) const        { return desired_panel_->get_data(id); }
    void                            create(const ID& id)                        { create(id, get_chart_settings(id)); }
    void                            create(const ID& id, const QString& data);
    void                            erase(const ID& id);
    void                            erase_all()                                 { data_.clear(); setVisible(false); enableForm(); }
    void                            show_all()                                  { add_panels(class_name::panel_all()); }
    void                            click(const ID& id, bool to_open)           { to_open ? create(id) : erase(id); }    
    void                            setVisible(bool value)                      { inherited::setVisible(value); enableForm(); }

signals:
    void                            on_state_changed();
    
private slots:
    void                            enableForm()                                { inherited::setWindowModified(true);  emit(on_state_changed()); }
    void                            show_context_menu(const QPoint &position);
    void                            update_chart_current_gear(const id_name::id& id, double value);
    void                            update_chart_desired_gear(const id_name::id& id , double value);

public:
    explicit                        vLDMeasurementHistoryDialogPanelMBs( vLDMeasurementHistoryDialogPanelCurrent*  current_panel,  vLDMeasurementHistoryDialogPanelDesired*  desired_panel);
    bool                            load();
    bool                            save();
    bool                            test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_H_ */
