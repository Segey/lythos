/* 
 * File:   LDMeasurementHistoryDialogPanelMBsPuM.cpp
 * Author: S.Panin
 * 
 * Created on 06 Марта 2010 г., 23:13
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelMBsPuM.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelMBsPuM::vLDMeasurementHistoryDialogPanelMBsPuM(parent_name* parent)
          : inherited(parent), parent_(parent)
{
        instance_chart();
        addSeparator();
        instance_panel_open();
        instance_panel_close();
}
//------------------------------------------------------------------------------
void  vLDMeasurementHistoryDialogPanelMBsPuM::instance_chart(QMenu* menu,  QAction* action)
{
        action->setCheckable(true);
        menu->addAction(action);
        if ( parent_->find(action->data().toString()) )     action->setChecked(true);
        connect(action, SIGNAL(triggered(bool)), SLOT(click_chart()));
}
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelMBsPuM::ActionCharts vLDMeasurementHistoryDialogPanelMBsPuM::get_action_charts()
{
        vLDMeasurementHistoryDialogPanelMBsPuM::ActionCharts array
        { {
            vActionBody::createGrowth(this),           vActionBody::createWeight(this),       vActionBody::createNeck(this),
            vActionBody::createShoulders(this),        vActionBody::createChest(this),        vActionBody::createBiceps(this),
            vActionBody::createForearms(this),         vActionBody::createWrist(this),        vActionBody::createAbdomen(this),
            vActionBody::createWaist(this),            vActionBody::createHips(this),         vActionBody::createThighs(this),
            vActionBody::createCalves(this)
          } };

        array[0]->setData( Data::growth() );    array[1]->setData( Data::weight() );   array[2]->setData( Data::neck() );
        array[3]->setData( Data::shoulders() ); array[4]->setData( Data::chest() );    array[5]->setData( Data::biceps() );
        array[6]->setData( Data::forearms() );  array[7]->setData( Data::wrist() );    array[8]->setData( Data::abdomen() );
        array[9]->setData( Data::waist() );     array[10]->setData( Data::hips() );    array[11]->setData( Data::thighs() );
        array[12]->setData( Data::calves() );
        return array;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsPuM::instance_panel_open()
{
        QAction* action= vActionChart::createPanelOpen(this);
        inherited::addAction(action);       
        if ( parent_->isVisible() )  action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_panel_open()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsPuM::instance_panel_close()
{
        QAction* action= vActionChart::createPanelClose(this);
        inherited::addAction(action);        
        if ( parent_->isVisible() == false ) action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_panel_close()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsPuM::instance_chart()
{
        QMenu* menu = inherited::addMenu(vtr::Chart::tr("Visible &Charts"));

        class_name::ActionCharts array = get_action_charts();
        std::for_each(array.begin(), array.end(), boost::bind(&class_name::instance_chart, this, menu, _1));

        menu->addSeparator();
        instance_show_all(menu);
        instance_hide_all(menu);
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsPuM::instance_show_all(QMenu* menu)
{
        QAction* action= vActionChart::createShowAll(this);
        menu->addAction(action);       
        if (parent_->data_.size() == parent_->charts_.size()) action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_show_all()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsPuM::instance_hide_all(QMenu* menu)
{
        QAction* action= vActionChart::createHideAll(this);
        menu->addAction(action);        
        if (parent_->data_.isEmpty() ) action->setVisible(false);
        else connect(action, SIGNAL(triggered(bool)), SLOT(click_hide_all()));
}
//------------------------------------------------------------------------------
/*signal*/ void  vLDMeasurementHistoryDialogPanelMBsPuM::click_panel_open()
{
        parent_->setVisible(true);
}
           //------------------------------------------------------------------------------
/*signal*/ void  vLDMeasurementHistoryDialogPanelMBsPuM::click_panel_close()
{
        parent_->setVisible(false);
}
//------------------------------------------------------------------------------
/*signal*/ void  vLDMeasurementHistoryDialogPanelMBsPuM::click_show_all()
{
        parent_->show_all();
}
//------------------------------------------------------------------------------
/*signal*/ void  vLDMeasurementHistoryDialogPanelMBsPuM::click_hide_all()
{
        parent_->erase_all();
}
//------------------------------------------------------------------------------
/*signal*/ void  vLDMeasurementHistoryDialogPanelMBsPuM::click_chart()
{
        QAction* action = (QAction*)sender();
        action->isChecked() ? parent_->create( action->data().toString() ) : parent_->erase( action->data().toString() );
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelMBsPuM::test()
{
        Q_ASSERT(parent_);
        QList<QAction*> list =inherited::actions();
        Q_ASSERT(list.size() == 4);
        Q_ASSERT(list[0]->menu()->actions().size() == 16 );
        Q_ASSERT(list[0]->text() ==vtr::Chart::tr("Visible &Charts") );
        for (int i=0 ; i < 13 ; ++i ) Q_ASSERT(list[0]->menu()->actions()[i]->isCheckable() );

        Q_ASSERT(list[1]->isSeparator());
        Q_ASSERT(list[2]->menu() ==0 );
        Q_ASSERT(list[2]->text() ==vtr::Chart::tr("&Open Chart Panel") );
        Q_ASSERT(list[3]->menu() ==0 );
        Q_ASSERT(list[3]->text() ==vtr::Chart::tr("&Close Chart Panel") );

        parent_name::Data data = parent_->data_;

        parent_->setWindowModified(false);
        Q_ASSERT( parent_->isWindowModified() == false );
        click_show_all();
        Q_ASSERT(parent_->data_.size() ==13);
        Q_ASSERT( parent_->isWindowModified() == true );

        parent_->setWindowModified(false);
        Q_ASSERT( parent_->isWindowModified() == false );
        click_hide_all();
        Q_ASSERT(parent_->data_.size() ==0);
        Q_ASSERT( parent_->isWindowModified() == true );

        Q_ASSERT( parent_->isVisible() ==false );

        parent_->setWindowModified(false);
        Q_ASSERT( parent_->isWindowModified() == false );
        click_panel_open();
        Q_ASSERT(parent_->data_.size() ==0);
        Q_ASSERT( parent_->isVisible() );
        Q_ASSERT( parent_->isWindowModified() == true );

        click_hide_all();
        Q_ASSERT(parent_->data_.size() ==0);
        Q_ASSERT( parent_->isVisible() ==false );
        Q_ASSERT(list[0]->menu()->actions()[0]->data().toString()       == Data::growth() );
        Q_ASSERT(list[0]->menu()->actions()[1]->data().toString()       == Data::weight() );
        Q_ASSERT(list[0]->menu()->actions()[2]->data().toString()       == Data::neck() );
        Q_ASSERT(list[0]->menu()->actions()[3]->data().toString()       == Data::shoulders() );
        Q_ASSERT(list[0]->menu()->actions()[4]->data().toString()       == Data::chest() );
        Q_ASSERT(list[0]->menu()->actions()[5]->data().toString()       == Data::biceps() );
        Q_ASSERT(list[0]->menu()->actions()[6]->data().toString()       == Data::forearms() );
        Q_ASSERT(list[0]->menu()->actions()[7]->data().toString()       == Data::wrist() );
        Q_ASSERT(list[0]->menu()->actions()[8]->data().toString()       == Data::abdomen() );
        Q_ASSERT(list[0]->menu()->actions()[9]->data().toString()       == Data::waist() );
        Q_ASSERT(list[0]->menu()->actions()[10]->data().toString()      == Data::hips() );
        Q_ASSERT(list[0]->menu()->actions()[11]->data().toString()      == Data::thighs() );
        Q_ASSERT(list[0]->menu()->actions()[12]->data().toString()      == Data::calves() );
        Q_ASSERT(list[0]->menu()->actions()[0]->text()                  == vtr::Body::tr("&Growth") );
        Q_ASSERT(list[0]->menu()->actions()[1]->text()                  == vtr::Body::tr("&Weight") );
        Q_ASSERT(list[0]->menu()->actions()[2]->text()                  == vtr::Body::tr("&Neck") );
        Q_ASSERT(list[0]->menu()->actions()[3]->text()                  == vtr::Body::tr("&Shoulders") );
        Q_ASSERT(list[0]->menu()->actions()[4]->text()                  == vtr::Body::tr("&Chest") );
        Q_ASSERT(list[0]->menu()->actions()[5]->text()                  == vtr::Body::tr("&Biceps") );
        Q_ASSERT(list[0]->menu()->actions()[6]->text()                  == vtr::Body::tr("&Forearms") );
        Q_ASSERT(list[0]->menu()->actions()[7]->text()                  == vtr::Body::tr("W&rist") );
        Q_ASSERT(list[0]->menu()->actions()[8]->text()                  == vtr::Body::tr("&Abdomen") );
        Q_ASSERT(list[0]->menu()->actions()[9]->text()                  == vtr::Body::tr("Wa&ist") );
        Q_ASSERT(list[0]->menu()->actions()[10]->text()                 == vtr::Body::tr("Hi&ps") );
        Q_ASSERT(list[0]->menu()->actions()[11]->text()                 == vtr::Body::tr("&Thighs") );
        Q_ASSERT(list[0]->menu()->actions()[12]->text()                 == vtr::Body::tr("Ca&lfes") );
        Q_ASSERT(list[0]->menu()->actions()[13]->isSeparator());
        Q_ASSERT(list[0]->menu()->actions()[14]->text()                 == vtr::Chart::tr("&Show All" , "Show All Charts"));
        Q_ASSERT(list[0]->menu()->actions()[15]->text()                 == vtr::Chart::tr("&Hide All" , "Hide All Charts") );        

        for (parent_name::Data::const_iterator it=data.begin() ; it != data.end(); ++it)  parent_->create(it.key());
        parent_->setWindowModified(false);
        return true;
}