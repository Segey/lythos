/* 
 * File:   LDMeasurementHistoryDialogPanelMBsInstance.h
 * Author: S.Panin
 *
 * Created on 19 Февраль 2010 г., 14:12
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_INSTANCE_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_INSTANCE_H_
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelMBs;
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelMBsInstance : go::noncopyable
{
    typedef                     vLDMeasurementHistoryDialogPanelMBsInstance     class_name;
    typedef                     vLDMeasurementHistoryDialogPanelMBs*            inherited;

    inherited                                                                   parent_;

    void                        instance_charts_names();
    void                        instance_widgets();
    void                        instance_scroll_area();

public:
    explicit vLDMeasurementHistoryDialogPanelMBsInstance(inherited parent = 0) : parent_(parent){ }
    void                        instance();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_INSTANCE_H_ */
