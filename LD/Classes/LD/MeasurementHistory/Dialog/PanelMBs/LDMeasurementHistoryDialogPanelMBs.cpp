/* 
 * File:   LDMeasurementHistoryDialogPanelMBs.cpp
 * Author: S.Panin
 * 
 * Created on 11 Февраль 2010 г., 15:01
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelMBs.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelMBs::vLDMeasurementHistoryDialogPanelMBs(vLDMeasurementHistoryDialogPanelCurrent*  current_panel,  vLDMeasurementHistoryDialogPanelDesired*  desired_panel)
                :  current_panel_(current_panel), desired_panel_(desired_panel)
{
        vLDMeasurementHistoryDialogPanelMBsInstance(this).instance();
        instance_signals();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBs::instance_signals()
{
      connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(show_context_menu(const QPoint &)));
      connect(current_panel_, SIGNAL(on_changed_values(const id_name::id& , double )), SLOT(update_chart_current_gear(const id_name::id& , double )));
      connect(desired_panel_, SIGNAL(on_changed_values(const id_name::id& , double )), SLOT(update_chart_desired_gear(const id_name::id& , double )));
}
//------------------------------------------------------------------------------
void   vLDMeasurementHistoryDialogPanelMBs::update_chart_current_gear(const id_name::id& id, double value)
{
        Data::iterator it = data_.find(id);
        if (it != data_.end()) it.value()->set_value_first(get_chart_os_name(id).get<0>(), value);
}
//------------------------------------------------------------------------------
void   vLDMeasurementHistoryDialogPanelMBs::update_chart_desired_gear(const id_name::id& id, double value)
{
        Data::iterator it = data_.find(id);
        if (it != data_.end()) it.value()->set_value_second(get_chart_os_name(id).get<1>(), value);
}
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelMBs::DataSettings vLDMeasurementHistoryDialogPanelMBs::panel_all()
{
        DataSettings panel_alls;
        panel_alls.insert(id_name::growth(), "");
        panel_alls.insert(id_name::biceps(), "");
        panel_alls.insert(id_name::abdomen(), "");
        panel_alls.insert(id_name::calves(), "");
        panel_alls.insert(id_name::chest(), "");
        panel_alls.insert(id_name::forearms(), "");
        panel_alls.insert(id_name::hips(), "");
        panel_alls.insert(id_name::neck(), "");
        panel_alls.insert(id_name::shoulders(), "");
        panel_alls.insert(id_name::thighs(), "");
        panel_alls.insert(id_name::waist(), "");
        panel_alls.insert(id_name::weight(), "");
        panel_alls.insert(id_name::wrist(), "");
        return panel_alls;
}
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelMBs::DataSettings vLDMeasurementHistoryDialogPanelMBs::panel_default()
{
        DataSettings panel_default;
        panel_default.insert(id_name::growth(), "");
        panel_default.insert(id_name::weight(), "");
        panel_default.insert(id_name::chest(), "");
        panel_default.insert(id_name::biceps(), "");
        panel_default.insert(id_name::abdomen(), "");
        return panel_default;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBs::add_panels(const DataSettings& data )
{
        for(vLDMeasurementHistoryDialogPanelMBs::DataSettings::const_iterator it =data.begin(); it !=data.end(); ++it )
                create (it.key(), it.value());
}
//------------------------------------------------------------------------------
QString vLDMeasurementHistoryDialogPanelMBs::get_chart_caption(const ID& id) const
{
        Charts::const_iterator it = charts_.find(id);
        return it == charts_.end() ? QString() : it->name_;
}
//------------------------------------------------------------------------------
boost::tuple<QString,QString> vLDMeasurementHistoryDialogPanelMBs::get_chart_os_name(const ID& id) const
{
        Charts::const_iterator it = charts_.find(id);
        return it == charts_.end() ? boost::make_tuple(QString(),QString())
                : boost::make_tuple(it->current_name_,it->desired_name_);
}
//------------------------------------------------------------------------------
QString vLDMeasurementHistoryDialogPanelMBs::get_chart_settings(const ID& id) const
{
        DataSettings data =vSettings::get(settings_table(), settings::make_map(settings_key(), DataSettings())).get<1>();

        DataSettings::const_iterator it = data.find(id);
        return it == data.end() ? QString() : it.value();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBs::create(const ID& id, const QString& data)
{
        const boost::tuple<QString,QString> names = get_chart_os_name(id);

        vMB* mb = new vMB(this, data);
        mb->set_caption(get_chart_caption(id));
        mb->set_value_first(names.get<0>(),get_current_data(id));
        mb->set_value_second(names.get<1>(),get_desired_data(id));
        layout_->insertWidget(layout_->count() -1, mb);
        data_.insert(id,boost::shared_ptr<vMB>(mb));
        connect(mb, SIGNAL(on_state_changed()), SLOT(enableForm()));
      //  layout_->addWidget(vWidgetLine::VLine(this));
        if ( isVisible() ==false ) setVisible(true);
        enableForm();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBs::erase(const ID& id)
{
        Data::iterator it = data_.find(id);
        if (it == data_.end()) return;

        data_.erase(it);
        if (layout_->count() < 2) setVisible(false);
        enableForm();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelMBs::load()
{
        DataSettings data =vSettings::get(settings_table(), settings::make_map(settings_key(), DataSettings())).get<1>();

        add_panels(data);
        set_modified_enabled(true);
        return true;
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelMBs::save()
{       
        DataSettings data_settings;
        for(Data::const_iterator it =data_.begin(); it !=data_.end(); ++it )
                data_settings.insert(it.key(), it.value()->get_settings());
        
        vSettings::set(settings_table(), settings::make_map(settings_key(), data_settings));
        setWindowModified(false);
        return true;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBs::show_context_menu(const QPoint &position)
{
        vLDMeasurementHistoryDialogPanelMBsPuM pum (this);
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelMBs::test()
{
        Q_ASSERT(current_panel_);
        Q_ASSERT(desired_panel_);
        Q_ASSERT(layout_);
        Q_ASSERT(charts_.size() ==13);
        
        vLDMeasurementHistoryDialogPanelMBs::DataSettings data_all = panel_all();
        Q_ASSERT(data_all.size() ==13);

        data_all = panel_default();
        Q_ASSERT(data_all.size() ==5);

        Data data = data_;
        erase_all();
        Q_ASSERT(data_.size() ==false);
        create(id_name::neck(),"22 3");
        create(id_name::growth()," 3");
        Q_ASSERT(data_.size() ==2);
        erase(id_name::growth());
        Q_ASSERT(data_.size() ==1);
        erase(id_name::growth());
        Q_ASSERT(data_.size() ==1);
        erase(id_name::neck());
        Q_ASSERT(data_.size() ==0);

        for (Data::const_iterator it=data.begin() ; it != data.end(); ++it)  create(it.key());
        Q_ASSERT(vLDMeasurementHistoryDialogPanelMBsPuM(this).test());
        
        vMain::user().set_test_id();
        DataSettings map;
        map["1"] = "888";
        map["2"] = "444";
        map["3"] = "000";
        vSettings::set(settings_table(), settings::make_map(settings_key(), map));
        Q_ASSERT(vSettings::get(settings_table(), settings::make_map(settings_key(), DataSettings())).get<0>() );
        Q_ASSERT(vSettings::get(settings_table(), settings::make_map(settings_key(), DataSettings())).get<1>() == map);

        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------