/* 
 * File:   LDMeasurementHistoryDialogPanelMBsPuM.h
 * Author: S.Panin
 *
* Created on 06 Марта 2010 г., 23:13
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_PUM_H_
#define _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_PUM_H_
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelMBs;
class vLDMeasurementHistoryData;
//------------------------------------------------------------------------------
#include "../../../../../../Go/BuiltType/Action/Body/ActionBody.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelMBsPuM : public QMenu
{
Q_OBJECT

    typedef                     QMenu                                           inherited;
    typedef                     vLDMeasurementHistoryDialogPanelMBsPuM          class_name;
    typedef                     vLDMeasurementHistoryDialogPanelMBs             parent_name;
    typedef                     vLDMeasurementHistoryData                       Data;
    typedef                     boost::array<QAction*, 13>                      ActionCharts;

    parent_name*                                                                parent_;

    void                        instance_panel_open();
    void                        instance_panel_close();
    void                        instance_chart();
    void                        instance_show_all(QMenu* menu);
    void                        instance_hide_all(QMenu* menu);
    ActionCharts                get_action_charts();
    void                        instance_chart(QMenu* menu, QAction* action);

private slots:
    void                        click_panel_open();
    void                        click_panel_close();
    void                        click_show_all();
    void                        click_hide_all();
    void                        click_chart();

public:
    explicit                    vLDMeasurementHistoryDialogPanelMBsPuM(parent_name* parent);
    bool test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_MBS_PUM_H_ */
