/* 
 * File:   LDMeasurementHistoryDialogPanelMBsInstance.cpp
 * Author: S.Panin
 * 
 * Created on 19 Февраль 2010 г., 14:12
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelMBsInstance.h"
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsInstance::instance()
{       
        instance_charts_names();
        instance_widgets();
        instance_scroll_area();
               
        parent_->setMaximumHeight(vMB_base::maximumHeight() + 50);
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsInstance::instance_charts_names()
{
        parent_->charts_.insert(vLDMeasurementHistoryData::growth(),        vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Growth"),          vtr::Body::tr("Growth")));
        parent_->charts_.insert(vLDMeasurementHistoryData::biceps(),        vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Biceps"),          vtr::Body::tr("Biceps")));
        parent_->charts_.insert(vLDMeasurementHistoryData::abdomen(),       vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Abdomen"),         vtr::Body::tr("Abdomen")));
        parent_->charts_.insert(vLDMeasurementHistoryData::calves(),        vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Calves"),          vtr::Body::tr("Calves")));
        parent_->charts_.insert(vLDMeasurementHistoryData::chest(),         vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Chest"),           vtr::Body::tr("Chest")));
        parent_->charts_.insert(vLDMeasurementHistoryData::forearms(),      vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Forearms"),        vtr::Body::tr("Forearms")));
        parent_->charts_.insert(vLDMeasurementHistoryData::hips(),          vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Hips"),            vtr::Body::tr("Hips")));
        parent_->charts_.insert(vLDMeasurementHistoryData::neck(),          vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Neck"),            vtr::Body::tr("Neck")));
        parent_->charts_.insert(vLDMeasurementHistoryData::shoulders(),     vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Shoulders"),       vtr::Body::tr("Shoulders")));
        parent_->charts_.insert(vLDMeasurementHistoryData::thighs(),        vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Thighs"),          vtr::Body::tr("Thighs")));
        parent_->charts_.insert(vLDMeasurementHistoryData::waist(),         vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Waist"),           vtr::Body::tr("Waist")));
        parent_->charts_.insert(vLDMeasurementHistoryData::weight(),        vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Weight"),          vtr::Body::tr("Weight")));
        parent_->charts_.insert(vLDMeasurementHistoryData::wrist(),         vLDMeasurementHistoryDialogPanelMBsChartData(vtr::Body::tr("Wrist"),           vtr::Body::tr("Wrist")));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsInstance::instance_widgets()
{
        parent_->setVisible(false);
        parent_->setContextMenuPolicy(Qt::CustomContextMenu);
        
        QWidget* widget                 = new QWidget;
        QHBoxLayout* layout             = new QHBoxLayout(parent_);
        parent_->layout_                = new QHBoxLayout(widget);
        parent_->scroll_area_           = new QScrollArea;        

        layout->addWidget(parent_->scroll_area_);
        parent_->scroll_area_->setWidget(widget);
        parent_->layout_->addStretch();              
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelMBsInstance::instance_scroll_area()
{
        parent_->scroll_area_->setWidgetResizable(true);
        parent_->scroll_area_->setFocusPolicy(Qt::NoFocus);
}

