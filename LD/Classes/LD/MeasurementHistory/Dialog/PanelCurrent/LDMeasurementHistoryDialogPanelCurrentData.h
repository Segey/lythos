/* 
 * File:   vLDMeasurementHistoryDialogPanelCurrentData.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 12:16
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_CURRENT_DATA_H_
#define _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_CURRENT_DATA_H_
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDialogPanelCurrentData
{
    typedef                     vLDMeasurementHistoryData                       id_name;
    typedef                     vLDMeasurementHistoryDataLong::data_name        data_name;
    static QString              name_dll()                                      { return const_instance_libs::ldmdpl1223(); }
    static std::string          property_name()                                 { return "name"; }

    QLabel*                                                                     date_label_;
    QDateEdit*                                                                  date_edit_;
    QLabel*                                                                     growth_label_;
    QLineEdit*                                                                  growth_edit_;
    QLabel*                                                                     weight_label_;
    QLineEdit*                                                                  weight_edit_;
    QLabel*                                                                     neck_label_;
    QLineEdit*                                                                  neck_edit_;
    QLabel*                                                                     shoulders_label_;
    QLineEdit*                                                                  shoulders_edit_;
    QLabel*                                                                     chest_label_;
    QLineEdit*                                                                  chest_edit_;
    QLabel*                                                                     bicep_left_label_;
    QLineEdit*                                                                  bicep_left_edit_;
    QLabel*                                                                     bicep_right_label_;
    QLineEdit*                                                                  bicep_right_edit_;
    QLabel*                                                                     forearm_left_label_;
    QLineEdit*                                                                  forearm_left_edit_;
    QLabel*                                                                     forearm_right_label_;
    QLineEdit*                                                                  forearm_right_edit_;
    QLabel*                                                                     wrist_left_label_;
    QLineEdit*                                                                  wrist_left_edit_;
    QLabel*                                                                     wrist_right_label_;
    QLineEdit*                                                                  wrist_right_edit_;
    QLabel*                                                                     abdomen_label_;
    QLineEdit*                                                                  abdomen_edit_;
    QLabel*                                                                     waist_label_;
    QLineEdit*                                                                  waist_edit_;
    QLabel*                                                                     hips_label_;
    QLineEdit*                                                                  hips_edit_;
    QLabel*                                                                     thigh_left_label_;
    QLineEdit*                                                                  thigh_left_edit_;
    QLabel*                                                                     thigh_right_label_;
    QLineEdit*                                                                  thigh_right_edit_;
    QLabel*                                                                     calf_left_label_;
    QLineEdit*                                                                  calf_left_edit_;
    QLabel*                                                                     calf_right_label_;
    QLineEdit*                                                                  calf_right_edit_;

void  set_data(const data_name& data)
{
        growth_edit_            ->setText(data.growth_);   
        abdomen_edit_           ->setText(data.abdomen_);
        bicep_left_edit_        ->setText(data.bicep_left_);
        bicep_right_edit_       ->setText(data.bicep_right_);
        calf_left_edit_         ->setText(data.calf_left_);
        calf_right_edit_        ->setText(data.calf_right_);
        chest_edit_             ->setText(data.chest_);
        forearm_left_edit_      ->setText(data.forearm_left_);
        forearm_right_edit_     ->setText(data.forearm_right_);
        neck_edit_              ->setText(data.neck_);
        shoulders_edit_         ->setText(data.shoulders_);
        thigh_left_edit_        ->setText(data.thigh_left_);
        thigh_right_edit_       ->setText(data.thigh_right_);
        waist_edit_             ->setText(data.waist_);
        hips_edit_              ->setText(data.hips_);
        weight_edit_            ->setText(data.weight_);
        wrist_left_edit_        ->setText(data.wrist_left_);
        wrist_right_edit_       ->setText(data.wrist_right_);
}
//------------------------------------------------------------------------------
data_name  get_data() const
{
        data_name       data;
        data.growth_            = growth_edit_->text();
        data.abdomen_           = abdomen_edit_->text();
        data.bicep_left_        = bicep_left_edit_->text();
        data.bicep_right_       = bicep_right_edit_->text();
        data.calf_left_         = calf_left_edit_->text();
        data.calf_right_        = calf_right_edit_->text();
        data.chest_             = chest_edit_->text();
        data.forearm_left_      = forearm_left_edit_->text();
        data.forearm_right_     = forearm_right_edit_->text();
        data.neck_              = neck_edit_->text();
        data.shoulders_         = shoulders_edit_->text();
        data.thigh_left_        = thigh_left_edit_->text();
        data.thigh_right_       = thigh_right_edit_->text();
        data.waist_             = waist_edit_->text();
        data.hips_              = hips_edit_->text();
        data.weight_            = weight_edit_->text();
        data.wrist_left_        = wrist_left_edit_->text();
        data.wrist_right_       = wrist_right_edit_->text();
        return data;
}
//------------------------------------------------------------------------------
void translate()
{
        const QString cms=vMain::user().locale().height()->cms();
        date_label_            ->setText(vtr::Date::tr("&Date:"));
        growth_label_          ->setText(vtr::Body::tr("&Growth (%1):").arg(cms));
        weight_label_          ->setText(vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        neck_label_            ->setText(vtr::Body::tr("&Neck (%1):").arg(cms));
        shoulders_label_       ->setText(vtr::Body::tr("&Shoulders (%1):").arg(cms));
        chest_label_           ->setText(vtr::Body::tr("&Chest (%1):").arg(cms));
        bicep_left_label_      ->setText(vtr::Body::tr("&Bicep left (%1):").arg(cms));
        bicep_right_label_     ->setText(vtr::Body::tr("&Bicep right (%1):").arg(cms));
        forearm_left_label_    ->setText(vtr::Body::tr("&Forearm left (%1):").arg(cms));
        forearm_right_label_   ->setText(vtr::Body::tr("&Forearm right (%1):").arg(cms));
        wrist_left_label_      ->setText(vtr::Body::tr("W&rist left (%1):").arg(cms));
        wrist_right_label_     ->setText(vtr::Body::tr("W&rist right (%1):").arg(cms));
        abdomen_label_         ->setText(vtr::Body::tr("&Abdomen (%1):").arg(cms));
        waist_label_           ->setText(vtr::Body::tr("Wa&ist (%1):").arg(cms));
        hips_label_            ->setText(vtr::Body::tr("Hi&ps (%1):").arg(cms));
        thigh_left_label_      ->setText(vtr::Body::tr("&Thigh left (%1):").arg(cms));
        thigh_right_label_     ->setText(vtr::Body::tr("&Thigh right (%1):").arg(cms));
        calf_left_label_       ->setText(vtr::Body::tr("Ca&lf left (%1):").arg(cms));
        calf_right_label_      ->setText(vtr::Body::tr("Ca&lf right (%1):").arg(cms));
}
//------------------------------------------------------------------------------
void instance_names()
{
        growth_edit_        ->setProperty(property_name().c_str(), id_name::growth());
        weight_edit_        ->setProperty(property_name().c_str(), id_name::weight());
        neck_edit_          ->setProperty(property_name().c_str(), id_name::neck());
        shoulders_edit_     ->setProperty(property_name().c_str(), id_name::shoulders());
        chest_edit_         ->setProperty(property_name().c_str(), id_name::chest());
        bicep_right_edit_   ->setProperty(property_name().c_str(), id_name::biceps());
        forearm_right_edit_ ->setProperty(property_name().c_str(), id_name::forearms());
        wrist_right_edit_   ->setProperty(property_name().c_str(), id_name::wrist());
        abdomen_edit_       ->setProperty(property_name().c_str(), id_name::abdomen());
        waist_edit_         ->setProperty(property_name().c_str(), id_name::waist());
        hips_edit_          ->setProperty(property_name().c_str(), id_name::hips());
        thigh_right_edit_   ->setProperty(property_name().c_str(), id_name::thighs());
        calf_right_edit_    ->setProperty(property_name().c_str(), id_name::calves());
}
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_CURRENT_DATA_H_ */
