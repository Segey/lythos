/* 
 * File:   LDMeasurementHistoryDialogPanelCurrent.cpp
 * Author: S.Panin
 * 
 * Created on 18 Декабрь 2009 г., 23:11
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelCurrent.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelCurrent::vLDMeasurementHistoryDialogPanelCurrent(QWidget* parent /*=0*/)
            : inherited(parent), data_(new Data)
{
        vDllInstance::instance(this, data_.get());       
        data_->instance_names();
        data_->translate();
        instance_signals();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelCurrent::instance_signals() const
{
        connect(data_->date_edit_,          SIGNAL(dateChanged ( const QDate& )), SLOT(enableForm()));
        connect(data_->growth_edit_,        SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->weight_edit_,        SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->neck_edit_,          SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->shoulders_edit_,     SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->chest_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->bicep_left_edit_,    SIGNAL(textChanged(const QString &)), SLOT(enableForm()));
        connect(data_->bicep_right_edit_,   SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->forearm_left_edit_,  SIGNAL(textChanged(const QString &)), SLOT(enableForm()));
        connect(data_->forearm_right_edit_, SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->wrist_left_edit_,    SIGNAL(textChanged(const QString &)), SLOT(enableForm()));
        connect(data_->wrist_right_edit_,   SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->abdomen_edit_,       SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->waist_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->hips_edit_,          SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->thigh_left_edit_,    SIGNAL(textChanged(const QString &)), SLOT(enableForm()));
        connect(data_->thigh_right_edit_,   SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
        connect(data_->calf_left_edit_,     SIGNAL(textChanged(const QString &)), SLOT(enableForm()));
        connect(data_->calf_right_edit_,    SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString& )));
}
//------------------------------------------------------------------------------
void    vLDMeasurementHistoryDialogPanelCurrent::changed_values(const QString& str)
{
        QLineEdit *edit = qobject_cast<QLineEdit *>(sender());
        if ( !edit && !v_user::is_double(str, true) ) return;
        emit(on_changed_values(edit->property(Data::property_name().c_str()).toString(), v_user::get_double(str)));
        enableForm();
}
//------------------------------------------------------------------------------
double  vLDMeasurementHistoryDialogPanelCurrent::get_data(const QString& id ) const
{
        typedef vLDMeasurementHistoryData id_name;
        if(id ==id_name::abdomen())     return v_user::get_double(data_->abdomen_edit_->text());
        if(id ==id_name::biceps())      return std::max(v_user::get_double(data_->bicep_left_edit_->text()), v_user::get_double(data_->bicep_right_edit_->text()));
        if(id ==id_name::calves())      return std::max(v_user::get_double(data_->calf_left_edit_->text()), v_user::get_double(data_->calf_right_edit_->text()));
        if(id ==id_name::chest())       return v_user::get_double(data_->chest_edit_->text());
        if(id ==id_name::forearms())    return std::max(v_user::get_double(data_->forearm_left_edit_->text()), v_user::get_double(data_->forearm_right_edit_->text()));
        if(id ==id_name::hips())        return v_user::get_double(data_->hips_edit_->text());
        if(id ==id_name::neck())        return v_user::get_double(data_->neck_edit_->text());
        if(id ==id_name::shoulders())   return v_user::get_double(data_->shoulders_edit_->text());
        if(id ==id_name::thighs())      return std::max(v_user::get_double(data_->thigh_left_edit_->text()), v_user::get_double(data_->thigh_right_edit_->text()));
        if(id ==id_name::waist())       return v_user::get_double(data_->waist_edit_->text());
        if(id ==id_name::weight())      return v_user::get_double(data_->weight_edit_->text());
        if(id ==id_name::wrist())       return std::max(v_user::get_double(data_->wrist_left_edit_->text()), v_user::get_double(data_->wrist_right_edit_->text()));
        return double();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelCurrent::check() const
{
        boost::array<QLineEdit*, 18> vec =
        {{
                data_->growth_edit_,            data_->weight_edit_,            data_->neck_edit_,
                data_->shoulders_edit_,         data_->chest_edit_,             data_->bicep_left_edit_,
                data_->bicep_right_edit_,       data_->forearm_left_edit_,      data_->forearm_right_edit_,
                data_->wrist_left_edit_,        data_->wrist_right_edit_,       data_->abdomen_edit_,
                data_->waist_edit_,             data_->hips_edit_,              data_->thigh_left_edit_,
                data_->thigh_right_edit_,       data_->calf_left_edit_,         data_->calf_right_edit_
         }} ;
        return vValidator::is_number_with_migalo(vec.begin(), vec.end(), &v_user::is_double, true);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelCurrent::test()
{
  //      Q_ASSERT(parent_);
        Q_ASSERT(data_->date_label_);
        Q_ASSERT(data_->date_edit_);
        Q_ASSERT(data_->growth_label_);
        Q_ASSERT(data_->growth_edit_);
        Q_ASSERT(data_->weight_label_);
        Q_ASSERT(data_->weight_edit_);
        Q_ASSERT(data_->neck_label_);
        Q_ASSERT(data_->neck_edit_);
        Q_ASSERT(data_->shoulders_label_);
        Q_ASSERT(data_->shoulders_edit_);
        Q_ASSERT(data_->chest_label_);
        Q_ASSERT(data_->chest_edit_);
        Q_ASSERT(data_->bicep_left_label_);
        Q_ASSERT(data_->bicep_left_edit_);
        Q_ASSERT(data_->bicep_right_label_);
        Q_ASSERT(data_->bicep_right_edit_);
        Q_ASSERT(data_->forearm_left_label_);
        Q_ASSERT(data_->forearm_left_edit_);
        Q_ASSERT(data_->forearm_right_label_);
        Q_ASSERT(data_->forearm_right_edit_);
        Q_ASSERT(data_->wrist_left_label_);
        Q_ASSERT(data_->wrist_left_edit_);
        Q_ASSERT(data_->wrist_right_label_);
        Q_ASSERT(data_->wrist_right_edit_);
        Q_ASSERT(data_->abdomen_label_);
        Q_ASSERT(data_->abdomen_edit_);
        Q_ASSERT(data_->waist_label_);
        Q_ASSERT(data_->waist_edit_);
        Q_ASSERT(data_->hips_label_);
        Q_ASSERT(data_->hips_edit_);
        Q_ASSERT(data_->thigh_left_label_);
        Q_ASSERT(data_->thigh_left_edit_);
        Q_ASSERT(data_->thigh_right_label_);
        Q_ASSERT(data_->thigh_right_edit_);
        Q_ASSERT(data_->calf_left_label_);
        Q_ASSERT(data_->calf_left_edit_);
        Q_ASSERT(data_->calf_right_label_);
        Q_ASSERT(data_->calf_right_edit_);

        const QString& cms=vMain::user().locale().height()->cms();
        Q_ASSERT( data_->date_label_            ->text() == vtr::Date::tr("&Date:"));
        Q_ASSERT( data_->growth_label_          ->text() == vtr::Body::tr("&Growth (%1):").arg(cms));
        Q_ASSERT( data_->weight_label_          ->text() == vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        Q_ASSERT( data_->neck_label_            ->text() == vtr::Body::tr("&Neck (%1):").arg(cms));
        Q_ASSERT( data_->shoulders_label_       ->text() == vtr::Body::tr("&Shoulders (%1):").arg(cms));
        Q_ASSERT( data_->chest_label_           ->text() == vtr::Body::tr("&Chest (%1):").arg(cms));
        Q_ASSERT( data_->bicep_left_label_      ->text() == vtr::Body::tr("&Bicep left (%1):").arg(cms));
        Q_ASSERT( data_->bicep_right_label_     ->text() == vtr::Body::tr("&Bicep right (%1):").arg(cms));
        Q_ASSERT( data_->forearm_left_label_    ->text() == vtr::Body::tr("&Forearm left (%1):").arg(cms));
        Q_ASSERT( data_->forearm_right_label_   ->text() == vtr::Body::tr("&Forearm right (%1):").arg(cms));
        Q_ASSERT( data_->wrist_left_label_      ->text() == vtr::Body::tr("W&rist left (%1):").arg(cms));
        Q_ASSERT( data_->wrist_right_label_     ->text() == vtr::Body::tr("W&rist right (%1):").arg(cms));
        Q_ASSERT( data_->abdomen_label_         ->text() == vtr::Body::tr("&Abdomen (%1):").arg(cms));
        Q_ASSERT( data_->waist_label_           ->text() == vtr::Body::tr("Wa&ist (%1):").arg(cms));
        Q_ASSERT( data_->hips_label_            ->text() == vtr::Body::tr("Hi&ps (%1):").arg(cms));
        Q_ASSERT( data_->thigh_left_label_      ->text() == vtr::Body::tr("&Thigh left (%1):").arg(cms));
        Q_ASSERT( data_->thigh_right_label_     ->text() == vtr::Body::tr("&Thigh right (%1):").arg(cms));
        Q_ASSERT( data_->calf_left_label_       ->text() == vtr::Body::tr("Ca&lf left (%1):").arg(cms));
        Q_ASSERT( data_->calf_right_label_      ->text() == vtr::Body::tr("Ca&lf right (%1):").arg(cms));

        Q_ASSERT( data_->growth_edit_        ->property(data_->property_name().c_str()) == id_name::growth());
        Q_ASSERT( data_->weight_edit_        ->property(data_->property_name().c_str()) == id_name::weight());
        Q_ASSERT( data_->neck_edit_          ->property(data_->property_name().c_str()) == id_name::neck());
        Q_ASSERT( data_->shoulders_edit_     ->property(data_->property_name().c_str()) == id_name::shoulders());
        Q_ASSERT( data_->chest_edit_         ->property(data_->property_name().c_str()) == id_name::chest());
        Q_ASSERT( data_->bicep_right_edit_   ->property(data_->property_name().c_str()) == id_name::biceps());
        Q_ASSERT( data_->forearm_right_edit_ ->property(data_->property_name().c_str()) == id_name::forearms());
        Q_ASSERT( data_->wrist_right_edit_   ->property(data_->property_name().c_str()) == id_name::wrist());
        Q_ASSERT( data_->abdomen_edit_       ->property(data_->property_name().c_str()) == id_name::abdomen());
        Q_ASSERT( data_->waist_edit_         ->property(data_->property_name().c_str()) == id_name::waist());
        Q_ASSERT( data_->hips_edit_          ->property(data_->property_name().c_str()) == id_name::hips());
        Q_ASSERT( data_->thigh_right_edit_   ->property(data_->property_name().c_str()) == id_name::thighs());
        Q_ASSERT( data_->calf_right_edit_    ->property(data_->property_name().c_str()) == id_name::calves());

        data_->abdomen_edit_           ->setText(" ");
        data_->bicep_left_edit_        ->setText("99");
        data_->bicep_right_edit_       ->setText("-");
        data_->calf_left_edit_         ->setText("hhh");
        data_->calf_right_edit_        ->setText("yy8");
        data_->chest_edit_             ->setText("gg40");
        data_->forearm_left_edit_      ->setText("n553");
        data_->forearm_right_edit_     ->setText("f555");
        data_->neck_edit_              ->setText("3se");
        data_->shoulders_edit_         ->setText("44sd");
        data_->thigh_left_edit_        ->setText("dsr");
        data_->thigh_right_edit_       ->setText("bvb");
        data_->waist_edit_             ->setText("88");
        data_->hips_edit_              ->setText("vbvbn");
        data_->weight_edit_            ->setText("0000");
        data_->wrist_left_edit_        ->setText("777");
        data_->wrist_right_edit_       ->setText("i");

        data_name  data = get_data();

         Q_ASSERT( data.abdomen_            == " ");
         Q_ASSERT( data.bicep_left_         == "99");
         Q_ASSERT( data.bicep_right_        == "-");
         Q_ASSERT( data.calf_left_          == "hhh");
         Q_ASSERT( data.calf_right_         == "yy8");
         Q_ASSERT( data.chest_              == "gg40");
         Q_ASSERT( data.forearm_left_       == "n553");
         Q_ASSERT( data.forearm_right_      == "f555");
         Q_ASSERT( data.neck_               == "3se");
         Q_ASSERT( data.shoulders_          == "44sd");
         Q_ASSERT( data.thigh_left_         == "dsr");
         Q_ASSERT( data.thigh_right_        == "bvb");
         Q_ASSERT( data.waist_              == "88");
         Q_ASSERT( data.hips_               == "vbvbn");
         Q_ASSERT( data.weight_             == "0000");
         Q_ASSERT( data.wrist_left_         == "777");
         Q_ASSERT( data.wrist_right_        == "i");


          data.abdomen_            = " 0";
          data.bicep_left_         = "990";
          data.bicep_right_        = "-0";
          data.calf_left_          = "hhh0";
          data.calf_right_         = "yy80";
          data.chest_              = "gg400";
          data.forearm_left_       = "n5530";
          data.forearm_right_      = "f5550";
          data.neck_               = "3se-";
          data.shoulders_          = "44sd-";
          data.thigh_left_         = "dsr-";
          data.thigh_right_        = "bvb-";
          data.waist_              = "88-";
          data.hips_               = "vbvbn-";
          data.weight_             = "0000-";
          data.wrist_left_         = "777-";
          data.wrist_right_        = "i-";

          set_data(data);

         Q_ASSERT( data_->abdomen_edit_->text()            == " 0");
         Q_ASSERT( data_->bicep_left_edit_->text()         == "990");
         Q_ASSERT( data_->bicep_right_edit_->text()        == "-0");
         Q_ASSERT( data_->calf_left_edit_->text()          == "hhh0");
         Q_ASSERT( data_->calf_right_edit_->text()         == "yy80");
         Q_ASSERT( data_->chest_edit_->text()              == "gg400");
         Q_ASSERT( data_->forearm_left_edit_->text()       == "n5530");
         Q_ASSERT( data_->forearm_right_edit_->text()      == "f5550");
         Q_ASSERT( data_->neck_edit_->text()               == "3se-");
         Q_ASSERT( data_->shoulders_edit_->text()          == "44sd-");
         Q_ASSERT( data_->thigh_left_edit_->text()         == "dsr-");
         Q_ASSERT( data_->thigh_right_edit_->text()        == "bvb-");
         Q_ASSERT( data_->waist_edit_->text()              == "88-");
         Q_ASSERT( data_->hips_edit_->text()               == "vbvbn-");
         Q_ASSERT( data_->weight_edit_->text()             == "0000-");
         Q_ASSERT( data_->wrist_left_edit_->text()         == "777-");
         Q_ASSERT( data_->wrist_right_edit_->text()        == "i-");

         setWindowModified(false);
         return true;
}
//------------------------------------------------------------------------------