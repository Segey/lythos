/* 
 * File:   vLDMeasurementHistoryDialogPanelDesired.h
 * Author: S.Panin
 *
 * Created on 18 Декабрь 2009 г., 23:11
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_V_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_V_
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelDesiredData.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogPanelDesired : public vWidgetModified<QGroupBox>, go::noncopyable
{
Q_OBJECT

public:
    typedef                     vLDMeasurementHistoryDataShort::data_name       data_name;

private:
    typedef                     vWidgetModified<QGroupBox>                      inherited;
    typedef                     vLDMeasurementHistoryDialogPanelDesired         class_name;
    typedef                     vLDMeasurementHistoryDialogPanelDesiredData     Data;
    typedef                     vLDMeasurementHistoryData                       id_name;

    QWidget*                                                                    parent_;
    boost::shared_ptr<Data>                                                     data_;

    void                        instance_signals() const;

signals:
    void                        on_state_changed();
    void                        on_changed_values(const id_name::id& id , double value);

private slots:
    void                        enableForm()                                    { setWindowModified(true);  emit(on_state_changed());}
    void                        changed_values(const QString& str);

public:
    explicit                    vLDMeasurementHistoryDialogPanelDesired(QWidget* parent = 0);
    bool                        check() const;
    QDate                       get_date() const                                { return data_->date_edit_->date(); }
    void                        set_date(const QDate& date)                     { data_->date_edit_->setDate(date); }
    void                        set_data(const data_name& data)                 { data_->set_data(data);}
    data_name                   get_data() const                                { return data_->get_data();}
    double                      get_data(const QString& id ) const;
    bool    test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_V_ */
