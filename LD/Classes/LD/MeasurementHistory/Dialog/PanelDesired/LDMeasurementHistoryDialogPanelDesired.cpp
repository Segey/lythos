/* 
 * File:   vLDMeasurementHistoryDialogPanelDesired.cpp
 * Author: S.Panin
 * 
 * Created on 18 Декабрь 2009 г., 23:11
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialogPanelDesired.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialogPanelDesired::vLDMeasurementHistoryDialogPanelDesired(QWidget* parent /*=0*/)
            : inherited(parent), data_(new Data)
{
        vDllInstance::instance(this, data_.get());
        data_->instance_names();
        data_->translate();
        instance_signals();
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelDesired::instance_signals() const
{
        connect(data_->date_edit_,          SIGNAL(dateChanged ( const QDate& )), SLOT(enableForm()));
        connect(data_->growth_edit_,        SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->weight_edit_,        SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->neck_edit_,          SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->shoulders_edit_,     SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->chest_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->bicep_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->forearm_edit_,       SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->wrist_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->abdomen_edit_,       SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->waist_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->hips_edit_,          SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->thigh_edit_,         SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
        connect(data_->calf_edit_,          SIGNAL(textChanged(const QString &)), SLOT(changed_values(const QString &)));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialogPanelDesired::changed_values(const QString& str)
{
        QLineEdit *edit = qobject_cast<QLineEdit *>(sender());
        if ( !edit && !v_user::is_double(str, true) ) return;
        emit(on_changed_values(edit->property(Data::property_name().c_str()).toString(), v_user::get_double(str)));
        enableForm();
}
//------------------------------------------------------------------------------
double  vLDMeasurementHistoryDialogPanelDesired::get_data(const QString& id ) const
{
        if(id ==id_name::abdomen())     return v_user::get_double(data_->abdomen_edit_->text());
        if(id ==id_name::biceps())      return v_user::get_double(data_->bicep_edit_->text());
        if(id ==id_name::calves())      return v_user::get_double(data_->calf_edit_->text());
        if(id ==id_name::chest())       return v_user::get_double(data_->chest_edit_->text());
        if(id ==id_name::forearms())    return v_user::get_double(data_->forearm_edit_->text());
        if(id ==id_name::hips())        return v_user::get_double(data_->hips_edit_->text());
        if(id ==id_name::neck())        return v_user::get_double(data_->neck_edit_->text());
        if(id ==id_name::shoulders())   return v_user::get_double(data_->shoulders_edit_->text());
        if(id ==id_name::thighs())      return v_user::get_double(data_->thigh_edit_->text());
        if(id ==id_name::waist())       return v_user::get_double(data_->waist_edit_->text());
        if(id ==id_name::weight())      return v_user::get_double(data_->weight_edit_->text());
        if(id ==id_name::wrist())       return v_user::get_double(data_->wrist_edit_->text());
        return double();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelDesired::check() const
{
        boost::array<QLineEdit*, 13> vec = 
        {{
                data_->growth_edit_, data_->weight_edit_, data_->neck_edit_, data_->shoulders_edit_,
                data_->chest_edit_,  data_->bicep_edit_, data_->forearm_edit_, data_->wrist_edit_,
                data_->abdomen_edit_, data_->waist_edit_,  data_->hips_edit_, data_->thigh_edit_,
                data_->calf_edit_
         }} ;
        return vValidator::is_number_with_migalo(vec.begin(), vec.end(), &v_user::is_double, true);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialogPanelDesired::test()
{
  //      Q_ASSERT(parent_);
        Q_ASSERT(data_->date_edit_);
        Q_ASSERT(data_->date_label_);
        Q_ASSERT(data_->growth_label_);
        Q_ASSERT(data_->growth_edit_);
        Q_ASSERT(data_->weight_label_);
        Q_ASSERT(data_->weight_edit_);
        Q_ASSERT(data_->neck_label_);
        Q_ASSERT(data_->neck_edit_);
        Q_ASSERT(data_->shoulders_label_);
        Q_ASSERT(data_->shoulders_edit_);
        Q_ASSERT(data_->chest_label_);
        Q_ASSERT(data_->chest_edit_);
        Q_ASSERT(data_->bicep_label_);
        Q_ASSERT(data_->bicep_edit_);
        Q_ASSERT(data_->forearm_label_);
        Q_ASSERT(data_->forearm_edit_);
        Q_ASSERT(data_->wrist_label_);
        Q_ASSERT(data_->wrist_edit_);
        Q_ASSERT(data_->abdomen_label_);
        Q_ASSERT(data_->abdomen_edit_);
        Q_ASSERT(data_->waist_label_);
        Q_ASSERT(data_->waist_edit_);
        Q_ASSERT(data_->hips_label_);
        Q_ASSERT(data_->hips_edit_);
        Q_ASSERT(data_->thigh_label_);
        Q_ASSERT(data_->thigh_edit_);
        Q_ASSERT(data_->calf_label_);
        Q_ASSERT(data_->calf_edit_);

        const QString cms=vMain::user().locale().height()->cms();
        Q_ASSERT(data_->date_label_        ->text() == vtr::Date::tr("&Date:"));
        Q_ASSERT(data_->growth_label_      ->text() == vtr::Body::tr("&Growth (%1):").arg(cms));
        Q_ASSERT(data_->weight_label_      ->text() == vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        Q_ASSERT(data_->neck_label_        ->text() == vtr::Body::tr("&Neck (%1):").arg(cms));
        Q_ASSERT(data_->shoulders_label_   ->text() == vtr::Body::tr("&Shoulders (%1):").arg(cms));
        Q_ASSERT(data_->chest_label_       ->text() == vtr::Body::tr("&Chest (%1):").arg(cms));
        Q_ASSERT(data_->bicep_label_       ->text() == vtr::Body::tr("&Biceps (%1):").arg(cms));
        Q_ASSERT(data_->forearm_label_     ->text() == vtr::Body::tr("&Forearms (%1):").arg(cms));
        Q_ASSERT(data_->wrist_label_       ->text() == vtr::Body::tr("W&rist (%1):").arg(cms));
        Q_ASSERT(data_->abdomen_label_     ->text() == vtr::Body::tr("&Abdomen (%1):").arg(cms));
        Q_ASSERT(data_->waist_label_       ->text() == vtr::Body::tr("Wa&ist (%1):").arg(cms));
        Q_ASSERT(data_->hips_label_        ->text() == vtr::Body::tr("Hi&ps (%1):").arg(cms));
        Q_ASSERT(data_->thigh_label_       ->text() == vtr::Body::tr("&Thigh (%1):").arg(cms));
        Q_ASSERT(data_->calf_label_        ->text() == vtr::Body::tr("Ca&lf (%1):").arg(cms));

        Q_ASSERT(data_->weight_edit_     ->property(data_->property_name().c_str()) == id_name::weight());
        Q_ASSERT(data_->growth_edit_     ->property(data_->property_name().c_str()) == id_name::growth());
        Q_ASSERT(data_->neck_edit_       ->property(data_->property_name().c_str()) == id_name::neck());
        Q_ASSERT(data_->shoulders_edit_  ->property(data_->property_name().c_str()) == id_name::shoulders());
        Q_ASSERT(data_->chest_edit_      ->property(data_->property_name().c_str()) == id_name::chest());
        Q_ASSERT(data_->bicep_edit_      ->property(data_->property_name().c_str()) == id_name::biceps());
        Q_ASSERT(data_->forearm_edit_    ->property(data_->property_name().c_str()) == id_name::forearms());
        Q_ASSERT(data_->wrist_edit_      ->property(data_->property_name().c_str()) == id_name::wrist());
        Q_ASSERT(data_->abdomen_edit_    ->property(data_->property_name().c_str()) == id_name::abdomen());
        Q_ASSERT(data_->waist_edit_      ->property(data_->property_name().c_str()) == id_name::waist());
        Q_ASSERT(data_->hips_edit_       ->property(data_->property_name().c_str()) == id_name::hips());
        Q_ASSERT(data_->thigh_edit_      ->property(data_->property_name().c_str()) == id_name::thighs());
        Q_ASSERT(data_->calf_edit_       ->property(data_->property_name().c_str()) == id_name::calves());
        
        
        data_->abdomen_edit_           ->setText("55");
        data_->bicep_edit_             ->setText("good");
        data_->calf_edit_              ->setText("lyt");
        data_->chest_edit_             ->setText("чум");
        data_->forearm_edit_           ->setText("Jm");
        data_->hips_edit_              ->setText("root");
        data_->neck_edit_              ->setText("jjj");
        data_->shoulders_edit_         ->setText("888");
        data_->thigh_edit_             ->setText("pcbsd");
        data_->waist_edit_             ->setText("7&");
        data_->weight_edit_            ->setText("&&&");
        data_->wrist_edit_             ->setText("\\");
        data_->growth_edit_            ->setText("Bicr");
        
        data_name data = get_data();

        Q_ASSERT(data.abdomen_          == data_->abdomen_edit_->text());
        Q_ASSERT(data.bicep_            == data_->bicep_edit_->text());
        Q_ASSERT(data.calf_             == data_->calf_edit_->text());
        Q_ASSERT(data.chest_            == data_->chest_edit_->text());
        Q_ASSERT(data.forearm_          == data_->forearm_edit_->text());
        Q_ASSERT(data.hips_             == data_->hips_edit_->text());
        Q_ASSERT(data.neck_             == data_->neck_edit_->text());
        Q_ASSERT(data.shoulders_        == data_->shoulders_edit_->text());
        Q_ASSERT(data.thigh_            == data_->thigh_edit_->text());
        Q_ASSERT(data.waist_            == data_->waist_edit_->text());
        Q_ASSERT(data.weight_           == data_->weight_edit_->text());
        Q_ASSERT(data.wrist_            == data_->wrist_edit_->text());
        Q_ASSERT(data.growth_           == data_->growth_edit_->text());

        data.abdomen_            = "575";
        data.bicep_              = "go3od";
        data.calf_               = "ly2t";
        data.chest_              = "ч7ум";
        data.forearm_            = "Jm6";
        data.hips_               = "ro0ot";
        data.neck_               = "jj0j";
        data.shoulders_          = "8808";
        data.thigh_              = "p0cbsd";
        data.waist_              = "780&";
        data.weight_             = "&&0&";
        data.wrist_              = "\\777";
        data.growth_             = "Bi77cr";
        
        set_data(data);

        data_->abdomen_edit_           ->setText("575");
        data_->bicep_edit_             ->setText("go3od");
        data_->calf_edit_              ->setText("ly2t");
        data_->chest_edit_             ->setText("ч7ум");
        data_->forearm_edit_           ->setText("Jm6");
        data_->hips_edit_              ->setText("ro0ot");
        data_->neck_edit_              ->setText("jj0j");
        data_->shoulders_edit_         ->setText("8808");
        data_->thigh_edit_             ->setText("p0cbsd");
        data_->waist_edit_             ->setText("780&");
        data_->weight_edit_            ->setText("&&0&");
        data_->wrist_edit_             ->setText("\\777");
        data_->growth_edit_            ->setText("Bi77cr");

        Q_ASSERT(data.abdomen_          == data_->abdomen_edit_->text());
        Q_ASSERT(data.bicep_            == data_->bicep_edit_->text());
        Q_ASSERT(data.calf_             == data_->calf_edit_->text());
        Q_ASSERT(data.chest_            == data_->chest_edit_->text());
        Q_ASSERT(data.forearm_          == data_->forearm_edit_->text());
        Q_ASSERT(data.hips_             == data_->hips_edit_->text());
        Q_ASSERT(data.neck_             == data_->neck_edit_->text());
        Q_ASSERT(data.shoulders_        == data_->shoulders_edit_->text());
        Q_ASSERT(data.thigh_            == data_->thigh_edit_->text());
        Q_ASSERT(data.waist_            == data_->waist_edit_->text());
        Q_ASSERT(data.weight_           == data_->weight_edit_->text());
        Q_ASSERT(data.wrist_            == data_->wrist_edit_->text());
        Q_ASSERT(data.growth_           == data_->growth_edit_->text());

        setWindowModified(false);
        return true;
}
//------------------------------------------------------------------------------