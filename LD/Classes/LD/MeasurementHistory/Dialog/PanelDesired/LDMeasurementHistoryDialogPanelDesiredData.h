// -*- C++ -*-
/* 
 * File:   LDMeasurementHistoryDialogPanelDesiredData.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 14:51
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_DATA_H_
#define	_V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_DATA_H_
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDialogPanelDesiredData
{
    typedef                     vLDMeasurementHistoryData                       id_name;
    typedef                     vLDMeasurementHistoryDataShort::data_name       data_name;
    static QString              name_dll()                                      { return const_instance_libs::ldmdpr41(); }
    static std::string          property_name()                                 { return "name"; }

    QLabel*                                                                     date_label_;
    QDateEdit*                                                                  date_edit_;
    QLabel*                                                                     growth_label_;
    QLineEdit*                                                                  growth_edit_;
    QLabel*                                                                     weight_label_;
    QLineEdit*                                                                  weight_edit_;
    QLabel*                                                                     neck_label_;
    QLineEdit*                                                                  neck_edit_;
    QLabel*                                                                     shoulders_label_;
    QLineEdit*                                                                  shoulders_edit_;
    QLabel*                                                                     chest_label_;
    QLineEdit*                                                                  chest_edit_;
    QLabel*                                                                     bicep_label_;
    QLineEdit*                                                                  bicep_edit_;
    QLabel*                                                                     forearm_label_;
    QLineEdit*                                                                  forearm_edit_;
    QLabel*                                                                     wrist_label_;
    QLineEdit*                                                                  wrist_edit_;
    QLabel*                                                                     abdomen_label_;
    QLineEdit*                                                                  abdomen_edit_;
    QLabel*                                                                     waist_label_;
    QLineEdit*                                                                  waist_edit_;
    QLabel*                                                                     hips_label_;
    QLineEdit*                                                                  hips_edit_;
    QLabel*                                                                     thigh_label_;
    QLineEdit*                                                                  thigh_edit_;
    QLabel*                                                                     calf_label_;
    QLineEdit*                                                                  calf_edit_;

void  set_data(const data_name& data)
{
        abdomen_edit_           ->setText(data.abdomen_);
        bicep_edit_             ->setText(data.bicep_);
        calf_edit_              ->setText(data.calf_);
        chest_edit_             ->setText(data.chest_);
        forearm_edit_           ->setText(data.forearm_);
        hips_edit_              ->setText(data.hips_);
        neck_edit_              ->setText(data.neck_);
        shoulders_edit_         ->setText(data.shoulders_);
        thigh_edit_             ->setText(data.thigh_);
        waist_edit_             ->setText(data.waist_);
        weight_edit_            ->setText(data.weight_);
        wrist_edit_             ->setText(data.wrist_);
        growth_edit_            ->setText(data.growth_);
}
//------------------------------------------------------------------------------
data_name  get_data() const
{
        data_name data ;
        data.abdomen_          = abdomen_edit_->text();
        data.bicep_            = bicep_edit_->text();
        data.calf_             = calf_edit_->text();
        data.chest_            = chest_edit_->text();
        data.forearm_          = forearm_edit_->text();
        data.hips_             = hips_edit_->text();
        data.neck_             = neck_edit_->text();
        data.shoulders_        = shoulders_edit_->text();
        data.thigh_            = thigh_edit_->text();
        data.waist_            = waist_edit_->text();
        data.weight_           = weight_edit_->text();
        data.wrist_            = wrist_edit_->text();
        data.growth_           = growth_edit_->text();
        return data;
}
//------------------------------------------------------------------------------
void translate()
{
        const QString cms=vMain::user().locale().height()->cms();
        date_label_        ->setText(vtr::Date::tr("&Date:"));
        growth_label_      ->setText(vtr::Body::tr("&Growth (%1):").arg(cms));
        weight_label_      ->setText(vtr::Body::tr("&Weight (%1):").arg(vMain::user().locale().weight()->kgs()));
        neck_label_        ->setText(vtr::Body::tr("&Neck (%1):").arg(cms));
        shoulders_label_   ->setText(vtr::Body::tr("&Shoulders (%1):").arg(cms));
        chest_label_       ->setText(vtr::Body::tr("&Chest (%1):").arg(cms));
        bicep_label_       ->setText(vtr::Body::tr("&Biceps (%1):").arg(cms));
        forearm_label_     ->setText(vtr::Body::tr("&Forearms (%1):").arg(cms));
        wrist_label_       ->setText(vtr::Body::tr("W&rist (%1):").arg(cms));
        abdomen_label_     ->setText(vtr::Body::tr("&Abdomen (%1):").arg(cms));
        waist_label_       ->setText(vtr::Body::tr("Wa&ist (%1):").arg(cms));
        hips_label_        ->setText(vtr::Body::tr("Hi&ps (%1):").arg(cms));
        thigh_label_       ->setText(vtr::Body::tr("&Thigh (%1):").arg(cms));
        calf_label_        ->setText(vtr::Body::tr("Ca&lf (%1):").arg(cms));
}
//------------------------------------------------------------------------------
void instance_names()
{
        weight_edit_     ->setProperty(property_name().c_str(), id_name::weight());
        growth_edit_     ->setProperty(property_name().c_str(), id_name::growth());
        neck_edit_       ->setProperty(property_name().c_str(), id_name::neck());
        shoulders_edit_  ->setProperty(property_name().c_str(), id_name::shoulders());
        chest_edit_      ->setProperty(property_name().c_str(), id_name::chest());
        bicep_edit_      ->setProperty(property_name().c_str(), id_name::biceps());
        forearm_edit_    ->setProperty(property_name().c_str(), id_name::forearms());
        wrist_edit_      ->setProperty(property_name().c_str(), id_name::wrist());
        abdomen_edit_    ->setProperty(property_name().c_str(), id_name::abdomen());
        waist_edit_      ->setProperty(property_name().c_str(), id_name::waist());
        hips_edit_       ->setProperty(property_name().c_str(), id_name::hips());
        thigh_edit_      ->setProperty(property_name().c_str(), id_name::thighs());
        calf_edit_       ->setProperty(property_name().c_str(), id_name::calves());
}
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_DIALOG_PANEL_DESIRED_DATA_H_ */

