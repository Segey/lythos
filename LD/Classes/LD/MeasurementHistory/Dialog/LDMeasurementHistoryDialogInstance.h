/* 
 * File:   LDMeasurementHistoryInstance.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:38
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_MEASUREMENT_HISTORY_INSTANCE_H_
#define	_V_LD_MEASUREMENT_HISTORY_INSTANCE_H_
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialog;
//------------------------------------------------------------------------------
class vLDMeasurementHistoryDialogInstance : go::noncopyable
{
    typedef                     vLDMeasurementHistoryDialogInstance             class_name;
    typedef                     vLDMeasurementHistoryDialog                     parent_name;

private:
    parent_name*                                                                parent_;

protected:
    parent_name*&               parent()                                        {return parent_;}
    const parent_name*          parent() const                                  {return parent_;}

    void                        instance_form()                                 {}
    void                        instance_widgets();
    void                        instance_images();    
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout();

public:
    explicit                    vLDMeasurementHistoryDialogInstance(parent_name* parent) : parent_(parent)  { }
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_MEASUREMENT_HISTORY_INSTANCE_H_ */
