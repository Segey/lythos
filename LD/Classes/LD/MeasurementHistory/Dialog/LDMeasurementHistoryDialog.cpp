/* 
 * File:   LDMeasurementHistoryDialog.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryDialog.h"
//------------------------------------------------------------------------------
vLDMeasurementHistoryDialog::vLDMeasurementHistoryDialog()
         : QDialog(0, Qt::WindowTitleHint), label_username_icon_(0), label_username_(0), label_cap_(0),
         panel_current_(0), panel_desired_(0), button_save_(0),button_cancel_(0)
{     
        ::vInstance< vLDMeasurementHistoryDialogInstance > (this).instance();
        settings_read();
        instance_signals();
        set_enabled(false);          
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::settings_read()
{
        QRect rect_default =  vDialog::form_size_calculation(this);
        QRect rect = vSettings::get(settings_table(), settings::make_form_size(settings_key(), rect_default)).get<1>();
        inherited::setGeometry(rect);
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::settings_write() const
{
        vSettings::set(settings_table(), settings::make_form_size(settings_key(), geometry()));
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::instance_signals()
{
        connect(button_cancel_, SIGNAL(clicked()),          SLOT(close()));
        connect(button_save_,   SIGNAL(clicked()),          SLOT(save()));
        connect(button_method_, SIGNAL(clicked()),          SLOT(openDialogMethod()));
        connect(panel_current_, SIGNAL(on_state_changed()), SLOT(panelStateChanged()));
        connect(panel_desired_, SIGNAL(on_state_changed()), SLOT(panelStateChanged()));
        connect(panel_mbs_,     SIGNAL(on_state_changed()), SLOT(panelStateChanged()));
        connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), SLOT(show_context_menu(const QPoint &)));
}
//------------------------------------------------------------------------------
/*virtual*/ void vLDMeasurementHistoryDialog::closeEvent(QCloseEvent *event)
{
        settings_write();
        if(!isWindowModified()) return;

        QMessageBox::StandardButton  click_button =QMessageBox::warning(this, program::name_full(), vtr::Message::tr("Do you want to save the changes to this document before closing?\n\nIf you don't save, your changes will be lost."), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if(click_button == QMessageBox::Cancel) event->ignore();
        if(click_button == QMessageBox::Save) save();
}
//------------------------------------------------------------------------------
/*slot*/ void vLDMeasurementHistoryDialog::show_context_menu(const QPoint &position)
{
        vLDMeasurementHistoryDialogPanelMBsPuM pum (panel_mbs_);
        pum.exec(mapToGlobal(position));
}
//------------------------------------------------------------------------------
/*slot*/ void vLDMeasurementHistoryDialog::panelStateChanged()
{
        inherited::setWindowModified(true);
        button_save_    ->setEnabled(true);
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::set_enabled(bool value)
{
        inherited::setWindowModified(value);
        panel_current_  ->setWindowModified(value);
        panel_desired_  ->setWindowModified(value);
        panel_mbs_      ->setWindowModified(value);
        button_save_    ->setEnabled(value);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialog::check()
{
        const bool done = panel_current_->check();
        return panel_desired_->check() && done;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::save()
{
        if( !inherited::isWindowModified() || !check() || !do_save() || !panel_mbs_->save() ) return;
        QMessageBox::information(this, program::name_full(), vtr::Message::tr("Your data are successfully updated!"), QMessageBox::Ok);
        accept();
        set_enabled(false);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialog::load(const QDate& date)
{
        if( !do_load(date) || !panel_mbs_->load() ) return false;
        set_enabled(false);
        return true;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryDialog::openDialogMethod()
{
       vLDMeasurementHistoryDialogMethod dialog;
       if (dialog.exec() == QDialog::Accepted)  panel_desired_->set_data(dialog.get_data());
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryDialog::test()
{
        Q_ASSERT(label_username_icon_);
        Q_ASSERT(label_username_);
        Q_ASSERT(label_cap_);
        Q_ASSERT(panel_current_);
        Q_ASSERT(panel_desired_);
        Q_ASSERT(button_save_);
        Q_ASSERT(button_cancel_);
        Q_ASSERT(!label_username_icon_->pixmap()->isNull());
        Q_ASSERT(!label_username_->text().isEmpty());
        Q_ASSERT(!label_cap_->text().isEmpty());
        Q_ASSERT(!button_save_->text().isEmpty());
        Q_ASSERT(!button_cancel_->text().isEmpty());

        vMain::user().set_test_id();
        QRect rect =  vDialog::form_size_calculation(this);
        Q_ASSERT( vSettings::set(settings_table(), settings::make_form_size(settings_key(), rect)) );
        settings_write();
        settings_read();        
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<0>() );
        Q_ASSERT( vSettings::get(settings_table(), settings::make_form_size(settings_key(), QRect())).get<1>() == rect );
        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------