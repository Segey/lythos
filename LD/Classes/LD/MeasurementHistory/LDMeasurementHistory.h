/* 
 * File:   MeasurementHistory.h
 * Author: S.Panin
 *
 * Created on 13 Марта 2009 г., 0:36
 */
//------------------------------------------------------------------------------
#ifndef _V_MEASUREMENT_HISTORY_H_
#define	_V_MEASUREMENT_HISTORY_H_
//------------------------------------------------------------------------------
#include "../../../../Go/Component/All/WidgetModified/WidgetModified.h"
#include "../../../../Go/Successor/SuccessorsChange.h"
#include "TreeView/LDMeasurementHistoryTreeView.h"
#include "LDMeasurementHistoryData.h"
#include "Dialog/LDMeasurementHistoryDialog.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistory : public QWidget , public vSuccessorsChange
{
    typedef                     QWidget                                         inherited;
    typedef                     vLDMeasurementHistory                           class_name;
    typedef                     vLDMeasurementHistoryWidgetData                 data_name;

    vClientLDDialog*                                                            parent_;
    boost::shared_ptr<data_name>                                                data_;


    virtual void                        do_change_state()                       { data_->tree_view_->setEnabled(true); }
    virtual QWidget*                    do_toWidget()                           {return this;}
    virtual bool                        do_check()                              {return true;}
    virtual bool                        do_save()                               {return true;}
    virtual bool                        do_load()                               {return data_->tree_view_->load();}

public:
    explicit                            vLDMeasurementHistory(vClientLDDialog* owner);
    static vLDMeasurementHistory*       make_new(vClientLDDialog* parent);
    static vLDMeasurementHistory*       make_exists(vClientLDDialog* parent);
    virtual bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MEASUREMENT_HISTORY_H_ */
