/*
 * File:   LDMeasurementHistoryTreeView.h
 * Author: S.Panin
 *
 * Created on 14 Марта 2010 г., 00:51
 */
//------------------------------------------------------------------------------
#include "LDMeasurementHistoryTreeView.h"
#include "../Dialog/Successors/LDMeasurementHistoryDialogSuccessors.h"
//------------------------------------------------------------------------------
/* virtual */ QStringList  vLDMeasurementHistoryTreeView::do_get_header_labels_full()
{
      const QString cms=vMain::user().locale().height()->cms();
      return QStringList()        << vtr::Date::tr("Date")       << vtr::Body::tr("Growth (%1):").arg(cms)
                << vtr::Record::tr("Weight (%1):").arg(vMain::user().locale().weight()->kgs())
                << vtr::Body::tr("Neck (%1):").arg(cms)          << vtr::Body::tr("Shoulders (%1):").arg(cms)
                << vtr::Body::tr("Chest (%1):").arg(cms)         << vtr::Body::tr("Bicep left (%1):").arg(cms)
                << vtr::Body::tr("Bicep right (%1):").arg(cms)   << vtr::Body::tr("Forearm left (%1):").arg(cms)
                << vtr::Body::tr("Forearm right (%1):").arg(cms) << vtr::Body::tr("Wrist left (%1):").arg(cms)
                << vtr::Body::tr("Wrist right (%1):").arg(cms)   << vtr::Body::tr("Abdomen (%1):").arg(cms)
                << vtr::Body::tr("Waist (%1):").arg(cms)         << vtr::Body::tr("Hips (%1):").arg(cms)
                << vtr::Body::tr("Thigh left (%1):").arg(cms)    << vtr::Body::tr("Thigh right (%1):").arg(cms)
                << vtr::Body::tr("Calf left (%1):").arg(cms)     << vtr::Body::tr("Calf right (%1):").arg(cms);
}
//------------------------------------------------------------------------------
/* virtual */ QStringList  vLDMeasurementHistoryTreeView::do_get_header_labels_default()
{
       const QString cms=vMain::user().locale().height()->cms();
       return QStringList()        << vtr::Date::tr("Date")     << vtr::Body::tr("Growth (%1):").arg(cms)
               << vtr::Record::tr("Weight (%1):").arg(vMain::user().locale().weight()->kgs())
               << vtr::Body::tr("Chest (%1):").arg(cms)         << vtr::Body::tr("Bicep right (%1):").arg(cms)
               << vtr::Body::tr("Abdomen (%1):").arg(cms);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::do_settings_read()
{
        vbtTreeWidget::set_header_items_width_and_hidden(toTreeWidget(),vSettings::get(settings_table(), settings::make_stringlist(settings_key())).get<1>());
        return true;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryTreeView::settings_write()
{
        QStringList widths_items =vbtTreeWidget::get_header_items_widths(toTreeWidget());
        vSettings::set(settings_table(), settings::make_stringlist(settings_key(), widths_items));
}
//------------------------------------------------------------------------------
/*virtual*/ bool vLDMeasurementHistoryTreeView::do_record_new()
{
        vLDMeasurementHistoryDialogSuccessorsNew dialog;
        return dialog.exec() == QDialog::Accepted;
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::do_record_open_with_item(const QString& name)
{
        vLDMeasurementHistoryDialogSuccessorsOpen dialog;
        dialog.load(date_from_string(name));
        return dialog.exec() == QDialog::Accepted ? load() : true;
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::do_record_delete(const QString& name)
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("DELETE FROM MeasurementHistory WHERE (UserID =? AND HistoryDate =?);");
        query.addBindValue(vMain::user().id());
        query.addBindValue(date_from_string(name));
        return query.exec();
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::do_records_clear_all()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("DELETE FROM MeasurementHistory WHERE UserID =?;");
        query.addBindValue(vMain::user().id());
        return query.exec() ? load() : false;
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::do_load()
{
        toTreeWidget()->clear();
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT HistoryDate, Growth, Weight, Neck, Shoulders, Chest, BicepLeft, BicepRight, ForearmLeft, ForearmRight, WristLeft, WristRight, Abdomen, Waist, Hips, ThighLeft, ThighRight, CalfLeft, CalfRight "
                       " FROM MeasurementHistory WHERE (UserID =? AND Ghost ='false');");
        query.addBindValue(vMain::user().id());
        if(!query.exec()) return false;
        while(query.next()) add_items(query);
        return true;
}
//------------------------------------------------------------------------------
void vLDMeasurementHistoryTreeView::add_items(const QSqlQuery& query)
{
        QTreeWidgetItem* item = new QTreeWidgetItem(toTreeWidget());
        item->setText(0, string_from_variant(query.value(0)));
        for(int i =1; i < 19; ++i )    item->setText(i, query.value(i).toString());
        toTreeWidget()->setItemExpanded(item, true);
}
//------------------------------------------------------------------------------
bool vLDMeasurementHistoryTreeView::test()
{
        inherited::test();
        QStringList  list_full =do_get_header_labels_full();
        QStringList  list_default =do_get_header_labels_default();
        Q_ASSERT(go::is(list_full.begin(), list_full.end(), list_default.begin(), list_default.end() ));
        Q_ASSERT(do_get_header_base_column_name()  == vtr::Date::tr("Date"));

        vMain::user().set_test_id();
        QStringList list_set_width = QStringList() << "9" <<"7" <<"77";
        Q_ASSERT(vSettings::set(settings_table(), settings::make_stringlist(settings_key(), list_set_width)));
        Q_ASSERT( vSettings::get(settings_table(), settings::make_stringlist(settings_key())).get<0>() );
        QStringList list_get_width = vSettings::get(settings_table(), settings::make_stringlist(settings_key())).get<1>();
        Q_ASSERT(list_set_width == list_get_width);

        setWindowModified(false);
        QSqlQuery query(vMain::settingsbd().database());
        return query.exec(QString("DELETE FROM %1 WHERE UserID =0").arg(settings_table()));
}
//------------------------------------------------------------------------------