/*
 * File:   LDMeasurementHistoryTreeView.h
 * Author: S.Panin
 *
 * Created on 14 Марта 2010 г., 00:51
 */
//------------------------------------------------------------------------------
#ifndef _V_MEASUREMENT_HISTORY_TREE_VIEW_H_
#define	_V_MEASUREMENT_HISTORY_TREE_VIEW_H_
//------------------------------------------------------------------------------
#include "../../../../../Go/Component/Tree/TreeView/TreeView.h"
//------------------------------------------------------------------------------
class vLDMeasurementHistoryTreeView : public vTreeView
{
Q_OBJECT
    typedef                     vTreeView                                       inherited;
    typedef                     vLDMeasurementHistoryTreeView                   class_name;

    static QString              settings_table()                                { return "MeasurementHistory"; }
    static QString              settings_key()                                  { return "HeaderList"; }
    static QDate                date_from_string(const QString& name)           { return QDate().fromString(name); }
    static QString              string_from_variant(const QVariant& var)        { return var.toDate().toString(); }
    void                        add_items(const QSqlQuery& query);
    void                        settings_write();

protected:
    virtual QString             do_get_header_base_column_name()                { return vtr::Date::tr("Date"); }
    virtual QStringList         do_get_header_labels_full();
    virtual QStringList         do_get_header_labels_default();
    virtual bool                do_record_new();
    virtual bool                do_record_open_with_item(const QString& name);
    virtual bool                do_record_delete(const QString& name);
    virtual bool                do_records_clear_all();
    virtual bool                do_settings_read();
    virtual bool                do_load();

public:
    explicit                    vLDMeasurementHistoryTreeView(QWidget* parent =0) : inherited(parent) {}
                                ~vLDMeasurementHistoryTreeView() { settings_write(); }
    bool                        test();
};
//------------------------------------------------------------------------------
#endif	/* _V_MEASUREMENT_HISTORY_TREE_VIEW_H_ */
