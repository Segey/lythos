// -*- C++ -*-
/* 
 * File:   MeasurementHistoryData.h
 * Author: S.Panin
 *
 * Created on 3 Январь 2010 г., 23:16
 */
//------------------------------------------------------------------------------
#ifndef _V_MEASUREMENT_HISTORY_DATA_H_
#define	_V_MEASUREMENT_HISTORY_DATA_H_
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryData
{
    typedef                             QString                                 id;

    static id                           growth()                                { return "growth";}
    static id                           biceps()                                { return "biceps"; }
    static id                           abdomen()                               { return "abdomen"; }
    static id                           calves()                                { return "calves"; }
    static id                           chest()                                 { return "chest"; }
    static id                           forearms()                              { return "forearms"; }
    static id                           hips()                                  { return "hips"; }
    static id                           neck()                                  { return "neck"; }
    static id                           shoulders()                             { return "shoulders"; }
    static id                           thighs()                                { return "thighs"; }
    static id                           waist()                                 { return "waist"; }
    static id                           weight()                                { return "weight"; }
    static id                           wrist()                                 { return "wrist"; }
};
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDataShort
{
    VDATA_MONO_13(QString, growth_, weight_, neck_, shoulders_, chest_, bicep_, forearm_, wrist_, abdomen_, waist_, hips_, thigh_, calf_);
    typedef                         vData<QString>                              data_name;
    
};
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryDataLong
{
    VDATA_MONO_18(QString, growth_, weight_, neck_, shoulders_, chest_, bicep_left_, bicep_right_, forearm_left_, forearm_right_, wrist_left_, wrist_right_, abdomen_, waist_, hips_, thigh_left_, thigh_right_, calf_left_, calf_right_);
    typedef                         vData<QString>                              data_name;
};
//------------------------------------------------------------------------------
struct vLDMeasurementHistoryWidgetData
{
    static QString                      name_dll()                              { return const_instance_libs::ldmhwd0046(); }
    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    vLDMeasurementHistoryTreeView*                                              tree_view_;

    explicit vLDMeasurementHistoryWidgetData() :  tree_view_(new vLDMeasurementHistoryTreeView)   {}

    void translate()
    {
        username_icon_label_->setPixmap(const_pixmap_64::measurement_history());
        username_label_->setText(vtr::Panel::tr("Personal Measurement History"));
        cap_label_->setText(vtr::Panel::tr("Measurement History"));
    }
};
//------------------------------------------------------------------------------
#endif	/* _V_MEASUREMENT_HISTORY_DATA_H_ */

