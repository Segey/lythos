/* 
 * File:   OptionsInstance.h
 * Author: S.Panin
 *
 * Created on 11 Апреля 2009 г., 23:26
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_OPTIONS_INSTANCE_H_
#define	_V_LD_OPTIONS_INSTANCE_H_
//------------------------------------------------------------------------------
class vLDOptions;
//------------------------------------------------------------------------------
class vLDOptionsInstance
{
    typedef                     vLDOptionsInstance                                class_name;
    typedef                     vLDOptions                                        dialog_type;

    dialog_type*                                                                 parent_;

protected:
    dialog_type*&               parent()                                        {return parent_;}
    dialog_type*                parent() const                                  {return parent_;}
    void                        instance_form()                                 {}
    void                        instance_widgets();
    void                        instance_images();
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout()                        {return 0;}

public:
    explicit                    vLDOptionsInstance( vLDOptions* parent) : parent_(parent)    { }
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_OPTIONS_INSTANCE_H_ */
