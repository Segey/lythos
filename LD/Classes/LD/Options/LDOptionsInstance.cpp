/* 
 * File:   OptionsInstance.cpp
 * Author: S.Panin
 * 
 * Created on 11 Апреля 2009 г., 23:26
 */
//------------------------------------------------------------------------------
#include "LDOptionsInstance.h"
//------------------------------------------------------------------------------
void vLDOptionsInstance::instance_widgets()
{
        parent_->username_label_         = new QLabel;
        parent_->username_icon_label_    = new QLabel;
        parent_->cap_label_              = new QLabel;
        parent_->label_language_         = new QLabel;
        parent_->combobox_language_      = new QComboBox;

        parent_->label_language_ ->setBuddy(parent_->combobox_language_);
}
//------------------------------------------------------------------------------
void  vLDOptionsInstance::instance_images()
{
parent_->username_icon_label_->setPixmap(const_pixmap_64::options());
}
//------------------------------------------------------------------------------
QLayout* vLDOptionsInstance::instance_top_layout()
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(parent_->username_icon_label_, parent_->username_label_);    
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* vLDOptionsInstance::instance_middle_layout()
{
        vLay lay(new QGridLayout, 8);
        lay.add_caption_bevel_first     (parent_->cap_label_);
        lay.add_unary_label             (parent_->label_language_,             parent_->combobox_language_);
        return lay.clayout();
}
//------------------------------------------------------------------------------
void vLDOptionsInstance::translate()
{
        parent_->username_label_->setText(vtr::Dialog::tr("Options"));
        parent_->cap_label_->setText(vtr::Dialog::tr("Option Settings"));
        parent_->label_language_->setText(vtr::Language::tr("&User interface"));
        parent_->setWindowTitle("[*]");
}
