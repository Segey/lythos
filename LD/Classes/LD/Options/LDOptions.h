/* 
 * File:   Options.h
 * Author: S.Panin
 *
 * Created on 11 Апреля 2010 г., 23:18
 */
//------------------------------------------------------------------------------
#ifndef _V_LD_OPTIONS_H_
#define	_V_LD_OPTIONS_H_
//------------------------------------------------------------------------------
#include "../../../../Go/Successor/SuccessorsChange.h"
#include "../../../../Go/Instance/Instance.h"
#include "LDOptionsInstance.h"
//------------------------------------------------------------------------------
class vLDOptions : public QWidget , public vSuccessorsChange
{
Q_OBJECT

    friend class vLDOptionsInstance;

    typedef                     QString                                         File;
    typedef                     QString                                         Text;
    typedef                     QMap<Text, File>                                map;
    typedef                     QWidget                                         inherited;
    typedef                     vLDOptions                                      class_name;

    map                                                                         map_;
    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    QLabel*                                                                     label_language_;
    QComboBox*                                                                  combobox_language_;
    vClientLDDialog*                                                            parent_;


    virtual void                do_change_state()                               {inherited::setEnabled(true);}
    virtual QWidget*            do_toWidget()                                   {return this;}
    virtual bool                do_check()                                      {return true;}
    virtual bool                do_save();
    virtual bool                do_load();
    explicit                    vLDOptions(vClientLDDialog* owner);
    void                        instance_signals() const;

private slots:
    void                        enable_save_button();

public:
    static vLDOptions*          make_new(vClientLDDialog* parent);
    static vLDOptions*          make_exists(vClientLDDialog* parent);
    virtual bool                test();
};
//------------------------------------------------------------------------------
#endif	/* _V_LD_OPTIONS_H_ */
