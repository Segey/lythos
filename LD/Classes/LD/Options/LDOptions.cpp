/* 
 * File:   Options.cpp
 * Author: S.Panin
 * 
 * Created on 11 Апреля 2010 г., 23:18
 */
//------------------------------------------------------------------------------
#include "LDOptions.h"
//------------------------------------------------------------------------------
vLDOptions::vLDOptions(vClientLDDialog* parent)
                :  username_icon_label_(0), username_label_(0), cap_label_(0), label_language_(0),
                combobox_language_(0), parent_(parent)
{
        ::vInstance< vLDOptionsInstance > (this).instance();
        instance_signals();
}
//------------------------------------------------------------------------------
vLDOptions* vLDOptions::make_new(vClientLDDialog* parent)
{
     vLDOptions* result = new vLDOptions(parent);
     result->setEnabled(false);
     return result;
}
//------------------------------------------------------------------------------
vLDOptions* vLDOptions::make_exists(vClientLDDialog* parent)
{
     return new vLDOptions(parent);
}
//------------------------------------------------------------------------------
void vLDOptions::instance_signals() const
{
        connect(combobox_language_, SIGNAL(currentIndexChanged ( const QString& )),  SLOT(enable_save_button()));
}
//------------------------------------------------------------------------------
/*slot*/void vLDOptions::enable_save_button()
{
        inherited::setWindowModified(true);
        parent_->enableSaveButton();
        QApplication::processEvents();
}
//------------------------------------------------------------------------------
bool vLDOptions::do_save()
{     
        if(!inherited::isWindowModified()) return true;
        vMain::user().translation()->load_file( map_.value(combobox_language_->currentText()) );
        vMain::user().settings_write();
        inherited::setWindowModified(false);
        return true;
}
//------------------------------------------------------------------------------
bool vLDOptions::do_load()
{
        map_  =  v_translation::get_all_translations();
        combobox_language_->addItems(map_.keys());
        const int index =map_.values().indexOf(vMain::user().translation()->file());
        combobox_language_->setCurrentIndex(index);
        inherited::setEnabled(true);
        return true;
}
//------------------------------------------------------------------------------
bool vLDOptions::test()
{
        Q_ASSERT(username_icon_label_);
        Q_ASSERT(username_label_);
        Q_ASSERT(cap_label_);
        Q_ASSERT(label_language_);
        Q_ASSERT(combobox_language_);
        do_load();
        Q_ASSERT(combobox_language_->count() >1);
        Q_ASSERT(parent_);
        Q_ASSERT(!username_icon_label_->pixmap()->isNull());
        Q_ASSERT(username_icon_label_->text().isEmpty());
        Q_ASSERT(!cap_label_->text().isEmpty());
        Q_ASSERT(!username_label_->text().isEmpty());
        inherited::setEnabled(false);
        Q_ASSERT(!inherited::isEnabled());
        do_change_state(); 
        Q_ASSERT(inherited::isEnabled());
        return true;
}
//------------------------------------------------------------------------------