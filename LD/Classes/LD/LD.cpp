/* 
 * File:   vLD.cpp
 * Author: S.Panin
 * 
 * Created on 13 Июль 2009 г., 11:23
 */
//------------------------------------------------------------------------------
#include "LD.h"
//------------------------------------------------------------------------------
void vLD::enterEvent(QEvent* event)
{
        inherited::setEnabled(true);
        inherited::enterEvent(event);
}
//------------------------------------------------------------------------------
void vLD::leaveEvent(QEvent* event)
{
        inherited::setEnabled(false);
        inherited::leaveEvent(event);
}
//------------------------------------------------------------------------------
void vLD::mousePressEvent(QMouseEvent* event)
{
        parent_->setCurrentIndex(page_index_);
        inherited::mousePressEvent(event);
}
//------------------------------------------------------------------------------
vLD* vLD::make_PersonalInfo(vClientLDDialog* parent, bool is_new_user)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(new vLDPersonalInfo(parent, is_new_user));
        result->setPixmap(const_pixmap_panels::personal_info());
        result->setCaption(vtr::Dialog::tr("Personal info"));
        result->setText(vtr::Dialog::tr("- Enter your personal data (information) including: Personal Info (Name, Password, Date of Birth, Sex, Height), and Contact Info(Zip code, Country,  State/Region, City/Town, Address (Street, Apartment), E-Mail, Phone number, ICQ, Mobule) and Additional info (Doctor, Instructor)."));
        return result;
}
//------------------------------------------------------------------------------
void vLD::create_PhotoGallery(vLD* result)
{
        result->setPixmap(const_pixmap_panels::photogallery());
        result->setCaption(vtr::Dialog::tr("PhotoGallery"));
        result->setText(vtr::Dialog::tr("- The page \"Photogallery\" allows you  to keep the most important photos of your successes and achievements step by step, day by day.  Regular supplements to the Photogallery with new photos will allow you to timely make necessary correcting amendments to the working program of trainings with the purpose of advancing results."));
}
//------------------------------------------------------------------------------
void vLD::create_RecordHistory(vLD* result)
{
        result->setPixmap(const_pixmap_panels::record_history());
        result->setCaption(vtr::Dialog::tr("Record History"));
        result->setText(vtr::Dialog::tr("- The page \"Records\" will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way."));
}
//------------------------------------------------------------------------------
void vLD::create_MeasurementHistory(vLD* result)
{
        result->setPixmap(const_pixmap_panels::measurement_history());
        result->setCaption(vtr::Dialog::tr("Measurement History"));
        result->setText(vtr::Dialog::tr("- The page \"Measurements\" will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way."));
}
//------------------------------------------------------------------------------
void vLD::create_Options(vLD* result)
{
        result->setPixmap(const_pixmap_panels::options());
        result->setCaption(vtr::Dialog::tr("Options"));
        result->setText(vtr::Dialog::tr("- The page \"Options\" will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way."));
}
//------------------------------------------------------------------------------
vLD* vLD::make_NewPhotoGallery(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vPhotoGallery::make_new(parent));
        create_PhotoGallery(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_ExistsPhotoGallery(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vPhotoGallery::make_exists(parent));
        create_PhotoGallery(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_NewRecordHistory(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vRecordHistory::make_new(parent));
        create_RecordHistory(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_ExistsRecordHistory(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vRecordHistory::make_exists(parent));
        create_RecordHistory(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_NewMeasurementHistory(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vLDMeasurementHistory::make_new(parent));
        create_MeasurementHistory(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_ExistsMeasurementHistory(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vLDMeasurementHistory::make_exists(parent));
        create_MeasurementHistory(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_NewOptions(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vLDOptions::make_new(parent));
        create_Options(result);
        return result;
}
//------------------------------------------------------------------------------
vLD* vLD::make_ExistsOptions(vClientLDDialog* parent)
{
        vLD* result =new vLD(parent);
        result->ld_.reset(vLDOptions::make_exists(parent));
        create_Options(result);
        return result;
}