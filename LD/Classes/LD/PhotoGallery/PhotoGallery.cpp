/* 
 * File:   PersonalInfo.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#include "PhotoGallery.h"
//------------------------------------------------------------------------------
vPhotoGallery::vPhotoGallery(vClientLDDialog* parent)
                :  username_icon_label_(0), username_label_(0), cap_label_(0), parent_(parent)
{
        inherited::setWindowModified(false);
        createImages();
        ::vInstance< vPhotoGalleryInstance > (this).instance();
        inherited::setEnabled(false);
}
//------------------------------------------------------------------------------
vPhotoGallery* vPhotoGallery::make_new(vClientLDDialog* parent)
{
     vPhotoGallery* result = new vPhotoGallery(parent);
     result->setEnabled(false);
     return result;
}
//------------------------------------------------------------------------------
vPhotoGallery* vPhotoGallery::make_exists(vClientLDDialog* parent)
{
     return new vPhotoGallery(parent);
}
//------------------------------------------------------------------------------
void vPhotoGallery::createImages()
{
        images_.reserve(count_himages_ * count_wimages_);

        for(int i = 0; i < count_himages_ * count_wimages_; ++i)
        {
                boost::shared_ptr<vPanelImage> image(new vPanelImage);
                image->tag() = i;
                image->connect_clear_image(boost::bind(&vPhotoGallery::enableSaveButton, this));
                image->connect_update_image(boost::bind(&vPhotoGallery::enableSaveButton, this));
                images_.push_back(image);
        }
}
//------------------------------------------------------------------------------
/*slot*/void vPhotoGallery::enableSaveButton()
{
        inherited::setWindowModified(true);
        parent_->enableSaveButton();
        QApplication::processEvents();
}
//------------------------------------------------------------------------------
void vPhotoGallery::addImage(const boost::shared_ptr<vPanelImage>& image)
{
        std::vector<QVariant> vec;
        vec.push_back(vMain::user().id());
        vec.push_back(image->tag());
        vec.push_back(QDateTime::fromString(image->get_image_date()));
        vec.push_back(image->get_image_description());
        v_bd_image::add(vMain::basebd().database(),QString("INSERT INTO PhotoGallery (Image, UserID, ImagePosition, ImageDate, ImageDescription) VALUES (?, ?, ?, ?, ?);"), image->file_name(), vec);
}
//------------------------------------------------------------------------------
void vPhotoGallery::updateImage(const boost::shared_ptr<vPanelImage>& image)
{
        std::vector<QVariant> vec;
        vec.push_back(QDateTime::fromString(image->get_image_date()));
        vec.push_back(image->get_image_description());
        vec.push_back(image->tag());
        vec.push_back(vMain::user().id());
        v_bd_image::add(vMain::basebd().database(),QString("UPDATE PhotoGallery SET Image =? , ImageDate =? , ImageDescription = ?  WHERE (ImagePosition =? AND UserID =?);"), image->file_name(), vec);
}
//------------------------------------------------------------------------------
void vPhotoGallery::saveImages(const boost::shared_ptr<vPanelImage>& image)
{
        if ( !image->isWindowModified() ) return;
        v_bd::exists(vMain::basebd().database(),"SELECT ImagePosition FROM PhotoGallery WHERE (ImagePosition =? AND UserID =?);", image->tag()) ? updateImage(image) :  addImage(image);
        image->setWindowModified(false);
}
//------------------------------------------------------------------------------
bool vPhotoGallery::do_save()
{     
        if(!inherited::isWindowModified()) return true;
        std::for_each(images_.begin(), images_.end(), boost::bind(&vPhotoGallery::saveImages, this, _1));
        inherited::setWindowModified(false);
        return true;
}
//------------------------------------------------------------------------------
bool vPhotoGallery::do_load()
{
        QSqlQuery query(vMain::basebd().database());
        query.prepare("SELECT ImagePosition, ImageDate, ImageDescription, Image FROM PhotoGallery WHERE UserID =?;");
        query.addBindValue(QVariant(vMain::user().id()));
        query.exec();
        while(query.next())
        {
                std::for_each(images_.begin(), images_.end(),boost::mem_fn(&vPanelImage::ctag));
                std::vector<boost::shared_ptr<vPanelImage> >::iterator iter = std::find_if(images_.begin(), images_.end(), boost::bind(&vPanelImage::ctag, _1) == query.value(0).toInt());
                if(iter != images_.end() && !query.value(3).isNull()) instance_image(iter->get(), query.value(3).toByteArray(), query.value(1).toString(), query.value(2).toString());
        }
        inherited::setEnabled(true);
        return true;
}
//------------------------------------------------------------------------------
void vPhotoGallery::instance_image(vPanelImage* iter, const QByteArray& array, const QString& date, const QString& description)
{
        QPixmap pictureFromDb;
        pictureFromDb.loadFromData(array);
        iter->set_data(date, description, pictureFromDb);
}
//------------------------------------------------------------------------------
bool vPhotoGallery::test()
{
        Q_ASSERT(username_icon_label_);
        Q_ASSERT(username_label_);
        Q_ASSERT(cap_label_);
        Q_ASSERT(parent_);
        Q_ASSERT(!username_icon_label_->pixmap()->isNull());
        Q_ASSERT(username_icon_label_->text().isEmpty());
        Q_ASSERT(!cap_label_->text().isEmpty());
        Q_ASSERT(!username_label_->text().isEmpty());
        Q_ASSERT(count_himages_ ==3);
        Q_ASSERT(count_wimages_ ==5);
        inherited::setEnabled(false);
        Q_ASSERT(!inherited::isEnabled());
        do_change_state(); 
        Q_ASSERT(inherited::isEnabled());
        return true;
}
//------------------------------------------------------------------------------