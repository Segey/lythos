/* 
 * File:   PhotoGalleryInstance.cpp
 * Author: S.Panin
 * 
 * Created on 14 Июль 2009 г., 9:38
 */
//------------------------------------------------------------------------------
#include "PhotoGalleryInstance.h"
//------------------------------------------------------------------------------
void vPhotoGalleryInstance::instance_widgets()
{
        parent_->username_label_         = new QLabel;
        parent_->username_icon_label_    = new QLabel;
        parent_->cap_label_              = new QLabel;
}
//------------------------------------------------------------------------------
void  vPhotoGalleryInstance::instance_images()                               
{
parent_->username_icon_label_->setPixmap(const_pixmap_64::photos());
}
//------------------------------------------------------------------------------
QLayout* vPhotoGalleryInstance::instance_top_layout()
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(parent_->username_icon_label_, parent_->username_label_);
        TopInstance.add_caption_bevel(parent_->cap_label_);       
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* vPhotoGalleryInstance::instance_middle_layout()
{
        QGridLayout* layout = new QGridLayout;
        layout->setAlignment(Qt::AlignVCenter);
        std::for_each(parent_->images_.begin(), parent_->images_.end(), instanceImages(layout, parent_->count_wimages_));
        return layout;
}
//------------------------------------------------------------------------------
void vPhotoGalleryInstance::translate()
{
        parent_->username_label_->setText(vtr::Dialog::tr("PhotoGallery"));
        parent_->cap_label_->setText(vtr::Button::tr("Photos"));
        parent_->setWindowTitle("[*]");
}
