/* 
 * File:   PhotoGallery.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:36
 */
//------------------------------------------------------------------------------
#ifndef _V_PHOTO_GALLERY_H_
#define	_V_PHOTO_GALLERY_H_
//------------------------------------------------------------------------------
#include "../../../../Go/Component/Panel/Image/Image/PanelImage.h"
#include "../../../../Go/Successor/SuccessorsChange.h"
#include "../../../../Go/BD/Algorithm/BD.h"
#include "PhotoGalleryInstance.h"
//------------------------------------------------------------------------------
class vPhotoGallery : public QWidget , public vSuccessorsChange
{
Q_OBJECT

    friend class vPhotoGalleryInstance;

    typedef                     std::vector<boost::shared_ptr<vPanelImage> >    image;
    typedef                     QWidget                                         inherited;
    typedef                     vPhotoGallery                                   class_name;

    QLabel*                                                                     username_icon_label_;
    QLabel*                                                                     username_label_;
    QLabel*                                                                     cap_label_;
    vClientLDDialog*                                                            parent_;
    image                                                                       images_;
    static const int                                                            count_himages_ =3;
    static const int                                                            count_wimages_ =5;

    void                        createImages();
    void                        saveImages(const boost::shared_ptr<vPanelImage>& image);
    void                        addImage(const boost::shared_ptr<vPanelImage>& image);
    void                        updateImage(const boost::shared_ptr<vPanelImage>& image);
    void                        enableSaveButton();
    void                        instance_image(vPanelImage* iter, const QByteArray& array, const QString& date, const QString& description);
    virtual void                do_change_state()                               {inherited::setEnabled(true);}
    virtual QWidget*            do_toWidget()                                   {return this;}
    virtual bool                do_check()                                      {return true;}
    virtual bool                do_save();
    virtual bool                do_load();
    explicit                    vPhotoGallery(vClientLDDialog* owner);

public:
    static vPhotoGallery*       make_new(vClientLDDialog* parent);
    static vPhotoGallery*       make_exists(vClientLDDialog* parent);
    virtual bool                test();
};
//------------------------------------------------------------------------------
#endif	/* _V_PHOTO_GALLERY_H_ */
