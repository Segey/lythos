/* 
 * File:   PhotoGalleryInstance.h
 * Author: S.Panin
 *
 * Created on 14 Июль 2009 г., 9:38
 */
//------------------------------------------------------------------------------
#ifndef _V_PHOTO_GALLERY_INSTANCE_H_
#define	_V_PHOTO_GALLERY_INSTANCE_H_
//------------------------------------------------------------------------------
class vPhotoGallery;
//------------------------------------------------------------------------------
class vPhotoGalleryInstance
{
    typedef                     vPhotoGalleryInstance                           class_name;
    typedef                     vPhotoGallery*                                  dialog_type;

class instanceImages : public std::unary_function<void, boost::shared_ptr<vPanelImage> >
    {
        int number_, windex_, hindex_;
        QGridLayout* layout_;

    public:
        instanceImages(QGridLayout* layout, int number) : number_(number), windex_(0), hindex_(0), layout_(layout){ }
        void operator()(const boost::shared_ptr<vPanelImage>& lhs)
        {
            if(windex_ > number_ - 1)
            {
                windex_ = 0;
                hindex_ += 2;
                layout_->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), hindex_ - 1, 0);
            }
            layout_->addWidget(lhs->window(), hindex_, windex_++, 1, 1, Qt::AlignAbsolute | Qt::AlignCenter);
        }
    };



    dialog_type                                                                 parent_;

protected:
    dialog_type&                parent()                                        {return parent_;}
    dialog_type                 parent() const                                  {return parent_;}
    void                        instance_form()                                 {}
    void                        instance_widgets();
    void                        instance_images();
    QLayout*                    instance_top_layout();
    QLayout*                    instance_middle_layout();
    QLayout*                    instance_bottom_layout()                        {return 0;}

public:
    explicit                    vPhotoGalleryInstance( vPhotoGallery* parent) : parent_(parent)    { }
    void                        translate();
};
//------------------------------------------------------------------------------
#endif	/* _V_PHOTOGALLERY_INSTANCE_H_ */
