/*
 * File:   main.cpp
 * Author: dix75
 *
 * Created on 24 Июнь 2009 г., 21:52
 */
//------------------------------------------------------------------------------
#include "Classes/LD/MeasurementHistory/Dialog/Successors/LDMeasurementHistoryDialogSuccessors.h"
//------------------------------------------------------------------------------
bool try_load_dlls()
{
        int x = 0;
        x += vDLL<bool, vdll::Message > (const_libs::st001ng(),                 "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_libs::f12nt(),                   "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_libs::mb1810(),                  "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_libs::in1256t(),                 "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::da1128(),         "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::di1245(),         "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldmdmp26(),       "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldmdpl1223(),     "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldmdpr41(),       "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldpi99(),         "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldrh300(),        "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldrhd740(),       "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldrhp022(),       "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::tw1313(),         "is_exists")(false);
        x += vDLL<bool, vdll::Message > (const_instance_libs::ldmhwd0046(),     "is_exists")(false);
        return (x==15);
}
//------------------------------------------------------------------------------
 void myMessageOutput(QtMsgType type, const char *msg)
 {
     switch (type)
     {
     case QtDebugMsg:    fprintf(stderr, "%s\n", msg);    break;
     case QtWarningMsg:  break;
     case QtCriticalMsg: fprintf(stderr, "Critical: %s\n", msg); break;
     case QtFatalMsg:    fprintf(stderr, "Fatal: %s\n", msg);    abort();
     }
 }
//------------------------------------------------------------------------------
void ExecuteTest()
{
        QtMsgHandler msg =qInstallMsgHandler(myMessageOutput);

        boost::shared_ptr<vClientLDDialog> ld_new_user(vClientLDDialog::make_NewUserClientLDDialog());
        boost::shared_ptr<vClientLDDialog> ld_exists_user(vClientLDDialog::make_ExistsUserClientLDDialog());

        typedef                 vLDMeasurementHistoryDialogPanelCurrent               vLDMDCurrent;
        typedef                 vLDMeasurementHistoryDialogPanelDesired               vLDMDDesired;

        vTest test;
        test.start();
        CLASS_TEST(test, vMain());
        CLASS_TEST(test, vLay(new QGridLayout,4));
        CLASS_TEST(test, vPanelPresentation());
        CLASS_TEST(test, vWidgetImage());
        CLASS_TEST(test, vPanelImage());
        CLASS_TEST(test, vDockRichEdit());
        CLASS_TEST(test, vSingletonUser());
        CLASS_TEST(test, vDialogAbout());
        CLASS_TEST(test, ActionBD());
        CLASS_TEST(test, vbtTreeWidget());
        CLASS_TEST_REF(test, vClientLDDialog::make_NewUserClientLDDialog());
        CLASS_TEST(test, vLDPersonalInfo(ld_new_user.get(), true));
        CLASS_TEST_REF(test, vPhotoGallery::make_new(ld_new_user.get()));
        CLASS_TEST_REF(test, vPhotoGallery::make_exists(ld_exists_user.get()));
        CLASS_TEST_REF(test, vRecordHistory::make_new(ld_new_user.get()));
        CLASS_TEST_REF(test, vRecordHistory::make_exists(ld_exists_user.get()));
        CLASS_TEST_REF(test, vLDMeasurementHistory::make_new(ld_new_user.get()));
        CLASS_TEST_REF(test, vLDMeasurementHistory::make_exists(ld_exists_user.get()));
        CLASS_TEST_REF(test, vLDOptions::make_new(ld_new_user.get()));
        CLASS_TEST_REF(test, vLDOptions::make_exists(ld_exists_user.get()));
        CLASS_TEST(test, vLDRecordHistoryTreeView());       
        CLASS_TEST(test, vLDRecordHistoryDialogSuccessorsNew());
        CLASS_TEST(test, vLDRecordHistoryDialogSuccessorsOpen());
        CLASS_TEST(test, vLDMeasurementHistoryDialogSuccessorsNew());
        CLASS_TEST(test, vLDMeasurementHistoryDialogSuccessorsOpen());
        CLASS_TEST(test, vLDRecordHistoryDialogPanel());
        CLASS_TEST(test, vMBHistogramPercentage(0));
        CLASS_TEST(test, vMBHistogramScale(0));
        CLASS_TEST(test, vMBHistogramSimple(0));
        CLASS_TEST(test, vMBPiePercentage(0));
        CLASS_TEST(test, vMBPieScale(0));
        CLASS_TEST(test, vMBPieSimple(0));
        CLASS_TEST(test, vMBTextPercentage(0));
        CLASS_TEST(test, vMBTextScale(0));
        CLASS_TEST(test, vMBTextSimple(0));
        CLASS_TEST(test, vMBBaseStatic());
        CLASS_TEST(test, vMBPuM(new vMB(new QWidget, "")));
        CLASS_TEST(test, vMB(0, " "));
        CLASS_TEST(test, vXMLProportion());
        CLASS_TEST(test, vLDMeasurementHistoryDialogPanelCurrent());
        CLASS_TEST(test, vLDMeasurementHistoryDialogPanelDesired());
        CLASS_TEST(test, vLDMeasurementHistoryDialogMethod());
        CLASS_TEST(test, vLDMeasurementHistoryDialogPanelMBs(new vLDMDCurrent, new vLDMDDesired));
        CLASS_TEST(test, vTreeViewHeaderPuM(new QTreeWidget));
        CLASS_TEST(test, vTreeViewBodyPuM(new QTreeWidget));
        CLASS_TEST(test, vLDMeasurementHistoryTreeView());
        CLASS_TEST_STATIC(test, v_user);
        CLASS_TEST_STATIC(test, v_translation);
        CLASS_TEST(test, vLocaleType<vLocaleTypeInt>() );
        CLASS_TEST(test, vLocaleType<vLocaleTypeDouble>() );
        CLASS_TEST(test, vLocaleType<vLocaleTypeDate>() );
        CLASS_TEST(test, vLocaleType<vLocaleTypeDateTime>() );
        CLASS_TEST(test, vLocale() );
        test.end();
       // qDebug() << sizeof (QApplication);
        qInstallMsgHandler(msg);
}
//------------------------------------------------------------------------------
int main() //int main(int argc, char *argv[])
{
        vMain::application();
        vMain::application().setApplicationName(program::name());
        vMain::application().setApplicationVersion(program::version());
        vMain::application().setWindowIcon(const_icon_16::program());
        vMain::application().setOrganizationName(program::name_organisation());

        vMain::user();
        vMain::user().set_application(&vMain::application());

        if ( !try_load_dlls() ) return EXIT_FAILURE;
      //  ExecuteTest();

        vMain Main;
        Main.show();

        return vMain::application().exec();
}