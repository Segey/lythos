/* 
 * File:   ldmdpl1223.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 12:33
 */
//------------------------------------------------------------------------------
#ifndef _V_LDMDPL1223_H
#define	_V_LDMDPL1223_H
//------------------------------------------------------------------------------
class QLayout;
class vLDMeasurementHistoryDialogPanelCurrentData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDMeasurementHistoryDialogPanelCurrentData* data);
extern "C" void                     instance_widgets(vLDMeasurementHistoryDialogPanelCurrentData* data);
extern "C" void                     set_buddy(vLDMeasurementHistoryDialogPanelCurrentData* data);
extern "C" QLayout*                 instance_middle_layout(vLDMeasurementHistoryDialogPanelCurrentData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDMDPL1223_H */
