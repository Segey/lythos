/* 
 * File:   ldmdpl1223.cpp
 * Author: S.Panin
 * 
 * Created on 16 Январь 2010 г., 12:33
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/MeasurementHistory/LDMeasurementHistory.h"

#include "ldmdpl1223.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDMeasurementHistoryDialogPanelCurrentData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        set_buddy(data);
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDMeasurementHistoryDialogPanelCurrentData* data)
{
        data->date_label_            = new QLabel;
        data->date_edit_             = new QDateEdit;
        data->growth_label_          = new QLabel;
        data->growth_edit_           = new QLineEdit;
        data->weight_label_          = new QLabel;
        data->weight_edit_           = new QLineEdit;
        data->neck_label_            = new QLabel;
        data->neck_edit_             = new QLineEdit;
        data->shoulders_label_       = new QLabel;
        data->shoulders_edit_        = new QLineEdit;
        data->chest_label_           = new QLabel;
        data->chest_edit_            = new QLineEdit;
        data->bicep_left_label_      = new QLabel;
        data->bicep_left_edit_       = new QLineEdit;
        data->bicep_right_label_     = new QLabel;
        data->bicep_right_edit_      = new QLineEdit;
        data->forearm_left_label_    = new QLabel;
        data->forearm_left_edit_     = new QLineEdit;
        data->forearm_right_label_   = new QLabel;
        data->forearm_right_edit_    = new QLineEdit;
        data->wrist_left_label_      = new QLabel;
        data->wrist_left_edit_       = new QLineEdit;
        data->wrist_right_label_     = new QLabel;
        data->wrist_right_edit_      = new QLineEdit;
        data->abdomen_label_         = new QLabel;
        data->abdomen_edit_          = new QLineEdit;
        data->waist_label_           = new QLabel;
        data->waist_edit_            = new QLineEdit;
        data->hips_label_            = new QLabel;
        data->hips_edit_             = new QLineEdit;
        data->thigh_left_label_      = new QLabel;
        data->thigh_left_edit_       = new QLineEdit;
        data->thigh_right_label_     = new QLabel;
        data->thigh_right_edit_      = new QLineEdit;
        data->calf_left_label_       = new QLabel;
        data->calf_left_edit_        = new QLineEdit;
        data->calf_right_label_      = new QLabel;
        data->calf_right_edit_       = new QLineEdit;
}
//------------------------------------------------------------------------------
void set_buddy(vLDMeasurementHistoryDialogPanelCurrentData* data)
{
        data->date_label_            ->setBuddy(data->date_edit_);
        data->growth_label_          ->setBuddy(data->growth_edit_);
        data->weight_label_          ->setBuddy(data->weight_edit_);
        data->neck_label_            ->setBuddy(data->neck_edit_);
        data->shoulders_label_       ->setBuddy(data->shoulders_edit_);
        data->chest_label_           ->setBuddy(data->chest_edit_);
        data->bicep_left_label_      ->setBuddy(data->bicep_left_edit_);
        data->bicep_right_label_     ->setBuddy(data->bicep_right_edit_);
        data->forearm_left_label_    ->setBuddy(data->forearm_left_edit_);
        data->forearm_right_label_   ->setBuddy(data->forearm_right_edit_);
        data->wrist_left_label_      ->setBuddy(data->wrist_left_edit_);
        data->wrist_right_label_     ->setBuddy(data->wrist_right_edit_);
        data->abdomen_label_         ->setBuddy(data->abdomen_edit_);
        data->waist_label_           ->setBuddy(data->waist_edit_);
        data->hips_label_            ->setBuddy(data->hips_edit_);
        data->thigh_left_label_      ->setBuddy(data->thigh_left_edit_);
        data->thigh_right_label_     ->setBuddy(data->thigh_right_edit_);
        data->calf_left_label_       ->setBuddy(data->calf_left_edit_);
        data->calf_right_label_      ->setBuddy(data->calf_right_edit_);
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDMeasurementHistoryDialogPanelCurrentData* data)
{
         vLay lay(new QGridLayout, 4);
        lay.add_unary_label (data->date_label_ ,             data->date_edit_);
        lay.add_unary_label (data->growth_label_ ,           data->growth_edit_);
        lay.add_unary_label (data->weight_label_ ,           data->weight_edit_);
        lay.add_unary_label (data->neck_label_ ,             data->neck_edit_);
        lay.add_unary_label (data->shoulders_label_ ,        data->shoulders_edit_);
        lay.add_unary_label (data->chest_label_ ,            data->chest_edit_);
        lay.add_binary_label(data->bicep_left_label_ ,       data->bicep_left_edit_,      data->bicep_right_label_ ,   data->bicep_right_edit_);
        lay.add_binary_label(data->forearm_left_label_ ,     data->forearm_left_edit_,    data->forearm_right_label_ , data->forearm_right_edit_);
        lay.add_binary_label(data->wrist_left_label_ ,       data->wrist_left_edit_,      data->wrist_right_label_ ,   data->wrist_right_edit_);
        lay.add_unary_label (data->abdomen_label_ ,          data->abdomen_edit_);
        lay.add_unary_label (data->waist_label_ ,            data->waist_edit_);
        lay.add_unary_label (data->hips_label_ ,             data->hips_edit_);
        lay.add_binary_label(data->thigh_left_label_ ,       data->thigh_left_edit_,      data->thigh_right_label_ ,   data->thigh_right_edit_);
        lay.add_binary_label(data->calf_left_label_ ,        data->calf_left_edit_,       data->calf_right_label_ ,    data->calf_right_edit_);
        return lay.clayout();
}