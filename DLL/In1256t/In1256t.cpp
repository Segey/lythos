/*
 * File:   In1256t.h
 * Author: S.Panin
 *
 * Created on 27 Сентябрь 2009 г., 12:59
 */
//------------------------------------------------------------------------------
#include "math.h"
#include "In1256t.h"
//------------------------------------------------------------------------------
int round_to_multiple_ten(int number)
{
        if(!number) return number;
        if(number < 10) return 10;
        else if(number < 100) return 100;
        else if(number < 1000) return 1000;
        else if(number < 10000) return 10000;
        return 100000;
}
//------------------------------------------------------------------------------