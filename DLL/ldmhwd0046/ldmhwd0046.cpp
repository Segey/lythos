/* 
 * File:   ldmhwd0046.cpp
 * Author: S.Panin
 * 
 * Created on 19 Март 2010 г., 0:48
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/MeasurementHistory/LDMeasurementHistory.h"

#include "ldmhwd0046.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDMeasurementHistoryWidgetData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        vec.push_back(instance_top_layout(data));
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDMeasurementHistoryWidgetData* data)
{
        data->username_label_        = new QLabel;
        data->username_icon_label_   = new QLabel;
        data->cap_label_             = new QLabel;
}
//------------------------------------------------------------------------------
QLayout* instance_top_layout(vLDMeasurementHistoryWidgetData* data)
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(data->username_icon_label_, data->username_label_);
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDMeasurementHistoryWidgetData* data)
{
        vLay lay(new QGridLayout);
        lay.add_caption_bevel_first(data->cap_label_);
        lay.add_unary(data->tree_view_, false);
        return lay.clayout();
}
