/* 
 * File:   ldmhwd0046.h
 * Author: S.Panin
 *
 * Created on 19 Март 2010 г., 0:48
 */
//------------------------------------------------------------------------------
#ifndef _V_LDMHWD_0046_H_
#define	_V_LDMHWD_0046_H_
//------------------------------------------------------------------------------
class QLayout;
class vLDMeasurementHistoryWidgetData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDMeasurementHistoryWidgetData* data);
extern "C" void                     instance_widgets(vLDMeasurementHistoryWidgetData* data);
extern "C" QLayout*                 instance_top_layout(vLDMeasurementHistoryWidgetData* data);
extern "C" QLayout*                 instance_middle_layout(vLDMeasurementHistoryWidgetData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDMHWD0046_H */
