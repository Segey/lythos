/* 
 * File:   St001ng.h
 * Author: S.Panin
 *
 * Created on 16 Сентябрь 2009 г., 12:59
 Работа со шрифтами
 */

//------------------------------------------------------------------------------
#ifndef _DLL_F12NT_H
#define	_DLL_F12NT_H
//------------------------------------------------------------------------------
#include <QSize>
#include <QFont>
#include <QString>
#include <QFontMetrics>
//------------------------------------------------------------------------------
extern "C" int                  height(QFont font);
extern "C" int                  width(QFont font, QString text);
extern "C" QSize                size(QFont font, QString text);
extern "C" bool                 is_exists()                                     { return true; }
//------------------------------------------------------------------------------
#endif	/* _DLL_F12NT_H */
