/* 
 * File:   St001ng.cpp
 * Author: S.Panin
 * 
 * Created on 16 Сентябрь 2009 г., 12:59
 */
//------------------------------------------------------------------------------
#include "f12nt.h"
//------------------------------------------------------------------------------
int width(QFont font, QString text)
{
        return QFontMetrics(font).width(text);
}
//------------------------------------------------------------------------------
int height(QFont font)
{
        return QFontMetrics(font).height();
}
//------------------------------------------------------------------------------
QSize size(QFont font, QString text)
{
        return QSize( width(font, text), height(font) );
}