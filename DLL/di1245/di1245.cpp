/* 
 * File:   di1245.cpp
 * Author: S.Panin
 * 
 * Created on 18 Январь 2010 г., 12:56
 */
//------------------------------------------------------------------------------
#include "../../Go/go.h"
#include "../../Go/Component/Dialog/Introduction/DialogIntroduction.h"

#include "di1245.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vDialogIntroductionData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        instance_buttons_layout(data);
        vec.push_back(instance_base_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vDialogIntroductionData* data)
{
        data->buy_button_            = new QPushButton;
        data->print_button_          = new QPushButton;
        data->thanks_button_         = new QPushButton;
        data->top_image_label_       = new QLabel;
        data->text_label_            = new QLabel;
        data->bottom_image_label_    = new QLabel;

        data->buy_button_->setDefault(true);
        data->text_label_->setWordWrap(true);
        data->text_label_->setAlignment(Qt::AlignJustify);
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vDialogIntroductionData* data)
{
        QVBoxLayout* center_layout = new QVBoxLayout;
        center_layout->addWidget(data->text_label_);
        center_layout->setMargin(14);
        return center_layout;
}
//------------------------------------------------------------------------------
QLayout* instance_base_layout(vDialogIntroductionData* data)
{
        QVBoxLayout* base_layout = new QVBoxLayout;
        base_layout->addWidget(data->top_image_label_);
        base_layout->addLayout(instance_middle_layout(data));
        base_layout->addWidget(data->bottom_image_label_);
        base_layout->setSpacing(0);
        base_layout->setMargin(0);
        return base_layout;
}
//------------------------------------------------------------------------------
void instance_buttons_layout(vDialogIntroductionData* data)
{
        QHBoxLayout* buttons_layout = new QHBoxLayout(data->bottom_image_label_);
        buttons_layout->setAlignment(Qt::AlignBottom);
        buttons_layout->addWidget(data->buy_button_,3);
        buttons_layout->addWidget(data->print_button_,2);
        buttons_layout->addWidget(data->thanks_button_,2);
}