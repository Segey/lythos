/* 
 * File:   di1245.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 12:56
 */
//------------------------------------------------------------------------------
#ifndef _V_DI1245_H
#define	_V_DI1245_H
//------------------------------------------------------------------------------
class QLayout;
class vDialogIntroductionData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance                (vDialogIntroductionData* data);
extern "C" void                     instance_widgets        (vDialogIntroductionData* data);
extern "C" QLayout*                 instance_middle_layout  (vDialogIntroductionData* data);
extern "C" void                     instance_buttons_layout (vDialogIntroductionData* data);
extern "C" QLayout*                 instance_base_layout    (vDialogIntroductionData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_DI1245_H */
