/* 
 * File:   da1128.cpp
 * Author: S.Panin
 * 
 * Created on 18 Январь 2010 г., 11:31
 */
//------------------------------------------------------------------------------
#include "../../Go/go.h"
#include "../../Go/Instance/Lay.h"
#include "../../Go/BuiltType/Label.h"
#include "../../Go/Component/Dialog/About/DialogAbout.h"

#include "da1128.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vDialogAboutData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        vec.push_back(instance_top_layout(data));
        instance_middle_layout(data);
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vDialogAboutData* data)
{
        data->background_label_      = new QLabel;
        data->text_label_            = new QLabel;
        data->icon_label_            = new QLabel;
        data->user_info_label_       = new QLabel;
        data->user_licence_label_    = new QLabel;
        data->warning_label_         = new QLabel;
        data->copyright_label_       = new QLabel;

        vLabel::set_word_wrap(data->warning_label_);
        vLabel::set_word_wrap(data->user_licence_label_);
}
//------------------------------------------------------------------------------
QLayout* instance_top_layout(vDialogAboutData* data)
{
        QVBoxLayout* center_layout = new QVBoxLayout;
        center_layout->addWidget(data->background_label_);
        center_layout->setMargin(0);
        return center_layout;
 }
//------------------------------------------------------------------------------
void instance_middle_layout(vDialogAboutData* data)
{
        vLay lay(new QGridLayout(data->background_label_), 2);
        lay.add_binary_2_layout(data->icon_label_, instance_user_layout(data));
        lay.add_unary(data->warning_label_);
        lay.add_unary(data->copyright_label_, false);
 }
//------------------------------------------------------------------------------
QLayout* instance_user_layout(vDialogAboutData* data)
{
        QVBoxLayout* layout = new QVBoxLayout;
        layout->setContentsMargins(40,0,0,0);
        layout->addWidget(data->text_label_);
        layout->addStretch();
        layout->addWidget(data->user_info_label_);
        layout->addWidget(data->user_licence_label_);
        layout->addStretch();
        return layout;
}
