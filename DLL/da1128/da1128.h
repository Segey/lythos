/* 
 * File:   da1128.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 11:31
 */
//------------------------------------------------------------------------------
#ifndef _V_DA1128_H_
#define	_V_DA1128_H_
//------------------------------------------------------------------------------
class QLayout;
class vDialogAboutData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vDialogAboutData* data);
extern "C" void                     instance_widgets(vDialogAboutData* data);
extern "C" QLayout*                 instance_top_layout(vDialogAboutData* data);
extern "C" void                     instance_middle_layout(vDialogAboutData* data);
extern "C" QLayout*                 instance_user_layout(vDialogAboutData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_DA1128_H_ */
