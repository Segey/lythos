TEMPLATE = lib
DESTDIR = dist/Release/GNU-Linux-x86
TARGET = da1128
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += dll release 
QT = core gui
SOURCES += da1128.cpp
HEADERS += da1128.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
QMAKE_CXXFLAGS += -std=gnu++0x
