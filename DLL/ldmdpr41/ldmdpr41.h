/* 
 * File:   ldmdpr41.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 14:49
 */
//------------------------------------------------------------------------------
#ifndef _V_LDMDPR41_H
#define	_V_LDMDPR41_H
//------------------------------------------------------------------------------
class QLayout;
class vLDMeasurementHistoryDialogPanelDesiredData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" void                     instance_widgets(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" void                     set_buddy(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" QLayout*                 instance_top_layout(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" QLayout*                 instance_middle_layout(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" QLayout*                 instance_bottom_layout(vLDMeasurementHistoryDialogPanelDesiredData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDMDPR41_H */
