/* 
 * File:   ldmdpr41.cpp
 * Author: S.Panin
 * 
 * Created on 16 Январь 2010 г., 14:49
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/MeasurementHistory/LDMeasurementHistory.h"

#include "ldmdpr41.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDMeasurementHistoryDialogPanelDesiredData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        set_buddy(data);
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDMeasurementHistoryDialogPanelDesiredData* data)
{
        data->date_label_            = new QLabel;
        data->date_edit_             = new QDateEdit;
        data->growth_label_          = new QLabel;
        data->growth_edit_           = new QLineEdit;
        data->weight_label_          = new QLabel;
        data->weight_edit_           = new QLineEdit;
        data->neck_label_            = new QLabel;
        data->neck_edit_             = new QLineEdit;
        data->shoulders_label_       = new QLabel;
        data->shoulders_edit_        = new QLineEdit;
        data->chest_label_           = new QLabel;
        data->chest_edit_            = new QLineEdit;
        data->bicep_label_           = new QLabel;
        data->bicep_edit_            = new QLineEdit;
        data->forearm_label_         = new QLabel;
        data->forearm_edit_          = new QLineEdit;
        data->wrist_label_           = new QLabel;
        data->wrist_edit_            = new QLineEdit;
        data->abdomen_label_         = new QLabel;
        data->abdomen_edit_          = new QLineEdit;
        data->waist_label_           = new QLabel;
        data->waist_edit_            = new QLineEdit;
        data->hips_label_            = new QLabel;
        data->hips_edit_             = new QLineEdit;
        data->thigh_label_           = new QLabel;
        data->thigh_edit_            = new QLineEdit;
        data->calf_label_            = new QLabel;
        data->calf_edit_             = new QLineEdit;
}
//------------------------------------------------------------------------------
void set_buddy(vLDMeasurementHistoryDialogPanelDesiredData* data)
{
        data->date_label_            ->setBuddy(data->date_edit_);
        data->growth_label_          ->setBuddy(data->growth_edit_);
        data->weight_label_          ->setBuddy(data->weight_edit_);
        data->neck_label_            ->setBuddy(data->neck_edit_);
        data->shoulders_label_       ->setBuddy(data->shoulders_edit_);
        data->chest_label_           ->setBuddy(data->chest_edit_);
        data->bicep_label_           ->setBuddy(data->bicep_edit_);
        data->forearm_label_         ->setBuddy(data->forearm_edit_);
        data->wrist_label_           ->setBuddy(data->wrist_edit_);
        data->abdomen_label_         ->setBuddy(data->abdomen_edit_);
        data->waist_label_           ->setBuddy(data->waist_edit_);
        data->hips_label_            ->setBuddy(data->hips_edit_);
        data->thigh_label_           ->setBuddy(data->thigh_edit_);
        data->calf_label_            ->setBuddy(data->calf_edit_);
 }
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDMeasurementHistoryDialogPanelDesiredData* data)
{
        vLay lay(new QGridLayout, 2);
        lay.add_unary_label (data->date_label_ ,             data->date_edit_);
        lay.add_unary_label (data->growth_label_ ,           data->growth_edit_);
        lay.add_unary_label (data->weight_label_ ,           data->weight_edit_);
        lay.add_unary_label (data->neck_label_ ,             data->neck_edit_);
        lay.add_unary_label (data->shoulders_label_ ,        data->shoulders_edit_);
        lay.add_unary_label (data->chest_label_ ,            data->chest_edit_);
        lay.add_unary_label (data->bicep_label_ ,            data->bicep_edit_);
        lay.add_unary_label (data->forearm_label_ ,          data->forearm_edit_);
        lay.add_unary_label (data->wrist_label_ ,            data->wrist_edit_);
        lay.add_unary_label (data->abdomen_label_ ,          data->abdomen_edit_);
        lay.add_unary_label (data->waist_label_ ,            data->waist_edit_);
        lay.add_unary_label (data->hips_label_ ,             data->hips_edit_);
        lay.add_unary_label (data->thigh_label_ ,            data->thigh_edit_);
        lay.add_unary_label (data->calf_label_ ,             data->calf_edit_);
        return lay.clayout();
}