TEMPLATE = lib
DESTDIR = dist/Debug/GNU-Linux-x86
TARGET = ldrhd740
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += dll debug 
QT = core gui sql
SOURCES += ldrhd740.cpp
HEADERS += ldrhd740.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
QMAKE_CXXFLAGS += -std=gnu++0x
