/* 
 * File:   ldrhd740.cpp
 * Author: S.Panin
 * 
 * Created on 19 Январь 2010 г., 7:41
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/Instance/Lay.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../Go/Instance/FormInstance/FormInstance.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/RecordHistory/RecordHistory.h"

#include "ldrhd740.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDRecordHistoryDialogData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        set_buddy(data);
        vec.push_back(instance_top_layout(data));
        vec.push_back(instance_middle_layout(data));
        vec.push_back(instance_bottom_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDRecordHistoryDialogData* data)
{
        data->username_label_        = new QLabel;
        data->username_icon_label_   = new QLabel;
        data->cap_label_             = new QLabel;
        data->exercise_label_        = new QLabel;
        data->exercise_edit_         = new QLineEdit;
        data->exercise_button_       = new QPushButton;
     //   data->left_panel_            = new vLDRecordHistoryDialogPanel;
     //   data->right_panel_           = new vLDRecordHistoryDialogPanel;
        data->update_button_         = new QPushButton;
        data->cancel_button_         = new QPushButton;
}
//------------------------------------------------------------------------------
void set_buddy(vLDRecordHistoryDialogData* data)
{
        data->exercise_label_->setBuddy(data->exercise_edit_);
}
//------------------------------------------------------------------------------
QLayout* instance_top_layout(vLDRecordHistoryDialogData* data)
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(data->username_icon_label_, data->username_label_);
        return TopInstance.clayout_with_size_hint();
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDRecordHistoryDialogData* data)
{
        QHBoxLayout* top_layout = new QHBoxLayout;
        top_layout->setMargin(0);
        top_layout->addWidget(data->exercise_label_);
        top_layout->addWidget(data->exercise_edit_);
        top_layout->addWidget(data->exercise_button_);

        vLay lay(new QGridLayout);
        lay.add_caption_bevel_first(data->cap_label_);
        lay.add_layout(top_layout, false);
        lay.add_binary(data->left_panel_, data->right_panel_, false);
        return lay.clayout();
}
//------------------------------------------------------------------------------
QLayout* instance_bottom_layout(vLDRecordHistoryDialogData* data)
{
        vBottomInstance BottomInstance;
        BottomInstance.add_bevel();
        BottomInstance.add_buttons(data->update_button_, data->cancel_button_);
        return BottomInstance.clayout_with_size_hint();
}
