/* 
 * File:   ldrhd740.h
 * Author: S.Panin
 *
 * Created on 19 Январь 2010 г., 7:41
 */
//------------------------------------------------------------------------------
#ifndef _V_LDRHD740_H
#define	_V_LDRHD740_H
//------------------------------------------------------------------------------
class QLayout;
class vLDRecordHistoryDialogData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDRecordHistoryDialogData* data);
extern "C" void                     instance_widgets(vLDRecordHistoryDialogData* data);
extern "C" void                     set_buddy(vLDRecordHistoryDialogData* data);
extern "C" QLayout*                 instance_top_layout(vLDRecordHistoryDialogData* data);
extern "C" QLayout*                 instance_middle_layout(vLDRecordHistoryDialogData* data);
extern "C" QLayout*                 instance_bottom_layout(vLDRecordHistoryDialogData* data);
extern "C" bool                     is_exists()                                 {  return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDRHD740_H */
