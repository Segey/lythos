/* 
 * File:   ldmdmp26.h
 * Author: S.Panin
 *
 * Created on 14 Январь 2010 г., 13:30
 */
//------------------------------------------------------------------------------
#ifndef _V_LDMDMP26_H
#define	_V_LDMDMP26_H
//------------------------------------------------------------------------------
class QLayout;
class vLDMeasurementHistoryDialogMethodData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDMeasurementHistoryDialogMethodData* data);
extern "C" void                     instance_widgets(vLDMeasurementHistoryDialogMethodData* data);
extern "C" void                     set_buddy(vLDMeasurementHistoryDialogMethodData* data);
extern "C" QLayout*                 instance_top_layout(vLDMeasurementHistoryDialogMethodData* data);
extern "C" QLayout*                 instance_middle_layout(vLDMeasurementHistoryDialogMethodData* data);
extern "C" QLayout*                 instance_bottom_layout(vLDMeasurementHistoryDialogMethodData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDMDMP26_H */
