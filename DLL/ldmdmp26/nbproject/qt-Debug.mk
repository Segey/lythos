#############################################################################
# Makefile for building: libldmdmp26.so.1.0.0
# Generated by qmake (2.01a) (Qt 4.7.0) on: Sun Mar 20 12:55:47 2011
# Project:  nbproject/qt-Debug.pro
# Template: lib
# Command: /home/dix/qtsdk-2010.05/qt/bin/qmake VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_SQL_LIB -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -pipe -g -Wall -W -D_REENTRANT -fPIC $(DEFINES)
CXXFLAGS      = -pipe -std=gnu++0x -g -Wall -W -D_REENTRANT -fPIC $(DEFINES)
INCPATH       = -I../../../../qtsdk-2010.05/qt/mkspecs/linux-g++ -Inbproject -I../../../../qtsdk-2010.05/qt/include/QtCore -I../../../../qtsdk-2010.05/qt/include/QtGui -I../../../../qtsdk-2010.05/qt/include/QtSql -I../../../../qtsdk-2010.05/qt/include -I. -Inbproject -I.
LINK          = g++
LFLAGS        = -Wl,-rpath,/home/dix/qtsdk-2010.05/qt/lib -shared -Wl,-soname,libldmdmp26.so.1
LIBS          = $(SUBLIBS)  -L/home/dix/qtsdk-2010.05/qt/lib -lQtSql -L/home/dix/qtsdk-2010.05/qt/lib -lQtGui -L/usr/X11R6/lib -lQtCore -lpthread 
AR            = ar cqs
RANLIB        = 
QMAKE         = /home/dix/qtsdk-2010.05/qt/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = build/Debug/GNU-Linux-x86/

####### Files

SOURCES       = ldmdmp26.cpp 
OBJECTS       = build/Debug/GNU-Linux-x86/ldmdmp26.o
DIST          = ../../../qtsdk-2010.05/qt/mkspecs/common/g++.conf \
		../../../qtsdk-2010.05/qt/mkspecs/common/unix.conf \
		../../../qtsdk-2010.05/qt/mkspecs/common/linux.conf \
		../../../qtsdk-2010.05/qt/mkspecs/qconfig.pri \
		../../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/debug.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/dll.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/shared.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/qt.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/moc.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/resources.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/uic.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/lex.prf \
		../../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf \
		nbproject/qt-Debug.pro
QMAKE_TARGET  = ldmdmp26
DESTDIR       = dist/Debug/GNU-Linux-x86/
TARGET        = libldmdmp26.so.1.0.0
TARGETA       = dist/Debug/GNU-Linux-x86/libldmdmp26.a
TARGETD       = libldmdmp26.so.1.0.0
TARGET0       = libldmdmp26.so
TARGET1       = libldmdmp26.so.1
TARGET2       = libldmdmp26.so.1.0

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: qttmp-Debug.mk  dist/Debug/GNU-Linux-x86/$(TARGET)

dist/Debug/GNU-Linux-x86/$(TARGET):  $(OBJECTS) $(SUBLIBS) $(OBJCOMP)  
	@$(CHK_DIR_EXISTS) dist/Debug/GNU-Linux-x86/ || $(MKDIR) dist/Debug/GNU-Linux-x86/ 
	-$(DEL_FILE) $(TARGET) $(TARGET0) $(TARGET1) $(TARGET2)
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS) $(OBJCOMP)
	-ln -s $(TARGET) $(TARGET0)
	-ln -s $(TARGET) $(TARGET1)
	-ln -s $(TARGET) $(TARGET2)
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET)
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET0)
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET1)
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET2)
	-$(MOVE) $(TARGET) $(TARGET0) $(TARGET1) $(TARGET2) dist/Debug/GNU-Linux-x86/



staticlib: $(TARGETA)

$(TARGETA):  $(OBJECTS) $(OBJCOMP) 
	-$(DEL_FILE) $(TARGETA) 
	$(AR) $(TARGETA) $(OBJECTS)

qttmp-Debug.mk: nbproject/qt-Debug.pro  ../../../../qtsdk-2010.05/qt/mkspecs/common/g++.conf \
		../../../../qtsdk-2010.05/qt/mkspecs/common/unix.conf \
		../../../../qtsdk-2010.05/qt/mkspecs/common/linux.conf \
		../../../../qtsdk-2010.05/qt/mkspecs/qconfig.pri \
		../../../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri \
		../../../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/debug.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/dll.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/shared.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/qt.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/moc.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/resources.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/uic.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/lex.prf \
		../../../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf \
		/home/dix/qtsdk-2010.05/qt/lib/libQtSql.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtCore.prl \
		/home/dix/qtsdk-2010.05/qt/lib/libQtGui.prl
	$(QMAKE) VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro
../../../../qtsdk-2010.05/qt/mkspecs/common/g++.conf:
../../../../qtsdk-2010.05/qt/mkspecs/common/unix.conf:
../../../../qtsdk-2010.05/qt/mkspecs/common/linux.conf:
../../../../qtsdk-2010.05/qt/mkspecs/qconfig.pri:
../../../../qtsdk-2010.05/qt/mkspecs/modules/qt_webkit_version.pri:
../../../../qtsdk-2010.05/qt/mkspecs/features/qt_functions.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/qt_config.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/exclusive_builds.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/default_pre.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/debug.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/default_post.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/dll.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/shared.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/warn_on.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/qt.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/unix/thread.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/moc.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/resources.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/uic.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/yacc.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/lex.prf:
../../../../qtsdk-2010.05/qt/mkspecs/features/include_source_dir.prf:
/home/dix/qtsdk-2010.05/qt/lib/libQtSql.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtCore.prl:
/home/dix/qtsdk-2010.05/qt/lib/libQtGui.prl:
qmake:  FORCE
	@$(QMAKE) VPATH=. -o qttmp-Debug.mk nbproject/qt-Debug.pro

dist: 
	@$(CHK_DIR_EXISTS) nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0 || $(MKDIR) nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0/ && $(COPY_FILE) --parents ldmdmp26.h nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0/ && $(COPY_FILE) --parents ldmdmp26.cpp nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0/ && (cd `dirname nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0` && $(TAR) ldmdmp261.0.0.tar ldmdmp261.0.0 && $(COMPRESS) ldmdmp261.0.0.tar) && $(MOVE) `dirname nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0`/ldmdmp261.0.0.tar.gz . && $(DEL_FILE) -r nbproject/build/Debug/GNU-Linux-x86/ldmdmp261.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET) 
	-$(DEL_FILE) dist/Debug/GNU-Linux-x86/$(TARGET0) dist/Debug/GNU-Linux-x86/$(TARGET1) dist/Debug/GNU-Linux-x86/$(TARGET2) $(TARGETA)
	-$(DEL_FILE) qttmp-Debug.mk


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all:
compiler_moc_header_clean:
compiler_rcc_make_all:
compiler_rcc_clean:
compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: 

####### Compile

build/Debug/GNU-Linux-x86/ldmdmp26.o: ldmdmp26.cpp ../../Go/go.h \
		../../Go/qt.h \
		../../Go/stl.h \
		../../Go/boost.h \
		../../Go/const.h \
		../../Go/Const/const_icon.h \
		../../Go/data.h \
		../../Go/tr.h \
		../../Go/go/noncopyable.h \
		../../Go/Const/const_namespace.h \
		../../LD/Classes/Main/vMain.h \
		../../LD/Classes/Main/MainDialog.h \
		../../LD/Classes/Main/MainInstance.h \
		../../Go/Singleton/BD/Base/SingletonBaseBD.h \
		../../Go/Singleton/BD/Action/ActionBD.h \
		../../Go/BD/new_bd/base_bd.h \
		../../Go/Strategy/message/strategy_message.h \
		../../Go/BuiltType/Action/Action.h \
		../../Go/Singleton/BD/Settings/SingletonSettingsBD.h \
		../../Go/Singleton/User/SingletonUser.h \
		../../Go/Singleton/User/SingletonUserFunctions.h \
		../../Go/Singleton/User/Translation/Translation.h \
		../../Go/Singleton/User/Translation/TranslationFunctions.h \
		../../Go/Singleton/User/Locale/Locale.h \
		../../Go/Singleton/User/Locale/LocaleDate.h \
		../../Go/Singleton/User/Locale/Mea/MeaHeight.h \
		../../Go/Singleton/User/Locale/Mea/MeaWeight.h \
		../../LD/Classes/LD/ClientLD/ClientLDDialog.h \
		../../LD/Classes/LD/ClientLD/ClientLDDialogInstance.h \
		../../LD/Classes/LD/MeasurementHistory/LDMeasurementHistory.h \
		../../Go/Component/All/WidgetModified/WidgetModified.h \
		../../Go/BuiltType/String.h \
		../../Go/Successor/SuccessorsChange.h \
		../../Go/Successor/SuccessorsTab.h \
		../../LD/Classes/LD/MeasurementHistory/TreeView/LDMeasurementHistoryTreeView.h \
		../../Go/Component/Tree/TreeView/TreeView.h \
		../../Go/Component/PuM/ThreeView/Body/TreeViewBodyPuM.h \
		../../Go/Component/PuM/ThreeView/Header/TreeViewHeaderPuM.h \
		../../Go/Component/Tree/TreeView/TreeViewData.h \
		../../LD/Classes/LD/MeasurementHistory/LDMeasurementHistoryData.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/LDMeasurementHistoryDialog.h \
		../../Go/Instance/Lay.h \
		../../Go/BuiltType/Dialog.h \
		../../Go/Instance/FormInstance/FormInstance.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelCurrent/LDMeasurementHistoryDialogPanelCurrent.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelCurrent/LDMeasurementHistoryDialogPanelCurrentData.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelDesired/LDMeasurementHistoryDialogPanelDesired.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelDesired/LDMeasurementHistoryDialogPanelDesiredData.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelMBs/LDMeasurementHistoryDialogPanelMBs.h \
		../../Go/Component/Chart/MatchBox/MatchBox.h \
		../../Go/Strategy/DLL/Dll.h \
		../../Go/Strategy/DLL/vStrategyDll.h \
		../../Go/Algorithm/Validator.h \
		../../Go/Algorithm/Interfase.h \
		../../Go/Component/Chart/MatchBox/Base/MBBase.h \
		../../Go/Component/Chart/MatchBox/Histogram/MBHistogram.h \
		../../Go/Component/Chart/MatchBox/Histogram/MBHistogramPercentage/MBHistogramPercentage.h \
		../../Go/Component/Chart/MatchBox/Histogram/MBHistogramScale/MBHistogramScale.h \
		../../Go/Component/Chart/MatchBox/Histogram/MBHistogramSimple/MBHistogramSimple.h \
		../../Go/Component/Chart/MatchBox/Pie/MBPie.h \
		../../Go/Component/Chart/MatchBox/Pie/MBPiePercentage/MBPiePercentage.h \
		../../Go/Component/Chart/MatchBox/Pie/MBPieScale/MBPieScale.h \
		../../Go/Component/Chart/MatchBox/Pie/MBPieSimple/MBPieSimple.h \
		../../Go/Component/Chart/MatchBox/Text/MBText.h \
		../../Go/Component/Chart/MatchBox/Text/MBTextPercentage/MBTextPercentage.h \
		../../Go/Component/Chart/MatchBox/Text/MBTextScale/MBTextScale.h \
		../../Go/Component/Chart/MatchBox/Text/MBTextSimple/MBTextSimple.h \
		../../Go/Component/Chart/MatchBox/Base/MBBaseStatic.h \
		../../Go/Component/Chart/MatchBox/MB.h \
		../../Go/Component/Chart/MatchBox/MBPuM.h \
		../../Go/BuiltType/Action/Chart/ActionChart.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelMBs/LDMeasurementHistoryDialogPanelMBsPuM.h \
		../../Go/BuiltType/Action/Body/ActionBody.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/PanelMBs/LDMeasurementHistoryDialogPanelMBsInstance.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/Dialog/LDMeasurementHistoryDialogMethod.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/Dialog/XML/XMLProportion.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/Dialog/LDMeasurementHistoryDialogMethodData.h \
		../../LD/Classes/LD/MeasurementHistory/Dialog/LDMeasurementHistoryDialogInstance.h \
		ldmdmp26.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o build/Debug/GNU-Linux-x86/ldmdmp26.o ldmdmp26.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

