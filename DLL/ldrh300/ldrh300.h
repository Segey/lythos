/* 
 * File:   ldrh300.h
 * Author: S.Panin
 *
 * Created on 19 Январь 2010 г., 13:01
 */
//------------------------------------------------------------------------------
#ifndef _V_LDRH300_H_
#define	_V_LDRH300_H_
//------------------------------------------------------------------------------
class QLayout;
class vRecordHistoryData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vRecordHistoryData* data);
extern "C" void                     instance_widgets(vRecordHistoryData* data);
extern "C" QLayout*                 instance_top_layout(vRecordHistoryData* data);
extern "C" QLayout*                 instance_middle_layout(vRecordHistoryData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDRH300_H_ */
