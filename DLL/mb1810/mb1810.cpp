/*
 * File:   In1256t.h
 * Author: S.Panin
 *
 * Created on 27 Сентябрь 2009 г., 12:59
 */
//------------------------------------------------------------------------------
#include "../../Go/go.h"
#include "mb1810.h"
//------------------------------------------------------------------------------
int count_percent_lines(int height,int font_height)
{
        double count = static_cast<double>(height) / font_height;
        if(count >20.0 ) return 20;
        if(count >10.0 ) return 10;
        if(count >5.0 ) return 5;
        if(count >4.0 ) return 4;
        if(count >2.0 ) return 2;
        return 1;
}
//------------------------------------------------------------------------------
int max_value(int value)
{
        if (value <10) return 10;
        int     val = pow( 10, QString::number(value).size() -2);
        return  (value/(val ? val : 1)  + 1) *val;
}
//------------------------------------------------------------------------------
double radian_to_degree(double Angle)
{
        return Angle * M_PI / 180;
};
//------------------------------------------------------------------------------
QRectF pie_point_value_min(QRect rect, int Angle, QSize font_size)
{
        const double Rx = (double)rect.width() / 2;
        const double Ry = (double)rect.height() / 2;
        const double x = cos(radian_to_degree(Angle / 2)) * Rx / 2;
        const double y = sin(radian_to_degree(Angle / 2)) * Ry / 2;

        if(font_size.height() > y * 2) return QRectF(0, 0, 0, 0);
        return QRectF(rect.x() + Rx + x - font_size.width() / 2, rect.y() + Ry - y - font_size.height() / 2, font_size.width(), font_size.height());
}
//------------------------------------------------------------------------------
QRectF pie_point_value_max(QRect rect, int Angle, QSize font_size)
{
        const double Rx = (double)rect.width() / 2;
        const double Ry = (double)rect.height() / 2;
        const double x = cos(radian_to_degree(Angle / 2)) * Rx / 2;
        const double y = sin(radian_to_degree(Angle / 2)) * Ry / 2;
        return QRectF(rect.x() + Rx - x - font_size.width() / 2, rect.y() + Ry + y - font_size.height() / 2, font_size.width(), font_size.height());
}