/* 
 * File:   In1256t.h
 * Author: S.Panin
 *
 * Created on 27 Сентябрь 2009 г., 12:59
 MatchBox MB
 */
//------------------------------------------------------------------------------
#ifndef _DLL_MB1810_H
#define	_DLL_MB1810_H
//------------------------------------------------------------------------------
#include <QRectF>
//------------------------------------------------------------------------------
extern "C" int      count_percent_lines(int height, int font_height);
extern "C" int      max_value(int value);
extern "C" double   radian_to_degree(double Angle);
extern "C" QRectF   pie_point_value_min(QRect rect, int Angle, QSize font_size);
extern "C" QRectF   pie_point_value_max(QRect rect, int Angle, QSize font_size);
extern "C" bool     is_exists()                                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _DLL_IN1256T_H */
