/* 
 * File:   tw1313.h
 * Author: S.Panin
 *
 * Created on 13 Март 2010 г., 21:50
 */
//------------------------------------------------------------------------------
#ifndef _V_TW1313_H
#define	_V_TW1313_H
//------------------------------------------------------------------------------
class QLayout;
class vTreeViewData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vTreeViewData* data);
extern "C" void                     instance_widgets(vTreeViewData* data);
extern "C" void                     instance_tree_view(vTreeViewData* data);
extern "C" void                     instance_buttons(vTreeViewData* data);
extern "C" QLayout*                 instance_middle_layout(vTreeViewData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_TW1313_H */
