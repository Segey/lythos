/* 
 * File:   tw1313.cpp
 * Author: S.Panin
 * 
 * Created on 13 Март 2010 г., 21:50
 */
//------------------------------------------------------------------------------
#include "../../Go/go.h"
#include "../../Go/Instance/Lay.h"
#include "../../Go/Component/Tree/TreeView/TreeView.h"

#include "tw1313.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vTreeViewData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        instance_tree_view(data);
        instance_buttons(data);
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vTreeViewData* data)
{
        data->tree_view_     = new QTreeWidget;
        data->new_button_    = new QPushButton;
        data->open_button_   = new QPushButton;
        data->delete_button_ = new QPushButton;
 }
//------------------------------------------------------------------------------
void instance_tree_view(vTreeViewData* data)
{
        data->tree_view_->setRootIsDecorated(false);
        data->tree_view_->setAlternatingRowColors(true);
        data->tree_view_->setSortingEnabled(true);
        data->tree_view_->setContextMenuPolicy(Qt::CustomContextMenu);
        data->tree_view_->header()->setContextMenuPolicy(Qt::CustomContextMenu);
}
//------------------------------------------------------------------------------
void instance_buttons(vTreeViewData* data)
{
        data->new_button_->setDefault(true);
        data->new_button_->setShortcut(QKeySequence::New);
        data->open_button_->setShortcut(QKeySequence::Open);
        data->delete_button_->setShortcut(QKeySequence::Delete);
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vTreeViewData* data)
{
        QHBoxLayout* h_layout = new QHBoxLayout;
        h_layout->addWidget(data->new_button_);
        h_layout->addWidget(data->open_button_);
        h_layout->addWidget(data->delete_button_);
        h_layout->addStretch();

        vLay lay(new QGridLayout);
        lay.add_unary (data->tree_view_, false);
        lay.add_layout(h_layout, false);
        lay.layout()->setMargin(0);
        return lay.clayout();
 }