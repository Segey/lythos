/* 
 * File:   St001ng.h
 * Author: S.Panin
 *
 * Created on 16 Сентябрь 2009 г., 12:59
 */
//------------------------------------------------------------------------------
#ifndef _V_ST001NG_H
#define	_V_ST001NG_H
//------------------------------------------------------------------------------
#include <QString>
//------------------------------------------------------------------------------
extern "C" QString                  s_truncate(QString str,int size);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_ST001NG_H */
