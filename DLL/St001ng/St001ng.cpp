/* 
 * File:   St001ng.cpp
 * Author: S.Panin
 * 
 * Created on 16 Сентябрь 2009 г., 12:59
 */
//------------------------------------------------------------------------------
#include "St001ng.h"
//------------------------------------------------------------------------------
QString s_truncate(QString str,int size)
{
        if (str.size() <size) return str;

        str.truncate(size);
        str +="...";
        return str;
}