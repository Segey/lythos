/* 
 * File:   ldpi99.cpp
 * Author: S.Panin
 * 
 * Created on 16 Январь 2010 г., 21:30
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/Instance/Lay.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../Go/Instance/FormInstance/FormInstance.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/PersonalInfo/LDPersonalInfo.h"

#include "ldpi99.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDPersonalInfoData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        set_buddy(data);
        instance_validators(data);
        vec.push_back(instance_top_layout(data));
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDPersonalInfoData* data)
{
        data->caption_label_            = new QLabel;
        data->cap_personal_label_       = new QLabel;
        data->name_label_               = new QLabel;
        data->name_edit_                = new QLineEdit;
        data->password_label_           = new QLabel;
        data->password_edit_            = new QLineEdit;
        data->re_password_label_        = new QLabel;
        data->re_password_edit_         = new QLineEdit;
        data->sex_label_                = new QLabel;
        data->sex_combobox_             = new QComboBox;
        data->date_of_birth_label_      = new QLabel;
        data->date_of_birth_edit_       = new QDateEdit;
        data->height_label_             = new QLabel;
        data->height_edit_              = new QLineEdit;       
        data->cap_contact_label_        = new QLabel;
        data->country_label_            = new QLabel;
        data->country_edit_             = new QLineEdit;
        data->state_region_label_       = new QLabel;
        data->state_region_edit_        = new QLineEdit;
        data->city_town_label_          = new QLabel;
        data->city_town_edit_           = new QLineEdit;
        data->address_label_            = new QLabel;
        data->address_edit_             = new QLineEdit;
        data->email_label_              = new QLabel;
        data->email_edit_               = new QLineEdit;
        data->icq_label_                = new QLabel;
        data->icq_edit_                 = new QLineEdit;
        data->phone_label_              = new QLabel;
        data->phone_edit_               = new QLineEdit;
        data->mobule_label_             = new QLabel;
        data->mobule_edit_              = new QLineEdit;
        data->cap_addition_label_       = new QLabel;
        data->doctor_label_             = new QLabel;
        data->doctor_edit_              = new QLineEdit;
        data->instructor_label_         = new QLabel;
        data->instructor_edit_          = new QLineEdit;
}
//------------------------------------------------------------------------------
void set_buddy(vLDPersonalInfoData* data)
{
        data->name_label_               ->setBuddy(data->name_edit_);
        data->password_label_           ->setBuddy(data->password_edit_);
        data->re_password_label_        ->setBuddy(data->re_password_edit_);
        data->sex_label_                ->setBuddy(data->sex_combobox_);
        data->date_of_birth_label_      ->setBuddy(data->date_of_birth_edit_);
        data->height_label_             ->setBuddy(data->height_edit_);
        data->country_label_            ->setBuddy(data->country_edit_);
        data->state_region_label_       ->setBuddy(data->state_region_edit_);
        data->city_town_label_          ->setBuddy(data->city_town_edit_);
        data->address_label_            ->setBuddy(data->address_edit_);
        data->email_label_              ->setBuddy(data->email_edit_);
        data->icq_label_                ->setBuddy(data->icq_edit_);
        data->phone_label_              ->setBuddy(data->phone_edit_);
        data->mobule_label_             ->setBuddy(data->mobule_edit_);
        data->doctor_label_             ->setBuddy(data->doctor_edit_);
        data->instructor_label_         ->setBuddy(data->instructor_edit_);
}
//------------------------------------------------------------------------------
void instance_validators(vLDPersonalInfoData* data)
{
        data->country_edit_     ->setMaxLength(128);
        data->state_region_edit_->setMaxLength(128);
        data->city_town_edit_   ->setMaxLength(128);
        data->address_edit_     ->setMaxLength(128);
        data->doctor_edit_      ->setMaxLength(128);
        data->instructor_edit_  ->setMaxLength(128);
        data->password_edit_    ->setEchoMode(QLineEdit::Password);
        data->re_password_edit_ ->setEchoMode(QLineEdit::Password);
        data->name_edit_        ->setMaxLength(128);
        data->password_edit_    ->setMaxLength(64);
        data->re_password_edit_ ->setMaxLength(64);
        data->height_edit_      ->setMaxLength(10);
        data->height_edit_      ->setValidator(new QDoubleValidator(1.0 , 300.0, 2, data->height_edit_));

        instance_reg_validator(data->icq_edit_,"^[0-9]{5,9}$");
        instance_reg_validator(data->mobule_edit_,"^[0-9]{9,12}$");
        instance_reg_validator(data->email_edit_,"[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,6}");
        instance_reg_validator(data->phone_edit_,"^[0-9]{9,12}$");
}
//------------------------------------------------------------------------------
QLayout* instance_top_layout(vLDPersonalInfoData* data)
{
        vTopInstance TopInstance;
        TopInstance.add_caption_label(data->caption_icon_image_panel_, data->caption_label_);
        return TopInstance.clayout_with_size_hint();
 }
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDPersonalInfoData* data)
{
        vLay lay(new QGridLayout, 4);
        lay.add_caption_bevel_first     (data->cap_personal_label_);
        lay.add_unary_label             (data->name_label_,             data->name_edit_);
        lay.add_binary_label            (data->password_label_,         data->password_edit_, data->re_password_label_, data->re_password_edit_);
        lay.add_unary_label             (data->sex_label_,              data->sex_combobox_);
        lay.add_unary_label             (data->date_of_birth_label_,    data->date_of_birth_edit_);
        lay.add_unary_label             (data->height_label_,           data->height_edit_);

        lay.add_caption                 (data->cap_contact_label_, false);
        lay.add_unary_label             (data->country_label_,          data->country_edit_);
        lay.add_unary_label             (data->state_region_label_,     data->state_region_edit_);
        lay.add_binary_label            (data->city_town_label_,        data->city_town_edit_,          data->address_label_,   data->address_edit_);
        lay.add_binary_label            (data->email_label_,            data->email_edit_,              data->icq_label_,       data->icq_edit_);
        lay.add_binary_label            (data->phone_label_,            data->phone_edit_,              data->mobule_label_,    data->mobule_edit_);

        lay.add_caption                 (data->cap_addition_label_, false);
        lay.add_unary_label             (data->doctor_label_,           data->doctor_edit_);
        lay.add_unary_label             (data->instructor_label_,       data->instructor_edit_);
        return lay.clayout();
}
//------------------------------------------------------------------------------
void instance_reg_validator(QLineEdit* edit, const QString& value)
{
        QRegExp reg(value);
        edit->setValidator(new QRegExpValidator(reg, edit));
}
