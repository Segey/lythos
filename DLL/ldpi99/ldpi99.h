/* 
 * File:   ldpi99.h
 * Author: S.Panin
 *
 * Created on 16 Январь 2010 г., 21:30
 */
//------------------------------------------------------------------------------
#ifndef _V_LDPI99_H
#define	_V_LDPI99_H
//------------------------------------------------------------------------------
class QLayout ;
class QString;
class QLineEdit;
class vLDPersonalInfoData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDPersonalInfoData* data);
extern "C" void                     instance_widgets(vLDPersonalInfoData* data);
extern "C" void                     set_buddy(vLDPersonalInfoData* data);
extern "C" void                     instance_validators(vLDPersonalInfoData* data);
extern "C" QLayout*                 instance_top_layout(vLDPersonalInfoData* data);
extern "C" QLayout*                 instance_middle_layout(vLDPersonalInfoData* data);
extern "C" void                     instance_reg_validator(QLineEdit* edit, const QString& value);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDPI99_H */
