/* 
 * File:   ldrhp022.h
 * Author: S.Panin
 *
 * Created on 18 Январь 2010 г., 9:22
 */
//------------------------------------------------------------------------------
#ifndef _V_LDRHP022_H
#define	_V_LDRHP022_H
//------------------------------------------------------------------------------
class QLayout;
class vLDRecordHistoryDialogPanelData;
//------------------------------------------------------------------------------
extern "C" std::vector<QLayout*>    instance(vLDRecordHistoryDialogPanelData* data);
extern "C" void                     instance_widgets(vLDRecordHistoryDialogPanelData* data);
extern "C" void                     set_buddy(vLDRecordHistoryDialogPanelData* data);
extern "C" void                     instance_comboboxs(vLDRecordHistoryDialogPanelData* data);
extern "C" QLayout*                 instance_middle_layout(vLDRecordHistoryDialogPanelData* data);
extern "C" bool                     is_exists()                                 { return true; }
//------------------------------------------------------------------------------
#endif	/* _V_LDRHP022_H */
