/* 
 * File:   ldrhp022.cpp
 * Author: S.Panin
 * 
 * Created on 18 Январь 2010 г., 9:22
 */
//------------------------------------------------------------------------------
#include <QSqlQuery>
#include "../../Go/go.h"
#include "../../Go/Instance/Lay.h"
#include "../../Go/go/noncopyable.h"
#include "../../Go/Const/const_namespace.h"
#include "../../LD/Classes/Main/vMain.h"
#include "../../LD/Classes/LD/ClientLD/ClientLDDialog.h"
#include "../../LD/Classes/LD/RecordHistory/RecordHistory.h"

#include "ldrhp022.h"
//------------------------------------------------------------------------------
std::vector<QLayout*> instance(vLDRecordHistoryDialogPanelData* data)
{
        std::vector<QLayout*> vec;
        instance_widgets(data);
        set_buddy(data);
        instance_comboboxs(data);
        vec.push_back(instance_middle_layout(data));
        return vec;
}
//------------------------------------------------------------------------------
void instance_widgets(vLDRecordHistoryDialogPanelData* data)
{
        data->cap_label_                     = new QLabel;
        data->date_label_                    = new QLabel;
        data->date_edit_                     = new QDateEdit;
        data->cap_record_label_              = new QLabel;
        data->weight_label_                  = new QLabel;
        data->weight_combobox_               = new QComboBox;
        data->repetitions_label_             = new QLabel;
        data->repetitions_combobox_          = new QComboBox;
        data->cap_notes_label_               = new QLabel;
     //   data->notes_text_edit_               = new vDockRichEdit(data);
        data->notes_progress_label_          = new QLabel;
        data->notes_progress_bar_            = new QProgressBar;
}
//------------------------------------------------------------------------------
void set_buddy(vLDRecordHistoryDialogPanelData* data)
{
        data->date_label_->setBuddy(data->date_edit_);
        data->weight_label_->setBuddy(data->weight_combobox_);
        data->repetitions_label_ ->setBuddy(data->repetitions_combobox_);
}
//------------------------------------------------------------------------------
void instance_comboboxs(vLDRecordHistoryDialogPanelData* data)
{
        for(int i=0; i<=data->max_weight()/5; ++i) data->weight_combobox_->addItem(QString::number(i));
        for(int i=0; i<=data->max_repetitions()/2; ++i) data->repetitions_combobox_->addItem(QString::number(i));
        data->weight_combobox_->setEditable(true);
        data->repetitions_combobox_->setEditable(true);
}
//------------------------------------------------------------------------------
QLayout* instance_middle_layout(vLDRecordHistoryDialogPanelData* data)
{
        vLay lay(new QGridLayout, 4);
        lay.add_caption         (data->cap_label_, false);
        lay.add_unary_label     (data->date_label_,          data->date_edit_);
        lay.add_caption         (data->cap_record_label_, false);
        lay.add_unary_label     (data->weight_label_,        data->weight_combobox_);
        lay.add_unary_label     (data->repetitions_label_,   data->repetitions_combobox_);
        lay.add_caption         (data->cap_notes_label_, false);
        lay.add_unary           (data->notes_text_edit_);
        lay.add_unary_label     (data->notes_progress_label_,data->notes_progress_bar_);
        return lay.clayout();
}