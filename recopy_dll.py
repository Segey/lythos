#!/usr/bin/env python3.1

import os.path;
import shutil;
import re;
import os;

path="DLL";
Config="Debug";
copy_path='/home/dix/Projects/Lythos/LD/dist/Debug/GNU-Linux-x86/';
Regex = r'{0}.*(lib.*\.so)(\.1\.0\.0)'.format(Config);
is_copied=0;

print ("WORK with config '{0}' in dir  '{1}'.".format(Config, os.path.abspath(path)));

for root, dirs, files in os.walk(path) :
    for filename in files :
        abs_file_name = os.path.join(os.path.abspath(root), filename);
        if (None != re.search(Regex, abs_file_name)) :
            text = re.sub(Regex,r'\1',abs_file_name);
            os.rename(abs_file_name,  text);
            shutil.copyfile(text, os.path.abspath(copy_path) + os.path.basename(text));
            is_copied +=1;
            print (" - file \t{0:<25} replased to {1:<25} and copied to\t {2}!".format(filename, os.path.basename(text), os.path.abspath(copy_path) + os.path.basename(text)));


if (is_copied) : print ("WELL DONE. Was copied {0} files!".format(is_copied));
else : print ("Nothing to DO!");
exit (0);
