# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Debug/GNU-Linux-x86
TARGET = EG_Creater
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += debug 
QT = core gui sql xml
SOURCES += main.cpp classes/Stockman/Stockman.cpp classes/Token/TokenExtractor.cpp
HEADERS += classes/Token/ByteCode.h ../Go/BD/new_bd/base_bd.h ../Go/Algorithm/Validator.h ../Go/tr.h classes/Token/TokenExtractor.h ../Go/Component/Console/Console.h ../Go/const.h ../Go/Component/Console/ConsoleKey.h classes/Token/Token.h ../Go/BD/new_bd/BD.h pch.h ../Go/BD/new_bd/Source.h ../Go/Strategy/message/strategy_message.h classes/Database_info.h ../Go/execption.h classes/Console/ConsoleEGCreater.h classes/Source_EG_Creater.h classes/Stockman/Stockman.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc-4.5
QMAKE_CXX = g++-4.5
DEFINES += 
INCLUDEPATH += . 
LIBS += /usr/local/lib/libboost_program_options.a  
QMAKE_CXXFLAGS += -std=gnu++0x
CONFIG += precompile_header
CONFIG +=console
PRECOMPILED_HEADER =pch.h
