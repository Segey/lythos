// -*- C++ -*-
/* 
 * File:   pch.h
 * Author: S.Panin
 *
 * Created on 8 Январь 2010 г., 22:55
 */
//------------------------------------------------------------------------------
#ifndef _V_PCH_H_
#define	_V_PCH_H_
//------------------------------------------------------------------------------
#include "../Go/go_full.h"
#include "../Go/go/noncopyable.h"
#include "../Go/Test/Test.h"

//------------------------------------------------------------------------------
#include "../Go/BuiltType/String.h"
#include "../Go/BuiltType/Number.h"

#include "../Go/Classes/Html/Html.h"
#include "../Go/Component/Panel/EditList/EditListPanel.h"
//------------------------------------------------------------------------------
//#include "../EGG/consts.h"
//------------------------------------------------------------------------------
#endif	/* _V_PCH_H_ */

