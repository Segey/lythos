/*
 * File:   main.cpp
 * Author: dix75
 *
 * Created on 15 Июнь 2010 г., 11:02
 */
//------------------------------------------------------------------------------
#include <iostream>
#include <QDirIterator>
#include "../Go/const.h"
#include "../Go/execption.h"

#include "../Go/BD/new_bd/BD.h"
#include "../Go/BD/new_bd/Source.h"
#include "../Go/Component/Console/Console.h"
#include "../Go/Strategy/message/strategy_message.h"

#include "classes/Database_info.h"
#include "classes/Token/ByteCode.h"
#include "classes/Source_EG_Creater.h"
#include "classes/Stockman/Stockman.h"
#include "classes/Token/TokenExtractor.h"
#include "classes/Console/ConsoleEGCreater.h"
//------------------------------------------------------------------------------
typedef                 go::Console<go::ConsoleEGCreator>                       console_type;
typedef                 go::db::Source<go::db::source::EG_Creater>::class_name  source;
//------------------------------------------------------------------------------
void    test(int argc, char *argv[])
{
        vTest test;
        test.start();
        CLASS_TEST(test, go::Console<go::ConsoleEGCreator>(argc, argv));
        CLASS_TEST(test, go::TokenExtractor(""));
        test.end();
}
//------------------------------------------------------------------------------
bool    check_exists(const console_type& console)
{
        if (false == QDir(console.path()).exists())     FALSE_MESSAGE (std::cout << " - Dirrectory '" + console.path().toStdString() + "' not exists.\n");
        if (false == QFile(console.conffile()).exists()) FALSE_MESSAGE (std::cout << " - Configuration file '" + console.conffile().toStdString() + "' not exists.\n");
        return true;
}
//------------------------------------------------------------------------------
bool    create_database(const QString& file)
{
        if (go::DB<go::strategy::message::console>::create_database(source::list() ,file,source::database())) return true;

        std::cout << go::consts::program::name_full() << " could not open \'" << file.toStdString() << "\' because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn't correctly decoded)." <<std::endl;
        return false;
}
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
        using namespace go;

        console_type console (argc, argv);
        if ( false == console.execute()) return EXIT_FAILURE;
        if (console.is_test())  test(argc, argv);

        if (false == check_exists(console) || false == create_database(console.file()) )  return EXIT_FAILURE;
        go::DB<go::strategy::message::console> db (console.file(), source::database());

        QDirIterator it(console.path(), QStringList() <<"*.html" <<"*.htm", QDir::Files | QDir::Hidden | QDir::NoSymLinks | QDir::System | QDir::Dirs | QDir::NoDotAndDotDot ,QDirIterator::Subdirectories);
while (it.hasNext())
{
        const QString file_name = it.next();
        try
        {
                typedef go::TokenExtractor::class_tokens class_tokens;

                go::TokenExtractor extractor(file_name);
                extractor.extract();
                class_tokens& tokens = extractor.tokens();

                if (typeid(tokens.front()) != typeid(go::ByteCodeFile) ) throw go::bad_data(" - File '" + file_name.toStdString() + "' incorrect. First token must be <!--- \\file ... -->.");
                go::ByteCode& bc = tokens.front();
                bc.set_db(db.database());
                if (false == bc.save()) throw go::bad_data(" - File '" + file_name.toStdString() + "' incorrect. First token must be <!--- \\file ... -->.");

                const size_t file_id = bc.file_id();
                if ( 0 == file_id ) throw go::bad_data(" - File '" + file_name.toStdString() + "' incorrect. Dont add this file in bd.");          
                
                std::for_each(tokens.begin() + 1, tokens.end(),[&]( class_tokens::reference val )
                {
                        val.set_db(db.database());
                        val.set_file_id(file_id);
                        val.save();
                });

                if (QString() != console.conffile())
                {
                        Database_info info (console.conffile(), db.database());
                        if (false == info.save()) throw go::bad_data(" - Configuration file '" + console.conffile().toStdString() + "' incorrect. Dont save database info in bd.");
                }

        }
        catch (go::file_not_found& e)
        {
                std::cout << e.what() <<std::endl;
        }
        catch(go::bad_data& e)
        {
               std::cout << e.what() <<std::endl;
        }
}
        if (QFile(console.file()).exists()) std::cout << "Database '" + console.file().toStdString() + "' is successfully created!" << std::endl;
        return EXIT_SUCCESS;
}