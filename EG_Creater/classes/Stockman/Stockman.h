/**
    \file   Stockman.h
    \brief  Файл Stockman.h

    \author dix75
    \date   Created on 12 Декабрь 2010 г., 13:49
 */
//------------------------------------------------------------------------------
#ifndef _STOCKMAN_H_
#define	_STOCKMAN_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
/**
 \class Stockman
 */
template<typename T>
class Stockman
{
    typedef                         T                                           class_token;
    typedef                         Stockman<class_token>                       class_name;

    class_token                                                                 tokens_;

public:

    explicit Stockman(const class_token& tokens) : tokens_(tokens)              {}
    void    save()
    {
        
    }
};
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _STOCKMAN_H_ */
