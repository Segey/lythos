/**
    \file   Database_info.h
    \brief  Файл Database_info.h

    \author S.Panin
    \date   Created on 7 Января 2011 г., 01:42
 */
//------------------------------------------------------------------------------
#ifndef _DATABASE_INFO_H_
#define	_DATABASE_INFO_H_
//------------------------------------------------------------------------------
#include "../../Go/Algorithm/xml.h"
#include "../../Go/Algorithm/Validator.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

class Database_info : go::noncopyable
{
    typedef                     Database_info                                   class_name;
    typedef                     QSqlDatabase                                    db_type;

    QString                                                                     file_;
    db_type                                                                     db_;

    static QString                  title()                                     { return  "title"; }
    static QString                  content()                                   { return  "content"; }
    static QString                  version()                                   { return  "version"; }

public:
    explicit                        Database_info(const QString& file, const db_type& db) : file_(file), db_(db) {}
    bool                            save()
    {
         validator::type_double val = validator::is_double(xml::element(file_,version()));
         const double version = val.first() ? val.second() : 1.0 ;

         QSqlQuery query(db_);
         query.prepare("INSERT INTO database_info (version, date, title, content) VALUES(?,?,?,?);");
         query.addBindValue(version);
         query.addBindValue(QDateTime::currentMSecsSinceEpoch());
         query.addBindValue(xml::element(file_,title()));
         query.addBindValue(xml::element(file_,content()));
         return query.exec();
    }
};
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _DATABASE_INFO_H_ */
