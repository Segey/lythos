/**
    \file   ByteCode.h
    \brief  Файл ByteCode.h

    \author S.Panin
    \date   Created on 12 Декабрь 2010 г., 22:43
 */
//------------------------------------------------------------------------------
#ifndef _V_BYTE_CODE_H_
#define	_V_BYTE_CODE_H_
//------------------------------------------------------------------------------
#include "../../../Go/Algorithm/Validator.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
/**
 \class ByteCode
 \brief Базовый класс кодов
 */
class ByteCode
{
    typedef                     ByteCode                                        class_name;
    typedef                     QSqlDatabase                                    db_type;

    QStringList                                                                 code_;
    db_type                                                                     db_;
    size_t                                                                      file_id_;

protected:
    /** 
    \brief Функция для получения строки с кодом.
    \result Строка передаваемая по ссылке.
    */
    QStringList&                code()                                          { return code_; }
    /** \brief Виртуальная функция явного сохранения данных в БД.  */
    virtual bool                do_save() = 0;
    /** \brief Виртуальная функция явной проверки корректности данных. */
    virtual bool                do_check() = 0;
     /**
     \brief Функция получения указателя 'Базы данных'.
     \param db - указатель базы данных.
     */
    db_type                     db()                                            { return db_;}
    
public:
    /**
     \brief Функция добавления строки с кодом в класс
     \param code - Строка с кодом
     */
     void                       set_code(const QStringList& code)               { code_ = code; }
     /**
     \brief Функция добавления указателя 'Базы данных'.
     \param db - указатель базы данных.
     */
     void                       set_db(db_type db)                              { db_ = db; }

     /**
     \brief Функция добавления file_id.
     \param file_id - id файла.
     */
     void                       set_file_id(size_t file_id)                     { file_id_ = file_id; }

     /**
     \brief Функция получения file_id.
     \result file_id - id файла.
     */
     uint                       file_id()                                       { return file_id_; }

    /** 
    \brief Функция для сохранения данных в БД.
    \result Успешность проведения операции;
     */
    bool                        save()                                          { return do_save(); }
    /** \brief Функция для сохранения данных в БД. */
    bool                        check()                                         { return do_check(); }
    /** \brief Виртуальный деструктор. */
    virtual ~ByteCode() {}
};
//------------------------------------------------------------------------------
/**
 \class ByteCodeNone
 \brief Пустой класс
 */
class ByteCodeNone : public ByteCode
{
    typedef                     ByteCode                                        inherited;
    typedef                     ByteCodeNone                                    class_name;

protected:
    /** \brief Виртуальная функция явного сохранения данных в БД. */
    virtual bool                do_save()                                       {return false; }
    /** \brief Виртуальная функция явной проверки корректности данных. */
    virtual bool                do_check()                                      {return false; }
};
//------------------------------------------------------------------------------
/**
 \class ByteCodeFile
 \brief Класс кода - File
 */
class ByteCodeFile : public ByteCode
{
    typedef                     ByteCode                                        inherited;
    typedef                     ByteCodeFile                                    class_name;
    static std::string          re_string()                                     { return "<!--- *\\\\file +([^(\r\n|\n|\\-\\->)]+).*-->"; }

    QString                                                                     title_;
    QString                                                                     file_;

protected:

    /**   \brief Виртуальная функция явного сохранения данных в БД. */
    virtual bool                do_save()
    {
        QFile file(file_);
        if (false == file.open(QIODevice::ReadOnly | QIODevice::Text))  return false;

         QSqlQuery query(inherited::db());
         query.prepare("INSERT INTO files (path, date, title, content) VALUES(?,?,?,?);");
         query.addBindValue(file_);
         query.addBindValue(QDateTime::currentMSecsSinceEpoch());
         query.addBindValue(title_);
         query.addBindValue(QTextStream(&file).readAll());
         if (false == query.exec()) return false;

         inherited::set_file_id(query.lastInsertId().toUInt());
         return true;
    }
    
    /** \brief Виртуальная функция явной проверки корректности данных. */
    virtual bool                do_check()
    {
        QRegExp re( re_string().c_str() );
        if ( -1 == re.indexIn(inherited::code().join(""))) return false;

        title_ = re.cap(1);
        return true;
    }
public:
    /**
     \brief Конструктор
     \param file - Имя добавляемого файла. */
    explicit ByteCodeFile(const QString& file) : file_(file)                    {}
};
//------------------------------------------------------------------------------
/**
 \class ByteCodeSet
 \brief Класс кода - Set
 */
class ByteCodeSet : public ByteCode
{
    typedef                     ByteCode                                        inherited;
    typedef                     ByteCodeSet                                     class_name;
    typedef                     std::pair<int,int>                              pair_type;

    static std::string          re_string()                                     { return "<!--- *\\\\set +([^(\r(\n)?|\\\\value)]+).*\\\\value +\\[([^\\]]+).*-->"; }
    static std::string          re_string_value()                               { return "(\\d+) *: *(\\d+) *(,|$)"; }

    QString                                                                     title_;
    std::vector<pair_type >                                                     values_;

    bool                        check_values(const QString& str)
    {
        size_t pos = 0;
        QRegExp rx(re_string_value().c_str());

        while ((pos = rx.indexIn(str, pos)) != -1)
        {
            try
            {
                go::validator::type_int val1 = go::validator::is_int(rx.cap(1));
                go::validator::type_int val2 = go::validator::is_int(rx.cap(2));
                if (val1.first() && val2.first()) values_.push_back(pair_type(val1.second(), val2.second()));
                pos += rx.matchedLength();
            }
            catch(...)
            {
                return false;
            }
        }
        return pos;
    }

protected:
    /** \brief Виртуальная функция явного сохранения данных в БД. */
    virtual bool                do_save()
    {
         QSqlQuery query(inherited::db());
         query.prepare("INSERT INTO sets (file_id, title) VALUES(?,?);");
         query.addBindValue(inherited::file_id());
         query.addBindValue(title_);
         if (false == query.exec()) return false;

         const uint set_id = query.lastInsertId().toUInt();
         uint num = 0;

         std::for_each(values_.begin(), values_.end(), [&](const pair_type& pair)
         {
                query.prepare("INSERT INTO sets_stack (set_id, num, weight, repetition) VALUES(?,?,?,?);");
                query.addBindValue(set_id);
                query.addBindValue(++num);
                query.addBindValue(pair.first);
                query.addBindValue(pair.second);
                query.exec();
         });
        return true;
    }
    /** \brief Виртуальная функция явной проверки корректности данных. */
    virtual bool                do_check()
    {
        QRegExp re( re_string().c_str() );
        if ( -1 == re.indexIn(inherited::code().join(""))) return false;

        title_ = re.cap(1);
        return check_values(re.cap(2));
    }
};
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _V_BYTE_CODE_H_ */
