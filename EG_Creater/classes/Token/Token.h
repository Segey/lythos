/**
    \file   Token.h
    \brief  Файл Token.h

    \author S.Panin
    \date   Created on 11 Декабрь 2010 г., 22:26
 */
//------------------------------------------------------------------------------
#ifndef _V_TOKEN_H_
#define	_V_TOKEN_H_
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {
    /** \namespace go::token  */
    namespace token {
/** \struct base */
struct base
{
    static QString                  first()                                     { return "<!---"; }
    static QString                  last()                                      { return "-->"; }
};
/** \struct base */
struct byte_code
{
    static QString                  set()                                       { return "\\set"; }
    static QString                  file()                                      { return "\\file"; }
    static QString                  value()                                     { return "\\value"; }
};
//------------------------------------------------------------------------------
    } // end namespace token
//------------------------------------------------------------------------------
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _V_TOKEN_H_ */

