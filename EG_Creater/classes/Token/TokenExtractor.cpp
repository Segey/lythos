/* 
 * File:   TokenExtractor.cpp
 * Author: S.Panin
 * 
 * Created on 8 Декабрь 2010 г., 0:10
 */
//------------------------------------------------------------------------------
#include <algorithm>
#include <iostream>
#include <QRegExp>
#include <boost/bind.hpp>
#include "Token.h"
#include "../../../Go/execption.h"
#include "TokenExtractor.h"
//------------------------------------------------------------------------------
using namespace go;
using namespace go::token;
//------------------------------------------------------------------------------
void TokenExtractor::create_factories()
{
        factories_[byte_code::file()]   = boost::bind(boost::factory<go::ByteCodeFile*>(),file_);
        factories_[byte_code::set()]    = boost::factory<go::ByteCodeSet*>();
        factories_[""]                  = boost::factory<go::ByteCodeNone*>();
}
//------------------------------------------------------------------------------
bool TokenExtractor::extract()
{
     QFile file(file_);
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) throw go::file_not_found(std::string(" - File \'") + file_.toStdString() + "\' not found.");
     create_factories();


     QString line;
     QStringList list;

     while (!file.atEnd())
     {
        line =file.readLine();
        int first = line.indexOf(base::first());
        int second = line.indexOf(base::last());

        if (-1 != first) begin(first, second, list, line);
        else if (-1 != second) end(list, line);
        else if (!list.empty()) list.push_back(line);
     }

     if(!list.empty()) throw_bad_data();
     file.close();
     return true;
}
//------------------------------------------------------------------------------
void   TokenExtractor::throw_bad_data() const
{
        throw go::bad_data(std::string(" - File \'") + file_.toStdString() + "\' incorrect. Data is wrong");
}
//------------------------------------------------------------------------------
void TokenExtractor::begin(int first, int last,  QStringList& list, const QString& line)
{
        list << line;
        if ( first < last)  add_token(list);
}
//------------------------------------------------------------------------------
void TokenExtractor::end(QStringList& list, const QString& line)
{
        if (list.empty()) return;
        list << line;
        add_token(list);
}
//------------------------------------------------------------------------------
void TokenExtractor::add_token(QStringList& list)
{
        try
        {
                class_byte_code* code = factories_[get_code(list)]();
                code->set_code(list);
                if(code->check()) tokens_.push_back(code);
        } 
        catch(boost::bad_function_call& ) {}

        list.clear();
}
//------------------------------------------------------------------------------
QString TokenExtractor::get_code(QStringList& list) const
{
        QRegExp re("<!--- *(.\\w+).*-->");
        return  -1 != re.indexIn(list.join("")) ? re.cap(1) : QString();
}
//------------------------------------------------------------------------------
bool TokenExtractor::test()
{
        using namespace go::consts::test;

        TokenExtractor tok(eg_creater::one_line_bracket());
        Q_ASSERT( tok.extract() );

        Q_ASSERT( tok.tokens().size() == 8 );
        Q_ASSERT( typeid(tok.tokens().front()) == typeid(go::ByteCodeFile) );
        Q_ASSERT( tok.tokens().front().check() );
        Q_ASSERT( typeid(tok.tokens().at(1)) == typeid(go::ByteCodeSet) );
        Q_ASSERT( typeid(tok.tokens().at(2)) == typeid(go::ByteCodeSet) );
        Q_ASSERT( typeid(tok.tokens().at(3)) == typeid(go::ByteCodeFile) );
        Q_ASSERT( typeid(tok.tokens().at(4)) == typeid(go::ByteCodeFile) );
        Q_ASSERT( typeid(tok.tokens().at(5)) == typeid(go::ByteCodeFile) );
        Q_ASSERT( typeid(tok.tokens().at(6)) == typeid(go::ByteCodeSet) );
        Q_ASSERT( typeid(tok.tokens().at(7)) == typeid(go::ByteCodeSet) );
        tokens().clear();

        try {
                TokenExtractor tok2(eg_creater::no_end());
                tok2.extract();
                Q_ASSERT_X(0, "Way", "No way");
        } catch(go::bad_data& e) {}

        TokenExtractor tok3(eg_creater::set_error());
        Q_ASSERT( tok3.extract() );
        Q_ASSERT( tok3.tokens().size() == 1 );

        TokenExtractor tok4(eg_creater::set());
        Q_ASSERT( tok4.extract() );
        Q_ASSERT( tok4.tokens().size() == 2 );
        Q_ASSERT( typeid(tok4.tokens().front()) == typeid(go::ByteCodeFile) );
        Q_ASSERT( typeid(tok4.tokens().at(1)) == typeid(go::ByteCodeSet) );

        return true;
}
//------------------------------------------------------------------------------
