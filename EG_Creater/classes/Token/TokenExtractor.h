/**
    \file   TokenExtractor.h
    \brief  Файл TokenExtractor.h

    \author dix75
    \date   Created on 8 Декабрь 2010 г., 0:10
 */
//------------------------------------------------------------------------------
#ifndef _TOKEN_EXTRACTOR_H_
#define	_TOKEN_EXTRACTOR_H_
//------------------------------------------------------------------------------
#include <vector>
#include <boost/functional/factory.hpp>
#include <boost/functional/value_factory.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include "ByteCode.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go
{
/**
 \class TokenExtractor
 */
class TokenExtractor
{
    typedef                     TokenExtractor                                  class_name;
    typedef                     go::ByteCode                                    class_byte_code;
    typedef                     boost::function < class_byte_code*() >          class_factory;

public:
    typedef                     boost::ptr_vector<class_byte_code>              class_tokens;

private:
    QString                                                                     file_;
    class_tokens                                                                tokens_;
    std::map<QString, class_factory>                                            factories_;


    void                        throw_bad_data() const;
    void                        create_factories();
    void                        begin(int first, int last,  QStringList& list, const QString& line);
    void                        end(QStringList& vec, const QString& line);
    void                        add_token(QStringList& list);
    QString                     get_code(QStringList& list) const;
public:
    /** 
     \brief Конструктор.
     \param file - Имя файла добавляемого в Базу данных.
     */
    explicit                    TokenExtractor(const QString& file) : file_(file) {}
    /**
     \brief Оснавная функция извлечения токенов из файла Справочника.
     \result bool - Успешность данной операции.
    */
    bool                        extract();
    /** \brief Функция возвращает ссылку на контейнер со всеми найдеными токенами. */
    class_tokens&               tokens()                                        { return tokens_;}
    /** \brief Тестирование */
    bool test();
};
//------------------------------------------------------------------------------
} /* end namespace go  */
//------------------------------------------------------------------------------
#endif	/* _TOKEN_EXTRACTOR_H_ */
