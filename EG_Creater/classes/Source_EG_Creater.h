/**
    \file   Source_EG_Creater.h
    \brief  Файл c сырцами Справочника упражнений Source_EG_Creater.h

    \author S.Panin
    \date   Created on 20 Декабрь 2010 г., 1:45
 */
//------------------------------------------------------------------------------
#ifndef _V_SOURCE_EG_CREATER_H_
#define	_V_SOURCE_EG_CREATER_H_
//------------------------------------------------------------------------------
#include "../../Go/const.h"
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

    /** \namespace go::db  */
    namespace db {

    /** \namespace go::db::source  */
    namespace source {

/** \brief Скрипт для создания базы Справочника упражнений. */
class EG_Creater
{
    typedef                         EG_Creater                                  class_name;

    static QString files()
    {
        return "CREATE TABLE files ("
        "file_id    INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
        "path       TEXT NOT NULL UNIQUE, "
        "date       DATATIME NOT NULL, "
        "title      TEXT NOT NULL, "
        "content    CLOB NOT NULL "
        ");";
    }

    static QString sets()
    {
        return "CREATE TABLE sets ("
        "set_id     INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
        "file_id    INTEGER NOT NULL, "
        "title      TEXT NOT NULL, "
        "FOREIGN KEY(file_id) REFERENCES files(file_id)"
        ");";
    }

    static QString sets_stack()
    {
        return "CREATE TABLE sets_stack ("
        "set_id     INTEGER NOT NULL, "
        "num        INTEGER NOT NULL, "
        "weight     REAL NOT NULL, "
        "repetition REAL NOT NULL, "
        "FOREIGN KEY(set_id) REFERENCES sets(set_id)"
        ");";
    }

    static QString database_info()
    {
        return "CREATE TABLE database_info ("
        "version    REAL NOT NULL UNIQUE, "
        "date       DATATIME NOT NULL UNIQUE, "
        "title      TEXT NOT NULL UNIQUE, "
        "content    CLOB NOT NULL UNIQUE"
        ");";
    }

public:
    /**
     \brief Статическая функция для получения строк скриптов.
     \result QStringList - список строк скриптов для создания таблиц базы данных  database().
     */
    static QStringList                  source()
    {
        QStringList list;
        list.push_back(class_name::files());
        list.push_back(class_name::sets());
        list.push_back(class_name::sets_stack());
        list.push_back(class_name::database_info());
        return list;
    }
    /** \brief Последняя версия Справочника упражнений. */
    static double                       version()                               { return consts::module::eg::db_version(); }
    /** \brief Имя Справочника упражнений. */
    static QString                      database()                              { return consts::module::eg::db_name(); }
};
//------------------------------------------------------------------------------
        } // end namespace go::db::source
    } // end namespace go::db
} // end namespace go
//------------------------------------------------------------------------------
#endif	/* _V_SOURCE_EG_CREATER_H_ */

