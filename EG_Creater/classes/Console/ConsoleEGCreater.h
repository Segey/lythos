/**
    \file   ConsoleEGCreater.h
    \brief  Файл консольной работы модуля Expercises Guide Creater.

    \author dix75
    \date   Created on 24 Ноябрь 2010 г., 1:24
 */
//------------------------------------------------------------------------------
#ifndef _CONSOLE_EG_GREATOR_H_
#define	_CONSOLE_EG_GREATOR_H_
//------------------------------------------------------------------------------
#include "../../../Go/const.h"
#include "../../../Go/Algorithm/path.h"
#include <boost/program_options.hpp>
#include <boost/compressed_pair.hpp>
//------------------------------------------------------------------------------
/** \namespace go  */
namespace go {

/**
 \class ConsoleEG
 */
class ConsoleEGCreator
{
    typedef                 ConsoleEGCreator                                    class_name;
    typedef                 boost::compressed_pair<std::string, std::string>    pair;

    std::string                                                                 file_;
    std::string                                                                 path_;
    std::string                                                                 conffile_;

    static pair             key_file()                                          { return pair("file","Place the output into <file>.\n(default is exercises.eg)"); }
    static pair             key_path()                                          { return pair("path","Use <path> as root directory for files.\n(default is /exercises)"); }
    static pair             key_conffile()                                      { return pair("conffile","Set configuration file. Empty string means no conf file.\n(default is /exercises/exercises.conf)"); }

protected:
    static QString module_name_full()                                           { return consts::module::eg_creater::name_full(); }
    static QString module_name_console()                                        { return consts::module::eg_creater::name_console(); }

     /**
     \brief Конструктор.
     \note Задаются значения по умолчанию: <br />
      file - exercises.eg (Выходной файл базы данных). <br />
      path - /exercises (Путь к папке с файлами). <br />
      conffile - /exercises/exercises.conf (Конфигурационный файл) . <br />
     */
    explicit ConsoleEGCreator() :   file_(go::file::absolute(std::string("exercises.") + go::consts::exp::eg().toStdString())) ,
                                    path_(go::path::absolute("exercises")),
                                    conffile_(go::file::absolute("exercises/exercises.conf"))  {}
    /**
     \brief Вывод краткой справки о использовании программы.
     \result Первая строка справки --help
     */
    static std::string usage()
    {
        return std::string("Usage: ") + module_name_console().toStdString()  +
               " [options] [--" + key_file().first() +"=FILE] [--" +
                key_path().first() +"=PATH] [--" +
                key_conffile().first() + "=CONFFILE]\n";}

     /**
     \brief Вывод развёрнутой краткой справки о использовании программы.
     \result Вывод --usage
     */
    static std::string usage_full()                                             { return class_name::usage();}
    
    /** 
     \brief Вывод дополнительной справки о работе данного модуля.
     \result Дополнительный модуль справки --help
     */
    static boost::program_options::options_description help()
    {
        boost::program_options::options_description desc("Configuration");

        desc.add_options()
                (key_file().first().c_str(),        boost::program_options::value<std::string>(), key_file().second().c_str())
                (key_path().first().c_str(),        boost::program_options::value<std::string>(), key_path().second().c_str())
                (key_conffile().first().c_str(),    boost::program_options::value<std::string>(), key_conffile().second().c_str())
                ;
        return desc;
    }

     /**
     \brief Вывод дополнительной справки о работе данного модуля.
     \result Дополнительный модуль справки --help
     */
    bool    execute(const boost::program_options::variables_map& vm)
    {
        if (vm.count(key_file().first().c_str())) file_ = vm[key_file().first().c_str()].as<std::string>();
        if (vm.count(key_path().first().c_str())) path_ = vm[key_path().first().c_str()].as<std::string>();
        if (vm.count(key_conffile().first().c_str())) path_ = vm[key_conffile().first().c_str()].as<std::string>();
        return true;
    }
    
public:
    /** \brief Файл создаваемой библиотеки. */
    QString                 file() const                                        { return file_.c_str(); }
    /** \brief Путь к директории с файлами для библиотеки. */
    QString                 path() const                                        { return path_.c_str(); }
    /** \brief Файл настроек Базы данных. */
    QString                 conffile() const                                    { return conffile_.c_str(); }
    /** \brief модульный тест. */
    bool test()
    {
        class Creator : public ConsoleEGCreator   { } creator;

        Q_ASSERT(creator.file() == QDir().absoluteFilePath("exercises.eg"));
        Q_ASSERT(creator.path() == QDir().absoluteFilePath("exercises"));
        Q_ASSERT(creator.conffile() == QDir().absoluteFilePath("exercises/exercises.conf"));

        std::stringstream s;
        s << help() <<std::ends;

        std::string str = s.str();
        Q_ASSERT(str.find("Configuration") != std::string::npos);
        Q_ASSERT(str.find("--file") != std::string::npos);
        Q_ASSERT(str.find("--path") != std::string::npos);
        s.clear();

        const int argc=4;
        char *argv [] = {(char*)"iii", (char*)"--file", (char*)"cool_file.sc", (char*)"--path=777"};

        variables_map vm;
        store(parse_command_line(argc, argv, class_name::help()), vm);
        notify(vm);

        execute(vm);
        Q_ASSERT(file() == "cool_file.sc");
        Q_ASSERT(path() == "777");

        Q_ASSERT(usage()        == "Usage: egc [options] [--file=FILE] [--path=PATH] [--conffile=CONFFILE]\n");
        Q_ASSERT(usage_full()   == "Usage: egc [options] [--file=FILE] [--path=PATH] [--conffile=CONFFILE]\n");
        return true;
    }
};
}
//------------------------------------------------------------------------------
#endif	/* _CONSOLE_EG_GREATOR_H_ */
