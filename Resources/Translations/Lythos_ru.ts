<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU" sourcelanguage="en_GB">
<context>
    <name>Dialog</name>
    <message>
        <source>Personal info</source>
        <translation type="obsolete">Личная информация</translation>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <source>English</source>
        <translation type="obsolete">Русский</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Choose the database file name</source>
        <translation type="obsolete">Выберете имя базы данных</translation>
    </message>
    <message>
        <source>Database &apos;%1&apos; is successfully created!</source>
        <translation type="obsolete">База данных &apos;%1&apos; успешно создана!</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Открыть</translation>
    </message>
    <message>
        <source>%1 could not open &apos;%2&apos; because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn&apos;t correctly decoded).</source>
        <translation type="obsolete">Программа &quot;%1&quot; не может открыть файл &apos;%2&apos;.
		Файл поврежден или имеет неизвестный формат.</translation>
    </message>
    <message>
        <source>Database &apos;%1&apos; is successfully opened!</source>
        <translation type="obsolete">База данных &apos;%1&apos; успешно открыта!</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="obsolete">Сохранить как</translation>
    </message>
    <message>
        <source>Error while trying to save database file &apos;%1&apos; !</source>
        <translation type="obsolete">Невозможно выполнить текущую операцию.
		Сохранение базы данных ’%1’ привело к ошибке!
		Убедитесь, что база данных открыта и не используется сторонними программами!</translation>
    </message>
    <message>
        <source>Database &apos;%1&apos; is successfully saved!</source>
        <translation type="obsolete">База данных &apos;%1&apos; успешно сохранена!</translation>
    </message>
    <message>
        <source>%1 could not open &apos;%2&apos; because it Is either not a supported file type or because the file has been damaged!</source>
        <translation type="obsolete">Программа &quot;%1&quot; не может открыть файл &apos;%2&apos;.
		Файл поврежден или имеет неизвестный формат!</translation>
    </message>
    <message>
        <source>Error while trying to create database file &apos;%1&apos; !</source>
        <translation type="obsolete">Невозможно выполнить текущую операцию. Создание базы данных ’%1’ привело к ошибке.
		Попробуйте создать файл с отличным от текущего именем и вновь выполнить операцию сохранения!</translation>
    </message>
    <message>
        <source>Sign In</source>
        <translation type="obsolete">Войти</translation>
    </message>
    <message>
        <source>User Name and password</source>
        <translation type="obsolete">Имя пользователя и пароль</translation>
    </message>
    <message>
        <source>&amp;Select User:</source>
        <translation type="obsolete">&amp;Имя пользователя:</translation>
    </message>
    <message>
        <source>&amp;New User</source>
        <translation type="obsolete">&amp;Новый</translation>
    </message>
    <message>
        <source>&amp;Continue</source>
        <translation type="obsolete">П&amp;родолжить</translation>
    </message>
    <message>
        <source>&amp;Exit</source>
        <translation type="obsolete">В&amp;ыход</translation>
    </message>
    <message>
        <source>Save changes to the Personal Office document before quitting?

If you don&apos;t save, your changes will be lost.</source>
        <translation type="obsolete">Сохранить внесённые изменения?.</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">О&amp;тмена</translation>
    </message>
    <message>
        <source>&amp;New Record...</source>
        <translation type="obsolete">Созд&amp;ать рекорд...</translation>
    </message>
    <message>
        <source>&amp;Open Record...</source>
        <translation type="obsolete">&amp;Открыть рекорд...</translation>
    </message>
    <message>
        <source>&amp;Delete Record</source>
        <translation type="obsolete">&amp;Удалить рекорд</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="obsolete">Созд&amp;ать...</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation type="obsolete">&amp;Открыть...</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="obsolete">&amp;Удалить</translation>
    </message>
    <message>
        <source>Copyright 2005-2009 Lythos Software Inc. All rights reserved.</source>
        <translation type="obsolete">Copyright 2005-2009 Lythos Software Inc. Все права защищены.</translation>
    </message>
    <message>
        <source>Edit in new window...</source>
        <translation type="obsolete">Редактировать в новом окне...</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="obsolete">&amp;Пароль:</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Сохранить</translation>
    </message>
    <message>
        <source>Do you want to save the changes to this document before closing?

If you don&apos;t save, your changes will be lost.</source>
        <translation type="obsolete">Сохранить внесённые изменения?.</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation type="obsolete">Персональная информация</translation>
    </message>
</context>
<context>
    <name>inherited</name>
    <message>
        <source>Cannot write file %1:
%2.</source>
        <translation type="obsolete">Невозможно выполнить текущую операцию!
		Создание файла ‘%1’ завершилось неудачей:
		%2.</translation>
    </message>
    <message>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation type="obsolete">Невозможно выполнить текущую операцию!
		Создание файла ‘%1’ завершилось неудачей!
		Попробуйте создать файл с отличным от текущего именем и вновь выполнить операцию сохранения.</translation>
    </message>
    <message>
        <source>The Exercise &apos;%1&apos; already is in a database!</source>
        <translation type="obsolete">Упражнение с именем &apos;%1&apos;, уже находится в базе данных!</translation>
    </message>
</context>
<context>
    <name>vAboutDialog</name>
    <message>
        <source>This product is licensed to:</source>
        <translation type="obsolete">Права на использование этой версии принадлежат:</translation>
    </message>
    <message>
        <source>Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.</source>
        <translation type="obsolete">ВНИМАНИЕ: Эта компьютерная программа защищена законом «Об авторском праве и смежных правах», а также  международными соглашениями. Несанкционированное воспроизводство и распространение программы или каких-либо ее компонентов влечет к гражданской или уголовной ответственности, и преследуется по закону.</translation>
    </message>
    <message>
        <source>Copyright (C) 2005-2009 Lythos Software Inc. All rights reserved.</source>
        <translation type="obsolete">Copyright (C) 2005-2009 Lythos Software Inc. Все права защищены.</translation>
    </message>
</context>
<context>
    <name>vAction</name>
    <message>
        <source>&amp;About...</source>
        <translation type="obsolete">&amp;О программе...</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="obsolete">Созд&amp;ать</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Создать</translation>
    </message>
    <message>
        <source>Create &amp;New Database...</source>
        <translation type="obsolete">Созд&amp;ать базу данных...</translation>
    </message>
    <message>
        <source>Create New Database</source>
        <translation type="obsolete">Создать базу данных</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation type="obsolete">&amp;Открыть...</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Открыть</translation>
    </message>
    <message>
        <source>Open Database</source>
        <translation type="obsolete">Открыть базу данных</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Сохранить</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation type="obsolete">Сохранить &amp;как...</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Сохранить как</translation>
    </message>
    <message>
        <source>Clos&amp;e</source>
        <translation type="obsolete">Закр&amp;ыть</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Вырезать</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="obsolete">Копи&amp;ровать</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Копировать</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="obsolete">Встави&amp;ть</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Вставить</translation>
    </message>
    <message>
        <source>C&amp;lear</source>
        <translation type="obsolete">Удалит&amp;ь</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Удалить</translation>
    </message>
    <message>
        <source>Cl&amp;ear All</source>
        <translation type="obsolete">Удали&amp;т&amp;ь всё</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">Удалить всё</translation>
    </message>
    <message>
        <source>Completely clears the document contents</source>
        <translation type="obsolete">Полностью и безвозвратно удаляет содержимое документа</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Печать</translation>
    </message>
    <message>
        <source>Print Preview...</source>
        <translation type="obsolete">Предварительный просмотр...</translation>
    </message>
    <message>
        <source>Print Preview</source>
        <translation type="obsolete">Предварительный просмотр</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="obsolete">Отменить</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="obsolete">Вернуть</translation>
    </message>
    <message>
        <source>&amp;Left</source>
        <translation type="obsolete">по &amp;Левому краю</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="obsolete">по Левому краю</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">по Центру</translation>
    </message>
    <message>
        <source>&amp;Right</source>
        <translation type="obsolete">по Правому кра&amp;ю</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">по Правому краю</translation>
    </message>
    <message>
        <source>&amp;Justify</source>
        <translation type="obsolete">по &amp;Ширине</translation>
    </message>
    <message>
        <source>Justify</source>
        <translation type="obsolete">по Ширине</translation>
    </message>
    <message>
        <source>&amp;Bold</source>
        <translation type="obsolete">Полу&amp;жирный</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="obsolete">Полужирный</translation>
    </message>
    <message>
        <source>&amp;Italic</source>
        <translation type="obsolete">К&amp;урсив</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="obsolete">Курсив</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation type="obsolete">По&amp;дчёркнутый</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="obsolete">Подчёркнутый</translation>
    </message>
    <message>
        <source>&amp;Color...</source>
        <translation type="obsolete">Цв&amp;е&amp;т...</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Цвет</translation>
    </message>
    <message>
        <source>&amp;Help Contents...</source>
        <translation type="obsolete">&amp;Содержание &amp;справки...</translation>
    </message>
    <message>
        <source>Help Contents</source>
        <translation type="obsolete">Содержание справки</translation>
    </message>
    <message>
        <source>&amp;Registration...</source>
        <translation type="obsolete">Регистрац&amp;ия...</translation>
    </message>
    <message>
        <source>Registration</source>
        <translation type="obsolete">Регистрация</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation type="obsolete">Домашняя страница</translation>
    </message>
    <message>
        <source>Cut the current selection&apos;s contents to the clipboard.</source>
        <translation type="obsolete">Помещает выделенный фрагмент текста в буфер обмена.</translation>
    </message>
    <message>
        <source>Copy the current selection&apos;s contents to the clipboard.</source>
        <translation type="obsolete">Копирует  выделенный фрагмент текста в буфер обмена.</translation>
    </message>
    <message>
        <source>Paste the clipboard&apos;s contents into the current selection.</source>
        <translation type="obsolete">Вставляет содержимое буфера обмена в текущую позицию курсора.</translation>
    </message>
    <message>
        <source>C&amp;ut</source>
        <translation type="obsolete">&amp;Вырезать</translation>
    </message>
    <message>
        <source>U&amp;ndo</source>
        <translation type="obsolete">От&amp;менить</translation>
    </message>
    <message>
        <source>Re&amp;do</source>
        <translation type="obsolete">Вер&amp;нуть</translation>
    </message>
    <message>
        <source>Cen&amp;ter</source>
        <translation type="obsolete">по &amp;Центру</translation>
    </message>
    <message>
        <source>Ho&amp;me Page...</source>
        <translation type="obsolete">Домашн&amp;яя страница...</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="obsolete">&amp;Печать...</translation>
    </message>
    <message>
        <source>&amp;New a image</source>
        <translation type="obsolete">&amp;Добавить изображение</translation>
    </message>
    <message>
        <source>New a image</source>
        <translation type="obsolete">Добавить изображение</translation>
    </message>
    <message>
        <source>Delete a image</source>
        <translation type="obsolete">Удалить изображение</translation>
    </message>
    <message>
        <source>&amp;Delete a image</source>
        <translation type="obsolete">&amp;Удалить изображение</translation>
    </message>
</context>
<context>
    <name>vAddRecordHistory</name>
    <message>
        <source>Your data are successfully updated!</source>
        <translation type="obsolete">Ваши данные успешно сохранены!</translation>
    </message>
    <message>
        <source> %1%</source>
        <translation type="obsolete"> %1%</translation>
    </message>
    <message>
        <source> %1 day(days)</source>
        <translation type="obsolete"> %1 дня(дней)</translation>
    </message>
    <message>
        <source>Add/Edit a Personal Record</source>
        <translation type="obsolete">Добавить/Изменить рекорд</translation>
    </message>
    <message>
        <source>&amp;Exercise</source>
        <translation type="obsolete">&amp;Упражнение</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="obsolete">&amp;Открыть</translation>
    </message>
    <message>
        <source>Period left:</source>
        <translation type="obsolete">Осталось:</translation>
    </message>
    <message>
        <source>Progress:</source>
        <translation type="obsolete">Прогресс:</translation>
    </message>
    <message>
        <source>Select Exercise</source>
        <translation type="obsolete">Выбор упражнения</translation>
    </message>
    <message>
        <source>My Current Result</source>
        <translation type="obsolete">Текущий результат</translation>
    </message>
    <message>
        <source>My Desired Result</source>
        <translation type="obsolete">Желаемый результат</translation>
    </message>
</context>
<context>
    <name>vAlignSection</name>
    <message>
        <source>Align</source>
        <translation type="obsolete">Выравнивание</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Правка</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Open File...</source>
        <translation type="obsolete">Открыть файл...</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation type="obsolete">Сохранить файл...</translation>
    </message>
    <message>
        <source>Font</source>
        <translation type="obsolete">Шрифт</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Печать</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Стиль</translation>
    </message>
    <message>
        <source>Undo/Redo</source>
        <translation type="obsolete">Отменить/Вернуть</translation>
    </message>
    <message>
        <source>Print Document</source>
        <translation type="obsolete">Печатать</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Помощь</translation>
    </message>
</context>
<context>
    <name>vBulletSection</name>
    <message>
        <source>Standard</source>
        <translation type="obsolete">Стандартный список</translation>
    </message>
    <message>
        <source>Bullet List (Disc)</source>
        <translation type="obsolete">Маркированный список (Диск)</translation>
    </message>
    <message>
        <source>Bullet List (Circle)</source>
        <translation type="obsolete">Маркированный список (Круг)</translation>
    </message>
    <message>
        <source>Bullet List (Square)</source>
        <translation type="obsolete">Маркированный список (Квадрат)</translation>
    </message>
    <message>
        <source>Ordered List (Decimal)</source>
        <translation type="obsolete">Нумерованный список (десятичный)</translation>
    </message>
    <message>
        <source>Ordered List (Alpha lower)</source>
        <translation type="obsolete">Нумерованный список (строчный алфавит)</translation>
    </message>
    <message>
        <source>Ordered List (Alpha upper)</source>
        <translation type="obsolete">Нумерованный список (ПРОПИСНОЙ АЛФАВИТ)</translation>
    </message>
    <message>
        <source>List</source>
        <translation type="obsolete">Список</translation>
    </message>
</context>
<context>
    <name>vClientLDDialog</name>
    <message>
        <source>Personal office [*]</source>
        <translation type="obsolete">Кабинет[*]</translation>
    </message>
    <message>
        <source>Main</source>
        <translation type="obsolete">Главная</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="obsolete">&amp;Сохранить</translation>
    </message>
    <message>
        <source>Your data are successfully updated!</source>
        <translation type="obsolete">Ваши данные успешно обновлены!</translation>
    </message>
</context>
<context>
    <name>vFilterDialog</name>
    <message>
        <source>Image Files %1Microsoft Windows Bitmap BMP%2Graphics Interchange Format GIF %3Portable Network Graphics PNG %4JPEG %5XPixMap XPM %6PPM %7</source>
        <translation type="obsolete">Файлы изображений %1Microsoft Windows Bitmap BMP%2Graphics Interchange Format GIF %3Portable Network Graphics PNG %4JPEG %5XPixMap XPM %6PPM %7</translation>
    </message>
    <message>
        <source>Image Files %1Microsoft Windows Bitmap BMP%2Portable Network Graphics PNG %3JPEG %4</source>
        <translation type="obsolete">Файлы изображений %1Microsoft Windows Bitmap BMP%2Portable Network Graphics PNG %3JPEG %4</translation>
    </message>
    <message>
        <source>All supported types %1Adobe PDF Files %2ODF files %3Hyper Text Markup Language file %4Text Files %5</source>
        <translation type="obsolete">Все поддержанные типы файлов %1Adobe PDF Files %2ODF files %3Hyper Text Markup Language file %4Text Files %5</translation>
    </message>
    <message>
        <source>All supported types %1ODF files %2Hyper Text Markup Language file %3Text Files %4</source>
        <translation type="obsolete">Все поддержанные типы файлов %1ODF files %2Hyper Text Markup Language file %3Text Files %4</translation>
    </message>
    <message>
        <source>Database file %1All files %2</source>
        <translation type="obsolete">База данных %1Все файлы %2</translation>
    </message>
</context>
<context>
    <name>vImageBlock</name>
    <message>
        <source>New name of the image:</source>
        <translation type="obsolete">Введите имя изображения:</translation>
    </message>
</context>
<context>
    <name>vImagePanel</name>
    <message>
        <source>Open</source>
        <translation type="obsolete">Открыть</translation>
    </message>
    <message>
        <source>Are you sure you want to send the image to the Recycle Bin?!</source>
        <translation type="obsolete">Вы действительно хотите удалить изображение?!</translation>
    </message>
</context>
<context>
    <name>vIntroductionDialog</name>
    <message>
        <source>

Sincerely,
     Panin S.</source>
        <translation type="obsolete">
		
		C уважением,    С. Панин.</translation>
    </message>
    <message>
        <source>Print Introduction</source>
        <translation type="obsolete">Напечатать Введение</translation>
    </message>
    <message>
        <source> [trial version]</source>
        <translation type="obsolete">[пробная версия]</translation>
    </message>
    <message>
        <source>&amp;How To Buy %1</source>
        <translation type="obsolete">&amp;Как приобрести %1</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="obsolete">&amp;Печать...</translation>
    </message>
    <message>
        <source>&amp;Thanks</source>
        <translation type="obsolete">&amp;Хорошо</translation>
    </message>
    <message>
        <source>Dear Madam,
Dear Sir.

   Thank you very much for evaluating %1. I am sure you will soon find %1 to be an assistant you don&apos;t want to be without, when working with trainings.

   %1 tracks everything related to bodybuilding and fitness: track exercises, meals, measurements, record results, general health and more! You can quickly and easily work with your product information directly in %1. Easily track and visualize progress, know if your training workouts are effective, and reach goals faster. Includes a  journal for recording information and four major tools to view and analyze progress.

   %1 is highly customizable. Whether you&apos;re a bodybuilder, a cyclist, training for a marathon, or just want to monitor and control your body weight, %1 has everything you need.</source>
        <translation type="obsolete">      Уважаемые дамы и господа!
Большое спасибо Вам за проявленный интерес к программе ‘%1’ нашей компании. Я убежден в том, что в скором времени программа ‘%1’ станет прекрасным помощником для Вас в систематизации тренировочных данных различной направленности.

   С помощью программы ‘%1’ Вы сможете вести дневник тренировок, изучить и скорректировать Вашу технику выполнения упражнений, отслеживать динамику роста результатов (рекордов), вносить антропометрические данные, а также следить и, что более важно, контролировать общее состояние здоровья как глобально, на уровне системы, так и локально, т.е. контролировать состояние здоровья или его некоторых составляющих (артериальное давление, температура тела, частота пульса и др.) в наиболее важных контрольных точках (до и после проведённой тренировки) и многое другое.

   Основной навигационный календарь программы ‘%1’, а также несколько его собратьев позволят Вам буквально в несколько щелчков мыши манипулировать всей совокупностью тренировочных данных (добавлять, редактировать, удалять, перемещать тренировки, а также производить другие операции над данными).

   Занимаетесь ли Вы бодибилдингом, фитнесом, пауэрлифтингом или бегом на короткие и длинные дистанции, возможно, Вы просто следите за собственным весом, в любом случае стандартного набора технических средств программы ‘%1’ будет достаточно для удовлетворения практических всех Ваших требований по систематизации тренировочных данных.

Помните, что программа ‘%1’ не является бесплатной. Чтобы использовать в своей работе все её возможности, Вам необходимо приобрести лицензию.</translation>
    </message>
</context>
<context>
    <name>vLD</name>
    <message>
        <source>Personal info</source>
        <translation type="obsolete">Персональная информация</translation>
    </message>
    <message>
        <source>- Enter your personal data (information) including: Personal Info (Name, Password, Date of Birth, Sex, Height), and Contact Info(Zip code, Country,  State/Region, City/Town, Address (Street, Apartment), E-Mail, Phone number, ICQ, Mobule) and Additional info (Doctor, Instructor).</source>
        <translation type="obsolete">- Страница персональных данных позволяет зафиксировать все необходимые данные о Вас, а именно: личную информацию (имя пользователя, пароль, дата рождения, пол, рост), контактную информацию (страна проживания, район или область, город, домашний адрес, адрес электронной почты (E-Mail), номер телефона, номер ICQ, номер мобильного телефона), а также дополнительную информацию (имя персонального доктора, имя персонального тренера или партнёра по занятиям).</translation>
    </message>
    <message>
        <source>PhotoGallery</source>
        <translation type="obsolete">Фотогалерея</translation>
    </message>
    <message>
        <source>Record History</source>
        <translation type="obsolete">Рекорды</translation>
    </message>
    <message>
        <source>- The page &quot;Photogallery&quot; allows you  to keep the most important photos of your successes and achievements step by step, day by day.  Regular supplements to the Photogallery with new photos will allow you to timely make necessary correcting amendments to the working program of trainings with the purpose of advancing results.</source>
        <translation type="obsolete">-  Страница “Фотогалерея” позволяет Вам шаг за шагом, день за днем  сохранить наиболее важные фотографии Ваших успехов и достижений.  Регулярное пополнение Фотогалереи новыми фотографиями позволит Вам своевременно вносить необходимые корректирующие поправки в рабочую программу тренировок с целью усиления результатов.</translation>
    </message>
    <message>
        <source>- The page &quot;Records&quot; will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way.</source>
        <translation type="obsolete">-  Страница “Рекорды” позволит Вам систематизировать Ваши спортивные рекорды, а именно: внести основные данные о текущих рекордах, сгруппированных по имени упражнения; запланировать желаемый результат в практикуемом (тренируемом) упражнении.
Систематическое добавление персональных рекордов и тщательный анализ динамики их роста позволит Вам ставить перед собой и достигать намеченных целей наиболее продуктивным образом.</translation>
    </message>
</context>
<context>
    <name>vLDRecordHistoryDialog</name>
    <message>
        <source> %1%</source>
        <translation type="obsolete"> %1%</translation>
    </message>
</context>
<context>
    <name>vMainInstance</name>
    <message>
        <source>&amp;Continue</source>
        <translation type="obsolete">П&amp;родолжить</translation>
    </message>
</context>
<context>
    <name>vMessage</name>
    <message>
        <source>%1Try to reinstall to fix this problem.</source>
        <translation type="obsolete">%1Пожалуйста, переустановите приложение.</translation>
    </message>
</context>
<context>
    <name>vPersonalInfo</name>
    <message>
        <source>Additional info</source>
        <translation type="obsolete">Дополнительная информация</translation>
    </message>
    <message>
        <source>Contact info</source>
        <translation type="obsolete">Контактная информация</translation>
    </message>
    <message>
        <source>Personal information</source>
        <translation type="obsolete">Персональная информация</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation type="obsolete">Личная информация</translation>
    </message>
    <message>
        <source>Doc&amp;tor:</source>
        <translation type="obsolete">Док&amp;тор:</translation>
    </message>
    <message>
        <source>Instruct&amp;or:</source>
        <translation type="obsolete">Инструкт&amp;ор:</translation>
    </message>
    <message>
        <source>&amp;Country:</source>
        <translation type="obsolete">Стра&amp;на:</translation>
    </message>
    <message>
        <source>State/Re&amp;gion:</source>
        <translation type="obsolete">Область/Ра&amp;йон:</translation>
    </message>
    <message>
        <source>C&amp;ity/Town:</source>
        <translation type="obsolete">&amp;Город:</translation>
    </message>
    <message>
        <source>&amp;Address:</source>
        <translation type="obsolete">&amp;Адрес:</translation>
    </message>
    <message>
        <source>&amp;E-mail:</source>
        <translation type="obsolete">&amp;E-mail:</translation>
    </message>
    <message>
        <source>IC&amp;Q:</source>
        <translation type="obsolete">IC&amp;Q:</translation>
    </message>
    <message>
        <source>Phone n&amp;umber:</source>
        <translation type="obsolete">Теле&amp;фон:</translation>
    </message>
    <message>
        <source>&amp;Mobule:</source>
        <translation type="obsolete">&amp;Мобильный:</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation type="obsolete">Им&amp;я:</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="obsolete">&amp;Пароль:</translation>
    </message>
    <message>
        <source>&amp;Re-enter password:</source>
        <translation type="obsolete">Повторите па&amp;роль:</translation>
    </message>
    <message>
        <source>Se&amp;x:</source>
        <translation type="obsolete">По&amp;л:</translation>
    </message>
    <message>
        <source>&amp;Date of Birth:</source>
        <translation type="obsolete">Дата ро&amp;ждения:</translation>
    </message>
    <message>
        <source>&amp;Height (%1):</source>
        <translation type="obsolete">Ро&amp;ст (%1):</translation>
    </message>
    <message>
        <source>Male</source>
        <translation type="obsolete">Мужской</translation>
    </message>
    <message>
        <source>Female</source>
        <translation type="obsolete">Женский</translation>
    </message>
    <message>
        <source>The user with a name &apos;%1&apos; already exists!</source>
        <translation type="obsolete">Пользователь с именем &quot;%1&quot; уже зарегистрирован в системе!</translation>
    </message>
</context>
<context>
    <name>vPhotoGallery</name>
    <message>
        <source>PhotoGallery</source>
        <translation type="obsolete">Фотогалерея</translation>
    </message>
    <message>
        <source>Photos</source>
        <translation type="obsolete">Фотографии</translation>
    </message>
</context>
<context>
    <name>vRecordHistory</name>
    <message>
        <source>Personal Records History</source>
        <translation type="obsolete">Рекорды</translation>
    </message>
    <message>
        <source>Personal Records</source>
        <translation type="obsolete">Таблица рекордов</translation>
    </message>
</context>
<context>
    <name>vRecordHistoryPanel</name>
    <message>
        <source>Enter the Date of the Record</source>
        <translation type="obsolete">Дата рекорда</translation>
    </message>
    <message>
        <source>Enter Record</source>
        <translation type="obsolete">Рекорд</translation>
    </message>
    <message>
        <source>Enter Record Notes</source>
        <translation type="obsolete">Примечание</translation>
    </message>
    <message>
        <source>&amp;Date:</source>
        <translation type="obsolete">&amp;Дата:</translation>
    </message>
    <message>
        <source>&amp;Weight:</source>
        <translation type="obsolete">&amp;Вес:</translation>
    </message>
    <message>
        <source>&amp;Repetitions:</source>
        <translation type="obsolete">&amp;Кол-во повторений:</translation>
    </message>
</context>
<context>
    <name>vRecordHistoryTreeView</name>
    <message>
        <source>Are you sure you want to remove the selected record?!</source>
        <translation type="obsolete">Вы действительно хотите удалитьтекущий рекорд?!</translation>
    </message>
    <message>
        <source>Are you sure you want to remove all records?!</source>
        <translation type="obsolete">Вы действительно хотите удалить все существующие рекорды?!</translation>
    </message>
    <message>
        <source>Create New Record</source>
        <translation type="obsolete">Создать рекорд</translation>
    </message>
    <message>
        <source>Open Record</source>
        <translation type="obsolete">Открыть рекорд</translation>
    </message>
    <message>
        <source>Delete Current Record</source>
        <translation type="obsolete">Удалить рекорд</translation>
    </message>
    <message>
        <source>Exercise</source>
        <translation type="obsolete">Упражнение</translation>
    </message>
    <message>
        <source>Record Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
    <message>
        <source>Record Weight</source>
        <translation type="obsolete">Вес</translation>
    </message>
    <message>
        <source>Record Repetitions</source>
        <translation type="obsolete">Кол-во повторений</translation>
    </message>
    <message>
        <source>Record Notes</source>
        <translation type="obsolete">Примечание</translation>
    </message>
</context>
<context>
    <name>vRichEdit</name>
    <message>
        <source>Save changes to the document before quitting?

If you don&apos;t save, your changes will be lost.</source>
        <translation type="obsolete">Сохранить изменения в документе?.</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Файл</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="obsolete">П&amp;равка</translation>
    </message>
    <message>
        <source>F&amp;ormat</source>
        <translation type="obsolete">Ф&amp;ормат</translation>
    </message>
</context>
<context>
    <name>vSingletonLanguage</name>
    <message>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Язык</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Русский</translation>
    </message>
</context>
<context>
    <name>vtr::Action</name>
    <message>
        <source>Open File...</source>
        <translation type="obsolete">Открыть файл...</translation>
    </message>
    <message>
        <source>Print Document</source>
        <translation type="obsolete">Печатать</translation>
    </message>
</context>
<context>
    <name>vtr::BD</name>
    <message>
        <source>The database already has a record for the current date!
Enter a new date or change the existing record with data!</source>
        <translation>В базе данных уже имеется запись на текущую дату!
Введите новую дату или измените существующую запись с данными!</translation>
    </message>
    <message>
        <source>Exercise &apos;%1&apos; is already contained in the database!
Enter a new Exercise or change the existing record with data!</source>
        <translation>Упражнение &apos;%1&apos; содержится в базе данных!
Введите новое упражнение или измените существующую запись с данными!</translation>
    </message>
    <message>
        <source>New name of the image:</source>
        <translation type="obsolete">Введите имя изображения:</translation>
    </message>
</context>
<context>
    <name>vtr::Body</name>
    <message>
        <source>&amp;Growth</source>
        <translation>&amp;Рост</translation>
    </message>
    <message>
        <source>&amp;Biceps</source>
        <translation>Би&amp;цепсы</translation>
    </message>
    <message>
        <source>&amp;Abdomen</source>
        <translation>&amp;Живот</translation>
    </message>
    <message>
        <source>Ca&amp;lfes</source>
        <translation>&amp;Икроножные</translation>
    </message>
    <message>
        <source>&amp;Chest</source>
        <translation>&amp;Грудные</translation>
    </message>
    <message>
        <source>&amp;Forearms</source>
        <translation>&amp;Предплечья</translation>
    </message>
    <message>
        <source>Hi&amp;ps</source>
        <translation>&amp;Бедра</translation>
    </message>
    <message>
        <source>&amp;Neck</source>
        <translation>&amp;Шея</translation>
    </message>
    <message>
        <source>&amp;Shoulders</source>
        <translation>Дел&amp;ьтовидные</translation>
    </message>
    <message>
        <source>&amp;Thighs</source>
        <translation>&amp;Бедра</translation>
    </message>
    <message>
        <source>Wa&amp;ist</source>
        <translation>&amp;Талия</translation>
    </message>
    <message>
        <source>&amp;Weight</source>
        <translation>&amp;Вес</translation>
    </message>
    <message>
        <source>W&amp;rist</source>
        <translation>&amp;Запястье</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Вес</translation>
    </message>
    <message>
        <source>Chest</source>
        <translation>Грудные</translation>
    </message>
    <message>
        <source>Bicep right</source>
        <translation>Правый бицепс</translation>
    </message>
    <message>
        <source>Abdomen</source>
        <translation>Живот</translation>
    </message>
    <message>
        <source>Male</source>
        <translation>Мужской</translation>
    </message>
    <message>
        <source>Female</source>
        <translation>Женский</translation>
    </message>
    <message>
        <source>&amp;Growth:</source>
        <translation type="obsolete">&amp;Рост:</translation>
    </message>
    <message>
        <source>Se&amp;x:</source>
        <translation>По&amp;л:</translation>
    </message>
    <message>
        <source>&amp;Weight:</source>
        <translation type="obsolete">&amp;Вес:</translation>
    </message>
    <message>
        <source>Growth</source>
        <translation>Рост</translation>
    </message>
    <message>
        <source>Biceps</source>
        <translation>Бицепсы</translation>
    </message>
    <message>
        <source>Calves</source>
        <translation>Икроножные</translation>
    </message>
    <message>
        <source>Forearms</source>
        <translation>Предплечья</translation>
    </message>
    <message>
        <source>Hips</source>
        <translation>Бедра</translation>
    </message>
    <message>
        <source>Neck</source>
        <translation>Шея</translation>
    </message>
    <message>
        <source>Shoulders</source>
        <translation>Дельтовидные</translation>
    </message>
    <message>
        <source>Thighs</source>
        <translation>Бедра</translation>
    </message>
    <message>
        <source>Waist</source>
        <translation>Талия</translation>
    </message>
    <message>
        <source>Wrist</source>
        <translation>Запястье</translation>
    </message>
    <message>
        <source>Bicep left</source>
        <translation type="obsolete">Левый бицепс</translation>
    </message>
    <message>
        <source>Forearm left</source>
        <translation type="obsolete">Левое предплечье</translation>
    </message>
    <message>
        <source>Forearm right</source>
        <translation type="obsolete">Правое предплечье</translation>
    </message>
    <message>
        <source>Wrist left</source>
        <translation type="obsolete">Левое запястье</translation>
    </message>
    <message>
        <source>Wrist right</source>
        <translation type="obsolete">Правое запястье</translation>
    </message>
    <message>
        <source>Thigh left</source>
        <translation type="obsolete">Левое бедро</translation>
    </message>
    <message>
        <source>Thigh right</source>
        <translation type="obsolete">Правое бедро</translation>
    </message>
    <message>
        <source>Calf left</source>
        <translation type="obsolete">Левая икроножная</translation>
    </message>
    <message>
        <source>Calf right</source>
        <translation type="obsolete">Правая икроножная</translation>
    </message>
    <message>
        <source>&amp;Height (%1):</source>
        <translation>Ро&amp;ст (%1):</translation>
    </message>
    <message>
        <source>&amp;Growth (%1):</source>
        <translation>&amp;Рост (%1):</translation>
    </message>
    <message>
        <source>&amp;Weight (%1):</source>
        <translation>&amp;Вес (%1):</translation>
    </message>
    <message>
        <source>&amp;Neck (%1):</source>
        <translation>&amp;Шея (%1):</translation>
    </message>
    <message>
        <source>&amp;Shoulders (%1):</source>
        <translation>&amp;Дельтовидные (%1):</translation>
    </message>
    <message>
        <source>&amp;Chest (%1):</source>
        <translation>&amp;Грудные  (%1):</translation>
    </message>
    <message>
        <source>&amp;Biceps (%1):</source>
        <translation>&amp;Бицепсы (%1):</translation>
    </message>
    <message>
        <source>&amp;Forearms (%1):</source>
        <translation>&amp;Предплечье (%1):</translation>
    </message>
    <message>
        <source>W&amp;rist (%1):</source>
        <translation>&amp;Запястье (%1):</translation>
    </message>
    <message>
        <source>&amp;Abdomen (%1):</source>
        <translation>&amp;Живот (%1):</translation>
    </message>
    <message>
        <source>Wa&amp;ist (%1):</source>
        <translation>&amp;Талия (%1):</translation>
    </message>
    <message>
        <source>Hi&amp;ps (%1):</source>
        <translation>&amp;Бедра (%1):</translation>
    </message>
    <message>
        <source>&amp;Thigh (%1):</source>
        <translation>&amp;Бедро (%1):</translation>
    </message>
    <message>
        <source>Ca&amp;lf (%1):</source>
        <translation>&amp;Икроножная (%1):</translation>
    </message>
    <message>
        <source>&amp;Bicep left (%1):</source>
        <translation>Левый би&amp;цепс (%1):</translation>
    </message>
    <message>
        <source>&amp;Bicep right (%1):</source>
        <translation>Правый би&amp;цепс (%1):</translation>
    </message>
    <message>
        <source>&amp;Forearm left (%1):</source>
        <translation>Левое &amp;предплечье (%1):</translation>
    </message>
    <message>
        <source>&amp;Forearm right (%1):</source>
        <translation>Правое &amp;предплечье (%1):</translation>
    </message>
    <message>
        <source>W&amp;rist left (%1):</source>
        <translation>Левое &amp;Запястье (%1):</translation>
    </message>
    <message>
        <source>W&amp;rist right (%1):</source>
        <translation>Правое &amp;Запястье (%1):</translation>
    </message>
    <message>
        <source>&amp;Thigh left (%1):</source>
        <translation>Левое &amp;бедро (%1):</translation>
    </message>
    <message>
        <source>&amp;Thigh right (%1):</source>
        <translation>Правое &amp;бедро (%1):</translation>
    </message>
    <message>
        <source>Ca&amp;lf left (%1):</source>
        <translation>Левая &amp;икроножная (%1):</translation>
    </message>
    <message>
        <source>Ca&amp;lf right (%1):</source>
        <translation>Правая &amp;икроножная (%1):</translation>
    </message>
    <message>
        <source>Growth (%1):</source>
        <translation>Рост (%1):</translation>
    </message>
    <message>
        <source>Neck (%1):</source>
        <translation>Шея (%1):</translation>
    </message>
    <message>
        <source>Shoulders (%1):</source>
        <translation>Дельтовидные (%1):</translation>
    </message>
    <message>
        <source>Chest (%1):</source>
        <translation>Грудные (%1):</translation>
    </message>
    <message>
        <source>Bicep left (%1):</source>
        <translation>Левый бицепс (%1):</translation>
    </message>
    <message>
        <source>Bicep right (%1):</source>
        <translation>Правый бицепс (%1):</translation>
    </message>
    <message>
        <source>Forearm left (%1):</source>
        <translation>Левое предплечье (%1):</translation>
    </message>
    <message>
        <source>Forearm right (%1):</source>
        <translation>Правое предплечье (%1):</translation>
    </message>
    <message>
        <source>Wrist left (%1):</source>
        <translation>Левое запястье (%1):</translation>
    </message>
    <message>
        <source>Wrist right (%1):</source>
        <translation>Правое запястье (%1):</translation>
    </message>
    <message>
        <source>Abdomen (%1):</source>
        <translation>Живот (%1):</translation>
    </message>
    <message>
        <source>Waist (%1):</source>
        <translation>Талия (%1):</translation>
    </message>
    <message>
        <source>Hips (%1):</source>
        <translation>Бедра (%1):</translation>
    </message>
    <message>
        <source>Thigh left (%1):</source>
        <translation>Левое бедро (%1):</translation>
    </message>
    <message>
        <source>Thigh right (%1):</source>
        <translation>Правое бедро (%1):</translation>
    </message>
    <message>
        <source>Calf left (%1):</source>
        <translation>Левая икроножная (%1):</translation>
    </message>
    <message>
        <source>Calf right (%1):</source>
        <translation>Правая икроножная (%1):</translation>
    </message>
</context>
<context>
    <name>vtr::Bullet</name>
    <message>
        <source>Standard</source>
        <translation>Стандартный список</translation>
    </message>
    <message>
        <source>Bullet List (Disc)</source>
        <translation>Маркированный список (Диск)</translation>
    </message>
    <message>
        <source>Bullet List (Circle)</source>
        <translation>Маркированный список (Круг)</translation>
    </message>
    <message>
        <source>Bullet List (Square)</source>
        <translation>Маркированный список (Квадрат)</translation>
    </message>
    <message>
        <source>Ordered List (Decimal)</source>
        <translation>Нумерованный список (десятичный)</translation>
    </message>
    <message>
        <source>Ordered List (Alpha lower)</source>
        <translation>Нумерованный список (строчный алфавит)</translation>
    </message>
    <message>
        <source>Ordered List (Alpha upper)</source>
        <translation>Нумерованный список (ПРОПИСНОЙ АЛФАВИТ)</translation>
    </message>
</context>
<context>
    <name>vtr::Button</name>
    <message>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>Созд&amp;ать</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>&amp;New a image</source>
        <translation>&amp;Добавить изображение</translation>
    </message>
    <message>
        <source>New a image</source>
        <translation>Добавить изображение</translation>
    </message>
    <message>
        <source>Create &amp;New Database...</source>
        <translation>Созд&amp;ать базу данных...</translation>
    </message>
    <message>
        <source>Create New Database</source>
        <translation>Создать базу данных</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Open Database</source>
        <translation>Открыть базу данных</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <source>Clos&amp;e</source>
        <translation>Закр&amp;ыть</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>C&amp;lear</source>
        <translation>Удалит&amp;ь</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Cl&amp;ear All</source>
        <translation>Удали&amp;т&amp;ь всё</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>Удалить всё</translation>
    </message>
    <message>
        <source>Completely clears the document contents</source>
        <translation>Полностью и безвозвратно удаляет содержимое документа</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&amp;Печать...</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>Print Preview...</source>
        <translation>Предварительный просмотр...</translation>
    </message>
    <message>
        <source>Print Preview</source>
        <translation>Предварительный просмотр</translation>
    </message>
    <message>
        <source>U&amp;ndo</source>
        <translation>От&amp;менить</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <source>Re&amp;do</source>
        <translation>Вер&amp;нуть</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <source>&amp;Left</source>
        <translation>по &amp;Левому краю</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>по Левому краю</translation>
    </message>
    <message>
        <source>Cen&amp;ter</source>
        <translation>по &amp;Центру</translation>
    </message>
    <message>
        <source>Center</source>
        <translation>по Центру</translation>
    </message>
    <message>
        <source>&amp;Right</source>
        <translation>по Правому кра&amp;ю</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>по Правому краю</translation>
    </message>
    <message>
        <source>&amp;Justify</source>
        <translation>по &amp;Ширине</translation>
    </message>
    <message>
        <source>Justify</source>
        <translation>по Ширине</translation>
    </message>
    <message>
        <source>&amp;Bold</source>
        <translation>Полу&amp;жирный</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Полужирный</translation>
    </message>
    <message>
        <source>&amp;Italic</source>
        <translation>К&amp;урсив</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation>По&amp;дчёркнутый</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>Подчёркнутый</translation>
    </message>
    <message>
        <source>&amp;Help Contents...</source>
        <translation>&amp;Содержание &amp;справки...</translation>
    </message>
    <message>
        <source>Help Contents</source>
        <translation>Содержание справки</translation>
    </message>
    <message>
        <source>Ho&amp;me Page...</source>
        <translation>Домашн&amp;яя страница...</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation>Домашняя страница</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <source>Restore Default Settings</source>
        <translation>Восстановить значения по умолчанию</translation>
    </message>
    <message>
        <source>&amp;How To Buy %1</source>
        <translation>&amp;Как приобрести %1</translation>
    </message>
    <message>
        <source>&amp;Thanks</source>
        <translation>&amp;Хорошо</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>П&amp;равка</translation>
    </message>
    <message>
        <source>F&amp;ormat</source>
        <translation>Ф&amp;ормат</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <source>Align</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <source>List</source>
        <translation>Список</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <source>Undo/Redo</source>
        <translation>Отменить/Вернуть</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation>Созд&amp;ать...</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <source>&amp;Save as Template</source>
        <translation>&amp;Сохранить как Шаблон</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>О&amp;тмена</translation>
    </message>
    <message>
        <source>&amp;Add Ideal Proportions...</source>
        <translation>&amp;Добавить Идеальные Пропорции ...</translation>
    </message>
    <message>
        <source>Photos</source>
        <translation>Фотографии</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <source>&amp;New User</source>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <source>&amp;Continue</source>
        <translation>П&amp;родолжить</translation>
    </message>
    <message>
        <source>Open File...</source>
        <translation>Открыть файл...</translation>
    </message>
    <message>
        <source>Print Document</source>
        <translation>Печатать</translation>
    </message>
</context>
<context>
    <name>vtr::Chart</name>
    <message>
        <source>&amp;Open Chart Panel</source>
        <comment>Open Chart Panel</comment>
        <translation>&amp;Отобразить панель Диаграмм</translation>
    </message>
    <message>
        <source>Open Panel</source>
        <comment>Open Chart Panel</comment>
        <translation>Открыть панель</translation>
    </message>
    <message>
        <source>&amp;Close Chart Panel</source>
        <comment>Close Chart Panel</comment>
        <translation>&amp;Скрыть панель Диаграмм</translation>
    </message>
    <message>
        <source>Close Panel</source>
        <comment>Close Chart Panel</comment>
        <translation>Закрыть панель</translation>
    </message>
    <message>
        <source>&amp;Show All</source>
        <comment>Show All Charts</comment>
        <translation>Показать &amp;всё</translation>
    </message>
    <message>
        <source>Show All Charts</source>
        <translation>Отобразить все Диаграммы</translation>
    </message>
    <message>
        <source>&amp;Hide All</source>
        <comment>Hide All Charts</comment>
        <translation>Скр&amp;ыть всё</translation>
    </message>
    <message>
        <source>Hide All Charts</source>
        <translation>Скрыть все Диаграммы</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Обычная</translation>
    </message>
    <message>
        <source>Style Simple</source>
        <translation>Стиль \&quot;Обычный\&quot;</translation>
    </message>
    <message>
        <source>&amp;Percentage</source>
        <translation>&amp;Процентный</translation>
    </message>
    <message>
        <source>Style Percentage</source>
        <translation>Стиль \&quot;Процетный\&quot;</translation>
    </message>
    <message>
        <source>S&amp;cale</source>
        <translation>С ма&amp;штабированием</translation>
    </message>
    <message>
        <source>Style Scale</source>
        <translation>Стиль \&quot;C маштабированием\&quot;</translation>
    </message>
    <message>
        <source>&amp;Histogram</source>
        <translation>&amp;Гистограмма</translation>
    </message>
    <message>
        <source>Type Histogram</source>
        <translation>Тип \&quot;Гистограмма&quot;</translation>
    </message>
    <message>
        <source>&amp;Pie</source>
        <translation>&amp;Круговая</translation>
    </message>
    <message>
        <source>Type Pie</source>
        <translation>Тип \&quot;Круговая\&quot;</translation>
    </message>
    <message>
        <source>&amp;Text</source>
        <translation>&amp;Текст</translation>
    </message>
    <message>
        <source>Type Text</source>
        <translation>Тип \&quot;Текстовая\&quot;</translation>
    </message>
    <message>
        <source>Chart &amp;Type</source>
        <translation>&amp;Тип диаграммы</translation>
    </message>
    <message>
        <source>Chart &amp;Style</source>
        <translation>&amp;Стиль диаграммы</translation>
    </message>
    <message>
        <source>Chart &amp;Color</source>
        <translation>&amp;Цвет диаграммы</translation>
    </message>
    <message>
        <source>&amp;Red</source>
        <translation type="obsolete">&amp;Красный</translation>
    </message>
    <message>
        <source>&amp;Green</source>
        <translation type="obsolete">&amp;Зелёный</translation>
    </message>
    <message>
        <source>&amp;Blue</source>
        <translation type="obsolete">&amp;Голубой</translation>
    </message>
    <message>
        <source>&amp;Cyan</source>
        <translation type="obsolete">&amp;Сине-зелёный</translation>
    </message>
    <message>
        <source>&amp;Magenta</source>
        <translation type="obsolete">&amp;Красно-синий</translation>
    </message>
    <message>
        <source>&amp;Yellow</source>
        <translation type="obsolete">&amp;Жёлтый</translation>
    </message>
    <message>
        <source>Gr&amp;ay</source>
        <translation type="obsolete">&amp;Серый</translation>
    </message>
    <message>
        <source>B&amp;lack</source>
        <translation type="obsolete">&amp;Чёрный</translation>
    </message>
    <message>
        <source>Visible &amp;Charts</source>
        <translation>&amp;Доступные диаграммы</translation>
    </message>
    <message>
        <source>&amp;Open Chart Panel</source>
        <translation>&amp;Открыть панель диаграмм</translation>
    </message>
    <message>
        <source>&amp;Close Chart Panel</source>
        <translation>&amp;Закрыть панель диаграмм</translation>
    </message>
    <message>
        <source>Chart &apos;Growth&apos;</source>
        <translation>диаграмма \&apos;Рост\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Biceps&apos;</source>
        <translation>диаграмма \&apos;Бицепсы\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Abdomen&apos;</source>
        <translation>диаграмма \&apos;Живот\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Calves&apos;</source>
        <translation>диаграмма \&apos;Икроножные\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Chest&apos;</source>
        <translation>диаграмма \&apos;Грудные\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Forearms&apos;</source>
        <translation>диаграмма \&apos;Предплечья\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Hips&apos;</source>
        <translation>диаграмма \&apos;Бедра\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Neck&apos;</source>
        <translation>диаграмма \&apos;Шея\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Shoulders&apos;</source>
        <translation>диаграмма \&apos;Дельтовидные\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Thighs&apos;</source>
        <translation>диаграмма \&apos;Бедра\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Waist&apos;</source>
        <translation>диаграмма \&apos;Талия\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Weight&apos;</source>
        <translation>диаграмма \&apos;Вес\&apos;</translation>
    </message>
    <message>
        <source>Chart &apos;Wrist&apos;</source>
        <translation>диаграмма \&apos;Запястья\&apos;</translation>
    </message>
</context>
<context>
    <name>vtr::Color</name>
    <message>
        <source>&amp;Color...</source>
        <translation>&amp;Цвет...</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Цветовая палитра</translation>
    </message>
    <message>
        <source>&amp;Red</source>
        <translation>&amp;Красный</translation>
    </message>
    <message>
        <source>Color Red</source>
        <translation>Красный цвет</translation>
    </message>
    <message>
        <source>&amp;Green</source>
        <translation>&amp;Зелёный</translation>
    </message>
    <message>
        <source>Color Green</source>
        <translation>Зелёный цвет</translation>
    </message>
    <message>
        <source>&amp;Blue</source>
        <translation>&amp;Голубой</translation>
    </message>
    <message>
        <source>Color Blue</source>
        <translation>Голубой цвет</translation>
    </message>
    <message>
        <source>&amp;Cyan</source>
        <translation>&amp;Сине-зелёный</translation>
    </message>
    <message>
        <source>Color Cyan</source>
        <translation>Сине-зелёный цвет</translation>
    </message>
    <message>
        <source>&amp;Magenta</source>
        <translation>&amp;Красно-синий</translation>
    </message>
    <message>
        <source>Color Magenta</source>
        <translation>Красно-синий цвет</translation>
    </message>
    <message>
        <source>&amp;Yellow</source>
        <translation>&amp;Жёлтый</translation>
    </message>
    <message>
        <source>Color Yellow</source>
        <translation>Жёлтый цвет</translation>
    </message>
    <message>
        <source>Gr&amp;ay</source>
        <translation>&amp;Серый</translation>
    </message>
    <message>
        <source>Color Gray</source>
        <translation>Серый цвет</translation>
    </message>
    <message>
        <source>B&amp;lack</source>
        <translation>&amp;Чёрный</translation>
    </message>
    <message>
        <source>Color Black</source>
        <translation>Чёрный цвет</translation>
    </message>
    <message>
        <source>Color palette</source>
        <translation>Цветовая палитра</translation>
    </message>
</context>
<context>
    <name>vtr::Copyright</name>
    <message>
        <source>&amp;Registration...</source>
        <translation>Регистрац&amp;ия...</translation>
    </message>
    <message>
        <source>Registration</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <source>This product is licensed to:</source>
        <translation>Права на использование этой версии принадлежат:</translation>
    </message>
    <message>
        <source>Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.</source>
        <translation>ВНИМАНИЕ: Эта компьютерная программа защищена законом «Об авторском праве и смежных правах», а также  международными соглашениями. Несанкционированное воспроизводство и распространение программы или каких-либо ее компонентов влечет к гражданской или уголовной ответственности, и преследуется по закону.</translation>
    </message>
    <message>
        <source>Copyright (C) 2005-2010 Lythos Software Inc. All rights reserved.</source>
        <translation>Copyright (C) 2005-2010 Lythos Software Inc. Все права защищены.</translation>
    </message>
    <message>
        <source> [trial version]</source>
        <translation> [пробная версия]</translation>
    </message>
    <message>
        <source>

Sincerely,
     Panin S.</source>
        <translation>
		
		C уважением,    С. Панин.</translation>
    </message>
</context>
<context>
    <name>vtr::Date</name>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>&amp;Date:</source>
        <translation>&amp;Дата:</translation>
    </message>
    <message>
        <source>&amp;Date of Birth:</source>
        <translation>Дата ро&amp;ждения:</translation>
    </message>
    <message>
        <source>Progress:</source>
        <translation>Прогресс:</translation>
    </message>
    <message>
        <source>Period left:</source>
        <translation>Осталось:</translation>
    </message>
    <message>
        <source> %1 day(days)</source>
        <translation> %1 дня(дней)</translation>
    </message>
</context>
<context>
    <name>vtr::Dialog</name>
    <message>
        <source>Choose the database file name</source>
        <translation>Выберете имя базы данных</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <source>Print Introduction</source>
        <translation>Распечатать Вступительное слово</translation>
    </message>
    <message>
        <source>Edit in new window...</source>
        <translation>Редактировать в новом окне...</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>New name of the image:</source>
        <translation type="obsolete">Введите имя изображения:</translation>
    </message>
    <message>
        <source>Main</source>
        <translation>Главная</translation>
    </message>
    <message>
        <source>Personal office [*]</source>
        <translation>Личный кабинет [*]</translation>
    </message>
    <message>
        <source>Show this dialog at startup</source>
        <translation>Показывать это окно каждый раз при запуске</translation>
    </message>
    <message>
        <source>- Enter your personal data (information) including: Personal Info (Name, Password, Date of Birth, Sex, Height), and Contact Info(Zip code, Country,  State/Region, City/Town, Address (Street, Apartment), E-Mail, Phone number, ICQ, Mobule) and Additional info (Doctor, Instructor).</source>
        <translation>- Страница персональных данных позволяет зафиксировать все необходимые данные о Вас, а именно: личную информацию (имя пользователя, пароль, дата рождения, пол, рост), контактную информацию (страна проживания, район или область, город, домашний адрес, адрес электронной почты (E-Mail), номер телефона, номер ICQ, номер мобильного телефона), а также дополнительную информацию (имя персонального доктора, имя персонального тренера или партнёра по занятиям).</translation>
    </message>
    <message>
        <source>PhotoGallery</source>
        <translation>Фотогалерея</translation>
    </message>
    <message>
        <source>- The page &quot;Photogallery&quot; allows you  to keep the most important photos of your successes and achievements step by step, day by day.  Regular supplements to the Photogallery with new photos will allow you to timely make necessary correcting amendments to the working program of trainings with the purpose of advancing results.</source>
        <translation>-  Страница “Фотогалерея” позволяет Вам шаг за шагом, день за днем  сохранить наиболее важные фотографии Ваших успехов и достижений.  Регулярное пополнение Фотогалереи новыми фотографиями позволит Вам своевременно вносить необходимые корректирующие поправки в рабочую программу тренировок с целью усиления результатов.</translation>
    </message>
    <message>
        <source>Record History</source>
        <translation>Рекорды</translation>
    </message>
    <message>
        <source>- The page &quot;Records&quot; will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way.</source>
        <translation>-  Страница “Рекорды” позволит Вам систематизировать Ваши спортивные рекорды, а именно: внести основные данные о текущих рекордах, сгруппированных по имени упражнения; запланировать желаемый результат в практикуемом (тренируемом) упражнении.
Систематическое добавление персональных рекордов и тщательный анализ динамики их роста позволит Вам ставить перед собой и достигать намеченных целей наиболее продуктивным образом.</translation>
    </message>
    <message>
        <source>Add/Edit a Ideal Proportions</source>
        <translation>Добавить/Изменить идеальные пропорции</translation>
    </message>
    <message>
        <source>Add/Edit a Measurement Record</source>
        <translation>Добавить/Изменить размеры мышечных групп</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>Им&amp;я:</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation>&amp;Пароль:</translation>
    </message>
    <message>
        <source>&amp;Re-enter password:</source>
        <translation>Повторите па&amp;роль:</translation>
    </message>
    <message>
        <source>Add/Edit a Personal Record</source>
        <translation>Добавить/Изменить рекорд</translation>
    </message>
    <message>
        <source>Sign In</source>
        <translation>Войти</translation>
    </message>
    <message>
        <source>User Name and password</source>
        <translation>Имя пользователя и пароль</translation>
    </message>
    <message>
        <source>&amp;Select User:</source>
        <translation>&amp;Имя пользователя:</translation>
    </message>
    <message>
        <source>&amp;Exit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <source>New Name a Image:</source>
        <translation>Введите имя изображения:</translation>
    </message>
    <message>
        <source>Measurement History</source>
        <translation>История измерений размеров мышечных групп</translation>
    </message>
    <message>
        <source>- The page &quot;Measurements&quot; will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way.</source>
        <translation>-  Страница “Измерения” позволит Вам тщательным образом контролировать изменения в Вашем телосложении и своевременно оптимизировать рабочую программу тренировок с целью максимально быстрого достижения Вами запланированного  идеального телосложения. Дополнительно данная страница содержит модуль идеальных пропорций  по следующим методикам: и др. методикапнаиболее эффективным образом.</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Общие настройки системы</translation>
    </message>
    <message>
        <source>- The page &quot;Options&quot; will allow you to systematize your sports records, namely: to enter data about the current records grouped by naming (type) of exercises; to plan a desirable result in the exercise to be practised. Regular supplements with personal records and careful analysis of the growth dynamics will allow you to set desired targets and to realize them in the most productive way.</source>
        <translation>- Страница “Опции” позволит Вам настроить программу по вашему вкусу, а именно: изменить язык интерфейса приложения.</translation>
    </message>
    <message>
        <source>Measurement Record</source>
        <translation>Таблица измерений</translation>
    </message>
    <message>
        <source>Option Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Персональная информация</translation>
    </message>
</context>
<context>
    <name>vtr::Editor</name>
    <message>
        <source>C&amp;ut</source>
        <translation>&amp;Вырезать</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <source>Cut the current selection&apos;s contents to the clipboard.</source>
        <translation>Помещает выделенный фрагмент текста в буфер обмена.</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>Копи&amp;ровать</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Copy the current selection&apos;s contents to the clipboard.</source>
        <translation>Копирует  выделенный фрагмент текста в буфер обмена.</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>Встави&amp;ть</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <source>Paste the clipboard&apos;s contents into the current selection.</source>
        <translation>Вставляет содержимое буфера обмена в текущую позицию курсора.</translation>
    </message>
</context>
<context>
    <name>vtr::Exercise</name>
    <message>
        <source>Exercise</source>
        <translation>Упражнение</translation>
    </message>
    <message>
        <source>Select Exercise</source>
        <translation>Выбор упражнения</translation>
    </message>
    <message>
        <source>&amp;Exercise</source>
        <translation>&amp;Упражнение</translation>
    </message>
</context>
<context>
    <name>vtr::Filter</name>
    <message>
        <source>Database file %1All files %2</source>
        <translation>База данных %1Все файлы %2</translation>
    </message>
    <message>
        <source>Image Files %1Microsoft Windows Bitmap BMP%2Graphics Interchange Format GIF %3Portable Network Graphics PNG %4JPEG %5XPixMap XPM %6PPM %7</source>
        <translation>Файлы изображений %1Microsoft Windows Bitmap BMP%2Graphics Interchange Format GIF %3Portable Network Graphics PNG %4JPEG %5XPixMap XPM %6PPM %7</translation>
    </message>
    <message>
        <source>Image Files %1Microsoft Windows Bitmap BMP%2Portable Network Graphics PNG %3JPEG %4</source>
        <translation>Файлы изображений %1Microsoft Windows Bitmap BMP%2Portable Network Graphics PNG %3JPEG %4</translation>
    </message>
    <message>
        <source>All supported types %1Adobe PDF Files %2ODF files %3Hyper Text Markup Language file %4Text Files %5</source>
        <translation>Все поддержанные типы файлов %1Adobe PDF Files %2ODF files %3Hyper Text Markup Language file %4Text Files %5</translation>
    </message>
    <message>
        <source>All supported types %1ODF files %2Hyper Text Markup Language file %3Text Files %4</source>
        <translation>Все поддержанные типы файлов %1ODF files %2Hyper Text Markup Language file %3Text Files %4</translation>
    </message>
</context>
<context>
    <name>vtr::Image</name>
    <message>
        <source>&amp;Delete a image</source>
        <translation>&amp;Удалить изображение</translation>
    </message>
    <message>
        <source>Delete a image</source>
        <translation>Удалить изображение</translation>
    </message>
</context>
<context>
    <name>vtr::LD</name>
    <message>
        <source>Personal information</source>
        <translation>Персональная информация</translation>
    </message>
    <message>
        <source>Contact info</source>
        <translation>Контактная информация</translation>
    </message>
    <message>
        <source>&amp;Country:</source>
        <translation>Стра&amp;на:</translation>
    </message>
    <message>
        <source>State/Re&amp;gion:</source>
        <translation>Область/Ра&amp;йон:</translation>
    </message>
    <message>
        <source>C&amp;ity/Town:</source>
        <translation>&amp;Город:</translation>
    </message>
    <message>
        <source>&amp;Address:</source>
        <translation>&amp;Адрес:</translation>
    </message>
    <message>
        <source>&amp;E-mail:</source>
        <translation>&amp;E-mail:</translation>
    </message>
    <message>
        <source>IC&amp;Q:</source>
        <translation>IC&amp;Q:</translation>
    </message>
    <message>
        <source>Phone n&amp;umber:</source>
        <translation>Теле&amp;фон:</translation>
    </message>
    <message>
        <source>&amp;Mobule:</source>
        <translation>&amp;Мобильный:</translation>
    </message>
    <message>
        <source>Personal info</source>
        <translation>Персональная информация</translation>
    </message>
    <message>
        <source>Additional info</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <source>Doc&amp;tor:</source>
        <translation>Док&amp;тор:</translation>
    </message>
    <message>
        <source>Instruct&amp;or:</source>
        <translation>Инструкт&amp;ор:</translation>
    </message>
</context>
<context>
    <name>vtr::Language</name>
    <message>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Язык</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Русский</translation>
    </message>
    <message>
        <source>&amp;User interface</source>
        <translation>&amp;Язык интерфейса</translation>
    </message>
</context>
<context>
    <name>vtr::Mea</name>
    <message>
        <source>inch</source>
        <translation>дюйм</translation>
    </message>
    <message>
        <source>inches</source>
        <translation>дюймы</translation>
    </message>
    <message>
        <source>cm</source>
        <translation>см</translation>
    </message>
    <message>
        <source>cms</source>
        <translation>см</translation>
    </message>
    <message>
        <source>lb</source>
        <translation>фунт</translation>
    </message>
    <message>
        <source>lbs</source>
        <translation>фунты</translation>
    </message>
    <message>
        <source>kg</source>
        <translation>кг</translation>
    </message>
</context>
<context>
    <name>vtr::Message</name>
    <message>
        <source>Database &apos;%1&apos; is successfully created!</source>
        <translation>База данных &apos;%1&apos; успешно создана!</translation>
    </message>
    <message>
        <source>%1 could not open &apos;%2&apos; because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn&apos;t correctly decoded).</source>
        <translation>Программа &quot;%1&quot; не может открыть файл &apos;%2&apos;.
		Файл поврежден или имеет неизвестный формат.</translation>
    </message>
    <message>
        <source>Database &apos;%1&apos; is successfully opened!</source>
        <translation>База данных &apos;%1&apos; успешно открыта!</translation>
    </message>
    <message>
        <source>Error while trying to save database file &apos;%1&apos; !</source>
        <translation>Невозможно выполнить текущую операцию.
		Сохранение базы данных ’%1’ привело к ошибке!
		Убедитесь, что база данных открыта и не используется сторонними программами!</translation>
    </message>
    <message>
        <source>Database &apos;%1&apos; is successfully saved!</source>
        <translation>База данных &apos;%1&apos; успешно сохранена!</translation>
    </message>
    <message>
        <source>Dear Madam,
Dear Sir.

   Thank you very much for evaluating %1. I am sure you will soon find %1 to be an assistant you don&apos;t want to be without, when working with trainings.

   %1 tracks everything related to bodybuilding and fitness: track exercises, meals, measurements, record results, general health and more! You can quickly and easily work with your product information directly in %1. Easily track and visualize progress, know if your training workouts are effective, and reach goals faster. Includes a  journal for recording information and four major tools to view and analyze progress.

   %1 is highly customizable. Whether you&apos;re a bodybuilder, a cyclist, training for a marathon, or just want to monitor and control your body weight, %1 has everything you need.</source>
        <translation>      Уважаемые дамы и господа!
Большое спасибо Вам за проявленный интерес к программе ‘%1’ нашей компании. Я убежден в том, что в скором времени программа ‘%1’ станет прекрасным помощником для Вас в систематизации тренировочных данных различной направленности.

   С помощью программы ‘%1’ Вы сможете вести дневник тренировок, изучить и скорректировать Вашу технику выполнения упражнений, отслеживать динамику роста результатов (рекордов), вносить антропометрические данные, а также следить и, что более важно, контролировать общее состояние здоровья как глобально, на уровне системы, так и локально, т.е. контролировать состояние здоровья или его некоторых составляющих (артериальное давление, температура тела, частота пульса и др.) в наиболее важных контрольных точках (до и после проведённой тренировки) и многое другое.

   Основной навигационный календарь программы ‘%1’, а также несколько его собратьев позволят Вам буквально в несколько щелчков мыши манипулировать всей совокупностью тренировочных данных (добавлять, редактировать, удалять, перемещать тренировки, а также производить другие операции над данными).

   Занимаетесь ли Вы бодибилдингом, фитнесом, пауэрлифтингом или бегом на короткие и длинные дистанции, возможно, Вы просто следите за собственным весом, в любом случае стандартного набора технических средств программы ‘%1’ будет достаточно для удовлетворения практических всех Ваших требований по систематизации тренировочных данных.

Помните, что программа ‘%1’ не является бесплатной. Чтобы использовать в своей работе все её возможности, Вам необходимо приобрести лицензию.</translation>
    </message>
    <message>
        <source>Save changes to the document before quitting?

If you don&apos;t save, your changes will be lost.</source>
        <translation>Сохранить изменения в документе?.</translation>
    </message>
    <message>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Невозможно выполнить текущую операцию!
		Создание файла ‘%1’ завершилось неудачей!
		Попробуйте создать файл с отличным от текущего именем и вновь выполнить операцию сохранения.</translation>
    </message>
    <message>
        <source>Cannot write file %1:
%2.</source>
        <translation>Невозможно выполнить текущую операцию!
		Создание файла ‘%1’ завершилось неудачей:
		%2.</translation>
    </message>
    <message>
        <source>Are you sure you want to remove the selected record?!</source>
        <translation>Вы действительно хотите удалитьтекущий рекорд?!</translation>
    </message>
    <message>
        <source>Are you sure you want to remove all records?!</source>
        <translation>Вы действительно хотите удалить все существующие рекорды?!</translation>
    </message>
    <message>
        <source>Are you sure you want to send the image to the Recycle Bin?!</source>
        <translation>Вы действительно хотите удалить изображение?!</translation>
    </message>
    <message>
        <source>%1Try to reinstall to fix this problem.</source>
        <translation type="obsolete">%1Пожалуйста, переустановите приложение.</translation>
    </message>
    <message>
        <source>Your data are successfully updated!</source>
        <translation>Ваши данные успешно обновлены!</translation>
    </message>
    <message>
        <source>Do you want to save the changes to this document before closing?

If you don&apos;t save, your changes will be lost.</source>
        <translation>Сохранить внесённые изменения?.</translation>
    </message>
    <message>
        <source>The user with a name &apos;%1&apos; already exists!</source>
        <translation>Пользователь с именем &quot;%1&quot; уже зарегистрирован в системе!</translation>
    </message>
    <message>
        <source>Save changes to the Personal Office document before quitting?

If you don&apos;t save, your changes will be lost.</source>
        <translation>Сохранить внесённые изменения?.</translation>
    </message>
    <message>
        <source>%1 could not open &apos;%2&apos; because it Is either not a supported file type or because the file has been damaged!</source>
        <translation>Программа &quot;%1&quot; не может открыть файл &apos;%2&apos;.
		Файл поврежден или имеет неизвестный формат!</translation>
    </message>
    <message>
        <source>Error while trying to create database file &apos;%1&apos; !</source>
        <translation>Невозможно выполнить текущую операцию. Создание базы данных ’%1’ привело к ошибке.
		Попробуйте создать файл с отличным от текущего именем и вновь выполнить операцию сохранения!</translation>
    </message>
</context>
<context>
    <name>vtr::Method</name>
    <message>
        <source>&amp;Method:</source>
        <translation>&amp;Метод:</translation>
    </message>
</context>
<context>
    <name>vtr::Panel</name>
    <message>
        <source>My Current Result</source>
        <translation>Текущий результат</translation>
    </message>
    <message>
        <source>My Desired Result</source>
        <translation>Желаемый результат</translation>
    </message>
    <message>
        <source>Measurement Summary</source>
        <translation>Резюме</translation>
    </message>
    <message>
        <source>Personal Measurement History</source>
        <translation>Измерения</translation>
    </message>
    <message>
        <source>Measurement History</source>
        <translation>Измерения</translation>
    </message>
</context>
<context>
    <name>vtr::Record</name>
    <message>
        <source>&amp;New Record...</source>
        <translation>&amp;Новый рекорд...</translation>
    </message>
    <message>
        <source>Create New Record</source>
        <translation>Добавить новый рекорд</translation>
    </message>
    <message>
        <source>&amp;Open Record...</source>
        <translation>&amp;Открыть рекорд...</translation>
    </message>
    <message>
        <source>Open Record</source>
        <translation>Открыть рекорд</translation>
    </message>
    <message>
        <source>&amp;Delete Record</source>
        <translation>&amp;Удалить рекорд</translation>
    </message>
    <message>
        <source>Delete Current Record</source>
        <translation>Удалить рекорд</translation>
    </message>
    <message>
        <source>Record Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Record Weight</source>
        <translation type="obsolete">Вес</translation>
    </message>
    <message>
        <source>Record Repetitions</source>
        <translation>Кол-во повторений</translation>
    </message>
    <message>
        <source>Record Notes</source>
        <translation>Примечание</translation>
    </message>
    <message>
        <source>My Current Result</source>
        <translation>Текущий результат</translation>
    </message>
    <message>
        <source>My Desired Result</source>
        <translation>Желаемый результат</translation>
    </message>
    <message>
        <source>Enter the Date of the Record</source>
        <translation>Дата рекорда</translation>
    </message>
    <message>
        <source>Enter Record</source>
        <translation>Рекорд</translation>
    </message>
    <message>
        <source>Enter Record Notes</source>
        <translation>Примечание</translation>
    </message>
    <message>
        <source>Personal Records History</source>
        <translation>Рекорды</translation>
    </message>
    <message>
        <source>Personal Records</source>
        <translation>Таблица рекордов</translation>
    </message>
    <message>
        <source>Record Weight (%1):</source>
        <translation>Вес (%1):</translation>
    </message>
    <message>
        <source>Weight (%1):</source>
        <translation>Вес (%1):</translation>
    </message>
</context>
<context>
    <name>vtr::Training</name>
    <message>
        <source>&amp;Repetitions:</source>
        <translation>&amp;Кол-во повторений:</translation>
    </message>
</context>
</TS>
